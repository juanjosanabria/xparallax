QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

#--------------------------------------------------------------------
#Dependencias
#--------------------------------------------------------------------
DEPENDPATH += .
INCLUDEPATH += .

DEPENDPATH +=  ../Core
INCLUDEPATH +=  ../Core

DEPENDPATH +=  ../Algebra
INCLUDEPATH +=  ../Algebra

DEPENDPATH +=  ../Novas
INCLUDEPATH +=  ../Novas

DEPENDPATH +=  ../Sky
INCLUDEPATH +=  ../Sky

SOURCES +=  tst_xptest.cpp

