#-------------------------------------------------
#
# Project created by QtCreator 2013-08-08T14:01:29
#
#-------------------------------------------------

QT       -= gui

TARGET = Algebra
TEMPLATE = lib

DEFINES += ALGEBRA_LIBRARY
DEFINES  += _CRT_SECURE_NO_WARNINGS

#Optimizaciones de paralelización para MSVC
mingw:{
QMAKE_CXXFLAGS += -std=gnu++0x
QMAKE_CXXFLAGS += -Wno-unused
QMAKE_CXXFLAGS += -Wno-format
QMAKE_CXXFLAGS += -Wno-extra
}

release:{
   # QMAKE_CXXFLAGS += -Qpar
   # QMAKE_CXXFLAGS += -Qvec-report:1
}



linux:{
    CONFIG += static
    QMAKE_CXXFLAGS += -std=c++11
}

macx:{
    CONFIG += static
}

SOURCES += \
    statistics.cpp \
    specialfunctions.cpp \
    solvers.cpp \
    optimization.cpp \
    linalg.cpp \
    interpolation.cpp \
    integration.cpp \
    fasttransforms.cpp \
    diffequations.cpp \
    dataanalysis.cpp \
    ap.cpp \
    alglibmisc.cpp \
    alglibinternal.cpp

HEADERS +=\
        algebra_global.h \
    stdafx.h \
    statistics.h \
    specialfunctions.h \
    solvers.h \
    optimization.h \
    linalg.h \
    interpolation.h \
    integration.h \
    fasttransforms.h \
    diffequations.h \
    dataanalysis.h \
    ap.h \
    alglibmisc.h \
    alglibinternal.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
