#ifndef ALGEBRA_GLOBAL_H
#define ALGEBRA_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ALGEBRA_LIBRARY)
#  define ALGEBRASHARED_EXPORT Q_DECL_EXPORT
#else
#  define ALGEBRASHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ALGEBRA_GLOBAL_H
