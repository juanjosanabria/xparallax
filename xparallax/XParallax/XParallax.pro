#-------------------------------------------------
#
# Project created by QtCreator 2013-07-18T21:19:16
#
#-------------------------------------------------

QT       += core gui widgets network xml concurrent svg script
TARGET = XParallax
TEMPLATE = app
DEFINES  += _CRT_SECURE_NO_WARNINGS

#Consultar: http://qt-project.org/doc/qt-4.8/qmake-advanced-usage.html
VERSION = 1.2.5

RC_FILE = myapp.rc
#DEFINES += _CRT_SECURE_NO_WARNINGS

#--------------------------------------------------------------------
#Flags de construcción
#--------------------------------------------------------------------
mingw:{
    QMAKE_CXXFLAGS += -std=gnu++0x -Wdeprecated-declarations
}


#--------------------------------------------------------------------
#Dependencias
#--------------------------------------------------------------------
DEPENDPATH += .
INCLUDEPATH += .

DEPENDPATH +=  ../Core
INCLUDEPATH +=  ../Core

DEPENDPATH +=  ../Algebra
INCLUDEPATH +=  ../Algebra

DEPENDPATH +=  ../Novas
INCLUDEPATH +=  ../Novas

DEPENDPATH +=  ../Sky
INCLUDEPATH +=  ../Sky

win32 { ICON = icons/favicon.ico}
win32:release {
    LIBS +=  "-L$$OUT_PWD/../Algebra/Release" -lAlgebra
    LIBS +=  "-L$$OUT_PWD/../Core/Release" -lCore
    LIBS +=  "-L$$OUT_PWD/../Novas/Release" -lNovas
    LIBS +=  "-L$$OUT_PWD/../Sky/Release" -lSky
}
win32:debug {
    LIBS +=  "-L$$OUT_PWD/../Algebra/Debug" -lAlgebra
    LIBS +=  "-L$$OUT_PWD/../Core/Debug" -lCore
    LIBS +=  "-L$$OUT_PWD/../Novas/Debug" -lNovas
    LIBS +=  "-L$$OUT_PWD/../Sky/Debug" -lSky
}


release {
    #QMAKE_CXXFLAGS += -Qpar
    #QMAKE_CXXFLAGS += -Qvec-report:1
}

linux:{
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Core" -lCore
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
    LIBS += -L"$$OUT_PWD/../Sky" -lSky
    RESOURCES += ../Core/res_core.qrc
    QMAKE_CXXFLAGS += -std=c++11
    #Indica dos posibles rutas para cargar las librerías dinámicas
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN/lib
}

macx:{
    ICON = icons/favicon.icns
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Core" -lCore
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
    LIBS += -L"$$OUT_PWD/../Sky" -lSky
    RESOURCES += ../Core/res_core.qrc
}

INCLUDEPATH +=  ui_input_widgets


#Optimizaciones de paralelización para MSVC (de momento para msvc2012 no funcionan, cuando se cambie a 2013)
#CONFIG(MSVC){
#    release:{
#       QMAKE_CXXFLAGS += /Qpar
#        QMAKE_CXXFLAGS += /Qvec
#        QMAKE_CXXFLAGS += /Qvec-report:2
#    }
#}


#unix:LIBS+=  -L../../build/Core/debug -lCore
#unix:LIBS+=  -L../../build/Algebra/debug -lAlgebra

SOURCES += main.cpp\
        mainwindow.cpp \
    aboutdialog.cpp \
    astrometrydialog.cpp \
    mdifitimage.cpp \
    headersdialog.cpp \
    inputdialog.cpp \
    tests.cpp \
    ui_helpers/animatedlabel.cpp \
    ui_input_widgets/bkgparamslayout.cpp \
    ui_input_widgets/detparamslayout.cpp \
    ui_input_widgets/selectfileslayout.cpp \
    ui_input_widgets/pixelsizelayout.cpp \
    ui_input_widgets/astrometrylayout.cpp \
    ui_mdi_childs/mdilogger.cpp \
    ui_helpers/progressdialog.cpp \
    calibrationdialog.cpp \
    ui_input_widgets/calibrationlayout.cpp \
    ui_input_widgets/lightcurveparamslayout.cpp \
    ui_helpers/linechart.cpp \
    modext/blink/blinkdialog.cpp \
    overlay/xoverlaylayer.cpp \
    overlay/xoverlayitem.cpp \
    overlay/xoverlaylabel.cpp \
    overlay/xoverlaycircle.cpp \
    overlay/xoverlaystyle.cpp \
    overlay/xoverlaycross.cpp \
    overlay/xoverlaysegment.cpp \
    experimental.cpp \
    modext/blink/mdifitblink.cpp \
    imagecatalogdialog.cpp \
    imgtools/ximgtool.cpp \
    imgtools/ximgtoollinearprofile.cpp \
    imgtools/ximgtoollinearprofiledialog.cpp \
    imgtools/ximgtooldistance.cpp \
    imgtools/ximgtoollocate.cpp \
    imgtools/ximgtoolstretch.cpp \
    imgtools/ximgtoolstretchdialog.cpp \
    imgutils/batchheaderdialog.cpp \
    ui_mdi_childs/mdimpc.cpp \
    ui_mdi_childs/xpmdiwidget.cpp \
    tools/angulardistance.cpp \
    mdifitimage_mpc.cpp \
    imgtools/ximgtoolmpcaddmeasure.cpp \
    imgutils/headereditor.cpp \
    imgtools/ximgtoolgif.cpp \
    overlay/xoverlayrect.cpp \
    utilities/gif.cpp \
    tools/coordinatetransform.cpp \
    overlay/xoverlaypolygon.cpp

HEADERS  += mainwindow.h \
    aboutdialog.h \
    astrometrydialog.h \
    mdifitimage.h \
    headersdialog.h \
    inputdialog.h \
    ui_helpers/animatedlabel.h \
    ui_input_widgets/bkgparamslayout.h \
    ui_input_widgets/detparamslayout.h \
    ui_input_widgets/selectfileslayout.h \
    ui_input_widgets/pixelsizelayout.h \
    ui_input_widgets/astrometrylayout.h \
    ui_mdi_childs/mdilogger.h \
    ui_helpers/progressdialog.h \
    calibrationdialog.h \
    ui_input_widgets/calibrationlayout.h \
    ui_input_widgets/lightcurveparamslayout.h \
    ui_helpers/linechart.h \
    modext/blink/blinkdialog.h \
    overlay/xoverlaylayer.h \
    overlay/xoverlayitem.h \
    overlay/xoverlaylabel.h \
    overlay/xoverlay.h \
    overlay/xoverlaycircle.h \
    overlay/xoverlaystyle.h \
    overlay/xoverlaycross.h \
    types.h \
    overlay/xoverlaysegment.h \
    experimental.h \
    modext/blink/mdifitblink.h \
    imagecatalogdialog.h \
    imgtools/ximgtool.h \
    imgtools/ximgtoollinearprofile.h \
    imgtools/ximgtoollinearprofiledialog.h \
    imgtools/ximgtooldistance.h \
    imgtools/ximgtoollocate.h \
    imgtools/ximgtoolstretch.h \
    imgtools/ximgtoolstretchdialog.h \
    imgutils/batchheaderdialog.h \
    ui_mdi_childs/mdimpc.h \
    ui_mdi_childs/xpmdiwidget.h \
    tools/angulardistance.h \
    imgtools/ximgtoolmpcaddmeasure.h \
    imgutils/headereditor.h \
    utilities/gif.h \
    imgtools/ximgtoolgif.h \
    overlay/xoverlayrect.h \
    tools/coordinatetransform.h \
    overlay/xoverlaypolygon.h

FORMS    += mainwindow.ui \
    aboutdialog.ui \
    astrometrydialog.ui \
    headersdialog.ui \
    inputdialog.ui \
    ui_input_widgets/bkgparamslayout.ui \
    ui_input_widgets/detparamslayout.ui \
    ui_input_widgets/selectfileslayout.ui \
    ui_input_widgets/pixelsizelayout.ui \
    ui_input_widgets/astrometrylayout.ui \
    ui_helpers/progressdialog.ui \
    calibrationdialog.ui \
    ui_input_widgets/calibrationlayout.ui \
    ui_input_widgets/lightcurveparamslayout.ui \
    modext/blink/blinkdialog.ui \
    imagecatalogdialog.ui \
    imgtools/ximgtoollinearprofiledialog.ui \
    imgtools/ximgtoolstretchdialog.ui \
    imgutils/batchheaderdialog.ui \
    tools/angulardistance.ui \
    tools/coordinatetransform.ui

RESOURCES += \
    res_xp.qrc

OTHER_FILES += \
    myapp.rc
	

DISTFILES +=
