#include "inputdialog.h"
#include "ui_inputdialog.h"
#include "util.h"
#include "mainwindow.h"
#include "ui_helpers/animatedlabel.h"
#include "core.h"
#include "xglobal.h"
#include <QMessageBox>
#include <QLineEdit>
#include <QLayout>
#include <QFormLayout>
#include <QSpacerItem>
#include <QToolButton>
#include <QToolButton>

#define  ID_TYPE_UNKNOWN        0
#define  ID_TYPE_RA_DEC         1


InputDialog::InputDialog(const QString& title,QWidget *parent):
    QDialog(parent),
    ui(new Ui::InputDialog)
{
    ui->setupUi(this);
    setWindowFlags( windowFlags()  & ~Qt::WindowContextHelpButtonHint);
    SetTitle(title);
    SetSubTitle("");
    mFocusWidget = NULL;
    mSearchEdit = NULL;
    mFormLayout = NULL;
    mType = ID_TYPE_UNKNOWN;
}

InputDialog::InputDialog(QWidget *parent) : InputDialog("",parent)
{
}

InputDialog::~InputDialog()
{
    delete ui;
}

void InputDialog::SetTitle(const QString& title)
{
   setWindowTitle(title);
   if (title.length())
        ui->labelTitle->setText(title);
}

void InputDialog::SetSubTitle(const QString& subtitle)
{
   ui->labelSubtitle->setText(subtitle);
}

void InputDialog::SetImage(const QString& resource)
{
    SetImage(QPixmap(resource));
}

void InputDialog::SetImage(const QPixmap& resource)
{
    ui->labelPicture->setPixmap(resource);
}

void InputDialog::SetImageBehaivour(QString bh)
{
    bool ok;
    QStringList list = bh.split(";");
    for (int i=0; i<list.count(); i+=3)
    {
        list[i].toInt(&ok);
        if (!ok) continue;
        QString value = list[i+1];
        QString res = list[i+2];
        mImageBehaivour.append(QString("%1;%2;%3").arg(list[0], value, res));
    }
}

QString InputDialog::ShowText(const QString& label,const QString& def,const QString& inputMask)
{
    QVBoxLayout* layout = new QVBoxLayout(ui->baseFrame); ui->baseFrame->setLayout(layout);
    QLabel* llabel = new QLabel(this);
    llabel->setText(label);

    QLineEdit* ledit = new QLineEdit(this);
    ledit->setText(def);
    if (inputMask.length()) ledit->setInputMask(inputMask);

    layout->addWidget(llabel);
    layout->addWidget(ledit);
    layout->addItem(new QSpacerItem(10,10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding));


    MainWindow::DPIAware(this, true);
    ledit->setFocus();
    this->exec();
    if (result() == QDialog::Rejected) return "";
    //Borrar todo el layout por si se va a volver a mostrar el inputbox
    delete layout;
    return ledit->text();
}

QString InputDialog::ShowBlob(const QString& label,const QString& def)
{
    QVBoxLayout* layout = new QVBoxLayout(ui->baseFrame); ui->baseFrame->setLayout(layout);
    QLabel* llabel = new QLabel(this);
    llabel->setText(label);

    QTextEdit* ledit = new QTextEdit(this);
    ledit->setText(def);

    layout->addWidget(llabel);
    layout->addWidget(ledit);
    layout->addItem(new QSpacerItem(10,10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding));


    MainWindow::DPIAware(this, true);
    ledit->setFocus(); ledit->selectAll();
    this->exec();
    if (result() == QDialog::Rejected) return "";
    //Borrar todo el layout por si se va a volver a mostrar el inputbox
    delete layout;
    return ledit->document()->toPlainText();
}

QVariant InputDialog::ShowList(
                     const QString& caption,
                     QStringList labels,
                     QList<QVariant> values,
                     QVariant def
                     )
{
    QVBoxLayout* layout = new QVBoxLayout(ui->baseFrame); ui->baseFrame->setLayout(layout);
    QLabel* label = new QLabel(this);
    label->setText(caption);
    layout->addWidget(label);

    //Nos aseguramos de que los valores son los mismos ne número que las etiquetas
    for (int i=values.count(); i<labels.count(); i++)
        values << labels[i];

    QComboBox* combo = new QComboBox(this);
    layout->addWidget(combo);
    for (int i=0;i<values.length();i++)
    {
        combo->addItem(labels.at(i), values.at(i));
        if (!def.isNull() && def == values.at(i)) combo->setCurrentIndex(i);
    }
    layout->addItem(new QSpacerItem(10,10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding));

    MainWindow::DPIAware(this, true);
    this->setFixedSize(this->width(), this->height());
    exec();
    if (result() == QDialog::Rejected) return "";

    //Borrar todo el layout por si se va a volver a mostrar el inputbox
    delete layout;
    return combo->itemData(combo->currentIndex());
}


QStringList  InputDialog::ShowTextAndBlob(
                                 const QString& title_text,
                                 const QString& def_text,
                                 const QString& title_blob,
                                 const QString& def_blob
                                 )
{
    mFormLayout = new QFormLayout(ui->baseFrame); ui->baseFrame->setLayout(mFormLayout);
    QStringList res;
    QLineEdit* line_edit = new QLineEdit(this); line_edit->setText(def_text);
    QTextEdit* blob_edit = new QTextEdit(this); blob_edit->setText(def_blob);
    ui->baseFrame->setBaseSize(0, 80 * XGlobal::SCREEN_DPI);

    mFormLayout->addRow(title_text, line_edit);
    mFormLayout->addRow(title_blob, blob_edit);

    this->setBaseSize(0,0);
    MainWindow::DPIAware(this, true);
    //this->setFixedSize(this->width(), this->height());


    exec();
    //Sacamos los datos introducidos por el usuario a la lista
    res.append(line_edit->text());
    res.append(blob_edit->document()->toPlainText());
    delete line_edit;
    delete blob_edit;
    delete mFormLayout;
    mFormLayout = NULL;

    //Comprobamos la salida y devolvemos la lista vacía
    if (result() == QDialog::Rejected) return QStringList();
    return res;
}

QString InputDialog::objectName()
{
    return mSearchEdit->text();
}


QVector<double> InputDialog::ShowRADec(double def_ra, double def_dec, QString objname)
{
   mType = ID_TYPE_RA_DEC;
   QVector<double> vec;
   ui->labelPicture->setPixmap(QPixmap(":/graph/images/icn_globe.svg"));
   if (!ui->labelSubtitle->text().length()) this->SetSubTitle("Set the object coordinates.");

   //Layout principal (vertical)
   QVBoxLayout * vlayout = new QVBoxLayout(); ui->baseFrame->setLayout(vlayout);
   //vlayout->setSpacing(20);
   //Layout de formulario (RA, DEC)
   mFormLayout = new QFormLayout();
   //Layout inferior (horizontal de búsqueda)
   QHBoxLayout * hlayout = new QHBoxLayout();

   vlayout->addLayout(mFormLayout);         //Formulario de coordenadas
   vlayout->addLayout(hlayout);         //Formulario de búsqueda
   //vlayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Expanding));

   //Ascensión recta
   QHBoxLayout* l1 = new QHBoxLayout();
   mFocusWidget = mRALine = new QLineEdit(this);
   mRALine->setAlignment(Qt::AlignRight);
   if (!IS_INVALIDF(def_ra)) mRALine->setText(Util::RA2String(def_ra));
   mRaImage = new QLabel();
   mRaImage->setPixmap(QPixmap(":/graph/images/icn_cross.svg"));
   mRaImage->setBaseSize(QSize(16,16));
   l1->addWidget(mRALine);  l1->addWidget(mRaImage);
   mFormLayout->addRow("RA  (HH MM SS)", l1);
   connect(mRALine, SIGNAL(textChanged(QString)), this, SLOT(UpdateRaDecStatus()));

   //Declinación
   QHBoxLayout* l2 = new QHBoxLayout();
   mDECLine = new QLineEdit(this);
   mDECLine->setAlignment(Qt::AlignRight);
   if (!IS_INVALIDF(def_dec)) mDECLine->setText(Util::DEC2String(def_dec));
   mDecImage = new QLabel();
   mDecImage->setBaseSize(QSize(16,16));
   mDecImage->setPixmap(QPixmap(":/graph/images/icn_cross.svg"));
   l2->addWidget(mDECLine);  l2->addWidget(mDecImage);
   mFormLayout->addRow("DEC (DD MM SS)", l2);
   connect(mDECLine, SIGNAL(textChanged(QString)), this, SLOT(UpdateRaDecStatus()));

   //Opciones de búsqueda
   mAnimatedLabel = new AnimatedLabel(this);
   mAnimatedLabel->setImage(":/graph/images/preloader_22x22.png");
   mAnimatedLabel->setSpriteCount(12);
   mAnimatedLabel->setBaseSize(SIZE_ANIMATEDLABEL,SIZE_ANIMATEDLABEL);
   mAnimatedLabel->stop();
   mAnimatedLabel->setVisible(false);

   //Nombre del objeto
   mObjNameLabel = new QLabel();
  // mObjNameLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
   //mObjNameLabel->setAlignment(Qt::AlignRight | Qt::AlignBaseline);
   mObjNameLabel->setTextFormat(Qt::RichText);

   //Cuadro de edición de objeto
   mSearchEdit = new QLineEdit(this);
   mSearchEdit->setPlaceholderText("Object name");
   mSearchEdit->setText(objname);
   mSearchModified = false;

   mSearchBtn = new QToolButton();
   mSearchBtn->setIcon(QIcon(":/graph/images/search-16.png"));
   mSearchBtn->setBaseSize(26,26);
   mSearchBtn->setAutoRaise(true);
   connect(mSearchBtn, SIGNAL(clicked()), this, SLOT(RequestSearch_Slot()));
   connect(mSearchEdit, SIGNAL(returnPressed()), this, SLOT(RequestSearch_Slot()));
   connect(mSearchEdit, &QLineEdit::textChanged, this, [=]{ mSearchModified = true; });

   ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(false);
   ui->buttonBox->button(QDialogButtonBox::Ok)->setAutoDefault(false);
   ui->buttonBox->button(QDialogButtonBox::Cancel)->setDefault(false);
   ui->buttonBox->button(QDialogButtonBox::Cancel)->setAutoDefault(false);

   hlayout->addWidget(mAnimatedLabel);
   hlayout->addWidget(mObjNameLabel);
   hlayout->addWidget(mSearchEdit);
   hlayout->addWidget(mSearchBtn);


   //Conecto las señales de la búsqueda de RA DEC
   connect(&mSesameReader, SIGNAL(InitRead_Signal()), this,  SLOT(SesameStart_Slot()));
   connect(&mSesameReader, SIGNAL(EndRead_Signal(NameRecord&)), this,  SLOT(SesameEnd_Slot(NameRecord&)));

   //Redimensionamos el diálogo
   this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
   this->setBaseSize(BASE_INPUTDIALOG_WIDTH * XGlobal::SCREEN_DPI, 0);
   MainWindow::DPIAware(this, true);
   mDefRA = INVALID_FLOAT; mDefDEC = INVALID_FLOAT; UpdateRaDecStatus();

   //Mostramos el diálogo
   exec();
   if (result() == QDialog::Rejected) return QVector<double>() << def_ra << def_dec;

   mDefRA = Util::ParseRA( mRALine->text() );
   mDefDEC = Util::ParseDEC( mDECLine->text() );

   //Borrar todo el layout por si se va a volver a mostrar el inputbox
   delete vlayout;
   return vec << mDefRA << mDefDEC;
}

void InputDialog::showEvent(QShowEvent* ev)
{
    if (mFocusWidget) mFocusWidget->setFocus();
    QDialog::showEvent(ev);
}

void InputDialog::accept()
{
    //En los diálogos de coordenadas, controlamos si estamos en medio de una búsqueda
    if (mSearchEdit && mSearchModified && (mSearchEdit->hasFocus() || !mSearchEdit->isEnabled())) return;
    //Establecer el resultado a OK y retornar
    QDialog::accept();
}

void InputDialog::reject()
{
    setResult(QDialog::Rejected);
    QDialog::reject();
}

QStringList InputDialog::ShowMultipleTexts(const QStringList& titles, const QStringList& values, const QStringList& inputMask)
{
    mFormLayout = new QFormLayout(ui->baseFrame); ui->baseFrame->setLayout(mFormLayout);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    QStringList result;

    //Creamos una lista con etiquetas de edición
    QList<QWidget*> list_ed;
    for (int i=0; i<titles.length(); i++)
    {
        QWidget* le;
        QLabel* label = new QLabel(titles.at(i) + ":");
        //Opciones de formato
        if (!inputMask.length() || !inputMask[i].startsWith("COMBO:"))
        {
            le = new QLineEdit(this);
            list_ed.append(le);

            if (values.at(i).contains("[UPPERCASE]"))
            {
                QFont font = list_ed.at(i)->font();
                font.setCapitalization(QFont::AllUppercase);
                list_ed.at(i)->setFont(font);
                QString txt = values[i];
                ((QLineEdit*)le)->setText(txt.replace("[UPPERCASE]", ""));
            }
            else
            {
                ((QLineEdit*)le)->setText(values.at(i));
            }
            //Máscara de entrada si la tuviera
            if (inputMask.length() > i) ((QLineEdit*)le)->setInputMask(inputMask.at(i));
        }
        else
        {
            //Combobox
            le = new QComboBox(this); bool ocultation = false;
            list_ed.append(le);
            QStringList lst_combo = inputMask[i].split(":", QString::SkipEmptyParts);
            if (lst_combo.length() > 0 && lst_combo[1].startsWith("/"))
            {
                ocultation = true;
                lst_combo[1] = lst_combo[1].mid(1);
            }
            QStringList lst = lst_combo.at(lst_combo.count()-1).split(";", QString::SkipEmptyParts);
            if (lst_combo.count() == 3)
            {
                for (int j=0; j<list_ed.count()-1; j++)
                {
                    if (IS(list_ed[j], QComboBox))
                    {
                        connect((QComboBox*)list_ed[j],
                                static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [=]{
                            ActivateDeactivateEdit_Slot(i, le, label, list_ed[j], lst_combo[1], (QList<QWidget*>*)&list_ed, ocultation);
                        });
                    }
                    else if (IS(list_ed[j], QLineEdit))
                    {
                        connect((QLineEdit*)list_ed[j],
                                &QLineEdit::textChanged, this, [=]{
                            ActivateDeactivateEdit_Slot(i, le, label, list_ed[j], lst_combo[1], (QList<QWidget*>*)&list_ed, ocultation);
                        });
                    }
                    ActivateDeactivateEdit_Slot(i, le, label, list_ed[j], lst_combo[1], (QList<QWidget*>*)&list_ed, ocultation);
                }
            }
            for (int j=0; j<lst.count(); j++)
            {
                QString str = lst[j].trimmed();
                if (!str.length()) continue;
                ((QComboBox*)le)->addItem(str);
            }
            if (values.length() > i) ((QComboBox*)le)->setCurrentText(values[i]);
        }
        mFormLayout->addRow(label, le);
    }
    adjustSize();
    setBaseSize(BASE_INPUTDIALOG_WIDTH * XGlobal::SCREEN_DPI, height() / XGlobal::SCREEN_DPI);

    //Revisar este dpiaware
    MainWindow::DPIAware(this, false);

    for (int i=0; i<list_ed.length(); i++)
    {
        if (IS(list_ed.at(i), QLineEdit))
        {
            ((QLineEdit*)list_ed.at(i))->setFocus();
            break;
        }
    }
    exec();
    if (this->result() == QDialog::Rejected) return result;

    for (int i=0; i<list_ed.length(); i++)
    {
        if (IS(list_ed.at(i), QLineEdit))
        result.append(((QLineEdit*)list_ed.at(i))->text());
        else
        result.append(((QComboBox*)list_ed.at(i))->currentText());
    }

    //Borrar todo el layout por si se va a volver a mostrar el inputbox
    delete mFormLayout;

    return result;
}



void InputDialog::UpdateRaDecStatus()
{
    double ra = Util::ParseRA(mRALine->text());
    double dec = Util::ParseDEC(mDECLine->text());

    //Ocurre cuando pegamos un texto largo que contiene a la vez ra y dec
    if (IS_INVALIDF(ra) || IS_INVALIDF(dec))
    {
        QVector<double> radd = Util::ParseRADEC(mRALine->text());
        if (IS_INVALIDF(radd[0]) || IS_INVALIDF(radd[1]))
            radd = Util::ParseRADEC(mDECLine->text());
        if (!IS_INVALIDF(radd[0]) && !IS_INVALIDF(radd[1]))
        {
            QString ra_str = Util::RA2String(radd[0]);
            QString dec_str = Util::DEC2String(radd[1]);
            mRALine->setText(ra_str);
            mDECLine->setText(dec_str);
            ra = radd[0];
            dec = radd[1];
        }
    }

    if (IS_INVALIDF(ra) && IS_VALIDF(mDefRA))
         mRaImage->setPixmap(QPixmap(":/graph/images/icn_cross.svg"));
    else if (IS_VALIDF(ra) && IS_INVALIDF(mDefRA))
        mRaImage->setPixmap(QPixmap(":/graph/images/tick-16.png"));

    if (IS_INVALIDF(dec) && IS_VALIDF(mDefDEC))
        mDecImage->setPixmap(QPixmap(":/graph/images/icn_cross.svg"));
    else if (IS_VALIDF(dec) && IS_INVALIDF(mDefDEC))
        mDecImage->setPixmap(QPixmap(":/graph/images/tick-16.png"));

    mDefRA = ra;
    mDefDEC = dec;

    //Comprobar si es visible el objeto seleccionado
    if (mObjNameLabel->isVisible())
    {
        if (Util::RA2String(mNameRecord.dec()) != mRALine->text().trimmed()
            ||
            Util::DEC2String(mNameRecord.dec()) != mDECLine->text().trimmed())
         mObjNameLabel->setVisible(false);
         mObjNameLabel->setText("");
         mNameRecord.setNull();
    }
    //Habilitamos el botón de OK solamente si todo está bien
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(IS_VALIDF(mDefRA)&&IS_VALIDF(mDefDEC));
}


void InputDialog::RequestSearch_Slot()
{
    if (mSearchEdit->text().length() < 1) return;
    mSesameReader.resolveNameAsync(mSearchEdit->text());
}

void InputDialog::SesameStart_Slot()
{
    mSearchEdit->setEnabled(false);
    mSearchBtn->setEnabled(false);
    mAnimatedLabel->setVisible(true);
    mAnimatedLabel->start();
}

void InputDialog::SesameEnd_Slot(NameRecord& nr)
{
     mSearchEdit->setEnabled(true);
     mSearchBtn->setEnabled(true);
     mAnimatedLabel->stop();
     mAnimatedLabel->setVisible(false);
     mSearchBtn->setEnabled(true);

     if (!nr.isnull())
     {
         mRALine->setText( Util::RA2String(nr.ra()) );
         mDECLine->setText( Util::DEC2String(nr.dec()) );
         mAnimatedLabel->hide();
         mObjNameLabel->setVisible(true);
         mObjNameLabel->setText(QString("<b>%1</b>").arg(nr.name().isNull()?mSearchEdit->text():nr.name()));
         mObjNameLabel->setFocus();
         mObjNameLabel->setSelection(0,mObjNameLabel->text().length());
     }
     else
     {
         mObjNameLabel->setVisible(false);
         mAnimatedLabel->setPixmap(QPixmap(":/graph/images/icn_cross.svg"));
         mAnimatedLabel->setVisible(true);
     }
     mNameRecord = nr;
     mSearchEdit->setFocus();
     mSearchEdit->selectAll();
     mSearchModified = false;
}

void InputDialog::ActivateDeactivateEdit_Slot(int/*rowindex*/, QWidget* base,QLabel* label, QWidget* trigger, QString behaivour, QList<QWidget*>* list, bool occult)
{
    bool ok;
    QStringList bh = behaivour.split(",", QString::SkipEmptyParts);
    int idx_trigger = bh[0].toInt(&ok);
    if (!ok || !list || list->count() <= idx_trigger) return;
    if (list->at(idx_trigger) != trigger) return;
    QString val;
    if (IS(trigger, QComboBox)) val = ((QComboBox*)trigger)->currentText();
    else if (IS(trigger, QLineEdit)) val = ((QLineEdit*)trigger)->text();

    bool activated = false;
    for (int i=1; i<bh.length() && !activated; i++)
    {
        if (val == bh[i])
            activated = true;
    }
    occult ? base->setVisible(activated) : base->setEnabled(activated);
    occult ? label->setVisible(activated) : label->setEnabled(activated);

    if (occult)
    {
        if (!base->isVisible())
        {
            base->setParent(this);
            label->setParent(this);
            mFormLayout->removeWidget(base);
            mFormLayout->removeWidget(label);
        }
        else
            mFormLayout->insertRow(mFormLayout->rowCount(), label, base);
    }
    //Quizá tenga que aplicar algún comportamiento de imagen
    if (mImageBehaivour.count())
    {
        int idx_trigger = (mFormLayout->indexOf(trigger)-1)/2;
        for (int i=0; i<mImageBehaivour.count(); i++)
        {
            QStringList list = mImageBehaivour.at(i).split(";");
            int idx = list[0].toInt();
            if (idx_trigger != idx || list[1] != val) continue;
            SetImage(list[2]);
            break;
        }
    }
}













