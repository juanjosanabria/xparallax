#include <QThread>

#include "astrometrydialog.h"
#include "ui_astrometrydialog.h"
#include "inputdialog.h"
#include "ui_helpers/progressdialog.h"
#include "xglobal.h"
#include "astrometry.h"
#include "mainwindow.h"
#include "overlay/xoverlay.h"

const char* AstrometryDialog::ASTROMETRY_DIALOG_INI = "astrometry_dialog.ini";

AstrometryDialog::AstrometryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AstrometryDialog)
{
    ui->setupUi(this);

   //Carbamos los parámetros desde disco
   mSettings = XGlobal::GetSettings(ASTROMETRY_DIALOG_INI);
   mMatchP.Load(mSettings, "MATCHER");
   mDetectP.Load(mSettings, "DETECTION");
   mInputFiles.Load(mSettings, "FILES");

   ui->widgetAstrometry->SetParams(&mMatchP);
   ui->widgetPixelSize->SetParams(&mMatchP);
   ui->widgetStars->SetParameters(&mDetectP);
   ui->widgetBackground->SetParameters(&mDetectP.p_bkg);

   ui->widgetSourceFiles->AddOption(SelectFilesLayout::SFO_OVERWRITE_CHECKED);
   mAstrometryComputer = NULL;
   ui->widgetSourceFiles->FN_GET_ITEM_IMAGE = FN_GET_ITEM_IMAGE;

   connect(ui->widgetSourceFiles, SIGNAL(RADECDetected_Signal(double,double)), ui->widgetPixelSize, SLOT(RADECDetected_Slot(double,double)) );
   ui->widgetSourceFiles->LookforImageInformation();
   MainWindow::DPIAware(this, true);
}

AstrometryDialog::~AstrometryDialog()
{
    SaveParameters();
    if (mAstrometryComputer) delete mAstrometryComputer;
    delete ui;
    if (mSettings) delete mSettings;
    mSettings = NULL;
}

void AstrometryDialog::SaveParameters()
{
    mMatchP.Save(mSettings, "MATCHER");
    mDetectP.Save(mSettings, "DETECTION");
    mInputFiles.Save(mSettings, "FILES");
}


void AstrometryDialog::accept()
{
    MdiFitImage* fi = MainWindow::mainwin->ActiveFitImage();
    if (fi) INFO("Selected image: %s",PTQ(fi->filename()));

    //Obtener todos los parámetros
    if (!ui->widgetSourceFiles->CheckData(&mInputFiles)) return;
    if (!ui->widgetAstrometry->CheckData(&mMatchP)) return;
    if (!ui->widgetPixelSize->CheckData(&mMatchP)) return;
    if (!ui->widgetStars->CheckData(&mDetectP)) return;
    if (!ui->widgetBackground->CheckData(&mDetectP.p_bkg)) return;
    SaveParameters();

    //Después de las comprobaciones podemos comenzar a calcular la astrometría
    if (mAstrometryComputer) delete mAstrometryComputer;
    mAstrometryComputer = new AstrometryComputer();
    XGlobal::APP_SETTINGS.ct_ast++;
    //Ocultamos el diálogo para mostrar el de progreso
    this->hide();
    ProgressDialog prg(this,"Astrometric reduction in progress");
    prg.SetIcon(":/graph/images/rule.png");
    prg.Connect(mAstrometryComputer);
    prg.ExecInBackground(Background_Atrometry, this);

    if (!mInputFiles.fitimages()->count()) return;

    if (mAstrometryComputer->cancelled())
    {
        this->close();
        return;
    }

    //Si la astrometría ha fallado mostramos un mensaje
    if (mAstrometryComputer->CountKO() && mInputFiles.fitimages()->count() == mAstrometryComputer->CountKO() )
    {
        QMessageBox::warning(this,"Astrometry failed",
                             "No succesfull match found. Please, check input parameters");
    }
    else if (mAstrometryComputer->CountKO())
    {
        QMessageBox::warning(this,"Astrometry failed",
                             "Some files were not reduced due an unsuccesfull match. Please check input parameters and retry.");
    }

    //Si todo ha ido bien podemos cerrar el diálogo, pero mientras tanto no
    else if (!mAstrometryComputer->cancelled() && mInputFiles.fitimages()->count() == 1
             && fi && fi->LookForPlateInFit())
    {
        fi->CheckDirty();
        fi->SetMatch(mAstrometryComputer->matcher(), fi->plate());
    }

    this->close();
}



int AstrometryDialog::Background_Atrometry(void *data)
{
   AstrometryDialog* dlg = (AstrometryDialog*)data;
   bool result = dlg->mAstrometryComputer->doWork(&(dlg->mInputFiles), &(dlg->mDetectP), &(dlg->mMatchP));
   return result?0:1;
}



QString  AstrometryDialog::FN_GET_ITEM_IMAGE(FitHeaderList* fh,const QString& /*filename*/)
{
    PlateConstants *pc = fh->LookForPlateConstants();
    if (pc)
    {
        delete pc;
        return ":/graph/images/rule.png";
    }
    if ((pc = fh->LookForPlateRATAN()))
    {
        delete pc;
        return ":/graph/images/rule.png";
    }
    return "";
}























