#ifndef EXPERIMENTAL_MN_H
#define EXPERIMENTAL_MN_H

#include <QObject>
#include <QMenu>
#include <QAction>
#include <QSettings>
#include "matcher.h"
#include "fitimage.h"
#include "mdifitimage.h"

namespace Ui {
    class Experimental;
}

#define     EXP_INI_FILE        "experimental.ini"
#define     FORCE_ALIGN_SECTION "FORCE_ALIGN"

class Experimental : public QObject
{
    Q_OBJECT

public:
    explicit Experimental(QObject *parent = 0);

    void CreateExperimentalMenu();
    inline QMenu* getExpMenu(){return mExperimentalMenu;}
    inline bool shown(){return mShowExperimental;}
    inline void setShown(bool shown){mShowExperimental = shown;}

signals:

public slots:
    void StarDetector_Slot();   //Detecta estrellas en la imagen actual
    void ForceAlign_Slot();     //Fuerza una alineación
    void Tool_AngularDistance_Slot();
    void Tool_CoordTransform_Slot();

private:
    static int StarDetector_Thread(void *);

    //Miembros privados
    QSettings*             mSettings;
    QAction*               mSearchStarsAction;
    QAction*               mForceAlignAction;
    QAction*               mAngularDistance;
    QAction*               mEpochConverter;

    //Miembros estáticos
    static bool                 mShowExperimental;
    static QMenu*               mExperimentalMenu;
    static StarDetector *       mStarDetector;
    static DetectedStar*        mDetectedStars;
    static int                  mDetectedStarCount;
    static FitImage*            mImage;
    static MdiFitImage*         mMdiFitImage;

    static MatchParameters*     mMatchParameters;
    static DetectionParameters* mDetectionParameters;
    static WorkFileList*        mInputFiles;
    static PlateConstants*      mPlateConstants;

};

#endif // EXPERIMENTAL_MN_H
