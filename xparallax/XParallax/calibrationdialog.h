#ifndef CALIBRATIONDIALOG_H
#define CALIBRATIONDIALOG_H

#include <QDialog>
#include <QStringList>
#include "calibration.h"

namespace Ui {
class CalibrationDialog;
}

class CalibrationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CalibrationDialog(QWidget *parent = 0);
    ~CalibrationDialog();

protected:
    virtual void accept();
    bool CheckAdvancedData(CalibrationParameters *par);
    void SetAdvancedData(CalibrationParameters *par);
    static int Background_Calibration(void *data);


private:
    Ui::CalibrationDialog *ui;
    CalibrationList        mCalibrationFiles;
    CalibrationParameters  mParmeters;
    WorkFileList           mInputFiles;
    CalibrationComputer*   mComputer;

    static const char* CALIBRATION_DIALOG_INI;
    static  QString  FN_GET_ITEM_IMAGE(FitHeaderList* fh,const QString& filename);
    static  QString  FN_GET_ITEM_TOOLTIP(FitHeaderList* fh,const QString& filename);
    static  int      FN_ITEM_SORT(const QString& a,const QString& b);


public slots:
    void SetDefaults_Slot();
};

#endif // CALIBRATIONDIALOG_H
