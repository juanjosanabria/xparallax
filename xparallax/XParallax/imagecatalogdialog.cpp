#include "imagecatalogdialog.h"
#include "ui_imagecatalogdialog.h"
#include "mainwindow.h"
#include "inputdialog.h"
#include "ui_helpers/ProgressDialog.h"

const char* ImageCatalogDialog::IMAGE_CATALOG_INI = "imgcat_dialog.ini";

ImageCatalogDialog::ImageCatalogDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImageCatalogDialog)
{
    ui->setupUi(this);
    mComputer = new CatalogComputer();
    mSettings =  XGlobal::GetSettings(IMAGE_CATALOG_INI);

    ui->widgetSourceFiles->SetOptions((SelectFilesLayout::SelectFilesOptions)
                                      (
                                          (
                                              ui->widgetSourceFiles->options()
                                              & (~ SelectFilesLayout::SFO_OUTPUT_FOLDER_MANDATORY)
                                              & (~ SelectFilesLayout::SFO_ALLOW_OVERWRITE)
                                          )
                                          | SelectFilesLayout::SFO_OUTPUT_FOLDER_MANDATORY
                                        )
                                      );
    //Campos de salida del catálogo, voy a usar bastantes expresiones lambda
    connect(ui->treeCatFields, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), SLOT(SelectCatField_Slot()));
    connect(ui->buttonUpCatField, &QToolButton::clicked, this, [=]{ FieldUp_Slot(ui->treeCatFields); });
    connect(ui->buttonDownCatField, &QToolButton::clicked, this, [=]{ FieldDown_Slot(ui->treeCatFields); });

    //Campos de salida de la tabla de flujo
    connect(ui->treeFluxFields, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), SLOT(SelectFluxField_Slot()));
    connect(ui->buttonUpFluxField, &QToolButton::clicked, this, [=]{ FieldUp_Slot(ui->treeFluxFields); });
    connect(ui->buttonDownFluxField, &QToolButton::clicked, this, [=]{ FieldDown_Slot(ui->treeFluxFields); });

    //Estrellas de salida con sus posiciones
    connect(ui->treeSelectedSources, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), SLOT(SelectSourceStar_Slot()));
    connect(ui->buttonAddSource, &QToolButton::clicked, this, [=]{AddSourceStar_Slot(true);});
    connect(ui->treeSelectedSources, &QTreeWidget::doubleClicked, [=]{ AddSourceStar_Slot(false); });
    connect(ui->buttonRemoveSource, SIGNAL(clicked()), this, SLOT(RemoveSourceStar_Slot()));
    connect(ui->buttonClearSources, SIGNAL(clicked()), this, SLOT(ClearSourceStar_Slot()));
    connect(ui->buttonUpSource, &QToolButton::clicked, this, [=]{ FieldUp_Slot(ui->treeSelectedSources); });
    connect(ui->buttonDownSource, &QToolButton::clicked, this, [=]{ FieldDown_Slot(ui->treeSelectedSources); });

    //Cambian los checkboxes de la información de salida
    connect(ui->checkOutputCatalog, &QCheckBox::stateChanged, this, [=]{ CheckBoxChange_Slot(ui->checkOutputCatalog); });
    connect(ui->checkOutputFluxTable, &QCheckBox::stateChanged, this, [=]{ CheckBoxChange_Slot(ui->checkOutputFluxTable); });
    connect(ui->checkSelectSources, &QCheckBox::stateChanged, this, [=]{ CheckBoxChange_Slot(ui->checkSelectSources); });
    connect(ui->checkSortCat, &QCheckBox::stateChanged, this, [=]{ CheckBoxChange_Slot(ui->checkSortCat); });
    connect(ui->checkSortFlux, &QCheckBox::stateChanged, this, [=]{ CheckBoxChange_Slot(ui->checkSortFlux); });
    CheckBoxChange_Slot(NULL);

    //Generales del diálogo
    connect(ui->buttonResetCat, SIGNAL(clicked()), this, SLOT(ResetCatalog_Slot()));
    connect(ui->buttonBox->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), this, SLOT(reject()));
    connect(ui->buttonBox->button(QDialogButtonBox::Ok), SIGNAL(clicked()), this, SLOT(accept()));

    //Acciones de seleccionar los archivos
    connect(ui->widgetSourceFiles->radioCurrentFile(), &QRadioButton::toggled, this, [=]{ RaioInputFilesChanged_Slot(ui->widgetSourceFiles->radioCurrentFile()); });
    connect(ui->widgetSourceFiles->radioWorkspace(), &QRadioButton::toggled, this, [=]{ RaioInputFilesChanged_Slot(ui->widgetSourceFiles->radioWorkspace()); });
    connect(ui->widgetSourceFiles->radioDiskFiles(), &QRadioButton::toggled, this, [=]{ RaioInputFilesChanged_Slot(ui->widgetSourceFiles->radioDiskFiles()); });
    RaioInputFilesChanged_Slot(ui->widgetSourceFiles->radioCurrentFile());
    RaioInputFilesChanged_Slot(ui->widgetSourceFiles->radioWorkspace());
    RaioInputFilesChanged_Slot(ui->widgetSourceFiles->radioDiskFiles());

    ui->widgetSourceFiles->FN_GET_ITEM_IMAGE = FN_GET_ITEM_IMAGE;
    mInputFiles.Load(mSettings, "FILES");
    mDetectP.Load(mSettings, "DETECTION");
    mCatalogP.Load(mSettings, "CATALOG");
    LoadDataToDialog();

    MainWindow::DPIAware(this, true);
}

ImageCatalogDialog::~ImageCatalogDialog()
{
    delete ui;
    if (mSettings) delete mSettings;
    mSettings = NULL;
    if (mComputer) delete mComputer;
    mComputer = NULL;
}

void ImageCatalogDialog::CheckBoxChange_Slot(QCheckBox* check)
{
    if (check == ui->checkOutputCatalog || !check)
    {
        ui->labelCatalogFmt->setEnabled(ui->checkOutputCatalog->isChecked());
        ui->comboCatlogFmt->setEnabled(ui->checkOutputCatalog->isChecked());
        ui->treeCatFields->setEnabled(ui->checkOutputCatalog->isChecked());
        SelectCatField_Slot();
    }
    if (check == ui->checkOutputFluxTable || !check)
    {
        ui->labelFluxFmt->setEnabled(ui->checkOutputFluxTable->isChecked());
        ui->comboFluxFmt->setEnabled(ui->checkOutputFluxTable->isChecked());
        ui->labelFluxType->setEnabled(ui->checkOutputFluxTable->isChecked());
        ui->comboFluxType->setEnabled(ui->checkOutputFluxTable->isChecked());
        ui->treeFluxFields->setEnabled(ui->checkOutputFluxTable->isChecked());
        SelectFluxField_Slot();
    }
    if (check == ui->checkSelectSources || !check)
    {
        ui->treeSelectedSources->setEnabled(ui->checkSelectSources->isChecked());
        ui->buttonAddSource->setEnabled(ui->checkSelectSources->isChecked());
        ui->buttonClearSources->setEnabled(ui->checkSelectSources->isChecked() && ui->treeSelectedSources->topLevelItemCount());
        ui->buttonRemoveSource->setEnabled(ui->checkSelectSources->isChecked() &&  (ui->treeSelectedSources->currentIndex().row() > -1));
        ui->labelSeparation->setEnabled(ui->checkSelectSources->isChecked());
        ui->spinSeparation->setEnabled(ui->checkSelectSources->isChecked());
        ui->labelSeparationScale->setEnabled(ui->checkSelectSources->isChecked());
        SelectSourceStar_Slot();
    }
    if (check == ui->checkSortCat || !check)
    {
        ui->labelSortBy->setEnabled(ui->checkSortCat->isChecked());
        ui->comboSortCatBy->setEnabled(ui->checkSortCat->isChecked());
        ui->labelSortOrder->setEnabled(ui->checkSortCat->isChecked());
        ui->comboSortCatOrder->setEnabled(ui->checkSortCat->isChecked());
    }
    if (check == ui->checkSortFlux || !check)
    {
        ui->labelFluxSortBy->setEnabled(ui->checkSortFlux->isChecked());
        ui->comboFluxSortBy->setEnabled(ui->checkSortFlux->isChecked());
        ui->labelFluxSortOrder->setEnabled(ui->checkSortFlux->isChecked());
        ui->comboFluxSortOrder->setEnabled(ui->checkSortFlux->isChecked());
    }
}

void ImageCatalogDialog::SelectSourceStar_Slot()
{
    ui->buttonRemoveSource->setEnabled((ui->treeSelectedSources->currentIndex().row() > -1));
    ui->buttonUpSource->setEnabled(ui->checkSelectSources->isChecked() ?
                                   (ui->treeSelectedSources->currentIndex().row() > 0)
                                     : false);
    ui->buttonDownSource->setEnabled( ui->checkSelectSources->isChecked()
                                ? (ui->treeSelectedSources->currentIndex().row() > -1 &&
                                   ui->treeSelectedSources->currentIndex().row() < ui->treeSelectedSources->topLevelItemCount()-1) : false);

}

void ImageCatalogDialog::SelectCatField_Slot()
{
    ui->buttonUpCatField->setEnabled(ui->checkOutputCatalog->isChecked()
                             ? (ui->treeCatFields->currentIndex().row() > 0) : false);
    ui->buttonDownCatField->setEnabled( ui->checkOutputCatalog->isChecked()
                                ? (ui->treeCatFields->currentIndex().row() > -1 &&
                                   ui->treeCatFields->currentIndex().row() < ui->treeCatFields->topLevelItemCount()-1) : false);
}


void ImageCatalogDialog::SelectFluxField_Slot()
{
    ui->buttonUpFluxField->setEnabled(ui->checkOutputFluxTable->isChecked()
                             ? (ui->treeFluxFields->currentIndex().row() > 0) : false);
    ui->buttonDownFluxField->setEnabled( ui->checkOutputFluxTable->isChecked()
                                ? (ui->treeFluxFields->currentIndex().row() > -1 &&
                                   ui->treeFluxFields->currentIndex().row() < ui->treeFluxFields->topLevelItemCount()-1) : false);
}


void ImageCatalogDialog::FieldUp_Slot(QTreeWidget *tree)
{
    int index = tree->currentIndex().row();
    if (index <= 0) return;
    QTreeWidgetItem* item = tree->topLevelItem(index);
    QTreeWidgetItem* item2 = new QTreeWidgetItem();
    for (int i=0; i<item->columnCount(); i++)
        item2->setText(i, item->text(i));
    if (tree == ui->treeCatFields || tree == ui->treeFluxFields) item2->setCheckState(0,item->checkState(0));
    delete item;
    tree->insertTopLevelItem(index - 1, item2);
    tree->setCurrentItem(item2);
}

void ImageCatalogDialog::FieldDown_Slot(QTreeWidget *tree)
{
    int index = tree->currentIndex().row();
    if (index < 0) return;
    QTreeWidgetItem* item = tree->topLevelItem(index);
    QTreeWidgetItem* item2 = new QTreeWidgetItem();
    for (int i=0; i<item->columnCount(); i++)
        item2->setText(i, item->text(i));
    if (tree == ui->treeCatFields || tree == ui->treeFluxFields) item2->setCheckState(0,item->checkState(0));
    delete item;
    tree->insertTopLevelItem(index + 1, item2);
    tree->setCurrentItem(item2);
}

void ImageCatalogDialog::AddSourceStar_Slot(bool add /*true=nueva, false=editar*/)
{
    double ra = INVALID_FLOAT, dec = INVALID_FLOAT;
    int idx = add ? -1 : ui->treeSelectedSources->currentIndex().row();
    if (idx > -1)
    {
        ra = Util::ParseRA(ui->treeSelectedSources->topLevelItem(idx)->text(1));
        dec = Util::ParseDEC(ui->treeSelectedSources->topLevelItem(idx)->text(2));
    }
    InputDialog id;
    id.SetTitle("Input coordinates");
    id.SetSubTitle("Select the star coordinates you are going to look for");
    QVector<double> radec = id.ShowRADec(ra, dec, add ? "" : ui->treeSelectedSources->topLevelItem(idx)->text(0).trimmed());
    if ((radec.length() != 2) || IS_INVALIDF(radec[0]) || IS_INVALIDF(radec[1])) return;
    QTreeWidgetItem* item = add ? new QTreeWidgetItem(ui->treeSelectedSources) : ui->treeSelectedSources->topLevelItem(idx);
    item->setText(0, id.objectName() + "   ");
    item->setText(1, Util::RA2String(radec[0]) + "   ");
    item->setText(2, Util::DEC2String(radec[1]));
    ui->treeSelectedSources->resizeColumnToContents(0);
    ui->treeSelectedSources->resizeColumnToContents(1);
    ui->buttonClearSources->setEnabled(ui->checkSelectSources->isChecked() && ui->treeSelectedSources->topLevelItemCount());
}

void ImageCatalogDialog::ResetCatalog_Slot()
{
    mCatalogP.SetDefaults();
    LoadDataToDialog();
}

void ImageCatalogDialog::ResetFluxTable_Slot()
{

}

void ImageCatalogDialog::RemoveSourceStar_Slot()
{
    int row = ui->treeSelectedSources->currentIndex().row();
    if (row < 0) return;
    delete  ui->treeSelectedSources->topLevelItem(row);
    ui->buttonClearSources->setEnabled(ui->checkSelectSources->isChecked() && ui->treeSelectedSources->topLevelItemCount());
}

void ImageCatalogDialog::ClearSourceStar_Slot()
{
    ui->treeSelectedSources->clear();
    ui->buttonClearSources->setEnabled(ui->checkSelectSources->isChecked() && ui->treeSelectedSources->topLevelItemCount());
}

void ImageCatalogDialog::LoadDataToDialog()
{
    //Selector de archivos
    //------------------------------------------------------------------------------------------------------------------
    ui->widgetSourceFiles->SetLastDirs(mInputFiles.lastSearchPath(), mInputFiles.outputDir());

    //Catálogo
    //------------------------------------------------------------------------------------------------------------------
    ui->treeCatFields->clear();
    QStringList fields;
    for (int i=0; CatalogParameters::AVAILABLE_CAT_FIELDS[i]; i+=2) fields << CatalogParameters::AVAILABLE_CAT_FIELDS[i];
    mCatalogP.SortFields(&fields, CatalogParameters::AVAILABLE_CAT_FIELDS);
    for (int i=0; i<fields.length(); i++)
    {
        QTreeWidgetItem* item = new QTreeWidgetItem(ui->treeCatFields);
        item->setText(0, fields[i] + "   ");
        item->setText(1, CatalogParameters::getDescCatField(fields[i]));
        item->setCheckState(0, Qt::Checked);
        item->setCheckState(0, mCatalogP.cat_fields.contains(fields[i]) ? Qt::Checked : Qt::Unchecked);
    }
    ui->treeCatFields->resizeColumnToContents(0);


    //Rellenar los comobos de formatos de salida
    ui->comboCatlogFmt->clear();
    ui->comboFluxFmt->clear();
    for (int i=0; CatalogParameters::OUTPUT_FORMATS[i]; i+=2)
    {
        ui->comboCatlogFmt->addItem(CatalogParameters::OUTPUT_FORMATS[i+1]);
        ui->comboFluxFmt->addItem(CatalogParameters::OUTPUT_FORMATS[i+1]);
    }

    //Formato de salida del catálogo
    for (int i=0; i<ui->comboCatlogFmt->count(); i++)
    if (CatalogParameters::getFormatFromDesc(ui->comboCatlogFmt->itemText(i)) == mCatalogP.cat_fmt)
    {
        ui->comboCatlogFmt->setCurrentIndex(i);
        break;
    }

    //Tabla de flujo
    //------------------------------------------------------------------------------------------------------------------
    ui->treeFluxFields->clear();
    fields.clear();
    for (int i=0; CatalogParameters::AVAILABLE_FLUX_FIELDS[i]; i+=2) fields << CatalogParameters::AVAILABLE_FLUX_FIELDS[i];
    mCatalogP.SortFields(&fields, CatalogParameters::AVAILABLE_FLUX_FIELDS);
    for (int i=0; i<fields.length(); i++)
    {
        QTreeWidgetItem* item = new QTreeWidgetItem(ui->treeFluxFields);
        item->setText(0, fields[i] + "   ");
        item->setText(1, CatalogParameters::getDescFluxField(fields[i]));
        item->setCheckState(0, Qt::Checked);
        item->setCheckState(0, mCatalogP.flux_fields.contains(fields[i]) ? Qt::Checked : Qt::Unchecked);
    }
    ui->treeFluxFields->resizeColumnToContents(0);

    //Tabla de flujo
    ui->checkOutputFluxTable->setChecked(mCatalogP.flux_output);
    for (int i=0; i<ui->comboFluxType->count(); i++)
    if (ui->comboFluxType->itemText(i) == mCatalogP.flux_type)
    {
        ui->comboFluxType->setCurrentIndex(i);
        break;
    }
    for (int i=0; i<ui->comboFluxFmt->count(); i++)
    if (CatalogParameters::getFormatFromDesc(ui->comboFluxFmt->itemText(i)) == mCatalogP.flux_fmt)
    {
        ui->comboFluxFmt->setCurrentIndex(i);
        break;
    }

    //Rellenar el treeview con las fuentes de salida
    ui->treeSelectedSources->clear();
    for (int i=0; i<mCatalogP.filt_sources.length(); i++)
    {
        QStringList cf = mCatalogP.filt_sources.at(i).split(',');
        if (cf.length() != 3) continue;
        double ra = Util::ParseRA(cf[1]);
        if (IS_INVALIDF(ra)) continue;
        double dec = Util::ParseDEC(cf[2]);
        if (IS_INVALIDF(dec)) continue;
        QTreeWidgetItem* item = new QTreeWidgetItem(ui->treeSelectedSources);
        item->setText(0, cf[0].trimmed() + "   ");
        item->setText(1, cf[1].trimmed() + "   ");
        item->setText(2, cf[2].trimmed());
    }
    ui->spinSeparation->setValue(mCatalogP.filt_separation);
    ui->checkSelectSources->setChecked(mCatalogP.filt_output);

    //Ordenación de campos del catálogo
    ui->checkSortCat->setChecked(mCatalogP.cat_sort);
    ui->comboSortCatBy->clear();
    for (int i=0; CatalogParameters::SORTABLE_FIELDS[i]; i++)
        ui->comboSortCatBy->addItem(CatalogParameters::SORTABLE_FIELDS[i]);
    for (int i=0; i<ui->comboSortCatBy->count(); i++)
    if (ui->comboSortCatBy->itemText(i) == mCatalogP.cat_sort_field)
    {
        ui->comboSortCatBy->setCurrentIndex(i);
        break;
    }
    ui->comboSortCatOrder->setCurrentIndex(mCatalogP.cat_sort_order ? 0 : 1);

    //Ordenación de los campos de la tabla de flujo
    ui->checkSortFlux->setChecked(mCatalogP.flux_sort);
    ui->comboFluxSortBy->clear();
    for (int i=0; CatalogParameters::SORTABLE_FIELDS[i]; i++)
        ui->comboFluxSortBy->addItem(CatalogParameters::SORTABLE_FIELDS[i]);
    for (int i=0; i<ui->comboFluxSortBy->count(); i++)
    if (ui->comboFluxSortBy->itemText(i) == mCatalogP.flux_sort_field)
    {
        ui->comboFluxSortBy->setCurrentIndex(i);
        break;
    }
    ui->comboFluxSortOrder->setCurrentIndex(mCatalogP.flux_sort_order ? 0 : 1);

    //Background y detección de fuentes
    ui->widgetBackground->SetParameters(&mDetectP.p_bkg);
    ui->widgetStars->SetParameters(&mDetectP);
}

bool ImageCatalogDialog::CheckData()
{
    //Catálogo de imagen
    //-------------------------------------------------------------------------------------------------------------
    mCatalogP.cat_output = ui->checkOutputCatalog->isChecked();
    mCatalogP.cat_fmt = CatalogParameters::getFormatFromDesc(ui->comboCatlogFmt->currentText().trimmed());
    mCatalogP.cat_fields.clear(); mCatalogP.cat_fields_order.clear();
    for (int i=0; i<ui->treeCatFields->topLevelItemCount(); i++)
    {
        mCatalogP.cat_fields_order << ui->treeCatFields->topLevelItem(i)->text(0).trimmed();
        if (ui->treeCatFields->topLevelItem(i)->checkState(0) == Qt::Checked)
            mCatalogP.cat_fields << ui->treeCatFields->topLevelItem(i)->text(0).trimmed();
    }
    mCatalogP.cat_sort = ui->checkSortCat->isChecked();
    mCatalogP.cat_sort_field = ui->comboSortCatBy->currentText().trimmed();
    mCatalogP.cat_sort_order = !ui->comboSortCatOrder->currentIndex();

    //Tabla de flujo
    //-------------------------------------------------------------------------------------------------------------
    mCatalogP.flux_output = ui->checkOutputFluxTable->isChecked();
    mCatalogP.flux_type = ui->comboFluxType->currentText().trimmed();
    mCatalogP.flux_fmt = CatalogParameters::getFormatFromDesc(ui->comboFluxFmt->currentText().trimmed());
    mCatalogP.flux_fields.clear(); mCatalogP.flux_fields_order.clear();
    for (int i=0; i<ui->treeFluxFields->topLevelItemCount(); i++)
    {
        mCatalogP.flux_fields_order << ui->treeFluxFields->topLevelItem(i)->text(0).trimmed();
        if (ui->treeCatFields->topLevelItem(i)->checkState(0) == Qt::Checked)
            mCatalogP.flux_fields << ui->treeFluxFields->topLevelItem(i)->text(0).trimmed();
    }
    mCatalogP.flux_sort = ui->checkSortFlux->isChecked();
    mCatalogP.flux_sort_field = ui->comboFluxSortBy->currentText().trimmed();
    mCatalogP.flux_sort_order = !ui->comboFluxSortOrder->currentIndex();

    //Filtrado de fuentes de entrada si lo hubiera
    //-------------------------------------------------------------------------------------------------------------
    mCatalogP.filt_output = ui->checkSelectSources->isChecked();
    mCatalogP.filt_separation = (float)ui->spinSeparation->value();
    mCatalogP.filt_sources.clear();
    for (int i=0; i<ui->treeSelectedSources->topLevelItemCount(); i++)
    {
        QString src = QString("%1,%2,%3")
                .arg(ui->treeSelectedSources->topLevelItem(i)->text(0).trimmed())
                .arg(ui->treeSelectedSources->topLevelItem(i)->text(1).trimmed())
                .arg(ui->treeSelectedSources->topLevelItem(i)->text(2).trimmed());
        mCatalogP.filt_sources << src;
    }
    if (mCatalogP.filt_output && !mCatalogP.filt_sources.count())
    {
        QMessageBox::warning(this, "Warning",
                                   "You have selected the option <b>Filter sources</b> but no"
                                   " sources to filter were selected.<br/><br/>"
                                   "Please, uncheck the 'Filter sources' box or insert one or more sources to filter in "
                                   " the 'Filter sources' tab.");
        return false;
    }

    //De otros widgets
    //-------------------------------------------------------------------------------------------------------------
    if (!ui->widgetSourceFiles->CheckData(&mInputFiles)) return false;
    if (!ui->widgetBackground->CheckData(&mDetectP.p_bkg)) return false;
    if (!ui->widgetStars->CheckData(&mDetectP)) return false;

    return true;
}

void ImageCatalogDialog::accept()
{
    //Comprobar parámetros de entrada
    if (!CheckData()) return;
    mInputFiles.Save(mSettings, "FILES");
    mDetectP.Save(mSettings, "DETECTION");
    mCatalogP.Save(mSettings, "CATALOG");
    this->close();

    ProgressDialog prg(this,"Creating image catalog");
    prg.SetIcon(":/graph/images/icn_stars.svg");
    prg.Connect(mComputer);
    prg.ExecInBackground(Background_CatalogCompute, this);

    if (mComputer->cancelled())
    {
        this->close();
        return;
    }

    //Si hubiera que añadir la detección a cada imagen
    if (mInputFiles.fitimages()->count() == 1 && MainWindow::mainwin->ActiveFitImage())
    {
       addDetectionToImage(MainWindow::mainwin->ActiveFitImage(), mComputer->detector());
    }

    this->close();
}

QString  ImageCatalogDialog::FN_GET_ITEM_IMAGE(FitHeaderList* fh,const QString& /*filename*/)
{
    PlateConstants *pc = fh->LookForPlateConstants();
    if (pc)
    {
        delete pc;
        return ":/graph/images/rule.png";
    }
    if ((pc = fh->LookForPlateRATAN()))
    {
        delete pc;
        return ":/graph/images/rule.png";
    }
    return "";
}


//Hilo de cálculo
int ImageCatalogDialog::Background_CatalogCompute(void *data)
{
   ImageCatalogDialog* dlg = (ImageCatalogDialog*)data;
   bool result = dlg->mComputer->doWork(&(dlg->mInputFiles),&(dlg->mDetectP),&(dlg->mCatalogP));
   return result?0:1;
}

void ImageCatalogDialog::RaioInputFilesChanged_Slot(QRadioButton* btn)
{
    if (!btn->isChecked()) return;
    if (btn == ui->widgetSourceFiles->radioCurrentFile())
    {
        ui->tabCatalog->setEnabled(false);
        ui->tabFilter->setEnabled(false);
        ui->tabFluxTable->setEnabled(false);
        ui->widgetSourceFiles->SetOptions((SelectFilesLayout::SelectFilesOptions)
                                          (
                                                  ui->widgetSourceFiles->options()
                                                  & (~ SelectFilesLayout::SFO_OUTPUT_FOLDER_MANDATORY)
                                                  & (~ SelectFilesLayout::SFO_REQUEST_ASTROMETRIC)
                                                  & (~ SelectFilesLayout::SFO_REQUEST_OUTPUT_DIR)
                                            )
                                          ,false);
    }
    else
    {
        ui->tabCatalog->setEnabled(true);
        ui->tabFilter->setEnabled((true));
        ui->tabFluxTable->setEnabled((true));
        ui->widgetSourceFiles->SetOptions((SelectFilesLayout::SelectFilesOptions)
                                          (
                                                  ui->widgetSourceFiles->options()
                                                  |  SelectFilesLayout::SFO_OUTPUT_FOLDER_MANDATORY
                                                  |  SelectFilesLayout::SFO_REQUEST_ASTROMETRIC
                                                  |  SelectFilesLayout::SFO_REQUEST_OUTPUT_DIR
                                            )
                                          ,false);
    }
}

void  ImageCatalogDialog::addDetectionToImage(MdiFitImage* image, StarDetector* detector)
{
    if (!image || !detector) return;

    XOverlayLayer* layer = new XOverlayLayer(XLAYER_DETECTED_STARS);
    XOverlayStyle* mc_style = layer->addStyle(XGROUP_DETECTED_STAR);
    XOverlayStyle* deblend_Style = layer->addStyle(XGROUP_DEBLENDED_STAR);

    deblend_Style->setBorderColor(Qt::magenta);
    deblend_Style->setHoverBorderColor(Qt::red);
    mc_style->setBorderColor(Qt::green);
    mc_style->setHoverBorderColor(Qt::yellow);
    PlateConstants* pt = image->plate();
    DetectedStar* detstars = detector->stars();


    for (int i=0; i<detector->count(); i++)
    {
        XOverlayCircle* cir = new XOverlayCircle(layer, detstars[i].hasFlag(DetectedStar::DF_DEBLENDED)?XGROUP_DEBLENDED_STAR:XGROUP_DETECTED_STAR);
        cir->setRadius(detstars[i].radius());
        cir->setImageAttatched(true);
        cir->setZoomAttatched(true);
        cir->setHoverable(true);
        cir->setBasePos(detstars[i].x(), detstars[i].y());
        if (!pt)
        {
            cir->setLabel(QString("").sprintf("XY: %0.2f / %0.2f", detstars[i].x(), detstars[i].y()));
        }
        else
        {
            double ra, dec;
            pt->map(detstars[i].x(), detstars[i].y(), &ra, &dec);
            cir->setLabel(QString("").sprintf("XY:%0.2f/%0.2f | r:%0.2f\nRA: %s\nDEC: %s\n\nFLUX: %0.3f"
                                              "\n\n a:%0.2f; b:%0.2f; ecc:%0.2f"
                                              "\n WX:%d; WY:%d"
                                              "\n CCX:%0.2f; CCY:%0.2f"
                                             //Primera línea
                                             ,detstars[i].x(), detstars[i].y(),
                                             detstars[i].radius(),
                                             PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)),
                                             detstars[i].fluxISO(),
                                             //Segunda línea
                                             detstars[i].a(),detstars[i].b(), detstars[i].eccentricity(),
                                             //Tercera línea
                                             detstars[i].limits().width(),detstars[i].limits().height(),
                                             //Tercera línea
                                             detstars[i].ccx(), detstars[i].ccy()
                          ));
            /*
            DINFO("%d\t%7.2f\t%7.2f\t%7.2f\t%7.2f\t%14.10f\t%14.10f",i,
                  mDetectedStars[i].x()+1, mDetectedStars[i].y()+1,
                  mDetectedStars[i].x(), mDetectedStars[i].y(),
                  ra * HOR2DEG , dec);*/
        }
    }
    //Agregamos la nueva capa de estrellas detectadas y salimos
    image->addXLayer(layer, XLAYER_DETECTED_STARS_PRIORITY);
    detector->TakeMemControlOfStars();
    image->update();
}

