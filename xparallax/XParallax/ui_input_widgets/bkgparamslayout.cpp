#include "bkgparamslayout.h"
#include "ui_bkgparamslayout.h"

BkgParamsLayout::BkgParamsLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BkgParamsLayout)
{
    ui->setupUi(this);
    connect(ui->buttonReset, SIGNAL(clicked()), this, SLOT(SetDefaults_Slot()));
    SetDefaults_Slot();

    //De momento están desactivados todos los métodos de fondo excepto el kappa sigma
    QVariant disable_flag(0);
    for (int i=1; i<ui->comboBkgMethod->count();i++)
    {
        QModelIndex index = ui->comboBkgMethod->model()->index(i, 0);
        ui->comboBkgMethod->model()->setData(index, disable_flag, Qt::UserRole - 1);
    }
}

BkgParamsLayout::~BkgParamsLayout()
{
    delete ui;
}

bool BkgParamsLayout::CheckData(BackgroundParameters* bp)
{
    bp->bkg_upper_conv = ui->spingSigmaUpper->value();
    bp->bkg_lower_conv = ui->spinSigmaLower->value();
    bp->bkg_level = ui->comboBkgLevel->currentIndex() == 0 ? Core::BKGLV_TESSEL :
                    ui->comboBkgLevel->currentIndex() == 1 ? Core::BKGLV_BICUBIC_SPLINE : Core::BKGLV_BILINEAR_SPLINE;
    /*
    bp->bkg_method = ui->comboBkgMethod->currentIndex() == 0 ? Core::BKG_KAPPA_SIGMA :
                     ui->comboBkgMethod->currentIndex() == 1 ? Core::BKG_MEAN_MEDIAN :
                     ui->comboBkgMethod->currentIndex() == 2 ? Core::BKG_MEDIAN : Core::BKG_AVERAGE;
    */
    bp->bkg_smooth = ui->spinSmooth->value();
    bp->bkg_mesh_size = ui->spinMeshSz->value();
    bp->bkg_stdev_stop = ui->spingSigmaPrecission->value();
    return true;
}

void BkgParamsLayout::SetParameters(BackgroundParameters* bp)
{
    ui->comboBkgLevel->setCurrentIndex(
                        bp->bkg_level == Core::BKGLV_TESSEL ? 0 :
                        bp->bkg_level == Core::BKGLV_BICUBIC_SPLINE ? 1 : 2);

    //Desactivado de momento hasta nueva orden
    /*
    ui->comboBkgMethod->setCurrentIndex(
                         bp->bkg_method == Core::BKG_KAPPA_SIGMA ? 0:
                         bp->bkg_method == Core::BKG_MEAN_MEDIAN ? 1:
                         bp->bkg_method == Core::BKG_MEDIAN ? 2: 3);
                         */
    ui->spingSigmaUpper->setValue(bp->bkg_upper_conv);
    ui->spinSigmaLower->setValue(bp->bkg_lower_conv);
    ui->spinMeshSz->setValue(bp->bkg_mesh_size);
    ui->spinSmooth->setValue(bp->bkg_smooth);
    ui->spingSigmaPrecission->setValue(bp->bkg_stdev_stop);
}

void BkgParamsLayout::SetDefaults_Slot()
{
    BackgroundParameters bp;
    SetParameters(&bp);
}
