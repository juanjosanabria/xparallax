#ifndef LIGHTCURVEPARAMSLAYOUT_H
#define LIGHTCURVEPARAMSLAYOUT_H

#include <QWidget>

namespace Ui {
class LightCurveParamsLayout;
}

class LightCurveParamsLayout : public QWidget
{
    Q_OBJECT
    
public:
    explicit LightCurveParamsLayout(QWidget *parent = 0);
    ~LightCurveParamsLayout();
    
private:
    Ui::LightCurveParamsLayout *ui;
    QList<QPointF>              mPoints;
    QStringList                 mNames;

public slots:
    void AddStar_Slot();
    void RemoveStar_Slot();
    void ClearStars_Slot();
    void UpdateInterface_Slot();

};

#endif // LIGHTCURVEPARAMSLAYOUT_H
