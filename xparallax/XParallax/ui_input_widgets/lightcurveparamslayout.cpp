#include "lightcurveparamslayout.h"
#include "ui_lightcurveparamslayout.h"
#include "../inputdialog.h"
#include "util.h"

LightCurveParamsLayout::LightCurveParamsLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LightCurveParamsLayout)
{
    ui->setupUi(this);
    connect(ui->buttonAddStar, SIGNAL(clicked()), this, SLOT(AddStar_Slot()));
    connect(ui->buttonClearStars, SIGNAL(clicked()), this, SLOT(ClearStars_Slot()));
    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(UpdateInterface_Slot()));
    connect(ui->buttonRemoveStar, SIGNAL(clicked()), this, SLOT(RemoveStar_Slot()));
    UpdateInterface_Slot();
}

LightCurveParamsLayout::~LightCurveParamsLayout()
{
    delete ui;
}


void LightCurveParamsLayout::AddStar_Slot()
{
    InputDialog id("Source position", this);

    QVector<double> sp = id.ShowRADec();
    if (!sp.count()) return;
    if (id.nameRec().isnull())
    {
        QString cd = QString("α: %1; δ: %2").arg(Util::RA2String(sp.at(0))).arg(Util::DEC2String(sp.at(1)));
        ui->listWidget->addItem(cd);
        mNames.append("");
    }
    else
    {
        ui->listWidget->addItem(id.nameRec().name());
        mNames.append(id.nameRec().name());
    }
    mPoints.append(QPointF(sp.at(0), sp.at(1)));
    ui->buttonClearStars->setEnabled(true);
    UpdateInterface_Slot();
}


void LightCurveParamsLayout::RemoveStar_Slot()
{
    for (int i=0; i<ui->listWidget->count(); i++)
    {
        QListWidgetItem* qi = ui->listWidget->item(i);
        if (qi->isSelected())
        {
            mPoints.removeAt(i);
            mNames.removeAt(i);
            delete qi;
            i--;
        }
    }
    UpdateInterface_Slot();
}

void LightCurveParamsLayout::ClearStars_Slot()
{
    mPoints.clear();
    ui->listWidget->clear();
    UpdateInterface_Slot();
}


void LightCurveParamsLayout::UpdateInterface_Slot()
{
    if (ui->checkAllSatars->isChecked())
    {
        ui->listWidget->setEnabled(false);
        ui->buttonAddStar->setEnabled(false);
        ui->buttonClearStars->setEnabled(false);
        ui->buttonRemoveStar->setEnabled(false);
    }
    else
    {
        ui->buttonRemoveStar->setEnabled(ui->listWidget->count() && ui->listWidget->selectedItems().count());
        ui->buttonAddStar->setEnabled(true);
        ui->buttonClearStars->setEnabled(ui->listWidget->count());
    }
}
