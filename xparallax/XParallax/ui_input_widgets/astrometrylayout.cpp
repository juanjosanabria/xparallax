#include "astrometrylayout.h"
#include "ui_astrometrylayout.h"
#include "matcher.h"
#include "vo.h"

AstrometryLayout::AstrometryLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AstrometryLayout)
{
    ui->setupUi(this);

    //Cargar el combo de catálogos
    ui->comboCatalog->clear();
    for (int i=0; i<VizierCatalog::SOURCE_CATALOGS.count(); i++)
        ui->comboCatalog->addItem( VizierCatalog::SOURCE_CATALOGS.at(i)->desc(),VizierCatalog::SOURCE_CATALOGS.at(i)->name());

    //Cargar el combo de servidores vizier
    ui->comboServer->clear();
    for (int i=0; i<VizierServer::SOURCE_SERVERS.count(); i++)
        ui->comboServer->addItem(VizierServer::SOURCE_SERVERS.at(i)->desc(), VizierServer::SOURCE_SERVERS.at(i)->name());

    connect(ui->buttonSetDefaults, SIGNAL(clicked()), this, SLOT(SetDefaults_Slot()));
    connect(ui->checkScaleFilter, SIGNAL(toggled(bool)), ui->spinScaleTolerance, SLOT(setEnabled(bool)));
    connect(ui->checkMagFilter, SIGNAL(toggled(bool)), ui->spinMagTolerance, SLOT(setEnabled(bool)));  
}

AstrometryLayout::~AstrometryLayout()
{
    delete ui;
}


void AstrometryLayout::SetParams(MatchParameters* par)
{
    //Combo de catálogo
    ui->comboCatalog->setCurrentIndex(0);
    for (int i=0; i<ui->comboCatalog->count(); i++)
        if (ui->comboCatalog->itemData(i).toString() == par->vizier_catalog)
        {
            ui->comboCatalog->setCurrentIndex(i);
            break;
        }

    //Combo de servidor vizier
    ui->comboServer->setCurrentIndex(0);
    for (int i=0; i<ui->comboServer->count(); i++)
        if (ui->comboServer->itemData(i).toString() == par->vizier_server)
        {
            ui->comboServer->setCurrentIndex(i);
            break;
        }

    ui->spinMag->setValue(par->mag_limit);
    ui->spinMagTolerance->setValue(par->mag_tolerance*100);
    ui->spinScaleTolerance->setValue(par->scale_tolerance * 100);
    ui->spinFovExtension->setValue(par->fov_extension *100);
    ui->spinAlignStars->setValue(par->num_align_stars);
    ui->spinMatchDist->setValue(par->dist_tolerance);
    ui->checkScaleFilter->setChecked(par->scale_filter);
    ui->checkMagFilter->setChecked(par->mag_filter);

    if (par->preferred_center == Core::STARCENTER_PEAK) ui->comboCenter->setCurrentIndex(0);
    else if (par->preferred_center == Core::STARCENTER_ISO) ui->comboCenter->setCurrentIndex(1);
    else if (par->preferred_center == Core::STARCENTER_PSF) ui->comboCenter->setCurrentIndex(3);
    else ui->comboCenter->setCurrentIndex(2 /* Windowed */);
}


void AstrometryLayout::SetDefaults_Slot()
{
    MatchParameters par; par.SetDefaults();
    SetParams(&par);
}


bool AstrometryLayout::CheckData(MatchParameters* mp)
{
    mp->vizier_server = ui->comboServer->itemData(ui->comboServer->currentIndex()).toString();
    mp->vizier_catalog = ui->comboCatalog->itemData(ui->comboCatalog->currentIndex()).toString();
    mp->mag_limit = ui->spinMag->value();
    mp->num_align_stars = ui->spinAlignStars->value();
    mp->dist_tolerance = ui->spinMatchDist->value();
    mp->fov_extension = ui->spinFovExtension->value() / 100.0;
    mp->scale_filter = ui->checkScaleFilter->isChecked();
    mp->scale_tolerance = ui->spinScaleTolerance->value() / 100.0;
    mp->mag_filter = ui->checkMagFilter->isChecked();
    mp->mag_tolerance = ui->spinMagTolerance->value() / 100.0;

    if (ui->comboCenter->currentIndex() == 0) mp->preferred_center = Core::STARCENTER_PEAK;
    else if (ui->comboCenter->currentIndex() == 1) mp->preferred_center = Core::STARCENTER_ISO;
    else if (ui->comboCenter->currentIndex() == 2) mp->preferred_center = Core::STARCENTER_WINDOWED;
    else mp->preferred_center = Core::STARCENTER_PSF;
    return true;
}











