#include "calibrationlayout.h"
#include "ui_calibrationlayout.h"
#include "calibration.h"
#include "util.h"

CalibrationLayout::CalibrationLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CalibrationLayout)
{
    ui->setupUi(this);

    connect(ui->checkNormalize, SIGNAL(toggled(bool)), ui->spinNormLevel, SLOT(setEnabled(bool)));
    connect(ui->comboRejectMethod, SIGNAL(currentIndexChanged(int)), this, SLOT(ChangeRejectAlgorithm_Slot(int)));
    connect(ui->buttonDefaults, SIGNAL(clicked()), this, SLOT(SetDefaults_Slot()));

    SetDefaults_Slot();
}

CalibrationLayout::~CalibrationLayout()
{
    delete ui;
}


bool CalibrationLayout::CheckData(CalibrationParameters* cp)
{
    cp->reject_algorithm = ui->comboRejectMethod->currentIndex() == 0 ? Core::PIXREJECT_SIGMA :
                           ui->comboRejectMethod->currentIndex() == 1 ? Core::PIXREJECT_MINMAX : Core::PIXREJECT_NONE;
    cp->bit_pp = ui->comboOutputFormat->currentIndex() == 0 ? -1 :
                 ui->comboOutputFormat->currentIndex() == 1 ? -32 :
                 ui->comboOutputFormat->currentIndex() == 2 ? 16 :
                 ui->comboOutputFormat->currentIndex() == 3 ? 32 : 8;
    cp->save_master_flat = ui->checkSaveFlat->isChecked();
    cp->save_master_dark = ui->checkSaveDark->isChecked();
    cp->save_master_bias = ui->checkSaveBias->isChecked();
    cp->norm_level = ui->checkNormalize->isChecked() ? ui->spinNormLevel->value() : 0;
    cp->reject_sigma_low = ui->spinSigmaLow->value();
    cp->reject_sigma_high = ui->spinSigmaHigh->value();
    cp->reject_min = ui->spinAduMin->value();
    cp->reject_max = ui->spinAduMax->value();
    return true;
}

void CalibrationLayout::SetParameters(CalibrationParameters* cp)
{
    ui->comboRejectMethod->setCurrentIndex(
                cp->reject_algorithm == Core::PIXREJECT_SIGMA ? 0 :
                (cp->reject_algorithm == Core::PIXREJECT_MINMAX ? 1 : 2)
    );
    ui->comboOutputFormat->setCurrentIndex(
        cp->bit_pp == -1 ? 0 :
        cp->bit_pp == -32 ? 1 :
        cp->bit_pp == 16 ? 2 :
        cp->bit_pp == 32 ? 3 : 4
    );
    ui->checkSaveFlat->setChecked(cp->save_master_flat);
    ui->checkSaveDark->setChecked(cp->save_master_dark);
    ui->checkSaveBias->setChecked(cp->save_master_bias);
    ui->spinSigmaHigh->setValue(cp->reject_sigma_high);
    ui->spinSigmaLow->setValue(cp->reject_sigma_low);
    ui->checkNormalize->setChecked(cp->norm_level > 0);
    if (cp->norm_level > 0) ui->spinNormLevel->setValue(cp->norm_level);
}

void CalibrationLayout::ChangeRejectAlgorithm_Slot(int newindex)
{
    ui->spinSigmaLow->setEnabled(newindex == 0);
    ui->spinSigmaHigh->setEnabled(newindex == 0);
    ui->labelSigma->setEnabled(newindex == 0);
    ui->spinAduMin->setEnabled(newindex == 1);
    ui->spinAduMax->setEnabled(newindex == 1);
    ui->labelAdu->setEnabled(newindex == 1);
}


void CalibrationLayout::SetDefaults_Slot()
{
    //Establecemos valores por defecto para comenzar
    CalibrationParameters cp;
    cp.SetDefaults();
    SetParameters(&cp);
}



