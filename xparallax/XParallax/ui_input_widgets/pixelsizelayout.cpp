#include <QPixmap>

#include "pixelsizelayout.h"
#include "ui_pixelsizelayout.h"
#include "xglobal.h"
#include "../mainwindow.h"
#include "../inputdialog.h"

#define  SETTINGS_USE_EQUIPEMENT_PAR        "pxs_use_epars"

PixelSizeLayout::PixelSizeLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PixelSizeLayout)
{
    ui->setupUi(this);

    mRAok = mDECok = false;
    mInternalChange = false;

    connect(ui->buttonSearch, SIGNAL(clicked()), this, SLOT(SelectRaDec_Slot()));
    connect(ui->editRA, SIGNAL(textChanged(QString)), this, SLOT(ChangeRADEC_Slot()));
    connect(ui->editDEC, SIGNAL(textChanged(QString)), this, SLOT(ChangeRADEC_Slot()));
    connect(ui->checkReducer, SIGNAL(toggled(bool)), ui->spinFocalReducer, SLOT(setEnabled(bool)));

    //Cuando se cambia algo que afecta al píxel se recalcula
    connect(ui->spinFocalLength, SIGNAL(valueChanged(int)), this, SLOT(ComputePixelScale_Slot()));
    connect(ui->spinFocalReducer, SIGNAL(valueChanged(double)), this, SLOT(ComputePixelScale_Slot()));
    connect(ui->spinPixelSize, SIGNAL(valueChanged(double)), this, SLOT(ComputePixelScale_Slot()));
    connect(ui->checkReducer, SIGNAL(toggled(bool)), this, SLOT(ComputePixelScale_Slot()));
    connect(ui->comboBin, SIGNAL(currentIndexChanged(int)), this, SLOT(ComputePixelScale_Slot()));
    connect(ui->radioDateFixed, SIGNAL(toggled(bool)), ui->dateEdit, SLOT(setEnabled(bool)));
    connect(ui->radioFLPixSize, SIGNAL(toggled(bool)), this, SLOT(RadioPxSizeSlot()));
    connect(ui->radioPlateScale, SIGNAL(toggled(bool)), this, SLOT(RadioPxSizeSlot()));


    //Establecemos todos los valores por defecto
    MatchParameters mp;
    mp.SetDefaults();
    SetParams(&mp);

    ui->dateEdit->calendarWidget()->setFirstDayOfWeek(QLocale::system().firstDayOfWeek());

    //Desgraciadamente, para Windows a 100dpi no nos vale el diseño
    if (XGlobal::SCREEN_DPI <= 1.10)
    {
        ui->verticalLayout->setSpacing(5);
        ui->groupBoxImageCenter->layout()->setSpacing(3);
        ui->groupBoxImageCenter->layout()->setContentsMargins(11,2,11,2);
        ui->groupBoxDatePM->layout()->setSpacing(3);
        ui->groupBoxDatePM->layout()->setContentsMargins(11,2,11,2);
        ui->groupBoxPlateScale->layout()->setSpacing(3);
        ui->groupBoxPlateScale->layout()->setContentsMargins(11,2,11,2);
    }
}

PixelSizeLayout::~PixelSizeLayout()
{
    delete ui;
}

void PixelSizeLayout::SelectRaDec_Slot()
{
    InputDialog id(this);

    double current_ra = Util::ParseRA(ui->editRA->text());
    double current_dec = Util::ParseDEC(ui->editDEC->text());

    id.SetTitle("Select image center");
    QVector<double> ret = id.ShowRADec(current_ra, current_dec);
    if (!ret.count()) return;

    ui->editRA->setText(Util::RA2String(ret[0]));
    ui->editDEC->setText(Util::DEC2String(ret[1]));
}

void PixelSizeLayout::ChangeRADEC_Slot()
{
    if (mInternalChange) return;
    double ra = INVALID_FLOAT, dec = INVALID_FLOAT;
    //Cuidado!, si se ha pegado el valor en uno de los dos combos
    QVector<double> radec = Util::ParseRADEC(ui->editRA->text());
    if (radec.length() == 2 && IS_VALIDF(radec[0]) && IS_VALIDF(radec[1]))
    {
        ra = radec[0];
        dec = radec[1];
        mInternalChange = true;
        ui->editRA->setText(Util::RA2String(ra));
        ui->editDEC->setText(Util::DEC2String(dec));
        mInternalChange = false;
    }
    else
    {
        radec = Util::ParseRADEC(ui->editDEC->text());
        if (radec.length() == 2 && IS_VALIDF(radec[0]) && IS_VALIDF(radec[1]))
        {
            ra = radec[0];
            dec = radec[1];
            mInternalChange = true;
            ui->editRA->setText(Util::RA2String(ra));
            ui->editDEC->setText(Util::DEC2String(dec));
            mInternalChange = false;
        }
        else
        {
            ra = Util::ParseRA(ui->editRA->text());
            dec = Util::ParseDEC(ui->editDEC->text());
        }
    }
    if (!IS_INVALIDF(ra) != mRAok)
        ui->imgRA->setPixmap(IS_INVALIDF(ra) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png") );
    mRAok = !IS_INVALIDF(ra);

    if (!IS_INVALIDF(dec) != mDECok)
        ui->imgDEC->setPixmap(IS_INVALIDF(dec) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png") );
    mDECok = !IS_INVALIDF(dec);

    //Como se ha editado manualmente ponemos la fuente en negro
    ui->editRA->setStyleSheet("");
    ui->editDEC->setStyleSheet("");
}

bool PixelSizeLayout::CheckData(MatchParameters* mp)
{
    mp->focal_length_mm = ui->spinFocalLength->value();
    mp->focal_reducer = ui->checkReducer->isChecked() ? ui->spinFocalReducer->value() : 1.0;
    mp->center_ra = Util::ParseRA(ui->editRA->text());
    mp->center_dec = Util::ParseDEC(ui->editDEC->text());
    mp->flip_field = ui->checkFlip->isChecked();
    if (isnan(mp->center_dec) || isnan(mp->center_ra))
    {
        QMessageBox::warning(this,"Error", "Equatorial coordinates are not valid, please, fill in the form with the image center.");
        return false;
    }

    mp->pixel_size_um = ui->spinPixelSize->value();
    mp->binning = ui->comboBin->currentIndex() + 1;
    mp->date_frame = ui->dateEdit->date();
    mp->focal_reducer = ui->spinFocalReducer->value();
    mp->pixel_scale_arcsec = ui->spinPlateScale->value();
    mp->use_scale_by_fl_ps = ui->radioFLPixSize->isChecked();

    if (ui->radioDateDisabled->isChecked()) mp->pm_correct = MatchParameters::PMDATE_NO_CORRECTION;
    else if (ui->radioDateFit->isChecked()) mp->pm_correct = MatchParameters::PMDATE_FROM_FIT;
    else
    {
        mp->pm_correct = MatchParameters::PMDATE_FIXED;
        mp->date_frame = ui->dateEdit->date();
    }

    mp->ComputeScale();
    return true;
}


void PixelSizeLayout::SetParams(MatchParameters* par)
{
    ui->radioFLPixSize->setChecked(par->use_scale_by_fl_ps);
    ui->radioPlateScale->setChecked(!par->use_scale_by_fl_ps);
    ui->spinFocalLength->setValue(isnan(par->focal_length_mm)?2800:par->focal_length_mm);
    ui->spinPixelSize->setValue(isnan(par->pixel_size_um)?5.4:par->pixel_size_um);
    ui->editRA->setText(isnan(par->center_ra)?"":Util::RA2String(par->center_ra));
    ui->editDEC->setText(isnan(par->center_dec)?"":Util::DEC2String(par->center_dec));
    ui->checkReducer->setChecked(par->focal_reducer != 1);
    ui->spinFocalReducer->setValue(par->focal_reducer);
    ui->dateEdit->setDate(par->date_frame.isValid()?par->date_frame:QDate::currentDate());
    ui->checkFlip->setChecked(par->flip_field);

    ui->radioDateDisabled->setChecked(par->pm_correct == MatchParameters::PMDATE_NO_CORRECTION);
    ui->radioDateFixed->setChecked(par->pm_correct == MatchParameters::PMDATE_FIXED);
    ui->dateEdit->setEnabled(par->pm_correct == MatchParameters::PMDATE_FIXED);
    ui->radioDateFit->setChecked(par->pm_correct == MatchParameters::PMDATE_FROM_FIT);
    ui->comboBin->setCurrentIndex(par->binning-1);
    ui->spinPlateScale->setValue(par->pixel_scale_arcsec);
    RadioPxSizeSlot();
}

void PixelSizeLayout::ComputePixelScale_Slot()
{
    if (ui->radioFLPixSize->isChecked())
    {
        double fl = ui->spinFocalLength->value();
        if (ui->checkReducer->isChecked()) fl *= ui->spinFocalReducer->value();
        double pix_size = ui->spinPixelSize->value() * (double)(ui->comboBin->currentIndex() + 1);
        ui->spinPlateScale->setValue((pix_size / fl) * 206.3);
    }
}


void PixelSizeLayout::RADECDetected_Slot(double ra, double dec)
{
    mInternalChange = true;
    ui->editRA->setText(Util::RA2String(ra));
    ui->editDEC->setText(Util::DEC2String(dec));
    //Marcamos en otro color los edits
    ui->editRA->setStyleSheet("color: #0000FF;");
    ui->editDEC->setStyleSheet("color: #0000FF;");
    mInternalChange = false;
}


void PixelSizeLayout::RadioPxSizeSlot()
{
    ui->labelFocalLength->setEnabled(ui->radioFLPixSize->isChecked());
    ui->spinFocalLength->setEnabled(ui->radioFLPixSize->isChecked());
    ui->labelPixelSize->setEnabled(ui->radioFLPixSize->isChecked());
    ui->spinPixelSize->setEnabled(ui->radioFLPixSize->isChecked());
    ui->spinFocalLength->setEnabled(ui->radioFLPixSize->isChecked());
    ui->checkReducer->setEnabled(ui->radioFLPixSize->isChecked());
    ui->spinFocalReducer->setEnabled(ui->radioFLPixSize->isChecked()&&ui->checkReducer->isChecked());
    ui->labelBinning->setEnabled(ui->radioFLPixSize->isChecked());
    ui->comboBin->setEnabled(ui->radioFLPixSize->isChecked());
    ui->spinPlateScale->setEnabled(ui->radioPlateScale->isChecked());
    ComputePixelScale_Slot();
}

























