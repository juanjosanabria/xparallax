#ifndef DETPARAMSWIDGET_H
#define DETPARAMSWIDGET_H

#include <QWidget>
#include "detection.h"

namespace Ui {
class DetParamsLayout;
}

class DetParamsLayout : public QWidget
{
    Q_OBJECT
    
public:
    explicit DetParamsLayout(QWidget *parent = 0);
    ~DetParamsLayout();

    void SetParameters(DetectionParameters* par);
    bool CheckData(DetectionParameters* par);
    
private:
    Ui::DetParamsLayout *ui;

public slots:
    void Reset_Slot();

};

#endif // DETPARAMSWIDGET_H
