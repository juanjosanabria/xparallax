#include "detparamslayout.h"
#include "ui_detparamslayout.h"

DetParamsLayout::DetParamsLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DetParamsLayout)
{
    ui->setupUi(this);

    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->spinDeblendContrast, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->spinDeblendLevels, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->spinDeblendLower, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->spinDeblendUpper, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->labelDeblend1, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->labelDeblend2, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->labelDeblend3, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->labelDeblend4, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->labelDeblend5, SLOT(setEnabled(bool)));
    connect(ui->checkTryDeblend, SIGNAL(toggled(bool)), ui->labelDeblend6, SLOT(setEnabled(bool)));
    connect(ui->buttonReset, SIGNAL(clicked()), this, SLOT(Reset_Slot()));

    DetectionParameters p;
    SetParameters(&p);
}

void DetParamsLayout::SetParameters(DetectionParameters* par)
{
    ui->spinThresSigma->setValue(par->detect_thres_sigma);
    ui->spinMinStarArea->setValue(par->detect_min_area);
    ui->spinMaxStarRadius->setValue(par->detect_max_radius);
    ui->spinMinStarRadius->setValue(par->detect_min_radius);
    ui->spinStarExcc->setValue(par->detect_ecc_max);

    //Parámetros de deblend
    ui->checkTryDeblend->setChecked(par->deblend_stars);
    ui->spinDeblendLevels->setValue(par->deblend_nthres);
    ui->spinDeblendContrast->setValue(par->deblend_contrast);
    ui->spinDeblendLower->setValue(par->deblend_lower_sigma);
    ui->spinDeblendUpper->setValue(par->deblend_upper_sigma);
    //Parámetros de fondo??? los ignoramos, se leen de otro widget

}

bool DetParamsLayout::CheckData(DetectionParameters* par)
{
    par->detect_thres_sigma = ui->spinThresSigma->value();
    par->detect_min_area = ui->spinMinStarArea->value();
    par->detect_max_radius = ui->spinMaxStarRadius->value();
    par->detect_min_radius = ui->spinMinStarRadius->value();
    par->detect_ecc_max = ui->spinStarExcc->value();

    par->deblend_nthres = ui->spinDeblendLevels->value();
    par->deblend_contrast = ui->spinDeblendContrast->value();
    par->deblend_lower_sigma = ui->spinDeblendLower->value();
    par->deblend_upper_sigma = ui->spinDeblendUpper->value();
    return true;
}

DetParamsLayout::~DetParamsLayout()
{
    delete ui;
}


void DetParamsLayout::Reset_Slot()
{
    DetectionParameters dp;
    dp.SetDefaults();
    SetParameters(&dp);
}



