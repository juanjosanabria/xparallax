#ifndef PIXELSIZELAYOUT_H
#define PIXELSIZELAYOUT_H

#include <QWidget>
#include "matcher.h"

namespace Ui {
class PixelSizeLayout;
}

class PixelSizeLayout : public QWidget
{
    Q_OBJECT
    
public:
    explicit PixelSizeLayout(QWidget *parent = 0);
    ~PixelSizeLayout();
    bool CheckData(MatchParameters* mp);
    void SetParams(MatchParameters* par);


private:
    Ui::PixelSizeLayout *ui;

    bool        mRAok;
    bool        mDECok;
    bool        mInternalChange;

public slots:
    void SelectRaDec_Slot();
    void ChangeRADEC_Slot();
    void ComputePixelScale_Slot();
    void RADECDetected_Slot(double ra, double lat);
    void RadioPxSizeSlot();

};

#endif // PIXELSIZELAYOUT_H
