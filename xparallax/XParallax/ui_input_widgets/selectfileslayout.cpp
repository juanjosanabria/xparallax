#include "selectfileslayout.h"
#include "ui_selectfileslayout.h"
#include "util.h"
#include "xglobal.h"
#include "../mainwindow.h"

QString (*          SelectFilesLayout::FN_GET_ITEM_IMAGE)(FitHeaderList* fh,const QString& filename);
QString (*        SelectFilesLayout::FN_GET_ITEM_TOOLTIP)(FitHeaderList* fh,const QString& filename);
int (*               SelectFilesLayout::FN_GET_ITEM_SORT)(const QString& filename_a,const QString& filename_b);

SelectFilesLayout::SelectFilesOptions SelectFilesLayout::DEFAULT_OPTIONS =
        (SelectFilesLayout::SelectFilesOptions)
        (SFO_ALLOW_CURRENT_FILE |
        SFO_ALLOW_ALL_FILES |
        SFO_ALLOW_DISK_FILES |
        SFO_ALLOW_OVERWRITE |
        SFO_REQUEST_OUTPUT_DIR |
        SFO_OUTPUT_ALLOWED |
        SFO_REQUEST_SAME_SIZE         //Se solicitará astrometría de todas las imágenes

         );

SelectFilesLayout::SelectFilesLayout(QWidget *parent, SelectFilesOptions options) :
    QWidget(parent),
    ui(new Ui::SelectFilesLayout)
{
    ui->setupUi(this);
    mMinImages = 1;

    //Conexión
    connect(ui->radioCurrentFile, SIGNAL(toggled(bool)), this, SLOT(SetSelCurrentImage_Slot(bool)));
    connect(ui->radioCurrentFiles, SIGNAL(toggled(bool)),this, SLOT(SetSelCurrentImages_Slot(bool)));
    connect(ui->radioFileList, SIGNAL(toggled(bool)), this, SLOT(SetSelFiles_Slot(bool)));
    connect(ui->buttonAddFiles, SIGNAL(clicked()), this, SLOT(AddFiles_Slot()));
    connect(ui->outputDirButton, SIGNAL(clicked()), this, SLOT(SelectOutputDir_Slot()));
    connect(ui->checkOverwrite, SIGNAL(toggled(bool)), this, SLOT(CheckOverWrite_Slot(bool)));
    //Botón de limpiar y otras opciones
    connect(ui->buttonClearFiles, SIGNAL(clicked()),this, SLOT(ClearFiles_Slot()));
    connect(ui->buttonRemoveFile, SIGNAL(clicked()), this, SLOT(RemoveSelected_Slot()));
    connect(ui->tableImages, SIGNAL(itemSelectionChanged()), this, SLOT(ChangeSelection_Slot()));

    //Inicialización de las opciones
    //Si la ventana principal no tiene ventanas fit seleccionamos la opción de ficheros
    mFitcount = MainWindow::mainwin->SubWindowCount(MdiFitImage::staticMetaObject.className());
    mFitSelected = MainWindow::mainwin->ActiveFitImage();
    //Aplicamos las opciones
    SetOptions(options);

    if (!mFitcount)
    {
        ui->radioCurrentFile->setEnabled(false);
        ui->radioCurrentFiles->setEnabled(false);
        ui->radioFileList->setChecked(true);
        SetSelFiles_Slot(true);                          //Nos aseguramos de que se habilitan los elementos correspondientes
    }
    else if (mFitSelected && (mOptions&SFO_ALLOW_CURRENT_FILE))
    {
        ui->radioCurrentFile->setEnabled(mOptions&SFO_ALLOW_CURRENT_FILE);
        ui->radioCurrentFiles->setEnabled(false);
        ui->radioCurrentFile->setChecked(true);
    }
    else if (mFitcount && !mFitSelected && (mOptions&SFO_ALLOW_ALL_FILES))
    {
        ui->radioCurrentFile->setEnabled(false);
        ui->radioCurrentFile->setEnabled(false);
        ui->radioCurrentFiles->setEnabled(mOptions&SFO_ALLOW_ALL_FILES);
        ui->radioCurrentFiles->setChecked(true);
    }
    //Si no ha habido nada seleccionado, seleccionamos archivos de disoc
    else
    {
        ui->radioCurrentFile->setEnabled(false);
        ui->radioCurrentFiles->setEnabled(false);
        ui->radioFileList->setChecked(true);
    }

    //Funciones específicas para ordenar, obtener la imagen y el tooltip
    FN_GET_ITEM_IMAGE = NULL;
    FN_GET_ITEM_TOOLTIP = NULL;
    FN_GET_ITEM_SORT = FN_ORDER_BY_NAME;
    ChangeSelection_Slot();
}

void SelectFilesLayout::SetOptions(SelectFilesOptions options, bool auto_select_input)
{
   mOptions = options;
   ui->checkOverwrite->setVisible(options & SFO_ALLOW_OVERWRITE);
   CheckOverWrite_Slot(options & SFO_ALLOW_OVERWRITE);
   ui->checkOverwrite->setChecked(options & SFO_OVERWRITE_CHECKED);

   ui->radioCurrentFile->setEnabled(options & SFO_ALLOW_CURRENT_FILE && mFitSelected );
   ui->radioCurrentFiles->setEnabled(options & SFO_ALLOW_CURRENT_FILE && mFitcount);
   ui->radioFileList->setEnabled(options & SFO_ALLOW_DISK_FILES);

   if(auto_select_input)
   {
       if (!ui->radioCurrentFile->isEnabled() && ui->radioCurrentFile->isChecked())
           ui->radioCurrentFile->setChecked(false);
       if (!ui->radioCurrentFiles->isEnabled() && ui->radioCurrentFiles->isChecked())
           ui->radioCurrentFiles->setChecked(false);
       if (!ui->radioFileList->isEnabled() && ui->radioFileList->isChecked())
           ui->radioFileList->setChecked(false);
   }

   //Control de solicitud de directorio de salida
   if (!(mOptions & SFO_REQUEST_OUTPUT_DIR) && ui->outputDirEdit->isEnabled())
   {
        ui->outputDirEdit->setEnabled(false);
        ui->outputDirButton->setEnabled(false);
        ui->outputDirLabel->setEnabled(false);
   }
   else if ((mOptions & SFO_REQUEST_OUTPUT_DIR) && !ui->outputDirEdit->isEnabled())
   {
       ui->outputDirEdit->setEnabled(true);
       ui->outputDirButton->setEnabled(true);
       ui->outputDirLabel->setEnabled(true);
   }

   //Si no está permitida la salida de archivos se ocultarán todas las opciones de archivos
   if (! (mOptions & SFO_OUTPUT_ALLOWED) )
   {
        //Ocultamos la opción de guardar salida
        ui->frame_options->setVisible(false);
        ui->frame_output->setVisible(false);
        //Además hacemos crecer los otros elementos hasta que se ajusten al tamaño de la ventana

        ui->buttonAddFiles->move(ui->buttonAddFiles->x(), this->height() -  ui->buttonAddFiles->height() - 5);
        ui->buttonClearFiles->move(ui->buttonClearFiles->x(), this->height() -  ui->buttonClearFiles->height() - 5);
        ui->buttonRemoveFile->move(ui->buttonRemoveFile->x(), this->height() -  ui->buttonRemoveFile->height() - 5);
        ui->tableImages->resize(ui->tableImages->width(),
                                (ui->tableImages->height() + (ui->buttonAddFiles->y() - (ui->tableImages->y() + ui->tableImages->height()) - 5)) );
   }

   if (auto_select_input)
   {
       //Imagen actual
       if (! (mOptions & SFO_ALLOW_CURRENT_FILE))
       {
           ui->radioCurrentFile->setEnabled(false);
           ui->radioCurrentFile->setChecked(false);
       }
       else if ((mOptions & SFO_ALLOW_CURRENT_FILE) && MainWindow::mainwin->ActiveFitImage() && !ui->tableImages->topLevelItemCount())
       {
           ui->radioCurrentFile->setEnabled(true);
           ui->radioCurrentFile->setChecked(true);
       }

       //Todas las imágenes en el espacio de trabajo
       if (! (mOptions & SFO_ALLOW_ALL_FILES))
       {
           ui->radioCurrentFiles->setEnabled(false);
           ui->radioCurrentFiles->setChecked(false);
       }
       else if (mFitcount && mFitcount >= mMinImages && !ui->tableImages->topLevelItemCount())
       {
           ui->radioCurrentFiles->setEnabled(true);
           ui->radioCurrentFiles->setChecked(!ui->radioCurrentFile->isChecked());
       }

       //No hay nada habilitado de imágenes abiertas, seleccionamos archivos de disco
       if (!ui->radioCurrentFile->isEnabled() && !ui->radioCurrentFiles->isEnabled())
           ui->radioFileList->setChecked(true);
   }
}

SelectFilesLayout::~SelectFilesLayout()
{
    delete ui;
}


void SelectFilesLayout::SetSelCurrentImage_Slot(bool act)
{
    if (!act) return;
    ui->tableImages->setEnabled(false);

    ui->buttonRemoveFile->setEnabled(false);
    ui->buttonClearFiles->setEnabled(false);
    ui->buttonAddFiles->setEnabled(false);

    ui->outputDirButton->setEnabled(false);
    ui->outputDirEdit->setEnabled(false);
    ui->outputDirLabel->setEnabled(false);
    ui->checkOverwrite->setEnabled(false);

    LookforImageInformation();
}

//Busca en las opciones seleccionadas actualmente para localizar en alguna imagen
//el centro y emitir las señales correspondientes
void SelectFilesLayout::LookforImageInformation()
{
    double ra = INVALID_FLOAT;
    double dec = INVALID_FLOAT;
    MdiFitImage* fi = NULL;
    //Compribamos si la imagen actual tiene coordenadas
    if (ui->radioCurrentFile->isChecked())
    {
        fi = MainWindow::mainwin->ActiveFitImage();;
        ra = fi->fitimage()->headers()->ra();
        dec = fi->fitimage()->headers()->dec();
    }
    else if (ui->radioCurrentFiles->isChecked())
    {
        QList<QMdiSubWindow*> sw =
        MainWindow::mainwin->SubWindows(MdiFitImage::staticMetaObject.className());
        if (sw.count())
        {
             fi = (MdiFitImage*) sw[0]->widget();
             ra = fi->fitimage()->headers()->ra();
             dec = fi->fitimage()->headers()->dec();
        }
    }
    else if (mAnyFileHeaders.count()) //(ui->radioFilesFromDisk->isChecked)
    {
        ra = mAnyFileHeaders.ra();
        dec = mAnyFileHeaders.dec();
    }

    //Si hemos capturado las coordenadas indicamos que provienen del archivo
    if (!IS_INVALIDF(ra) && !IS_INVALIDF(dec))
        emit RADECDetected_Signal(ra, dec);

}

void SelectFilesLayout::SetSelCurrentImages_Slot(bool act)
{
    if (!act) return;
    ui->tableImages->setEnabled(false);
    ui->buttonRemoveFile->setEnabled(false);
    ui->buttonClearFiles->setEnabled(false);
    ui->outputDirButton->setEnabled(false);
    ui->outputDirEdit->setEnabled(false);
    ui->outputDirLabel->setEnabled(false);
    ui->checkOverwrite->setEnabled(false);
    LookforImageInformation();
}

void SelectFilesLayout::SetSelFiles_Slot(bool act)
{
    if (!act) return;
    ui->tableImages->setEnabled(true);
    ui->buttonRemoveFile->setEnabled(true);
    ui->buttonClearFiles->setEnabled(true);
    ui->buttonAddFiles->setEnabled(true);
    ui->outputDirButton->setEnabled(true);
    ui->outputDirEdit->setEnabled(true);
    ui->outputDirLabel->setEnabled(true);
    ui->checkOverwrite->setEnabled(true);
    LookforImageInformation();
}


bool SelectFilesLayout::ContainsFile(const QString& path)
{
    QString fullpath = QFileInfo(path).absoluteFilePath();
    for (int i=0; i<count(); i++)
    {
         QTreeWidgetItem* item = ui->tableImages->topLevelItem(i);
         return item->data(0, Qt::UserRole).toString() == fullpath;
    }
    return false;
}

void SelectFilesLayout::AddFiles_Slot()
{
    QString default_dir = mLastFilePath.length() && QFileInfo(mLastFilePath).exists() ? mLastFilePath : "";

    QStringList list =
    QFileDialog::getOpenFileNames(this,
                                  "Select input files",
                                  default_dir, "Science images (*.fit *.fits)"
                                  );

    QList<QTreeWidgetItem*> to_load_items;
    for (int i=0; i<list.count(); i++)
    {
        if (ContainsFile(list.at(i))) continue;
        QFileInfo finfo(list[i]);
        if (!i) mLastFilePath = finfo.absolutePath();

        //if (mSelectedFiles.contains(list[i])) continue;
        FitHeaderList fhl = FitHeaderList::ReadFromFile(list[i]);
        if (fhl.CheckFormat().length()) continue;
        PlateConstants* plate_constants = fhl.LookForPlateConstants();
        if (plate_constants) delete plate_constants;

        //Una vez agregadas si no tenemos cabeceras cogemos las cabeceras de la primera
        if (!mAnyFileHeaders.count()) mAnyFileHeaders = fhl;

        QTreeWidgetItem* item = new QTreeWidgetItem();
        item->setText(0,finfo.fileName());
        item->setText(1,QString("%1 s").arg(fhl.exptime()));
        item->setText(2, QString("%1x%2").arg(fhl.width()).arg(fhl.height()) );

        //¿Tiiene imagen el item?
        if (FN_GET_ITEM_IMAGE) item->setIcon(0, QIcon(QPixmap(FN_GET_ITEM_IMAGE(&fhl, finfo.fileName()))));
        if (FN_GET_ITEM_TOOLTIP) item->setToolTip(0, FN_GET_ITEM_TOOLTIP(&fhl, finfo.fileName()));


        item->setData(0, Qt::UserRole, finfo.absoluteFilePath());
        item->setData(1, Qt::UserRole, finfo.fileName());
        item->setData(2, Qt::UserRole, fhl.width() *  fhl.height());    //Ancho del archivo

        to_load_items.append(item);
    }

    if (FN_GET_ITEM_SORT) qSort(to_load_items.begin(), to_load_items.end(), SelectFilesLayout::SelectedFile_greaterThan);

    //Ahora ordenamos los items si fuera necesario y los agregamos a la lista
    for (int i=0; i<to_load_items.count(); i++)
        ui->tableImages->addTopLevelItem(to_load_items[i]);

    //Si hemos cargado algún item seleccionamos la opción de archivos de disco
    if (to_load_items.count()) ui->radioFileList->setChecked(true);

    ui->tableImages->setEnabled(true);
    ui->tableImages->setColumnWidth(0,250);
    ui->tableImages->setColumnWidth(1,60);
    ChangeSelection_Slot();
    LookforImageInformation();
    CheckImageSizes();
}

bool SelectFilesLayout::SelectedFile_greaterThan(QTreeWidgetItem *a, QTreeWidgetItem *b)
{
    QString name_a = a->data(1,Qt::UserRole).toString();
    QString name_b = b->data(1,Qt::UserRole).toString();
    return FN_GET_ITEM_SORT(name_a, name_b);
}

void SelectFilesLayout::ClearFiles_Slot()
{
    ui->tableImages->clear();
    ui->buttonClearFiles->setEnabled(false);
    mAnyFileHeaders.Clear();
}

void SelectFilesLayout::SelectOutputDir_Slot()
{
    QString default_dir = ui->outputDirEdit->text().length() ?  ui->outputDirEdit->text() : mLastOutputPath;
    if (!default_dir.length()) default_dir = "/home";
    QString dir = QFileDialog::getExistingDirectory(this, "Select an output folder",
                                                 default_dir,
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    if (dir.length())
    {
        if (!QDir(dir).exists())
        {
            if ( QMessageBox::question(this,"Output directory",
                                         QString("Directory '%1' does not exist.\n\n"
                                         "¿Do you want to create it?"
                                         ).arg(dir),QMessageBox::Yes|QMessageBox::No
                                      ) != QMessageBox::Yes
                ) return;
            if (!QDir().mkdir(dir))
            {
                QMessageBox::question(this,"Output directory",
                                                     QString("Unable to create directory '%1'").arg(dir));
                return;
            }
        }
        ui->outputDirEdit->setText(QDir::toNativeSeparators(dir));
        mOutputDir = ui->outputDirEdit->text();
    }
}


void SelectFilesLayout::CheckOverWrite_Slot(bool ow)
{
    if (ow)
    {
        ui->outputDirEdit->setEnabled(false);
        ui->outputDirButton->setEnabled(false);
    }
    else
    {
        ui->outputDirEdit->setEnabled(true);
        ui->outputDirButton->setEnabled(true);
    }
}

void SelectFilesLayout::AddOption(SelectFilesOptions option)
{
    SetOptions((SelectFilesOptions)(mOptions | option));
}

void SelectFilesLayout::RemoveOption(SelectFilesOptions option)
{
      SetOptions((SelectFilesOptions)(mOptions & (~option)));
}

bool SelectFilesLayout::CheckImageSizes()
{
    int width, height;
    for (int i=0; i<ui->tableImages->topLevelItemCount(); i++)
    {
        QTreeWidgetItem* twi =  ui->tableImages->topLevelItem(i);
        QStringList dtsize = twi->text(2).split("x");
        int swidth = dtsize[0].toInt();
        int sheight = dtsize[1].toInt();
        if (!i)
        {
           width = swidth;
           height = sheight;
        }
        else if (width != swidth || height != sheight)
        {
            QMessageBox::warning(this, "Invalid image sizes",
                                 "Not every image in the selected list are the same size.");
            return false;
        }
    }
    return true;
}

bool  SelectFilesLayout::CheckData(WorkFileList* wf)
{
     bool ret = true;

     wf->Clear();
     wf->setOverwrite((mOptions & SFO_ALLOW_OVERWRITE) && ui->checkOverwrite->isChecked());
     wf->setOutputDir(wf->overwrite() ? "/" :  ui->outputDirEdit->text());

     if (mOptions & SFO_REQUEST_SAME_SIZE)
     {
         ret = CheckImageSizes();
     }
     if ((mOptions & SFO_OUTPUT_FOLDER_MANDATORY)
        &&  !ui->outputDirEdit->text().trimmed().length() )
     {
         if (ret)
         QMessageBox::warning(this, "Invalid parameters",
                              "No output path selected. \n\n Please, choose an output path to save the results.");
         ret = false;
     }
     if ((mOptions & SFO_OUTPUT_FOLDER_MANDATORY)
        && !QDir(ui->outputDirEdit->text()).exists())
     {
         if (ret)
         QMessageBox::warning(this, "Invalid parameters",
                              "Output folder does not exits. \n\n Please, choose an existing path to save the results.");
         ret = false;
     }


     if (ui->radioCurrentFile->isChecked())
     {
         if (!MainWindow::mainwin->ActiveFitImage())
         {
             if (ret)
             QMessageBox::warning(this, "Invalid parameters",
                                        "No image active in workspace, please choose one.");
             ret = false;
         }
         else
         {
            wf->fitimages()->append(MainWindow::mainwin->ActiveFitImage()->fitimage());
            ret = true;
         }
     }
     else if (ui->radioCurrentFiles->isChecked())
     {
         if (!MainWindow::mainwin->SubWindowCount(MdiFitImage::staticMetaObject.className()))
         {
             if (ret)
             QMessageBox::warning(this, "Invalid parameters",
                                  "No image active in workspace, please choose one.");
         }
         else
         {             
             QList<QMdiSubWindow*> windows = MainWindow::mainwin->SubWindows( MdiFitImage::staticMetaObject.className() );
             for (int i=0; i<windows.count(); i++)
             {
                    MdiFitImage* fi =(MdiFitImage*) windows.at(i)->widget();
                    wf->fitimages()->append(fi->fitimage());
             }
         }
     }
     else if (ui->radioFileList->isChecked())
     {
         if (!count())
         {
             if (ret)
             QMessageBox::warning(this, "Invalid parameters",
                                        "No input files selected. \n\nPlease select files to process.");
             ret = false;
         }
         else
         {
             for (int i=0; i<count(); i++)
             {
                 QTreeWidgetItem* item = ui->tableImages->topLevelItem(i);
                 wf->filenames()->append(item->data(0, Qt::UserRole).toString());
             }
         }
     }
     else
     {
         if (ret)
         QMessageBox::warning(this, "Invalid parameters",
                          "Invalid parameters in input files tab");

     }


     if ((!ui->radioFileList->isChecked() && mMinImages > wf->fitimages()->count())
        ||
        (ui->radioFileList->isChecked() && mMinImages > wf->filenames()->count())
        )
     {
         if (ret)
         QMessageBox::warning(this,"Image count is not valid",
                              QString("At least %1 images are needed to perform this operation\n\n"
                                      "Please, select %1 or more images."
                                      ).arg(mMinImages));
         ret = false;
     }

     //Todos los archivos tienen reducción astrométrica
     if (ret && (mOptions & SFO_REQUEST_ASTROMETRIC))
     {
        if (ui->radioFileList->isChecked())
        {
            for (int i=0; i<wf->filenames()->count() && ret; i++)
            {
                FitHeaderList fhl = FitHeaderList::ReadFromFile(wf->filenames()->at(i));
                if (fhl.CheckFormat().length())
                {
                    QMessageBox::warning(this, "Invalid image header",
                                         QString("Invalid image header detected for <b>%1</b>\n\n"
                                                 "Unable to continue."
                                                 ).arg(QFileInfo(wf->filenames()->at(i)).fileName()));
                    ret = false;
                }
                else
                {
                    PlateConstants* pc = fhl.LookForPlate();
                    if (pc) delete pc;
                    else
                    {
                        QMessageBox::warning(this, "Couldn't find astrometic reduction",
                                             QString("The image <b>%1</b> has not a valid astrometric reduction.\n\n"
                                                     "Please, compute it before continue."
                                                     ).arg(QFileInfo(wf->filenames()->at(i)).fileName()));
                        ret = false;
                    }
                }
            }
        }
        else
        {
            for (int i=0; i<wf->fitimages()->count() && ret; i++)
            {
                if (!wf->fitimages()->at(i)->LookForPlateConstants())
                {
                    QMessageBox::warning(this, "No astrometric reduction found",
                                         QString("Some images in workspace are not astrometrically reduced.\n\n"
                                                 "Unable to continue."
                                                 ));
                    ret = false;
                }
            }
        }
     }

     return ret;
}

void SelectFilesLayout::RemoveSelected_Slot()
{
    QList<QTreeWidgetItem*> items =  ui->tableImages->selectedItems();
    for (int i=0;i<items.count(); i++)
        delete items.at(i);
    if (items.count() <= 1) mAnyFileHeaders.Clear();
    ChangeSelection_Slot();
}

int SelectFilesLayout::count()
{
    return ui->tableImages->topLevelItemCount();
}


void SelectFilesLayout::ChangeSelection_Slot()
{
    int cnt = ui->tableImages->selectedItems().count();
    ui->buttonRemoveFile->setEnabled(cnt);
    ui->buttonClearFiles->setEnabled(cnt || ui->tableImages->topLevelItemCount());
}

void SelectFilesLayout::keyPressEvent(QKeyEvent *ke)
{
    if (ke->key() == Qt::Key_Delete)
    {
       QList<QTreeWidgetItem*> qwi = ui->tableImages->selectedItems();
       for (int i=0; i<qwi.count(); i++)
           delete qwi.at(i);
    }
    else if (ke->key() == Qt::Key_Plus)
    {
        AddFiles_Slot();
    }
    ChangeSelection_Slot();
}

QStringList SelectFilesLayout::selectedFiles()
{
    QStringList lst;
    for (int i=0;i<ui->tableImages->topLevelItemCount(); i++)
    {
    QTreeWidgetItem* item = ui->tableImages->topLevelItem(i);
    lst.append(item->data(0, Qt::UserRole).toString());
    }
    return lst;
}

int  SelectFilesLayout::FN_ORDER_BY_NAME(const QString& filename_a,const QString& filename_b)
{
    return filename_a<filename_b;
}

QRadioButton*   SelectFilesLayout::radioCurrentFile()
{
    return ui->radioCurrentFile;
}

QRadioButton*   SelectFilesLayout::radioWorkspace()
{
    return ui->radioCurrentFiles;
}

QRadioButton*   SelectFilesLayout::radioDiskFiles()
{
    return ui->radioFileList;
}




