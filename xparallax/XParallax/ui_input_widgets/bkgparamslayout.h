#ifndef BKGPARAMSLAYOUT_H
#define BKGPARAMSLAYOUT_H

#include <QWidget>
#include "background.h"

namespace Ui {
class BkgParamsLayout;
}

class BkgParamsLayout : public QWidget
{
    Q_OBJECT
    
public:
    explicit BkgParamsLayout(QWidget *parent = 0);
    ~BkgParamsLayout();
    
    bool CheckData(BackgroundParameters* bp);
    void SetParameters(BackgroundParameters* bp);

private:
    Ui::BkgParamsLayout *ui;

public slots:
    void SetDefaults_Slot();
};

#endif // BKGPARAMSLAYOUT_H
