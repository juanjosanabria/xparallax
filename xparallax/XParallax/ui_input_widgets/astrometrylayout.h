#ifndef ASTROMETRYLAYOUT_H
#define ASTROMETRYLAYOUT_H

#include <QWidget>
#include "matcher.h"

namespace Ui {
class AstrometryLayout;
}

class AstrometryLayout : public QWidget
{
    Q_OBJECT
    
public:
    explicit AstrometryLayout(QWidget *parent = 0);
    ~AstrometryLayout();
    void SetParams(MatchParameters* par);
    bool CheckData(MatchParameters* mp);

private:
    Ui::AstrometryLayout *ui;


public slots:
    void SetDefaults_Slot();

};

#endif // ASTROMETRYLAYOUT_H
