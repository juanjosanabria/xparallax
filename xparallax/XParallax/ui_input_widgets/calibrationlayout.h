#ifndef CALIBRATIONLAYOUT_H
#define CALIBRATIONLAYOUT_H

#include <QWidget>
#include "calibration.h"

namespace Ui {
class CalibrationLayout;
}

class CalibrationLayout : public QWidget
{
    Q_OBJECT

public:
    explicit CalibrationLayout(QWidget *parent = 0);
    ~CalibrationLayout();    
    bool CheckData(CalibrationParameters* cp);
    void SetParameters(CalibrationParameters* cp);

private:
    Ui::CalibrationLayout *ui;

public slots:
    void ChangeRejectAlgorithm_Slot(int newindex);
    void SetDefaults_Slot();


};

#endif // CALIBRATIONLAYOUT_H
