#ifndef SELECTFILESLAYOUT_H
#define SELECTFILESLAYOUT_H

#include <QWidget>
#include <QTreeWidgetItem>
#include <QRadioButton>
#include "progress.h"

namespace Ui {
class SelectFilesLayout;
}

class SelectFilesLayout : public QWidget
{
    Q_OBJECT

public:

enum  SelectFilesOptions
{
    SFO_ALLOW_CURRENT_FILE = (1<<1),        //Permitir el archivo actualmente abierto
    SFO_ALLOW_ALL_FILES = (1<<2),           //Permitir todos los archivos del workspace
    SFO_ALLOW_DISK_FILES = (1<<3),          //Permitir seleccionar archivos de disco

    SFO_ALLOW_OVERWRITE = (1<<4),           //Permitir la selección de sobreescribir
    SFO_OVERWRITE_CHECKED = (1<<5),
    SFO_REQUEST_OUTPUT_DIR = (1<<6),
    SFO_OUTPUT_FOLDER_MANDATORY = (1<<7),
    SFO_OUTPUT_ALLOWED = (1<<8),            //Se mostrarán opciones

    SFO_REQUEST_SAME_SIZE = (1<<9),         //Todas las imágenes deben tener el mismo tamaño
    SFO_REQUEST_ASTROMETRIC = (1<<10)       //Solamente se permiten imágenes con reducción astrométrica

};

public:
    explicit SelectFilesLayout(QWidget *parent = 0, SelectFilesOptions options = SelectFilesLayout::DEFAULT_OPTIONS);
    ~SelectFilesLayout();

    QStringList         selectedFiles();
    QString             outputDir(){return mOutputDir;}
    bool                overWrite(){return mOverwrite;}
    bool                CheckData(WorkFileList* wf);
    bool                CheckImageSizes();
    void                AddOption(SelectFilesOptions option);
    void                RemoveOption(SelectFilesOptions option);
    SelectFilesOptions  options(){return mOptions;}
    int                 count();
    void                setMinImages(int min){mMinImages = min;}

    //Funciones de selección y formateo de las imágenes
    static QString (*          FN_GET_ITEM_IMAGE)(FitHeaderList* fh,const QString& filename);
    static QString (*        FN_GET_ITEM_TOOLTIP)(FitHeaderList* fh,const QString& filename);
    static int (*               FN_GET_ITEM_SORT)(const QString& filename_a,const QString& filename_b);

    //Acceso a los controles internos
    QRadioButton*   radioCurrentFile();
    QRadioButton*   radioWorkspace();
    QRadioButton*   radioDiskFiles();

protected:
    void keyPressEvent(QKeyEvent *);

private:
    Ui::SelectFilesLayout *ui;
    bool                   mOverwrite;
    QString                mOutputDir;
    SelectFilesOptions     mOptions;
    FitHeaderList          mAnyFileHeaders;
    int                    mMinImages;
    bool                   mFitSelected;
    int                    mFitcount;
    QString                mLastFilePath;
    QString                mLastOutputPath;

    static SelectFilesOptions DEFAULT_OPTIONS;
    static bool SelectedFile_greaterThan(QTreeWidgetItem *a, QTreeWidgetItem *b);
    static int  FN_ORDER_BY_NAME(const QString& filename_a,const QString& filename_b);

public:
    bool ContainsFile(const QString& path);
    void SetOptions(SelectFilesOptions options, bool auto_select_input=true);
    void SetLastDirs(QString filePath, QString outputPath){mLastFilePath=filePath; mLastOutputPath=outputPath;}

    //Busca en las opciones seleccionadas actualmente para localizar en alguna imagen
    //el centro y emitir las señales correspondientes
    void LookforImageInformation();

public slots:
    void AddFiles_Slot();
    void SelectOutputDir_Slot();
    void CheckOverWrite_Slot(bool ow);
    void RemoveSelected_Slot();
    void SetSelCurrentImage_Slot(bool act=true);          //Establece la selección en la imagen actual
    void SetSelCurrentImages_Slot(bool act=true);         //Establece la selección en todas las imágenes
    void SetSelFiles_Slot(bool act=true);                 //Establece la selección en imágenes de disco
    void ChangeSelection_Slot();
    void ClearFiles_Slot();

signals:
    void RADECDetected_Signal(double ra, double lat);

};

#endif // SELECTFILESLAYOUT_H
