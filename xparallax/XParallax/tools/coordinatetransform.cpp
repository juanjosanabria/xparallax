#include "coordinatetransform.h"
#include "../mainwindow.h"
#include "ui_coordinatetransform.h"
#include "xglobal.h"
#include "util.h"
#include "novas.h"

const char* CoordinateTransformDialog::TOOL_EPOCH_CONVERTER_INI = "tools.ini";
const char* CoordinateTransformDialog::TOOL_CFG_SECTION = "COORDINATE_TRANSFORM";

CoordinateTransformParameters::CoordinateTransformParameters()
{
    SetDefaults();
}

CoordinateTransformParameters::~CoordinateTransformParameters()
{

}

void CoordinateTransformParameters::SetDefaults()
{
    input_system = CoordinateSystem::EQUATORIAL;
    input_equinox = 2000.0f;
    input_ra = input_dec = INVALID_FLOAT;
    output_system = CoordinateSystem::EQUATORIAL;
    output_equinox = 2000.0f;
    angle_format = AngleFormat::SEXAGES;
    precission = 1;
}

void CoordinateTransformParameters::Save(QSettings* settings,const QString& section)
{
    SaveInt(settings, section, "input_system", (int)input_system);
    SaveFloat(settings, section, "input_equinox", input_equinox);
    SaveDouble(settings, section, "input_ra", input_ra);
    SaveDouble(settings, section, "input_dec", input_dec);
    SaveInt(settings, section, "output_system", output_system);
    SaveFloat(settings, section, "output_equinox", (int)output_equinox);
    SaveInt(settings, section, "angle_format", (int)angle_format);
    SaveInt(settings, section, "precission", precission);
}

void CoordinateTransformParameters::Load(QSettings* settings,const QString& section)
{
    input_system = (CoordinateSystem) ReadInt(settings, section, "input_system", (int)CoordinateSystem::EQUATORIAL);
    input_equinox = ReadFloat(settings, section, "input_equinox", 2000.0f);
    input_ra = ReadDouble(settings, section, "input_ra", INVALID_FLOAT);
    input_dec = ReadDouble(settings, section, "input_dec", INVALID_FLOAT);
    output_system = (CoordinateSystem) ReadInt(settings, section, "output_system", (int)CoordinateSystem::EQUATORIAL);
    output_equinox = ReadFloat(settings, section, "output_equinox", 2000.0f);
    angle_format = (AngleFormat) ReadInt(settings, section, "angle_format", (int)AngleFormat::SEXAGES);
    precission = ReadInt(settings, section, "precission", 1);
    //Comprobar valores dentro de límites
    if (input_system < CoordinateSystem::EQUATORIAL || input_system > CoordinateSystem::GALACTIC)
        input_system = CoordinateSystem::EQUATORIAL;
    if (output_system < CoordinateSystem::EQUATORIAL || output_system > CoordinateSystem::GALACTIC)
        output_system = CoordinateSystem::EQUATORIAL;
    if (angle_format < AngleFormat::SEXAGES || angle_format > AngleFormat::DECIMAL)
        angle_format = AngleFormat::SEXAGES;
    if (angle_format == AngleFormat::SEXAGES && precission > 3) precission = 3;
}


CoordinateTransformDialog::CoordinateTransformDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EpochConverterDialog)
{
    ui->setupUi(this);
    mRAFormatStr = "%02d %02d %05.2f";
    mDECFormatStr = "%c%02d %02d %04.1f";

    MainWindow::mainwin->NoModalAdd(this);
    mRA1 = mDEC1 = INVALID_FLOAT;
    //Cargar datos del ini
    QSettings* sett = XGlobal::GetSettings(TOOL_EPOCH_CONVERTER_INI);
    mEParameters = new CoordinateTransformParameters();
    mEParameters->Load(sett, TOOL_CFG_SECTION);
    delete sett;
    mInternalChange = true;
    LoadParamsToUI();
    mInternalChange = false;
    Compute_Slot();

    //Cambios en coordenadas de entrada
    connect(ui->editRA_1, &QLineEdit::textChanged, this, [=]{ Compute_Slot();});
    connect(ui->editDEC_1, &QLineEdit::textChanged, this, [=]{ Compute_Slot(); });
    connect(ui->comboInputSystem, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [=]{ Compute_Slot(); });
    connect(ui->comboOutputFormat, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [=]{ Compute_Slot(); });
    connect(ui->comboOutputSystem, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [=]{ Compute_Slot(); });
    connect(ui->editEquinox_1, &QLineEdit::textChanged, this, [=]{ Compute_Slot(); });
    connect(ui->editEquinox_2, &QLineEdit::textChanged, this, [=]{ Compute_Slot(); });
    connect(ui->spinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, [=]{ Compute_Slot(); });

    MainWindow::mainwin->DPIAware(this, true);
}

CoordinateTransformDialog::~CoordinateTransformDialog()
{
    if (mEParameters)
    {
        QSettings* sett = XGlobal::GetSettings(TOOL_EPOCH_CONVERTER_INI);
        mEParameters->Save(sett, TOOL_CFG_SECTION);
        delete sett;
        delete mEParameters;
    }
    mEParameters =  NULL;

    delete ui;
}

void CoordinateTransformDialog::LoadParamsToUI()
{
    switch(mEParameters->input_system)
    {
        case CoordinateTransformParameters::CoordinateSystem::ECLIPTIC:
            ui->labelRA_1->setText("LON (deg):");
            ui->comboInputSystem->setCurrentIndex(1);
            break;
        case CoordinateTransformParameters::CoordinateSystem::GALACTIC:
            ui->labelRA_1->setText("LON (deg):");
            ui->comboInputSystem->setCurrentIndex(2);
            break;
        default:
            ui->labelRA_1->setText("RA (hours):");
            ui->comboInputSystem->setCurrentIndex(0);
            break;
    }
    switch(mEParameters->output_system)
    {
        case CoordinateTransformParameters::CoordinateSystem::ECLIPTIC:
            ui->labelDEC_2->setText("LAT (deg):");
            ui->comboOutputSystem->setCurrentIndex(1);
            break;
        case CoordinateTransformParameters::CoordinateSystem::GALACTIC:
            ui->labelDEC_2->setText("LAT (deg):");
            ui->comboOutputSystem->setCurrentIndex(2);
            break;
        default:
            ui->labelDEC_1->setText("DEC (deg):");
            ui->comboOutputSystem->setCurrentIndex(0);
            break;
    }
    ui->editEquinox_1->setText(Util::Equinox2String(mEParameters->input_equinox));
    ui->editEquinox_2->setText(Util::Equinox2String(mEParameters->output_equinox));
    ui->comboOutputFormat->setCurrentIndex(mEParameters->angle_format ==  CoordinateTransformParameters::AngleFormat::SEXAGES ? 0 : 1);

    if (mEParameters->angle_format == CoordinateTransformParameters::AngleFormat::SEXAGES)
    {
        ui->spinBox->setMinimum(1);
        ui->spinBox->setMaximum(3);
    }
    else
    {
        ui->spinBox->setMinimum(1);
        ui->spinBox->setMaximum(10);
    }
    ui->spinBox->setValue(mEParameters->precission);
    if (mEParameters->input_system == CoordinateTransformParameters::CoordinateSystem::EQUATORIAL)
        ui->editRA_1->setText(IS_VALIDF(mEParameters->input_ra) ? Util::RA2String(mEParameters->input_ra, mEParameters->precission+1) : "");
    else
        ui->editRA_1->setText(IS_VALIDF(mEParameters->input_ra) ? Util::DEC2String(mEParameters->input_ra, mEParameters->precission+1) : "");
    ui->editDEC_1->setText(IS_VALIDF(mEParameters->input_dec) ? Util::DEC2String(mEParameters->input_dec, mEParameters->precission) : "");
}

void CoordinateTransformDialog::SetValid(bool valid)
{
    ui->editRA_2->setEnabled(valid);
    ui->editDEC_2->setEnabled(valid);
    //ui->buttonCopyOutCoordinate->setEnabled(valid);
}


QString CoordinateTransformDialog::FmtFloat(double f, int decimals)
{
    char cad[16];
    if (decimals == 0) return QString("").sprintf("%d",(int)f);
    //Calcular tamño de la parte entera
    int pent_tam = QString("").sprintf("%d",floor(f)).length();
    sprintf(cad,"%%0%d.%df",pent_tam+decimals+1,decimals);
    return QString("").sprintf(cad, f);
}


void CoordinateTransformDialog::Compute_Slot()
{
    //Inicializaciones y valores intermedios
    if (mInternalChange) return;
    bool ok = false;
    SetValid(false);

    //Precisión y formato de la precisión
    mEParameters->precission = ui->spinBox->value();

    //Control del pegado de coordenadas completas en alguno de los cuadros de RA o DEC
    QVector<double> vec = Util::ParseRADEC(ui->editRA_1->text());
    if (vec.length() == IS_VALIDF(vec[0]) && IS_VALIDF(vec[1]))
    {
        mInternalChange = true;
        ui->editRA_1->setText(Util::RA2String(vec[0], mEParameters->precission+1));
        ui->editDEC_1->setText(Util::DEC2String(vec[1], mEParameters->precission));
        mInternalChange = false;
    }
    vec = Util::ParseRADEC(ui->editDEC_1->text());
    if (vec.length() == IS_VALIDF(vec[0]) && IS_VALIDF(vec[1]))
    {
        mInternalChange = true;
        ui->editRA_1->setText(Util::RA2String(vec[0], mEParameters->precission+1));
        ui->editDEC_1->setText(Util::DEC2String(vec[1], mEParameters->precission));
        mInternalChange = false;
    }

    //Cuadro de ascensión recta/longitud
    if (mEParameters->input_system == CoordinateTransformParameters::CoordinateSystem::EQUATORIAL)
        mEParameters->input_ra = Util::ParseRA(ui->editRA_1->text());
    else
        mEParameters->input_ra = Util::ParseDEC(ui->editRA_1->text());
    if (IS_INVALIDF(mEParameters->input_ra))
    {
        mEParameters->input_ra = ui->editRA_1->text().toDouble(&ok);
        if (!ok) mEParameters->input_ra = INVALID_FLOAT;
        if (IS_VALIDF(mEParameters->input_ra))
        {
             if (mEParameters->input_system == CoordinateTransformParameters::CoordinateSystem::EQUATORIAL &&
                (mEParameters->input_ra > 24 || mEParameters->input_ra < 0)) mEParameters->input_ra = INVALID_FLOAT;
             if (mEParameters->input_system != CoordinateTransformParameters::CoordinateSystem::EQUATORIAL &&
                   mEParameters->input_ra > 180) mEParameters->input_ra = INVALID_FLOAT;
        }
    }
    ui->imgRA_1->setPixmap(IS_INVALIDF(mEParameters->input_ra) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png"));
    //Cuadro de declinación/longitud
    mEParameters->input_dec = Util::ParseDEC(ui->editDEC_1->text());
    if (IS_INVALIDF(mEParameters->input_dec))
    {
        mEParameters->input_dec = ui->editDEC_1->text().toDouble(&ok);
        if (!ok) mEParameters->input_dec = INVALID_FLOAT;
        if (IS_VALIDF(mEParameters->input_dec) && mEParameters->input_dec < -90 && mEParameters->input_dec > 90)
              mEParameters->input_dec = INVALID_FLOAT;
    }
    ui->imgDEC_1->setPixmap(IS_INVALIDF(mEParameters->input_dec) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png"));


    //Épocas de entrada y de salida
    mEParameters->input_equinox = Util::ParseEquinox(ui->editEquinox_1->text());
    ui->imgEquinox_1->setPixmap(IS_INVALIDF(mEParameters->input_equinox) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png"));
    mEParameters->output_equinox = Util::ParseEquinox(ui->editEquinox_2->text());
    ui->imgEquinox_2->setPixmap(IS_INVALIDF(mEParameters->output_equinox) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png"));


    //Etiquetas de los cuadros de coordenadas
    mEParameters->input_system = ui->comboInputSystem->currentIndex()==0?CoordinateTransformParameters::CoordinateSystem::EQUATORIAL:
                                 (ui->comboInputSystem->currentIndex()==1?CoordinateTransformParameters::CoordinateSystem::ECLIPTIC:
                                 CoordinateTransformParameters::CoordinateSystem::GALACTIC);
    switch(mEParameters->input_system)
    {
        case CoordinateTransformParameters::CoordinateSystem::ECLIPTIC:
        case CoordinateTransformParameters::CoordinateSystem::GALACTIC:
            ui->labelRA_1->setText("LON (deg):");
            break;
        default:
            ui->labelRA_1->setText("RA (hours):");
            break;
    }
    mEParameters->output_system = ui->comboOutputSystem->currentIndex()==0?CoordinateTransformParameters::CoordinateSystem::EQUATORIAL:
                                 (ui->comboOutputSystem->currentIndex()==1?CoordinateTransformParameters::CoordinateSystem::ECLIPTIC:
                                 CoordinateTransformParameters::CoordinateSystem::GALACTIC);
    switch(mEParameters->output_system)
    {
        case CoordinateTransformParameters::CoordinateSystem::ECLIPTIC:
        case CoordinateTransformParameters::CoordinateSystem::GALACTIC:
            ui->labelRA_2->setText("LAT (deg):");
        default:
            ui->labelDEC_2->setText("DEC (deg):");
            ui->comboOutputSystem->setCurrentIndex(0);
            break;
    }

    bool valid = IS_VALIDF(mEParameters->input_ra) && IS_VALIDF(mEParameters->input_dec) && IS_VALIDF(mEParameters->input_equinox)
                && IS_VALIDF(mEParameters->output_equinox) && (mEParameters->input_system >= 1 && mEParameters->input_system <= 3)
                && (mEParameters->output_system >= 1 && mEParameters->output_system <= 3);

    //Realizar los cálculos
    if (valid)
    {
        //


    }
    SetValid(valid);
}

void CoordinateTransformDialog::CopyResult_Slot()
{
    /*
    if (IS_INVALIDF(mRA1) || IS_INVALIDF(mDEC1) || IS_INVALIDF(mRA2) || IS_INVALIDF(mDEC2)) return;
     QApplication::clipboard()->setText(Util::RA2String(mRA2, ui->spinPrecission->value()+1) + " " + Util::DEC2String(mDEC2, ui->spinPrecission->value()));
*/
}


void CoordinateTransformDialog::on_buttonOK_accepted()
{
    this->close();
}
