#ifndef COORDTRANSFORMERDIALOG_H
#define COORDTRANSFORMERDIALOG_H

#include <QDialog>
#include <QSettings>
#include "core.h"


//https://ned.ipac.caltech.edu/forms/calculator.html

namespace Ui {
class EpochConverterDialog;
}

class CoordinateTransformParameters: Parameters
{
public:
    enum CoordinateSystem { EQUATORIAL = 1, HORIZONTAL = 2, ECLIPTIC = 3, GALACTIC = 4 };
    enum AngleFormat      { SEXAGES = 1, DECIMAL = 2};

public:
    CoordinateTransformParameters();
    ~CoordinateTransformParameters();
    void SetDefaults();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);

    CoordinateSystem    input_system;
    float               input_equinox;
    double              input_ra;
    double              input_dec;
    CoordinateSystem    output_system;
    float               output_equinox;
    AngleFormat         angle_format;
    int                 precission;

};

class CoordinateTransformDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CoordinateTransformDialog(QWidget *parent = 0);
    ~CoordinateTransformDialog();

private:
    Ui::EpochConverterDialog *ui;

    CoordinateTransformParameters*   mEParameters;

    bool        mInternalChange;
    double      mRA1;                                       //Ascensión recta en horas o longitud en grados
    double      mDEC1;                                      //Declinación o latitud. Siempre en grados
    double      mDEC2;                                      //Ascensión recta en horas o longitud en grados
    double      mRA2;                                       //Declinación o latitud. Siempre en grados
    QString     mRAFormatStr;                               //
    QString     mDECFormatStr;

    void LoadParamsToUI();                                  //Carga los parámetros a la interfaz de usuario

    void SetValid(bool valid = false);
    static QString FmtFloat(double f, int decimals);        //Formatea un double a un string con el número de decimales indicado
    static const char* TOOL_EPOCH_CONVERTER_INI;
    static const char* TOOL_CFG_SECTION;

private slots:
    void Compute_Slot();
    void CopyResult_Slot();

    void on_buttonOK_accepted();
};

#endif // COORDTRANSFORMERDIALOG_H
