#ifndef ANGULARDISTANCE_H
#define ANGULARDISTANCE_H

#include <QDialog>

namespace Ui {
class AngularDistanceDialog;
}

class AngularDistanceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AngularDistanceDialog(QWidget *parent = 0);
    ~AngularDistanceDialog();

private:
    Ui::AngularDistanceDialog *ui;

    double mRA1;
    double mDEC1;
    double mRA2;
    double mDEC2;
    bool   mInternalChange;

private slots:
    void Compute_Slot();

};

#endif // ANGULARDISTANCE_H
