#include "angulardistance.h"
#include "../mainwindow.h"
#include "ui_angulardistance.h"

AngularDistanceDialog::AngularDistanceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AngularDistanceDialog)
{
    ui->setupUi(this);
    MainWindow::mainwin->NoModalAdd(this);
    mRA1 = mDEC1 = mRA2 = mDEC2 = INVALID_FLOAT;
    mInternalChange = false;

    connect(ui->editRA_1, &QLineEdit::textChanged, this,  [=]{ Compute_Slot(); });
    connect(ui->editDEC_1, &QLineEdit::textChanged, this, [=]{ Compute_Slot(); });
    connect(ui->editRA_2, &QLineEdit::textChanged, this,  [=]{ Compute_Slot(); });
    connect(ui->editDEC_2, &QLineEdit::textChanged, this, [=]{ Compute_Slot(); });

    MainWindow::mainwin->DPIAware(this, true);
}


AngularDistanceDialog::~AngularDistanceDialog()
{
    delete ui;
}


void AngularDistanceDialog::Compute_Slot()
{
    if (mInternalChange) return;
    double ra1, dec1, ra2, dec2;

    //Solicitamos el parseo total capturando el número de lugares decimales
    QVector<double> radec = Util::ParseRADEC(ui->editRA_1->text(), true);
    if (radec.count() != 4 || !IS_VALIDF(radec[0]) || !IS_VALIDF(radec[1]))
        radec = Util::ParseRADEC(ui->editDEC_1->text(), true);
    if (radec.count() == 4 && IS_VALIDF(radec[0]) && IS_VALIDF(radec[1]))
    {
        mInternalChange = true;
        ui->editRA_1->setText(Util::RA2String(radec[0], (int)radec[2]));
        ui->editDEC_1->setText(Util::DEC2String(radec[1], (int)radec[3]));
        mInternalChange = false;
        ra1 = radec[0];
        dec1 = radec[1];
    }
    else
    {
        ra1 = Util::ParseRA(ui->editRA_1->text());
        dec1 = Util::ParseDEC(ui->editDEC_1->text());
    }

    radec = Util::ParseRADEC(ui->editRA_2->text(), true);
    if (radec.count() != 4 || !IS_VALIDF(radec[0]) || !IS_VALIDF(radec[1]))
        radec = Util::ParseRADEC(ui->editDEC_2->text(), true);
    if (radec.count() == 4 && IS_VALIDF(radec[0]) && IS_VALIDF(radec[1]))
    {
        mInternalChange = true;
        ui->editRA_2->setText(Util::RA2String(radec[0], (int)radec[2]));
        ui->editDEC_2->setText(Util::DEC2String(radec[1], (int)radec[3]));
        mInternalChange = false;
        ra2 = radec[0];
        dec2 = radec[1];
    }
    else
    {
        ra2 = Util::ParseRA(ui->editRA_2->text());
        dec2 = Util::ParseDEC(ui->editDEC_2->text());
    }

    if (IS_INVALIDF(ra1) ^ IS_INVALIDF(mRA1))
       ui->imgRA_1->setPixmap(isnan((ra1)) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png") );
    if (IS_INVALIDF(dec1) ^ IS_INVALIDF(mDEC1))
       ui->imgDEC_1->setPixmap(isnan((dec1)) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png") );
    if (IS_INVALIDF(ra2) ^ IS_INVALIDF(mRA2))
       ui->imgRA_2->setPixmap(isnan((ra2)) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png") );
    if (IS_INVALIDF(dec2) ^ IS_INVALIDF(mDEC2))
       ui->imgDEC_2->setPixmap(isnan((dec2)) ? QPixmap(":/graph/images/icn_cross.svg") : QPixmap(":/graph/images/tick-16.png") );

    mRA1 = ra1; mDEC1 = dec1;
    mRA2 = ra2; mDEC2 = dec2;
    if (IS_INVALIDF(mRA1) || IS_INVALIDF(mDEC1) || IS_INVALIDF(mRA2) || IS_INVALIDF(mDEC2))
    {
        ui->editAngle->setText("");
        ui->editDistance->setText("");
    }
    else
    {
        double dist_deg = Util::Adist(mRA1, mDEC1, mRA2, mDEC2);
        double dist_min = dist_deg * 60;
        double dist_asec = dist_deg * 3600;
        if (dist_asec < 60)
             ui->editDistance->setText(QString("").sprintf("%0.3f\"", dist_asec));
        else if (dist_min < 60)
            ui->editDistance->setText(QString("").sprintf("%02d' %05.3f\"", (int)floor(dist_min),
                                                          (dist_min-floor(dist_min))*60 ));
        else
            ui->editDistance->setText(QString("").sprintf("%02dº %02d' %05.3f\"",
                                                          (int)floor(dist_deg),
                                                          (int)floor(dist_min = (dist_deg-floor(dist_deg))*60),
                                                          (dist_min - floor(dist_min))*60 ));
        double angle = Util::PosAngle(mRA1, mDEC1, mRA2, mDEC2);
        ui->editAngle->setText(QString("").sprintf("%0.3fº",angle));
    }
}










