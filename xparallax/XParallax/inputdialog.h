#ifndef INPUTDIALOG_H
#define INPUTDIALOG_H

#include "core.h"
#include "vo.h"
#include "ui_helpers/animatedlabel.h"
#include <QDialog>
#include <QPlainTextEdit>
#include <QComboBox>
#include <QLabel>
#include <QTimer>
#include <QPushButton>
#include <QToolButton>
#include <QFormLayout>

namespace Ui {
class InputDialog;
}

#define BASE_INPUTDIALOG_WIDTH  300

class InputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InputDialog(const QString& title, QWidget *parent);
    explicit InputDialog(QWidget *parent = 0);
    ~InputDialog();

    void SetTitle(const QString& title);
    void SetSubTitle(const QString& subtitle);
    void SetImage(const QString& resource);
    void SetImage(const QPixmap& resource);
    void SetImageBehaivour(QString bh);

    //Operaciones a realizar cuando se muestre el diálogo
    void showEvent(QShowEvent* ev);

    //Controles previos de aceptar o cancelar controlados
    void accept();
    void reject();

    //Objeto seleccionado del servicio SESSAME VO
    NameRecord& nameRec(){return mNameRecord;}

    //Texto de la etiqueta
    QString objectName();

    //Adaptado a DPIAWARE
    QString ShowText(const QString& label,const QString& def,const QString& inputMask = QString(""));

    //Adaptado a DPIAWARE
    QString ShowBlob(const QString& label,const QString& def);

    //Adaptado a DPIAWARE
    QVariant ShowList(const QString& caption, QStringList labels, QList<QVariant> values = QList<QVariant>(), QVariant def="");

    //Adaptado a DPIAWARE
    QStringList ShowMultipleTexts(const QStringList& titles, const QStringList& values, const QStringList& inputmask = QStringList());

    //Adaptado a DPIAWARE
    QVector<double> ShowRADec(double def_ra = INVALID_FLOAT, double def_dec = INVALID_FLOAT, QString objname="");

    //Adaptado a DPIAWARE
    QStringList  ShowTextAndBlob(const QString& title_text, const QString& def_text,const QString& title_blob,const QString& def_blob);


private:
    Ui::InputDialog *ui;

    //Para la opción de lectura de coordenadas ecuatoriales
    int                 mType;
    double              mDefRA;            //Ascensión recta por defecto
    double              mDefDEC;           //Declinación por defecto
    QWidget*            mFocusWidget;      //Widget que tendrá el foco una vez se muestre el diálogo
    QLineEdit*          mRALine;           //Línea de lectura de ascensión recta
    QLineEdit*          mDECLine;          //Línea de lectura de declinación
    NameRecord          mNameRecord;       //Nombre del objeto leído del servicio de SESSAME
    QLabel*             mRaImage;          //Imagen a la derecha de RA
    QLabel*             mDecImage;         //Imagen a la derecha de DEC
    QLineEdit*          mSearchEdit;       //Etiqueta de búsqueda
    bool                mSearchModified;   //Indica si el cuadro de búsqueda ha sido modificado
    AnimatedLabel*      mAnimatedLabel;    //Etiqueta con animación de búsqueda
    QToolButton *       mSearchBtn;        //Botón de búsqueda (RA-DEC)
    SesameReader        mSesameReader;     //Lector de SESSAME
    QLabel*             mObjNameLabel;     //Nombre del objeto leído (etiqueta)
    QFormLayout*        mFormLayout;       //FormLayout cuando sea de este tipo
    QStringList         mImageBehaivour;   //Comportamiento de imagen 0=>"Comet;:imgcometresource;Satellite:imgsatelliteresource";

public slots:
    void UpdateRaDecStatus();
    void RequestSearch_Slot();

    void SesameStart_Slot();
    void SesameEnd_Slot(NameRecord& nr);
    /**
     * @brief ActivateDeactivateEdit_Slot Activa o desactiva un input de entrada condicional. Un input de entrada condicional se
     * activará/desactivará según el valor de otro input. COMBO:1,A,B,C:Value1,Value2,Value3
     * En el ejemplo anterior, el combo se activará condicionalmente cuando el campo 1, valga A,B,C
     * Si por el contrario no se desea desactivar sino ocultar. Usar /. Ej: COMBO:/1,A,B,C:Value1,Value2,Value3
     * @param base Elemento a activar o desactivar
     * @param trigger Desencadentante de la acción
     * @param behaivour Comportamiento. En el caso anterior sería 1,A,B,C
     * @param list Lista de los inputs de entrada del formulario
     */
    void ActivateDeactivateEdit_Slot(int rowindex, QWidget* base,QLabel* label, QWidget* trigger, QString behaivour, QList<QWidget*>* list,bool occult=false);

};



#endif // INPUTDIALOG_H
