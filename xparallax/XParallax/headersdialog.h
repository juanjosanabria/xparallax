#ifndef HEADERSDIALOG_H
#define HEADERSDIALOG_H

#include <QDialog>
#include <QModelIndex>
#include <QTreeWidgetItem>
#include "fitimage.h"

namespace Ui {
class HeadersDialog;
}

class HeadersDialog : public QDialog
{
    Q_OBJECT
    
public:
    //Constructores, destructores y otros métodos públicos
    //***********************************************************************************************************************************
    explicit HeadersDialog(QWidget *parent = 0);
    ~HeadersDialog();
    void SetHeaders(FitHeaderList* headers);
    

private:
    Ui::HeadersDialog *     ui;
    FitHeaderList*          mHeaders;
    QList<QTreeWidgetItem*> mMarkedItems;
    QList<QBrush>           mMarkedBrushes;
    QList<int>              mAddedHeaders;

    //Funciones usadas internamente por el diálogo
    //***********************************************************************************************************************************
    bool InList(const char *list[],const QString& str);     /** Comprueba si una cabecera se encuentra en una lista terminada en NULL */
    void DeleteAllHeaders(const char *list[]);              /** Elimina todas las cabeceras que aparezcan en la lista terminada en NULL */
    bool IsModified();
    void ClearSearch(bool clear_box=false);
    void FindAndMarkItems(const QString &_txt,int from_idx);
    void SetItemColor(QTreeWidgetItem* it, const QString& hdr);
    FitHeader* ExistsHeader(const QString& name);

    //Funciones virtuales sobrecargadas
    //***********************************************************************************************************************************
    virtual void accept();
    virtual void reject();

public slots:
    void DoubleClick_Slot(QModelIndex idx);
    void AddHeader_Slot();
    void DeleteHeader_Slot();
    void MoveUp_Slot();
    void MoveDown_Slot();
    void ChangeHeaderIndex_Slot(QTreeWidgetItem* new_item, QTreeWidgetItem* old_item);


private slots:
    void on_editFind_textChanged(const QString &arg1);
    void on_buttonNext_clicked();

};

#endif // HEADERSDIALOG_H
