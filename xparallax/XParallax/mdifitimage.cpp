#include "mdifitimage.h"
#include "fitimage.h"
#include "core.h"
#include "mainwindow.h"
#include "headersdialog.h"
#include "projection.h"
#include "calibration.h"
#include "astrometry.h"
#include "imgtools/ximgtoolstretch.h"
#include "imgtools/ximgtoollocate.h"
#include "imgtools/ximgtoollinearprofile.h"
#include "imgtools/ximgtooldistance.h"
#include "imgtools/ximgtoolmpcaddmeasure.h"
#include "imgtools/ximgtoolgif.h"
#include "ui_helpers/progressdialog.h"

#define     MAX_SCALE       40.0f
#define     SAVE_FMT_FIT    "fit astronomy file (*.fit)"
#define     SAVE_FMT_FITS   "fits astronomy file (*.fits)"
#define     SAVE_FMT_JPG    "jpeg image (*.jpg)"
#define     SAVE_FMT_PNG    "png image (*.png)"
#define     SAVE_FIT_FMTS   SAVE_FMT_FIT ";;" SAVE_FMT_FITS ";;" SAVE_FMT_JPG ";;" SAVE_FMT_PNG
#define     EXPORT_VIEW_FMT SAVE_FMT_JPG ";;" SAVE_FMT_PNG
#define     SZ_SEG_ORIENT   (30*XGlobal::SCREEN_DPI)
#define     SZ_ARROW_ORIENT (5*XGlobal::SCREEN_DPI)

MdiFitImage::MdiFitImage(QWidget *parent,const QString& filename)
    : QWidget(parent)
{
    Init(FitImage::OpenFile(filename, &mErrorMsg));
}

MdiFitImage::MdiFitImage(QWidget *parent, FitImage* fimage)
    : QWidget(parent)
{
    Init(fimage);
}


void MdiFitImage::Init(FitImage* fimage)
{
    initExtensionInterface();
    mCreating = true;
    mMdiSubWindow = NULL;
    mConvergeProfile = NULL;
    mPlate = NULL;
    mMeshAction = NULL;
    mMeshLayer = mAuxLayer = NULL;
    mFileDownloader = NULL;
    mKnownObjectsLayer = NULL;
    mMpcKnownObjectPd = NULL;
    mFitImage = fimage;
    mWheelSpeed = 0.2f;
    setMouseTracking(true);
    setContextMenuPolicy(Qt::CustomContextMenu);

    if (mFitImage && mFitImage->error())
    {
        mErrorMsg = mFitImage->errorMsg();
        delete mFitImage;
        mFitImage = NULL;
    }
    if (mFitImage)
    {
        setObjectName(QFileInfo(fimage->filename()).fileName());
        mImage = QImage(mFitImage->width(),mFitImage->height(),QImage::Format_Indexed8);
        //Inicializamos la tabla de escala de grises
        QVector<QRgb> colortable(256);
        for (int i=0; i< 256;i++)
            colortable[i] = QColor(i, i, i).rgb();
        mImage.setColorTable(colortable);

        mBackground = QBrush(QColor(0x33, 0x33, 0x33));
        //mBackground.setStyle(Qt::FDiagPattern);
        mStarGround = QBrush(QColor(0xff, 0xff, 0xff));

        mConvergeProfile = new ConvergeProfile();
        mConvergeProfile->SetOptimumFor(mFitImage);
        mConvergeProfile->CreateColorTable();
        mConvergeProfile->Converge(&mImage);
        mScale = 1;
        mImageCenter.setX(mFitImage->width()/2.0f);
        mImageCenter.setY(mFitImage->height()/2.0f);

        //Calculamos los dígitos significativos del fondo de la imagen
        mSignificantDigits = 0;
        mScaleFactor = 1;
        float avgs = mFitImage->averagepix() + 3.0f * mFitImage->stdevpix();
        if (avgs < 1) avgs = 1;
        if (avgs < 10000)
        {
            while (avgs < 10000)
            {
                avgs *= 10.0f;
                mScaleFactor *= 10.0f;
                mSignificantDigits++;
            }
        }
        else
        {
            while (avgs >= 65535)
            {
                avgs /= 10.0f;
                mScaleFactor /= 10.0f;
                mSignificantDigits--;
            }
        }
        //Con factores de escala entre -1 y +1 no haremos nada puesto que no interesa
        if (mSignificantDigits == -1 || mSignificantDigits == 1)
        {
            mScaleFactor *= mSignificantDigits==-1?10.0f:0.1f;
            mSignificantDigits = 0;
        }
        LookForPlateInFit();
    }
    mSelectedTool = NULL;
    //Cambiar el cursor
    setCursor(QCursor(Qt::CrossCursor));
    updateMesh();
    updateAuxLayer();

    //Conectar slots
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ContextMenu_Slot(QPoint)));
    mCreating = false;
}

MdiFitImage::~MdiFitImage()
{
    if (mFitImage) delete mFitImage;
    mFitImage = NULL;
    if (mConvergeProfile) delete mConvergeProfile;
    mConvergeProfile = NULL;
    if (mFileDownloader) delete mFileDownloader;
    mFileDownloader = NULL;
    if (mMpcKnownObjectPd) delete mMpcKnownObjectPd;
    mMpcKnownObjectPd = NULL;
    //Destruir los XOverlays
    for (int i=0; i<mXOverlays.count(); i++)
    {
        mXOverlays.at(i)->setWindow(NULL);
        delete mXOverlays.at(i);
    }

    //Destruir la interfaz de menús
    destroyExtnsionInterface();
}

/**
 * @brief MdiFitImage::initExtensionInterface
 * @param mainwindow
 */
void MdiFitImage::initExtensionInterface()
{
    //Acción de "Guardar"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_save_fit;
    xp_save_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_save_fit.act = mSaveFitAction = new QAction(QIcon(ACTION_SAVE_ICON), "Save fit image", this);
    xp_save_fit.act->setObjectName(ACTION_SAVE);
    mSaveFitAction->setShortcut(QKeySequence::Save);
    mSaveFitAction->setStatusTip(ACTION_SAVE_DESC);
    xp_save_fit.men = MENU_FILE;
    mSaveFitAction->setEnabled(false);      //No comienza habilitada
    menusItems()->append(xp_save_fit);
    xp_save_fit.men = TOOLBAR_GENERAL;      //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_save_fit);
    connect(xp_save_fit.act, SIGNAL(triggered()), this, SLOT(Save_Slot()));

    //Acción de "Guardar Como"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_saveas_fit;
    xp_saveas_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_saveas_fit.act = new QAction("Save fit as...", this);
    xp_saveas_fit.act->setObjectName(ACTION_SAVEAS);
    xp_saveas_fit.act->setStatusTip(ACTION_SAVEAS_DESC);
    xp_saveas_fit.men = MENU_FILE;
    menusItems()->append(xp_saveas_fit);
    connect(xp_saveas_fit.act, SIGNAL(triggered()), this, SLOT(SaveAs_Slot()));

    //Acción de "Exportar vista"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_export_view;
    xp_export_view.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_export_view.act = new QAction(ACTION_EXPORT_VIEW, this);
    xp_export_view.act->setObjectName(ACTION_EXPORT_VIEW);
    xp_export_view.act->setStatusTip(ACTION_EXPORT_VIEW_DESC);
    xp_export_view.pri = ACTION_EXPORT_VIEW_PRIORITY;
    xp_export_view.men = MENU_FILE;
    menusItems()->append(xp_export_view);
    connect(xp_export_view.act, SIGNAL(triggered()), this, SLOT(ExportView_Slot()));

    //Acción de Restore fit image
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_restore_pos;
    xp_restore_pos.beh = ITEM_BEHAVIOUR_SHOW_HIDE;
    xp_restore_pos.act = mShowHeadersAction = new QAction(QIcon(ACTION_RESTORE_ICON), ACTION_RESTORE, this);
    xp_restore_pos.act->setObjectName(ACTION_RESTORE);
    xp_restore_pos.act->setStatusTip(ACTION_RESTORE_DESC);
    xp_restore_pos.men = MENU_IMAGE;
    menusItems()->append(xp_restore_pos);
    xp_restore_pos.men = TOOLBAR_FITIMAGE;
    toolbarItems()->append(xp_restore_pos);
    connect(xp_restore_pos.act, SIGNAL(triggered()), this, SLOT(Restorepos_Slot()));

    //Acción de Fit image to window
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_fitwin_pos;
    xp_fitwin_pos.beh = ITEM_BEHAVIOUR_SHOW_HIDE;
    xp_fitwin_pos.act = mShowHeadersAction = new QAction(QIcon(ACTION_FITWINDOW_ICON), ACTION_FITWINDOW, this);
    xp_fitwin_pos.act->setObjectName(ACTION_FITWINDOW);
    xp_fitwin_pos.act->setStatusTip(ACTION_FITWINDOW_DESC);
    xp_fitwin_pos.men = MENU_IMAGE;
    xp_fitwin_pos.grp = EXTGRP_IMG;
    menusItems()->append(xp_fitwin_pos);
    xp_fitwin_pos.men = TOOLBAR_FITIMAGE;
    toolbarItems()->append(xp_fitwin_pos);
    connect(xp_fitwin_pos.act, SIGNAL(triggered()), this, SLOT(FitToWindow_Slot()));

    //Acción de "Show fit headers"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_show_fit;
    xp_show_fit.beh = ITEM_BEHAVIOUR_SHOW_HIDE;
    xp_show_fit.act = mShowHeadersAction = new QAction(QIcon(ACTION_FIT_ICON), ACTION_FIT_HEADERS, this);
    xp_show_fit.act->setObjectName(ACTION_FIT_HEADERS);
    xp_show_fit.act->setStatusTip(ACTION_FIT_DESC);
    xp_show_fit.men = MENU_IMAGE;
    menusItems()->append(xp_show_fit);
    xp_show_fit.men = TOOLBAR_FITIMAGE;
    toolbarItems()->append(xp_show_fit);
    connect(xp_show_fit.act, SIGNAL(triggered()), this, SLOT(FitHeaders_Slot()));

    //Menú de "Tools"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_tool_stretch;
    xp_tool_stretch.beh = ITEM_BEHAVIOUR_SHOW_HIDE;
    xp_tool_stretch.act = new QAction(QIcon(TOOL_STRETCH_ICON), TOOL_STRETCH, this);
    xp_tool_stretch.act->setObjectName(TOOL_STRETCH);
    xp_tool_stretch.act->setStatusTip(TOOL_STRETCH_DESC);
    xp_tool_stretch.men = MENU_IMAGE_TOOLS;
    menusItems()->append(xp_tool_stretch);
    xp_tool_stretch.men = TOOLBAR_TOOLS;
    toolbarItems()->append(xp_tool_stretch);
    connect(xp_tool_stretch.act, &QAction::triggered, this, [=]{SelectTool_Slot(TOOL_STRETCH);});


    XPEXT_ACTION_ITEMS xp_tool_locate;
    xp_tool_locate.beh = ITEM_BEHAVIOUR_SHOW_HIDE;
    xp_tool_locate.act = new QAction(QIcon(TOOL_LOCATE_ICON), TOOL_LOCATE, this);
    xp_tool_locate.act->setShortcut(QKeySequence::Find);
    xp_tool_locate.act->setObjectName(TOOL_LOCATE);
    xp_tool_locate.act->setStatusTip(TOOL_LOCATE_DESC);
    xp_tool_locate.men = MENU_IMAGE_TOOLS;
    menusItems()->append(xp_tool_locate);
    xp_tool_locate.men = TOOLBAR_TOOLS;
    toolbarItems()->append(xp_tool_locate);
    connect(xp_tool_locate.act, &QAction::triggered, this, [=]{SelectTool_Slot(TOOL_LOCATE);});


    XPEXT_ACTION_ITEMS xp_tool_distance;
    xp_tool_distance.beh = ITEM_BEHAVIOUR_SHOW_HIDE;
    xp_tool_distance.act = new QAction(QIcon(TOOL_DISTANCE_ICON), TOOL_DISTANCE, this);
    xp_tool_distance.act->setObjectName(TOOL_DISTANCE);
    xp_tool_distance.act->setStatusTip(TOOL_DISTANCE_DESC);
    xp_tool_distance.men = MENU_IMAGE_TOOLS;
    menusItems()->append(xp_tool_distance);
    xp_tool_distance.men = TOOLBAR_TOOLS;
    toolbarItems()->append(xp_tool_distance);
    connect(xp_tool_distance.act, &QAction::triggered, this, [=]{SelectTool_Slot(TOOL_DISTANCE);});

    #ifdef QT_DEBUG
    XPEXT_ACTION_ITEMS xp_tool_line_profile;
    xp_tool_line_profile.beh = ITEM_BEHAVIOUR_SHOW_HIDE;
    xp_tool_line_profile.act = new QAction(QIcon(TOOL_LINEAR_PROFILE_ICON), TOOL_LINEAR_PROFILE, this);
    xp_tool_line_profile.act->setObjectName(TOOL_LINEAR_PROFILE);
    xp_tool_line_profile.act->setStatusTip(TOOL_LINEAR_PROFILE_DESC);
    xp_tool_line_profile.men = MENU_IMAGE_TOOLS;
    menusItems()->append(xp_tool_line_profile);
    xp_tool_line_profile.men = TOOLBAR_TOOLS;
    toolbarItems()->append(xp_tool_line_profile);
    connect(xp_tool_line_profile.act, &QAction::triggered, this, [=]{SelectTool_Slot(TOOL_LINEAR_PROFILE);});
    #endif

    //Inicializar el menú MPC
    MPC_InitMenu();
}


void MdiFitImage::destroyExtnsionInterface()
{

}

void MdiFitImage::SetScale(float scale)
{
    mScale = scale;
    for (int i=0; i<mXOverlays.count(); i++)
        mXOverlays.at(i)->setZoom(scale);
    ResetTransform();
}

void MdiFitImage::ResetTransform()
{
    float transx = mClientWidth/2.0f/mScale - mImageCenter.x() - 0.5;       //El 0.5 traslada a la mitad de un píxel
    float transy = mClientHeight/2.0f/mScale - mImageCenter.y() - 0.5;
    mTransform.reset();
    mTransform.scale(mScale, mScale);
    mTransform.translate(transx, transy);
    updateMesh();
    updateAuxLayer();
    this->update();
}


void MdiFitImage::wheelEvent(QWheelEvent* event)
{
    //Continúo con mi actividad
    if (dragging()) return;
    float newscale = mScale + event->angleDelta().y() * mWheelSpeed * mScale / 120;
    float oldscale = mScale;
    if (newscale < 0.1f) SetScale(0.1f);
    else if (newscale > MAX_SCALE) SetScale(MAX_SCALE);
    else SetScale(newscale);
    //Llamamos a la herramienta seleccionada con la nueva escala. Si la rechaza, cambiamos
    if (mSelectedTool && !mSelectedTool->zoomChangeEvent(newscale)) SetScale(oldscale);
    MainWindow::mainwin->UpdateLabel(0,"");
}

void MdiFitImage::mousePressEvent(QMouseEvent* event)
{
    if (mSelectedTool && !mSelectedTool->mousePressEvent(event)) return;
    //TODO: Nuevos eventos de XOverlay
    for (int i=0; i<mXOverlays.count(); i++)
        if (mXOverlays[i]->visible() && !mXOverlays[i]->mousePressEvent(event)) return;

    //Continuo con mi actividad, comienzo el arrastre
    mDragStartPoint = event->pos();
    mStarDespCenter = mImageCenter;
    setCursor(QCursor(Qt::ClosedHandCursor));
}

void MdiFitImage::mouseReleaseEvent(QMouseEvent *event)
{
    if (mSelectedTool && !mSelectedTool->mouseReleaseEvent(event)) return;
    //Continuo con mi actividad, termino el arrastre
    if (dragging())
    {
        setCursor(QCursor(Qt::CrossCursor));
        mDragStartPoint = QPoint(0,0);
        mStarDespCenter = QPointF(0,0);
        return;
    }

    //Paso el evento al overlay
    for (int i=0; i<mXOverlays.count(); i++)
        if (mXOverlays[i]->visible() && !mXOverlays[i]->mouseReleaseEvent(event)) return;
}

void MdiFitImage::keyPressEvent(QKeyEvent* event)
{
    if (mSelectedTool && !mSelectedTool->keyPressEvent(event)) return;
    for (int i=0; i<mXOverlays.count(); i++)
        if (mXOverlays[i]->visible() && !mXOverlays[i]->keyPressEvent(event)) return;
}

void MdiFitImage::mouseMoveEvent(QMouseEvent *event)
{
    if (mSelectedTool && !mSelectedTool->mouseMoveEvent(event)) return;
    //Continúo con mi evento
    if (dragging())
    {
        float vdesp_x = mDragStartPoint.x() - event->pos().x();
        float vdesp_y = mDragStartPoint.y() - event->pos().y();
        mImageCenter.setX(  mStarDespCenter.x() + vdesp_x/mScale  );
        mImageCenter.setY(  mStarDespCenter.y() + vdesp_y/mScale  );
        ResetTransform();
        if (mSelectedTool) mSelectedTool->imageMoveEvent();
        //Eventos de XOverlay
        for (int i=0; i<mXOverlays.count(); i++)
            mXOverlays[i]->imageMoveEvent();
        return;
    }

    //No estoy arrastrando y arrastro el ratón por la pantalla. Muestro datos en la barra de abajo
    else
    {
        //Eventos de XOverlay
        for (int i=0; i<mXOverlays.count(); i++)
            if (mXOverlays[i]->visible() && !mXOverlays[i]->mouseMoveEvent(event)) return;

        ScreenXYToImageXY(event->pos(),mAuxPointF);
        char buffer[128];
        //Si la escala es grande ponemos con
        float fx =  mAuxPointF.x()+(mScale>5?0.5:0);
        float fy =  mAuxPointF.y()+(mScale>5?0.5:0);
        if (mScale > 5)
            snprintf(buffer, 128, "X:%0.2f Y:%0.2f", fx, fy);
        else
            snprintf(buffer, 128, "X:%d Y:%d", (int)fx, (int)fy);
        //Si existe transformación
        if (mPlate)
        {
            double ra=0, dec=0;
            map(fx, fy, &ra, &dec);
            QByteArray ra_str = Util::RA2String(ra).toLocal8Bit();
            QByteArray dec_str = Util::DEC2String(dec).toLocal8Bit();
            char *data1 = ra_str.data();
            char *data2 = dec_str.data();
            snprintf(buffer+strlen(buffer), 128-strlen(buffer), " / α: %s; δ: %s",data1, data2);
        }
        MainWindow::mainwin->UpdateLabel(1, buffer);
        int ix = (int)floor(fx);
        int iy = (int)floor(fy);
        if (ix < 0 || ix >= mFitImage->width() || iy < 0 || iy >= mFitImage->height())
            snprintf(buffer, 128, "---");
        else
        {
            float fdata = mFitImage->pixel(ix, iy);
            snprintf(buffer,128,"i: %d",(int)fdata);
        }
        MainWindow::mainwin->UpdateLabel(2, buffer);
    }
}

void MdiFitImage::resizeEvent(QResizeEvent *event)
{
   mClientWidth = event->size().width();
   mClientHeight = event->size().height();
   ResetTransform();
   if (mSelectedTool) mSelectedTool->resizeEvent(event);

   //Redimensionado de los overlays
   for (int i=0; i<mXOverlays.count(); i++)
        mXOverlays[i]->setZoom(mScale);
}

void MdiFitImage::setFitImage(FitImage* fi)
{
    mFitImage = fi;
    mConvergeProfile->SetImage(fi);
    mConvergeProfile->Converge(&mImage);
    mPlate = fi->plate();
    setObjectName(QFileInfo(mFitImage->filename()).fileName());
    CheckDirty();
    update();
}

void MdiFitImage::converge()
{
    mConvergeProfile->Converge(&mImage);
    update();
}


void MdiFitImage::activate()
{
    if (mSelectedTool && (mSelectedTool->name() == TOOL_MPC_ADD_MEASURE) && !MainWindow::mainwin->ActiveFitImage())
        DeselectCurrentTool_Slot();
    if (mSelectedTool) mSelectedTool->activate();
    MPC_CheckMenu();
}

void MdiFitImage::deactivate()
{
    if (mSelectedTool) mSelectedTool->deactivate();
}


void MdiFitImage::paintEvent(QPaintEvent *event)
{
   if (!mFitImage ||  mCreating || !mMdiSubWindow) return;
   //http://harmattan-dev.nokia.com/docs/library/html/qt4/qpainter.html
   QPainter painter;
   paintStart(event, &painter);
   paintImage(event, &painter);
   paintLayers(event, &painter);
   if (mSelectedTool) mSelectedTool->paintEvent(event, &painter);
   paintEnd(event, &painter);
}

void MdiFitImage::paintImage(QPaintEvent* /* event */, QPainter* painter)
{
    QPointF pa; QPointF pb;
    ScreenXYToImageXY(0,0, pa);
    ScreenXYToImageXY(this->width(),this->height(), pb);
    int sx =  (int)(pa.x()-1);
    int sy = (int)(pa.y()-1);

    int despco_x = 0;
    if (pb.x() < 0)
    {
        despco_x = floor(pb.x());
        pb.setX(pb.x() - despco_x);
    }

    int despco_y = 0;
    if (pb.y() < 0)
    {
        despco_y = floor(pb.y());
        pb.setY(pb.y() - despco_y);
    }

    if (pb.x() >= mImage.width()) pb.setX(mImage.width());
    if (pb.y() >= mImage.height()) pb.setY(mImage.height());
    double roundx = pa.x() - sx;
    double roundy = pa.y() - sy;


    mTransform.reset();
    mTransform.scale(mScale, mScale);
    mTransform.translate(-roundx - 0.5,  -roundy - 0.5);
    painter->setTransform(mTransform);
    painter->drawImage(despco_x, despco_y, mImage, sx, sy, ceil(pb.x() - pa.x())+2, ceil(pb.y() - pa.y())+2);
}

void MdiFitImage::paintStart(QPaintEvent *event, QPainter* painter)
{
    painter->begin(this);
    painter->setRenderHints(QPainter::Antialiasing, false);
    painter->setViewport(MdiWindow()->childrenRect());
    painter->fillRect(event->rect(), mBackground);
}

void MdiFitImage::paintEnd(QPaintEvent* /*event*/, QPainter* painter)
{
     painter->end();
}

void MdiFitImage::paintLayers(QPaintEvent *event, QPainter* painter)
{
    mTransform.reset();
    painter->setTransform(mTransform);
    XOverlayLayer* pending_label_layer = NULL;
    for (int i=0; i<mXOverlays.count(); i++)
    {
        if (!mXOverlays.at(i)->visible()) continue;
        if (!mXOverlays.at(i)->paintEvent(event, painter)) break;
        if (mXOverlays.at(i)->pending()) pending_label_layer = mXOverlays.at(i);
    }
    if (pending_label_layer) pending_label_layer->paintPending(painter);

}

void  MdiFitImage::ImageXYToScreenXY(float imgx, float imgy, QPointF& scrxy)
{
   scrxy.setX( mClientWidth/2.0 + (imgx - mImageCenter.x()) * mScale );
   scrxy.setY( mClientHeight/2.0 + (imgy - mImageCenter.y()) * mScale   );
}

void  MdiFitImage::ImageXYToScreenXY(const QPointF& imgxy, QPointF& scrxy)
{
    ImageXYToScreenXY(imgxy.x(), imgxy.y(), scrxy);
}

void  MdiFitImage::ScreenXYToImageXY(float screenx, float screeny,QPointF& imgxy)
{
    imgxy.setX(  (screenx - mClientWidth/2.0)/mScale + mImageCenter.x());
    imgxy.setY(  (screeny - mClientHeight/2.0)/mScale + mImageCenter.y());
}

void  MdiFitImage::ScreenXYToImageXY(const QPointF& scrxy, QPointF& imgxy)
{
    ScreenXYToImageXY(scrxy.x(), scrxy.y(), imgxy);
}

void MdiFitImage::CheckDirty()
{
    mSaveFitAction->setEnabled(mFitImage->dirty());
    QString tit = getWindowTitle();
    if (windowTitle() != tit || objectName() != tit)
    {
        setWindowTitle(tit);
        MainWindow::mainwin->UpdateMenus();
    }
}

void MdiFitImage::SetImageCenterRADec(double ra, double dec)
{
    if (!mPlate) return;
    double px, py;
    mPlate->imap(ra, dec, &px, &py);
    setImageCenterPixel(px, py);
}

void MdiFitImage::setImageCenterPixel(double x, double y)
{
    mImageCenter = QPointF(x, y);
    ResetTransform();
    //if (mSelectedTool) mSelectedTool->moveEvent();

    //Con establecer el zoom al tamaño actual es suficiente
    for (int i=0; i<mXOverlays.count(); i++)
         mXOverlays[i]->imageMoveEvent();
}

QString MdiFitImage::getWindowTitle()
{
    if (mFitImage->dirty())
    {
        return QString("%1 %2").arg(objectName()).arg(" *Modified");
        MainWindow::mainwin->UpdateMenus();
    }
    else return objectName();
}

bool MdiFitImage::LookForPlateInFit()
{
   PlateConstants* init_plate = mPlate;
   //Busco una reducción astrométrica en la cabecera
   mPlate = fitimage()->plate() ? fitimage()->plate() : fitimage()->LookForPlate();
   if (init_plate != mPlate)
   {
       double ra, dec;
       map(fitimage()->width()/2, fitimage()->height()/2, &ra, &dec);
       INFO("Loaded astrometric reduction: scale[%0.3f\"/pix] rotation[%0.2fº] center[%s / %s]",
             mPlate->scale(), mPlate->rot(),
             PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)));
       updateMesh();
       updateAuxLayer();
   }
   return mPlate;
}



/**
 * @brief MdiFitImage::addXLayer  Agrega una nueva capa. Si existiera previamente una que tiene el mismo
 * nombre la eliminará de la lista y la destruirá sustituyéndola. La ventana al cerrarse se preocupará
 * de eliminar esta nueva capa insertada.
 * @param layer capa a añadir.
 */
void MdiFitImage::addXLayer(XOverlayLayer *layer, int priority)
{
    if (mXOverlays.contains(layer)) return;
    //Si ya contiene una capa con este nombre la borramos
    if (layer->objectName().length())
    {
        for (int i=0; i<mXOverlays.count(); i++)
        {
            if (layer->objectName() == mXOverlays.at(i)->objectName())
            {
                //Borramos el elemento de menú que mostraba esta capa. Muy importante!!!
                for (int j=0; j<menusItems()->count(); j++)
                {
                    if (menusItems()->at(j).act->objectName() == layer->objectName())
                    {
                        menusItems()->removeAt(j);
                        //No hace falta borrar el action, porque el padre es el layer en cuestión
                        //Al eliminar la capa también se eliminará el action
                        break;
                    }
                }
                delete mXOverlays[i];
                mXOverlays.removeAt(i);
                break;
            }
        }
    }
    mXOverlays.append(layer);
    layer->setFn(ImageXYToScreenXY_ST, ScreenXYToImageXY_ST, map_ST, imap_ST);
    layer->setWindow(this);
    layer->zoomChangeEvent(mScale);

    //Ahora añadimos el item de menú que mostrará u ocultará esta capa
    XPEXT_ACTION_ITEMS xp_show_hide_layer;
    xp_show_hide_layer.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_show_hide_layer.act = new QAction(layer->objectName(), layer); //Esta acción se destruye con la capa
    xp_show_hide_layer.act->setObjectName(layer->objectName());
    xp_show_hide_layer.act->setCheckable(true);
    xp_show_hide_layer.act->setChecked(layer->visible());
    xp_show_hide_layer.act->setStatusTip("Show/Hide " + layer->objectName() + " layer");
    xp_show_hide_layer.men = MENU_OVERLAY;
    xp_show_hide_layer.pri = priority;
    menusItems()->append(xp_show_hide_layer);
    connect(xp_show_hide_layer.act, &QAction::triggered, this, [=]{ShowHideLayer_Slot(layer);});

    //Nos aseguramos de que se muestra la nueva capa en el menú Overlays
    XPMdiWidget* xw = MainWindow::mainwin->ActiveXPMdiWidget();
    if (xw == qobject_cast<XPMdiWidget*>(this))
    {
        MainWindow::mainwin->InterfaceMdiFit_DeActivate(xw);
        MainWindow::mainwin->InterfaceMdiFit_Activate(xw);
    }
}

XOverlayLayer* MdiFitImage::getXlayer(const char* name)
{
    for (int i=0; i<mXOverlays.count(); i++)
        if (mXOverlays[i]->name() == name) return mXOverlays[i];
    return NULL;
}

XOverlayLayer* MdiFitImage::getXlayer(QString& name)
{
    for (int i=0; i<mXOverlays.count(); i++)
        if (mXOverlays[i]->name() == name) return mXOverlays[i];
    return NULL;
}


/**
 * @brief MdiFitImage::updateMesh Crea/Actualiza la malla de ascensión recta y declinación si fuera necesario.
 */
void MdiFitImage::updateMesh(int _x1, int _y1, int _x2, int _y2, bool border)
{
    if (!mPlate) return;
    if (_x1 == MININT)
    {
        _x1 = _y1 = 0;
        _x2 = mFitImage->width()-1;
        _y2 = mFitImage->height()-1;
    }
    if (!mMeshLayer)
    {
        //Como la capa no existe la creamos
        mMeshLayer = new XOverlayLayer(XLAYER_MESH);
        mMeshLayer->setVisible(false);
        addXLayer(mMeshLayer, XLAYER_MESH_PRIORITY);
        //Estilo para ascensión recta [1]
        XOverlayStyle* ra_style = mMeshLayer->addStyle(1);
        ra_style->setBorder(QColor::fromRgbF(1,1,1,0.2),1,Qt::DashLine);
        ra_style->setHoverBorderColor(Qt::red);
        //Estilo para la declinación [2]
        XOverlayStyle* dec_style = mMeshLayer->addStyle(2);
        dec_style->setBorder(QColor::fromRgbF(1,1,0,0.2),1,Qt::DashLine);
        dec_style->setHoverBorderColor(Qt::red);
    }
    if (!mMeshLayer->visible()) return;

    //Actalizar la capa de mesh
    mMeshLayer->clearItems();

    //Establezco el triángulo de dibujado de la capa
    QPointF p1, p2;
    ImageXYToScreenXY(_x1, _y1, p1);
    ImageXYToScreenXY(_x2, _y2, p2);
    QRectF cliprect(p1.x(), p1.y(), p2.x() - p1.x(), p2.y() - p1.y());

    //Preparamos los datos
    double ra1, dec1;
    double ra2, dec2;
    double ra3, dec3;
    double ra4, dec4;
    int mesh_sz = mPlate->scale() * MAXVAL(mFitImage->width(), mFitImage->height())/8.0;

    mPlate->map(_x1, _y1, &ra1, &dec1);
    mPlate->map(_x2, _y1, &ra2, &dec2);
    mPlate->map(_x2, _y2, &ra3, &dec3);
    mPlate->map(_x1, _y2, &ra4, &dec4);

    double min_ra = MINVAL(ra4, MINVAL(ra3, MINVAL(ra1, ra2)));
    double max_ra = MAXVAL(ra4, MAXVAL(ra3, MAXVAL(ra1, ra2)));
    double min_dec = MINVAL(dec4, MINVAL(dec3, MINVAL(dec1, dec2)));
    double max_dec = MAXVAL(dec4, MAXVAL(dec3, MAXVAL(dec1, dec2)));

    //Creamos las líneas de ascensión recta
    double x1, y1, x2, y2; QPointF src1, src2;
    for (double dec=min_dec; dec<max_dec; dec+=mesh_sz / 3600.0)
    {
        mPlate->imap(min_ra, dec, &x1, &y1);
        mPlate->imap(max_ra, dec, &x2, &y2);
        ImageXYToScreenXY((float)x1,(float)y1, src1);
        ImageXYToScreenXY((float)x2, (float)y2, src2);
        //Creamos la línea de la capa
        XOverlaySegment*sg = new XOverlaySegment(mMeshLayer, 1);
        sg->setStart(src1);
        sg->setEnd(src2);
        sg->setHoverable(true);
        sg->setLabelNearMouse(true);
        sg->setLabelWhenHover(true);
        //Le pongo etiqueta
        sg->setLabel(QString("δ=%1").arg(Util::DEC2String(dec)));
        sg->clip(cliprect);
    }
    //Creamos las líneas2 de declinación
    double cosdec = cos(mPlate->dec() * DEG2RAD);
    double start_ra = min_ra;
    double end_ra = max_ra;
    if (max_ra - min_ra > 12)
    {
        start_ra = max_ra;
        end_ra = min_ra;
    }
    for (double ra=start_ra; (start_ra<=end_ra && ra < end_ra) || ((start_ra>end_ra && ra>=start_ra) || ra<end_ra)  ;ra+=mesh_sz/3600.0/15.0/cosdec)
    {
        mPlate->imap(ra, min_dec, &x1, &y1);
        mPlate->imap(ra, max_dec, &x2, &y2);
        ImageXYToScreenXY((float)x1,(float)y1, src1);
        ImageXYToScreenXY((float)x2, (float)y2, src2);
        //Creamos la línea de la capa
        XOverlaySegment*sg = new XOverlaySegment(mMeshLayer, 2);
        sg->setStart(src1);
        sg->setEnd(src2);
        sg->setHoverable(true);
        sg->setLabelNearMouse(true);
        sg->setLabelWhenHover(true);
        //Le pongo etiqueta
        sg->setLabel(QString("α=%1").arg(Util::RA2String(ra)));
        if (!sg->clip(cliprect)) delete sg;
    }
    if (border)
    {
        QPointF p1, p2;
        ImageXYToScreenXY(_x1, _y1, p1);
        ImageXYToScreenXY(_x2, _y2, p2);
        //Estilo para los bordes de la imagen [4]
        XOverlayStyle* border_style = mMeshLayer->addStyle(4);
        border_style->setBorder(QColor::fromRgbF(1,1,1,0.2),1,Qt::SolidLine);
        //border_style->setHoverBorderColor(Qt::red);
        XOverlaySegment*sg = new XOverlaySegment(mMeshLayer, 4);
        sg->setStart(p1);
        sg->setEnd(QPointF(p2.x(), p1.y()));
        sg = new XOverlaySegment(mMeshLayer, 4);
        sg->setStart(QPointF(p2.x(), p1.y()));
        sg->setEnd(p2);
        sg = new XOverlaySegment(mMeshLayer, 4);
        sg->setStart(p2);
        sg->setEnd(QPointF(p1.x(), p2.y()));
        sg = new XOverlaySegment(mMeshLayer, 4);
        sg->setStart(QPointF(p1.x(), p2.y()));
        sg->setEnd(p1);
    }
}

//TODO: Actualiza la capa auxiliar con la orientación de los ejes
void MdiFitImage::updateAuxLayer()
{
    if (!mPlate) return;
    if (!mAuxLayer)
    {
        //Como la capa no existe la creamos
        mAuxLayer = new XOverlayLayer(XLAYER_INFORMATION);
        mAuxLayer->setVisible(true);
        addXLayer(mAuxLayer, XLAYER_INFORMATION_PRIORITY);
        //Estilo para las flechas de dirección [3]
        XOverlayStyle* dir_style = mAuxLayer->addStyle(1);
        dir_style->setBorder(Qt::green,2,Qt::SolidLine);
    }
    mAuxLayer->clearItems();
    //TODO: Añado las líneas de dirección si no existieran previamentes
    //Creamos la línea de la capa
    //Desde el borde superior izquierdo
    double size_segments = SZ_SEG_ORIENT;
    double vec_disp_x=width()-20, vec_disp_y=height()-40;
    double ra, dec, x1, y1, x2, y2, cscale;
    map(0,0,&ra,&dec);
    imap(ra+mPlate->scale()/36,dec,&x1,&y1);
    imap(ra,dec+mPlate->scale()/36,&x2,&y2);
    XOverlaySegment*sg_ra = new XOverlaySegment(mAuxLayer, 1);
    sg_ra->setArrowHead(SZ_ARROW_ORIENT, SEGMENT_ARROWHEAD_END);
    cscale = CART_DIST(0,0,x1,y1)/size_segments;
    sg_ra->setStart(QPointF(vec_disp_x,vec_disp_y));
    sg_ra->setEnd(QPointF(x1/cscale+vec_disp_x,y1/cscale+vec_disp_y));
    XOverlayLabel* lb_ra = new XOverlayLabel(mAuxLayer, 1);
    lb_ra->setLabel("E"); lb_ra->setPos(sg_ra->end());
    XOverlaySegment*sg_dec = new XOverlaySegment(mAuxLayer, 1);
    cscale = CART_DIST(0,0,x2,y2)/size_segments;
    sg_dec->setArrowHead(SZ_ARROW_ORIENT, SEGMENT_ARROWHEAD_END);
    sg_dec->setStart(QPointF(vec_disp_x,vec_disp_y));
    sg_dec->setEnd(QPointF(x2/cscale+vec_disp_x,y2/cscale+vec_disp_y));
    XOverlayLabel* lb_dec = new XOverlayLabel(mAuxLayer, 1);
    lb_dec->setLabel("N"); lb_dec->setPos(sg_dec->end());
    //Indicar el punto de anclado a cada etiqueta según la orientación de las flechas
    lb_dec->setAnchor(XOverlayLabel::AP_S);
    lb_ra->setAnchor(XOverlayLabel::AP_W);
}

void  MdiFitImage::ImageXYToScreenXY_ST(QObject* obj, float imagex, float imagey, QPointF& scrxy)
{
    MdiFitImage* img = (MdiFitImage*)obj;
    img->ImageXYToScreenXY(imagex, imagey, scrxy);
}

void  MdiFitImage::ScreenXYToImageXY_ST(QObject* obj, float screenx, float screeny, QPointF& imgxy)
{
    MdiFitImage* img = (MdiFitImage*)obj;
    img->ScreenXYToImageXY(screenx, screeny, imgxy);
}

void  MdiFitImage::map_ST(QObject* obj, double x, double y, double *ra, double *dec)
{
    MdiFitImage* img = (MdiFitImage*)obj;
    if (!img->mPlate) *ra = *dec = 0;
    else img->mPlate->map(x, y, ra, dec);
}

void  MdiFitImage::imap_ST(QObject* obj, double ra, double dec, double *x, double *y)
{
    MdiFitImage* img = (MdiFitImage*)obj;
    if (!img->mPlate) *x = *y = 0;
    else img->mPlate->imap(ra, dec, x, y);
}


void MdiFitImage::Save_Slot()
{
    //Cuidado, si no era originalmente un archivo fit, ofrecemos la posibilidad de guardar como
    QString ext = QFileInfo(filename()).suffix().toLower();
    if (ext != "fit" && ext != "fits")
    {
        int ret =
        QMessageBox::information(this, "Save file as fit image",
                                 "Source image was not a standard fit file.\n\n"
                                 "¿Do you want to save it as a fit image?",
                                 QMessageBox::Yes, QMessageBox::No
                                 );
        if (ret == QMessageBox::Yes) SaveAs_Slot();
        return;
    }
    if (mFitImage->SaveToFile(mFitImage->filename()))
    {
        mFitImage->SetDirty(false);
        CheckDirty();
    }
    else
        QMessageBox::warning(this, "Error", QString("Unable to open file '%1'\n\n"
                                                    "File format not recognized."
                                                    ).arg(filename()));
}


void MdiFitImage::SaveAs_Slot()
{
    QFileInfo finfo = QFileInfo(filename());
    QString sel_fmt = finfo.suffix().toLower() == "fit" ? SAVE_FMT_FIT : SAVE_FMT_FITS;
    QString fname =  QFileDialog::getSaveFileName(this,
                                                  "Save file as",
                                                  finfo.absoluteFilePath(),
                                                  SAVE_FIT_FMTS,
                                                  &sel_fmt
                                                  );
    if (!fname.length()) return;
    //Comprobamos la extensión de salida
    if (sel_fmt == SAVE_FMT_FIT || sel_fmt == SAVE_FMT_FITS)
    {
        if (mFitImage->SaveToFile(fname))
        {
            mFitImage->setFileName(fname);
            mFitImage->SetDirty(false);
            CheckDirty();
        }
        else
            QMessageBox::warning(this,"Error", QString("Unable to save file '%1'").arg(fname));
    }
    else if (sel_fmt == SAVE_FMT_JPG || sel_fmt == SAVE_FMT_PNG)
    {
        if (!mImage.save(fname))
            QMessageBox::warning(this,"Error", QString("Unable to save file '%1'").arg(fname));
    }
}

void MdiFitImage::ExportView_Slot()
{
    QFileInfo finfo = QFileInfo(filename());
    QString sel_fmt = SAVE_FMT_JPG;
    QString fname = QString("%1/%2.jpg").arg(finfo.absolutePath(), finfo.baseName());
    fname =  QFileDialog::getSaveFileName(this,
                                          "Export current view as image",
                                          fname,
                                          EXPORT_VIEW_FMT,
                                          &sel_fmt
                                          );
    if (!fname.length()) return;
    QImage img(this->width(), this->height(), QImage::Format_ARGB32);
    QPainter pt(&img);
    //pt.scale(2, 2); Para un futuro se puede hacer un buen listado de opciones de exportación
    this->render(&pt);
    if (!img.save(fname))
        QMessageBox::warning(this,"Error", QString("Unable to save file '%1'").arg(fname));
}


void MdiFitImage::FitHeaders_Slot()
{
    //Muestra el diálogo de cabeceras fit de la imagen
    HeadersDialog hd(MainWindow::mainwin);
    hd.SetHeaders(mFitImage->headers());
    hd.exec();
    CheckDirty();
}

void MdiFitImage::Restorepos_Slot()
{
    mScale = 1;
    if (!MdiWindow()->isMaximized())
    {
        //Redimensiono la ventana contenedora
        MdiWindow()->resize(mFitImage->width()>MdiWindow()->mdiArea()->width()?MdiWindow()->mdiArea()->width():mFitImage->width(),
                         mFitImage->height()>MdiWindow()->mdiArea()->height()?MdiWindow()->mdiArea()->height():mFitImage->height());
        //Si hace falta la recoloco
        int newx = MdiWindow()->pos().x();
        int newy = MdiWindow()->pos().y();
        if (newx + width() >  MdiWindow()->mdiArea()->width() || newx < 0)
            newx = 0;
        if (newy + height() > MdiWindow()->mdiArea()->height() || newy < 0)
            newy = 0;
        if (!newx || !newy) MdiWindow()->move(newx, newy);
    }
    //Inicializar el centro y la escala
    mImageCenter.setX(mFitImage->width()/2.0f);
    mImageCenter.setY(mFitImage->height()/2.0f);
    SetScale(1);
    MainWindow::mainwin->UpdateLabel(0,"");
}

void MdiFitImage::FitToWindow_Slot()
{
    if (!MdiWindow()->isMaximized())
    {
        //Redimensiono la ventana contenedora
        MdiWindow()->resize(mFitImage->width()>MdiWindow()->mdiArea()->width()?MdiWindow()->mdiArea()->width():mFitImage->width(),
                         mFitImage->height()>MdiWindow()->mdiArea()->height()?MdiWindow()->mdiArea()->height():mFitImage->height());
        //Si hace falta la recoloco
        int newx = MdiWindow()->pos().x();
        int newy = MdiWindow()->pos().y();
        if (newx + width() >  MdiWindow()->mdiArea()->width() || newx < 0)
            newx = 0;
        if (newy + height() > MdiWindow()->mdiArea()->height() || newy < 0)
            newy = 0;
        if (!newx || !newy) MdiWindow()->move(newx, newy);
    }
    //Inicializar el centro
    mImageCenter.setX(mFitImage->width()/2.0f);
    mImageCenter.setY(mFitImage->height()/2.0f);
    //Reinicializar la escala para ajustar a la ventana actual
    float img_aspect_ratio = (float)mFitImage->width()/(float)mFitImage->height();
    float win_aspect_ratio = (float)MdiWindow()->width()/(float)MdiWindow()->height();
    if (win_aspect_ratio >= img_aspect_ratio)
    {
        //Ajustar la escala al alto
        SetScale((float)MdiWindow()->height() / (float)mFitImage->height());
    }
    else
    {
        //Ajustar la escala el ancho
        SetScale((float)MdiWindow()->width() /  (float)mFitImage->width());
    }
    MainWindow::mainwin->UpdateLabel(0,"");
}

bool MdiFitImage::DeselectCurrentTool_Slot()
{
    if (!mSelectedTool) return true;
    if (!mSelectedTool->deactivate()) return false;
    //Chequear el elemento de menú o botón de barra de herramientas
    for(int i=0; i<menusItems()->count(); i++)
    {
        if (menusItems()->at(i).act->objectName() == mSelectedTool->name() && menusItems()->at(i).act->isChecked())
        {
            menusItems()->at(i).act->setChecked(false);
            break;
        }
    }
    for(int i=0; i<toolbarItems()->count(); i++)
    {
        if (toolbarItems()->at(i).act->objectName() == mSelectedTool->name() && toolbarItems()->at(i).act->isChecked())
        {
            toolbarItems()->at(i).act->setChecked(false);
            break;
        }
    }
    mSelectedTool = NULL;
    return true;
}


void MdiFitImage::SelectTool_Slot(QString name)
{
    if (mSelectedTool && mSelectedTool->name() == name)
    //Desactivar la tool
    {
        DeselectCurrentTool_Slot();
    }
    //No hay tool activada o podemos desactivar la actual
    else if (!mSelectedTool || DeselectCurrentTool_Slot())
    {
        //Chequear el elemento de menú o botón de barra de herramientas
        for(int i=0; i<menusItems()->count(); i++)
        {
            if (menusItems()->at(i).act->text() == name)
            {
                menusItems()->at(i).act->setCheckable(true);
                menusItems()->at(i).act->setChecked(true);
                break;
            }
        }
        for(int i=0; i<toolbarItems()->count(); i++)
        {
            if (toolbarItems()->at(i).act->text() == name)
            {
                toolbarItems()->at(i).act->setCheckable(true);
                toolbarItems()->at(i).act->setChecked(true);
                break;
            }
        }
        //Buscamos una instancia de la tool si estuviera disponible
        for (int i=0; i<mTools.count() && !mSelectedTool; i++)
            if (mTools[i]->name() == name) mSelectedTool = mTools[i];
        //Si nunca la hemos usado habrá que crearla
        if (!mSelectedTool)
        {
            if (name == TOOL_DISTANCE) mSelectedTool = new XImgToolDistance(this);
            else if (name == TOOL_LINEAR_PROFILE) mSelectedTool = new XImgToolLinearProfile(this);
            else if (name == TOOL_LOCATE) mSelectedTool = new XImgToolLocate(this);
            else if (name == TOOL_STRETCH) mSelectedTool = new XImgToolStretch(this);
            else if (name == TOOL_MPC_ADD_MEASURE) mSelectedTool = new XImgToolMPCAddMeasure(this);
            else if (name == TOOL_GIF) mSelectedTool = new XImgToolGif(this);
            mTools.append(mSelectedTool);
        }
        ASSERT(mSelectedTool,"Tool not found. Please edit the procedure MdiFitImage::SelectTool_Slot to instance it");
        if (!mSelectedTool->activate())
            DeselectCurrentTool_Slot();
    }
}

/**
 * @brief MdiFitImage::ShowHideLayer_Slot Slot llamado cuando se quiera mostrar/ocultar una capa.
 * Se le conectará a las distintas opciones de menú o donde fuera necesario. Se recibe como parámetro la capa
 * en cuestión.
 * @param _ly
 */
void MdiFitImage::ShowHideLayer_Slot(QObject* _ly)
{
    XOverlayLayer* ly = (XOverlayLayer*)_ly;
    if (mXOverlays.contains(ly))
    {
        ly->setVisible(!ly->visible());
        //Marco la acción o la desmarco
        for (int i=0; i<menusItems()->count(); i++)
            if (menusItems()->at(i).act->objectName() == ly->objectName())
            {
                menusItems()->at(i).act->setChecked(ly->visible());
                break;
            }
        if (ly == mMeshLayer && ly->visible()) updateMesh();
        if (ly == mAuxLayer && ly->visible()) updateAuxLayer();
        update();
    }
}


void MdiFitImage::SetMatch(Matcher* mc, PlateConstants* pt)
{
    XOverlayLayer* layer = new XOverlayLayer(XLAYER_ASTROMETRY);
    XOverlayStyle* style = layer->addStyle(XGROUP_ASTROMETRY_REFERENCE);         //Estilo básico de coincidencias
    style->setBorderColor(Qt::red);
    style->setHoverBorderColor(Qt::green);
    XOverlayStyle* nu_style = layer->addStyle(XGROUP_ASTROMETRY_REJECTED);       //Estilo de elementos rechazados
    nu_style->setBorderColor(Qt::yellow);
    nu_style->setHoverBorderColor(Qt::green);
    XOverlayStyle* align_style = layer->addStyle(XGROUP_ASTROMETRY_ALIGN);       //Estrellas de alineación
    align_style->setBorderColor(Qt::green);
    align_style->setBorderWidth(2);
    align_style->setHoverBorderColor(Qt::green);

    //Recorremos todas las estrellas coincidentes
    for (int i=0; i<mc->matchcoin()->count(); i++)
    {
        MatchCoincidence mcoin = mc->matchcoin()->at(i);
        DetectedStar ds = mc->getSourceStarDet(mcoin.src_det);
        XOverlayCircle* cir = new XOverlayCircle(layer);
        cir->setBasePos(ds.x(), ds.y());
        cir->setImageAttatched(true);
        cir->setHoverable(true);
        cir->setRadius(ds.radius());
        cir->setZoomAttatched(true);
        bool rejected = pt->rejected(ds.x(), ds.y());
        bool is_align = mc->candidatea1()->src_id == mcoin.src_det || mc->candidatea2()->src_id ==  mcoin.src_det;
        double ra, dec;

        map(ds.x(), ds.y(), &ra, &dec);
        VizierRecord vr = mc->getSourceStarCat(mcoin.src_cat);


        cir->setLabel(QString("").sprintf("Astrometry:\nXY: %0.2f / %0.2f"
                                          "\nRA: %s\nDEC:%s"
                                          "\n\nCatalog:\nRA: %s\nDEC:%s\n"
                                          "Delta: %0.5f\"",
                                          ds.x(), ds.y(),
                                          PTQ(Util::RA2String(ra)),PTQ(Util::DEC2String(dec)),
                                          PTQ(Util::RA2String(vr.ra())),PTQ(Util::DEC2String(vr.dec())),
                                          fabs(Matcher::AngularDistance(ra, dec, vr.ra(), vr.dec()) * 3600.0)
                                          ));
        //cir->setHtml(true);
        layer->addItem(cir, is_align ? XGROUP_ASTROMETRY_ALIGN : (rejected ? XGROUP_ASTROMETRY_REJECTED : XGROUP_ASTROMETRY_REFERENCE) );
    }
    addXLayer(layer, XLAYER_ASTROMETRY_PRIORITY);

    //Añadimos las estrellas de catálogo
    layer = new XOverlayLayer(XLAYER_DOWNLAODED_STARS);
    XOverlayStyle* mc_style = layer->addStyle(2);

    mc_style->setBorderColor(Qt::red);
    for (int i=0; i<mc->matchcat()->count(); i++)
    {
        MatchStar mcs = mc->matchcat()->at(i);
        VizierRecord vr = mc->getSourceStarCat(mcs.src_id);

        XOverlayCross* cir = new XOverlayCross(layer);
        cir->setRadius(5);
        cir->setRaDecAttatched(true);
        cir->setZoomAttatched(true);
        cir->setBasePos(vr.ra(), vr.dec());
        layer->addItem(cir, 2);
    }

    addXLayer(layer, XLAYER_DETECTED_STARS_PRIORITY);
    update();
    MainWindow::mainwin->UpdateMenus();
}


void MdiFitImage::ContextMenu_Slot(QPoint pos)
{
    XOverlayLayer* layer; XOverlayItem* item;
    QMenu* ctxMenu = new QMenu(this);
    QAction* sel_action = NULL;
    QAction* copyRADecAction = NULL;
    QAction* removeFromReduction = NULL;
    QAction* copySrcRADecAction = NULL;
    QAction* saveRefStarsAction = NULL;
    QPoint globalPos = this->mapToGlobal(pos);

    //Menú VO
    //----------------------------------------------------------------------------------
    float simbad_radius=0.2f;//Minutos de arco
    QMenu*  voMenu =  new QMenu("Virtual Observatory",this);
    QAction* voSimbad = NULL;

    double ra=0, dec=0, src_ra=0, src_dec=0;
    QPointF imgpos;
    ScreenXYToImageXY(pos, imgpos);
    if(mPlate) map(imgpos.x(), imgpos.y(), &ra, &dec);

    //Copiar coordenada de la estrella sobre la que estamos
    //----------------------------------------------------------------------------------
    if (mPlate &&  //Reducción astrométrica
                   ((layer = getXlayer(XLAYER_ASTROMETRY))
                         && ((item = layer->overItem(XGROUP_ASTROMETRY_REFERENCE)) || (item = layer->overItem(XGROUP_ASTROMETRY_ALIGN)))
                   //Estrellas detectadas
                   || ((layer = getXlayer(XLAYER_DETECTED_STARS))
                        && ((item = layer->overItem(XGROUP_DETECTED_STAR)) || (item = layer->overItem(XGROUP_DEBLENDED_STAR))))
                  )
       )
    {
        map(item->bx(), item->by(), &src_ra, &src_dec);
        copySrcRADecAction = ctxMenu->addAction("Copy source position: " + Util::RA2String(src_ra) + " " + Util::DEC2String(src_dec));
        if (layer->name() == XLAYER_ASTROMETRY
                && (item->group() == XGROUP_ASTROMETRY_REFERENCE || item->group() == XGROUP_ASTROMETRY_ALIGN)
                && mPlate->ctrlcount() > 6)
        removeFromReduction = ctxMenu->addAction("Reject reference star");
        voSimbad = voMenu->addAction("SIMBAD coordenate query");
        simbad_radius = 0.1f;
    }

    //Copiar coordenada actual action
    //----------------------------------------------------------------------------------
    if (mPlate && imgpos.x() >= 0 && imgpos.x() < fitimage()->width() && imgpos.y() >= 0 && imgpos.y() < fitimage()->height())
    {
        copyRADecAction = ctxMenu->addAction("Copy cursor position: " + Util::RA2String(ra) + " " + Util::DEC2String(dec));
        copyRADecAction->setEnabled(mPlate);
        if (!voSimbad)
        {
            voSimbad = voMenu->addAction("SIMBAD coordenate query");
            simbad_radius = 5;
        }
    }

    //Salvar estrellas de referencia
    //----------------------------------------------------------------------------------
    if (mPlate && mPlate->ctrlcount())
    {
        saveRefStarsAction = ctxMenu->addAction("Save reference stars");
        saveRefStarsAction->setEnabled(mPlate);
    }

    //Mostrar el menú y ejecutar la acción
    //----------------------------------------------------------------------------------
    if (voMenu->actions().count()) ctxMenu->addMenu(voMenu);
    if (ctxMenu->actions().count() && (sel_action = ctxMenu->exec(globalPos)))
    {
        if (sel_action == copyRADecAction)
            QApplication::clipboard()->setText(Util::RA2String(ra) + " " + Util::DEC2String(dec));
        if (sel_action == copySrcRADecAction)
            QApplication::clipboard()->setText(Util::RA2String(src_ra,4) + " " + Util::DEC2String(src_dec, 3));
        else if (sel_action == saveRefStarsAction)
        {
            QString fname =  QFileDialog::getSaveFileName(this,"Save astrometric reference stars","","text file (*.txt)");
            if (fname.length() && !AstrometryComputer::SaveReferenceStars(mPlate, QString(fname), fitimage()->filename()))
                QMessageBox::critical(MainWindow::mainwin, "Error", QString("Unable to save output file '<b>%1</b>'").arg(fname));
        }
        else if (sel_action == removeFromReduction)
        {
            mPlate->rejectPoint(item->bx(), item->by());
            mPlate->Compute();
            layer->removeItem(item, false);
            item->setGroup(XGROUP_ASTROMETRY_REJECTED);
            layer->addItem(item, XGROUP_ASTROMETRY_REJECTED);
            INFO(LOG_SEPARATOR);
            INFO("Source removed from reduction: RA: %s; DEC: %s", PTQ(Util::RA2String(src_ra)), PTQ(Util::DEC2String(src_dec)));
            INFO("    PLATE TRANSFORM STATS: Davg:  %6.4f\"; Dstdev:  %6.4f\"", mPlate->deltaavg(), mPlate->deltastdev());
            INFO("    REFERENCE STARS: %d" ,mPlate->ctrlcount());
            this->update();
        }

        else if (sel_action == voSimbad)
        {
            QDesktopServices::openUrl(QString("http://simbad.u-strasbg.fr/simbad/sim-coo?Coord=%1 %2&Radius=%3")
                                      .arg(Util::RA2String(ra!=0&&dec!=0?ra:src_ra))
                                      .arg(Util::DEC2String(ra!=0&&dec!=0?dec:src_dec))
                                      .arg(simbad_radius));
        }
    }

    delete voMenu;
    delete ctxMenu;
}

void MdiFitImage::AddMarks(QStringList& marks)
{
    //Creo la capa de user drawing si no existiera
    XOverlayLayer* layer = NULL;
    for (int i=0; i<mXOverlays.count() && !layer; i++)
    {
        if (mXOverlays.at(i)->name() == XLAYER_USER_DRAWING)
            layer = mXOverlays.at(i);
    }
    if (!layer)
    {
        layer = new XOverlayLayer(XLAYER_USER_DRAWING, this);
        this->addXLayer(layer, XLAYER_USER_DRAWING_PRIORITY);
    }
    layer->setVisible(false);
    //Ahora hay que crer varios estilos....
    XOverlayStyle* style_cross = layer->addStyle(1);
    style_cross->setBorder(Qt::red, 3, Qt::SolidLine);
    style_cross->setHoverBorder(Qt::green, 3, Qt::SolidLine);
    style_cross->setBoxBorder(Qt::green, 1, Qt::DashLine);
    XOverlayStyle* style_poly = layer->addStyle(2);
    style_poly->setBorder(Qt::green, 1, Qt::DashLine);
    style_poly->setHoverBorder(Qt::green, 1, Qt::DashLine);
    style_poly->setBoxBorder(Qt::green, 1, Qt::DashLine);


    //Se parsean los elementos para meterlos en la capa de dibujo
    for (int i=0; i<marks.length(); i++)
    {
        XOverlayItem* item = XOverlayItem::parseItem(marks[i]);
        if (!item)
        {
            WARN("Invalid mark definition: %s", PTQ(marks[i]));
            continue;
        }
        //Ahora le asignamos un estilo
        if (item->type() == XTYPE_CROSS)
        {
            layer->addItem(item);
            item->setStyle(style_cross);
        }
        else if (item->type() == XTYPE_SEGMENT)
        {
            layer->addItem(item);
            //item->setStyle(style_segment);
        }
        else if (item->type() == XTYPE_POLYGON)
        {
            layer->addItem(item);
            item->setStyle(style_poly);
        }
        else
        {
            layer->addItem(item);
        }
    }
    this->ShowHideLayer_Slot(layer);
}


















