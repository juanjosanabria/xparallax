#ifndef TYPES_H
#define TYPES_H

//---------------------------------------------------------
//Tamaños predefinidos por defecto y otros tipos de la interfaz
//---------------------------------------------------------
#define SIZE_TOOLBAR_BUTTON     18
#define SIZE_TOOL_BUTTON        20
#define SIZE_ANIMATEDLABEL      18
#define WINDOWS_MUTEX           L"XParallax_semaphore"
#define OPEN_FILE_FORMATS       "*.fit *.fits *.fts *.st6 *.jpg *.jpeg *.png *.gif *.tif *.tiff *.bmp *.mpc"

//---------------------------------------------------------
//Defines para la interfaz de menús y barras de herramientas
//---------------------------------------------------------
#define     XPRIORITY                   "XPRIORITY"         //Propiedad para almacenar la propiedad de menús, toolbars, items etc
#define     XSTATICACTION               "XSTATICATION"      //Acción estática se mostrará en todo momento
#define     XDYNAMICACTION              "XDYNAMICACTION"    //Acción dinámica

//---------------------------------------------------------
//Menús generales
//---------------------------------------------------------
#define     MENU_FILE                    "&File"
#define     MENU_FILE_PRIORITY           1
#define     MENU_ENVIRONMENT             "&Environment"
#define     MENU_ENVIRONMENT_PRIORITY    2
#define     MENU_IMAGE_ANALYSIS          "&Image process"
#define     MENU_IMAGE_ANALYSIS_PRIORITY 3
#define     MENU_TOOLS                   "&Tools"
#define     MENU_TOOLS_PRIORITY          4

//---------------------------------------------------------
//Menús de MdiFitImage
//---------------------------------------------------------
#define     MENU_IMAGE                   "Image"
#define     MENU_IMAGE_TOOLS             "Image Tools"
#define     MENU_OVERLAY                 "Overlay"
#define     MENU_MPC_REPORT              "MPC reports"

//---------------------------------------------------------
//Menús de MdiFitBlink
//---------------------------------------------------------
#define     MENU_MPC                     "Minor Planet Center"

//---------------------------------------------------------
//Menús de Generales a la derecha
//---------------------------------------------------------
#define     MENU_HELP                    "&Help"
#define     MENU_HELP_PRIORITY           65535

#define     MENU_TEST                    "Test"
#define     MENU_TEST_PRIORITY           (MENU_HELP_PRIORITY-1)

#define     MENU_EXPERIMENTAL            "Experimental"
#define     MENU_EXPERIMENTAL_PRIORITY   (MENU_HELP_PRIORITY-2)



#define     MENU_ADDITIONAL_PRIORITY     (MENU_HELP_PRIORITY-100)


#define     TOOLBAR_GENERAL             "General"
#define     TOOLBAR_FITIMAGE            "Fit image"
#define     TOOLBAR_BLINK               "Blink"
#define     TOOLBAR_TOOLS               "Tools"
#define     TOOLBAR_MPC                 "MPC"

#define     ACTION_OPEN                 "Open file"
#define     ACTION_OPEN_DESC            "Open a new file"
#define     ACTION_OPEN_ICON            ":/graph/images/folder-open-32.png"
#define     ACTION_OPEN_PRIORITY        1000
#define     ACTION_SAVE                 "&Save"
#define     ACTION_SAVE_DESC            "Saves the current file"
#define     ACTION_SAVE_ICON            ":/graph/images/save.png"
#define     ACTION_SAVE_PRIORITY        1001
#define     ACTION_SAVEAS               "Save as..."
#define     ACTION_SAVEAS_DESC          "Save current documment with different name"
#define     ACTION_SAVEAS_PRIORITY      1002
#define     ACTION_EXPORT               "Export"
#define     ACTION_EXPORT_DESC          "Export current view to other file type"
#define     ACTION_EXIT                 "Exit"
#define     ACTION_EXIT_ICON            ":/graph/images/door-closed.png"
#define     ACTION_EXIT_DESC            "Exits this program"
#define     ACTION_EXIT_PRIORITY        9999
#define     ACTION_FIT_HEADERS          "Show/Edit headers"
#define     ACTION_FIT_DESC             "Show and edit fit image headers"
#define     ACTION_FIT_ICON             ":/graph/images/icn_fit.svg"
#define     ACTION_RESTORE              "&Image size to 100%"
#define     ACTION_RESTORE_DESC         "Restore image position and size"
#define     ACTION_RESTORE_ICON         ":/graph/images/full-screen-exit.png"
#define     ACTION_FITWINDOW            "Fit image to window"
#define     ACTION_FITWINDOW_DESC       "Change the image size to fill the current window"
#define     ACTION_FITWINDOW_ICON       ":/graph/images/full-screen-alt-5.png"
#define     ACTION_EXPORT_VIEW          "Export current view"
#define     ACTION_EXPORT_VIEW_DESC     "Save current view as image"
#define     ACTION_EXPORT_VIEW_PRIORITY (ACTION_SAVEAS_PRIORITY+1)
#define     ACTION_MPC_KNOWN_OBJ        "Known Object overlay"
#define     ACTION_MPC_KNOWN_OBJ_DESC   "Displays the nominal position of asteroids and coments"
#define     ACTION_MPC_KNOWN_OBJ_ICON   ":/graph/images/icn_planets.svg"
#define     ACTION_MPC_MPCORB           "Update MPC orbit files"
#define     ACTION_MPC_MPCORB_DESC      "Check and download MPCORB file"
#define     ACTION_MPC_MPCORB_ICON      ":/graph/images/icn_download.svg"
#define     ACTION_MPC_NEW_REPORT       "New MPC report"
#define     ACTION_MPC_NEW_REPORT_DESC  "Creates a new MPC report"
#define     ACTION_MPC_NEW_REPORT_ICON  ":/graph/images/icn_asteroid.svg"
#define     ACTION_MPC_VALIDATE         "Validate MPC report"
#define     ACTION_MPC_VALIDATE_DESC    "Check the current report for consistency"
#define     ACTION_MPC_VALIDATE_ICON    ":/graph/images/icn_validate.svg"
#define     ACTION_MPC_SEND             "Submit MPC report"
#define     ACTION_MPC_SEND_DESC        "Send current MPC report to the Minor Planet Center"
#define     ACTION_MPC_SEND_ICON         ":/graph/images/icn_sendmail.svg"
#define     ACTION_MPC_ADD_HDR_DESC     "Adds a new header to the current report"
#define     ACTION_MPC_ADD_COD_HDR      "Add COD (Observatory code)"
#define     ACTION_MPC_ADD_CON_HDR      "Add CON (Contact details, Address)"
#define     ACTION_MPC_ADD_CON2_HDR     "Add CON (Contact details, Email)"
#define     ACTION_MPC_ADD_COM_HDR      "Add COM (Geographic location)"
#define     ACTION_MPC_ADD_ACK_HDR      "Add ACK (Acknowledge id)"
#define     ACTION_MPC_ADD_AC2_HDR      "Add AC2 (Acknowledge recipients)"
#define     ACTION_MPC_ADD_OBS_HDR      "Add OBS (Observer)"
#define     ACTION_MPC_ADD_MEA_HDR      "Add MEA (Measurer)"
#define     ACTION_MPC_ADD_TEL_HDR      "Add TEL (Telescope details)"
#define     ACTION_MPC_ADD_NET_HDR      "Add NET (Catalog)"
#define     ACTION_MPC_ADD_NUM_HDR      "Add NUM (Number of observations)"
#define     ACTION_MPC_ADD_BND_HDR      "Add BND (Band used)"

#define     ACTION_FIRST                "First image"
#define     ACTION_FIRST_DESC           "Go to the first image"
#define     ACTION_FIRST_ICON           ":/graph/images/first_32.png"
#define     ACTION_NEXT                 "Next image"
#define     ACTION_NEXT_DESC            "Go to the next image"
#define     ACTION_NEXT_ICON            ":/graph/images/next_32.png"
#define     ACTION_PREV                 "Previous image"
#define     ACTION_PREV_DESC            "Go to the previous image"
#define     ACTION_PREV_ICON            ":/graph/images/prev_32.png"
#define     ACTION_LAST                 "Last image"
#define     ACTION_LAST_DESC            "Go to the last image"
#define     ACTION_LAST_ICON            ":/graph/images/last_32.png"
#define     ACTION_PLAY                 "Play sequence"
#define     ACTION_PLAY_DESC            "Show images in loop"
#define     ACTION_PLAY_ICON            ":/graph/images/play_32.png"
#define     ACTION_PAUSE_ICON           ":/graph/images/pause_32.png"

#define     TOOL_STRETCH                "Stretch and contrast"
#define     TOOL_STRETCH_ICON           ":/graph/images/histogram_32.png"
#define     TOOL_STRETCH_DESC           "Image Stretch and contrast tool"

#define     TOOL_LOCATE                 "Locate image position"
#define     TOOL_LOCATE_ICON            ":/graph/images/target_32.png"
#define     TOOL_LOCATE_DESC            "Highlight a pixel inside the image"

#define     TOOL_DISTANCE               "Distance"
#define     TOOL_DISTANCE_ICON          ":/graph/images/rule_24.png"
#define     TOOL_DISTANCE_DESC          "Analize flux thru a rectangular aperture"

#define     TOOL_LINEAR_PROFILE         "Linear profile"
#define     TOOL_LINEAR_PROFILE_ICON    ":/graph/images/gauss_24.png"
#define     TOOL_LINEAR_PROFILE_DESC    "Analize flux thru a rectangular aperture"

#define     TOOL_MPC_ADD_MEASURE        "Add/Edit MPC measure"
#define     TOOL_MPC_ADD_MEASURE_ICON   ":/graph/images/icn_new_do.svg"
#define     TOOL_MPC_ADD_MEASURE_DESC   "Adds and edit MPC report measures"

#define     TOOL_GIF                    "Capture animated gif"
#define     TOOL_GIF_DESC                "Select an area and export it as an animated gif sequence"
#define     TOOL_GIF_ICON                ":/graph/images/icn_gif.svg"

//Acciones del menú imagen


#endif // TYPES_H
