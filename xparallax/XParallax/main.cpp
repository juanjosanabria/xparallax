#include "mainwindow.h"
#include "xglobal.h"
#include "core_def.h"
#include <QApplication>
#include <QCoreApplication>
#include <QGuiApplication>
#include <QtGui/QScreen>
#include <QResource>
#include <QDir>
#include <QLocale>
#include <locale.h>
#include <QMutex>
#include <QThread>
#include <QTextCodec>

void StartFunction(QApplication* app);
void EndFunction();
void LoadFonts();
void DebugTasks();
void ParseArguments(const QStringList& args, QStringList& filename, QStringList& marks);

int main(int argc, char *argv[])
{
#ifdef Q_OS_WIN
    CreateMutex(NULL,false,WINDOWS_MUTEX);
#endif
    //Inicializar la aplicación
    QApplication a(argc, argv);
    //Inicializar el path de los plugins. En debug no tendrá efecto, puesto que no hay harchivos terminados en "d" en esa ruta
    QStringList args = a.arguments();
    StartFunction(&a);


    //Crear la ventana principal y lanzarla
    MainWindow* w = new MainWindow();
    w->show();

    //Parsear los argumentos
    QStringList filenames, marks;
    ParseArguments(args, filenames, marks);
    for (int i=0; i<filenames.length(); i++)
         w->OpenFile(filenames[i]);
    //Añadir las marcas a las imágenes si las hubiera
    if (marks.length())
    {
        QList<MdiFitImage*> fitimages = w->fitImages();
        for (int i=0; i<fitimages.length();i++)
            fitimages[i]->AddMarks(marks);
    }

    //Ejecutar la instancia
    int ret = a.exec();
    delete w;

    //Función final
    EndFunction();
    return ret;
}

void ParseArguments(const QStringList& args, QStringList& filenames, QStringList& marks)
{
    //Buscar archivos que se ha solicitado abrir
    QStringList open_formats;
    bool last_is_modifier = false;
    //El primer argumento es siempre el nombre del programa
    for (int i=1; i<args.length(); i++)
    {
        QString arg = args[i];
        if (arg.startsWith('-'))
        {
            last_is_modifier = true;
            continue;
        }
        if (!open_formats.length())
            open_formats = QString(OPEN_FILE_FORMATS).replace("*","").split(" ");
        bool found = false;
        for (int j=0; j<open_formats.length() && !found; j++)
            found = arg.endsWith(open_formats[j], Qt::CaseInsensitive);
        if (!found)
        {
            last_is_modifier = false;
            continue;
        }
        if (QFile(args[i]).exists())
            filenames.append(args[i]);
        last_is_modifier = false;
    }
    //Buscar elementos de overlay pero solamente si hay una imagen
    if (filenames.length() == 1)
    {
        for (int i=1; i<args.length()-1; i++)
        {
            QString arg = args[i];
            if (arg.startsWith('-')) marks.append(args[i+1]);
        }
    }
}

void StartFunction(QApplication* app)
{
    XGlobal::initialize();
#ifdef QT_DEBUG
    XGlobal::LOGGER->setOutputDebug(true);
#else
    XGlobal::LOGGER->setOutputDebug(false);
#endif
    LoadFonts();

    //Inicializar el tamaño de fuentes en pantalla y otros elementos
    QScreen* scr = QGuiApplication::primaryScreen();
    XGlobal::SCREEN_DPI = scr->logicalDotsPerInchX() / 96.0;

   // QFontDatabase::addApplicationFont()

    app->setStyleSheet(  QString(
       //"* {  font-family: \"Ubuntu\";   }"
       //"* {  font-family: \"Segoe UI\";   }"
      // "* {  font-family: \"MS Shell Dlg 2\";   }"
       //"* {font-family: \"Microsoft Sans Serif\";}"
       "QProgressBar { text-align: center;}"
       "QToolButton{ width: %1px; height:%1px; }"
       // "QPushButton { background:#eee; border:1px solid #ccc;}"
       // " QPushButton:Hover{ background:#ccc; } "
       //"QLineEdit, QPlainTextEdit {background:#fff; border:1px solid #ccc; border-radius:2px; }"
       )
         //Tamaño de los botones de la barra de herramientas
         .arg( (int)(SIZE_TOOLBAR_BUTTON * XGlobal::SCREEN_DPI) )
       );

    app->addLibraryPath(XGlobal::PLUGIN_PATH);

#ifdef QT_DEBUG
    DebugTasks();
#endif
}

void EndFunction()
{
    XGlobal::End();
}

#ifdef QT_DEBUG
void DebugTasks()
{
    //Cuando nos ejecutamos en debug, si no existiera el archivo [CFG]/des_machine lo creamos
    //Este archivo nos indicará solamente que la máquina es de desarrollo
    QFile fdes(QString("%1%2%3").arg(XGlobal::CONF_PATH).arg(QDir::separator()).arg("des_machine"));
    if (!fdes.exists() && fdes.open(QFile::WriteOnly))
    {
        fdes.close();
        qDebug() << "Se ha creado el archivo de indicación de desarrollo: " << fdes.fileName();
    }

}
#endif


void LoadFonts()
{
    //Cargar todas las fuentes del sistema recorriendo recurivamente el directorio de fuentes
    QDir dir(XGlobal::FONT_PATH);
    QFileInfoList list = dir.entryInfoList(QDir::NoDot | QDir::NoDotDot | QDir::Dirs);
    for (int i=0; i<list.count(); i++)
    {
        QDir fontdir(list.at(i).filePath());
        QFileInfoList fnames = fontdir.entryInfoList(QDir::Files);
        for (int j=0; j<fnames.count(); j++)
        {
            if (!fnames.at(j).fileName().endsWith(".ttf")) continue;
            QString fname = fnames.at(j).filePath();
            QFontDatabase::addApplicationFont(fname);
        }
    }
}







