#include "mainwindow.h"
#include "inputdialog.h"
#include "calibration.h"
#include "ui_helpers/progressdialog.h"
#include "util.h"
#include "matcher.h"
#include "plateconstants.h"
#include "overlay/xoverlay.h"



void MainWindow::Test2_Slot()
{
   MdiFitImage* fi = ActiveFitImage();
   if (!fi) return;
   float* dt = fi->fitimage()->data();
   int width = fi->fitimage()->width();
   int height = fi->fitimage()->height();
   int n = width*height;
   for (int i=0; i<n; i++)
       dt[i] += 32768;
   QString qsavef = QFileDialog::getSaveFileName(this, "Guardar");
   CalibrationManager::NormalizeMinMax(dt, 32768, width, height);
   FitImage::SaveToFile(dt,width, height, qsavef, -32);
}

void MainWindow::Test3_Slot()
{
    InputDialog id;
    id.SetTitle("Coordenadas del centro de la imagen");
    id.ShowRADec(15.23412239821823, -32.112354);
}

void MainWindow::Test4_Slot()
{
    //Calculamos el fondo de la imagen
    MdiFitImage* fi = ActiveFitImage();
    if (!fi) return;
    BackgroundMap* bkg = new BackgroundMap();
    bkg->Setup(Core::BKG_KAPPA_SIGMA, fi->fitimage()->width(), fi->fitimage()->height(), 64, 3, 4,  2);

    bkg->Compute(fi->fitimage()->data());

    int n = fi->fitimage()->width() * fi->fitimage()->height();
    ImStat istat = ImStat::Compute(bkg->backdata(), fi->fitimage()->width(), fi->fitimage()->height() );
    for (int i=0; i< n; i++)
    {
        bkg->backdata()[i] -= istat.min();
    }

    FitImage::SaveToFile(bkg->backdata(), fi->fitimage()->width(), fi->fitimage()->height(), "c:\\back_00.fit",-32, 100 );

    int b_width = fi->fitimage()->width()/64;
    int b_height = fi->fitimage()->height()/64;
    float* dtf = new  float[b_width*b_height +1];
    for (int y=0; y<b_height;y++)
    {
        for (int x=0; x<b_width; x++)
        {
            dtf[y*b_width+x] = bkg->backentry(x*64,y*64)->Back;
        }
    }
    FitImage::SaveToFile(dtf, b_width,b_height, "c:\\back_local.fit",-32, 100 );


    delete bkg;
}


void MainWindow::Test5_Slot()
{

/*
    pcc->setCenter(Util::ParseRA("12 26 15.0"), Util::ParseDEC("44 15 00.0"));

    pcc->AddCtrlPoint(+47.5,  +73.3,   Util::ParseRA("12 22 40.293"), Util::ParseDEC("45 04 33.40"));
    pcc->AddCtrlPoint(+41.2,  -62.1,   Util::ParseRA("12 23 32.960"), Util::ParseDEC("43 23 42.59"));
    pcc->AddCtrlPoint(+29.9,  +65.2,   Util::ParseRA("12 23 56.037"), Util::ParseDEC("44 59 16.14"));
    pcc->AddCtrlPoint(+5.5,   +71.1,   Util::ParseRA("12 25 39.308"), Util::ParseDEC("45 04 14.33"));
    pcc->AddCtrlPoint(-4.8,   -72.7,   Util::ParseRA("12 26 43.594"), Util::ParseDEC("43 17 28.86"));
    pcc->AddCtrlPoint(-33.0,  +21.0,   Util::ParseRA("12 28 27.655"), Util::ParseDEC("44 27 53.23"));
    pcc->AddCtrlPoint(-31.2,  -62.0,   Util::ParseRA("12 28 27.755"), Util::ParseDEC("43 26 04.19"));
    pcc->AddCtrlPoint(-51.2,  -59.7,   Util::ParseRA("12 29 52.083"), Util::ParseDEC("43 28 33.76"));
    pcc->AddCtrlPoint(-55.7,  +60.6,   Util::ParseRA("12 29 56.524") , Util::ParseDEC("44 57 27.65"));
    double ra, dec;
    pcc->map(+47.5, +73.3, &ra, &dec);
*/
    //Intentamos hacer la astrometría de TrEs-1b
    MdiFitImage* fi = ActiveFitImage();
    if (!fi) return;
    FitImage* img = fi->fitimage();
    PlateConstants* pcc = new PlateConstants(img->width(), img->height());
    if (pcc) qDebug() << "Se encontró PCC";

    /*
    Center (RA, Dec):	(285.961, 36.633)
    Center (RA, hms):	19h 03m 50.726s
    Center (Dec, dms):	+36° 37' 58.742"
    Size:	30.8 x 30.8 arcmin
    Radius:	0.363 deg
    Pixel scale:	1.81 arcsec/pixel
    Orientation:	Up is -178 degrees E of N
    */

    StarDetector *detector = new StarDetector(img->data(),img->width(),img->height());
    Matcher* matcher = new Matcher();
    MatchParameters par = MatchParameters();
    if (detector || matcher)
    {
        qDebug() << "Se creó detector y matcher";
    }

    par.image_width = img->width();
    par.image_height = img->height();

    // TrES-1b
/*
    par.mag_limit = 14;
    par.pixel_scale_arcsec = 1.81;
    par.pixel_size_um = 18;
    par.fov_extension = 2;
    par.center_ra =  Util::ParseRA("19 03 50.729");
    par.center_dec = Util::ParseDEC("+36 37 58.29");
    par.ComputeScale();


    //R01_Cap_00000

    par.mag_limit = 12;
    par.focal_length_mm = 420;
    par.pixel_size_um = 5.4;
    par.fov_extension = 1.1;
    par.center_ra =  Util::ParseRA("20 58 25");
    par.center_dec = Util::ParseDEC("-14 07 08");
    par.ComputeScale();


    //R06_Cap_00000

    par.mag_limit = 12;
    par.focal_length_mm = 420;
    par.pixel_size_um = 5.4;
    par.center_ra =  Util::ParseRA("21 51 46");
    par.center_dec = Util::ParseDEC("-09 58 42");
    par.ComputeScale();



    par.mag_limit = 13;
    par.focal_length_mm = 3000;
    par.fov_extension = 2;
    par.pixel_size_um = 5.74f * 4.0f;
    par.scale_tolerance = 0.03f;
    par.dist_tolerance = 1;
    par.center_ra =  Util::ParseRA("08 22 36");
    par.center_dec = Util::ParseDEC("+29 29 48");
    par.ComputeScale();


    //Primero descargar las estrellas
    VizierReader* vr = new VizierReader();

    vr->WebRead(par.center_ra, par.center_dec, par.fov_radius_deg * par.fov_extension, par.mag_limit);
    DetectedStar* stars = detector->DetectStars(0,0,img->width() - 1, img->height() - 1);

    matcher->setParameters(par);

    matcher->Match(stars, detector->count(), vr->stars());


    //Calculamos las constantes de placa a ver qué tal va la cosilla
    pcc->setCenter(par.center_ra, par.center_dec);


    for (int i=0; i<matcher->matchcoin()->count(); i++)
    {
        MatchCoincidence mc = matcher->matchcoin()->at(i);
        pcc->AddCtrlPoint(mc.a_x, mc.a_y, mc.ra, mc.dec);
    }
    pcc->Compute();

    pcc->Refine(3);

    //Añadimos las cabeceras a la imagen fit
    fi->fitimage()->headers()->AddHeader("LSPC-A",pcc->a(),"Plate constant [a]");
    fi->fitimage()->headers()->AddHeader("LSPC-B",pcc->b(),"Plate constant [b]");
    fi->fitimage()->headers()->AddHeader("LSPC-C",pcc->c(),"Plate constant [c]");
    fi->fitimage()->headers()->AddHeader("LSPC-D",pcc->d(),"Plate constant [d]");
    fi->fitimage()->headers()->AddHeader("LSPC-E",pcc->e(),"Plate constant [e]");
    fi->fitimage()->headers()->AddHeader("LSPC-F",pcc->f(),"Plate constant [f]");
    fi->fitimage()->headers()->AddHeader("LSPC-SCA",pcc->scale(),"Plate scale");
    fi->fitimage()->headers()->AddHeader("LSPC-RA",pcc->ra(),"Plate center");
    fi->fitimage()->headers()->AddHeader("LSPC-DEC",pcc->dec(),"Plate center");
    //Añadir información del catálogo usado y el límite de magnitud

   // MatchOverlay* mo  = new MatchOverlay(fi , matcher, pcc);
   // fi->AddOverlay(mo);

    fi->setPlate(pcc);

    delete vr;
    delete detector;
    */
}



void MainWindow::Test6_Slot()
{
/*

    //Primero descargar las estrellas
    VizierReader* vr = new VizierReader();

    MatchParameters par = MatchParameters();

    par.image_width = 1024;
    par.image_height = 1024;

    par.mag_limit = 13;
    par.pixel_scale_arcsec = 1.81f;
    par.pixel_size_um = 18;
    par.center_ra =  Util::ParseRA("19 03 50.729");
    par.center_dec = Util::ParseDEC("+36 37 58.29");
    par.ComputeScale();

    vr->WebRead(par.center_ra, par.center_dec, par.fov_radius_deg * 1.2, 14);

    MdiStars* ms = new MdiStars();
    QMdiSubWindow *win = mMDIArea->addSubWindow(ms);
    win->setWindowState( (win->windowState() & ~Qt::WindowMinimized) | Qt::WindowMaximized  );
    win->setWindowTitle( "Prueba de estrellas");
    win->setMinimumSize(256, 256);
    win->setCursor(Qt::CrossCursor);

    ms->SetMdiWindow(win);
    ms->SetStars(vr->stars());
    win->show();
*
*/

}

struct StarParams
{
    double A;
    double stdv_x;
    double stdv_y;
    double theta;
};


void MainWindow::Test7_Slot()
{
    /*
    int width  =1024;
    int height =1024;

    QString saveto = QFileDialog::getExistingDirectory(this,"Directorio de destino");
    float* data = new float[width*height];
    float* dataux = new float[width*height];

    float noise_min = 120;
    float noise_max = 5930; //Imagen muy ruidosa
    int noise_levels = 10;
    //float noise_max = 130; // Imagen poco ruidosa


    int nstars = 500;
    float pedestal = 500;
    float max_radius = 0.2;
    float min_radius = 0.1;

    //Amplitud
    float max_A = 10000;
    float min_A = 4000;

    //Desviación típica
    float min_stdev = 0.5;
    float max_stdev = 4;

    //Ángulo
    float min_phi = 0;
    float max_phi = M_PI;


    QList<QPointF> stars;
    QList<StarParams> sparams;

    //Ponemos toda la imagen a 0
    for (int n=0; n<width*height; n++)
        data[n] = 0;

    for (int i=0; i<nstars; i++)
    {
        float star_x = Core::randFloat(max_radius+5, width-(max_radius+5));
        float star_y = Core::randFloat(max_radius+5, height-(max_radius+5));
        //Cuidado, si hay alguna estrella ya cerca cambiamos el punto de inicio
        bool search = true;
        while (search)
        {
            search = false;
            for (int i=0; i<stars.length(); i++)
            {
                double dist = CART_DIST(stars[i].x(), stars[i].y(), star_x, star_y);
                if (dist < 30)
                {
                    i=0;
                    star_x = Core::randFloat(max_radius+5, width-(max_radius+5));
                    star_y = Core::randFloat(max_radius+5, height-(max_radius+5));
                    search = true;
                }
            }
        }

        float stdevx = Core::randFloat(min_stdev, max_stdev);
        float stdevy = stdevx * Core::randFloat(0.2, 1.3);
        float phi = Core::randFloat(min_phi, max_phi);

        float radius = NAN;
        //El max radius es menos probable cuanto mayor es
        while (isnan(radius))
        {
             radius = Core::randFloat(min_radius, max_radius);
        }
        radius = 16;


        float A = Core::randFloat(min_A, max_A);

        double a = cos(phi)*cos(phi) / (2*stdevx*stdevx) + sin(phi)*sin(phi) / (2*stdevy*stdevy);
        double b = -sin(2*phi) / (4*stdevx*stdevx) + sin(2*phi) / (4*stdevy*stdevy);
        double c = sin(phi)*sin(phi) / (2*stdevx*stdevx) + cos(phi)*cos(phi) / (2*stdevy*stdevy);


        for (int x=star_x-radius; x<star_x+radius; x++)
            for (int y=star_y-radius; y<star_y+radius; y++)
                if (x >= 0 && y >= 0 && x<width && y<height)
            {
                 float valxy = A * exp ( - (a*(x - star_x)*(x - star_x) +
                                            2*b*(x - star_x)*(y - star_y) +
                                            c*(y - star_y)*(y - star_y))
                                            );

                 data[x + y * width] = valxy;
            }
        stars.append(QPointF(star_x,star_y));
        StarParams sp;
        sp.A = A; sp.stdv_x = stdevx; sp.stdv_y = stdevy;
        sparams.append(sp);
    }

    //Ahora que ya tenemos las estrellas
    //Aplicamos el ruido para cada nivel de ruido
    float noise_step = (noise_max-noise_min) / (float) noise_levels;
    for (int nl=1; nl<=noise_levels; nl++)
    {
        float mnoise = noise_min + noise_step*nl;
        for (int n=0; n<width*height; n++)
            dataux[n] =  data[n] + pedestal + Core::randFloat(noise_min, mnoise);
        QString filename = QString("%1/stars_%2.fit").arg(saveto).arg(nl);
        FitImage::SaveToFile(dataux, width, height, filename, -32);

        //Detectar las estrellas de la imagen
        filename = QString("%1/stars_%2.txt").arg(saveto).arg(nl);
        QFile fil(filename);
        fil.open(QFile::WriteOnly);
        QTextStream strs(&fil);
        //Ahora detectamos las estrellas
        StarDetector* detector = new StarDetector(dataux, width, height);
        DetectedStar* starsd = detector->DetectStars(0,0,width-1,height-1);

        //Cabecera del archivo de datos
        strs << "#synt_x synt_y det_x det_y dist1 wx wy dw A stdvx stdvy" << "\n";

        for (int s = 0; s<stars.count(); s++)
        {
            QPointF source_star = stars[s];
            DetectedStar* det_star = NULL;
            StarParams sp;
            float curdist = width*2;
            //Buscamos la estrella detectada que se corresponde con la original
            for (int d=0; d<detector->count(); d++)
            {
                float dist = CART_DIST(source_star.x(), source_star.y(),  starsd[d].x(), starsd[d].y());
                if (dist < curdist)
                {
                    curdist = dist;
                    det_star = starsd+d;
                    sp = sparams.at(s);
                }
            }
            if (curdist > 1) continue;

            double curdist_c = CART_DIST(source_star.x(), source_star.y(), det_star->wx(), det_star->wy());

            strs << source_star.x() << " " << source_star.y() << " " <<
                    det_star->x() << " " << det_star->y() << " " << curdist << " " <<
                    det_star->wx() << " " << det_star->wy() << " " << curdist_c << " " <<
                    sp.A << " " << sp.stdv_x << " " << sp.stdv_y <<
                    "\n";
        }

        delete detector;
        fil.close();
    }

*/


}


void MainWindow::Test8_Slot()
{
    /*
    QString dir = QFileDialog::getExistingDirectory(this);
    if (!dir.length()) return;
    //Construyo los nombres de los archivos de entrada y compruebo si existen
    QString fits_file = QString("%1/%2").arg(dir).arg("sky.fits");
    QString cat_file = QString("%1/%2").arg(dir).arg("sky.list");
    QString result_file = QString("%1/%2").arg(dir).arg("result.txt");
    if (!QFileInfo(fits_file).exists()) return;
    if (!QFileInfo(cat_file).exists()) return;
    //Cargo la imagen
    FitImage *img = FitImage::OpenFile(fits_file);

    //Cargo las estrellas
    QList<QPointF> stars;
    QFile file(cat_file);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
        return;
    }
    QTextStream in_file(&file);
    while(!in_file.atEnd()) {
        QString line = in_file.readLine();
        line = line.trimmed();
        if (line.startsWith("#")) continue;
        QStringList fields = line.split(" ",QString::SkipEmptyParts);
        if (fields.length() < 3) continue;
        int objid = fields[0].toInt();
        if (objid != 100) continue;
        float objx = fields[1].toFloat();
        float objy = fields[2].toFloat();
        QPointF pf(objx, objy);
        stars.append(pf);
    }
    file.close();

    //Detectamos las estrellas
    StarDetector* detector = new StarDetector(img->data(), img->width(), img->height());
    DetectedStar* starsd = detector->DetectStars(0,0, img->width() - 1, img->height() - 1);

    //Ahora buscamos cada estrella detectada en la lista, suponemos siempre la mas cercana
    //Volcando la información en el correspondiente archivo
    QFile fout(result_file);
    if(!fout.open(QIODevice::WriteOnly)) {
        QMessageBox::information(0, "error", file.errorString());
        return;
    }
    QTextStream ts(&fout);
    char buffer[1024];
    int detcount = detector->count();
    double diff_x = 0, diff_y = 0;
    double diff_wx = 0, diff_wy = 0;
    int star_match = 0;
    for (int i=0; i<detcount; i++)
    {
      float last_dist = FLOAT_MAX;
      QPointF last_star(-1,-1);
      for (int j=0; j<stars.count(); j++)
      {
          float dist = CART_DIST(stars.at(j).x(), stars.at(j).y(), starsd[i].x(), starsd[i].y());
          if (dist < last_dist)
          {
              last_dist = dist;
              last_star = stars.at(j);
          }
      }
      if (last_dist > 1.5) continue;
      diff_x += last_star.x() - starsd[i].x();
      diff_y +=  last_star.y() - starsd[i].y();
      diff_wx += last_star.x() - starsd[i].wx();
      diff_wy += last_star.y() - starsd[i].wy();

      star_match ++;
    }

    diff_x /= star_match;
    diff_y /= star_match;

    diff_wx /= star_match;
    diff_wy /= star_match;
    ts << "#x y dx dy dd wx wy dw" << "\n";
    for (int i=0; i<detcount; i++)
    {
      float last_dist = FLOAT_MAX;
      QPointF last_star(-1,-1);
      for (int j=0; j<stars.count(); j++)
      {
          float dist = CART_DIST(stars.at(j).x(), stars.at(j).y(), starsd[i].x() + diff_x, starsd[i].y()  + diff_y);
          if (dist < last_dist)
          {
              last_dist = dist;
              last_star = stars.at(j);
          }
      }
      if (CART_DIST(last_star.x(), last_star.y(),  starsd[i].x() + diff_x, starsd[i].y()  + diff_y) > 1.5) continue;
      sprintf(buffer,"%8.4f\t\t%8.4f\t\t%8.4f\t\t%8.4f\t\t%8.4f\t\t%8.4f\t\t%8.4f\t\t%8.4f\n",
              last_star.x(),  last_star.y(),
              starsd[i].x() + diff_x,  starsd[i].y()  + diff_y, CART_DIST(last_star.x(), last_star.y(),  starsd[i].x() + diff_x, starsd[i].y()  + diff_y),
              starsd[i].wx() + diff_wx,  starsd[i].wy()  + diff_wy, CART_DIST(last_star.x(), last_star.y(),  starsd[i].wx() + diff_wx, starsd[i].wy()  + diff_wy)
              );
      ts << buffer;
    }

    fout.close();
    delete detector;
    delete img;
    */
}

void MainWindow::Test9_Slot()
{
    /*
    QPointF a1 = QPointF(2, 3);
    QPointF a2 = QPointF(3, 8);

    QPointF b1 = QPointF(0, 0);
    QPointF b2 = QPointF(sqrt(2)/2,sqrt(2)/2);
    Transform2D t2; t2.reset();
    t2.rotate(195); t2.translate(12, 7); t2.scale(2, 2);
    b1 = t2.map(a1);
    b2 = t2.map(a2);


    Transform2D::ComputeTransform(a1.x(), a1.y(), a2.x(), a2.y(), b1.x(), b1.y(), b2.x(), b2.y(), &t2);

    QPointF kkp = t2.imap(b1);
    QPointF kkp2 = t2.imap(b2);

    QTransform tt2; tt2.reset();
    //Rtar 135 1,1 -> -1,1
    t2.reset(); t2.rotate(135);
    tt2.reset(); tt2.rotate(135);
    kkp = t2.map(QPointF(1,1));
    kkp2 = tt2.map(QPointF(1,1));

    //Rtar 225 1,1 -> -1,-1
    t2.reset(); t2.rotate(225);
    tt2.reset(); tt2.rotate(225);
    kkp = t2.map(QPointF(1,1));
    kkp2 = tt2.map(QPointF(1,1));

    //Rtar 315 1,1 -> 1,-1
    t2.reset(); t2.rotate(315);
    tt2.reset(); tt2.rotate(315);
    kkp = t2.map(QPointF(1,1));
    kkp2 = tt2.map(QPointF(1,1));

    //Prueba mierda, quitar esto
   // QPointF a1(0,0); QPointF a2(1,0);
    QTransform tfs; tfs.reset();
    //tfs.translate(30,);



    QPointF p2d =  t2.map(a2);

    tfs.rotate(45);
    tfs.translate(6, 3);
  //  QPointF b2 = tfs.map(a2);


    tfs.reset(); tfs.translate(-6 , -3); tfs.rotate(-45);
    QPointF bx = tfs.map(b2);

    int num_stars = 220;

    float min_flux = 100000;
    float max_flux = 122;

    int width = 1024;
    int height = 1024;

    double rotation_deg = 33.2512;
    double translation_x = 23.12;
    double translation_y = 122.2;

    QTransform tf; tf.reset();
    tf.translate(translation_x,translation_y); tf.rotate(rotation_deg);  tf.scale(1.018, 1.018);

    //Comenzamos
    QString filenam = QFileDialog::getSaveFileName(this,"Archivo con la información de las estrellas");
    if (!filenam.length()) return;

    char buffer[1024];
    QFile f(filenam);
    f.open(QFile::WriteOnly);
    QTextStream ts(&f);

    QVector<MatchStar> det_stars, cat_stars;
    //Generamos el array de estrellas
    for (int i=0; i<num_stars; i++)
    {
        MatchStar ms;
        ms.x = Core::randFloat(0, width);
        ms.y = Core::randFloat(0, height);
        ms.flux = Core::randFloat(min_flux, max_flux);
        ms.src_id = i;
        det_stars.append(ms);
        cat_stars.append(ms);
    }

    ts << ";ID X Y FLUX\n";
    //Volcamos las estrellas al disco (primera tanda)
     ts << ";DETECTED-STARS\n";
    for (int i=0; i<num_stars; i++)
    {
        sprintf(buffer, "%04d\t%8.4f\t%8.4f\t%8.4f\n",  det_stars.at(i).src_id, det_stars.at(i).x, det_stars.at(i).y,  det_stars.at(i).flux );
        ts << buffer;
    }



    //Mezclamos a tope
    for (int i=0; i<cat_stars.count(); i++)
    {
        int idx2 = Core::randInt(num_stars);
        MatchStar aux = cat_stars.at(i);
        cat_stars.replace(i, cat_stars.at(idx2));
        cat_stars.replace(idx2, aux);
    }
    ts << ";-----------------------------------------------------------------------------------\n";
    ts << ";-----------------------------------------------------------------------------------\n";
    ts << "; ROT: " << rotation_deg << "º, " << (rotation_deg*DEG2RAD) << " rad -- TRASL: " << translation_x << ", " << translation_y << "\n";
    ts << ";-----------------------------------------------------------------------------------\n";
    ts << ";-----------------------------------------------------------------------------------\n";
    ts << ";CATALOG-STARS\n";

    //Volcamos de nuevo las estrellas aplicando la transformación
    for (int i=0; i<num_stars; i++)
    {
        MatchStar ms = cat_stars.at(i);
        double newx, newy;
        tf.map( ms.x, ms.y, &newx, &newy );
        ms.x = newx;
        ms.y = newy;
        cat_stars.replace(i, ms);

        sprintf(buffer, "%04d\t%8.4f\t%8.4f\t%8.4f\n",  ms.src_id, ms.x, ms.y, ms.flux );
        ts << buffer;
    }
    f.close();

    //Intentamos machear
    Matcher* mc = new Matcher();


    mc->MatchBruteForce(&det_stars, &cat_stars);
    */
}













