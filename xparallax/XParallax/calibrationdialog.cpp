#include "calibrationdialog.h"
#include "ui_calibrationdialog.h"
#include "calibration.h"
#include "progress.h"
#include "mainwindow.h"
#include "ui_helpers/progressdialog.h"
#include "xglobal.h"

const char* CalibrationDialog::CALIBRATION_DIALOG_INI = "calibration_dialog.ini";

CalibrationDialog::CalibrationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalibrationDialog)
{
    ui->setupUi(this);

    mComputer = NULL;
    QSettings sett(QString("%1/%2").arg( XGlobal::CONF_PATH).arg(CALIBRATION_DIALOG_INI),QSettings::IniFormat);
    mParmeters.Load(&sett, "CALIBRATION");
    mInputFiles.Load(&sett, "FILES");
    ui->widgetCalibration->SetParameters(&mParmeters);
    ui->widgetBackground->SetParameters(&mParmeters.p_bkg);

    ui->widgetSourceFiles->RemoveOption(SelectFilesLayout::SFO_ALLOW_OVERWRITE);
    ui->widgetSourceFiles->AddOption(SelectFilesLayout::SFO_OUTPUT_FOLDER_MANDATORY);
    ui->widgetSourceFiles->AddOption(SelectFilesLayout::SFO_REQUEST_SAME_SIZE);
    ui->widgetSourceFiles->SetLastDirs(mInputFiles.lastSearchPath(), mInputFiles.outputDir());

    ui->widgetSourceFiles->FN_GET_ITEM_IMAGE = FN_GET_ITEM_IMAGE;
    ui->widgetSourceFiles->FN_GET_ITEM_TOOLTIP = FN_GET_ITEM_TOOLTIP;
    ui->widgetSourceFiles->FN_GET_ITEM_SORT = FN_ITEM_SORT;

    //Opciones avanzadas
    connect(ui->buttonDefaults, SIGNAL(clicked()), this, SLOT(SetDefaults_Slot()));
    connect(ui->checkHot, SIGNAL(toggled(bool)), ui->spinHot, SLOT(setEnabled(bool)));
    connect(ui->checkDead, SIGNAL(toggled(bool)), ui->spinDead, SLOT(setEnabled(bool)));
    connect(ui->checkCosmicRay, SIGNAL(toggled(bool)), ui->spinCosmicRay, SLOT(setEnabled(bool)));
    connect(ui->checkCold, SIGNAL(toggled(bool)), ui->spinCold, SLOT(setEnabled(bool)));

    SetAdvancedData(&mParmeters);
    MainWindow::DPIAware(this, true);
}

CalibrationDialog::~CalibrationDialog()
{
    delete ui;
    if (mComputer) delete mComputer;
    mComputer = NULL;
}

void CalibrationDialog::SetAdvancedData(CalibrationParameters *par)
{
    ui->radioConnFour->setChecked(par->hot_cold_connectivity == Core::CONNECTIVITY_FOUR);
    ui->radioConnEight->setChecked(par->hot_cold_connectivity == Core::CONNECTIVITY_EIGHT);
    ui->checkHot->setChecked(par->hot_pixel_sigma > 0);
    if (par->hot_pixel_sigma > 0) ui->spinHot->setValue(par->hot_pixel_sigma);
    ui->checkDead->setChecked(par->dead_pixel_sigma > 0);
    if (par->dead_pixel_sigma > 0) ui->spinDead->setValue(par->dead_pixel_sigma);
    ui->checkCosmicRay->setChecked(par->cosmic_ray_sigma > 0);
    if (par->cosmic_ray_sigma > 0) ui->spinCosmicRay->setValue(par->cosmic_ray_sigma);
    ui->checkCold->setChecked(par->cold_pixel_sigma > 0);
    if (par->cold_pixel_sigma > 0) ui->spinCold->setValue(par->cold_pixel_sigma);
}

bool CalibrationDialog::CheckAdvancedData(CalibrationParameters *par)
{
    par->hot_cold_connectivity = ui->radioConnFour->isChecked() ? Core::CONNECTIVITY_FOUR : Core::CONNECTIVITY_EIGHT;
    par->hot_pixel_sigma = ui->checkHot->isChecked() ? ui->spinHot->value() : -ui->spinHot->value();
    par->dead_pixel_sigma = ui->checkDead->isChecked() ? ui->spinDead->value() : -ui->spinDead->value();
    par->cosmic_ray_sigma = ui->checkCosmicRay->isChecked() ? ui->spinCosmicRay->value() : -ui->spinCosmicRay->value();
    par->cold_pixel_sigma = ui->checkCold->isChecked() ? ui->spinCold->value() : -ui->spinCold->value();
    return true;
}

void CalibrationDialog::accept()
{
    //Comprobacioenes
    if (!ui->widgetSourceFiles->CheckData(&mInputFiles)) return;
    if (!ui->widgetCalibration->CheckData(&mParmeters)) return;
    if (!ui->widgetBackground->CheckData(&mParmeters.p_bkg)) return;
    if (!CheckAdvancedData(&mParmeters)) return;
    mParmeters.output_dir = mInputFiles.outputDir();

    this->hide();
    if (mComputer) delete mComputer;
    mComputer = new CalibrationComputer();

    //Metemos los archivos de calibración en la lista de calibración
    mCalibrationFiles.Clear();
    QStringList ifiles = *mInputFiles.filenames();
    mCalibrationFiles.Append(ifiles);
    XGlobal::APP_SETTINGS.ct_cal++;

    //Ocultamos el diálogo para mostrar el de progreso
    this->hide();
    ProgressDialog prg(this, "Image calibration in progress");
    prg.SetIcon(":/graph/images/icn_camera.svg");
    prg.Connect(mComputer);
    prg.ExecInBackground(Background_Calibration, this);

    if (mComputer->cancelled())
    {
        prg.close();
        this->close();
        return;
    }

    //Guardamos los parámetros antes de nada
    QSettings sett( QString("%1/%2").arg( XGlobal::CONF_PATH).arg(CALIBRATION_DIALOG_INI),QSettings::IniFormat );
    mParmeters.Save(&sett, "CALIBRATION");
    mInputFiles.Save(&sett, "FILES");
    this->close();
}

int CalibrationDialog::Background_Calibration(void *data)
{
    CalibrationDialog* cdialog = (CalibrationDialog*)data;
    bool ok = cdialog->mComputer->doWork( &(cdialog->mParmeters), &(cdialog->mCalibrationFiles));
    return ok?0:1;
}


QString  CalibrationDialog::FN_GET_ITEM_IMAGE(FitHeaderList* fhl,const QString& filename)
{
    if (filename.contains("dark", Qt::CaseInsensitive)||fhl->getImageType() == FitHeaderList::Dark)
        return ":/graph/images/dark-16.png";
    if (filename.contains("flat", Qt::CaseInsensitive)||fhl->getImageType() == FitHeaderList::Flat)
        return ":/graph/images/flat-16.png";
    if (filename.contains("bias", Qt::CaseInsensitive)||fhl->getImageType() == FitHeaderList::Bias)
        return ":/graph/images/bias-16.png";
    return ":/graph/images/light-16.png";
}

QString  CalibrationDialog::FN_GET_ITEM_TOOLTIP(FitHeaderList* fhl,const QString& filename)
{
    if (filename.contains("dark", Qt::CaseInsensitive)||fhl->getImageType() == FitHeaderList::Dark)
        return "Dark frame";
    if (filename.contains("flat", Qt::CaseInsensitive)||fhl->getImageType() == FitHeaderList::Flat)
        return "Flat frame";
    if (filename.contains("bias", Qt::CaseInsensitive)||fhl->getImageType() == FitHeaderList::Object)
        return "Bias frame";
    return "Light frame";
}


int CalibrationDialog::FN_ITEM_SORT(const QString& a,const QString& b)
{
   bool a_is_bias =  a.contains("bias", Qt::CaseInsensitive);
   bool a_is_flat =  a.contains("flat", Qt::CaseInsensitive);
   bool a_is_dark =  a.contains("dark", Qt::CaseInsensitive);
   bool a_is_light = (!a_is_bias && !a_is_flat && !a_is_dark);

   bool b_is_bias =  b.contains("bias", Qt::CaseInsensitive);
   bool b_is_flat =  b.contains("flat", Qt::CaseInsensitive);
   bool b_is_dark =  b.contains("dark", Qt::CaseInsensitive);
   bool b_is_light = (!b_is_bias && !b_is_flat && !b_is_dark);

   if (a_is_light && !b_is_light) return -1;
   if (b_is_light && !a_is_light) return 0;

   if (a_is_bias && !b_is_bias) return -1;
   if (b_is_bias && !a_is_bias) return 0;

   if (a_is_dark && !b_is_dark) return -1;
   if (b_is_dark && !a_is_dark) return 0;

   return a < b ? -1 : 0;
}


/**
 * @brief CalibrationDialog::on_buttonDefaults_clicked Botón por defecto de la pestaña "Advanced"
 */
void CalibrationDialog::SetDefaults_Slot()
{
    CalibrationParameters def;      //Obtengo valores por defecto
    SetAdvancedData(&def);
}
