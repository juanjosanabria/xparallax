#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include "core.h"
#include "xglobal.h"
#include "mainwindow.h"
#include <QDesktopServices>
#include <QUrl>
#include <QString>
#include <QThread>
#include <QFuture>
#include <QtConcurrent>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonObject>
#include <QSysInfo>
#include <QtCore/QFileInfo>


XUpdateStruct  AboutDialog::mUpdateData;
bool           AboutDialog::mUpdateCancelled = false;
QMutex         AboutDialog::mUpdateMutex;
QThread*       AboutDialog::mUpdTrhead = NULL;
QSemaphore     AboutDialog::mUpdSem;
bool           AboutDialog::mUpdateRequestedbyUser = false;

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    setWindowFlags( windowFlags() & ~Qt::FramelessWindowHint );
    //setFixedSize(width(),height());

    QPalette palette = ui->frame->palette();
    palette.setColor( backgroundRole(), QColor( 255, 255, 255 ) );
    ui->frame->setPalette( palette );
    ui->frame->setAutoFillBackground( true );

    ui->labelVersion->setText(QString("<html><head/><body><p><span style=' font-size:14pt; font-weight:600; color:#000000;'>v %1</span></p></body></html>").arg(XVERSION));
    connect(ui->labelWeb, SIGNAL(linkActivated(QString)), this, SLOT(VisitWeb_Slot(QString)));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));

    MainWindow::DPIAware(this, true);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::VisitWeb_Slot(QString url)
{
    QDesktopServices::openUrl(url);
}

void AboutDialog::NoNewerVersion_Slot()
{

}

void AboutDialog::UpdateThread()
{
    //Damos tiempo a que reaccione el programa
    mUpdTrhead = QThread::currentThread();
    if (mUpdateCancelled) return;
    if (mUpdSem.tryAcquire(1, 3000))
    {
        mUpdTrhead = NULL;
        return;
    }
    if (mUpdateCancelled) return;

    QEventLoop loop;
    QNetworkAccessManager *manager = new QNetworkAccessManager();

    QNetworkRequest request;
    request.setHeader(QNetworkRequest::UserAgentHeader,XUSER_AGENT);
    QString url = UPDATE_URL;

    //Aplicamos los valores
    QString osname = XGlobal::OS_TYPE.toLower();
    url += QString("&version=%1").arg(XVERSION);
    url += QString("&os=%1").arg(osname);
    url += QString("&bits=%1").arg(XGlobal::OS_BITS);
    url += QString("&uid=%1").arg(XGlobal::APP_SETTINGS.uid.toString());
    url += QString("&rc=%1").arg(XGlobal::APP_SETTINGS.run_c);
    url += QString("&dpi=%1").arg(XGlobal::SCREEN_DPI);
    //Si es una máquina de desarrollo
    if (QFile(QString("%1%2%3").arg(XGlobal::CONF_PATH).arg(QDir::separator()).arg("des_machine")).exists())
        url += QString("&des_machine=true");

    //Contadores
    url += QString("&ct_ast=%1").arg(XGlobal::APP_SETTINGS.ct_ast);
    url += QString("&ct_fast=%1").arg(XGlobal::APP_SETTINGS.ct_fast);
    url += QString("&ct_cal=%1").arg(XGlobal::APP_SETTINGS.ct_cal);
    url += QString("&ct_fcal=%1").arg(XGlobal::APP_SETTINGS.ct_fcal);
    url += QString("&ct_bhe=%1").arg(XGlobal::APP_SETTINGS.ct_bhe);
    url += QString("&ct_fbhe=%1").arg(XGlobal::APP_SETTINGS.ct_fbhe);
    url += QString("&ct_ica=%1").arg(XGlobal::APP_SETTINGS.ct_ica);
    url += QString("&ct_fica=%1").arg(XGlobal::APP_SETTINGS.ct_fica);
    url += QString("&ct_mpc=%1").arg(XGlobal::APP_SETTINGS.ct_mpc);

    DINFO("Solicitando: %s", PTQ(url));

    request.setUrl(QUrl(url));

    QNetworkReply* resp = manager->get(request);
    connect(resp, SIGNAL(finished()), &loop, SLOT(quit()));

    //Esperar a que acabe
    loop.exec();
    QString xversion((char*)XVERSION);
    QNetworkReply::NetworkError err = resp->error();
    if (err == QNetworkReply::NoError)
    {
        QJsonDocument  doc = QJsonDocument::fromJson(resp->readAll());
        if (doc.isEmpty() || !doc.isArray())  WARN("Invalid update information got");
        else
        {
            QJsonArray arr = doc.array();
            for (int i=0; i<arr.count(); i++)
            {
                QJsonObject obj = arr.at(i).toObject();
                QString r_osname = obj.value("os").toString();
                int r_osbits = obj.value("bits").toVariant().toInt();
                QString version = obj.value("version").toString();
                QString filename = obj.value("filename").toString();
                if (versionIsUpper(version, xversion) && osname == r_osname && XGlobal::OS_BITS == r_osbits)
                {
                    mUpdateData.osBits = XGlobal::OS_BITS ;
                    mUpdateData.osName = osname;
                    mUpdateData.version = version;
                    mUpdateData.filename = filename;
                    mUpdateData.httpStatus = 200;
                    emit MainWindow::mainwin->UpdateFound_Signal(true, mUpdateRequestedbyUser);
                    break;
                }
            }
            if (mUpdateData.httpStatus != 200 && mUpdateRequestedbyUser)
                emit MainWindow::mainwin->UpdateFound_Signal(false, true);
        }
    }
    else
    {
         WARN("Unable to check for updates ERR[%d]. Please, visit http://www.xparallax.com/",(int)err);
    }
    //Se asegura de eliminar la petición y de que se borra el objeto de memoria compartida
    resp->finished();
    mUpdTrhead = NULL;

}

void AboutDialog::CheckForUpdates(bool userRequested)
{
    mUpdateMutex.lock();    
    if (mUpdTrhead)
    {
        mUpdateMutex.unlock();
        return;
    }
    mUpdateCancelled = false;
    mUpdateRequestedbyUser = userRequested;
    QtConcurrent::run(&AboutDialog::UpdateThread);
    while (!mUpdTrhead) QThread::msleep(23);
    mUpdateMutex.unlock();
}

bool AboutDialog::versionIsUpper(QString& remote_version, QString& local_version)
{
    bool ok;
    QStringList la = remote_version.split(".");
    QStringList lb = local_version.split(".");
    for (int i=0; i< MINVAL(la.count(), lb.count()); i++)
    {
        QString ca = la.at(i);
        QString cb = lb.at(i);
        int ia =  ca.toInt(&ok);
        if (!ok) return false;
        int ib = cb.toInt(&ok);
        if (!ok) return false;
        if (ia == ib) continue;
        if (ia > ib) return true;
        return false;
    }
    return false;

}

void AboutDialog::cancelUPdateCheck()
{
    mUpdateCancelled = true;
    mUpdSem.release(1);
    while (mUpdTrhead) QThread::msleep(23);
}
