#include "mdifitimage.h"
#include "mainwindow.h"
#include "ui_helpers/progressdialog.h"
#include "ui_mdi_childs/mdimpc.h"
#include "modext/blink/mdifitblink.h"
#include "types.h"
#include "xstringbuilder.h"
#include "sky.h"
#include "novas.h"

#define KNOWN_OBJECT_MARGIN_PIX     50

ProgressDialog* MdiFitImage::mMpcKnownObjectPd;

#define FILE_MPCORB "MPCORB.DAT"
#define FILE_COMETS "CometEls.txt"


void MdiFitImage::MPC_InitMenu()
{
    //Acción de descarga de archivos "Mpcorb"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_mpcorb;
    xp_mpc_mpcorb.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_mpcorb.act = new QAction(QIcon(ACTION_MPC_MPCORB_ICON), ACTION_MPC_MPCORB, this);
    xp_mpc_mpcorb.act->setObjectName(ACTION_MPC_MPCORB);
    xp_mpc_mpcorb.act->setStatusTip(ACTION_MPC_MPCORB_DESC);
    xp_mpc_mpcorb.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_mpcorb);
    xp_mpc_mpcorb.men = TOOLBAR_MPC;
    //toolbarItems()->append(xp_mpc_mpcorb);
    connect(xp_mpc_mpcorb.act, &QAction::triggered, this, [=]{ MPC_Mpcorb_Slot(false); });

    //Acción de "Known Objects overlay"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_known_obj;
    xp_mpc_known_obj.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_known_obj.act = new QAction(QIcon(ACTION_MPC_KNOWN_OBJ_ICON), ACTION_MPC_KNOWN_OBJ, this);
    xp_mpc_known_obj.act->setObjectName(ACTION_MPC_KNOWN_OBJ);
    xp_mpc_known_obj.act->setStatusTip(ACTION_MPC_KNOWN_OBJ_DESC);
    mMpcKnownObjectsAction = xp_mpc_known_obj.act;
    xp_mpc_known_obj.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_known_obj);
    xp_mpc_known_obj.men = TOOLBAR_MPC;
    toolbarItems()->append(xp_mpc_known_obj);
    connect(xp_mpc_known_obj.act, &QAction::triggered, this, [=]{ MPC_KnownObjs_Slot(); });

    //Acción de "New MPC report"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_new_report;
    xp_mpc_new_report.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_new_report.act = new QAction(QIcon(ACTION_MPC_NEW_REPORT_ICON), ACTION_MPC_NEW_REPORT, this);
    xp_mpc_new_report.act->setObjectName(ACTION_MPC_NEW_REPORT);
    xp_mpc_new_report.act->setStatusTip(ACTION_MPC_NEW_REPORT_DESC);
    xp_mpc_new_report.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_new_report);
    xp_mpc_new_report.men = TOOLBAR_MPC;
    //toolbarItems()->append(xp_mpc_new_report);
    connect(xp_mpc_new_report.act, &QAction::triggered, this, [=]{ MPC_NewReport(); });

    //Herramienta de "Add MPC measure"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_tool_add_measure;
    xp_tool_add_measure.beh = ITEM_BEHAVIOUR_WIDGET_CONTROLLED;
    xp_tool_add_measure.act = new QAction(QIcon(TOOL_MPC_ADD_MEASURE_ICON), TOOL_MPC_ADD_MEASURE, this);
    xp_tool_add_measure.act->setObjectName(TOOL_MPC_ADD_MEASURE);
    xp_tool_add_measure.act->setStatusTip(TOOL_MPC_ADD_MEASURE_DESC);
    mMpcAddMeasureAction = xp_tool_add_measure.act;
    mMpcAddMeasureAction->setEnabled(false);
    xp_tool_add_measure.men = MENU_MPC_REPORT;
    menusItems()->append(xp_tool_add_measure);
    xp_tool_add_measure.men = TOOLBAR_MPC;
    toolbarItems()->append(xp_tool_add_measure);
    connect(xp_tool_add_measure.act, &QAction::triggered, this, [=]{SelectTool_Slot(TOOL_MPC_ADD_MEASURE);});

}

void MdiFitImage::MPC_Mpcorb_Slot(bool dont_prompt)
{
    //Listas de archivos a descargar
    QStringList src_urls, dst_files, mpname, are_updated;
    src_urls << "https://www.minorplanetcenter.net/iau/MPCORB/CometEls.txt";
    dst_files << XGlobal::GetSettingsPath(FILE_COMETS);
    mpname << "Comets";
    src_urls << "https://www.minorplanetcenter.net/iau/MPCORB/NEA.txt";
    dst_files << XGlobal::GetSettingsPath("NEA.txt");
    mpname << "Near Earth Asteroids (NEAs)";
    src_urls << "https://www.minorplanetcenter.net/iau/MPCORB/PHA.txt";
    dst_files << XGlobal::GetSettingsPath("PHA.txt");
    mpname << "Potentially Hazardous Asteroids (PHAs)";
    src_urls << "https://www.minorplanetcenter.net/iau/MPCORB/Distant.txt";
    dst_files << XGlobal::GetSettingsPath("Distant.txt");
    mpname << "TNOs, Centaurs and SDOs";
    src_urls << "https://www.minorplanetcenter.net/iau/MPCORB/Unusual.txt";
    dst_files << XGlobal::GetSettingsPath("Unusual.txt");
    mpname << "Other unusual objects";
    src_urls << "https://www.minorplanetcenter.net/iau/MPCORB/MPCORB.DAT";
    dst_files << XGlobal::GetSettingsPath(FILE_MPCORB);
    mpname << "Minor planets";   
    src_urls << "https://www.minorplanetcenter.net/iau/MPCORB/DAILY.DAT";
    dst_files << XGlobal::GetSettingsPath("DAILY.DAT");
    mpname << "Orbits from the latest DOU MPEC";

    //Comprobar status de los archivos con respecto a status actual
    if (mFileDownloader) delete mFileDownloader;
    mFileDownloader = new FileDownloader();
    mFileDownloader->setFiles(src_urls, dst_files);
    mFileDownloader->setOnlyHeaders(true);
    //Incluimos la cabecera con la fecha de todos los archivos para solicitarlos solo en ese caso
    for (int i=0; i<dst_files.count(); i++)
    {
        if (!QFile(dst_files[i]).exists())
            mFileDownloader->addHeaders()->append(QStringList());
        else
        {
            QDateTime dtmod = QFileInfo(dst_files[i]).lastModified().addSecs(-120);
            QStringList dtstring;
            //Fri, 07 Aug 2015 02:28:36 GMT
            //No estoy agregando esta cabecera porque con ella no recibimos el content-length
            dtstring << "If-Modified-Since" << Util::HttpDate(dtmod.toUTC());
            mFileDownloader->addHeaders()->append(dtstring);
        }
    }
    ProgressDialog prgd(this,"Checking Minos Planet Center orbit files");
    prgd.SetIcon(":/graph/images/icn_download.svg");
    prgd.Connect(mFileDownloader);
    prgd.ExecInBackground(FileDownloader::Background_Download, mFileDownloader);
    if (mFileDownloader->cancelled()) return;
    if (mFileDownloader->error().length())
    {
        QMessageBox::warning(this, "Error", mFileDownloader->error());
        return;
    }
    //Mostrar y componer mensaje al usuario teniendo en cuenta la fecha del archivo local y del archivo web
    QString msg = "";
    if (!dont_prompt)
    {
        for (int i=0; i<src_urls.count(); i++)
        {
            QFile flocal(dst_files[i]);
            bool up_to_date = flocal.exists() && (mFileDownloader->httpStatus(i) == 304);

            if (i > 0) msg += "<br/><br/>";
            if (!QFile(dst_files[i]).exists())
                msg += QString("%1: file (%2) <b style='color:#F00;'>does not exist</b>").arg(mpname[i]).arg(QFileInfo(dst_files[i]).fileName());
            else if (!up_to_date)
                msg += QString("%1: file (%2) <b style='color:#00F;'>new version available</b>").arg(mpname[i]).arg(QFileInfo(dst_files[i]).fileName());
            else if (mFileDownloader->httpStatus(i) == 304)
            {
                msg += QString("%1: file (%2) <b style='color:#0E0;'>is up to date</b>").arg(mpname[i]).arg(QFileInfo(dst_files[i]).fileName());
                are_updated << src_urls[i];
            }
            else
                msg += QString("%1: file (%2) <b style='color:#00F;'>unknown status (%3)</b>").arg(mpname[i]).arg(QFileInfo(dst_files[i]).fileName()).arg(mFileDownloader->httpStatus(i));
        }
        msg += "<br/><br/>";
        if (are_updated.count() == dst_files.count())
            msg += "<b>No updates required</b>";
        else
            msg += QString("<b>%1 updates required</b>").arg(dst_files.count()-are_updated.count());
        //Messagebox con botones personalizados
        QMessageBox msgBox;
        msgBox.setText(msg);
        msgBox.setWindowTitle("MPC orbit file checker");
        msgBox.setWindowIcon(QIcon(":/graph/images/icn_asteroid.svg"));
        msgBox.setInformativeText("What do you want to do?");
        msgBox.setIcon(QMessageBox::Question);
        msgBox.addButton("Update all", QMessageBox::YesRole);
        QAbstractButton *updateRequiredBtn = are_updated.count() == dst_files.count() ? NULL : msgBox.addButton("Update required", QMessageBox::YesRole);
        QAbstractButton *closeBtn = msgBox.addButton("Close", QMessageBox::NoRole);
        msgBox.exec();
        QAbstractButton* btn = msgBox.clickedButton();
        if (btn == closeBtn) return;
        //Si solo se han pedido actualizar los actualizables se eliminan de la lista los que no se actualizarán
        if (btn == updateRequiredBtn)
        {
            for (int i=0; i<src_urls.count(); i++)
            {
                if (are_updated.contains(src_urls[i]))
                {
                    src_urls.removeAt(i);
                    dst_files.removeAt(i);
                    mpname.removeAt(i);
                    i--;
                }
            }
        }
    }
    //Finalmente descarga de todos los archivos elegidos + obscodes.html
    src_urls << "http://www.minorplanetcenter.net/iau/lists/ObsCodes.html";
    dst_files << XGlobal::GetSettingsPath("ObsCodes.html");
    mpname << "Observatory codes";

    if (mFileDownloader) delete mFileDownloader;
    mFileDownloader = new FileDownloader();
    mFileDownloader->setFiles(src_urls, dst_files);

    ProgressDialog prg(this,"Downloading MPC orbit files");
    prg.SetIcon(":/graph/images/icn_download.svg");
    prg.Connect(mFileDownloader);
    prg.ExecInBackground(FileDownloader::Background_Download, mFileDownloader);
    if (mFileDownloader->cancelled()) return;
    if (mFileDownloader->error().length())
    {
        QMessageBox::warning(this, "Error", mFileDownloader->error());
        return;
    }

    //Se tunean los archivos copiados, localizando dónde se encuentran los saltos de línea
    for (int i=0; i<dst_files.count(); i++)
        MpcOrb::addSearchIndexes(dst_files[i]);

    MpcOrb::clearCache();
    INFO("MPC orbit files downloaded succesfully");
    QMessageBox::information(this, "Information", "The <b style='color:#00F;'>MPC orbit files</b> file were downloaded successfuly");
}

int MdiFitImage::MPC_KnownObjectThread(void *p)
{
    //La primera imagen de todas es la que envia la orden y contiene el ProgressDialog
    ProgressDialog* pd = ((MdiFitImage**)p)[0]->mMpcKnownObjectPd;
    MpcGeoInfo geoinfo = ((MdiFitImage**)p)[0]->mMpcGeoInfo;
    //Creamos una lista con las imágenes a procesar y las fechas de observación
    MdiFitImage** images = (MdiFitImage**)p;
    QVector<MdiFitImage*> imgs; QVector<double> dtobs; QVector<QPointF> centers; QVector<double> sizes;
    for (int i=0; images[i]; i++)
    {
        if (!images[i]->plate()) continue;
        QDateTime dateobs = MpcUtils::GetDateFrame(images[i]->fitimage()->headers());
        if (dateobs == INVALID_DATETIME) continue;
        imgs.append(images[i]);
        dtobs.append(Util::JulianDate(dateobs));
        imgs[i]->mMpcKnownObjs.clear();
        double center_ra, center_dec;
        images[i]->map(images[i]->fitimage()->width()/2, images[i]->fitimage()->height()/2, &center_ra, &center_dec);
        centers.append(QPointF(center_ra, center_dec));
        double sz_img = MAXVAL(images[i]->fitimage()->width(), images[i]->fitimage()->height())*images[i]->plate()->scale()/3600.0;
        sizes.append(sz_img);
    }

    //Apertura de los archivos correspondientes MPCORB.dat y Comets.txt
    QString fname_mpcorb = XGlobal::GetSettingsPath(FILE_MPCORB);
    QString fname_comets = XGlobal::GetSettingsPath(FILE_COMETS);

    QFile fil_mpcorb(fname_mpcorb); if (!fil_mpcorb.open(QFile::ReadOnly)) return 1;
    QFile fil_comets(fname_comets); if (!fil_comets.open(QFile::ReadOnly)) return 1;
    quint64 mpcorb_sz = fil_mpcorb.size();
    quint64 comets_sz = fil_comets.size();
    quint64 total_size = comets_sz + mpcorb_sz;
    emit pd->Setup_Signal(100);

    //Recorrido del fichero
    QVector<int> sindex = MpcOrb::getSearchIndexes(fname_mpcorb);
    char buffer[203]; double ra, dec, ra_topo, dec_topo, x, y; float obsmag;
    Sky::OrbElems elems;
    int checked_items = 0;

    emit pd->Start_Signal();
    //Archivo MPCORB.dat
    //-------------------------------------------------------------------------
    for (int i=0; i<sindex.length(); i++)
    {
        if (!fil_mpcorb.seek(sindex[i])) break;
        emit pd->TitleChange_Signal(QString(FILE_MPCORB " Section: %1").arg(i+1));
        do
        {
            int numread = fil_mpcorb.read(buffer, 203);
            if (numread != 203) break;
            checked_items++;
            if (!Sky::ParseMPElems(buffer, &elems)) break;
            //Recorremos todas las imágenes de la lista para ver si está dentro de ella
            for (int j=0; j<imgs.count(); j++)
            {
                Sky::CometPos(&elems, dtobs[j], &ra, &dec);
                double dist = Util::Adist(ra, dec, centers[j].x(), centers[j].y());
                if (dist > sizes[j]) continue;
                //Es candidato. Comprobamos la distancia cartesiana
                imgs[j]->imap(ra, dec, &x, &y);
                if (x < -KNOWN_OBJECT_MARGIN_PIX || x > imgs[j]->fitimage()->width()+KNOWN_OBJECT_MARGIN_PIX ||
                    y < -KNOWN_OBJECT_MARGIN_PIX || y > imgs[j]->fitimage()->height()+KNOWN_OBJECT_MARGIN_PIX) continue;
                //Antes de insertar se vuelve a calcular su posición pero con magnitud
                Sky::CometPos(&elems, dtobs[j], &ra, &dec, &obsmag);
                buffer[7] = 0;
                //Puede que tengamos latitud y longitud. Podemos calcular coordenadas topocéntricas
                if (geoinfo.isValid())
                {
                    MPC_Topo(dtobs[j], &elems, geoinfo.lat, geoinfo.lng, geoinfo.alt, &ra_topo, &dec_topo);
                    INFO("\tFOUND - JD: %14.6f OBJ: %-7s GEO: %s %s TOPO: %s %s", dtobs[j], PTQ(QString(buffer).left(7).trimmed()),
                         PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)),
                         PTQ(Util::RA2String(ra_topo)), PTQ(Util::DEC2String(dec_topo))
                         );
                    ra = ra_topo;
                    dec = dec_topo;
                }
                else
                {
                    INFO("\tFOUND - JD: %14.6f OBJ: %-7s GEO: %s %s", dtobs[j], PTQ(QString(buffer).left(7).trimmed()),
                         PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)));
                }
                //Añadir el objeto conocido
                imgs[j]->mMpcKnownObjs.append(MpcKnownObject(QString(buffer).left(7).trimmed(), buffer, MPCReportMeasure::MINOR_PLANET, elems));
            }
            if (!(checked_items%1061)) //Usar números primos da aspecto de fluidez :)
            {
                if (pd->cancelled())
                {
                    emit pd->End_Signal();
                    return 2;
                }
                emit pd->Progress_Signal((int)(fil_mpcorb.pos()*100/total_size));
                emit pd->Msg_Signal(QString("Objects checked: %1").arg(checked_items));
            }
        }
        while (!fil_mpcorb.atEnd());
    }
    fil_mpcorb.close();
    //Archivo ComElems.txt
    //-------------------------------------------------------------------------
    emit pd->TitleChange_Signal("ComElems.txt");
    while (fil_comets.readLine(buffer, 203) > 100)
    {
        if (!Sky::ParseCometElems(buffer, &elems)) continue;
        //Recorremos todas las imágenes de la lista para ver si está dentro de ella
        for (int j=0; j<imgs.count(); j++)
        {
            Sky::CometPos(&elems, dtobs[j], &ra, &dec);
            double dist = Util::Adist(ra, dec, centers[j].x(), centers[j].y());
            if (dist > sizes[j]) continue;
            //Es candidato. Comprobamos la distancia cartesiana
            imgs[j]->imap(ra, dec, &x, &y);
            if (x < -KNOWN_OBJECT_MARGIN_PIX || x > imgs[j]->fitimage()->width()+KNOWN_OBJECT_MARGIN_PIX ||
                y < -KNOWN_OBJECT_MARGIN_PIX || y > imgs[j]->fitimage()->height()+KNOWN_OBJECT_MARGIN_PIX) continue;
            //Antes de insertar se vuelve a calcular su posición pero con magnitud
            Sky::CometPos(&elems, dtobs[j], &ra, &dec, &obsmag);
            buffer[7] = 0;
            //Puede que tengamos latitud y longitud. Podemos calcular coordenadas topocéntricas
            if (geoinfo.isValid())
            {
                MPC_Topo(dtobs[j], &elems, geoinfo.lat, geoinfo.lng, geoinfo.alt, &ra_topo, &dec_topo);
                INFO("\tFOUND - JD: %14.6f OBJ: %-7s GEO: %s %s TOPO: %s %s", dtobs[j], PTQ(QString(buffer).left(7).trimmed()),
                     PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)),
                     PTQ(Util::RA2String(ra_topo)), PTQ(Util::DEC2String(dec_topo))
                     );
                ra = ra_topo;
                dec = dec_topo;
            }
            else
            {
                INFO("\tFOUND - JD: %14.6f OBJ: %-7s GEO: %s %s", dtobs[j], PTQ(QString(buffer).left(7).trimmed()),
                     PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)));
            }
            imgs[j]->mMpcKnownObjs.append(MpcKnownObject(QString(buffer).left(7).trimmed(), buffer, MPCReportMeasure::COMET, elems));
        }

        if (!(checked_items%1061)) //Usar números primos da aspecto de fluidez :)
        {
            if (pd->cancelled())
            {
                emit pd->End_Signal();
                return 2;
            }
            emit pd->Progress_Signal((int)((mpcorb_sz + fil_comets.pos())*100/total_size));
            emit pd->Msg_Signal(QString("Objects checked: %1").arg(checked_items));
        }
    }
    fil_comets.close();

    emit pd->End_Signal();
    return 0;
}

void MdiFitImage::MPC_KnownObjs_Slot()
{
    //Comprobar que existen los archivos MPCORB y Comets y si no existen, preguntar por su descarga
    while (!QFileInfo(XGlobal::GetSettingsPath(FILE_COMETS)).exists() || !QFileInfo(XGlobal::GetSettingsPath(FILE_MPCORB)).exists())
    {
         int ret = QMessageBox::question(this,"Files are missing","Some files conatining MPC orbit elements are missing.\n\nDo you want to download them now?", QMessageBox::Yes, QMessageBox::No);
         if (ret == QMessageBox::No) return;
         MPC_Mpcorb_Slot(true);
    }
    //Buscar información del observatorio actual
    mMpcGeoInfo = MainWindow::mainwin->ActiveMPC()?MainWindow::mainwin->ActiveMPC()->latLong():MpcGeoInfo();
    if (!mMpcGeoInfo.isValid())
    {
        //Buscar en el último informe creado
        QFile fil(XGlobal::GetSettingsPath("mpc_last_report.mpc"));
        if (fil.open(QFile::ReadOnly))
        {
            QTextStream ts(&fil);
            MPCReport* report = MPCReport::LoadFromString(ts.readAll());
            fil.close();
            if (report)
            {
                mMpcGeoInfo = report->geoInfo();
                if (!mMpcGeoInfo.isValid())
                {
                    MpcObservatory obs = report->obsInfo();
                    if (obs.isValid())
                    {
                        mMpcGeoInfo.lat = obs.lat();
                        mMpcGeoInfo.lng = obs.lon();
                        mMpcGeoInfo.alt = 0;
                        mMpcGeoInfo.lat_str = Util::LAT2String(mMpcGeoInfo.lat);
                        mMpcGeoInfo.lng_str = Util::LON2tring(mMpcGeoInfo.lng); //Se asegura de que sea válido
                        mMpcGeoInfo.method = obs.code();
                    }
                }
            }
        }
    }
    MpcGeoInfo gi = mMpcGeoInfo;
    if (!gi.isValid()) INFO("Looking for known objects: GEOCENTRIC LOCATION");
    else INFO("Looking for known objects: TOPOCENTRIC LOCATION, LON: %s, LAT: %s, ALT: %dm", PTQ(mMpcGeoInfo.lng_str), PTQ(mMpcGeoInfo.lat_str), mMpcGeoInfo.alt);

    //Preparar los elementos de progreso e hilos de background
    if (mMpcKnownObjectPd) delete mMpcKnownObjectPd;
    mMpcKnownObjectPd = new ProgressDialog(this, "Looking for known objects");
    mMpcKnownObjectPd->ConnectThis();
    QVector<MdiFitImage*> images = MainWindow::mainwin->fitImages(true).toVector();
    //Eliminar aquellas que no tengas astrometría o fecha válida
    for (int i=0; i<images.length(); i++)
    {
        QDateTime date_frame = MpcUtils::GetDateFrame(images[i]->fitimage()->headers());
        if (date_frame == INVALID_DATETIME || !images[i]->fitimage()->plate())
        {
            images.removeAt(i);
            i--;
        }
    }
    images.append(NULL); //Indicador de final
    //Si no hay imágenes que procesar...
    if (images.count() <= 1)
    {
        QMessageBox::warning(this, "Warning", "No images with valid astrometry and/or date.", QMessageBox::Ok);
        return;
    }
    mMpcKnownObjectPd->SetIcon(":/graph/images/planets.svg");
    int ret = mMpcKnownObjectPd->ExecInBackground(MPC_KnownObjectThread, images.data());
    if (mMpcKnownObjectPd->cancelled()) return;
    if (!ret)
    {
        //Ahora añadimos los items a todas las imágenes
        for (int i=0; images[i]; i++)
            images[i]->MPC_KnownObjLayerUpdate();
    }
    SetScale(mScale);
}

void MdiFitImage::MPC_KnownObjLayerUpdate()
{
    if (mKnownObjectsLayer) mKnownObjectsLayer->clearItems(1);
    double ra, dec;
    for (int j=0;j<mMpcKnownObjs.count(); j++)
    {
        QDateTime dtt = MpcUtils::GetDateFrame(mFitImage->headers());
        if (dtt == INVALID_DATETIME) continue;
        double jd_date = Util::JulianDate(dtt);
        if (mMpcGeoInfo.isValid())
            MPC_Topo(jd_date,(Sky::OrbElems*)&(mMpcKnownObjs.at(j).orb_elems), mMpcGeoInfo.lat, mMpcGeoInfo.lng, mMpcGeoInfo.alt, &ra, &dec);
        else
            Sky::CometPos((Sky::OrbElems*)&(mMpcKnownObjs.at(j).orb_elems), jd_date, &ra, &dec);
        MPC_AddKnownObject(mMpcKnownObjs[j].id, ra, dec);
    }
    //Obligamos a recalcular las posiciones de los objetos en la capa ya que son RA-DEC Attatched
    if (mKnownObjectsLayer)
    {
        ShowHideLayer_Slot(mKnownObjectsLayer);
        ShowHideLayer_Slot(mKnownObjectsLayer);
    }
}

void MdiFitImage::MPC_NewReport()
{
    //Comprobar si existiera un MPC report
    QList<QMdiSubWindow*> lst = MainWindow::mainwin->SubWindows(MdiMPC::staticMetaObject.className());
    if (MainWindow::mainwin->ActiveMPC())
    {
        QMessageBox::warning(this, "Error",  "You are editing a previous MPC report.\n\n"
                                             "No more than one report can be edited at the same time.");
        return;
    }
    //Crear la ventana MPC report y el report con las últimas cabeceras guardadas
    MPCReport* report = NULL;
    QFile fil(XGlobal::GetSettingsPath("mpc_last_report.mpc"));
    if (fil.open(QFile::ReadOnly))
    {
        QTextStream ts(&fil);
        report = MPCReport::LoadFromString(ts.readAll());
        fil.close();
        //Eliminar cabeceras que no deben repetirse
        for (int i=0;report && i<report->headers()->count(); i++)
        {
            if (report->headers()->at(i).hdr() == "NUM") report->headers()->data()[i].setText("0");
            else if (report->headers()->at(i).hdr() == "ACK") {report->headers()->removeAt(i); i--;}
        }
    }
    if (!report) report = new MPCReport();
    report->setFilename("");
    MainWindow::mainwin->CreateMpcReport(report);
    report->setDirty(true);
    MainWindow::mainwin->ActiveMPC()->CheckDirty();
    MPC_CheckMenu();
}

void MdiFitImage::MPC_CheckMenu()
{
    bool exists_mpc_report = (bool)MainWindow::mainwin->ActiveMPC();
    mMpcAddMeasureAction->setEnabled(exists_mpc_report && !IS(this,MdiFitBlink));
}


void MdiFitImage::MPC_AddMark(MPCReportMeasure* mea, float radius)
{
    XOverlayLayer* layer;
    if (!(layer = getXlayer(XLAYER_MPC_REPORT)))
    {
        layer = new XOverlayLayer(XLAYER_MPC_REPORT);
        addXLayer(layer, XLAYER_MPC_REPORT_PRIORITY);
        //Estilo para objetos conocidos
        XOverlayStyle* ko_style = layer->addStyle(1);
        ko_style->setBorder(Util::WebColor("#EE5921"),XGROUP_MPC_REPORT_MEASURED,Qt::SolidLine);
        ko_style->setHoverBorderColor(Qt::yellow);
        ko_style->setLabelDist(3);
        ko_style->setHoverBorderColor(Util::WebColor("#CCCC00"));
        XOverlayStyle* cross_style = layer->addStyle(2);
        cross_style->setBorder(Util::WebColor("#EE5921"),XGROUP_MPC_REPORT_MEASURED,Qt::SolidLine);
        cross_style->setHoverBorderColor(Qt::yellow);
        cross_style->setBorderWidth(2);
        cross_style->setLabelDist(3);
        cross_style->setHoverBorderColor(Util::WebColor("#CCCC00"));
    }
    layer->setVisible(true);
    XOverlayItem* it;
    if (radius > 0)
    {
        XOverlayCircle* cir = new XOverlayCircle(layer, XGROUP_MPC_REPORT_MEASURED);
        cir->setRaDecAttatched(true);
        cir->setZoomAttatched(true);
        cir->setRadius(radius < 0 ? 5 : radius);
        cir->setBasePos(mea->ra(), mea->dec());
        cir->setHoverable(mSelectedTool && mSelectedTool->name() == TOOL_MPC_ADD_MEASURE);
        it = cir;
    }
    else
    {
        XOverlayCross* cr = new XOverlayCross(layer, XGROUP_MPC_REPORT_MEASURED);
        cr->setType(CROSSTYPE_X);
        cr->setRadius(20);
        cr->setHoleRadius(10);
        cr->setStyle(layer->styles()->value(2));
        cr->setRaDecAttatched(true);
        cr->setZoomAttatched(true);
        cr->setBasePos(mea->ra(), mea->dec());
        cr->setHoverable(mSelectedTool && mSelectedTool->name() == TOOL_MPC_ADD_MEASURE);
        it = cr;
    }
    MPC_UpdateMark(mea, it);
    SetScale(mScale);
}

void MdiFitImage::MPC_UpdateMark(MPCReportMeasure* mea,XOverlayItem* it)
{
    //Buscamos la marca dentro de la imagen si no nos viniera dada
    if (!it)
    {
        //Buscamos por nombre
        XOverlayLayer* layer  = getXlayer(XLAYER_MPC_REPORT);
        for (int i=0; i<layer->items()->count() && !it; i++)
        {
            if (layer->items()->at(i)->label().contains(mea->id()))
                it = layer->items()->at(i);
        }
        //Buscar por RA-DEC con una diferencia de 3 segundos de arco
        for (int i=0; i<layer->items()->count() && !it; i++)
        {
            if (Util::Adist(mea->ra(),mea->dec(),layer->items()->at(i)->bx(), layer->items()->at(i)->by()) < 8.33e-4)
                it = layer->items()->at(i);
        }
    }
    if (it)
    {
        //Añadir etiquetas a la marca si fuera necesario
        if (mea->type() == MPCReportMeasure::MINOR_PLANET)
        {
            QString tlab;
            MpcOrbRecord rec = MpcOrb::findMP(mea->id());
            if (!mea->isMPTempDesig())
            {
                if (QString("").sprintf("%05d",mea->number()) == mea->id())
                    tlab = QString("(%1)").arg(mea->number());
                else
                    tlab = QString("%1 (%2)").arg(mea->id()).arg(mea->number());
                if (rec.valid() && rec.name().length())
                    tlab += QString(" %1").arg(rec.name());
            }
            else if (rec.valid() && rec.name().length())
                tlab = QString("%1/%2").arg(mea->id(), rec.name());
            else
                tlab = QString("[%1]").arg(mea->id());
            it->setLabel(tlab);
            it->setLabelWhenHover(false);
        }
        else if (mea->type() == MPCReportMeasure::COMET)
        {
            QString tlab;
            MpcOrbRecord rec = MpcOrb::findComet(mea->id());
            if (rec.valid() && rec.name().length())
                tlab = QString("%1-%2").arg(mea->id(), rec.name());
            else
                tlab = QString("[%1]").arg(mea->id());
            it->setLabel(tlab);
            it->setLabelWhenHover(false);
        }
        else
        {
            it->setLabel(mea->id());
            it->setLabelWhenHover(false);
        }
        //Establecer el estado de hoverable o no según si está seleccionada la herramienta
       it->setHoverable(mSelectedTool && mSelectedTool->name() == TOOL_MPC_ADD_MEASURE);
    }
}

void MdiFitImage::MPC_AddKnownObject(const QString objectid, double ra, double dec, float mag)
{
    //Si no existiera la capa la creamos
    if (!mKnownObjectsLayer)
    {
        mKnownObjectsLayer = new XOverlayLayer(XLAYER_MPC_KNOWN_OBJECTS);
        addXLayer(mKnownObjectsLayer, XLAYER_MPC_KNOWN_OBJECTS_PRIORITY);
        //Estilo para objetos conocidos
        XOverlayStyle* ko_style = mKnownObjectsLayer->addStyle(1);
        ko_style->setBorder(Util::WebColor("#CC00FF")/*Morado*/,2,Qt::SolidLine);
        ko_style->setHoverBorderColor(Qt::yellow);
        ko_style->setLabelDist(3);
    }
    mKnownObjectsLayer->setVisible(true);
    XOverlayCross* cross = new XOverlayCross(mKnownObjectsLayer, 1);
    cross->setRaDecAttatched(true);
    cross->setZoomAttatched(true);
    cross->setRadius(20);
    cross->setHoleRadius(10);
    cross->setBasePos(ra, dec);
    cross->setLabel(mag > 5 ? QString("%1 (Mag:%2)").arg(objectid).arg(QString("").sprintf("%0.1f",mag)) : objectid);
    cross->setType(CROSSTYPE_X);
    cross->setLabelWhenHover(false);
}

void MdiFitImage::MPC_Topo(double jd_date,Sky::OrbElems* orbelems,double lat, double lon,double alt,double*ra,double*dec)
{   
    *ra = *dec = INVALID_FLOAT;
    Novas::observer observer;
    Novas::on_surface on_surf;
    Novas::make_on_surface(lat, lon, alt, 25/*Temperatura irrelevante*/, 992/*Presión atmosférica irrelevante*/, &on_surf);
    short ok = Novas::make_observer(1, &on_surf, NULL, &observer);
    Novas::sky_pos out;
    Novas::object object;
    //Objeto tipo 3 -> Creado por mí para la librería novas en object debo pasar la posición y velocidad
    ok = Novas::make_object(3, 1, "DUMMY", NULL, &object);
    Sky::CometPosAndVel(orbelems, jd_date, object.mp_pos, object.mp_vel);
    ok = Novas::place(jd_date, &object, &observer, 0, 3, 0, &out);
    *ra = out.ra;
    *dec = out.dec;
}

