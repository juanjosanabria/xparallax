#include "core.h"
#include "headersdialog.h"
#include "ui_headersdialog.h"
#include "inputdialog.h"
#include "mainwindow.h"
#include <QMessageBox>

#define MARKED_ROW_HARD_COLOR       QColor(255,217,102)
#define MARKED_ROW_COLOR            QColor(255,230,153)

HeadersDialog::HeadersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HeadersDialog)
{
    ui->setupUi(this);

    connect(ui->treeWidget, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(DoubleClick_Slot(QModelIndex)));
    connect(ui->buttonAddHeader, SIGNAL(clicked()), this, SLOT(AddHeader_Slot()));
    connect(ui->buttonDeleteHeader, SIGNAL(clicked()), this, SLOT(DeleteHeader_Slot()));
    connect(ui->buttonUp, SIGNAL(clicked()), this, SLOT(MoveUp_Slot()));
    connect(ui->buttonDown, SIGNAL(clicked()), this, SLOT(MoveDown_Slot()));
    connect(ui->treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), SLOT(ChangeHeaderIndex_Slot(QTreeWidgetItem*,QTreeWidgetItem*)));

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    mHeaders = NULL;
    MainWindow::DPIAware(this, true);
}

bool HeadersDialog::InList(const char *list[],const QString& str)
{
    for (char **hdr = (char**)list; *hdr; hdr++)
        if (str == *hdr) return true;
    return false;
}

HeadersDialog::~HeadersDialog()
{
    //Borramos toda la memoria que se reservó para las cabeceras
    for (int i=0;i<ui->treeWidget->topLevelItemCount();i++)
    {
        FitHeader* hdr = ui->treeWidget->topLevelItem(i)->data(0, Qt::UserRole).value<FitHeader*>();
        delete hdr;
    }
    delete ui;
}


void HeadersDialog::SetHeaders(FitHeaderList *headers)
{
   ui->treeWidget->clear();
   mHeaders = headers;
   for (int i=0; i<headers->count(); i++)
   {
       FitHeader* hd = new FitHeader(headers->get(i));
       if (hd->isEmpty()) continue;

       QTreeWidgetItem* item = new QTreeWidgetItem();
       item->setText(0,hd->name());
       item->setText(1,hd->value());
       item->setText(2,hd->comment());
       if (hd->comment().length())
            item->setToolTip(2,hd->comment());
       QVariant v;
       v.setValue(hd);
       item->setData(0, Qt::UserRole, v );
       SetItemColor(item, hd->name());
       ui->treeWidget->addTopLevelItem(item);
   }
   ui->treeWidget->resizeColumnToContents(0);
   ui->treeWidget->setColumnWidth(1, ui->treeWidget->font().pointSizeF()*27);
}

void HeadersDialog::SetItemColor(QTreeWidgetItem* item, const QString& hd)
{
    if (FitHeaderList::IsMainHeader(hd))
    {
        QFont fnt = item->font(0);
        fnt.setBold(true);
        item->setFont(0, fnt);
    }
    else if (InList(FitHeaderList::S_HEADERS_ASTROMETRY,hd))
    {
         item->setForeground(0, QBrush(QColor::fromRgb(0,150,0)));
    }
    else if (InList(FitHeaderList::S_HEADERS_XASTROMETRY,hd))
    {
         item->setForeground(0, QBrush(QColor::fromRgb(0,0,150)));
    }
    else if (InList(FitHeaderList::S_HEADERS_ASTROMETRY_EXTRA,hd))
    {
         item->setForeground(0, QBrush(QColor::fromRgb(150,0,0)));
    }
}

FitHeader* HeadersDialog::ExistsHeader(const QString& name)
{
    for (int i=0; i<mHeaders->count(); i++)
    {
        FitHeader* hdr = ui->treeWidget->topLevelItem(i)->data(0, Qt::UserRole).value<FitHeader*>();
        if (hdr->name() == name) return hdr;
    }
    return NULL;
}

void HeadersDialog::DoubleClick_Slot(QModelIndex)
{
    if ( ui->treeWidget->selectedItems().count() <= 0 ) return;
    int index = ui->treeWidget->indexOfTopLevelItem(ui->treeWidget->selectedItems().at(0));
    FitHeader* hdr = ui->treeWidget->topLevelItem(index)->data(0, Qt::UserRole).value<FitHeader*>();

    FitHeader old_header(hdr->name(),hdr->value(),hdr->comment());
    if (InList(FitHeaderList::S_HEADERS_MAIN, hdr->name()) && hdr->name() != "BITPIX")
    {
        QMessageBox::information(this,"Unable to modify header.",
                                 QString("It is not allowed to modify the '%1' header").arg(hdr->name()),
                                 QMessageBox::Ok
                                 );
        return;
    }
    InputDialog* idialog = new InputDialog(this);
    idialog->SetTitle(QString("Edit header '%1'").arg(hdr->name()));
    idialog->SetSubTitle((hdr->name().length() && FitHeaderList::IsStandardHeader(hdr->name())
                          ? "<b>Standard: </b>" : "<b>Non Standard: </b>") +
                         FitHeaderList::HeaderComments(hdr->name()));

    if (hdr->name() == "BITPIX")
    {
        QList<QString> names; QList<QVariant> values;
        names.append("Integer: 8 bits"); values.append(8);
        names.append("Integer: 16 bits"); values.append(16);
        names.append("Integer: 32 bits"); values.append(32);
        names.append("Floating point: IEEE 32 bits"); values.append(-32);
        QVariant bp = idialog->ShowList("Pixel data format",names,values, hdr->value().toInt() );
        //Cambiar la cabecera
        if (!bp.isNull() && bp.toString().length())
            hdr->setValue(bp.toString());
    }
    else
    {
        //Se permite la edición de valor y comentario
        QStringList result=

       //idialog->ShowTextAndBlob2("","Header value",hdr->value(),"Comments",hdr->comment());
       idialog->ShowTextAndBlob("Header value",hdr->value(),"Comments",hdr->comment());

        if (result.count() == 2)
        {
            hdr->setValue(result[0].trimmed());
            hdr->setComment(result[1].trimmed());
        }
    }

    bool changed = false;
    //Actualizar el valor en el treeview si hubiera cambiado algo
    if (hdr->name() != old_header.name())
    {
       ui->treeWidget->topLevelItem(index)->setText(0, hdr->name());
       changed  = true;
    }
    if (hdr->value() != old_header.value())
    {
       ui->treeWidget->topLevelItem(index)->setText(1, hdr->value());
       changed  = true;
    }
    if (hdr->comment() != old_header.comment())
    {
        ui->treeWidget->topLevelItem(index)->setText(2,hdr->comment());
        ui->treeWidget->topLevelItem(index)->setToolTip(2,hdr->comment());
        changed  = true;
    }
    if (changed)
    {
        QVariant v;
        v.setValue(hdr);
        ui->treeWidget->topLevelItem(index)->setData(0, Qt::UserRole, v);
    }
    delete idialog;
}

bool HeadersDialog::IsModified()
{
    char sbuffer1[81], sbuffer2[81];
    //Comparamos las 2 listas.
    //Para empezar, los contadores deben coincidir
    if (ui->treeWidget->topLevelItemCount() != mHeaders->count()) return true;

    for (int i=0; i<mHeaders->count(); i++)
    {
        FitHeader* hdr = ui->treeWidget->topLevelItem(i)->data(0, Qt::UserRole).value<FitHeader*>();
        hdr->toString(sbuffer1);
        FitHeader fh2 = mHeaders->get(i);
        fh2.toString(sbuffer2);
        if (strcmp(sbuffer1, sbuffer2)) return true;
    }
    return false;
}

void HeadersDialog::accept()
{
    if (ui->editFind->hasFocus() && ui->buttonNext->isEnabled())
    {
        on_buttonNext_clicked();
        return;
    }
    if (IsModified())
    {
        mHeaders->Clear();
        for (int i=0; i<ui->treeWidget->topLevelItemCount(); i++)
        {
            FitHeader* hdr = ui->treeWidget->topLevelItem(i)->data(0, Qt::UserRole).value<FitHeader*>();
            hdr->resetID();
            mHeaders->AddHeader(*hdr);
        }
    }
    QDialog::accept();
}

void HeadersDialog::reject()
{
    if (IsModified())
    {
        int ret =
        QMessageBox::question(this,
                              "Do you want to close the headers window?",
                              "Some headers were changed and you will loose this changes\n\nAre you sure?",
                              QMessageBox::Yes|QMessageBox::Cancel
                              );
        if (ret == QMessageBox::Cancel) return;
    }
    QDialog::reject();
}



void HeadersDialog::AddHeader_Slot()
{
    InputDialog idialog(this);
    idialog.SetImage(QPixmap(":/graph/images/icn_plus.svg"));
    idialog.SetTitle("Create a new header");
    idialog.SetSubTitle("Please, fill in the name and value fields.");
    QStringList names = QStringList()  << "Name" << "Value" << "Comments";
    QStringList values  = QStringList()  << "[UPPERCASE]" << "" << "";
    QStringList inputmask  = QStringList()  << "xxxxxxxx" << "" << "";

    QStringList result = idialog.ShowMultipleTexts(names, values, inputmask);
    if (result.count() == 0) return;

    //Normalizamos el nombre
    result[0] = result.at(0).trimmed().toUpper();

    if (!result.at(0).length())
    {
        QMessageBox::information(this,"Unable to add header.",
                             "Name is empty.\n\n Please, add a header name in the insert dialog box.",
                             QMessageBox::Ok
                             );
        return;
    }
    if (InList(FitHeaderList::S_HEADERS_MAIN, result[0]))
    {
        QMessageBox::information(this,"Insert header denied",
                                 QString("You can't add the header: '%1'.\n\n This header is mandatory and already exists").arg(result[0]),
                                 QMessageBox::Ok
                                 );
        return;
    }

    //Si la cabecera ya existe preguntamos que si quiere sobreescribirla
    FitHeader* fh = ExistsHeader(result.at(0));
    if (fh && fh->isData())
    {
        int dres =  QMessageBox::information(this,"Confirm replace",
                                               QString("Header '%1' already exists.\n\n Do you want to replace it with the new value?").arg(result[0]),
                                               QMessageBox::Yes | QMessageBox::No
                                               );
        if (dres == QMessageBox::No) return;
        //Reemplazar la cabecera con la nueva
        for (int i=0; i<ui->treeWidget->topLevelItemCount(); i++)
        {
            QTreeWidgetItem* qitem = ui->treeWidget->topLevelItem(i);
            FitHeader* hdr = qitem->data(0, Qt::UserRole).value<FitHeader*>();
            if (hdr->name() != result.at(0)) continue;
            hdr->setName(result.at(0));
            hdr->setValue(result.at(1));
            hdr->setComment(result.at(2));
            //Cambiar el item también
            qitem->setText(1, hdr->value());
            qitem->setText(2, hdr->comment());
            break;
        }
        return;
    }
    FitHeader* hd = new FitHeader(result.at(0), result.at(1), result.at(2));
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,hd->name());
    item->setText(1,hd->value());
    item->setText(2,hd->comment());
    if (hd->comment().length())
         item->setToolTip(2,hd->comment());
    QVariant v;
    v.setValue(hd);
    item->setData(0, Qt::UserRole, v );
    SetItemColor(item, hd->name());
    ui->treeWidget->addTopLevelItem(item);
    ui->treeWidget->setCurrentItem(item);
    IsModified();
}

void HeadersDialog::DeleteHeader_Slot()
{
    ClearSearch(true);
    if (!ui->treeWidget->selectedItems().count()) return;
    int index = ui->treeWidget->indexOfTopLevelItem(ui->treeWidget->selectedItems().at(0));
    if (index == -1) return;
    FitHeader* hdr = ui->treeWidget->topLevelItem(index)->data(0, Qt::UserRole).value<FitHeader*>();
    if (InList(FitHeaderList::S_HEADERS_MAIN, hdr->name()))
    {
        QMessageBox::information(this,"Delete denied",
                                 QString("You can't remove the header: '%1', it is mandatory.").arg(hdr->name()),
                                 QMessageBox::Ok
                                 );
        return;
    }
    else if (InList(FitHeaderList::S_HEADERS_ASTROMETRY, hdr->name()))
    {
       int dres = QMessageBox::warning(this,"Astrometic reduction header",
                                 QString("If you delete this header, the astrometric reduction information will be lost."
                                    "\n\n Do you want to delete all other astrometric reduction headers?").arg(hdr->name()),
                                  QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel
                                 );
       if (dres == QMessageBox::Cancel) return;
       if (dres == QMessageBox::Yes)
       {
           DeleteAllHeaders(FitHeaderList::S_HEADERS_ASTROMETRY);
           return;
       }
    }

    else if (InList(FitHeaderList::S_HEADERS_XASTROMETRY, hdr->name()))
    {
       int dres = QMessageBox::warning(this,"Astrometic reduction header",
                                 QString("If you delete this header, the astrometric reduction information will be lost."
                                    "\n\n  Do you want to delete all other astrometric reduction headers?").arg(hdr->name()),
                                  QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel
                                 );
       if (dres == QMessageBox::Cancel) return;
       if (dres == QMessageBox::Yes)
       {
           DeleteAllHeaders(FitHeaderList::S_HEADERS_XASTROMETRY);
           return;
       }
    }
    delete ui->treeWidget->topLevelItem(index);
    delete hdr;
}

void HeadersDialog::ChangeHeaderIndex_Slot(QTreeWidgetItem* newitem, QTreeWidgetItem* /*olditem*/)
{
    if (!newitem)
    {
        ui->buttonDeleteHeader->setEnabled(false);
        return;
    }
    FitHeader* hdr = newitem->data(0, Qt::UserRole).value<FitHeader*>();
    if (InList(FitHeaderList::S_HEADERS_MAIN, hdr->name()))
    {
        ui->buttonDeleteHeader->setEnabled(false);
        ui->buttonUp->setEnabled(false);
        ui->buttonDown->setEnabled(false);
        return;
    }
    ui->buttonDeleteHeader->setEnabled(newitem);
    //Botón arriba y abajo
    int idx = ui->treeWidget->currentIndex().row();
    int cont = ui->treeWidget->topLevelItemCount();
    if (idx < 0)
    {
        ui->buttonUp->setEnabled(false);
        ui->buttonDown->setEnabled(false);
        return;
    }
    if (idx == 0)
    {
        ui->buttonUp->setEnabled(false);
        ui->buttonDown->setEnabled(idx < cont-1);
    }
    else
    {
        QTreeWidgetItem* last = ui->treeWidget->topLevelItem(idx-1);
        FitHeader* hdr_last = last->data(0, Qt::UserRole).value<FitHeader*>();
        bool unmodif = InList(FitHeaderList::S_HEADERS_MAIN, hdr_last->name());
        ui->buttonUp->setEnabled(!unmodif);
        ui->buttonDown->setEnabled(idx < cont-1);
    }
}

void HeadersDialog::MoveUp_Slot()
{
    int idx = ui->treeWidget->currentIndex().row();
    QTreeWidgetItem* item = ui->treeWidget->topLevelItem(idx);
    FitHeader* hd = item->data(0, Qt::UserRole).value<FitHeader*>();
    delete item;
    item = new QTreeWidgetItem();
    item->setText(0,hd->name());
    item->setText(1,hd->value());
    item->setText(2,hd->comment());
    if (hd->comment().length())
         item->setToolTip(2,hd->comment());
    QVariant v;
    v.setValue(hd);
    item->setData(0, Qt::UserRole, v );
    SetItemColor(item, hd->name());
    ui->treeWidget->insertTopLevelItem(idx-1, item);
    ui->treeWidget->setCurrentItem(item);
}

void HeadersDialog::MoveDown_Slot()
{
    int idx = ui->treeWidget->currentIndex().row();
    QTreeWidgetItem* item = ui->treeWidget->topLevelItem(idx);
    FitHeader* hd = item->data(0, Qt::UserRole).value<FitHeader*>();
    delete item;
    item = new QTreeWidgetItem();
    item->setText(0,hd->name());
    item->setText(1,hd->value());
    item->setText(2,hd->comment());
    if (hd->comment().length())
         item->setToolTip(2,hd->comment());
    QVariant v;
    v.setValue(hd);
    item->setData(0, Qt::UserRole, v );
    SetItemColor(item, hd->name());
    ui->treeWidget->insertTopLevelItem(idx+1, item);
    ui->treeWidget->setCurrentItem(item);
}


void HeadersDialog::DeleteAllHeaders(const char *list[])
{
    while (*list)
    {
        for (int i=0; i<ui->treeWidget->topLevelItemCount(); i++)
        {
            QTreeWidgetItem* qwi = ui->treeWidget->topLevelItem(i);
            FitHeader* hdr = qwi->data(0, Qt::UserRole).value<FitHeader*>();
            if (hdr->name() == *list)
            {
                delete qwi;
                delete hdr;
                i = 0;
            }
        }
        list++;
    }
}

void HeadersDialog::ClearSearch(bool clear_box)
{
    if (!mMarkedItems.count()) return;
    //Desmarcamos todo lo que hubiera marcado
    for (int i=0; i<mMarkedItems.count(); i++)
        for (int j=0; j<3; j++) mMarkedItems[i]->setBackground(j, mMarkedBrushes[i]);
    mMarkedItems.clear();
    mMarkedBrushes.clear();
    if (clear_box) ui->editFind->setText("");
    ui->buttonNext->setEnabled(false);
}

void HeadersDialog::on_editFind_textChanged(const QString &_txt)
{
   FindAndMarkItems(_txt, 0);
}

void HeadersDialog::FindAndMarkItems(const QString &_txt,int from_index)
{
    ClearSearch(false);
    //Buscamos la cabecera y la selecionamos
    if (_txt.length() < 2) return;
    QString txt = _txt.toUpper();
    //Preferencia en el nombre de la cabecera
    for (int i=from_index; i<ui->treeWidget->topLevelItemCount(); i++)
    {
        QTreeWidgetItem* qwi = ui->treeWidget->topLevelItem(i);
        FitHeader* hdr = qwi->data(0, Qt::UserRole).value<FitHeader*>();
        if (hdr->name().toUpper().contains(txt))
        {
            mMarkedItems.append(qwi);
            mMarkedBrushes.append(qwi->background(0));
            qwi->setBackground(0, QBrush(MARKED_ROW_HARD_COLOR));
            qwi->setBackground(1, QBrush(MARKED_ROW_COLOR));
            qwi->setBackground(2, QBrush(MARKED_ROW_COLOR));
            ui->treeWidget->setCurrentItem(qwi);
            //Comprobar si hay siguiente
            for (int j=i+1; j<ui->treeWidget->topLevelItemCount(); j++)
            {
                qwi = ui->treeWidget->topLevelItem(j);
                hdr = qwi->data(0, Qt::UserRole).value<FitHeader*>();
                if (hdr->name().toUpper().contains(txt))
                {
                    ui->buttonNext->setEnabled(true);
                    break;
                }
            }
            return;
        }
    }
    //Continuamos por el valor
    for (int i=from_index; i<ui->treeWidget->topLevelItemCount(); i++)
    {
        QTreeWidgetItem* qwi = ui->treeWidget->topLevelItem(i);
        FitHeader* hdr = qwi->data(0, Qt::UserRole).value<FitHeader*>();
        if (hdr->value().toUpper().contains(txt))
        {
            mMarkedItems.append(qwi);
            mMarkedBrushes.append(qwi->background(1));
            qwi->setBackground(0, QBrush(MARKED_ROW_COLOR));
            qwi->setBackground(1, QBrush(MARKED_ROW_HARD_COLOR));
            qwi->setBackground(2, QBrush(MARKED_ROW_COLOR));
            ui->treeWidget->setCurrentItem(qwi);
            //Comprobar si hay siguiente
            for (int j=i+1; j<ui->treeWidget->topLevelItemCount(); j++)
            {
                qwi = ui->treeWidget->topLevelItem(j);
                hdr = qwi->data(0, Qt::UserRole).value<FitHeader*>();
                if (hdr->value().toUpper().contains(txt))
                {
                    ui->buttonNext->setEnabled(true);
                    break;
                }
            }
            return;
        }
    }
    //Continuamos por el comentarios
    for (int i=from_index; i<ui->treeWidget->topLevelItemCount(); i++)
    {
        QTreeWidgetItem* qwi = ui->treeWidget->topLevelItem(i);
        FitHeader* hdr = qwi->data(0, Qt::UserRole).value<FitHeader*>();
        if (hdr->comment().toUpper().contains(txt))
        {
            mMarkedItems.append(qwi);
            mMarkedBrushes.append(qwi->background(2));
            qwi->setBackground(0, QBrush(MARKED_ROW_COLOR));
            qwi->setBackground(1, QBrush(MARKED_ROW_COLOR));
            qwi->setBackground(2, QBrush(MARKED_ROW_HARD_COLOR));
            ui->treeWidget->setCurrentItem(qwi);
            //Comprobar si hay siguiente
            for (int j=i+1; j<ui->treeWidget->topLevelItemCount(); j++)
            {
                qwi = ui->treeWidget->topLevelItem(j);
                hdr = qwi->data(0, Qt::UserRole).value<FitHeader*>();
                if (hdr->comment().toUpper().contains(txt))
                {
                    ui->buttonNext->setEnabled(true);
                    break;
                }
            }
            return;
        }
    }
}

void HeadersDialog::on_buttonNext_clicked()
{
    int idx_start =  ui->treeWidget->indexOfTopLevelItem(mMarkedItems[0]);
    if (idx_start < -1) return;
    FindAndMarkItems(ui->editFind->text(), idx_start+1);
}

