#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>
#include <QFuture>
#include <QThread>
#include <QSemaphore>

#ifndef QT_DEBUG
    #define UPDATE_URL "https://www.xparallax.com/func_ws.php?f=CHECK_UPDATE"
#else
    #define UPDATE_URL "https://www.xparallax.com/func_ws.php?f=CHECK_UPDATE"
#endif


struct XUpdateStruct{
    int     httpStatus;                     //Status que estará a 200 si hay una actualización disponible
    QString version;                        //Nueva versión disponible
    QString osName;
    int     osBits;
    QString filename;
    XUpdateStruct(){httpStatus = 500;}
};


namespace Ui {
class AboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit AboutDialog(QWidget *parent = 0);
    ~AboutDialog();

    static void CheckForUpdates(bool userRequested);
    static bool versionIsUpper(QString& a, QString& b);
    static void cancelUPdateCheck();
    static const XUpdateStruct& updateData(){return mUpdateData;}
    static bool updRequestedByUser(){return mUpdateRequestedbyUser;}

protected:
    void run();

private:
    Ui::AboutDialog *          ui;

    static void                UpdateThread();
    static bool                mUpdateCancelled;
    static bool                mUpdateRequestedbyUser;
    static XUpdateStruct       mUpdateData;
    static QMutex              mUpdateMutex;
    static QThread*            mUpdTrhead;
    static QSemaphore          mUpdSem;

public slots:
    void VisitWeb_Slot(QString url);
    void NoNewerVersion_Slot();
};

#endif // ABOUTDIALOG_H
