#include "mainwindow.h"
#include <QtGui>
#include <QComboBox>
#include <QLabel>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QDesktopServices>
#include <QPluginLoader>
#include <QLayout>
#include <QSvgRenderer>
#include <QDesktopWidget>

#include "xglobal.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "calibrationdialog.h"
#include "astrometrydialog.h"
#include "imagecatalogdialog.h"
#include "aboutdialog.h"
#include "background.h"
#include "mdifitimage.h"
#include "inputdialog.h"
#include "fitimage.h"
#include "detection.h"
#include "vo.h"
#include "util.h"
#include "experimental.h"
#include "ui_mdi_childs/mdilogger.h"
#include "ui_mdi_childs/mdimpc.h"
#include "time.h"
#include "modext/blink/blinkdialog.h"
#include "modext/blink/mdifitblink.h"
#include "imgutils/batchheaderdialog.h"
#include "tools/angulardistance.h"
#include "tools/coordinatetransform.h"

MainWindow* MainWindow::mainwin = NULL;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mainwin = this;
    mDesktopWidget = new QDesktopWidget();
    #ifdef QT_DEBUG
    mTestAction2 = mTestAction3 = mTestAction4 = mTestAction5 = mTestAction6 = mTestAction7 =
            mTestAction8 = mTestAction9
            = NULL;
    #endif
    CreateMDIArea();
    CreateActions();
    CreateMenus();
    CreateToolBars();
    CreateStatusBar();
    UpdateMenus();
    UpdateWindowMenu();
    InterfaceMdiFit_DeActivate(NULL);
    if (XGlobal::APP_SETTINGS.mainwindow_maximized)
       setWindowState(windowState() | Qt::WindowMaximized);
    if (!(windowState() & Qt::WindowMaximized))
    {
        if (!XGlobal::APP_SETTINGS.mainwindow_size.isEmpty())
            resize(XGlobal::APP_SETTINGS.mainwindow_size);
        if (XGlobal::APP_SETTINGS.mainwindow_pos.x() >= 0 && XGlobal::APP_SETTINGS.mainwindow_pos.y() >= 0)
            move(XGlobal::APP_SETTINGS.mainwindow_pos);
    }

    //Lanzar el procso de actualizaciones
    connect(this, SIGNAL(UpdateFound_Signal(bool, bool)), this, SLOT(UpdateFound_Slot(bool, bool)));

    AboutDialog::CheckForUpdates(false);

    mActiveWindow = NULL;
    mExperimental->setShown(false);
    MdiFit_Load();                  //Carga las extensiones de Mdifit
    SetIcoDPI(this);

    //Menú experimental
    mExperimental = new Experimental(this);
    mExperimental->CreateExperimentalMenu();
}


MainWindow::~MainWindow()
{
    AboutDialog::cancelUPdateCheck();
    XGlobal::APP_SETTINGS.mainwindow_maximized = (windowState() & Qt::WindowMaximized);
    XGlobal::APP_SETTINGS.show_log_window = ActiveLogger();
    if (!(windowState() & Qt::WindowMaximized))
    {
        XGlobal::APP_SETTINGS.mainwindow_size = size();
        XGlobal::APP_SETTINGS.mainwindow_pos = pos();
    }
    if (mDesktopWidget) delete mDesktopWidget;
    mDesktopWidget = NULL;
    delete ui;
}


void MainWindow::CreateMenus()
{
     ui->menuBar->setMinimumHeight(24);
     mFileMenu = getMenuByName(MENU_FILE, true, NULL, MENU_FILE_PRIORITY);
     mFileMenu->addAction(mFileOpenAction);
     mFileMenu->addAction(mSaveAction);
     mFileMenu->addAction(mSaveAsAction);
     mFileMenu->addSeparator()->setProperty(XPRIORITY, ACTION_EXIT_PRIORITY-1);
     mFileMenu->addAction(mExitAction);

     mWindowMenu = getMenuByName(MENU_ENVIRONMENT, true, NULL, MENU_ENVIRONMENT_PRIORITY);
     UpdateWindowMenu();


     ui->menuBar->addSeparator();
     //Menú de imagen
     //---------------------------------
     mImageMenu = getMenuByName(MENU_IMAGE_ANALYSIS, true, NULL, MENU_IMAGE_ANALYSIS_PRIORITY);
     mImageMenu->addAction(mCalibrateAction);
     mImageMenu->addAction(mAstrometryAction);
     mImageMenu->addAction(mImageCatalogAction);
     mImageMenu->addAction(mBlinkAction);
     //Separador de herramientas que no son de análislis
     mImageMenu->addSeparator();
     mImageMenu->addAction(mBatchHeader);

     //Menú de herramientas
     //---------------------------------
     mToolsMenu = getMenuByName(MENU_TOOLS, true, NULL, MENU_TOOLS_PRIORITY);
     mToolsMenu->addAction(mToolAngularDistanceAction);
     mToolsMenu->addAction(mToolCoordinateConversion);

     ui->menuBar->addSeparator();

     //Menú de ayuda
     //---------------------------------
     mHelpMenu = getMenuByName(MENU_HELP, true, NULL,  MENU_HELP_PRIORITY);
     mHelpMenu->addAction(mHelpIndexAction);
     mHelpMenu->addAction(mSearchForUpdatesAction);
     mHelpMenu->addSeparator();
     mHelpMenu->addAction(mAboutXParallax);
     //mHelpMenu->addAction(mAboutQT);


    #ifdef QT_DEBUG
        mTestMenu = getMenuByName(MENU_TEST, true, NULL, MENU_TEST_PRIORITY);

        if (mTestAction2) mTestMenu->addAction(mTestAction2);
        if (mTestAction3) mTestMenu->addAction(mTestAction3);
        if (mTestAction4) mTestMenu->addAction(mTestAction4);
        if (mTestAction5) mTestMenu->addAction(mTestAction5);
        if (mTestAction6) mTestMenu->addAction(mTestAction6);
        if (mTestAction7) mTestMenu->addAction(mTestAction7);
        if (mTestAction8) mTestMenu->addAction(mTestAction8);
        if (mTestAction9) mTestMenu->addAction(mTestAction9);
   #endif
}

void MainWindow::CreateMDIArea()
{
    //Gestor de eventos de cambio de ventana en el menú
    mWindowMapper = new QSignalMapper(this);
    connect(mWindowMapper, SIGNAL(mapped(QWidget*)),this, SLOT(SetActiveSubwindow_Slot(QWidget*)));

    mMDIArea = new  QMdiArea();
    mMDIArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mMDIArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setCentralWidget(mMDIArea);
    connect(mMDIArea, SIGNAL(subWindowActivated(QMdiSubWindow*)),this, SLOT(ActiveSubwindowChanged_Slot(QMdiSubWindow*)));


    mMDIArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mMDIArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mMDIArea->setAcceptDrops(true);

    //Si tenemos que mostrar la ventana de log la mostramos
    if (XGlobal::APP_SETTINGS.show_log_window)
    {
        MdiLogger* logger =  new MdiLogger(mMDIArea);
        logger->setObjectName("Log window");
        AddMdiSubWindow(logger, ":/graph/images/list-ordered.png");
    }
    INFO(LOG_SEPARATOR);
    INFO("- %s - ready, version %s running on %s %d bits", XPROGRAM, XVERSION, PTQ(XGlobal::OS_TYPE), XGlobal::OS_BITS);
    INFO(LOG_SEPARATOR);
}

void MainWindow::dragEnterEvent(QDragEnterEvent *ev)
{
    QMimeData* data = const_cast<QMimeData*>(ev->mimeData());
    if (!data->hasUrls()) return;
    for (int i=0; i<data->urls().count(); i++)
    {
        QUrl url = data->urls().at(i);
        if (!url.isLocalFile()) return;
        QFileInfo fi(url.toLocalFile());
        if (fi.suffix().toUpper() != "FIT" && fi.suffix().toUpper() != "FITS"  && fi.suffix().toUpper() != "FTS") return;
        fi.size();
    }
    ev->accept();
}

void MainWindow::keyPressEvent(QKeyEvent* ke)
{
    //Pasar el evento de keypress a las distintas ventnas
    if (mActiveWindow && IS(mActiveWindow->widget(), MdiFitImage))
        (qobject_cast<MdiFitImage*>(mActiveWindow->widget()))->keyPressEvent(ke);

    if (mExperimental->shown()) return;
    static size_t  key_index = 0;
    static char perrunilla[]  = "perrunilla";
    static time_t last_time = time(NULL);

    if (time(NULL)-last_time > 5000) key_index = 0;

    if (ke->text() == QString(perrunilla[key_index])) key_index++;
    else key_index = 0;

    //Hemos dado con la palabra clave
    if (key_index == strlen(perrunilla))
    {
        ui->statusBar->showMessage("Perrunilla!!!");
        QMessageBox::warning(this, "Warning",
                                    "This will show the menu '" MENU_EXPERIMENTAL
                                    "'\n\n It was created with developing purposes, use it at your own responsibility.");
        mExperimental->setShown(true);
        mExperimental->getExpMenu()->setTitle(MENU_EXPERIMENTAL);
        mExperimental->getExpMenu()->setEnabled(true);
        XGlobal::DEBUG = true;
        key_index = 0;
    }
    last_time = time(NULL);
}


void MainWindow::dropEvent(QDropEvent *ev)
{
    QMimeData* data = const_cast<QMimeData*>(ev->mimeData());
    if (!data->hasUrls()) return;
    for (int i=0; i<data->urls().count(); i++)
    {
        QUrl url = data->urls().at(i);
        if (!url.isLocalFile()) return;
        QFileInfo fi(url.toLocalFile());
        if (fi.suffix().toUpper() != "FIT" && fi.suffix().toUpper() != "FITS") return;
        OpenFit(fi.filePath());
    }
    ev->accept();
}

void MainWindow::ActiveSubwindowChanged_Slot(QMdiSubWindow*  win)
{
       //Si no ha cambiado nada lo dejamos como está
       if (!win)
       {
          // InterfaceMdiFit_DeActivate(NULL);
          // mActiveWindow = NULL;
           return;
       }

       //Actualizar los menús correspondientes
       UpdateMenus();
       UpdateWindowMenu();

       //ACtivamos los menús propios de esta subventana

       //Ocultamos los menús y botones de la ventna que sale
       if (mActiveWindow && mActiveWindow->widget() && win != mActiveWindow)
           InterfaceMdiFit_DeActivate(qobject_cast<XPMdiWidget*>(mActiveWindow->widget()));

       //Mostramos los iconos y menús de la ventana que entra
       if (win && win->widget() && win != mActiveWindow)
           InterfaceMdiFit_Activate(qobject_cast<XPMdiWidget*>(win->widget()));

       mActiveWindow = win;
}

void MainWindow::SetActiveSubwindow_Slot(QWidget* widget)
{
    if (!widget && !ActiveLogger())
    {
        MdiLogger* logger =  new MdiLogger(mMDIArea);
        AddMdiSubWindow(logger,":/graph/images/list-ordered.png");
        logger->setObjectName("Log window");
        INFO(LOG_SEPARATOR);
        INFO("- " XPROGRAM " - ready, version %s", XVERSION);
        INFO(LOG_SEPARATOR);
        UpdateWindowMenu();
        return;
    }

    QList<QMdiSubWindow*> wins = mMDIArea->subWindowList();
    for (int i=0; i<wins.count(); i++)
        if (wins.at(i) == (QMdiSubWindow*)widget)
        {
           mMDIArea->setActiveSubWindow(wins.at(i));
           if (!wins.at(i)->isVisible()) wins.at(i)->show();
           UpdateWindowMenu();
           return;
        }
}

void MainWindow::ChildClosed_Slot(QObject* obj)
{
    //if (mActiveSubWindow == subwindow) mActiveSubWindow = NULL;
    mStatusScale->setEnabled(false);
    if (mStatusScale->count() > 5) mStatusScale->removeItem(5);

    QMdiSubWindow* subwin = (QMdiSubWindow*)obj;
    if (mActiveWindow == subwin)
    {
        InterfaceMdiFit_DeActivate(NULL);
        mActiveWindow = NULL;
    }

    //Si se ha cerrado la ventana de logger se desactiva su opción dle menú ventana
    if (IS(subwin->widget(), MdiLogger))
    {
        UpdateWindowMenu();
    }
}

void MainWindow::UpdateMenus()
{
    MdiFitImage* activeFit = ActiveFitImage();
    if (activeFit)
    {
        mStatusScale->setEnabled(true);
        //Obtenemos la escala actual y la buscamos en el combo
        int curscale = (int)( activeFit->scale()*100 );
        bool found = false;
        for (int i=0; i<mStatusScale->count(); i++)
            if (mStatusScale->itemData(i) == curscale)
            {
                mStatusScale->setCurrentIndex(i);
                found = true;
            }
        if (!found)
        {
            char buffscale[8];
            float selscale = (activeFit->scale() * 100.0);
            snprintf(buffscale, 8, "%d%%", (int) selscale );
            //Si no tenemos item número 5 lo insertamos
            if (mStatusScale->count() <= 5)
                mStatusScale->addItem("-", (int) selscale );

            mStatusScale->setItemText(5, buffscale);
            mStatusScale->setItemData(5, selscale);
            mStatusScale->setCurrentIndex(5);
        }
    }
    else
    {    
        mStatusScale->setEnabled(false);

    }
    mCloseAction->setEnabled(mMDIArea->activeSubWindow());
    mCloseAllAction->setEnabled(mMDIArea->activeSubWindow());
}

void  MainWindow::CreateToolBars()
{
    mMainToolbar = ui->mainToolBar;
    ui->mainToolBar->setObjectName(TOOLBAR_GENERAL);

    mFileOpenAction->setProperty(XPRIORITY, 1000);
    ui->mainToolBar->addAction(mFileOpenAction);

    mSaveAction->setProperty(XPRIORITY, 1001);
    ui->mainToolBar->addAction(mSaveAction);

    ui->mainToolBar->addSeparator()->setProperty(XPRIORITY, 1002);

    mCalibrateAction->setProperty(XPRIORITY, 2000);
    ui->mainToolBar->addAction(mCalibrateAction);

    mAstrometryAction->setProperty(XPRIORITY, 2001);
    ui->mainToolBar->addAction(mAstrometryAction);

    mImageCatalogAction->setProperty(XPRIORITY, 2003);
    ui->mainToolBar->addAction(mImageCatalogAction);

    mBlinkAction->setProperty(XPRIORITY, 2004);
    ui->mainToolBar->addAction(mBlinkAction);


    mFitToolBar = getToolbarByName(TOOLBAR_FITIMAGE, true);

    //Deshabilita el contextmenu en todos los elementos que heredan directamente de la ventana principal
    setContextMenuPolicy(Qt::NoContextMenu);
}


void  MainWindow::CreateStatusBar()
{
        mStatusXY = new QLabel("Position", this);
        mStatusXY->setMaximumWidth(320);
        mStatusXY->setMinimumWidth(320);
        ui->statusBar->addPermanentWidget(mStatusXY, 0);

        mStatusFlux = new QLabel("i: 0", this);
        mStatusFlux->setMaximumWidth(100);
        mStatusFlux->setMinimumWidth(100);
        ui->statusBar->addPermanentWidget(mStatusFlux, 0);

        QLabel*     empty = new QLabel("", this);
        empty->setMaximumWidth(128);
        empty->setMinimumWidth(128);
        ui->statusBar->addPermanentWidget(empty, 0);

        mStatusScale =  new QComboBox(ui->statusBar);
        mStatusScale->addItem("100%  ", 100);
        mStatusScale->addItem(" 75%  ", 75);
        mStatusScale->addItem(" 50%  ", 50);
        mStatusScale->addItem(" 25%  ", 25);
        mStatusScale->addItem(" 10%  ", 10);
        ui->statusBar->addPermanentWidget(mStatusScale);
        connect(mStatusScale, SIGNAL(currentIndexChanged(int)), this, SLOT(FitScaleChange_Slot()));

        ui->statusBar->showMessage("Ready...");
}


void MainWindow::CreateActions()
{
        //Acciones principales
        mFileOpenAction =  new QAction(QIcon(ACTION_OPEN_ICON), ACTION_OPEN, this);
        mFileOpenAction->setObjectName(ACTION_OPEN);
        mFileOpenAction->setProperty(XSTATICACTION, true);
        mFileOpenAction->setProperty(XPRIORITY, ACTION_OPEN_PRIORITY);
        mFileOpenAction->setShortcuts(QKeySequence::Open);
        mFileOpenAction->setStatusTip(ACTION_OPEN_DESC);
        connect(mFileOpenAction, SIGNAL(triggered()), this, SLOT(FileOpen_Slot()));

        //Acción estática guardar
        mSaveAction = new QAction(QIcon(ACTION_SAVE_ICON), ACTION_SAVE, this);
        mSaveAction->setObjectName(ACTION_SAVE);
        mSaveAction->setProperty(XSTATICACTION, true);
        mSaveAction->setProperty(XPRIORITY, ACTION_SAVE_PRIORITY);
        mSaveAction->setEnabled(false);
        mSaveAction->setShortcuts(QKeySequence::Save);
        mSaveAction->setStatusTip(ACTION_SAVE_DESC);

        //Acción estática guardar como
        mSaveAsAction = new QAction(ACTION_SAVEAS, this);
        mSaveAsAction->setObjectName(ACTION_SAVEAS);
        mSaveAsAction->setProperty(XSTATICACTION, true);
        mSaveAsAction->setProperty(XPRIORITY, ACTION_SAVEAS_PRIORITY);
        mSaveAsAction->setEnabled(false);
        mSaveAsAction->setShortcuts(QKeySequence::SaveAs);
        mSaveAsAction->setStatusTip(ACTION_SAVEAS_DESC);


        //Acción de salir
        mExitAction = new QAction(QIcon(ACTION_EXIT_ICON), ACTION_EXIT, this);
        mExitAction->setObjectName(ACTION_EXIT);
        mExitAction->setProperty(XPRIORITY, ACTION_EXIT_PRIORITY);
        mExitAction->setShortcuts(QKeySequence::Quit);
        mExitAction->setStatusTip(ACTION_EXIT_DESC);
        connect(mExitAction, SIGNAL(triggered()), this, SLOT(Exit_Slot()));


        mCloseAction = new QAction(QIcon(":/graph/images/cross.png"),"Close", this);
        mCloseAction->setShortcut(QKeySequence::Close);
        mCloseAction->setStatusTip("Close current window");
        connect(mCloseAction, SIGNAL(triggered()), mMDIArea, SLOT(closeActiveSubWindow()));

        mCloseAllAction = new QAction(QIcon(":/graph/images/closeall.png"), "Close all", this);
        mCloseAllAction->setStatusTip("Close all windows");
        connect(mCloseAllAction, SIGNAL(triggered()),
                mMDIArea, SLOT(closeAllSubWindows()));

        tileAct = new QAction("&Tile", this);
        tileAct->setStatusTip("Tile the windows");
        connect(tileAct, SIGNAL(triggered()), mMDIArea, SLOT(tileSubWindows()));

        cascadeAct = new QAction("&Cascade", this);
        cascadeAct->setStatusTip("Cascade the windows");
        connect(cascadeAct, SIGNAL(triggered()), mMDIArea, SLOT(cascadeSubWindows()));

        nextAct = new QAction("Ne&xt", this);
        nextAct->setShortcuts(QKeySequence::NextChild);
        nextAct->setStatusTip("Move the focus to the next window");
        connect(nextAct, SIGNAL(triggered()),
                mMDIArea, SLOT(activateNextSubWindow()));

        previousAct = new QAction("Pre&vious", this);
        previousAct->setShortcuts(QKeySequence::PreviousChild);
        previousAct->setStatusTip("Move the focus to the previous window");
        connect(previousAct, SIGNAL(triggered()),
                mMDIArea, SLOT(activatePreviousSubWindow()));

        //Acciones de herramientas
        mCalibrateAction = new QAction(QIcon(":/graph/images/icn_camera.svg"),"Image &Calibration", this);
        mCalibrateAction->setStatusTip("Dark, bias and flat calibration");
        connect(mCalibrateAction, SIGNAL(triggered()), this, SLOT(ImageCalibrate_Slot()));

        mAstrometryAction = new QAction(QIcon(":/graph/images/rule.png"), "Astrometric &Reduction",this);
        mAstrometryAction->setStatusTip("Computes astrometric reduction");
        connect(mAstrometryAction, SIGNAL(triggered()), this, SLOT(ImageAstrometry_Slot()));

        mBlinkAction = new QAction(QIcon(":/graph/images/blink_32.png"), "Blink images" /*"Blink images/MPC reports"*/,this);
        mBlinkAction->setStatusTip("Blink images");
        connect(mBlinkAction, SIGNAL(triggered()), this, SLOT(ImageBlink_Slot()));

        mImageCatalogAction = new QAction(QIcon(":/graph/images/icn_stars.svg"), "Image catalog", this);
        mImageCatalogAction->setStatusTip("Creates a catalog from astronomical images");
        connect(mImageCatalogAction, SIGNAL(triggered()), this, SLOT(ImageCatalog_Slot()));

        mBatchHeader = new QAction(QIcon(":/graph/images/icn_list.svg"), "Batch header editing", this);
        mBatchHeader->setStatusTip("Edit headers on multiple images");
        connect(mBatchHeader, SIGNAL(triggered()), this, SLOT(BatchHeaders_Slot()));

        //Menú de herramientas
        mToolAngularDistanceAction = new QAction("Angular distance", this);
        mToolAngularDistanceAction->setStatusTip("Compute angular distance");
        connect(mToolAngularDistanceAction, &QAction::triggered, this, []{ (new AngularDistanceDialog())->show(); });

        mToolCoordinateConversion = new QAction("Coordinate conversions", this);
        mToolCoordinateConversion->setStatusTip("Change between epochs and coordinate systems");
        connect(mToolCoordinateConversion, &QAction::triggered, this, []{ (new CoordinateTransformDialog())->show(); });

        mSeparatorAct = new QAction(this);
        mSeparatorAct->setSeparator(true);

        //Menú de ayuda
        mHelpIndexAction = new QAction(QIcon(":/graph/images/help.png"), "Help index", this);
        mHelpIndexAction->setStatusTip("Muestra la ayuda");
        connect(mHelpIndexAction, SIGNAL(triggered()), this, SLOT(HelpIndex_Slot()));

        mSearchForUpdatesAction = new QAction("Check for updates",this);
        mSearchForUpdatesAction->setStatusTip("Check for a new " XPROGRAM " version");
        connect(mSearchForUpdatesAction, SIGNAL(triggered()), this, SLOT(CheckForUpdates_Slot()));

        mAboutXParallax = new QAction("&About - " XPROGRAM " -", this);
        mAboutXParallax->setStatusTip("Show the application's About box");
        connect(mAboutXParallax, SIGNAL(triggered()), this, SLOT(AboutAction()));

        //mAboutQT = new QAction("About - Qt Framework -", this);
        //mAboutQT->setStatusTip("Show the Qt library's About box");
        //connect(mAboutQT, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

        // Acciones de debug
#ifdef QT_DEBUG
            mTestAction2 = new QAction("Guardar normalizada para pruebas",this);
            connect(mTestAction2, SIGNAL(triggered()), this, SLOT(Test2_Slot()));
            mTestAction3 = new QAction("Mostrar diálogo de coordenadas",this);
            connect(mTestAction3, SIGNAL(triggered()), this, SLOT(Test3_Slot()));
            mTestAction4 = new QAction("Calcular fondo",this);
            connect(mTestAction4, SIGNAL(triggered()), this, SLOT(Test4_Slot()));
            mTestAction5 = new QAction("Prueba de astrometría", this);
            connect(mTestAction5, SIGNAL(triggered()), this, SLOT(Test5_Slot()));
            mTestAction6 = new QAction("Show star field", this);
            connect(mTestAction6, SIGNAL(triggered()), this, SLOT(Test6_Slot()));
            mTestAction7 = new QAction("Generar imagen sintética", this);
            connect(mTestAction7, SIGNAL(triggered()), this, SLOT(Test7_Slot()));
            mTestAction8 = new QAction("Test de SkyMaker", this);
            connect(mTestAction8, SIGNAL(triggered()), this, SLOT(Test8_Slot()));
            mTestAction9 = new QAction("Generar archivos de estrellas", this);
            connect(mTestAction9, SIGNAL(triggered()), this, SLOT(Test9_Slot()));
#endif
}


void MainWindow::UpdateWindowMenu()
{
    mWindowMenu->clear();
    mWindowMenu->addAction(mCloseAction);
    mWindowMenu->addAction(mCloseAllAction);
    mWindowMenu->addSeparator();
    mWindowMenu->addAction(tileAct);
    mWindowMenu->addAction(cascadeAct);
    mWindowMenu->addSeparator();
    mWindowMenu->addAction(nextAct);
    mWindowMenu->addAction(previousAct);
    mWindowMenu->addAction(mSeparatorAct);

    //Insertamos todas las ventanas que existen en el menú Window
    QList<QMdiSubWindow*> wlist = mMDIArea->subWindowList();
    bool has_logger = false;
    mSeparatorAct->setVisible(wlist.count());
    for (int i = 0; i < wlist.count(); ++i)
    {
        QMdiSubWindow *child = wlist.at(i);
        QString text;
        if (i < 9) {
            text = QString("&%1 %2").arg(i + 1)
                               .arg(wlist.at(i)->widget()->objectName());
        } else {
            text = QString("%1 %2").arg(i + 1)
                              .arg(wlist.at(i)->widget()->objectName());
        }
        if (!strcmp(child->widget()->metaObject()->className(), MdiLogger::staticMetaObject.className()))
            has_logger = true;
        QAction *action  = mWindowMenu->addAction(text);
        action->setCheckable(true);
        action ->setChecked(child == mMDIArea->activeSubWindow());
        connect(action, SIGNAL(triggered()), mWindowMapper, SLOT(map()));
        mWindowMapper->setMapping(action, wlist.at(i));
    }
    //Opción de logger
    if (!ActiveLogger())
    {
        QAction *action  = mWindowMenu->addAction("&1 Log window");
        action->setCheckable(true);
        action->setChecked(false);
        connect(action, SIGNAL(triggered()), this, SLOT(LoggerWindow_Slot()));
    }
}


void MainWindow::FileOpen_Slot()
{
   QStringList filenames =
           QFileDialog::getOpenFileNames(this,
                                  "Open file/s",
                                   XGlobal::APP_SETTINGS.last_fit_open_directory ,
                                   "Supported file formats (" OPEN_FILE_FORMATS ")"
                                  );
   if (!filenames.length()) return;
   for (int i=0; i<filenames.length(); i++)
   {
       QString filename = filenames.at(i);
       if (!QFile(filename).exists())
       {
           QMessageBox::warning(this, "Error", "Selected file doesn't exits");
           return;
       }
       OpenFile(filename);
   }
   XGlobal::APP_SETTINGS.last_fit_open_directory =  QFileInfo(filenames.at(0)).absolutePath();
}

XPMdiWidget* MainWindow::OpenFile(const QString& filename)
{
    QFileInfo fi(filename);
    QString ext = fi.suffix().toLower();
    if (ext == "mpc") return OpenMpcReport(filename);
    else return OpenFit(filename);
}

MdiMPC* MainWindow::OpenMpcReport(const QString& filename)
{
    //Si ya hay abierto un informe MPC no podemos abrir mas
    if (ActiveMPC())
    {
        QMessageBox::warning(this, "Warning", QString("You are currently editing a MPC report\n\n"
                                                    "No more than one MPC report can be edited at the same time."
                                                    ).arg(filename));
        return NULL;
    }
    MPCReport* report = MPCReport::LoadFromFile(filename);
    if (!report)
    {
        QMessageBox::warning(this, "Error", QString("Unable to open file '%1'\n\n"
                                                    "File format not recognized."
                                                    ).arg(filename));
        return NULL;
    }
   return CreateMpcReport(report);
}

MdiMPC* MainWindow::CreateMpcReport(MPCReport* report)
{
    MdiMPC* mpc = new MdiMPC(mMDIArea, report);
    //inspector->setFilename(filename);
    QMdiSubWindow* win = AddMdiSubWindow(mpc,":/graph/images/icn_asteroid.svg");
    win->show();
    //Actualizamos todos los MDIFITImage con la nueva capa
    QList<MdiFitImage*> images = fitImages();
    for (int i=0; i<images.count(); i++)
        mpc->UpdateMPCLayer(images[i]);
    return mpc;
}

MdiFitImage* MainWindow::OpenFit(const QString& filename)
{
    //Cargamos la imagen y si no se hubiera cargado correctamente salimos con un mensaje
    MdiFitImage* child = new MdiFitImage(mMDIArea, filename);

    if (!child->fitimage() || child->errorMsg().length())
    {
        QString msg = child->errorMsg().length() ? child->errorMsg() : "Invalid file format";
        QMessageBox::critical(this, "Error", QString("Unable to open file '%1'\n\n%2"
                                                    ).arg(QFileInfo(filename).fileName(), msg));
        delete child;
        return NULL;
    }
    //mChildList.append(child);
    QMdiSubWindow *win = AddMdiSubWindow(child,":/graph/images/grid.png");

    win->setCursor(Qt::CrossCursor);                //Ponemos el cursor de la cruz
    child->SetMdiWindow(win);
    win->show();
    child->FitToWindow_Slot();
    UpdateMenus();
    UpdateWindowMenu();
    if (ActiveMPC()) ActiveMPC()->UpdateMPCLayer(child);
    return child;
}


void MainWindow::AboutAction()
{
    AboutDialog ad(this);
    ad.exec();
}

void  MainWindow::ImageCalibrate_Slot()
{
    CalibrationDialog pd(this);
    pd.exec();
}

void  MainWindow::ImageAstrometry_Slot()
{
    AstrometryDialog ad(this);
    ad.exec();
}

void  MainWindow::BatchHeaders_Slot()
{
    BatchHeaderDialog ad(this);
    ad.exec();
}


void MainWindow::ImageCatalog_Slot()
{
    ImageCatalogDialog ad(this);
    ad.exec();
}

void  MainWindow::ImageBlink_Slot()
{
    BlinkDialog bd(MainWindow::mainwin);
    bd.exec();
}

void MainWindow::ExitAction()
{
   this->close();
}


void MainWindow::Exit_Slot()
{
    this->close();
}

QList<QMdiSubWindow*> MainWindow::SubWindows(const char* classname)
{
    QList<QMdiSubWindow*> wnd;
    QList<QMdiSubWindow*> wnn = mMDIArea->subWindowList();
    for (int i=0; i<wnn.count(); i++)
    {
        QMdiSubWindow* subwin = wnn.at(i);
        if (!strcmp(subwin->widget()->metaObject()->className(),classname))
            wnd.append(subwin);
    }
    return wnd;
}

QMdiSubWindow* MainWindow::AddMdiSubWindow(QWidget* content, QString icon, QSize minsize)
{
   content->setParent(mMDIArea);
   QMdiSubWindow* subw =  mMDIArea->addSubWindow(content);
   connect(subw, SIGNAL(destroyed(QObject*)), this, SLOT(ChildClosed_Slot(QObject*)));

   subw->setWindowState( (subw->windowState() & ~Qt::WindowMinimized) | Qt::WindowMaximized  );
   subw->setWindowTitle( content->objectName()  );
   subw->setMinimumSize(minsize);
   subw->setWindowIcon(QIcon(icon));
   return subw;
}

XPMdiWidget* MainWindow::ActiveXPMdiWidget()
{
    if (!mActiveWindow) return NULL;
    else return qobject_cast<XPMdiWidget*>(mActiveWindow->widget());
}

MdiFitImage* MainWindow::ActiveFitImage()
{
    QMdiSubWindow* win = mMDIArea->currentSubWindow();
    if (!win) return NULL;
    if (strcmp(MdiFitImage::staticMetaObject.className(), win->widget()->metaObject()->className() )) return NULL;
    return (MdiFitImage*)win->widget();
}

QList<MdiFitImage*> MainWindow::fitImages(bool fitblink)
{
    QList<MdiFitImage*> lst = QList<MdiFitImage*>();
    QList<QMdiSubWindow*> wins = this->SubWindows(MdiFitImage::staticMetaObject.className());
    for (int i=0; i<wins.count(); i++)
        lst.append((MdiFitImage*) wins.at(i)->widget());
    if (fitblink)
    {
        wins = this->SubWindows(MdiFitBlink::staticMetaObject.className());
        for (int i=0; i<wins.count(); i++)
            lst.append((MdiFitImage*) wins.at(i)->widget());
    }
    return lst;
}

MdiLogger* MainWindow::ActiveLogger()
{
    QList<QMdiSubWindow*> subwindows = mMDIArea->subWindowList();
    for (int i=0; i<subwindows.count(); i++)
    {
        const char* cname = subwindows.at(i)->widget()->metaObject()->className();
        if (!strcmp(MdiLogger::staticMetaObject.className(), cname))
            return (MdiLogger*) (subwindows.at(i)->widget());
    }
    return NULL;
}

MdiMPC* MainWindow::ActiveMPC()
{
    QList<QMdiSubWindow*> subwindows = mMDIArea->subWindowList();
    for (int i=0; i<subwindows.count(); i++)
    {
        const char* cname = subwindows.at(i)->widget()->metaObject()->className();
        if (!strcmp(MdiMPC::staticMetaObject.className(), cname))
            return (MdiMPC*) (subwindows.at(i)->widget());
    }
    return NULL;
}

QMdiSubWindow* MainWindow::GetSubWindow(QWidget* content)
{
    QList<QMdiSubWindow*> subwindows = mMDIArea->subWindowList();
    for (int i=0; i<subwindows.count(); i++)
    {
        if ( subwindows.at(i)->widget() == content )
            return subwindows.at(i);
    }
    return NULL;
}


int MainWindow::SubWindowCount(const char* type)
{
  int cont = 0;
  QList<QMdiSubWindow*> subwindows = mMDIArea->subWindowList();
  for (int i=0; i<subwindows.count(); i++)
  {
      const char* cname = subwindows.at(i)->widget()->metaObject()->className();
      if (!strcmp(type, cname)) cont++;
  }
  return cont;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QList<QMdiSubWindow*> childs = SubWindows(MdiFitImage::staticMetaObject.className());
    //Comprobamos si hay algún elemento dirty
    bool dirty = false;
    for (int i=0; i<childs.count() && !dirty; i++)
    {
          dirty = ((MdiFitImage*)(childs[i]->widget()))->fitimage()->dirty();
    }
    //Si hay algún archivo dirty preguntamos
    if (dirty)
    {
        int result =
        QMessageBox::question(this,"Exit program",
                                 "There are files with unsaved changes\n\n"
                                 "Do you really want to quit? "
                                 ,QMessageBox::Yes|QMessageBox::No
                              );
        if (result == QMessageBox::No)
        {
            event->ignore();
            return;
        }
    }
    //Eliminar todos los diálogos no modales
    while (mNoModalDialogs.count())
        mNoModalDialogs[0]->close();
}


void MainWindow::UpdateLabel(int idx,const QString& text)
{
    switch (idx)
    {
        //Etiqueta de escala (combobox)
        case 0: UpdateMenus();
                break;
        case 1: mStatusXY->setText(text); break;
        case 2: mStatusFlux->setText(text); break;
    }
}

void MainWindow::FitScaleChange_Slot()
{
    MdiFitImage *im = ActiveFitImage();
    if (!im || mStatusScale->currentIndex() < 0 || !mStatusScale->isEnabled()) return;
    int idx = mStatusScale->currentIndex();
    //Si no es un índice de usuario salgo
    if (idx >= 5) return;
    float scale = mStatusScale->itemData(idx).toInt();
    double newscale = scale/100.0f;
    if (fabs(im->scale() - newscale)<0.001) return;
    im->SetScale(newscale);
    //Elimino todos los índices de programa
    while (mStatusScale->count() > 5)
        mStatusScale->removeItem(5);
}

void MainWindow::LoggerWindow_Slot()
{
    //Si no hay ninguna ventana de log la mostramos
    if (!ActiveLogger())
    {
        MdiLogger* logger =  new MdiLogger(mMDIArea);
        logger->setObjectName("Log window");
        AddMdiSubWindow(logger, ":/graph/images/list-ordered.png");
        INFO(LOG_SEPARATOR);
        INFO("- %s - ready, version %s running on %s %d bits", XPROGRAM, XVERSION, PTQ(XGlobal::OS_TYPE), XGlobal::OS_BITS);
        INFO(LOG_SEPARATOR);
    }
}

void MainWindow::Tool_CoordinateTransform_Slot()
{

}

void MainWindow::MessageAppReceived_Slot()
{

}

void MainWindow::HelpIndex_Slot()
{
    QDesktopServices::openUrl(QUrl("http://www.xparallax.com/help.php"));
}

void MainWindow::CheckForUpdates_Slot()
{
    XGlobal::APP_SETTINGS.do_not_prompt_updates = false;
    XGlobal::APP_SETTINGS.last_update_warn = QDate(1900,1,1);
    INFO("Checking for updates... Current version: %s %s", PTQ(QString(XPROGRAM)), PTQ(QString(XVERSION)));
    if (AboutDialog::updateData().httpStatus == 200)
        UpdateFound_Slot(true, false);
    else
        AboutDialog::CheckForUpdates(true);
}

void MainWindow::UpdateFound_Slot(bool found, bool user_requested)
{
    if (!found && !user_requested) return;
    if (!found && user_requested)
    {
        QMessageBox::information( this, "No update available",
                           "No updates available.\n\nYour version is the latest one.",
                                  QMessageBox::Yes );
    }
    else
    {
        INFO("A new update is available: version  %s", PTQ(AboutDialog::updateData().version));
        XGlobal::APP_SETTINGS.last_check_for_updates = QDate::currentDate();
        if (XGlobal::APP_SETTINGS.last_update_warn >= QDate::currentDate()) return;
        if (XGlobal::APP_SETTINGS.do_not_prompt_updates) return;

        XGlobal::APP_SETTINGS.last_update_warn = QDate::currentDate();
        QMessageBox msgBox(QMessageBox::Information, "New update available",
                           QString("<b>%1</b> version <b style='color:blue;'>%2</b> is available.<br/><br/>Do you want to visit the website to download the new version?")
                           .arg(XPROGRAM_HTML).arg(AboutDialog::updateData().version), 0, this);
        QCheckBox dontPrompt("Do not prompt again", &msgBox);

        // HACK: BLOCKING SIGNALS SO QMessageBox WON'T CLOSE
        dontPrompt.blockSignals(true);

        msgBox.addButton(&dontPrompt, QMessageBox::ActionRole);

        msgBox.addButton(QMessageBox::Yes);
        msgBox.addButton(QMessageBox::No);
        int result = msgBox.exec();
        if (result == QMessageBox::Yes)
        {
            QDesktopServices::openUrl( QUrl("http://www.xparallax.com/") );
        }
        XGlobal::APP_SETTINGS.do_not_prompt_updates = dontPrompt.checkState();
    }
}

/**
 * @brief MainWindow::MdiFit_Load Carga las extensiones de ventanas FIT
 */
void MainWindow::MdiFit_Load()
{

}






/**
 * @brief MainWindow::InterfaceMdiFit_ShowStaticAction Muestra/oculta la acción estática indicada
 * @param parent
 * @param ref
 * @param show
 * @return Si se especificó NULL en (ref), se devolverá el número de acciones que son visibles. Si no se especificó NULL,
 * el valor devuelto no es consistente y debará ser ignorado.
 */
int MainWindow::InterfaceMdiFit_ShowStaticAction(QWidget* parent,QAction* ref, bool show)
{
    QList<QAction*> act = IS(parent,QMenu) ? ((QMenu*)parent)->actions() : ((QToolBar*)parent)->actions();
    int cont_vis = 0;
    for (int i=0; i<act.count(); i++)
    {
        if (!act[i]->property(XSTATICACTION).isNull())
        {
            if (!ref)
            {
                act[i]->setVisible(show);
            }
            else if (act[i]->objectName() == ref->objectName())
            {
                ref->setProperty(XPRIORITY, act[i]->property(XPRIORITY));   //Se igualan las prioridades
                act[i]->setVisible(show);                                   //Se  muestra u oculta
                break;
            }
        }
        //Si ref==NULL y es una acción dinámica la ocultamos, se está desactivando la ventnaa
        else if (!ref && !act[i]->property(XDYNAMICACTION).isNull())
        {
            act[i]->setVisible(false);
        }
        if (act[i]->isVisible()) cont_vis++;
    }
    return cont_vis;
}

void MainWindow::InterfaceMdiFit_DeActivate(XPMdiWidget* fi)
{
    //Indicar a la ventana que ha sido desactivada
    if (fi) fi->deactivate();

    //Se desactiva la ventana pero por destrucción. Actuamos solamente sobre la barra denús
    //----------------------------------------------------------------------------------------
    if (!fi)
    {
        QList<QMenu*> menus = ui->menuBar->findChildren<QMenu*>(NULL, Qt::FindDirectChildrenOnly);
        for (int i=0; i<menus.count(); i++)
        {
            //En este caso se comprueba que si hay algún menú sin items visibles se oculta
            if (!InterfaceMdiFit_ShowStaticAction(menus[i], NULL, true))
            {
                menus[i]->setTitle("");
                menus[i]->setEnabled(false);
            }
            //El menú experimental siempre lo ocultamos si fuera necesario
            if (!mExperimental->shown() && menus[i]->objectName() == MENU_EXPERIMENTAL)
            {
                menus[i]->setTitle("");
                menus[i]->setEnabled(false);
            }
        }
        QList<QToolBar*> toolbars = findChildren<QToolBar*>();
        for (int i=0; i<toolbars.count(); i++)
        {
            //En este caso se comprueba que si hay algún toolbar sin items visibles y se oculta
            if (!InterfaceMdiFit_ShowStaticAction(toolbars[i], NULL, true))
                toolbars[i]->setVisible(false);
        }
        return;
    }

    //Comenzamos por los menús
    //----------------------------------------------------------------------------------------
    QList<XPEXT_ACTION_ITEMS>* menus_itms =  fi->menusItems();
    QList<QMenu*> updated_menus;
    //Añado todos los items a sus correspondientes menús
    for (int i=0; i<menus_itms->count(); i++)
    {
         QMenu* men = getMenuByName(menus_itms->at(i).men, true, NULL, menus_itms->at(i).mpr);
         men->removeAction(menus_itms->at(i).act);
         updated_menus.append(men);
         //Si la acción es estática oculto la otra acción estática que hubiera
         if (menus_itms->at(i).beh == ITEM_BEHAVIOUR_ENABLE_DISABLE)
             InterfaceMdiFit_ShowStaticAction(men, menus_itms->at(i).act, true);
    }
    //Ahora para todos los menús actualizados ordeno
    for (int i=0; i<updated_menus.count(); i++)
        sortActionItems(updated_menus[i]);

    //Seguimos por los toolbars
    //----------------------------------------------------------------------------------------
    QList<XPEXT_ACTION_ITEMS>* tool_itms =  fi->toolbarItems();
    QList<QToolBar*> updated_toolbars;
    for (int i=0; i<tool_itms->count(); i++)
    {
         QToolBar* tb = getToolbarByName(tool_itms->at(i).men, true, tool_itms->at(i).mpr);
         tb->removeAction(tool_itms->at(i).act);
         updated_toolbars.append(tb);
         //Si la acción es estática oculto la otra acción estática que hubiera
         if (tool_itms->at(i).beh == ITEM_BEHAVIOUR_ENABLE_DISABLE)
             InterfaceMdiFit_ShowStaticAction(tb, tool_itms->at(i).act, true);
    }
    for (int i=0; i<updated_toolbars.count(); i++)
            sortActionItems(updated_toolbars[i]);
}

void MainWindow::InterfaceMdiFit_Activate(XPMdiWidget* fi)
{
   //Indicar a la ventana que se ha vuelto la ventana activa
   if (fi) fi->activate();

   //Comenzamos por los menús
   //----------------------------------------------------------------------------------------
   QList<XPEXT_ACTION_ITEMS>* menus_itms =  fi->menusItems();
   QList<QMenu*> updated_menus;
   //Añado todos los items a sus correspondientes menús
   for (int i=0; i<menus_itms->count(); i++)
   {
        QMenu* men = getMenuByName(menus_itms->at(i).men, true, NULL, menus_itms->at(i).mpr);
        //Aquí se verá si el action tiene que estar enabled/disabled checked/unckecked
        men->addAction(menus_itms->at(i).act);
        menus_itms->at(i).act->setProperty(XPRIORITY, menus_itms->at(i).pri);
        menus_itms->at(i).act->setProperty(XDYNAMICACTION, true);
        if (!updated_menus.contains(men))  updated_menus.append(men);
        //Si la acción es estática oculto la otra acción estática que hubiera
        if (menus_itms->at(i).beh == ITEM_BEHAVIOUR_ENABLE_DISABLE)
            InterfaceMdiFit_ShowStaticAction(men, menus_itms->at(i).act, false);
   }
   //Ahora para todos los menús actualizados ordeno
   for (int i=0; i<updated_menus.count(); i++)
       sortActionItems(updated_menus[i]);

   //Seguimos por los toolbars
   //----------------------------------------------------------------------------------------
    QList<XPEXT_ACTION_ITEMS>* toolbar_itms =  fi->toolbarItems();
    QList<QToolBar*> updated_toolbars;
    for (int i=0; i<toolbar_itms->count(); i++)
    {
         QToolBar* tb = getToolbarByName(toolbar_itms->at(i).men, true, toolbar_itms->at(i).mpr);
         //Aquí se verá si el action tiene que estar enabled/disabled checked/unckecked
         tb->addAction(toolbar_itms->at(i).act);
         toolbar_itms->at(i).act->setProperty(XPRIORITY, toolbar_itms->at(i).pri);
         toolbar_itms->at(i).act->setProperty(XDYNAMICACTION, true);
         if (!updated_toolbars.contains(tb))  updated_toolbars.append(tb);
         //Si la acción es estática oculto la otra acción estática que hubiera
         if (toolbar_itms->at(i).beh == ITEM_BEHAVIOUR_ENABLE_DISABLE)
             InterfaceMdiFit_ShowStaticAction(tb, toolbar_itms->at(i).act, false);
    }

    //Ahora para todos los toolbars actualizados ordeno
    for (int i=0; i<updated_toolbars.count(); i++)
      sortActionItems(updated_toolbars[i]);
}

/**
 * @brief MainWindow::getMenuByName Obtiene un menú por nombre. Conviene llamar a esta función también para
 * crear los menús.
 * @param name Nombre del menú a buscar, debe coincidir con el nombre del objeto
 * @param create Si no se encuentra el menú, se creará
 * @param parent Widget padre donde se buscarán los sub-menús. Si no se especifica se usará ui->menuBar
 * @param priority Indica la prioridad a la hora de la inserción
 * @return
 */
QMenu* MainWindow::getMenuByName(const QString& name, bool create, QWidget* parent, int priority)
{
    if (!parent) parent = menuBar();
    //Opción simple, sin ningún tipo de herencia
    if (!name.contains('/'))
    {
        QList<QMenu*> menus = parent->findChildren<QMenu*>(NULL, Qt::FindDirectChildrenOnly);
        for (int i=0; i<menus.count(); i++)
            if (menus[i]->objectName() == name) return menus[i];

        if (create)
        {
            QMenu* men = new QMenu(name, parent);
            men->setProperty(XPRIORITY, priority);
            men->setObjectName(name);

            //Lo inserto en el lugar correspondiente
            QMenu* last = NULL;
            int last_dist = 0x0FFFFFFF;
            for (int i=0; i<menus.count(); i++)
            {
                int prio_base = menus[i]->property(XPRIORITY).isNull() ? 0 :  menus[i]->property(XPRIORITY).toInt();
                if (prio_base > priority)
                {
                    int dist = prio_base - priority;
                    if (dist < last_dist)
                    {
                        last = menus[i];
                        last_dist = dist;
                    }
                }
            }

            if (last)
            {
                if (IS(parent, QMenuBar)) ((QMenuBar*)parent)->insertMenu(last->menuAction(), men);
                else if (IS(parent, QMenu)) ((QMenu*)parent)->insertMenu(last->menuAction(), men);
            }
            else
            {
                if (IS(parent, QMenuBar))  ((QMenuBar*)parent)->addMenu(men);
                else if (IS(parent, QMenu))  ((QMenu*)parent)->addMenu(men);
            }
            return men;
        }
        return NULL;
    }
    //Opción con herencia y submenús
    else
    {
        QStringList lst = name.split('/');
        QString subitem = "";
        for (int i=1; i<lst.count(); i++) subitem += i==1?lst[i]:(QString("/")+lst[i]);
        QMenu* pt = getMenuByName(lst[0], create, parent);
        if (!pt) return NULL;
        QMenu* men = getMenuByName(subitem, create, pt);
        return men;
    }
}

void MainWindow::NoModalAdd(QDialog* dlg)
{
    if (mNoModalDialogs.contains(dlg)) return;
    mNoModalDialogs.append(dlg);
    //Debería aparecer en la barra de tareas para cuando se minimice
    dlg->setParent(NULL);
    //Nos aseguramos de mostrar el botón de minimizar
    Qt::WindowFlags flags = dlg->windowFlags() |
                                Qt::Window | Qt::WindowSystemMenuHint
                                | Qt::WindowMinimizeButtonHint
                                | Qt::WindowCloseButtonHint;
    dlg->setWindowFlags(flags);
    //Nos aseguramos de hacer un delete cuando se cierre el diálogo
    connect(dlg, &QDialog::finished, this, [=]{
        if (MainWindow::mainwin->mNoModalDialogs.removeOne(dlg))
            dlg->deleteLater();
    });
}

bool MainWindow::compareXPriority(const QObject *left,const QObject *right)
{
    QVariant pr1 =left->property(XPRIORITY);
    int pr1i = pr1.isNull() ? 0  : pr1.toInt();
    QVariant pr2 =right->property(XPRIORITY);
    int pr2i = pr2.isNull() ? 0  : pr2.toInt();
    return pr1i > pr2i;
}


/**
 * @brief MainWindow::sortActionItems Ordena las acciones contenidas en un Menú o en una Toolbar
 * @param parent
 */
void MainWindow::sortActionItems(QObject* parent)
{
    QToolBar *tb = NULL; QMenu* mn = NULL;
    QList<QAction*> items;
    if (IS(parent, QToolBar)) tb = (QToolBar*) parent;
    else if (IS(parent, QMenu)) mn = (QMenu*) parent;
    ASSERT(mn||tb,"No se especificó correctamente un menó o toolbar");
    if (!mn && !tb) return;
    items = tb ? tb->actions() : mn->actions();
    //Compruebo si necesitan ordenación
    bool sorted = false;

    //Intentamos ordenar la lista por el método de la burbuja a la vez que determinamos si estaba ordenada
    for (int i=0; i<items.count(); i++)
    {
        for (int j=1; j<items.count(); j++)
        {
            if (compareXPriority(items[j-1], items[j]))
            {
                QAction* act = items[j-1];
                items[j-1] = items[j];
                items[j] = act;
                sorted = true;
            }
        }
    }
    //Todos los separadores que queden al final los ocultamos
    for (int i=items.count()-1; i>= 0 && items[i]->isSeparator(); i--)
        items[i]->setVisible(false);

    //Si se ha producido ordenación volvemos a crear todo el menú
    if (sorted)
    {
        //Eliminamos todos los elementos del padre
        for (int i=0; i<items.count(); i++)
            tb ? tb->removeAction(items[i]) : mn->removeAction(items[i]);
        //Las volvemos a añadir pero esta vez ordenadas
         tb ? tb->addActions(items) : mn->addActions(items);
    }

    //Ahora comprobamos que todos los items son visibles y si no tiene nada escondemos el menú completo
    bool visible = false;
    for (int i=0; i<items.count() && !visible; i++)
        visible = items.at(i)->isVisible();

    if (!visible || !items.count())
    {
        if (tb) tb->setVisible(false);
        else mn->setTitle("");
        tb ? tb->setEnabled(false) : mn->setEnabled(false);
    }
    else
    {
        if (tb) tb->setVisible(true);
        else  mn->setTitle(mn->objectName());
        tb ? tb->setEnabled(true) : mn->setEnabled(true);
    }
}

QToolBar* MainWindow::getToolbarByName(const QString& name, bool create, int priority)
{
    QList<QToolBar*> toolbars = findChildren<QToolBar*>();
    for (int i=0; i<toolbars.count(); i++)
        if (toolbars[i]->objectName() == name) return toolbars[i];
    if (create)
    {
        QToolBar* tb =  NULL;
        tb = addToolBar(name);
        tb->setObjectName(name);
        tb->setVisible(false);
        tb->setProperty(XPRIORITY, priority);
        return tb;
    }
    return NULL;

}

/**
 * @brief MainWindow::SetIcoDPI Establece el icono de una ventana en base a los DPI del sistema.
 *        así se asegurará de establecer 40x40 en Windows 1.25 para que se vea correctamente en
 *        la barra de tareas.
 * @param mw
 */
void MainWindow::SetIcoDPI(QMainWindow* mw)
{
    /*
         dpi	Icon size	Scale factor
        96	32x32		1.0  (100%)		TAMAÑO BASE
        120	40x40		1.25 (125%)
        144	48x48		1.5  (150%)
        192	64x64		2.0  (200%)
   */
    QSvgRenderer renderer(QString(":/graph/images/logo.svg"));
    QPixmap pm;
    if (XGlobal::SCREEN_DPI < 1.15) pm =  QPixmap(32, 32);
    else if (XGlobal::SCREEN_DPI < 1.45) pm = QPixmap(40, 40);
    else if (XGlobal::SCREEN_DPI < 1.95) pm = QPixmap(32, 32);
    else pm = QPixmap(64, 64);
    pm.fill(Qt::transparent);  // Fondo transparente
    QPainter painter(&pm);
    renderer.render(&painter);
    mw->setWindowIcon(QIcon(pm));
}

/**
 * @brief MainWindow::DPIAware Prepara un diálogo para mostrarse en pantalla adaptándose a los DPI del sistema donde está
 *        corriendo la aplicación. Modificará el tamaño del diálogo y de los elementos gráficos en él contenidos (imágenes).
 *        Pero de las fuentes no se ocupa, puesto que Windows/Linux ya las cambian en relación a los DPI de la pantalla.
 * @param dlg
 * @param fix_size
 */
void MainWindow::DPIAware(QDialog* dlg, bool fix_size)
{
    QSize sz;

    //Ahora todos los widgets
    //----------------------------------------------------------------------------------------------------
    QList<QWidget*> wid = dlg->findChildren<QWidget*>(QRegularExpression(".*"),Qt::FindChildrenRecursively);
    for (int i=0; i<wid.count(); i++)
    {
        sz = wid.at(i)->baseSize();
        if (sz.width())
        {
            wid.at(i)->setMaximumWidth(sz.width() * XGlobal::SCREEN_DPI);
            wid.at(i)->setMinimumWidth(sz.width() * XGlobal::SCREEN_DPI);
        }
        if (sz.height())
        {
            wid.at(i)->setMaximumHeight(sz.height() * XGlobal::SCREEN_DPI);
            wid.at(i)->setMinimumHeight(sz.height() * XGlobal::SCREEN_DPI);
        }
    }


    //Cambiamos el tamaño de las etiquetas
    //----------------------------------------------------------------------------------------------------
    QList<QLabel*> labls = dlg->findChildren<QLabel*>(QRegularExpression(".*"),Qt::FindChildrenRecursively);
    for (int i=0; i<labls.count(); i++)
    {
        QLabel* lab = labls.at(i);
        QSize sz = lab->baseSize();
        if (!sz.width() || !sz.height() || lab->text().length()) continue;
        lab->resize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        lab->setMaximumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        lab->setMinimumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        lab->setScaledContents(true);
    }

    //Cambiamos el tamaño de los toolbuttons
    //----------------------------------------------------------------------------------------------------
    QList<QToolButton*> ltool = dlg->findChildren<QToolButton*>(QRegularExpression(".*"),Qt::FindChildrenRecursively);
    for (int i=0; i<ltool.count(); i++)
    {
        QToolButton* tb = ltool.at(i);
        QSize sz = tb->baseSize();
        if (!sz.width() || !sz.height() || tb->text().length()) continue;
        tb->resize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        tb->setMaximumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        tb->setMinimumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        sz = tb->iconSize();
        tb->setIconSize(QSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI));
    }

    //Cambiamos el tamaño de los animated label
    //----------------------------------------------------------------------------------------------------
    QList<AnimatedLabel*> alabs = dlg->findChildren<AnimatedLabel*>(QRegularExpression(".*"),Qt::FindChildrenRecursively);
    for (int i=0; i<alabs.count(); i++)
    {
        QLabel* lab = alabs.at(i);
        QSize sz = lab->baseSize();
        if (!sz.width() || !sz.height() || lab->text().length()) continue;
        lab->resize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        lab->setMaximumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        lab->setMinimumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
        lab->setScaledContents(true);
    }


    //Tamaño general del diálogo
    //----------------------------------------------------------------------------------------------------
    sz = dlg->baseSize();
    if (sz.width())
    {
        dlg->setMaximumWidth(sz.width() * XGlobal::SCREEN_DPI);
        dlg->setMinimumWidth(sz.width() * XGlobal::SCREEN_DPI);
    }
    if (sz.height())
    {
        dlg->setMaximumHeight(sz.height() * XGlobal::SCREEN_DPI);
        dlg->setMinimumHeight(sz.height() * XGlobal::SCREEN_DPI);
    }
    if (!sz.width() || !sz.height())
    {
        dlg->adjustSize();
    }

    //Operaciones finales sobre el borde de la ventna y algunos otros estilos
    //----------------------------------------------------------------------------------------------------
    if (fix_size)
        dlg->setFixedSize(sz.width() ? dlg->minimumWidth() : dlg->width(),
                          sz.height()? dlg->minimumHeight() : dlg->height() );
    dlg->setWindowFlags(dlg->windowFlags() & ~Qt::FramelessWindowHint & ~Qt::WindowContextHelpButtonHint);

    //Nos aseguramos de que se recalculan todos los tamaños aunque no se muestre el diálogo
    if (dlg->layout())
    {
        dlg->layout()->update();
        dlg->layout()->activate();
    }
}
































