#ifndef MDIMPC_H
#define MDIMPC_H

#include "mpc.h"
#include "xpmdiwidget.h"
#include "../mdifitimage.h"
#include <QTextEdit>


//Estructura para almacenar errores dentro del informe
struct ErrorLine
{
    int     line;       //Línea, índice basado en 1
    bool    warning;    //True si es un warning
    QString errDesc;    //Descripción del error
    QAction*action;     //Acción a realizar para solucionar el error
    int     anchorid;
    ErrorLine(QAction* act=NULL){warning=false; action=act;anchorid=++ANCHOR_ID;}
    ErrorLine(int ln, QString desc, QAction* act,bool war=false){line=ln;errDesc=desc;warning=war;action=act;anchorid=++ANCHOR_ID;}

private:
    static int ANCHOR_ID;
};

class MdiMPCSettings : Parameters
{
public:
    MdiMPCSettings();
    ~MdiMPCSettings();

    void SetDefaults();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);

    float font_size;

};

class MdiMPC : public /*QTextBrowser*/QTextEdit, public XPMdiWidget
{
    Q_OBJECT
    Q_INTERFACES(XPMdiWidget)

public:
    explicit MdiMPC(QWidget *parent, MPCReport* report);
    ~MdiMPC();
    void UpdateTextArea();                          //Cuando se ha modificado el documento, actualiza la visión en pantalla
    void EditMeasureByCoords(double ra, double dec);    //Da la orden de editar una medida basá
    MpcGeoInfo latLong();                              //Devuelve la latitud y longitud del observatorio del informe
                                                       //En method vendrá el nombre del observatorio si se obtuvo de ahí

private:
    void CreateMenus();
    void moveCursorToBlock(int block);
    QTextCursor cursorForBlock(int block);
    virtual void initExtensionInterface();
    virtual void destroyExtnsionInterface();
    bool   mpcHeaderRequestEdit();                  //Edita la cabecera donde se encuentra el cursor actualmente
    bool   mpcMeasureRequestEdit(int line=-1);      //Edita una línea MPC. Índice basado en 0. Indica línea de informe
    //Edición de los tres tipos de medidas, Minor Planet, Comet, Natural Satellite
    bool   mpcMeasureEditMinorPlanet(MPCReportMeasure* mea);
    bool   mpcMeasureEditComet(MPCReportMeasure* me);
    bool   mpcMeasureEditSatellite(MPCReportMeasure* me);

    QString parseMPName(QString nm);                //Parsea un nombre en formato MPC
    QString parseMPNames(QString nm);               //Parsea un conjunto de nombres separados por comas
    void clearErrors();
    void addError(int line,const QString& desc, QAction* act=NULL);
    void addWarning(int line,const QString& desc, QAction* act=NULL);
    bool containsWarningOrErrors(int line);
    void showInfoFox(int line);
    QString htmlErrors(int line);                   //Obtiene la descripción html de los errores de la línea indicada
    void enableDisableActions();
    bool isNote1(char note);
    const int    getHdrPos(int hdrline);
    const char*  getHdrName(const char* hdr, int pos=0);
    const char*  getHdrName(const QString& hdr, int pos=0);
    const char*  getHdrDesc(const char* hdr, int pos=0);
    const char*  getHdrDesc(const QString& hdr, int pos=0);


    MdiMPCSettings     mConfig;
    QSettings*         mSettings;
    QString            mObjectId;
    QFont              mScreenFont;
    bool               mCursorVisible;
    QTimer*            mCursorTimer;
    MPCReport*         mReport;
    QString            mFilename;
    bool               mInNumberArea;       //Indica si el puntero del ratón está sobre el área numérica
    QAction*           mSaveReportAction;   //Acción de guardar informe, solo activa si está dirty
    QVector<ErrorLine> mErrLines;           //Líneas con errores
    QImage             mImgWarning;
    QImage             mImgError;
    QTextBrowser*      mInfoBox;            //Caja informativa con los erroes que contiene la línea
    int                mInfoLine;           //Línea a la que se refiere el infobox

    //Agregar cabeceras
    QAction*           mAddCODHdr;
    QAction*           mAddCONContactHdr;
    QAction*           mAddCONEmailHdr;
    QAction*           mAddCOMHdr;
    QAction*           mAddACKHdr;
    QAction*           mAddAC2Hdr;
    QAction*           mAddOBSHdr;
    QAction*           mAddMEAHdr;
    QAction*           mAddTELHdr;
    QAction*           mAddNETHdr;
    QAction*           mAddBNDHdr;
    QAction*           mAddNUMHdr;

    //Barra de estado
    QLabel*            mLabelTotalMeasures;
    QLabel*            mLabelObjectMeasures;

    static const char* MPC_HEADERS[];
    static const char* TELESCOPES[];
    static const char* CATALOGS[];
    static const char* NOTE1[];
    static const char* NOTE2[];
    static const char* BANDS[];
    static const char* COMET_ORBIT[];
    static const char* PLANET_IDS[];
    static const char  MPC_REPORT_INI[];


public:
   inline const QString& filename(){return mFilename;}
   inline MPCReport* report(){return mReport;}
   void CheckDirty();
   void lineNumberAreaPaintEvent(QPainter *painter);
   int lineNumberAreaWidth(bool update=false);
   void UpdateMPCLayers();
   void UpdateMPCLayer(MdiFitImage* fi);

protected:
   QString getWindowTitle();
   void paintEvent(QPaintEvent* e);
   void mouseMoveEvent(QMouseEvent *e);
   void mousePressEvent(QMouseEvent *e);
   void mouseDoubleClickEvent(QMouseEvent *e);
   void wheelEvent(QWheelEvent* e);
   void keyPressEvent(QKeyEvent *e);
   void activate();
   void deactivate();

signals:

private slots:
    void BlinkCursor_Slot();
    void InfoBoxAnchorClicked_Slot(const QUrl& anchor);
    void highlightCurrentLine();
    void Save_Slot();
    void SaveAs_Slot();
    bool Validate_Slot(bool user_triggered=false);      /** Buscar errores y warnings dentro del informe **/
    void SendReport_Slot();                             /** Enviar el informe **/
    void ContextMenu_Slot(QPoint p);
    void AddHeader(const QString& hdr);
    void EditHeader(const QString& hdr);
    void EditHeader(int hdrpos);
    void RemoveMeasure(int post);                       /** Índice basado en cero **/
    void SetMeasureObsCode(int pos);
    void SetMeasureNote(int pos, int note, char val);
    void ReplaceRaw(int line, const QString& rawdata);

};




#endif // MDIMPC_H
