#include "mdilogger.h"
#include "xglobal.h"
#include "../mainwindow.h"
#include <QMdiArea>


MdiLogger::MdiLogger(QWidget *parent) :
    QPlainTextEdit(parent)
{
    initExtensionInterface();
    this->setStyleSheet(
                "QPlainTextEdit { background-color:#222; color:#fff; font-family: \"Lucida Console\", monaco, monospace; }"
    );
    this->setReadOnly(true);
    //this->setLineWrapMode(NoWrap);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    connect(XGlobal::LOGGER, SIGNAL(msg(qint64,int,QString)), this, SLOT(Log_Slot(qint64,int,QString)));
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ContextMenu_Slot(QPoint)));

    CreateMenus();
}

MdiLogger::~MdiLogger()
{
    destroyExtnsionInterface();
}


void MdiLogger::CreateMenus()
{
    mContextMenu = new QMenu(this);

    mClearAction = mContextMenu->addAction("Clear log");
    mClearAction->setIcon(QIcon(":/graph/images/icn_cross.png"));
    mSaveAction = mContextMenu->addAction("Save log");
    mSaveAction->setIcon(QIcon(":/graph/images/save.png"));
    mSaveAction->setShortcut(QKeySequence::Save);
    mContextMenu->addSeparator();

    mCopyAction = mContextMenu->addAction("Copy");
    mCopyAction->setShortcut(QKeySequence::Copy);

    mSelectAllAction = mContextMenu->addAction("Select all");
    mSelectAllAction->setShortcut(QKeySequence::SelectAll);

    mContextMenu->addSeparator();
    mClose = mContextMenu->addAction("Close logger");
}

void MdiLogger::initExtensionInterface()
{
    //Acción de "Guardar"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_saveas_log;
    xp_saveas_log.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_saveas_log.act = new QAction(QIcon(ACTION_SAVE_ICON), "Save log information", this);
    xp_saveas_log.act->setObjectName(ACTION_SAVE);
    xp_saveas_log.act->setStatusTip(ACTION_SAVE_DESC);
    xp_saveas_log.men = MENU_FILE;
    menusItems()->append(xp_saveas_log);
    connect(xp_saveas_log.act, SIGNAL(triggered()), this, SLOT(Save_Slot()));
    xp_saveas_log.men = TOOLBAR_GENERAL;
    toolbarItems()->append(xp_saveas_log);

    xp_saveas_log.act = new QAction("Save log as...", this);
    xp_saveas_log.act->setObjectName(ACTION_SAVEAS);
    xp_saveas_log.act->setStatusTip(ACTION_SAVEAS_DESC);
    xp_saveas_log.men = MENU_FILE;
    menusItems()->append(xp_saveas_log);
    connect(xp_saveas_log.act, SIGNAL(triggered()), this, SLOT(Save_Slot()));

}

void MdiLogger::destroyExtnsionInterface()
{

}

void MdiLogger::Log_Slot(qint64 /* thread_id */, int log_type, QString msg)
{
    QDateTime dt = QDateTime::currentDateTime();
    snprintf(mBuffer, LOG_BUFFER_LEN,"%02d:%02d:%02d.%03d [%s] %s",
             dt.time().hour(), dt.time().minute(), dt.time().second(), dt.time().msec(),
             log_type == (int)Logger::LOGTYPE_INFO?"INFO ":(log_type == (int)Logger::LOGTYPE_WARN?"WARN ": log_type == (int)Logger::LOGTYPE_DEBUG ? "DEBUG" : "ERROR"),
             msg.toUtf8().data());
    mBuffer[LOG_BUFFER_LEN-1] = 0;
    this->appendPlainText(mBuffer);
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->maximum());
}

void MdiLogger::ContextMenu_Slot(QPoint pos)
{
    QPoint globalPos = this->mapToGlobal(pos);
    QAction* sel_action =  mContextMenu->exec(globalPos);
    if (sel_action == mClearAction)
        this->clear();
    else if (sel_action == mCopyAction)
        copy();
    else if (sel_action == mSelectAllAction)
        selectAll();
    else if (sel_action == mClose)
    {
        XGlobal::APP_SETTINGS.show_log_window = false;
        parentWidget()->close();
    }
    else if (sel_action == mSaveAction)
        Save_Slot();
}

void MdiLogger::Save_Slot()
{
    QString filename = QFileDialog::getSaveFileName(this,
        "Save current log view",
        "","Log files (*.log)"
    );
    if (!filename.length()) return;
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::warning(this,"Error", "Unable to save content to file");
        return;
    }
    QTextStream out(&file);
    out.setGenerateByteOrderMark(true);
    out << QString::fromUtf8( toPlainText().toUtf8() );
}






















