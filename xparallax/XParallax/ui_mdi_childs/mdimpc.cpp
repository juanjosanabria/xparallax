#include "mdimpc.h"
#include <QHttpMultiPart>
#include <QHttpPart>
#include "xstringbuilder.h"
#include "vo.h"

#include "../mainwindow.h"
#include "../inputdialog.h"

#define    SAVE_FMT_MPC                 "text file mpc (*.mpc)"
#define    SAVE_FMT_TXT                 "text file txt (*.txt)"
#define    COLOR_SEL_LINE               "#E8E8FF"
#define    QCOLOR_SEL_LINE              QColor(222,222,255) //QColor(232,232,255)
#define    QCOLOR_REL_LINE              QColor(245,245,255)
#define    QCOLOR_LINE_NUMBER_AREA      QColor(228,228,228)
#define    QCOLOR_LINE_NUMBER_AREA_TEXT QColor(88,88,88)
#define    REGEX_OBSCODE                "^[A-Z0-9]{3}\\s*$"
#define    REGEX_EMAIL                  "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*" \
                                        "@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
#define    REGEX_MPNAME                 "^([A-Z]\\.\\s?)+\\s*([^\\s]+\\s*)+$"
#define    DEFAULT_MAIL                 "yourmail@yourdomain.com"
#define    MIN_INFO_BOX_HEIGHT          (65*XGlobal::SCREEN_DPI)
#define    MIN_INFO_BOX_WIDTH           (400*XGlobal::SCREEN_DPI)
#define    ASECS_IMAGE_MEASURE          8.33e-4 //3 segundos de arco diferencia para asumir que un measure pertenece a una imagen
#define    SECS_IMAGE_MEASURE           2 //Segundos (de tiempo) para asumir que una imagen y una medida coinciden
#define    COMBO_BANDS                  "COMBO:[Open];U;B;V;R;u;g;r;i;z"

int ErrorLine::ANCHOR_ID    = 1;
const char  MdiMPC::MPC_REPORT_INI[] = "mpc_report.ini";

//http://www.minorplanetcenter.net/iau/info/ObsDetails.html
const char* MdiMPC::MPC_HEADERS[]
{
    "COD","Observatory code",               "Your observatory code. If you don't have it and you want to apply for one type <b>XXX</b>.",
    "CON","Contact details, Postal address","Contact name and the full postal address.",
    "CON","Contact details, Email",         "Contact email address.",
    "COM","Site coordinates",               "Geographic coordinates of the observation site and the method used to determine them.<br/>e.g., <b>Long. 06 31 59 W, Lat. 38 53 02.3 N, Alt. 211m, GPS</b>",
    "OBS","Observer",                       "Observer who acquired the data.<br>e.g., <b>P. McCartney</b>",
    "MEA","Measuer",                        "Name of the person who made the measures.<br/>e.g., <b>S. Carl</b>",
    "AC2","Acknowledgement emails",         "Email addresses where the acknowledgement will be sent. Internet-style e-mail address separated by commas.",
    "ACK","Acknowledge Identifier",         "Enables the Minor Planet Center automatically acknowledge receipt of your observations.",
    "TEL","Telescope details",              "Information about the telescope and CCD used to capture the images.<br/>e.g., <b>0.15-m f/10 Refractor + CCD + f/7 focal reducer</b>",
    "NET","Catalog",                        "Name of the catalog used to perform the reduction.",
    "BND","Band",                           "Mag band used for magnitude estimates",
    NULL
};

//http://www.minorplanetcenter.net/iau/info/TelescopeDetails.html
const char* MdiMPC::TELESCOPES[]
{
    "Ritchey-Chretien","Schmidt-Cassegrain","Schmidt",
    "Newtonian reflector","Cassegrain reflector","Cassegrain",
    "hyperbolic astrograph","double astrograph","visual astrograph",
    "astrograph reflector","refractor",
    "Deltagraph","Hypergraph","Maksutov-Newtonian",
    "Maksutov-Cassegrain","Maksutov","Schmidt-Newtonian",
    "Coude","Corrected Dall-Kirkham", NULL
};

const char* MdiMPC::NOTE1[]
{
    "A","earlier approximate position inferior",
    "a","sense of motion ambiguous",
    "B","bright sky/black or dark plate",
    "b","bad seeing",
    "c","crowded star field",
    "D","declination uncertain",
    "d","diffuse image",
    "E","at or near edge of plate",
    "F","faint image",
    "f","involved with emulsion or plate flaw",
    "G","poor guiding",
    "g","no guiding",
    "H","hand measurement of CCD image",
    "h","observed through cloud/haze",
    "I","involved with star",
    "i","inkdot measured",
    "J","J2000.0 rereduction of previously-reported position",
    "K","stacked image",
    "k","stare-mode observation by scanning system",
    "M","measurement difficult",
    "m","image tracked on object motion",
    "N","near edge of plate, measurement uncertain",
    "O","image out of focus",
    "o","plate measured in one direction only",
    "P","position uncertain",
    "p","poor image",
    "R","right ascension uncertain",
    "r","poor distribution of reference stars",
    "S","poor sky",
    "s","streaked image",
    "T","time uncertain",
    "t","trailed image",
    "U","uncertain image",
    "u","unconfirmed image",
    "V","very faint image",
    "W","weak image",
    "w","weak solution",
    NULL
};

const char* MdiMPC::NOTE2[]
{
    "C","CCD",
    "P","Photographic (default if column is blank)",
    "e","Encoder",
    "T","Meridian or transit circle",
    "M","Micrometer",
    "V","\"Roving Observer\" observation",
    "R","Radar observation",
    "S","Satellite observation",
    "c","Corrected-without-republication CCD observation",
    "E","Occultation-derived observations",
    "O","Offset observations (used only for observations of natural satellites)",
    "H","Hipparcos geocentric observations",
    "N","Normal place",
    "n","Mini-normal place derived from averaging observations from video frames",
    NULL
};

const char* MdiMPC::COMET_ORBIT[]
{
    "C", "long-period comet",
    "P", "short-period comet",
    "D", "defunct' comet",
    "X", "uncertain comet",
    "A", "minor planet given a cometary designation",
    NULL
};

const char* MdiMPC::PLANET_IDS[]
{
    "J", "Jupiter",
    "S", "Saturn",
    "U", "Uranus",
    "N", "Neptune",
    NULL
};

const char* MdiMPC::BANDS[]
{
    "I-(Johnson)",
    "R-(Johnson)",
    "V-(Johnson)",
    "B-(Johnson)",
    "U-(Johnson)",
    "z-(Sloan)",
    "i-(Sloan)",
    "r-(Sloan)",
    "g-(Sloan)",
    "u-(Sloan)",
    NULL
};

MdiMPCSettings::MdiMPCSettings()
{
   SetDefaults();
}

MdiMPCSettings::~MdiMPCSettings()
{

}

void MdiMPCSettings::SetDefaults()
{

}

void MdiMPCSettings::Save(QSettings* settings,const QString& section)
{
    SaveFloat(settings, section, "font_size", font_size);
}

void MdiMPCSettings::Load(QSettings* settings,const QString& section)
{
    font_size = ReadFloat(settings, section, "font_size", -1);
}


MdiMPC::MdiMPC(QWidget *parent, MPCReport* report) :
    QTextEdit(parent)
{
    //Cargar las settings
    mSettings = XGlobal::GetSettings(MPC_REPORT_INI);
    mConfig.Load(mSettings, "General");
    if (mConfig.font_size > 0)
    {
        QFont fnt = font();
        fnt.setPointSizeF(mConfig.font_size);
        setFont(fnt);
    }
    mReport = report;
    initExtensionInterface();
    enableDisableActions();
    mImgWarning = QImage(":/graph/images/icn_warning.png");
    mImgError = QImage(":/graph/images/icn_error.png");
    mInfoBox = NULL; mInfoLine = 0;

    mInNumberArea = false;
    setObjectName(QString("MPC report: %1").arg(QFileInfo(report->filename()).fileName()));
    mCursorTimer = new QTimer();
    mCursorTimer->setInterval(500);
    mScreenFont = QFont("Courier New");

    this->setStyleSheet(
                "QTextEdit { font-family: 'Courier New', monaco, monospace; font-size:1em; "
                "selection-color:black; selection-background-color:#C6C3C6; }"
    );

    this->setMouseTracking(true);
    this->setReadOnly(true);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    this->setContextMenuPolicy(Qt::CustomContextMenu);

    QTextDocument *doc = new QTextDocument();
    doc->setUndoRedoEnabled(false);
    doc->setHtml(report->toHtml().replace("<pre>", QString("").sprintf("<pre style='margin-left:%dpx'>",
                                                                         lineNumberAreaWidth())));
    this->setDocument(doc);
    connect(doc, SIGNAL(cursorPositionChanged(QTextCursor)), this, SLOT(highlightCurrentLine()));
    if (verticalScrollBar()) verticalScrollBar()->setCursor(Qt::ArrowCursor);

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ContextMenu_Slot(QPoint)));
    highlightCurrentLine();
    connect(mCursorTimer, SIGNAL(timeout()), this, SLOT(BlinkCursor_Slot()));
    mCursorTimer->start();
    UpdateMPCLayers();
}

QString MdiMPC::getWindowTitle()
{
    if (mReport->dirty())
    {
        return QString("%1 %2").arg(objectName()).arg(" *Modified");
        MainWindow::mainwin->UpdateMenus();
    }
    else return objectName();
}

MdiMPC::~MdiMPC()
{
    //Salvar settings
    mConfig.font_size =  (float)font().pointSizeF();
    mConfig.Save(mSettings,"General");
    if (mSettings) delete mSettings;
    mSettings = NULL;
    //Salvar este report como el último editado
    QFile fil(XGlobal::GetSettingsPath("mpc_last_report.mpc"));
    if (fil.open(QFile::WriteOnly))
    {
        QTextStream ts(&fil);
        for (int i=0; i<mReport->headers()->count(); i++)
            ts << mReport->headers()->data()[i].toString() << "\n";
        fil.close();
    }
    clearErrors();
    destroyExtnsionInterface();
    if (mReport) delete mReport;
    mReport = NULL;
    if (mCursorTimer) delete mCursorTimer;
    mCursorTimer = NULL;
    if (mInfoBox) delete mInfoBox;
    mInfoBox = NULL;

    //Al cerrarse se eliminan todas las capas MPC de todas las imágenes fit
    QList<MdiFitImage*> fi =
        MainWindow::mainwin->fitImages();
    for (int i=0; i<fi.length(); i++)
    {
        XOverlayLayer* ov = fi[i]->getXlayer(XLAYER_MPC_REPORT);
        if (ov) ov->clearItems(XGROUP_MPC_REPORT_MEASURED);
        fi[i]->DeselectCurrentTool_Slot();
    }
}

void MdiMPC::CreateMenus()
{

}

void MdiMPC::initExtensionInterface()
{
    //Acción de "Guardar"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_save_report;
    xp_save_report.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_save_report.act = mSaveReportAction = new QAction(QIcon(ACTION_SAVE_ICON), "Save mpc report", this);
    xp_save_report.act->setObjectName(ACTION_SAVE);
    mSaveReportAction->setShortcut(QKeySequence::Save);
    mSaveReportAction->setStatusTip(ACTION_SAVE_DESC);
    xp_save_report.men = MENU_FILE;
    mSaveReportAction->setEnabled(false);      //No comienza habilitada
    menusItems()->append(xp_save_report);
    xp_save_report.men = TOOLBAR_GENERAL;      //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_save_report);
    connect(xp_save_report.act, SIGNAL(triggered()), this, SLOT(Save_Slot()));

    //Acción de "Guardar Como"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_saveas_report;
    xp_saveas_report.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_saveas_report.act = new QAction("Save mpc report as...", this);
    xp_saveas_report.act->setObjectName(ACTION_SAVEAS);
    xp_saveas_report.act->setStatusTip(ACTION_SAVEAS_DESC);
    xp_saveas_report.men = MENU_FILE;
    menusItems()->append(xp_saveas_report);
    connect(xp_saveas_report.act, SIGNAL(triggered()), this, SLOT(SaveAs_Slot()));

    //Acción de "Validar informe"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_validate_rep;
    xp_mpc_validate_rep.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_validate_rep.act = new QAction(QIcon(ACTION_MPC_VALIDATE_ICON), ACTION_MPC_VALIDATE, this);
    xp_mpc_validate_rep.act->setObjectName(ACTION_MPC_VALIDATE);
    xp_mpc_validate_rep.act->setStatusTip(ACTION_MPC_VALIDATE_DESC);
    xp_mpc_validate_rep.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_validate_rep);
    xp_save_report.men = TOOLBAR_MPC;
    toolbarItems()->append(xp_mpc_validate_rep);
    connect(xp_mpc_validate_rep.act, &QAction::triggered, this, [=]{ Validate_Slot(true); });

    //Acción de "Enviar informe"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_send_rep;
    xp_mpc_send_rep.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_send_rep.act = new QAction(QIcon(ACTION_MPC_SEND_ICON), ACTION_MPC_SEND, this);
    xp_mpc_send_rep.act->setObjectName(ACTION_MPC_SEND);
    xp_mpc_send_rep.act->setStatusTip(ACTION_MPC_SEND_DESC);
    xp_mpc_send_rep.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_send_rep);
    xp_save_report.men = TOOLBAR_MPC;
    toolbarItems()->append(xp_mpc_send_rep);
    connect(xp_mpc_send_rep.act, SIGNAL(triggered()), this, SLOT(SendReport_Slot()));

    //Accion separador
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_sep;
    xp_mpc_sep.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_sep.act = new QAction(this);
    xp_mpc_sep.act->setSeparator(true);
    xp_mpc_sep.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_sep);

    //Acción de "Agregar COD hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_cod;
    xp_mpc_addhdr_cod.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_cod.act = mAddCODHdr = new QAction(ACTION_MPC_ADD_COD_HDR, this);
    xp_mpc_addhdr_cod.act->setObjectName(ACTION_MPC_ADD_COD_HDR);
    xp_mpc_addhdr_cod.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_cod.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_cod);
    connect(xp_mpc_addhdr_cod.act, &QAction::triggered, this, [=]{AddHeader("COD");});

    //Acción de "Agregar CON hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_con;
    xp_mpc_addhdr_con.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_con.act = mAddCONContactHdr = new QAction(ACTION_MPC_ADD_CON_HDR, this);
    xp_mpc_addhdr_con.act->setObjectName(ACTION_MPC_ADD_CON_HDR);
    xp_mpc_addhdr_con.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_con.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_con);
    connect(xp_mpc_addhdr_con.act, &QAction::triggered, this, [=]{AddHeader("CON");});

    //Acción de "Agregar CON2 hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_con2;
    xp_mpc_addhdr_con2.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_con2.act = mAddCONEmailHdr = new QAction(ACTION_MPC_ADD_CON2_HDR, this);
    xp_mpc_addhdr_con2.act->setObjectName(ACTION_MPC_ADD_CON2_HDR);
    xp_mpc_addhdr_con2.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_con2.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_con2);
    connect(xp_mpc_addhdr_con2.act, &QAction::triggered, this, [=]{AddHeader("CON2");});

    //Acción de "Agregar COM hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_com;
    xp_mpc_addhdr_com.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_com.act = mAddCOMHdr = new QAction(ACTION_MPC_ADD_COM_HDR, this);
    xp_mpc_addhdr_com.act->setObjectName(ACTION_MPC_ADD_COM_HDR);
    xp_mpc_addhdr_com.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_com.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_com);
    connect(xp_mpc_addhdr_com.act, &QAction::triggered, this, [=]{AddHeader("COM");});

    //Acción de "Agregar ACK hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_ack;
    xp_mpc_addhdr_ack.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_ack.act = mAddACKHdr = new QAction(ACTION_MPC_ADD_ACK_HDR, this);
    xp_mpc_addhdr_ack.act->setObjectName(ACTION_MPC_ADD_ACK_HDR);
    xp_mpc_addhdr_ack.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_ack.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_ack);
    connect(xp_mpc_addhdr_ack.act, &QAction::triggered, this, [=]{AddHeader("ACK");});

    //Acción de "Agregar AC2 hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_ac2;
    xp_mpc_addhdr_ac2.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_ac2.act = mAddAC2Hdr = new QAction(ACTION_MPC_ADD_AC2_HDR, this);
    xp_mpc_addhdr_ac2.act->setObjectName(ACTION_MPC_ADD_AC2_HDR);
    xp_mpc_addhdr_ac2.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_ac2.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_ac2);
    connect(xp_mpc_addhdr_ac2.act, &QAction::triggered, this, [=]{AddHeader("AC2");});

    //Acción de "Agregar OBS hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_obs;
    xp_mpc_addhdr_obs.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_obs.act = mAddOBSHdr = new QAction(ACTION_MPC_ADD_OBS_HDR, this);
    xp_mpc_addhdr_obs.act->setObjectName(ACTION_MPC_ADD_OBS_HDR);
    xp_mpc_addhdr_obs.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_obs.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_obs);
    connect(xp_mpc_addhdr_obs.act, &QAction::triggered, this, [=]{AddHeader("OBS");});

    //Acción de "Agregar MEA hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_mea;
    xp_mpc_addhdr_mea.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_mea.act = mAddMEAHdr = new QAction(ACTION_MPC_ADD_MEA_HDR, this);
    xp_mpc_addhdr_mea.act->setObjectName(ACTION_MPC_ADD_MEA_HDR);
    xp_mpc_addhdr_mea.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_mea.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_mea);
    connect(xp_mpc_addhdr_mea.act, &QAction::triggered, this, [=]{AddHeader("MEA");});

    //Acción de "Agregar TEL hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_tel;
    xp_mpc_addhdr_tel.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_tel.act = mAddTELHdr = new QAction(ACTION_MPC_ADD_TEL_HDR, this);
    xp_mpc_addhdr_tel.act->setObjectName(ACTION_MPC_ADD_TEL_HDR);
    xp_mpc_addhdr_tel.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_tel.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_tel);
    connect(xp_mpc_addhdr_tel.act, &QAction::triggered, this, [=]{AddHeader("TEL");});

    //Acción de "Agregar NET hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_net;
    xp_mpc_addhdr_net.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_net.act = mAddNETHdr = new QAction(ACTION_MPC_ADD_NET_HDR, this);
    xp_mpc_addhdr_net.act->setObjectName(ACTION_MPC_ADD_NET_HDR);
    xp_mpc_addhdr_net.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_net.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_net);
    connect(xp_mpc_addhdr_net.act, &QAction::triggered, this, [=]{AddHeader("NET");});

    //Acción de "Agregar BND hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_bnd;
    xp_mpc_addhdr_bnd.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_bnd.act = mAddBNDHdr = new QAction(ACTION_MPC_ADD_BND_HDR, this);
    xp_mpc_addhdr_bnd.act->setObjectName(ACTION_MPC_ADD_BND_HDR);
    xp_mpc_addhdr_bnd.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_bnd.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_bnd);
    connect(xp_mpc_addhdr_bnd.act, &QAction::triggered, this, [=]{AddHeader("BND");});

    //Acción de "Agregar NUM hdr"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_mpc_addhdr_num;
    xp_mpc_addhdr_num.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_mpc_addhdr_num.act = mAddNUMHdr = new QAction(ACTION_MPC_ADD_NUM_HDR, this);
    xp_mpc_addhdr_num.act->setObjectName(ACTION_MPC_ADD_NUM_HDR);
    xp_mpc_addhdr_num.act->setStatusTip(ACTION_MPC_ADD_HDR_DESC);
    xp_mpc_addhdr_num.men = MENU_MPC_REPORT;
    menusItems()->append(xp_mpc_addhdr_num);
    connect(xp_mpc_addhdr_num.act, &QAction::triggered, this, [=]{AddHeader("NUM");});

    //Crear los elementos de la barra de estado
    mLabelTotalMeasures = new QLabel(this);
    mLabelObjectMeasures = new QLabel(this);
}

void MdiMPC::destroyExtnsionInterface()
{
    if (mLabelTotalMeasures) delete mLabelTotalMeasures;
    mLabelTotalMeasures = NULL;
    if (mLabelObjectMeasures) delete mLabelObjectMeasures;
    mLabelObjectMeasures = NULL;
}

void MdiMPC::InfoBoxAnchorClicked_Slot(const QUrl& anchor)
{
    mInfoLine = 0;
    //Muy importante el deletelater, ya que estamos en un evento del mismo infobox
    //no debemos eliminarlo aquí bajo ningún concepto
    mInfoBox->deleteLater(); mInfoBox = NULL;
    if (anchor.toString().length() < 2) return;
    bool ok,selected=false;
    int id = anchor.toString().mid(1).toInt(&ok);
    if (!ok || !id) return;
    for (int i=0; i<mErrLines.count() && !selected; i++)
    {
        if (mErrLines.data()[i].anchorid == id && mErrLines.data()[i].action)
        {
            mErrLines.data()[i].action->trigger();
            selected = true;
        }
    }
    if (selected) Validate_Slot();
}


int MdiMPC::lineNumberAreaWidth(bool update)
{
    static int width = 0;
    if (!width || update)
    {
        QFontMetrics fm(mScreenFont);
        return width = fm.width(QLatin1Char('9')) * 5;
    }
    else return width;
}


void  MdiMPC::paintEvent(QPaintEvent* e)
{
    QTextEdit::paintEvent(e);

    QPainter painter(viewport());

    if (mCursorVisible)
    {
        QRect r = cursorRect();
        r.setWidth(2);
        painter.fillRect(r, Qt::black);
    }

    lineNumberAreaPaintEvent(&painter);
}


void MdiMPC::mouseMoveEvent(QMouseEvent *e)
{
    QTextEdit::mouseMoveEvent(e);
    QTextCursor cursor = cursorForPosition(e->pos());
    //Si estamos sobre algún icono de warning/error...
    int line_num = cursor.blockNumber()+1;
    if (e->pos().x() <= lineNumberAreaWidth())
    {
        if (containsWarningOrErrors(line_num) && (!mInfoBox || line_num != mInfoLine))
           showInfoFox(line_num);
        else if (mInfoBox && line_num != mInfoLine)
        {
            mInfoLine = 0;
            mInfoBox->hide();
        }
    }
    else if (mInfoBox && mInfoBox->isVisible())
    {
        mInfoLine = 0;
        mInfoBox->hide();
    }

    //Comprobamos si estamos sobre el área numérica;
    if (e->pos().x() <= lineNumberAreaWidth() && !mInNumberArea)
    {
        viewport()->setCursor(Qt::ArrowCursor);
        mInNumberArea = true;
    }
    else if (e->pos().x() > lineNumberAreaWidth() && mInNumberArea)
    {
        viewport()->setCursor(Qt::IBeamCursor);
        mInNumberArea = false;
    }


   //QTextBlock blk = cursor.block();
   //qDebug() << blk.text() << blk.document()->blockCount() << "\n";
}

void MdiMPC::mousePressEvent(QMouseEvent *e)
{
    static int last_block_number = 0;

    if (e->button() == Qt::LeftButton)
    {
        QTextCursor cursor = cursorForPosition(e->pos());
        this->setTextCursor(cursor);
        mCursorVisible = true;
        if (cursor.blockNumber() != last_block_number)
        {
            last_block_number = cursor.blockNumber();
            highlightCurrentLine();
        }
    }
    QTextEdit::mousePressEvent(e);
}

void MdiMPC::mouseDoubleClickEvent(QMouseEvent *e)
{
    QTextCursor cursor = cursorForPosition(e->pos());
    if (cursor.blockNumber() < mReport->headers()->count()) mpcHeaderRequestEdit();
    else mpcMeasureRequestEdit();
}

void MdiMPC::wheelEvent(QWheelEvent* e)
{
    if (e->modifiers() & Qt::ControlModifier)
    {
       const int delta = e->delta();
       if (delta < 0) zoomOut();
       else zoomIn();
       mConfig.font_size =  (float)font().pointSizeF();
       if (mInfoBox) delete mInfoBox; mInfoBox = NULL;
       return;
    }
    QTextEdit::wheelEvent(e);
}

void MdiMPC::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Down)
    {
        QTextCursor tc = textCursor();
        int cl = tc.columnNumber();
        tc.movePosition(QTextCursor::NextBlock);
        tc.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, cl);
        setTextCursor(tc);
        highlightCurrentLine();
    }
    else if (e->key() == Qt::Key_Up)
    {
        QTextCursor tc = textCursor();
        int cl = tc.columnNumber();
        tc.movePosition(QTextCursor::PreviousBlock);
        tc.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, cl);
        setTextCursor(tc);
        highlightCurrentLine();
    }
    else if (e->key() == Qt::Key_Right)
    {
        QTextCursor tc = textCursor();
        tc.movePosition(QTextCursor::NextCharacter);
        setTextCursor(tc);
        highlightCurrentLine();
    }
    else if (e->key() == Qt::Key_Left)
    {
        QTextCursor tc = textCursor();
        tc.movePosition(QTextCursor::PreviousCharacter);
        setTextCursor(tc);
        highlightCurrentLine();
    }
    else if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
    {
        mpcHeaderRequestEdit();
    }
    else QTextEdit::keyPressEvent(e);
}


void MdiMPC::highlightCurrentLine()
{
    //Crear una selección extra
    QList<QTextEdit::ExtraSelection> extraSelections;
    QTextEdit::ExtraSelection selection;

    selection.format.setBackground(QCOLOR_SEL_LINE);
    selection.format.setProperty(QTextFormat::FullWidthSelection, true);
    selection.cursor = textCursor();
    selection.cursor.clearSelection();
    extraSelections.append(selection);

    //Si estoy en una línea de observación resalto todas aquellas que se refieran a la misma
    int obs_line = selection.cursor.blockNumber()-mReport->headers()->count();
    mObjectId = obs_line >= 0 && obs_line < mReport->measures()->count()
                ? mReport->measures()->data()[obs_line].id() : "";
    for (int i=0; i<mReport->measures()->count(); i++)
    if (i != obs_line && mReport->measures()->data()[i].id() == mObjectId)
    {
        QTextEdit::ExtraSelection selection;
        selection.format.setBackground(QCOLOR_REL_LINE);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        //selection.cursor = cursorForBlock(mReport->headers()->count()+i);
        selection.cursor = QTextCursor(document());
        selection.cursor.setPosition(82*(mReport->headers()->count()+i));
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }
    setExtraSelections(extraSelections);
}

void MdiMPC::BlinkCursor_Slot()
{
   mCursorVisible = !mCursorVisible;
   viewport()->update();
}

void MdiMPC::activate()
{
    if (!mCursorTimer->isActive())
        mCursorTimer->start();
}

void MdiMPC::deactivate()
{
    mCursorTimer->stop();
}


void MdiMPC::lineNumberAreaPaintEvent(QPainter* painter)
{
    QFont mfnt = this->font();
    painter->setFont(mScreenFont);
    painter->fillRect(0, 0, lineNumberAreaWidth(), height(), QCOLOR_LINE_NUMBER_AREA);
    painter->setPen(QCOLOR_LINE_NUMBER_AREA_TEXT);

    int blockNumber = 0;
    int cur_scroll = verticalScrollBar()->value();
    double icon_height = painter->fontMetrics().height()*3/4.0;
    double icon_margin = painter->fontMetrics().height()/4.0 / 2.0;

    QTextBlock block = document()->firstBlock();
    QRectF rct = document()->documentLayout()->blockBoundingRect(block);
    int top = rct.y();
    int y = 0;
    painter->setFont(mfnt);
    while (block.isValid() && y-cur_scroll <= height())
    {
          y = top + blockNumber*rct.height() ;
          if (y-cur_scroll > -rct.height())
          {
              QString number = QString::number(blockNumber + 1);
              painter->drawText(0, rct.top()-cur_scroll, lineNumberAreaWidth() * 3/4, rct.height(),
                               Qt::AlignRight, number);
              //Si la línea contiene un error o un warning se pinta el icono correspondiente
              bool with_error=false, with_warning=false;
              for (int i=0; i<mErrLines.count() && !with_error; i++)
              {
                 if (mErrLines.data()[i].line == block.blockNumber()+1)
                 {
                     if (mErrLines.data()[i].warning) with_warning = true;
                     else with_error = true;
                 }
              }
              if (with_error || with_warning)
              {
                  if (with_error)
                  {
                      QRectF dest_rect(icon_margin, rct.top()-cur_scroll+icon_margin, mImgError.width() * (icon_height/mImgError.height()), icon_height);
                      painter->drawImage(dest_rect, mImgError);
                  }
                  else if (with_warning)
                  {
                      QRectF dest_rect(icon_margin, rct.top()-cur_scroll+icon_margin, mImgWarning.width() * (icon_height/mImgWarning.height()), icon_height);
                      painter->drawImage(dest_rect, mImgWarning);
                  }
              }
          }
          block = block.next();
          if (block.isValid()) rct = document()->documentLayout()->blockBoundingRect(block);
          blockNumber++;
     }
}

QString MdiMPC::parseMPName(QString nm)
{
    QRegularExpression regex(REGEX_MPNAME, QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch mc = regex.match(nm);
    if (!mc.hasMatch()) return "";
    QStringList ls = nm.split('.', QString::SkipEmptyParts);
    QString xb;
    int initial = 0;
    for (;initial<ls.count(); initial++)
        if (ls[initial].trimmed().length() == 0) continue;
        else if (ls[initial].trimmed().length() > 1) break;
        else xb += ls[initial].trimmed().toUpper() + ". ";
    if (initial < 1) return "";
    int names = initial;
    for (;names<ls.count(); names++)
        if (ls[names].trimmed().length() == 0) continue;
        else if (ls[names].trimmed().length() <= 1) return "";
        else xb += ls[names].trimmed().left(1).toUpper() + ls[names].trimmed().mid(1).toLower() + " ";
    names -= initial;
    if (!names || !initial) return "";
    return xb.trimmed();
}

QString MdiMPC::parseMPNames(QString nm)
{
    QString total;
    QStringList mlist = nm.split(',', QString::SkipEmptyParts);
    for (int i=0; i<mlist.count(); i++)
    {
        QString name = parseMPName(mlist.at(i));
        if (!name.length()) continue;
        total += (total.length() ? ", ":"") + name.trimmed();
    }
    return total;
}

void MdiMPC::clearErrors()
{
    for (int i=0; i<mErrLines.count(); i++)
    {
        if (mErrLines.data()[i].action)
            delete mErrLines.data()[i].action;
    }
    mErrLines.clear();
}

void MdiMPC::addError(int line,const QString& desc, QAction* act)
{
    for (int i=0; i<mErrLines.count(); i++)
        if (mErrLines.data()[i].line == line && mErrLines.data()[i].errDesc == desc) return;
    mErrLines.append(ErrorLine(line, desc, act));
}

void MdiMPC::addWarning(int line,const QString& desc, QAction* act)
{
    for (int i=0; i<mErrLines.count(); i++)
        if (mErrLines.data()[i].line == line && mErrLines.data()[i].errDesc == desc) return;
    mErrLines.append(ErrorLine(line, desc, act, true));
}

bool MdiMPC::containsWarningOrErrors(int line)
{
    for (int i=0; i<mErrLines.count(); i++)
        if (mErrLines.data()[i].line == line) return true;
    return false;
}

void MdiMPC::showInfoFox(int line_num)
{
    QTextBlock blk = document()->findBlockByLineNumber(line_num-1);
    QRectF rct = document()->documentLayout()->blockBoundingRect(blk);
    mInfoLine = line_num;
    if (mInfoBox) delete mInfoBox; mInfoBox = NULL;
    if (!mInfoBox)
    {
        mInfoBox = new QTextBrowser(this);
        mInfoBox->document()->setTextWidth(-1);
        //mInfoBox->setMinimumHeight(MIN_INFO_BOX_HEIGHT);
       // mInfoBox->setMinimumWidth(this->width());
       // mInfoBox->setBaseSize(this->width(), this->height());
        mInfoBox->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        mInfoBox->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        mInfoBox->setStyleSheet(
        "QTextBrowser { border: 0.1em solid #CCC; background:#FFC; margin-left:0.5em;"
        "font-family: '" + MainWindow::mainwin->font().family() + "';"
        "font-size:" + QString("").sprintf("%0.2f",mConfig.font_size * 3/4) + "pt;"
        "}");
        connect(mInfoBox, SIGNAL(anchorClicked(QUrl)), this, SLOT(InfoBoxAnchorClicked_Slot(QUrl)));
    }
    mInfoBox->setText(htmlErrors(line_num));
    mInfoBox->setVisible(true);
    mInfoBox->move((int)rct.x(), (int)rct.bottom() - verticalScrollBar()->value());

    //Ajustar el tamaño del info box
    QMargins margs = mInfoBox->contentsMargins();
    int h = margs.top(), w = 0;
    QTextBlock tb = mInfoBox->document()->firstBlock();
    int topmargin = 0;
    while (tb.isValid())
    {
        QRectF rf = mInfoBox->document()->documentLayout()->blockBoundingRect(tb);
        int blockw = mInfoBox->fontMetrics().width(tb.text()) + rf.left()*3;
        if (!topmargin) topmargin = rf.top();
        if (blockw > w) w = (int)blockw;
        h += (int)rf.height();
        tb = tb.next();
    }
    h += (int)(margs.bottom() + topmargin*2);
    mInfoBox->setMinimumHeight(h);
    mInfoBox->setMaximumHeight(h);
    //Cuidado si me salgo de la pantalla
    if (mInfoBox->y() + mInfoBox->height() > this->height())
        mInfoBox->move(mInfoBox->x(), rct.top() - mInfoBox->height()- verticalScrollBar()->value() + 2);
}

QString MdiMPC::htmlErrors(int line)
{

    QString errors;
    QString warnings;
    for (int i=0; i<mErrLines.count(); i++)
    {
       if (mErrLines.data()[i].line == line)
       {
            QString rcad =
                    !mErrLines.data()[i].action ?
                    QString("<li>%1</li>").arg(mErrLines.data()[i].errDesc) :
                    QString("<li>%1 <br/><a href='#%2'>%3</a></li>").arg(mErrLines.data()[i].errDesc)
                        .arg(mErrLines.data()[i].anchorid).arg(mErrLines.data()[i].action->text());
            mErrLines.data()[i].warning ? (warnings += rcad) : (errors += rcad);
       }
    }

    return "<ul>" + errors + warnings + "</ul>";
}

void MdiMPC::enableDisableActions()
{
    mAddCODHdr->setEnabled(!mReport->containsHdr("COD"));
    mAddCONContactHdr->setEnabled(!mReport->containsHdr("CON", QRegularExpression("^[^\\[]")));
    mAddCONEmailHdr->setEnabled(!mReport->containsHdr("CON", QRegularExpression("^\\[")));
    mAddCOMHdr->setEnabled(!mReport->containsHdr("COM"));
    mAddACKHdr->setEnabled(!mReport->containsHdr("ACK"));
    mAddAC2Hdr->setEnabled(!mReport->containsHdr("AC2"));
    mAddOBSHdr->setEnabled(!mReport->containsHdr("OBS"));
    mAddMEAHdr->setEnabled(!mReport->containsHdr("MEA"));
    mAddTELHdr->setEnabled(!mReport->containsHdr("TEL"));
    mAddNETHdr->setEnabled(!mReport->containsHdr("NET"));
    mAddBNDHdr->setEnabled(!mReport->containsHdr("BND"));
    mAddNUMHdr->setEnabled(!mReport->containsHdr("NUM"));
}

bool MdiMPC::isNote1(char note)
{
    if (note == ' ') return true;
    for (int i=0; NOTE1[i]; i+=2)
        if (NOTE1[i][0] == note) return true;
    return false;
}

/**
 * @brief MdiMPC::getHdrPos Obtiene la posición relativa para líneas repetidas
 * @param hdrline Índice basado en 0
 * @return
 */
const int MdiMPC::getHdrPos(int hdrline)
{
    MPCReportHdr* hdr = mReport->headers()->data()+(hdrline-1);
    int cont = 0;
    for (int i=0; i<hdrline-1; i++)
        if (mReport->headers()->data()[i].hdr() == hdr->hdr())
            cont++;
    return cont;
}

const char*  MdiMPC::getHdrName(const QString& hdr, int pos)
{
   return getHdrName(hdr.toLatin1().data(), pos);
}

const char*  MdiMPC::getHdrName(const char* hdr, int pos)
{
    int found = 0;
    for (int i=0; MPC_HEADERS[i]; i+=3)
    {
        if (!strcmp(hdr, MPC_HEADERS[i]))
        {
            if (found == pos) return MPC_HEADERS[i+1];
            else found++;
        }
    }
    return "";
}

const char* MdiMPC::getHdrDesc(const QString& hdr, int pos)
{
    return getHdrDesc(hdr.toLatin1().data(), pos);
}

const char*  MdiMPC::getHdrDesc(const char* hdr, int pos)
{
    int found = 0;
    for (int i=0; MPC_HEADERS[i]; i+=3)
    {
        if (!strcmp(hdr, MPC_HEADERS[i]))
        {
            if (found == pos) return MPC_HEADERS[i+2];
            else found++;
        }
    }
    return "";
}

bool MdiMPC::mpcHeaderRequestEdit()
{
    InputDialog id("", MainWindow::mainwin);
    QTextCursor tc = textCursor();
    int hdrpos = tc.block().blockNumber();
    MPCReportHdr* hdr = mReport->hdrByLine(hdrpos);
    if (!hdr) return false;
    bool hdr_modified = false;
    //COD - Observatory code
    //------------------------------------------------------------------------------
    if (hdr->hdr() == "COD")
    {
        id.SetTitle(hdr->hdr() + ": " + getHdrName("COD"));
        id.SetSubTitle(getHdrDesc("COD"));
        QString ncode = id.ShowText(getHdrName("COD"),
                                    hdr->text(), "XXX").trimmed().toUpper();
        if (ncode.length() && ncode != hdr->hdr())
        {
            if (ncode.length() != 3)
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                            "Invalid observatory code."
                                            "\n\nObservatory codes contains 3 letters or numbers");

            }
            else if (ncode.length() && ncode != hdr->text())
            {
                hdr->setText(ncode);
                for (int i=0; i<mReport->measures()->count(); i++)
                    mReport->measures()->data()[i].setObsCode(ncode);
                hdr_modified = true;
            }
        }
    }

    //AC2 - Acknowledgement emails
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "AC2")
    {
        id.SetTitle(hdr->hdr() + ": " + getHdrName("AC2"));
        id.SetSubTitle(getHdrDesc("AC2"));
        XStringBuilder sb; QString ncode = hdr->text().trimmed(); bool ok=false;
        while (!ok)
        {
            ncode = id.ShowText(getHdrName("AC2"), ncode).trimmed();
            if (!ncode.length()) break;
            QRegularExpression regex(REGEX_EMAIL, QRegularExpression::CaseInsensitiveOption);
            QStringList lst = ncode.split(',');
            bool all_ok = true; sb.clear();
            for (int i=0; i<lst.count() && all_ok; i++)
            {
                if (!lst[i].trimmed().length()) continue;
                if (!regex.match(lst[i].trimmed()).hasMatch()) all_ok = false;
                else
                {
                    if (sb.count()) sb << ", ";
                    sb << lst[i].trimmed();
                }
            }
            if (!all_ok)
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                            "Invalid acknowledgement emails."
                                            "\n\nYou need to provide one or more comma-separated e-mail addresses.");
            }
            else if (all_ok && sb.toString().length() > 80-4)
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                            "Invalid acknowledgement emails."
                                            "\n\nThe resulting line is longer than te maximum 80.");
            } else ok = true;
        }
        if (ok && sb.toString() != hdr->text())
        {
           hdr->setText(sb.toString());
           hdr_modified = true;
        }
    }
    //ACK - Acknowledgement emails
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "ACK")
    {
        id.SetTitle(hdr->hdr() + ": " + getHdrName("ACK", 0));
        id.SetSubTitle(getHdrDesc("ACK", 0));
        QString ncode = id.ShowText(getHdrName("ACK", 0),
                                    "Keep me updated with ID: " + mReport->md5().mid(0,8));
        if (ncode.length() && (hdr_modified = ncode != hdr->text()))
            hdr->setText(ncode);
    }
    //CON - Datos de contacto
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "CON" && !hdr->text().startsWith("["))
    {
        id.SetTitle(hdr->hdr() + ": " + getHdrName("CON", 0));
        id.SetSubTitle(getHdrDesc("CON", 0));
        QString ncode = id.ShowBlob(getHdrName("CON", 0),
                                    hdr->text()).trimmed();
        while (ncode.contains("\n")) ncode = ncode.replace("\n"," ");
        while (ncode.contains("\r")) ncode = ncode.replace("\r"," ");
        while (ncode.contains("\t")) ncode = ncode.replace("\t"," ");
        while (ncode.contains("  ")) ncode = ncode.replace("  "," ");
        if (ncode.length() && ncode != hdr->text())
        {
            hdr->setText(ncode);
            hdr_modified = true;
        }
    }
    else if (hdr->hdr() == "CON" || hdr->hdr() == "CON2")
    {
        hdr->setHdr("CON");
        id.SetTitle(hdr->hdr() + ": " + getHdrName("CON", 1));
        id.SetSubTitle(getHdrDesc("CON", 1));
        QString txt = hdr->text();
        if (txt.startsWith("[") && txt.endsWith("]")) txt = txt.mid(1, txt.length()-2);
        QString ncode = id.ShowText(getHdrName("CON", 1),
                                    txt).trimmed();
        QRegularExpression regex(REGEX_EMAIL, QRegularExpression::CaseInsensitiveOption);
        if (ncode.length())
        {
            if (!regex.match(ncode).hasMatch())
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                            "Invalid contact email."
                                            "\n You need to provide a valid e-mail address.");
            }
            else if (ncode.length() && ncode != txt)
            {
                hdr->setText("["+ncode+"]");
                hdr_modified = true;
            }
        }
    }
    //OBS/MEA - Observador
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "OBS" || hdr->hdr() == "MEA")
    {
        id.SetImage(QPixmap(":/graph/images/icn_users.svg"));
        id.SetTitle(hdr->hdr() + ": " + getHdrName(hdr->hdr().toLatin1().data()));
        id.SetSubTitle(getHdrDesc(hdr->hdr().toLatin1().data()));
        bool ok=false;
        QString ncode = hdr->text();
        while (!ok)
        {
            ncode = id.ShowText(getHdrName(hdr->hdr().toLatin1().data()), ncode).trimmed();
            if (!ncode.length()) break;
            ncode = parseMPNames(ncode);
            if (!ncode.length())
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                     "Invalid name. Please, use one letter initials followed by the second name.<br/><br/>"
                                     "e.g., <b>J. R. R. Tolkien</b>");
            }
            else ok = true;
        }
        if (ncode.length() && ncode != hdr->text())
        {
            hdr->setText(ncode);
            hdr_modified = true;
        }
    }
    //COM - Latitud y longitud
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "COM")
    {
        id.SetImage(QPixmap(":/graph/images/icn_globe.svg"));
        id.SetTitle(hdr->hdr() + ": " + getHdrName(hdr->hdr().toLatin1().data()));
        id.SetSubTitle(getHdrDesc(hdr->hdr().toLatin1().data()));
        MpcGeoInfo gi = mReport->geoInfo();
        QStringList data; data << gi.lng_str << gi.lat_str << gi.alt_str << gi.method;
        QStringList tit; tit << "Longitude" << "Latitude" << "Altitude (m)" << "Method";
        QStringList mask; mask << "" << "" << "" << "COMBO:Google Maps;Google Earth;GPS;Map;Other method";
        bool ok = false;
        double lon,lat; QString method;
        int alt;
        while (!ok)
        {
            data = id.ShowMultipleTexts(tit, data, mask);
            if (!data.length()) break;
            lon = Util::ParseLon(data[0]);
            if (IS_INVALIDF(lon))
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                     "Invalid site longitude<br/><br/>Specify it in format: <b>06 15 59 W</b>.");
                continue;
            }
            lat = Util::ParseLat(data[1]);
            if (IS_INVALIDF(lat))
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                     "Invalid site latitude<br/><br/>Specify it in format: <b>38 50 07.3 N</b>.");
                continue;
            }
            alt = data[2].toInt(&ok);
            if (!ok)
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                     "Invalid site altitude<br/><br/>Specify it in meters.");
                continue;
            }
            method = data[3].trimmed();
        }
        if (ok)
        {
            QString fmt = "Long. " + Util::LON2tring(lon) +
                          ", Lat. " + Util::LAT2String(lat) +
                          ", Alt. " + QString("").sprintf("%d", alt) + "m, " + method;
            if (fmt != hdr->text())
            {
                hdr_modified = true;
                hdr->setText(fmt);
            }
        }
    }
    //TEL - Información del telescopio
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "TEL")
    {
        //http://www.minorplanetcenter.net/iau/info/TelescopeDetails.html
        id.SetImage(QPixmap(":/graph/images/icn_observatory.svg"));
        id.SetTitle(hdr->hdr() + ": " + getHdrName(hdr->hdr().toLatin1().data()));
        id.SetSubTitle(getHdrDesc(hdr->hdr().toLatin1().data()));
        MpcTelInfo ti = mReport->telInfo();
        QStringList hdrs; hdrs << "Telescope type" << "Aperture (m)" << "Focal ratio"
                               << "CCD present" << "CCD description" << "Focal reducer";
        XStringBuilder combotel;
        combotel << "COMBO:"; for (int i=0; TELESCOPES[i]; i++) combotel << TELESCOPES[i] << ";";
        QStringList inputmask; inputmask << combotel.toString() << "" << "" << "COMBO:yes;no" << "" << "";
        QStringList resul;
        resul << ti.type << QString("%1").arg(ti.aperture)
              << (ti.focalratio < 0.01 ? "" :  QString("%1").arg(ti.focalratio))
              << (ti.withCCD?"yes":"no") << ti.ccdInfo
              << (ti.reducer < 0.01 ? "" : QString("%1").arg(ti.reducer));
        bool ok = false;
        while (!ok)
        {
            resul = id.ShowMultipleTexts(hdrs, resul, inputmask);
            if (!resul.length()) break;
            ti.type = resul[0].trimmed();
            ti.aperture = resul[1].trimmed().toFloat(&ok);
            if (!ok || ti.aperture < 0.02 || ti.aperture > 15)
            {
                QMessageBox::warning(MainWindow::mainwin, "Warning",
                                     "Invalid aperture\nPlease specify aperture in meters.");
                continue;
            }
            if (resul[2].trimmed().length())
            {
                ti.focalratio = resul[2].trimmed().toFloat(&ok);
                if (!ok)
                {
                    QMessageBox::warning(MainWindow::mainwin, "Warning", "Invalid focal ratio.");
                    continue;
                }
            }
            else ti.focalratio = 0;
            if (resul[3].trimmed().length())
                ti.withCCD = resul[3].toLower() == "yes";
            ti.ccdInfo = resul[4].trimmed();
            if (resul[5].trimmed().length())
            {
                ti.reducer = resul[5].trimmed().toFloat(&ok);
                if (!ok || ti.focalratio < ti.reducer || ti.focalratio < 1 || ti.reducer < 1)
                {
                    ok = false;
                    QMessageBox::warning(MainWindow::mainwin, "Warning", "Invalid focal reducer ratio.");
                    continue;
                }
            }
        }
        if (ok)
        {           
            QString telinfo = ti.toString();
            if (hdr->text() != telinfo)
            {
                hdr_modified = true;
                hdr->setText(telinfo);
            }
        }
    }
    //NET - Catálogo
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "NET")
    {
        id.SetImage(QPixmap(":/graph/images/icn_stars.svg"));
        id.SetTitle(hdr->hdr() + ": " + getHdrName(hdr->hdr().toLatin1().data()));
        id.SetSubTitle(getHdrDesc(hdr->hdr().toLatin1().data()));
        QStringList strl; QList<QVariant> values;        
        for (VizierCatalog* cat : VizierCatalog::getCatalogs())
        {
            strl <<  cat->name() + " : " + cat->desc();
            values << cat->name();
        }
        QString result = id.ShowList(getHdrName(hdr->hdr().toLatin1().data()), strl, values, hdr->text()).toString();
        if (result.length() && hdr->text() != result)
        {
            hdr->setText(result);
            hdr_modified = true;
        }
    }
    //BND - Banda usada para la estimación de magnitudes
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "BND")
    {
        id.SetImage(QPixmap(":/graph/images/icn_stars.svg"));
        id.SetTitle(hdr->hdr() + ": " + getHdrName(hdr->hdr().toLatin1().data()));
        id.SetSubTitle(getHdrDesc(hdr->hdr().toLatin1().data()));
        QStringList label; label << getHdrName(hdr->hdr());
        QStringList strl; QList<QVariant> values;
        QString seltext;
        for (int i=0;BANDS[i];i++)
        {
            values << QString(BANDS[i]);
            strl << BANDS[i];
            if (hdr->text().length() && hdr->text().at(0) == BANDS[i][0]) seltext = BANDS[i];
        }
        QString result = id.ShowList(getHdrName(hdr->hdr().toLatin1().data()), strl, values, seltext).toString();
        if (result.length() && hdr->text() != result)
        {
            hdr->setText(result.left(1));
            hdr_modified = true;
        }

    }
    //NUM - Número de observaciones
    //------------------------------------------------------------------------------
    else if (hdr->hdr() == "NUM")
    {
        QMessageBox::warning(MainWindow::mainwin, "Warning", "This header is not editable.");
    }

    //Si la cabecera ha sido modificada actualizamos los datos
    if (hdr_modified)
    {
        if (mErrLines.count()) Validate_Slot();
        UpdateTextArea();
    }
    return hdr_modified;
}

bool MdiMPC::mpcMeasureRequestEdit(int line)
{
    //http://www.minorplanetcenter.net/iau/info/OpticalObs.html
    if (line < 0) line = textCursor().blockNumber();
    int mea_index = line - mReport->headers()->count();
    if (mea_index < 0 || mea_index >= mReport->measures()->count()) return false;
    MPCReportMeasure mea = mReport->measures()->at(mea_index);
    QString ant_data = mea.toString();
    //Según el tipo de línea observada se edita de una manera u otra
    if (mea.type() == MPCReportMeasure::MINOR_PLANET && !mpcMeasureEditMinorPlanet(&mea)) return false;
    else if (mea.type() == MPCReportMeasure::COMET && !mpcMeasureEditComet(&mea)) return false;
    else if (mea.type() == MPCReportMeasure::NATURAL_SATELLITE && !mpcMeasureEditSatellite(&mea)) return false;
    //Si ha sido modificada
    if (mea.toString() != ant_data)
    {
        mea.setDirty(true);
        mReport->measures()->replace(mea_index, mea);
        if (mErrLines.count()) Validate_Slot();
        UpdateTextArea();
    }
    UpdateMPCLayers();
    return false;
}

bool MdiMPC::mpcMeasureEditMinorPlanet(MPCReportMeasure* mea)
{
    InputDialog id("", MainWindow::mainwin); float mag;
    id.SetImage(QPixmap(":/graph/images/icn_asteroid.svg"));
    id.SetTitle("Edit Minor Planet measure: " + mea->id());
    id.SetSubTitle("Measure date: " + mea->dateObs().toString("yyyy-MM-dd HH:mm:ss") + " UT");
    QStringList tit; tit << "Number/Desig." << "Discovery" << "Note1" << "Note2" << "Mag" << "Band";
    XStringBuilder snote1, snote2; snote1 << "COMBO:[Empty]"; snote2 << "COMBO:[Empty]";
    QString sel_note1 = "[Empty]", sel_note2 = "[Empty]";
    for (int i=0; NOTE1[i]; i+=2)
    {
        snote1 << ";" << NOTE1[i] << " - " << NOTE1[i+1];
        if (NOTE1[i][0] == mea->note1()) sel_note1 = QString("%1 - %2").arg(NOTE1[i]).arg(NOTE1[i+1]);
    }
    for (int i=0; NOTE2[i]; i+=2)
    {
        snote2 << ";" << NOTE2[i] << " - " << NOTE2[i+1];
        if (NOTE2[i][0] == mea->note2()) sel_note2 = QString("%1 - %2").arg(NOTE2[i]).arg(NOTE2[i+1]);
    }
    QStringList values; values << QString("%1").arg(mea->id()) << (mea->discovery()?"*":"-")
                               << sel_note1 << sel_note2
                               << (mea->mag() < 1 ? "" : QString("").sprintf("%0.1f",mea->mag()))
                               << (mea->band() == ' ' ? "[Open]" : QString("%1").arg(mea->band()));
    QStringList mask; mask << "" << "COMBO:-;*" << snote1.toString() << snote2.toString()
                           << ""
                           << COMBO_BANDS;
    bool ok = false;
    while (!ok)
    {
        ok = true;
        QStringList data = id.ShowMultipleTexts(tit, values, mask);
        if (!data.length()) return false;

        data[0] = data[0].trimmed();
        values = data;
        //Check number
        if (data[0] != mea->id())
        {
            if (MpcUtils::isMinorPlanet(data[0]) || (data[0].toInt(&ok) > 0 &&  ok))
            {
                MpcOrbRecord rec = MpcOrb::findMP(data.at(0));
                if (!rec.valid())
                {
                    QMessageBox::warning(MainWindow::mainwin, "Error",
                                         QString("Unknown minor planet number <b>%1</b><br/><br/>"
                                                 "For temporary designations of unknown objects, please, use an unique 7 character string.").arg(data[0]));
                    ok = false;
                    continue;
                }
                //Antes de añadirlo comprobar si ya existía uno parecido
                int number = (data[0].toInt(&ok) > 0 && ok)?data[0].toInt():MpcUtils::unpack5Number(data[0]);
                ok = true;
                for (int i=0; i<mReport->measures()->count() && ok; i++)
                {
                    if (mReport->measures()->data()+i == mea) continue;
                    if (mReport->measures()->data()[i].number() == number)
                        ok = false;
                }
                if (!ok)
                {
                    QMessageBox::warning(MainWindow::mainwin, "Error",
                                         "A previous measure with the same number exists.");
                    continue;
                }
                mea->setNumber(number);
            }
            else
            {
                if (data[0].length() != 7)
                {
                    QMessageBox::warning(MainWindow::mainwin, "Error", "Provisional or temporary designations are 7 characters long.");
                    ok = false;
                    continue;
                }
                else
                {
                    ok = true;
                    for (int i=0; i<mReport->measures()->count() && ok; i++)
                    {
                        if (mReport->measures()->data()+i == mea) continue;
                        if (mReport->measures()->data()[i].designation() == data[0])
                            ok = false;
                    }
                    if (!ok)
                    {
                        QMessageBox::warning(MainWindow::mainwin, "Error",
                                             "A previous measure with the same designation exists.");
                        continue;
                    }
                    mea->setNumber(0);
                    mea->setDesignation(data[0]);
                    ok = true;
                }
            }
        }
        //Check note1 & note2
        char note1 = data[2].at(0).toLatin1(); if (note1 == '[') note1 = ' ';
        if (note1 != mea->note1())
            mea->setNote1(note1);
        char note2 = data[3].at(0).toLatin1();  if (note2 == '[') note2 = ' ';
        if (note2 != mea->note2())
            mea->setNote2(note2 == '-' ? ' ' : note2);
        //Check discovery asterisk
        if ((data[1] == "*") != (mea->discovery()))
        {
            mea->setDiscovery(data[2] == "*");
        }

        //Check magnitude
        if ((mag = data[4].toFloat(&ok)) < 3 || (mag > 25) || !ok)
        {
            QMessageBox::warning(MainWindow::mainwin, "Error", "Invalid magnitude.");
            ok = false;
            continue;
        } else mea->setMag(mag);
        //Check band
        if (data[5].at(0).toLatin1() != mea->band())
        {
            char chr_band = data[5].at(0).toLatin1();
            mea->setBand(chr_band == '[' ? ' ' : chr_band);
        }
    }
    //Es hora de buscar la imagen a la que pertenece y actualizar la marca
    QList<MdiFitImage*> fi = MainWindow::mainwin->fitImages();
    for (int i=0; i<fi.length(); i++)
    {
        QDateTime dtobs = MpcUtils::GetDateFrame(fi[i]->fitimage()->headers());
        if (dtobs == INVALID_DATETIME) continue;
        if (abs(mea->dateObs().secsTo(dtobs))>SECS_IMAGE_MEASURE) continue;
        fi[i]->MPC_UpdateMark(mea);
    }
    return true;
}

bool MdiMPC::mpcMeasureEditComet(MPCReportMeasure* mea)
{
    InputDialog id("", MainWindow::mainwin); float mag;
    id.SetImage(":/graph/images/icn_comet.svg");
    id.SetTitle("Edit Comet measure: " + mea->id());
    id.SetSubTitle("Measure date: " + mea->dateObs().toString("yyyy-MM-dd HH:mm:ss") + " UT");

    QStringList tit; tit << "Number/Desig." << "Orbit" << "Note1" << "Note2" << "Mag" << "Band";
    XStringBuilder snote1, snote2, sorbit; snote1 << "COMBO:[Empty]"; snote2 << "COMBO:[Empty]"; sorbit << "COMBO:";
    QString sel_note1 = "[Empty]", sel_note2 = "[Empty]", sel_orbit = QString(mea->cometOrbit());
    for (int i=0; COMET_ORBIT[i]; i+=2)
    {
       if (i>0) sorbit << ";";
       sorbit << COMET_ORBIT[i] << " - " << COMET_ORBIT[i+1];
       if (COMET_ORBIT[i][0] == mea->cometOrbit()) sel_orbit = QString("%1 - %2").arg(COMET_ORBIT[i]).arg(COMET_ORBIT[i+1]);
    }
    for (int i=0; NOTE1[i]; i+=2)
    {
        snote1 << ";" << NOTE1[i] << " - " << NOTE1[i+1];
        if (NOTE1[i][0] == mea->note1()) sel_note1 = QString("%1 - %2").arg(NOTE1[i]).arg(NOTE1[i+1]);
    }
    for (int i=0; NOTE2[i]; i+=2)
    {
        snote2 << ";" << NOTE2[i] << " - " << NOTE2[i+1];
        if (NOTE2[i][0] == mea->note2()) sel_note2 = QString("%1 - %2").arg(NOTE2[i]).arg(NOTE2[i+1]);
    }
    QStringList values; values << (mea->number() ? QString("%1").arg(mea->number()) : mea->designation() )
                               << sel_orbit
                               << sel_note1 << sel_note2
                               << (mea->mag() < 1 ? "" : QString("").sprintf("%0.1f",mea->mag()))
                               << (mea->band() == ' ' ? "[Open]" : QString("%1").arg(mea->band()));
    QStringList mask; mask << "" << sorbit.toString() << snote1.toString() << snote2.toString()
                           << ""
                           << COMBO_BANDS;
    bool ok = false;
    while (!ok)
    {
        ok = true;
        QStringList data = id.ShowMultipleTexts(tit, values, mask);
        if (!data.length()) return false;
        for (int i=0; i<data.length(); i++) data[i] = data[i].trimmed();

        //Check number or designation
        if (data[0].toInt(&ok) > 0 && ok && data[0].toInt() < 9999)
        {
            mea->setNumber(data[0].toInt());
            ok = true;
        }
        else if (data[0].length() == 7)
        {
            mea->setDesignation(data[0]);
            ok = true;
        }
        else
        {
            QMessageBox::warning(this, "Error", "Invalid comet number or designation");
            ok = false;
        }
        //Check orbit type
        char orbit = data[1].at(0).toLatin1();
        mea->setComtOrbit(orbit);
        //Check note1 & note2
        char note1 = data[2].at(0).toLatin1(); if (note1 == '[') note1 = ' ';
        if (note1 != mea->note1())
            mea->setNote1(note1);
        char note2 = data[3].at(0).toLatin1();  if (note2 == '[') note2 = ' ';
        if (note2 != mea->note2())
            mea->setNote2(note2 == '-' ? ' ' : note2);

        //Check magnitude
        if ((mag = data[4].toFloat(&ok)) < 3 || (mag > 25) || !ok)
        {
            QMessageBox::warning(MainWindow::mainwin, "Error", "Invalid magnitude.");
            ok = false;
            continue;
        } else mea->setMag(mag);
        //Check band
        if (data[5].at(0).toLatin1() != mea->band())
        {
            char chr_band = data[5].at(0).toLatin1();
            mea->setBand(chr_band == '[' ? ' ' : chr_band);
        }
    }
    return true;
}

bool MdiMPC::mpcMeasureEditSatellite(MPCReportMeasure* mea)
{
    InputDialog id("", MainWindow::mainwin); float mag;
    id.SetImage(":/graph/images/icn_nsatellite.svg");
    id.SetTitle("Edit Natural Satellite: " + mea->id());
    id.SetSubTitle("Measure date: " + mea->dateObs().toString("yyyy-MM-dd HH:mm:ss") + " UT");

    QStringList tit; tit << "Planet" << "Number/Desig." << "Note1" << "Note2" << "Mag" << "Band";
    XStringBuilder snote1, snote2, splanet; snote1 << "COMBO:[Empty]"; snote2 << "COMBO:[Empty]"; splanet << "COMBO:";
    QString sel_note1 = "[Empty]", sel_note2 = "[Empty]";
    QString sel_planet = QString(mea->planet());
    for (int i=0; PLANET_IDS[i]; i+=2)
    {
       if (i>0) splanet << ";";
       splanet << PLANET_IDS[i] << " - " << PLANET_IDS[i+1];
       if (mea->planet() == PLANET_IDS[i][0]) sel_planet = QString("%1 - %2").arg(PLANET_IDS[i]).arg(PLANET_IDS[i+1]);
    }
    for (int i=0; NOTE1[i]; i+=2)
    {
        snote1 << ";" << NOTE1[i] << " - " << NOTE1[i+1];
        if (NOTE1[i][0] == mea->note1()) sel_note1 = QString("%1 - %2").arg(NOTE1[i]).arg(NOTE1[i+1]);
    }
    for (int i=0; NOTE2[i]; i+=2)
    {
        snote2 << ";" << NOTE2[i] << " - " << NOTE2[i+1];
        if (NOTE2[i][0] == mea->note2()) sel_note2 = QString("%1 - %2").arg(NOTE2[i]).arg(NOTE2[i+1]);
    }
    QStringList values; values << sel_planet
                               << (mea->number() ? QString("%1").arg(mea->number()) : mea->designation())
                               << sel_note1 << sel_note2
                               << (mea->mag() < 1 ? "" : QString("").sprintf("%0.1f",mea->mag()))
                               << (mea->band() == ' ' ? "[Open]" : QString("%1").arg(mea->band()));
    QStringList mask; mask << splanet.toString() << "" << snote1.toString() << snote2.toString()
                           << ""
                           << COMBO_BANDS;
    bool ok = false;
    while (!ok)
    {
        ok = true;
        QStringList data = id.ShowMultipleTexts(tit, values, mask);
        if (!data.length()) return false;
        for (int i=0; i<data.length(); i++) data[i] = data[i].trimmed();
        //check planet
        char planet = data[0].at(0).toLatin1();
        mea->setPlanet(planet);
        //Check number or designation
        if (data[1].toInt(&ok) > 0 && ok && data[1].toInt() < 999)
        {
            mea->setNumber(data[1].toInt());
            ok = true;
        }
        else if (data[1].length() == 7 && data[1].endsWith('0'))
        {
            mea->setDesignation(data[1]);
            ok = true;
        }
        else
        {
            QMessageBox::warning(this, "Error",
            "Invalid natural satellite number or designation.\n\nNatural satellite provisional designations are 7 characer lenght and ends with 0.");
            ok = false;
        }
        //Check note1 & note2
        char note1 = data[2].at(0).toLatin1(); if (note1 == '[') note1 = ' ';
        if (note1 != mea->note1())
            mea->setNote1(note1);
        char note2 = data[3].at(0).toLatin1();  if (note2 == '[') note2 = ' ';
        if (note2 != mea->note2())
            mea->setNote2(note2 == '-' ? ' ' : note2);

        //Check magnitude
        if ((mag = data[4].toFloat(&ok)) < 3 || (mag > 25) || !ok)
        {
            QMessageBox::warning(MainWindow::mainwin, "Error", "Invalid magnitude.");
            ok = false;
            continue;
        } else mea->setMag(mag);
        //Check band
        if (data[5].at(0).toLatin1() != mea->band())
        {
            char chr_band = data[5].at(0).toLatin1();
            mea->setBand(chr_band == '[' ? ' ' : chr_band);
        }
    }
    return true;
}

void MdiMPC::CheckDirty()
{
    mSaveReportAction->setEnabled(mReport->dirty());
    QString tit = getWindowTitle();
    if (windowTitle() != tit || objectName() != tit)
    {
        setWindowTitle(tit);
        MainWindow::mainwin->UpdateMenus();
    }
}

void MdiMPC::moveCursorToBlock(int block)
{
    QTextBlock tb = document()->findBlockByNumber(block);
    if (tb.blockNumber() == block)
    {
        QTextCursor tc = QTextCursor(document());
        tc.setPosition(tb.position());
        setTextCursor(tc);
        highlightCurrentLine();
    }
}

QTextCursor MdiMPC::cursorForBlock(int block)
{
    QTextBlock tb = document()->findBlockByNumber(block);
    if (tb.blockNumber() == block)
    {
        QTextCursor tc = QTextCursor(document());
        tc.setPosition(tb.position());
        return tc;
    }
    else return QTextCursor(document());
}

void MdiMPC::EditMeasureByCoords(double ra, double dec)
{
    for (int i=0; i<mReport->measures()->count(); i++)
    {
        if (Util::Adist(ra, dec,mReport->measures()->data()[i].ra(), mReport->measures()->data()[i].dec())<ASECS_IMAGE_MEASURE)
        {
            mpcMeasureRequestEdit(i+mReport->headers()->count());
            return;
        }
    }
}

MpcGeoInfo MdiMPC::latLong()
{
    MpcGeoInfo geo;
    geo = mReport->geoInfo();
    if (!geo.isValid())
    {
        MPCReportHdr* hdr = mReport->hdrByHdr("COD");
        if (hdr && hdr->isValid())
        {
           MpcObservatory obs = MpcObservatory::find(hdr->text().trimmed());
           if (obs.isValid())
           {
               geo.lat = obs.lat();
               geo.lng = obs.lon();
               geo.alt = 0;
               geo.lat_str = Util::LAT2String(geo.lat);
               geo.lng_str = Util::LON2tring(geo.lng); //Se asegura de que sea válido
               geo.method = hdr->text();
           }
        }
    }
    else geo.method = "";
    return geo;
}

void MdiMPC::UpdateTextArea()
{
    QTextCursor tc = textCursor();
    int curpos = tc.position();
    int pos = verticalScrollBar()->value();
    setHtml(mReport->toHtml().replace("<pre>",  QString("").sprintf("<pre style='margin-left:%dpx'>",
                                                                         lineNumberAreaWidth())));
    tc = QTextCursor(document());
    tc.setPosition(curpos);
    setTextCursor(tc);
    verticalScrollBar()->setValue(pos);
    highlightCurrentLine();
    CheckDirty();
}

void MdiMPC::Save_Slot()
{
    if (!mReport->filename().length())
    {
        QString sel_fmt = "MPC report (*.mpc)";
        QString fname =  QFileDialog::getSaveFileName(this,
                                                      "Save file as",
                                                      "",
                                                      sel_fmt,
                                                      &sel_fmt
                                                      );
        if (!fname.length()) return;
        mReport->setFilename(fname);
        setObjectName(QFileInfo(fname).fileName());
        CheckDirty();
    }
    QFile fil(mReport->filename());
    if (!fil.open(QFile::WriteOnly))
    {
        mReport->setFilename("");
        mReport->setDirty(false);
        setObjectName("");
        CheckDirty();
        QMessageBox::warning(MainWindow::mainwin, "Error", "Error saving file: " + mReport->filename());
        return;
    }
    fil.write(mReport->toString().toLatin1());
    mReport->setDirty(false);
    CheckDirty();
}

void MdiMPC::SaveAs_Slot()
{
    QFileInfo finfo = QFileInfo(filename());
    QString sel_fmt = finfo.suffix().toLower() == "mpc" ? SAVE_FMT_MPC : SAVE_FMT_TXT;
    QString fname =  QFileDialog::getSaveFileName(this,
                                                  "Save file as",
                                                  finfo.absoluteFilePath(),
                                                  SAVE_FMT_MPC ";;" SAVE_FMT_TXT,
                                                  &sel_fmt
                                                  );
    if (!fname.length()) return;
    QFile fout(fname);
    if (!fout.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this,"Error", QString("Unable to save file '%1'").arg(fname));
        return;
    }
    fout.write(mReport->toString().toLatin1());
    fout.close();
}

bool MdiMPC::Validate_Slot(bool user_triggered)
{
    int start_errors = mErrLines.count();
    clearErrors();
    bool ok; QAction* act;
    //http://www.minorplanetcenter.net/iau/info/ObsDetails.html
    //Hay medidas en el informe
    if (!mReport->measures()->count())
        addWarning(1, "No measures in the current report");
    //Contiene la cabecera COD y el formato y posiciones son válidos
    int cod_hdr = mReport->containsHdr("COD");
    if (cod_hdr == 0)
    {
        act = new QAction("Add COD line",this);
        connect(act, &QAction::triggered, this, [this]{AddHeader("COD");});
        addError(1, "No COD line found. The first line of the report must be the COD header.", act);
    }
    else if (cod_hdr != 1)
    {
        act = new QAction("Sort report headers",this);
        connect(act, &QAction::triggered, this, [this]{mReport->sortHdrs(); UpdateTextArea();});
        addError(cod_hdr, "COD line must come first.", act);
    }
    QString obscode = cod_hdr ? mReport->headers()->data()[cod_hdr-1].text() : "";
    QRegularExpression regex_obs(REGEX_OBSCODE);
    if (cod_hdr && !regex_obs.match(mReport->headers()->data()[cod_hdr-1].text()).hasMatch())
    {
        act = new QAction("Edit COD line",this);
        connect(act, &QAction::triggered, this, [this]{EditHeader("COD");});
        addError(cod_hdr, mReport->headers()->data()[cod_hdr-1].text() + "is not a valid observatory code",act);
    }

    //Línea CON con los datos de contacto, obligatoria como segunda línea para observatorios distintos de XXX
    int con_contact = mReport->containsHdr("CON", QRegularExpression("^[^\\[]"));
    int con_email = mReport->containsHdr("CON", QRegularExpression("^\\["));
    if (con_contact == con_email) con_contact = 0;
    if (!con_contact && obscode == "XXX")
    {
        act = new QAction("Add contact details.",this);
        connect(act, &QAction::triggered, this, [this]{AddHeader("CON");});
        addError(1, "To request an observatory code, please, include your full postal address.",act);
    }
    if (con_contact)
    {
        //No contiene la dirección postal
        QStringList lstpostal = mReport->headers()->data()[con_contact-1].text().split(',',QString::SkipEmptyParts);
        if (lstpostal.count() < 2)
        {
            act = new QAction("Edit CON header to add postal address",this);
            connect(act, &QAction::triggered, this, [this]{EditHeader("CON");});
            addWarning(con_contact, "Please, include your postal address after your name.",act);
        }
    }
    if (con_email)
    {
        //Tiene el mail por defecto
        if (mReport->headers()->data()[con_email-1].text() == DEFAULT_MAIL)
        {
            act = new QAction("Edit COD line",this);
            connect(act, &QAction::triggered, this, [this]{EditHeader("COD");});
            addError(con_email, "Not a valid email address.",act);
        }
    }

    //Validar línea OBS y MEA
    int obs_hdr = mReport->containsHdr("OBS");
    if (obs_hdr)
    {
        QString mpn = parseMPNames(mReport->headers()->data()[obs_hdr-1].text());
        if (!mpn.length())
        {
           act = new QAction("Edit OBS header",this);
           connect(act, &QAction::triggered, this, [this]{EditHeader("OBS");});
           addError(obs_hdr, "Invalid name. Check name format " + mReport->headers()->data()[obs_hdr-1].text(), act);
        }
        else if (mpn != mReport->headers()->data()[obs_hdr-1].text().trimmed())
        {
            act = new QAction("Change contents to <b>"+mpn+"</b>",this);
            connect(act, &QAction::triggered, this, [&]{ReplaceRaw(obs_hdr-1, mpn);});
            addWarning(obs_hdr, "Invalid OBS header. The header is not well formed.",act);
        }
    }
    int mea_hdr = mReport->containsHdr("MEA");
    if (mea_hdr)
    {
        QString mpn = parseMPNames(mReport->headers()->data()[mea_hdr-1].text());
        if (!mpn.length())
        {
           act = new QAction("Edit MEA header",this);
           connect(act, &QAction::triggered, this, [this]{EditHeader("MEA");});
           addError(mea_hdr, "Invalid name. Check name format " + mReport->headers()->data()[mea_hdr-1].text(), act);
        }
        else if (mpn != mReport->headers()->data()[mea_hdr-1].text().trimmed())
        {
            act = new QAction("Change contents to <b>"+mpn+"</b>",this);
            connect(act, &QAction::triggered, this, [&]{ReplaceRaw(mea_hdr-1, mpn);});
            addWarning(mea_hdr, "Invalid MEA header. The header is not well formed.",act);
        }
    }

    //Validar línea COM con la posición geográfica
    int com_hdr = mReport->containsHdr("COM");
    if (com_hdr)
    {
        MpcGeoInfo gi = mReport->geoInfo();
        if (!gi.isValid())
        {
            act = new QAction("Edit COM header",this);
            connect(act, &QAction::triggered, this, [this]{EditHeader("COM");});
            addError(com_hdr, "Invalid COM header. Invalid format.",act);
        }
    }
    else
    {
        if (obscode == "XXX")
        {
            act = new QAction("Add COM header",this);
            connect(act, &QAction::triggered, this, [this]{AddHeader("COM");});
            addError(1, "No COM header found. COM header is mandatory when you're requesting a new observatory code.",act);
        }
        else
        {
            act = new QAction("Add COM header",this);
            connect(act, &QAction::triggered, this, [this]{AddHeader("COM");});
            addWarning(1, "No COM header found. Add a COM header to specify your site coordinates.",act);
        }
    }


    //Validar línea TEL
    int tel_hdr = mReport->containsHdr("TEL");
    if (tel_hdr)
    {
        QString tel_text = mReport->headers()->data()[tel_hdr-1].text().trimmed();
        MpcTelInfo ti = mReport->telInfo();
        if (ti.errors.length() || !ti.isValid())
        {
            act = new QAction("Edit TEL header",this);
            connect(act, &QAction::triggered, this, [this]{EditHeader("TEL");});
            addError(tel_hdr, "Invalid TEL header.", act);
        }
        else if (!tel_text.contains("CCD") && !ti.errors.length())
        {
            act = new QAction("Add CCD keyword to the TEL line",this);
            connect(act, &QAction::triggered, this, []{;});
            addWarning(tel_hdr, "No CCD keyword. Arn't you using a CCD?", act);
        }
        else if (ti.toString() != tel_text && !ti.errors.length())
        {
            QString suggested = ti.toString();
            act = new QAction("Change contents to <b>"+suggested+"</b>",this);
            connect(act, &QAction::triggered, this, [&]{ReplaceRaw(tel_hdr-1, suggested);});
            addWarning(tel_hdr, "Invalid TEL header. The header is not well formed.",act);
        }
    }
    else
    {
        act = new QAction("Add TEL header",this);
        connect(act, &QAction::triggered, this, [this]{AddHeader("TEL");});
        addWarning(1, "No TEL header found. Add a TEL header to specify your telescope and CCD information.",act);
    }

    //Validar línea ACK
    int ack_hdr = mReport->containsHdr("ACK");
    if (!ack_hdr)
    {
        act = new QAction("Add ACK header",this);
        connect(act, &QAction::triggered, this, [this]{AddHeader("ACK");});
        addWarning(1, "Suggestion. Add an ACK header to identify your report.", act);
    }

    //Validar línea AC2
    int ac2_hdr = mReport->containsHdr("AC2");
    if (ac2_hdr)
    {
        bool ok_ac2 = true;
        QRegularExpression rgx_mail(REGEX_EMAIL,QRegularExpression::CaseInsensitiveOption);
        QString ac2 = mReport->headers()->data()[ac2_hdr-1].text().trimmed();
        QStringList stl = ac2.split(",",QString::SkipEmptyParts);
        for (int i=0; i<stl.length() && ok_ac2; i++)
        {
            if (!rgx_mail.match(stl[i].trimmed()).hasMatch())
            {
                act = new QAction("Edit AC2 header",this);
                connect(act, &QAction::triggered, this, [this]{EditHeader("AC2");});
                addError(ac2_hdr, "Invalid AC2 header.", act);
                ok_ac2 = false;
            }
        }
        //Encontrar emails duplicados
        XStringBuilder xb_ac2;
        for (int i=0; i<stl.length(); i++)
        {
            xb_ac2 << (xb_ac2.count() ? ", " : "") << stl.at(i).trimmed();
            for (int j=i+1; j<stl.length(); j++)
            {
                if (ok_ac2 && (stl.at(i).trimmed() == stl.at(j).trimmed()))
                {
                    act = new QAction("Edit AC2 header",this);
                    connect(act, &QAction::triggered, this, [this]{EditHeader("AC2");});
                    addWarning(ac2_hdr, "Duplicated emails in AC2 header.", act);
                    ok_ac2 = false;
                }
             }
        }
        QString suggested = xb_ac2.toString();
        if (xb_ac2.count() && suggested != ac2)
        {
            act = new QAction("Change to <b>" + suggested + "</b>",this);
            connect(act, &QAction::triggered, this, [&]{ReplaceRaw(ac2_hdr-1, suggested);});
            addWarning(ac2_hdr, "The header AC2 is not well formed.", act);
        }
    }
    else
    {
        act = new QAction("Add AC2 header",this);
        connect(act, &QAction::triggered, this, [this]{AddHeader("AC2");});
        addWarning(1, "No AC2 header found. If this report is sent through XParallax you will need an Acknowledgement email to get the MPC response.", act);
    }

    //Validar línea NET
    int net_hdr = mReport->containsHdr("NET");
    if (net_hdr)
    {
        QString net = mReport->headers()->data()[net_hdr-1].text().trimmed();
        ok = false;
        for (VizierCatalog* cat : VizierCatalog::getCatalogs())
        {
            ok = cat->name() == net;
            if (ok) break;
        }
        if (!ok)
        {
            act = new QAction("Edit NET header",this);
            connect(act, &QAction::triggered, this, [this]{ EditHeader("NET"); });
            addError(net_hdr, "Invalid NET header. Unknown catalog.", act);
        }
    }
    else
    {
        act = new QAction("Add NET header",this);
        connect(act, &QAction::triggered, this, [this]{AddHeader("NET");});
        addWarning(1, "No NET header found. Add a NET header to specify the source catalog used for the reduction.",act);
    }

    //Validar línea NUM
    int num_hdr = mReport->containsHdr("NUM");
    if (num_hdr)
    {
        QString num = mReport->headers()->data()[num_hdr-1].text().trimmed();
        int xnum = num.toInt(&ok);
        if (!ok)
        {
            act = new QAction("Set NUM header to the right value",this);
            connect(act, &QAction::triggered, this,
                    [=]{ mReport->headers()->data()[num_hdr-1].setText(QString("%1").arg(mReport->measures()->count()));
                        UpdateTextArea();});
            addError(num_hdr, "Invalid NUM header. It doesn't contains a valid number.", act);
        }
        else if (xnum != mReport->measures()->count())
        {
            act = new QAction("Set NUM header to the right value",this);
            connect(act, &QAction::triggered, this,
                    [=]{ mReport->headers()->data()[num_hdr-1].setText(QString("%1").arg(mReport->measures()->count()));
                        UpdateTextArea();});
            addError(num_hdr, "The observation count does not match with NUM header.", act);
        }
    }
    else
    {
        act = new QAction("Add NUM header",this);
        connect(act, &QAction::triggered, this, [=]{AddHeader("NUM");});
        addWarning(mReport->headers()->count()+1, "Add a NUM header containing the number of observations.",act);
    }

    //Validar líneas de observación
    for (int i=0; i<mReport->measures()->count(); i++)
    {
        //Comprobar que el código de observatorio coincide con el código de todas las observaciones
        if (obscode.length() && mReport->measures()->data()[i].obsCode() != obscode)
        {
            act = new QAction("Set observatory code to: <b>" + obscode + "</b>",this);
            connect(act, &QAction::triggered, this, [=]{SetMeasureObsCode(i);});
            addError(i+mReport->headers()->count()+1, "The observatory code does not match the header CON",act);
        }
        if (mReport->measures()->data()[i].note2() != 'C')
        {
           act = new QAction("Set NOTE2 to <b>C</b>",this);
           connect(act, &QAction::triggered, this, [=]{SetMeasureNote(i, 2, 'C');});
           addWarning(i+mReport->headers()->count()+1,"Note2 distinct than <b>C</b>. Aren't you using a CCD?",act);
        }
        if (!isNote1(mReport->measures()->data()[i].note1()))
        {
           act = new QAction("Clear NOTE1",this);
           connect(act, &QAction::triggered, this, [=]{SetMeasureNote(i, 1, ' ');});
           addError(i+mReport->headers()->count()+1,
                    QString("Unknown note1 <b>")+mReport->measures()->data()[i].note1()+"</b>",act);
        }

        for (int j=i+1; j<mReport->measures()->count(); j++)
        {
             //Observaciones repetidas o muy parecidas
             if (mReport->measures()->data()[i].id() ==  mReport->measures()->data()[j].id()
                  && mReport->measures()->data()[i].dateObs() ==  mReport->measures()->data()[j].dateObs())
             {
                act = new QAction("Remove this observation",this);
                connect(act, &QAction::triggered, this, [=]{RemoveMeasure(i);});
                addError(i+mReport->headers()->count()+1,"Duplicated entry. Line: " + QString("%1").arg(mReport->headers()->count()+j+1),act);
                act = new QAction("Remove this observation",this);
                connect(act, &QAction::triggered, this, [=]{RemoveMeasure(j);});
                addError(j+mReport->headers()->count()+1,"Duplicated entry. Line: "+ QString("%1").arg(mReport->headers()->count()+i+1),act);
             }
        }
        //Comprobar la posición del planeta menor o cometa
        char buff_obj[512];
        Sky::OrbElems orbelems;
        MpcGeoInfo latlon = latLong();
        MpcOrbRecord rec =  MpcOrb::findObject(mReport->measures()->data()[i].id(), buff_obj);
        if (rec.valid())
        {
            double ra, dec;
            double jd = Util::JulianDate(mReport->measures()->data()[i].dateObs());
            if ((mReport->measures()->data()[i].type() == MPCReportMeasure::MINOR_PLANET && Sky::ParseMPElems(buff_obj, &orbelems)) ||
                (mReport->measures()->data()[i].type() == MPCReportMeasure::COMET && Sky::ParseCometElems(buff_obj, &orbelems)))
            {
                //Si tengo latitud y longitud puedo usar coordenadas topocéntricas
                if (latlon.isValid())
                    MdiFitImage::MPC_Topo(jd, &orbelems, latlon.lat, latlon.lng, latlon.alt, &ra, &dec);
                else
                    Sky::CometPos(&orbelems, jd, &ra, &dec);
                double ddist = Util::Adist(mReport->measures()->data()[i].ra(), mReport->measures()->data()[i].dec(),ra,dec);
                if (ddist*3600.0 > 7)
                {
                    if (ddist*3600.0 < 20)
                    {
                       addWarning(i+mReport->headers()->count()+1,
                                  "Possible wrong measure (diff " + QString("").sprintf("%0.0f",ddist*3600) + "\")"
                                  "<br/>The measure coordinates differs from the nominal position of the object."
                                  );
                    }
                    else
                    {
                        QString diff;
                        if (ddist*3600.0 < 60) diff = QString("").sprintf("%0.0f",ddist*3600) + "\"";
                        else if (ddist*60.0 < 60) diff = QString("").sprintf("%0.1f",ddist*60) + "'";
                        else diff = QString("").sprintf("%0.1f",ddist) + "º";
                        addError(i+mReport->headers()->count()+1,
                                   "<b style='color:red;'>Wrong measure</b> (diff " + diff + ")"
                                 "<br/>The measure coordinates differs from the nominal position of the object."
                                 );
                    }
                }
            }
        }
        //Warning de objeto no encontrado
        else
        {
            addWarning(i+mReport->headers()->count()+1, "Unknown object");
        }
    }
    if (user_triggered && !mErrLines.count())
        QMessageBox::information(MainWindow::mainwin, "Validation success","Congratulations!!\n\nThe current report contains no errors.");

    if (mErrLines.count() || mErrLines.count() != start_errors) update();
    return !mErrLines.count();
}



void MdiMPC::SendReport_Slot()
{
    if (!mReport->measures()->count())
    {
        QMessageBox::warning(MainWindow::mainwin, "Warning",
                                 "There are no observations in the current report.\n"
                                 "\nYou can't send it.");
        return;
    }
    int num_msg = 0;
    QString url = QString(XGlobal::DEBUG ? "http://minorplanetcenter.net/submit_obs_test":"http://minorplanetcenter.net/submit_obs");
    QString md5 = mReport->md5();
    if (!Validate_Slot())
    {
        num_msg++;
        if (QMessageBox::warning(MainWindow::mainwin, "Warning",
                                 "There are errors in the current report.\n\nDo you still want to continue?",
                                 QMessageBox::Yes|QMessageBox::No) == QMessageBox::No) return;
    }
    bool already_sent=false;
    QFile fil_sent(XGlobal::GetSettingsPath("sent_reports.txt"));
    if (fil_sent.open(QFile::ReadOnly))
    {
        QTextStream ts(&fil_sent);
        while (!ts.atEnd() && !already_sent)
            if (ts.readLine().trimmed() == md5) already_sent = true;
        fil_sent.close();
    }
    if (already_sent)
    {
        num_msg++;
        if (QMessageBox::warning(MainWindow::mainwin, "Warning",
                                 "A previous report containing this observations was sent to the Minor Planet Center.\n"
                                 "\nDo you still want to continue?",
                                 QMessageBox::Yes|QMessageBox::No) == QMessageBox::No) return;
    }
    if ((!num_msg || XGlobal::DEBUG)
            && QMessageBox::warning(MainWindow::mainwin, "Warning",
                                     #ifdef QT_DEBUG
                                        "<b style='color:red;'>SEND REPORT IN DEBUG MODE</b><br/>"
                                        "<b>This will not take effect</b><br/><br/>"
                                     #endif
                                         "This will send the current report to the Minor Planet Center.<br/><br/>"
                                         "Are you sure?",
                                         QMessageBox::Yes|QMessageBox::No) == QMessageBox::No) return;

    //Envío de los datos ----------->
    QEventLoop loop;
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"source\""));
    textPart.setBody(mReport->toString().toLatin1());
    multiPart->append(textPart);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, XUSER_AGENT);

    QNetworkAccessManager manager;
    INFO("Connecting to: %s", PTQ(url));
    QNetworkReply *reply = manager.post(request, multiPart);
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    QString data = QString(reply->readAll()).trimmed();
    if (data.length()) INFO("Response from the MPC: %s", PTQ(data));
    int error = (int)reply->error();
    if (error !=  QNetworkReply::NoError ||
            !(data.contains("Submission", Qt::CaseInsensitive) && data.contains("received", Qt::CaseInsensitive)))
    {
        QString errorString = WebReader::formatError(error);
        if (error != QNetworkReply::NoError) ERR("ERROR: %d : %s",error, PTQ(errorString));
        QMessageBox::warning(this, "Error", data.length() ? data : errorString, QMessageBox::Ok);
        return;
    }
    XGlobal::APP_SETTINGS.ct_mpc++;
    QMessageBox::information(this,"Report sent","Your report was sent succesfuly.<br/><br/>Response from the MPC:<br/><b>"
                             + data.remove("[]") + "</b>");
    //Guardamos los datos en disco
    if (fil_sent.open(QFile::Append))
    {
        QTextStream ts(&fil_sent);
        ts << md5 << "\n";
        fil_sent.close();
    }
}

void  MdiMPC::ContextMenu_Slot(QPoint pos)
{
     QTextCursor cur  = textCursor();
     QTextCursor tcur = cursorForPosition(pos);
     int linepos = tcur.blockNumber()+1;
     QPoint globalPos = this->mapToGlobal(pos);
     QMenu* ctxMenu = new QMenu(this);

     //Crear las acciones disponibles
     QAction* copy_action = cur.selectionStart() != cur.selectionEnd() && cur.selectionStart() > -1 && cur.selectionEnd() > -1
             ? ctxMenu->addAction("Copy to clipboard") : NULL;
     QAction* edit_hdr = linepos >= 1 && linepos <= mReport->headers()->count() && mReport->headers()->data()[linepos-1].hdr() != "NUM" ?
                           ctxMenu->addAction(QIcon(":/graph/images/pen-16.png"),QString("Edit header '%1'").arg(mReport->headers()->data()[linepos-1].hdr())):NULL;
     QAction* edit_measure = linepos > mReport->headers()->count() && linepos < (mReport->measures()->count() + mReport->headers()->count() + 1) ?
                               ctxMenu->addAction(QIcon(":/graph/images/pen-16.png"),QString("Edit measure")) : NULL;
     //La acción rawedit de momento la desactivo
     /*
     QAction* rawedit = linepos < mReport->headers()->count() + mReport->measures()->count() + 1 ?
                        ctxMenu->addAction(QIcon(":/graph/images/pen-16.png"),QString("Raw edit")) : NULL;
     */
     QAction* delete_hdr = linepos >= 1 && linepos <= mReport->headers()->count() ?
                           ctxMenu->addAction(QIcon(":/graph/images/icn_cross.svg"),QString("Delete header '%1'").arg(mReport->headers()->data()[linepos-1].hdr())):NULL;
     QAction* select_all = ctxMenu->addAction("Select all");

     QAction* delete_measure = linepos > mReport->headers()->count() && linepos < (mReport->measures()->count() + mReport->headers()->count() + 1) ?
                               ctxMenu->addAction(QIcon(":/graph/images/icn_cross.svg"),QString("Delete measure")) : NULL;

     if (mAddCODHdr->isEnabled() || mAddCONContactHdr->isEnabled() || mAddCONEmailHdr->isEnabled() || mAddCOMHdr->isEnabled() ||
         mAddACKHdr->isEnabled() || mAddAC2Hdr->isEnabled() || mAddOBSHdr->isEnabled() || mAddMEAHdr->isEnabled() ||
          mAddTELHdr->isEnabled() || mAddNETHdr->isEnabled() || mAddNUMHdr->isEnabled()) ctxMenu->addSeparator();
     if (mAddCODHdr->isEnabled()) ctxMenu->addAction(mAddCODHdr);
     if (mAddCONContactHdr->isEnabled()) ctxMenu->addAction(mAddCONContactHdr);
     if (mAddCONEmailHdr->isEnabled()) ctxMenu->addAction(mAddCONEmailHdr);
     if (mAddCOMHdr->isEnabled()) ctxMenu->addAction(mAddCOMHdr);
     if (mAddACKHdr->isEnabled()) ctxMenu->addAction(mAddACKHdr);
     if (mAddAC2Hdr->isEnabled()) ctxMenu->addAction(mAddAC2Hdr);
     if (mAddOBSHdr->isEnabled()) ctxMenu->addAction(mAddOBSHdr);
     if (mAddMEAHdr->isEnabled()) ctxMenu->addAction(mAddMEAHdr);
     if (mAddTELHdr->isEnabled()) ctxMenu->addAction(mAddTELHdr);
     if (mAddNETHdr->isEnabled()) ctxMenu->addAction(mAddNETHdr);
     if (mAddNUMHdr->isEnabled()) ctxMenu->addAction(mAddNUMHdr);

     QAction* sel_action = ctxMenu->actions().count() ? ctxMenu->exec(globalPos) : NULL;

     //Ejecutar la acción seleccionada
     if (sel_action)
     {
         if (sel_action == copy_action)
         {
             QString st = cur.selection().toPlainText();
             QApplication::clipboard()->setText(st);
         }
         else if (sel_action == select_all)
         {
            tcur.select(QTextCursor::Document);
            setTextCursor(tcur);
         }
         else if (sel_action == edit_hdr)
         {
            setTextCursor(tcur);
            mpcHeaderRequestEdit();
         }
         else if (sel_action == edit_measure)
         {
            setTextCursor(tcur);
            mpcMeasureRequestEdit();
         }
         else if (sel_action == delete_hdr)
         {
             if (QMessageBox::warning(MainWindow::mainwin, "Warning",
                                      "<style type='text/css'>.hdr{color:#000080; font-weight:bold;}</style>"
                                      "The header will be deleted.<br/><br/>"+
                                      mReport->headers()->data()[linepos-1].toHtml() +
                                      "<br/><br/>Are you sure?",
                                      QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes)
             {
                setTextCursor(tcur);
                mReport->headers()->removeAt(linepos-1);
                mReport->setDirty(true);
                UpdateTextArea();
                enableDisableActions();
                if (mErrLines.count()) Validate_Slot();
                UpdateMPCLayers();
             }
         }
         else if (sel_action == delete_measure)
         {
             if (QMessageBox::warning(MainWindow::mainwin, "Warning",
                                      "The measure line will be deleted.<br/><br/><tt>"+
                                       mReport->measures()->data()[linepos - mReport->headers()->count() - 1].toHtml() +
                                      "</tt><br/><br/>Are you sure?",
                                      QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes)
             {
                setTextCursor(tcur);
                mReport->removeMeasure(linepos - mReport->headers()->count() - 1);
                UpdateTextArea();
                enableDisableActions();
                if (mErrLines.count()) Validate_Slot();
                UpdateMPCLayers();
            }
         }
     }
     CheckDirty();
     delete ctxMenu;
}

void MdiMPC::UpdateMPCLayers()
{
    QList<MdiFitImage*> fi =
        MainWindow::mainwin->fitImages();
    for (int i=0; i<fi.length(); i++)
        UpdateMPCLayer(fi.at(i));
}

void MdiMPC::UpdateMPCLayer(MdiFitImage* fi)
{
    if (!fi->plate()) return;
    QDateTime dtobs = MpcUtils::GetDateFrame(fi->fitimage()->headers());
    if (dtobs == INVALID_DATETIME) return;
    XOverlayLayer* layer = fi->getXlayer(XLAYER_MPC_REPORT);

    for (int i=0; i<mReport->measures()->count(); i++)
    {
        //Comparación de fechas con una desviación máxima de 2 segundos
        if (abs(mReport->measures()->data()[i].dateObs().secsTo(dtobs))>SECS_IMAGE_MEASURE) continue;
        if (layer)
        {
            //Buscar la marca y actualizarla si fuera necesario
            fi->MPC_UpdateMark(mReport->measures()->data()+i);
        }
        else
        {
            //La marca no existe, pues la añadimos
            fi->MPC_AddMark(mReport->measures()->data()+i, -1);
        }
    }
    //Marcas borradas
    for (int i=0;layer && i<layer->items()->count(); i++)
    {
        XOverlayItem* it = layer->items()->at(i);
        double src_ra = it->bx();
        double src_dec = it->by();
        bool found = false;
        for (int j=0; j<mReport->measures()->count() && !found; j++)
        {
           if (it->label().contains(mReport->measures()->data()[j].id()) ||
                (mReport->measures()->data()[j].number() && it->label().contains(QString("(%1)").arg(mReport->measures()->data()[j].number())))
               )
           {
               double dist = Util::Adist(mReport->measures()->data()[j].ra(),mReport->measures()->data()[j].dec(),src_ra,src_dec);
               if (dist < 0.1) found = true;
           }
        }
        if (!found)
        {
            layer->removeItem(it);
            i--;
        }
    }
}

void MdiMPC::EditHeader(const QString& hdr)
{
    int hdrpos = mReport->containsHdr(hdr);
    if (hdrpos) EditHeader(hdrpos);
}

void MdiMPC::EditHeader(int hdrpos)
{
    moveCursorToBlock(hdrpos-1);
    if (mpcHeaderRequestEdit() && mErrLines.count())
    {
        Validate_Slot();
    }
}

void MdiMPC::RemoveMeasure(int pos)
{
    mReport->measures()->removeAt(pos);
    //Si hay una línea NUM actualizo el número de observaciones
    int net_idx = mReport->containsHdr("NUM");
    if (net_idx) mReport->headers()->data()[net_idx-1].setText(QString("%1").arg(mReport->measures()->count()));
    UpdateTextArea();
    if (mErrLines.count()) Validate_Slot();
}

void MdiMPC::SetMeasureObsCode(int pos)
{
    int cod_idx = mReport->containsHdr("COD");
    if (!cod_idx) return;
    QString cod = mReport->headers()->data()[cod_idx-1].text();
    mReport->measures()->data()[pos].setObsCode(cod);
    UpdateTextArea();
    if (mErrLines.count()) Validate_Slot();
}

void MdiMPC::SetMeasureNote(int pos, int note, char val)
{
    if (note != 1 && note != 2) return;
    note == 1 ? mReport->measures()->data()[pos].setNote1(val) : mReport->measures()->data()[pos].setNote2(val);
    UpdateTextArea();
    if (mErrLines.count()) Validate_Slot();
}

void MdiMPC::AddHeader(const QString& hdr)
{
    MPCReportHdr newhdr;
    newhdr.setHdr(hdr);
    if (hdr == "NUM") newhdr.setText(QString("%1").arg(mReport->measures()->count()));
    else if (hdr == "CON2") newhdr.setText(QString("[%1]").arg(DEFAULT_MAIL));
    else newhdr.setText("");

    mReport->headers()->insert(0, newhdr);
    QTextCursor old_cursor = textCursor();
    QTextCursor cursor = cursorForPosition(QPoint(0,0));
    this->setTextCursor(cursor);
    if (hdr == "NUM" || mpcHeaderRequestEdit())
    {
        mReport->sortHdrs();
        UpdateTextArea();
        enableDisableActions();
        int newpos;
        if (hdr != "CON2") newpos = mReport->containsHdr(newhdr.hdr());
        else newpos = mReport->containsHdr("CON", QRegularExpression("^\\["));
        if (newpos) moveCursorToBlock(newpos-1);
    }
    else
    {
        mReport->headers()->removeAt(0);
        setTextCursor(old_cursor);
    }

    if (mErrLines.count()) Validate_Slot();
}


void MdiMPC::ReplaceRaw(int line, const QString& rawdata)
{
    if (line < 0) line = textCursor().blockNumber();
    if (line < 0) return;
    //Si estoy en una cabecera
    if (line < mReport->headers()->count())
    {
        MPCReportHdr* hdr = mReport->headers()->data()+line;
        hdr->setText(rawdata);
        UpdateTextArea();
        if (mErrLines.count()) Validate_Slot();
    }
    //Si estoy en una línea de observación
    else
    {
        //No implementeado
    }
}




