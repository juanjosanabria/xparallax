#ifndef MDILOGGER_H
#define MDILOGGER_H

#include <QPlainTextEdit>
#include "log.h"
#include "xpmdiwidget.h"

class MdiLogger : public QPlainTextEdit, public XPMdiWidget
{
    Q_OBJECT
    Q_INTERFACES(XPMdiWidget)

public:
    explicit MdiLogger(QWidget *parent = 0);
    virtual ~MdiLogger();

protected:
    void close();
    virtual void initExtensionInterface();
    virtual void destroyExtnsionInterface();

signals:
    
public slots:
    void Log_Slot(qint64 thread_id, int log_type, QString msg);
    void ContextMenu_Slot(QPoint p);
    void Save_Slot();


private:
    void CreateMenus();
    char mBuffer[LOG_BUFFER_LEN];

    QMenu*      mContextMenu;
    QAction*    mSelectAllAction;
    QAction*    mCopyAction;
    QAction*    mClearAction;
    QAction*    mSaveAction;
    QAction*    mClose;

};

#endif // MDILOGGER_H
