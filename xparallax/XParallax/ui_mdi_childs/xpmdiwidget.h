#ifndef XPMDIWIDGET_H
#define XPMDIWIDGET_H

#include <QtWidgets>
#include "../types.h"

enum EXT_ITEM_BEHAVIOUR
{
    ITEM_BEHAVIOUR_NULL = 0,            //El item será ignorado a todos los efectos
    ITEM_BEHAVIOUR_ENABLED_ALWAYS,      //Siempre está activo y se verá esta opción
    ITEM_BEHAVIOUR_ENABLE_DISABLE,      //Se muestra siempre, pero se habilita siempre cuando la subventana correspondiente tiene el foco
    ITEM_BEHAVIOUR_SHOW_HIDE,           //Se muestra solo si se está en la subwindow correspondiente
    ITEM_BEHAVIOUR_CHECK_UNCHECK,       //Se muestra siempre, pero se chequea cuando la subventana correspondiente tiene el foco
    ITEM_BEHAVIOUR_WIDGET_CONTROLLED    //Se preguntará al widget por el estado de visible/checked
};

//Solamente para identificar el tipo de acción,
enum EXT_ITEM_GROUP
{
    EXTGRP_UNDEFINED,
    EXTGRP_IMGTOOLS,        //Herramientas de imagen
    EXTGRP_IMG,             //Del menú imagen
    EXTGRP_IMGANALYSIS,     //Del menú img analysis
};

struct XPEXT_ACTION_ITEMS
{
    QAction*				act;    //Acción a realizar, preconfigurada con todo lo necesario
    EXT_ITEM_BEHAVIOUR		beh;    //Comportamiento, forma de visualzar y activar el item
    QString                 men;    //Nombre del menú al que pertenecerá. Puede ser en el formato Menu/Submenu
    int                     pri;    //Prioridad de esta acción a la hora de ordenarlas
    int                     mpr;    //Prioridad del menú si fuera necesario insertar
    EXT_ITEM_GROUP          grp;    //Grupo de acciones al que pertenece. Puede usarse o dejarse sin usar
    //Inicializaciones por defecto de esta estructura
    XPEXT_ACTION_ITEMS(){pri=0; beh=ITEM_BEHAVIOUR_ENABLE_DISABLE; mpr=MENU_ADDITIONAL_PRIORITY;grp=EXTGRP_UNDEFINED;}
};


class XPMdiWidget
{
public:
    XPMdiWidget();
    virtual ~XPMdiWidget();

private:
    QList<XPEXT_ACTION_ITEMS>     mMenusItems;      /** Elementos que se añadirán a los menús **/
    QList<XPEXT_ACTION_ITEMS>     mToolbarItems;    /** Elementos que se añadirán a las barras de herramientas **/
    QList<QWidget*>               mStatusWidgets;   /** Widgets de la barra de estado **/

protected:

public:
    QList<XPEXT_ACTION_ITEMS>* menusItems() {return &mMenusItems;}
    QList<XPEXT_ACTION_ITEMS>* toolbarItems(){return &mToolbarItems;}
    QList<QWidget*>*           statusWidgets(){return &mStatusWidgets;}

    virtual void activate();
    virtual void deactivate();
};

Q_DECLARE_INTERFACE(XPMdiWidget, "XPMdiWidget. JJS 1.0")


#endif // XPMDIWIDGET_H











