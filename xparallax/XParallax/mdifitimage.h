#ifndef MDIFITIMAGE_H
#define MDIFITIMAGE_H

#include <QPointF>
#include <QVector>
#include "fitimage.h"
#include "detection.h"
#include "plateconstants.h"
#include "overlay/xoverlay.h"
#include "imgtools/ximgtool.h"
#include "ui_mdi_childs/xpmdiwidget.h"
#include "ui_helpers/progressdialog.h"
#include "matcher.h"
#include "mpc.h"
#include "sky.h"

class  Overlay;
struct MpcKnownObject
{
    MpcKnownObject(){;}
    MpcKnownObject(QString s_id, QString s_data,MPCReportMeasure::MeasureType s_type,Sky::OrbElems s_elems)
    {
        id = s_id;
        mpcorb_data = s_data;
        type = s_type;
        orb_elems = s_elems;
    }
    QString                         id;
    QString                         mpcorb_data;
    MPCReportMeasure::MeasureType   type;
    Sky::OrbElems                   orb_elems;
};


class MdiFitImage : public QWidget, public XPMdiWidget
{
    Q_OBJECT
    Q_INTERFACES(XPMdiWidget)

private:
    QAction*                      mSaveFitAction;
    QAction*                      mShowHeadersAction;
    QString                       mErrorMsg;
    //Items pertenecientes a menús
    //********************************************************************************************************************
    void initExtensionInterface();
    void destroyExtnsionInterface();
    void Init(FitImage* fimage);

public:
    explicit MdiFitImage(QWidget *parent, const QString& filename);
    explicit MdiFitImage(QWidget *parent, FitImage* fimage);
    virtual ~MdiFitImage();

    inline FitImage* fitimage(){return mFitImage;}
    inline QImage*  image(){return &mImage;}
    inline float weelSpeed(){return mWheelSpeed;}
    inline void  SetWheelSpeed(float speed){mWheelSpeed = speed;}
    inline const QString& filename(){return mFitImage->filename();}
    inline const QString& errorMsg(){return mErrorMsg;}
    inline float scale(){return mScale;}
    void SetScale(float scale);
    inline PlateConstants* plate(){return mPlate;}
    inline void SetMdiWindow(QMdiSubWindow* win){mMdiSubWindow = win;}
    inline QMdiSubWindow *MdiWindow(){return mMdiSubWindow;}
    void SetMatch(Matcher* mc, PlateConstants* pc);
    virtual void converge();
    virtual void activate();
    virtual void deactivate();
    void AddMarks(QStringList& marks);                                  /** Añade las marcas solicitadas por línea de comandos **/

    bool LookForPlateInFit();
    void CheckDirty();
    void SetImageCenterRADec(double ra, double lat);
    void setImageCenterPixel(double x, double y);
    ConvergeProfile* cProfile(){return mConvergeProfile;}

    //Nuevos overlays
    //************************************************************************************************************
    inline QList<XOverlayLayer*>*  xLayers(){return &mXOverlays;}        /** Devuelve la lista de las capas **/
    void addXLayer(XOverlayLayer *layer, int priority = 100);            /** Agrega una capa a la imagen con una prioridad en el menú **/
    XOverlayLayer* getXlayer(QString& name);                             /** Devuelve la capa por nombre **/
    XOverlayLayer* getXlayer(const char* name);                          /** Devuelve la capa por nombre **/
    virtual void updateMesh(int x1=MININT, int y1=0, int x2=0, int y2=0,
                            bool border=false);                          /** Crea/Actualiza la capa de mesh de la imagen **/
    void updateAuxLayer();

    //Transformaciones de imagen y astrométricas
    //************************************************************************************************************
    void         ImageXYToScreenXY(float imagex,float imagey, QPointF& scrxy);
    void         ImageXYToScreenXY(const QPointF& imgxy, QPointF& scrxy);
    void         ScreenXYToImageXY(float screenx, float screeny, QPointF& imgxy);
    void         ScreenXYToImageXY(const QPointF& scrxy, QPointF& imgxy);
    inline bool  hasMap(){return mPlate;}
    inline void  map(double x, double y, double* ra, double* dec){if(mPlate)mPlate->map(x,y,ra,dec);}
    inline void  imap(double ra, double dec, double* x, double* y){if(mPlate)mPlate->imap(ra,dec,x,y);}

    //Funciones estáticas que obtienen el objeto y llaman a las funciones de objeto
    //************************************************************************************************************
    static void  ImageXYToScreenXY_ST(QObject* obj, float imagex, float imagey, QPointF& scrxy);
    static void  ScreenXYToImageXY_ST(QObject* obj, float screenx, float screeny, QPointF& imgxy);
    static void  map_ST(QObject* obj, double x, double y, double *ra, double *lat);
    static void  imap_ST(QObject* obj, double ra, double lat, double *x, double *y);
    static void  SaveRefStars(PlateConstants pt);


    virtual void keyPressEvent(QKeyEvent* event);

protected:
    //Eventos de control sobrecargados y sobrecargables
    //************************************************************************************************************
    virtual void paintEvent(QPaintEvent *event);
    virtual void wheelEvent(QWheelEvent* event);
    virtual void mousePressEvent(QMouseEvent* event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual void setFitImage(FitImage* fi);
    virtual void paintStart(QPaintEvent *event, QPainter* painter);
    virtual void paintImage(QPaintEvent *event, QPainter* painter);
    virtual void paintLayers(QPaintEvent *event, QPainter* painter);
    virtual void paintEnd(QPaintEvent *event, QPainter* painter);
    virtual QString getWindowTitle();

    //No virtuales accesibles
    QPointF&  imageCenter(){return mImageCenter;}
    inline void setPlate(PlateConstants* pt){mPlate = pt;} //Para cambios intencionados

private:
    bool                    mCreating;
    QMdiSubWindow*          mMdiSubWindow;
    FitImage*               mFitImage;
    QBrush                  mBackground;
    QBrush                  mStarGround;
    QImage                  mImage;
    quint8                  mColorTable[CP_COLOR_TABLE_SZ]; //Tabla de escala de grises ecualizada
    ConvergeProfile*        mConvergeProfile;
    float                   mWheelSpeed;                    //Velocidad de la rueda
    int                     mSignificantDigits;             //Dígitos significativos de la media de la imagen
    float                   mScaleFactor;                   //Factor de escala para mostrar los dígitosº

    //Opciones de transformación
    QTransform              mTransform;
    int                     mClientWidth;                   //Ancho de la ventana de dibujo, se mantiene actualizado
    int                     mClientHeight;                  //Alto de la ventana de dibujo, se mantiene actualizado
    float                   mScale;                         //Escala en el eje x e y

    QPointF                 mImageCenter;                   //Píxel de la imagen que está en el centro de la ventana

    QPointF                 mAuxPointF;
    QPoint                  mDragStartPoint;                //Punto de inicio del desplazamiento
    QPointF                 mStarDespCenter;                //Posición del centro cuando empezó el desplazamiento

    PlateConstants*         mPlate;                         //Constantes de astrometría

    inline bool             dragging(){return mDragStartPoint.x() || mDragStartPoint.y(); }
    void                    ResetTransform();

    //Relativo a los overlays y herramientas
    //************************************************************************************************************
    QList<XOverlayLayer*>   mXOverlays;     //Lista de capas
    XOverlayLayer*          mMeshLayer;     //Capa de malla de coordenadas
    XOverlayLayer*          mAuxLayer;      //Capa auxiliar, direcciones de los ejes
    QAction*                mMeshAction;    //Acción heredada para mostrar la capa de mesh
    QList<XImgTool*>        mTools;         //Lista de todas las tools creadas
    XImgTool*               mSelectedTool;  //Tool actualmente seleccionada

    //Relativo a MPC check
    //************************************************************************************************************
    FileDownloader*         mFileDownloader;
    XOverlayLayer*          mKnownObjectsLayer;
    QAction*                mMpcKnownObjectsAction;
    QAction*                mMpcAddMeasureAction;

public slots:
    void Save_Slot();
    void SaveAs_Slot();
    void ExportView_Slot();
    void FitHeaders_Slot();
    void Restorepos_Slot();
    void FitToWindow_Slot();
    void SelectTool_Slot(QString name);
    void ShowHideLayer_Slot(QObject* ly);
    bool DeselectCurrentTool_Slot();
    void ContextMenu_Slot(QPoint p);

    //Menús y opciones de Minor Planet Center
    //************************************************************************************************************
private:
    MpcGeoInfo              mMpcGeoInfo;                             //Información geográfica para calbular coordenadas topocéntricas
    QVector<MpcKnownObject> mMpcKnownObjs;                           //Objetos conocidos MPC

    static ProgressDialog*  mMpcKnownObjectPd;                       //ProgressDialog de KnownObjectss
    static int MPC_KnownObjectThread(void *p);                       //Hilo de cálculo de known objects

public slots:
    void MPC_Mpcorb_Slot(bool dont_prompt=false);                    //Descarga de archivos de órbitas del MPC
    void MPC_KnownObjs_Slot();                                       //Busca objetos conocidos en el frame actual

public:
    void MPC_InitMenu();                                             //Inicialización del menú MPC
    void MPC_NewReport();                                            //Crea un nueo informe
    void MPC_CheckMenu();                                            //Comprueba si hay que mostrar ciertos menús
    void MPC_KnownObjLayerUpdate();                                  //Actualiza la capa de objetos conocidos
    void MPC_AddMark(MPCReportMeasure* mea, float radius=5);         //Añade una marca de medida MPC
    void MPC_UpdateMark(MPCReportMeasure* mea,XOverlayItem* it=NULL);//Actualiza la etiqueta de una marca
    void MPC_AddKnownObject(const QString objectid, double ra, double dec, float mag=0);
    static void MPC_Topo(double jd,Sky::OrbElems* elems,double lat,double lon,double alt,double*ra,double*dec);

};




#endif // MDIFITIMAGE_H




