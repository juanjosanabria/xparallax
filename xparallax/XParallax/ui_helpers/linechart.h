#ifndef LINECHART_H
#define LINECHART_H

#include "core.h"
#include <QWidget>
#include <QPen>

class LineChartParameters
    : Parameters
{
public:
    LineChartParameters();
    ~LineChartParameters();
    void SetDefaults();
    void Save(QSettings *settings, const QString &section);
    void Load(QSettings *settings, const QString &section);

    int    show_inter_poly;
    int    poly_resolution;
    int    poly_degree;


};

class LineChart : public QWidget
{
    Q_OBJECT

public:
    explicit LineChart(QWidget *parent = 0);
    ~LineChart();

    void setXData(const QVector<double>& data);
    void setYData(const QVector<double>& data);
    void paintEvent(QPaintEvent *);
    LineChartParameters* parameters(){return &mParameters;}

private:
    LineChartParameters     mParameters;
    QBrush                  mBackgroundBrush;
    QBrush                  mAxisColor;
    QBrush                  mDataBrush;
    QPen                    mDataPen;
    QPen                    mAxisPen;
    QPen                    mInterpolantPen;
    QFont                   mFontAxis;
    QFont                   mFontTitle;

    QVector<double>         mXData;
    double                  mMaxX;

    QVector<double>         mYData;
    double                  mMinData;
    double                  mMaxData;

    int                     mLeftMargin;
    int                     mTopmargin;
    int                     mRightMargin;
    int                     mBottomMargin;

    QVector<double>         mInterpolationX;
    QVector<double>         mInterpolationY;
    double                  mRMS;


    void Interpolate_Poly();
};

#endif // LINECHART_H
