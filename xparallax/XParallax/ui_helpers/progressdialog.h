#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QDialog>
#include <QTimer>
#include "progress.h"
#include "../ui_helpers/animatedlabel.h"
#include "../inputdialog.h"

namespace Ui {
class ProgressDialog;
}

class ProgressDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProgressDialog(QWidget *parent,const QString& title);
    ~ProgressDialog();

    void Connect(BackgroundWorker* wk);
    void ConnectThis(); //Para su uso sin backgroundworkers
    int ExecInBackground(int(*func)(void *obj), void *user_data);
    void SetIcon(const QString& icon);
    void SetMarquee(bool marquee);
    bool cancelled(){return mCancelled;}
    void keyPressEvent(QKeyEvent* ke);
    //void reject(){ mCancelled ? QDialog::reject() : qt_noop(); }

private:
    Ui::ProgressDialog *ui;

    BackgroundWorker*           mCurrentWorker;
    int                         mSize;
    int                         mPosition;
    bool                        mMarquee;
    bool                        mStarted;
    bool                        mEnded;
    bool                        mCancelled;
    bool                        mCloseWhenFinish;
    QTime                       mStartTime;
    QTimer                      mTimer;
    double                      mLastRemain;

    QList<BackgroundWorker*>    mWorkers;
    int(*                       mRunFunc)(void*);

    int StartInBackground(void *user_params);

signals:
    void StartModal_Signal();
    void Start_Signal();
    void End_Signal();
    void Error_Signal(QString desc);
    void Setup_Signal(int size);
    void Msg_Signal(QString msg);
    void TitleChange_Signal(QString msg);
    void MainTitleChange_Signal(QString msg);
    void Progress_Signal(int position);

public slots:
    //Slots conectados a los backgroundworkers
    void Start_Slot(BackgroundWorker*wk);
    void End_Slot(BackgroundWorker *wk);
    void Error_Slot(BackgroundWorker *wk, QString desc);
    void Setup_Slot(BackgroundWorker* wk, int size);
    void Msg_Slot(BackgroundWorker* wk, QString msg);
    void TitleChange_Slot(BackgroundWorker *wk, QString msg);
    void MainTitleChange_Slot(BackgroundWorker *wk, QString msg);
    void Progress_Slot(BackgroundWorker* wk,int position);
    void Timer_Slot();

private slots:
    //Slots de uso interno
    void StartModal_Slot();
    void Cancel_Slot();

};

#endif // PROGRESSDIALOG_H
