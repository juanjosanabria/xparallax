#include "animatedlabel.h"
#include "xglobal.h"

AnimatedLabel::AnimatedLabel(QWidget *parent)
    :     QLabel(parent), mCurrentPixmap(0)
{
    mSpriteCount = 0;
    setTimeOut(20);
    connect(&mTimer, SIGNAL(timeout()), SLOT(changeImage()));
    //Si tiene pixmap intento elegirlo
}


void AnimatedLabel::setImage(const QString& image)
{
   mImage.load(image);
   Construct();
}

void AnimatedLabel::setSpriteCount(int sprites)
{
    mSpriteCount = sprites;
    Construct();
}

void  AnimatedLabel::Construct()
{
    if (mImage.height() <= 0 || mImage.width() <= 0 || mSpriteCount <= 0) return;
    pixmaps.clear();
    int subImageWidth= mImage.width() / mSpriteCount;
    for (int i = 0; i < mSpriteCount; i++)
    {
        QImage subImage = mImage.copy(i * subImageWidth,0,subImageWidth, mImage.height());
        pixmaps.push_back(QPixmap::fromImage(subImage));
    }
}

void  AnimatedLabel::CheckNullimg()
{
    if (mSpriteCount) return;
    if (pixmap())
    {
        QPixmap* pm = const_cast<QPixmap*>(pixmap());
        int height  = pm->height();
        int sprites = pm->width() / height;
        if (pm->width() % pm->height() == 0)
        {
            mSpriteCount = sprites;
            int subImageWidth = pm->width() / mSpriteCount;
            for (int i = 0; i < mSpriteCount; i++)
            {
                QPixmap subImage = pm->copy(i * subImageWidth, 0, subImageWidth, pm->height());
                pixmaps.push_back(subImage);
            }
        }
    }
    QSize sz = baseSize();
    this->setMinimumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
    this->setMaximumSize(sz.width() * XGlobal::SCREEN_DPI, sz.height() * XGlobal::SCREEN_DPI);
}

void AnimatedLabel::changeImage()
{
    if (mCurrentPixmap >= pixmaps.length())
        mCurrentPixmap = 0;

    setPixmap(pixmaps.at(mCurrentPixmap));

    mCurrentPixmap++;
}
void AnimatedLabel::start()
{
    CheckNullimg();
    mTimer.start();
}

void AnimatedLabel::stop()
{
    mTimer.stop();
}
