#ifndef ANIMATED_LABEL_H
#define ANIMATED_LABEL_H

#include <QLabel>
#include <QTimer>


namespace Ui {
class AnimatedLabel;
}

class  AnimatedLabel
        : public QLabel
{
    Q_OBJECT

    Q_PROPERTY(int timeout READ timeout WRITE setTimeOut)

public:
    AnimatedLabel(QWidget* parent = 0);
    void start();
    void stop();

    void setImage(const QString& image);
    void setSpriteCount(int sprites);
    int timeout(){return mTimer.interval();}
    void setTimeOut(int to){mTimer.setInterval(to);}

private slots:
    void changeImage();

private:
    QImage          mImage;
    QList<QPixmap>  pixmaps;
    int             mCurrentPixmap;
    QTimer          mTimer;
    int             mSpriteCount;

    void            Construct();
    void            CheckNullimg();

};


#endif
