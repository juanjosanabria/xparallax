#include "ProgressDialog.h"
#include "ui_progressdialog.h"
#include "../mainwindow.h"
#include <QtConcurrentRun>


ProgressDialog::ProgressDialog(QWidget *parent,const QString& title) :
    QDialog(parent),
    ui(new Ui::ProgressDialog)
{
    ui->setupUi(this);
    setFixedSize(width(),height());
    setWindowFlags( ( (windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowCloseButtonHint & ~Qt::WindowContextHelpButtonHint));

    mCurrentWorker = NULL;
    mSize = 0;
    mPosition = 0;
    mStarted = false;
    mEnded = false;
    mMarquee = false;
    mRunFunc = NULL;
    mCloseWhenFinish = true;
    mCancelled = false;
    mTimer.setInterval(500); mTimer.stop();
    ui->labelWorking->setVisible(false);

    ui->labelTitle->setText(title);
    ui->labelSub1->setText("");
    ui->labelSub2->setText("");
    ui->labelTime->setText("--");

   // connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(Cancel_Slot()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(Cancel_Slot()));
    connect(&mTimer, SIGNAL(timeout()), this, SLOT(Timer_Slot()));

    MainWindow::DPIAware(this, true);
}

ProgressDialog::~ProgressDialog()
{
    delete ui;
}



void ProgressDialog::Connect(BackgroundWorker* wk)
{
    connect(wk, SIGNAL(Setup_Signal(BackgroundWorker*,int)), this, SLOT(Setup_Slot(BackgroundWorker*,int)));
    connect(wk, SIGNAL(Start_Signal(BackgroundWorker*)), this, SLOT(Start_Slot(BackgroundWorker*)));
    connect(wk, SIGNAL(End_Signal(BackgroundWorker*)), this, SLOT(End_Slot(BackgroundWorker*)));
    connect(wk, SIGNAL(Error_Signal(BackgroundWorker*,QString)), this, SLOT(Error_Slot(BackgroundWorker*,QString)));
    connect(wk, SIGNAL(Progress_Signal(BackgroundWorker*,int)), this, SLOT(Progress_Slot(BackgroundWorker*,int)));
    connect(wk, SIGNAL(Msg_Signal(BackgroundWorker*,QString)), this, SLOT(Msg_Slot(BackgroundWorker*,QString)));
    connect(wk, SIGNAL(TitleChange_Signal(BackgroundWorker*,QString)), this, SLOT(TitleChange_Slot(BackgroundWorker*,QString)));
    connect(wk, SIGNAL(MainTitleChange_Signal(BackgroundWorker*,QString)), this, SLOT(MainTitleChange_Slot(BackgroundWorker*,QString)) );
}

void ProgressDialog::ConnectThis()
{
    connect(this, &ProgressDialog::Setup_Signal, this, [=](int sz){ Setup_Slot(NULL, sz); } );
    connect(this, &ProgressDialog::Start_Signal, this, [=]{ Start_Slot(NULL); });
    connect(this, &ProgressDialog::End_Signal, this, [=]{ End_Slot(NULL);});
    connect(this, &ProgressDialog::Error_Signal, this, [=](QString msg){ Error_Slot(NULL, msg);});
    connect(this, &ProgressDialog::Progress_Signal, this, [=](int prog){Progress_Slot(NULL, prog);});
    connect(this, &ProgressDialog::Msg_Signal, this, [=](QString msg){ Msg_Slot(NULL, msg);});
    connect(this, &ProgressDialog::TitleChange_Signal, this, [=](QString msg){ TitleChange_Slot(NULL, msg);});
    connect(this, &ProgressDialog::MainTitleChange_Signal, this, [=](QString msg){ MainTitleChange_Slot(NULL, msg);});
}

void ProgressDialog::keyPressEvent(QKeyEvent* ke)
{
    if (ke->key() == Qt::Key_Escape) return;
    else ke->accept();
}

void ProgressDialog::SetMarquee(bool marquee)
{
    if (marquee)
    {
        ui->progressBar->setMinimum(0);
        ui->progressBar->setMaximum(0);
        ui->progressBar->setValue(-1);
    }
    else
    {
        ui->progressBar->setMinimum(0);
        ui->progressBar->setMaximum(100);
        ui->progressBar->setValue(0);
    }
    mMarquee = marquee;
}

int ProgressDialog::ExecInBackground(int(*func)(void *), void *user_data)
{
    mRunFunc = func;
    QFuture<int> future = QtConcurrent::run(this, &ProgressDialog::StartInBackground, user_data);
    this->exec();
    future.waitForFinished();
    return future.result();
}

void ProgressDialog::SetIcon(const QString& icon)
{
    ui->labelIcon->setPixmap(QPixmap(icon));
}

int ProgressDialog::StartInBackground(void *user_params)
{
   return mRunFunc(user_params);
}

void ProgressDialog::Start_Slot(BackgroundWorker* wk)
{
    if (mCurrentWorker != wk && ui->labelSub1->text() != wk->name())
    {
        mCurrentWorker = wk;
        ui->labelSub1->setText(wk ? wk->name() : "");
    }
    if (mStarted) return;
    mStarted = true;
    mTimer.start(); mLastRemain = -1;
    ui->labelWorking->setVisible(true);
    ui->labelWorking->start();
    mStartTime = QTime();
    mStartTime.start();
}

void ProgressDialog::End_Slot(BackgroundWorker *wk)
{
    if (mEnded || mCancelled)
    {
        if (mCloseWhenFinish) close();
        return;
    }
    if (mCurrentWorker != wk)
        mCurrentWorker = wk;
    //Comprobamos si tidos los workers han terminados
    for (int i=0; i<mWorkers.count(); i++)
        if (mWorkers[i] && !mWorkers[i]->ended()) return;

    mTimer.stop();
    //Condición de finalización
    mEnded = true;
    //Si hay que cerrar el diálogo a por ellos
    if (mCloseWhenFinish) close();
}

void ProgressDialog::Error_Slot(BackgroundWorker *, QString )
{

}

void ProgressDialog::Setup_Slot(BackgroundWorker* wk, int size)
{
    mWorkers.append(wk);
    mSize += size;
    SetMarquee(!mSize);
}

void ProgressDialog::Msg_Slot(BackgroundWorker* wk, QString msg)
{
    QFontMetrics metrics(ui->labelSub2->font());
    QString elidedMsg = metrics.elidedText(msg, Qt::ElideMiddle, ui->labelSub2->width());
    metrics = QFontMetrics(ui->labelSub1->font());
    QString elidedText = metrics.elidedText(wk ? wk->name() : "", Qt::ElideRight, ui->labelSub1->width());

    if (mCurrentWorker != wk && ui->labelSub1->text() != elidedText)
    {
        mCurrentWorker = wk;
        ui->labelSub1->setText(elidedText);
    }
    if (ui->labelSub2->text() != elidedMsg)
        ui->labelSub2->setText(elidedMsg);

}

void ProgressDialog::TitleChange_Slot(BackgroundWorker *wk, QString msg)
{
    if (mCurrentWorker != wk)
    {
        mCurrentWorker = wk;
        ui->labelSub1->setText(msg);
    }
    else if (ui->labelSub1->text() != msg)
        ui->labelSub1->setText(msg);
}

void ProgressDialog::MainTitleChange_Slot(BackgroundWorker *wk, QString msg)
{
    if (mCurrentWorker != wk)
    {
        mCurrentWorker = wk;
        ui->labelTitle->setText(msg);
    }
    else if (ui->labelSub1->text() != msg)
         ui->labelTitle->setText(msg);
}

void ProgressDialog::Progress_Slot(BackgroundWorker* wk,int psb)
{
    int last_percent =  mPosition * 100 / mSize;
    if (!wk)
    {
        mPosition = psb;
    }
    else
    {
        if (mCurrentWorker != wk && ui->labelSub1->text() != (wk ? wk->name() : ""))
        {
            mCurrentWorker = wk;
            ui->labelSub1->setText(wk ? wk->name() : "");
        }
        if (!mMarquee)
        {
            //Calculamos la posición actual
            int pos = 0;
            for (int i=0; i<mWorkers.count(); i++)
                pos += mWorkers[i]->position();
            mPosition = pos;
        }
    }
    //Actualizamos la barra de progreso
    int percent = mPosition * 100 / mSize;
    if (last_percent != percent)
        ui->progressBar->setValue(percent);
}


void ProgressDialog::StartModal_Slot()
{

}


void ProgressDialog::Cancel_Slot()
{
    mCancelled = true;
    for (int i=0; i<mWorkers.count(); i++)
       if (mWorkers[i]) mWorkers[i]->cancel();
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setEnabled(false);
    ui->labelTime->setText("Cancelling...");
}

void ProgressDialog::Timer_Slot()
{
    if (!mStarted || mEnded || mMarquee || !mSize) return;
    int percent = mPosition * 100 / mSize;
    if (!percent) return;
    int ell = mStartTime.elapsed();
    if (ell == 0) return;
    double pps = percent / (ell/1000.0);
    double cur_remain = mLastRemain < 0 ? (100-percent) : (100-percent)  * 0.2 + mLastRemain * 0.8;
    int remain = (int)ceil(cur_remain/pps);
    char buffer[64];
    snprintf(buffer, 64, "%02dm %02ds", remain/60, remain%60);
    if (mLastRemain > 0) ui->labelTime->setText(buffer);
    mLastRemain = cur_remain;
}






