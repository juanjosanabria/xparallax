#include "linechart.h"
#include "core.h"
#include "ap.h"
#include "interpolation.h"
#include <QPainter>
#include <QPaintEvent>

LineChartParameters::LineChartParameters()
{
    SetDefaults();
}

LineChartParameters::~LineChartParameters()
{

}

void LineChartParameters::SetDefaults()
{
    show_inter_poly = true;
    poly_resolution = 100;
    poly_degree = 6;
}

void LineChartParameters::Save(QSettings* /*settings*/, const QString& /*section*/)
{

}

void LineChartParameters::Load(QSettings* /*settings*/, const QString& /*section*/)
{

}

LineChart::LineChart(QWidget *parent) :
    QWidget(parent)
{
    mLeftMargin = 25;
    mTopmargin = 10;;
    mRightMargin = 10;
    mBottomMargin = 10;

    mBackgroundBrush = QBrush(QColor(0xff, 0xff, 0xff));
    mAxisColor = QBrush(QColor::fromRgb(0,0,0));
    mAxisPen = QPen(mAxisColor, 1);
    mDataBrush = QBrush(QColor::fromRgb(0,0,128));
    mDataPen = QPen(mDataBrush,1);
    mDataPen.setWidth(1);

    mInterpolantPen = QPen(QColor::fromRgb(128,0,0), 1);

    mFontAxis = QFont("\"Trebuchet MS\", Helvitica, \"Sans Serif\"",7);
}

LineChart::~LineChart()
{

}

void LineChart::setXData(const QVector<double>& x_data)
{
    double first_val = x_data.at(0);
    mXData.clear();
    for (int i=0; i<x_data.count(); i++)
        mXData.append(x_data.at(i) - first_val);
    mMaxX = x_data.at(x_data.count()-1) - first_val;
}

void LineChart::setYData(const QVector<double>& y_data)
{
    mYData = y_data;
    mMinData = FLOAT_MAX;
    mMaxData = FLOAT_MIN;

    for (int i=0; i<mYData.count(); i++)
    {
        if (mYData.at(i) > mMaxData) mMaxData = mYData.at(i);
        if (mYData.at(i) < mMinData) mMinData = mYData.at(i);
    }
    Interpolate_Poly();
    this->repaint();
}


void LineChart::Interpolate_Poly()
{
    //Hacemos el fit polinomial a ver que sale de esto

    //http://www.alglib.net/translator/man/manual.cpp.html#sub_polynomialfit
    //http://www.alglib.net/translator/man/manual.cpp.html#example_lsfit_d_pol
    alglib::real_1d_array array_x;  //= "[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]";
    alglib::real_1d_array array_y; //= "[0.00,0.05,0.26,0.32,0.33,0.43,0.60,0.60,0.77,0.98,1.02]";

    array_x.allocate_own(mYData.count(), alglib_impl::DT_REAL);
    array_y.allocate_own(mYData.count(), alglib_impl::DT_REAL);

    for (int i=0; i<mYData.count(); i++)
    {
        array_x[i] = mXData.at(i);
        array_y[i] = mYData.at(i);
    }

    alglib::ae_int_t info;
    alglib::barycentricinterpolant coeficients;
    alglib::polynomialfitreport report;

    alglib::polynomialfit(array_x, array_y, mParameters.poly_degree + 1, info, coeficients,  report);

    double fit_step = mMaxX / (double)mParameters.poly_resolution;
    //Hacemos el fit
    mInterpolationX.clear(); mInterpolationY.clear();
    for (double x = 0; x<=mMaxX; x+=fit_step)
    {
        double v = alglib::barycentriccalc(coeficients, x);
        mInterpolationX.append(x);
        mInterpolationY.append(v);
    }
    mRMS = report.rmserror;
}

void LineChart::paintEvent(QPaintEvent * event)
{
    QPainter painter;
    painter.setViewport(this->childrenRect());
    painter.begin(this);
    painter.fillRect(event->rect(), mBackgroundBrush);
    if (!mYData.count()) return;

    double width = this->width() - (mLeftMargin+mRightMargin);
    double height = this->height() - (mTopmargin+mBottomMargin) ;
    //Dibujo los ejes
    painter.drawLine(mLeftMargin,this->height()-mBottomMargin,mLeftMargin,mTopmargin); //Eje y
    painter.drawLine(mLeftMargin,this->height()-mBottomMargin,this->width()-mRightMargin,this->height()-mBottomMargin); //Eje x
    painter.setFont(mFontAxis);
    QFontMetrics fm = painter.fontMetrics();

    for (double y=(mMaxData-mMinData)/5.0;y<mMaxData;y+=(mMaxData-mMinData)/5.0)
    {
        double y_pos = RANGECHANGE(y, mMinData, mMaxData, height, 0) + mTopmargin;
        QString text = QString("").sprintf("%0.2f-",y);
        painter.drawText(mLeftMargin - fm.width(text),y_pos,text);
    }


    //Dibujamos la serie de datos
    painter.setPen(mDataPen);
    QPointF last_point;
    QPointF cur_point;
    for (int i=0; i<mXData.count(); i++)
    {
        double x_pos = RANGECHANGE(mXData.at(i), 0, mMaxX, 0, width) + mLeftMargin;  // width / mYData.count() * i + mLeftMargin;
        double y_pos = RANGECHANGE(mYData.at(i), mMinData, mMaxData, height, 0) + mTopmargin;
        cur_point.setX(x_pos);
        cur_point.setY(y_pos);
        if (i > 0) painter.drawLine(last_point, cur_point);
        painter.drawEllipse(cur_point,3,3);
        last_point = cur_point;
    }

    if (mParameters.show_inter_poly)
    {
        painter.setPen(mInterpolantPen);
        for (int i = 0; i<mInterpolationX.count(); i++)
        {
             double x_pos = RANGECHANGE(mInterpolationX.at(i), 0, mMaxX, 0, width) + mLeftMargin;
             double y_pos = RANGECHANGE(mInterpolationY.at(i), mMinData, mMaxData, height, 0) + mTopmargin;
             cur_point.setX(x_pos);
             cur_point.setY(y_pos);
             if (i > 0)painter.drawLine(last_point, cur_point);
             painter.drawEllipse(cur_point,3,3);
             last_point = cur_point;
        }
    }
    QString rms = QString("RMS: %1").arg(mRMS);
    painter.drawText(mLeftMargin +6, mTopmargin +3, rms);
}














