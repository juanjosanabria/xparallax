#ifndef IMAGECATALOGDIALOG_H
#define IMAGECATALOGDIALOG_H

#include <QDialog>
#include <QCheckBox>
#include <QTreeWidget>
#include <QTabWidget>
#include <QRadioButton>
#include "core.h"
#include "detection.h"
#include "catalog.h"
#include "mdifitimage.h"

namespace Ui {
class ImageCatalogDialog;
}

class ImageCatalogDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ImageCatalogDialog(QWidget *parent = 0);
    ~ImageCatalogDialog();

protected:
    void accept();

private:
    Ui::ImageCatalogDialog *ui;
    DetectionParameters     mDetectP;
    WorkFileList            mInputFiles;
    CatalogParameters       mCatalogP;
    QSettings*              mSettings;
    CatalogComputer*        mComputer;

    bool CheckData();
    void LoadDataToDialog();
    static int              Background_CatalogCompute(void *data);      //Hilo de cálculo
    static void             addDetectionToImage(MdiFitImage* img, StarDetector* detector);
    static  QString         FN_GET_ITEM_IMAGE(FitHeaderList* fh,const QString& filename);
    static const char*      IMAGE_CATALOG_INI;

private slots:
    /**
     * @brief CheckBoxChange_Slot Disparado cuando se cambie el estado de checked de algún checkbox. Actualiza el estado de enabled
     * de todos los elementos asociados.
     * @param check Checkbox que ha cambiado de estado. Llamar con NULL si se desea actualizar todos los elementos.
     */
    void CheckBoxChange_Slot(QCheckBox* check);

    //Edición en las coordenadas de las estrellas de salida
    void SelectSourceStar_Slot();
    void SelectCatField_Slot();
    void SelectFluxField_Slot();
    void FieldUp_Slot(QTreeWidget *tv);
    void FieldDown_Slot(QTreeWidget *tv);
    void AddSourceStar_Slot(bool add = true);
    void RemoveSourceStar_Slot();
    void ClearSourceStar_Slot();
    void ResetCatalog_Slot();
    void ResetFluxTable_Slot();
    void RaioInputFilesChanged_Slot(QRadioButton* btn);

};

#endif // IMAGECATALOGDIALOG_H
