#ifndef MDIFITBLLINK_H
#define MDIFITBLLINK_H

#include <QObject>
#include <QTimer>
#include "plateconstants.h"
#include "mpc.h"
#include "util.h"
#include "../ui_helpers/ProgressDialog.h"
#include "../../mdifitimage.h"
#include "imgtools/ximgtoolgif.h"


class MdiFitBlink : public MdiFitImage
{
    Q_OBJECT

public:
    explicit MdiFitBlink(QWidget *parent, QList<FitImage*>* images);
    ~MdiFitBlink();
    inline QVector<FitImage*>* images(){return &mImages;}
    inline int currentIdx(){return mCurrentIdx;}
    void MoveTo(int idx);

private:
    //Items pertenecientes a menús
    //********************************************************************************************************************
    void initExtensionInterface();
    void destroyExtnsionInterface();


    //Items de trabajo
    //********************************************************************************************************************
    QVector<FitImage*>          mImages;                //Lista de imágenes abiertas para hacer blink
    QVector<Transform2D_LS>     mTrans;                 //Transformación. Plate constants de imagen 0 a las otras
    XOverlayLayer*              mUserLayer;             //Capa de dibujo del usuario
    XOverlayLayer*              mInfoLayer;             //Capa informativa
    XOverlayLabel*              mImageNameLabel;        //Capa donde se muestra el nombre de la imagen
    QTransform                  mTransform;
    int                         mCurrentIdx;            //Imagen actual mostrándose
    QTimer*                     mPlayTimer;
    ProgressDialog*             mProgressDialog;        //Diálogo de progreso general
    MpcUtils                    mMpcUtils;

    //Items pertenecientes a menús
    //********************************************************************************************************************
    QAction*                    mFirstAction;
    QAction*                    mPrevAction;
    QAction*                    mNextAction;
    QAction*                    mLastAction;
    QAction*                    mPlayAction;

    QAction*                    mMpcKnownAction;
    QAction*                    mMpcEphemerisAction;
    QAction*                    mMpcReportsAction;

protected:
    virtual void paintImage(QPaintEvent* event, QPainter* painter);
    virtual QString getWindowTitle();
    virtual void updateMesh(int x1=MININT, int y1=0, int x2=0, int y2=0, bool border=false);
    virtual void setFitImage(FitImage *fi);

private:
    void computeTransform(PlateConstants* pa, PlateConstants*pb, Transform2D_LS* td);

public slots:
    void First_Slot();          //Ir a la primera imagen
    void Last_Slot();           //Ir a la última imagen
    void Next_Slot();           //Ir a la siguiente imagen
    void Prev_Slot();           //Ir a la imagen anterior
    void Play_Slot();           //Reproducir/Parar
    void Timer_Slot();          //Timer de reproducción

};

#endif // MDIFITBLLINK_H
