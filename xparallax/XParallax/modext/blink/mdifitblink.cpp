#include "mdifitblink.h"
#include "core.h"
#include "mpc.h"
#include "../ui_helpers/ProgressDialog.h"
#include "../../mainwindow.h"
#include "utilities/gif.h"


#define   OBJ_USER_LAYER      3
#define   OBJ_INFO_LAYER      4
#define   OBJ_MOVING_OBJ      5
#define   OBJ_KNOWN_OBJ       6

QVector<Transform2D_ER> transforms;

MdiFitBlink::MdiFitBlink(QWidget *parent, QList<FitImage*>* images) :
    MdiFitImage(parent, images->at(0))
{
    initExtensionInterface();
    mCurrentIdx = 0;
    mUserLayer = new XOverlayLayer("User drawing");
    addXLayer(mUserLayer, 1);
    mInfoLayer = new XOverlayLayer("Image information");
    mInfoLayer->setFn(ImageXYToScreenXY_ST, ScreenXYToImageXY_ST, map_ST, imap_ST);
    mInfoLayer->setWindow(this);
    addXLayer(mInfoLayer, 2);
    mInfoLayer->addStyle(OBJ_INFO_LAYER);
    mImageNameLabel = new XOverlayLabel(mInfoLayer, OBJ_INFO_LAYER);
    mImageNameLabel->setPos(20 * XGlobal::SCREEN_DPI, (40-SIZE_TOOLBAR_BUTTON) * XGlobal::SCREEN_DPI);
    mImageNameLabel->setLabel(QFileInfo(images->at(0)->filename()).baseName());
    mImageNameLabel->style()->setForeColor(Qt::yellow);
    mProgressDialog = NULL;

    //Asignar las imágenes
    for (int i=0; i<images->count(); i++)
    {
        mImages.append(images->at(i));
        mImages[i]->LookForPlate();
    }
    //Primera transformación es la identidad
    Transform2D_LS tls;
    tls.initialize(1, 0, 0, 0, 1, 0);
    mTrans.append(tls);

    for (int i=1; i<mImages.count(); i++)
    {
        //Calcular el desplazamiento para esta imagen
        computeTransform(mImages[0]->plate() , mImages[i]->plate(), &tls);
        mTrans.append(tls);

        INFO("\tTransform[%s] ROT: %9.5f; TRANSLATExy: %9.3f, %9.3f",
              PTQ(QFileInfo(images->at(i)->filename()).baseName()), mTrans[i].rot(), mTrans[i].c(), mTrans[i].f());
    }
    setFitImage(mImages[0]);
    CheckDirty();

    //Timer de play
    mPlayTimer = new QTimer(this);
    connect(mPlayTimer, SIGNAL(timeout()), this, SLOT(Timer_Slot()));
    mPlayTimer->setInterval(400);
    mPlayTimer->stop();
}

void MdiFitBlink::computeTransform(PlateConstants* pa, PlateConstants*pb, Transform2D_LS* td)
{
    double ra0, dec0, x1, y1;
    //Calculamos la transformación con 64 puntos de control
    double div = 8.0;
    double width_rct = image()->width() / div;
    double height_rct = image()->height() / div;

    td->reset();
    for (double y=height_rct; y<= height_rct*(div+0.01); y+= height_rct)
    {
        for (double x=width_rct; x<= width_rct*(div+0.01); x += width_rct)
        {
            pa->map(x, y, &ra0, &dec0);
            pb->imap(ra0, dec0, &x1, &y1);
            td->addCtrlPoint(x, y, x1, y1);
        }
    }
    td->Compute();
}

MdiFitBlink::~MdiFitBlink()
{
    //Si estamos viendo la secuencia la paramos
    if (mPlayTimer->isActive()) mPlayTimer->stop();
    //Cuidado, el destructir de MdiFitImage ya elimina la actual que tenga seleccionada
    for (int i=0; i<mImages.count(); i++)
        if (mImages[i] != fitimage())
             delete mImages[i];
    if (mProgressDialog) delete mProgressDialog;
    mProgressDialog = NULL;
}

void MdiFitBlink::initExtensionInterface()
{
    //Acción de "Guardar GIF"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_gif;
    xp_gif.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_gif.act = new QAction(QIcon(TOOL_GIF_ICON), TOOL_GIF, this);
    xp_gif.act->setObjectName(TOOL_GIF);
    xp_gif.act->setStatusTip(TOOL_GIF_DESC);
    xp_gif.act->setEnabled(true);
    xp_gif.men = TOOLBAR_BLINK;       //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_gif);
    connect(xp_gif.act, &QAction::triggered, this, [=]{SelectTool_Slot(TOOL_GIF);});

    //Acción de "Primero"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_first_fit;
    xp_first_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_first_fit.act = mFirstAction = new QAction(QIcon(ACTION_FIRST_ICON), ACTION_FIRST, this);
    xp_first_fit.act->setObjectName(ACTION_FIRST);
    mFirstAction->setShortcut(QKeySequence::MoveToStartOfDocument);
    mFirstAction->setStatusTip(ACTION_FIRST_DESC);
    mFirstAction->setEnabled(false);        //Comienza siempre habilitada
    xp_first_fit.men = TOOLBAR_BLINK;       //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_first_fit);
    connect(xp_first_fit.act, SIGNAL(triggered()), this, SLOT(First_Slot()));

    //Acción de "Anterior"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_prev_fit;
    xp_prev_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_prev_fit.act = mPrevAction = new QAction(QIcon(ACTION_PREV_ICON), ACTION_PREV, this);
    xp_prev_fit.act->setObjectName(ACTION_PREV);
    mPrevAction->setShortcut(QKeySequence::MoveToPreviousPage);
    mPrevAction->setStatusTip(ACTION_PREV_DESC);
    mPrevAction->setEnabled(false);        //Comienza siempre habilitada
    xp_prev_fit.men = TOOLBAR_BLINK;       //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_prev_fit);
    connect(xp_prev_fit.act, SIGNAL(triggered()), this, SLOT(Prev_Slot()));

    //Acción de "Siguiente"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_next_fit;
    xp_next_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_next_fit.act = mNextAction = new QAction(QIcon(ACTION_NEXT_ICON), ACTION_NEXT, this);
    xp_next_fit.act->setObjectName(ACTION_NEXT);
    mNextAction->setShortcut(QKeySequence::MoveToNextPage);
    mNextAction->setStatusTip(ACTION_NEXT_DESC);
    mNextAction->setEnabled(true);        //Comienza siempre habilitada
    xp_next_fit.men = TOOLBAR_BLINK;      //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_next_fit);
    connect(xp_next_fit.act, SIGNAL(triggered()), this, SLOT(Next_Slot()));

    //Acción de "Último"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_last_fit;
    xp_last_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_last_fit.act = mLastAction = new QAction(QIcon(ACTION_LAST_ICON), ACTION_LAST, this);
    xp_last_fit.act->setObjectName(ACTION_LAST);
    mLastAction->setShortcut(QKeySequence::MoveToEndOfDocument);
    mLastAction->setStatusTip(ACTION_LAST_DESC);
    mLastAction->setEnabled(true);        //Comienza siempre habilitada
    xp_last_fit.men = TOOLBAR_BLINK;       //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_last_fit);
    connect(xp_last_fit.act, SIGNAL(triggered()), this, SLOT(Last_Slot()));

    //Acción de "Separador"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_sep_fit;
    xp_sep_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_sep_fit.act = new QAction(this);
    xp_sep_fit.act->setSeparator(true);
    xp_sep_fit.act->setObjectName("sep_01");
    xp_sep_fit.men = TOOLBAR_BLINK;       //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_sep_fit);

    //Acción de "Play"
    //-------------------------------------------------------------------------
    XPEXT_ACTION_ITEMS xp_play_fit;
    xp_play_fit.beh = ITEM_BEHAVIOUR_ENABLE_DISABLE;
    xp_play_fit.act = mPlayAction = new QAction(QIcon(ACTION_PLAY_ICON), ACTION_PLAY, this);
    xp_play_fit.act->setObjectName(ACTION_PLAY);
    mPlayAction->setStatusTip(ACTION_PLAY);
    mPlayAction->setEnabled(true);         //Comienza siempre habilitada
    xp_play_fit.men = TOOLBAR_BLINK;       //La meto también en la barra de herramientas general
    toolbarItems()->append(xp_play_fit);
    connect(xp_play_fit.act, SIGNAL(triggered()), this, SLOT(Play_Slot()));
}


void MdiFitBlink::destroyExtnsionInterface()
{

}


void MdiFitBlink::paintImage(QPaintEvent* /*event*/, QPainter* painter)
{
    QPointF pa;
    ImageXYToScreenXY(0, 0, pa);
    mTransform.reset();

    mTransform.translate(pa.x() , pa.y() );
    mTransform.rotate(-mTrans[mCurrentIdx].rot());
    mTransform.translate((-mTrans[mCurrentIdx].c()-0.5) * scale(),(-mTrans[mCurrentIdx].f()-0.5) * scale());
    mTransform.scale(scale(), scale());

    painter->setTransform(mTransform);
    painter->drawImage(0,  0, *image(), 0, 0, image()->width(), image()->height());
}


QString MdiFitBlink::getWindowTitle()
{
    return QString("**BLINK** %1/%2 %3")
            .arg(mCurrentIdx+1)
            .arg(mImages.count())
            .arg(QFileInfo(mImages[mCurrentIdx]->filename()).baseName());
}

void MdiFitBlink::updateMesh(int, int, int, int, bool)
{
    //Calculamos un recuadro que envuelva a todos los frames
    int min_x = 0, min_y = 0, max_x = fitimage()->width()-1, max_y = fitimage()->height()-1;
    for (int i=1; i<mTrans.count(); i++)
    {
        double x1, y1, x2, y2;
        mTrans.data()[i].imap(0, 0, &x1, &y1);
        mTrans.data()[i].imap(fitimage()->width()-1, fitimage()->height()-1, &x2, &y2);
        min_x = MINVAL(min_x, MINVAL(x1, x2));
        max_x = MAXVAL(max_x, MAXVAL(x1, x2));
        min_y = MINVAL(min_y, MINVAL(y1, y2));
        max_y = MAXVAL(max_y, MAXVAL(y1, y2));
    }
    MdiFitImage::updateMesh(min_x, min_y, max_x, max_y, true);
}

void MdiFitBlink::setFitImage(FitImage* fi)
{
    MdiFitImage::setFitImage(fi);
    setPlate(mImages[0]->plate());   //Muy importante tener la referncia siempre de la primera
    MPC_KnownObjLayerUpdate();
}

void MdiFitBlink::Next_Slot()
{
    if (mCurrentIdx == mImages.count() - 1) return;
    mCurrentIdx++;
    //Habilito, deshabilito acciones
    mPrevAction->setEnabled(true);
    mFirstAction->setEnabled(true);
    if (mCurrentIdx == mImages.count() - 1)
    {
        mNextAction->setEnabled(false);
        mLastAction->setEnabled(false);
    }
    setFitImage(mImages[mCurrentIdx]);
    mImageNameLabel->setLabel(QFileInfo(mImages[mCurrentIdx]->filename()).baseName());
    CheckDirty();

}

void MdiFitBlink::Prev_Slot()
{
    if (!mCurrentIdx) return;
    mCurrentIdx--;
    mNextAction->setEnabled(true);
    mLastAction->setEnabled(true);
    if (!mCurrentIdx)
    {
        mPrevAction->setEnabled(false);
        mFirstAction->setEnabled(false);
    }
    setFitImage(mImages[mCurrentIdx]);
    mImageNameLabel->setLabel(QFileInfo(mImages[mCurrentIdx]->filename()).baseName());
    CheckDirty();
}

void MdiFitBlink::MoveTo(int idx)
{
    if (idx == 0) First_Slot();
    else if (idx == mImages.count() - 1) Last_Slot();
    else if (idx > 0 && idx < mImages.count() - 1)
    {
        mCurrentIdx = idx;
        mNextAction->setEnabled(true);
        mLastAction->setEnabled(true);
        if (!mCurrentIdx)
        {
            mPrevAction->setEnabled(false);
            mFirstAction->setEnabled(false);
        }
        setFitImage(mImages[mCurrentIdx]);
        mImageNameLabel->setLabel(QFileInfo(mImages[mCurrentIdx]->filename()).baseName());
        CheckDirty();
    }
}

void MdiFitBlink::First_Slot()
{
   mCurrentIdx = 0;
   mPrevAction->setEnabled(false);
   mNextAction->setEnabled(true);
   mFirstAction->setEnabled(false);
   mLastAction->setEnabled(true);
   setFitImage(mImages[mCurrentIdx]);
   mImageNameLabel->setLabel(QFileInfo(mImages[mCurrentIdx]->filename()).baseName());
   CheckDirty();
}

void MdiFitBlink::Last_Slot()
{
    mCurrentIdx = mImages.count() - 1;
    mNextAction->setEnabled(false);
    mPrevAction->setEnabled(true);
    mLastAction->setEnabled(false);
    mFirstAction->setEnabled(true);
    setFitImage(mImages[mCurrentIdx]);
    mImageNameLabel->setLabel(QFileInfo(mImages[mCurrentIdx]->filename()).baseName());
    CheckDirty();
}


void MdiFitBlink::Play_Slot()
{
    if (!mPlayTimer->isActive())
    {
        mPlayTimer->start();
        mPlayAction->setIcon(QIcon(ACTION_PAUSE_ICON));
        mPlayAction->setText("Pauses");
    }
    else
    {
        mPlayTimer->stop();
        mPlayAction->setIcon(QIcon(ACTION_PLAY_ICON));
        mPlayAction->setText(ACTION_PLAY);
    }
}

void MdiFitBlink::Timer_Slot()
{
    //Si estoy en la última imagen paso a la primera
    mCurrentIdx == mImages.count()-1 ? First_Slot() : Next_Slot();
}






