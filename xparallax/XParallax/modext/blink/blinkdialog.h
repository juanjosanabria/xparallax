#ifndef BLINKDIALOG_H
#define BLINKDIALOG_H

#include <QDialog>
#include <calibration.h>

namespace Ui {
class BlinkDialog;
}

class BlinkDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BlinkDialog(QWidget *parent = 0);
    ~BlinkDialog();
    WorkFileList    inputFiles(){return mInputFiles;}


protected:
    void accept();
    static QString  FN_GET_ITEM_IMAGE(FitHeaderList* fh,const QString& /*filename*/);

private:
    static const char* BLINK_DIALOG_INI;
    Ui::BlinkDialog *ui;
    WorkFileList    mInputFiles;

};

#endif // BLINKDIALOG_H
