#include "blinkdialog.h"
#include "ui_blinkdialog.h"
#include "ui_selectfileslayout.h"
#include "mdifitblink.h"
#include "xglobal.h"
#include "../../mainwindow.h"
#include <QMessageBox>

const char* BlinkDialog::BLINK_DIALOG_INI = "blink_dialog.ini";

BlinkDialog::BlinkDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BlinkDialog)
{
    ui->setupUi(this);
    //No queremos mostrarlo en la barra de tareas ni tampoco que se pueda redimensionar
    setWindowFlags( windowFlags() & ~Qt::FramelessWindowHint );
    setFixedSize(width(), height());

    ui->selectFilesWidget->setMinImages(2);
    ui->selectFilesWidget->FN_GET_ITEM_IMAGE = FN_GET_ITEM_IMAGE;
    ui->selectFilesWidget->SetOptions((SelectFilesLayout::SelectFilesOptions)
                                      (
                                          (
                                              ui->selectFilesWidget->options()
                                              & (~ SelectFilesLayout::SFO_OUTPUT_FOLDER_MANDATORY)
                                              & (~ SelectFilesLayout::SFO_OUTPUT_ALLOWED)           //Nada de salida
                                              & (~ SelectFilesLayout::SFO_ALLOW_CURRENT_FILE)       //No permitir archivo actual
                                          )
                                              | SelectFilesLayout::SFO_REQUEST_SAME_SIZE
                                              | SelectFilesLayout::SFO_REQUEST_ASTROMETRIC
                                        )
                                      );

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    //Parámetros por defecto
    QSettings sett(QString("%1/%2").arg( XGlobal::CONF_PATH).arg(BLINK_DIALOG_INI),QSettings::IniFormat);
    mInputFiles.Load(&sett, "FILES");
    ui->selectFilesWidget->SetLastDirs(mInputFiles.lastSearchPath(), mInputFiles.outputDir());
    MainWindow::DPIAware(this, true);
}

BlinkDialog::~BlinkDialog()
{
    QSettings sett(QString("%1/%2").arg( XGlobal::CONF_PATH).arg(BLINK_DIALOG_INI),QSettings::IniFormat);
    mInputFiles.Save(&sett, "FILES");
    delete ui;
}

void BlinkDialog::accept()
{
    if (!ui->selectFilesWidget->CheckData(&mInputFiles)) return;
    bool error = false;
    //Creamos la subventana de blinkinig
    QList<FitImage*> images;


    //Si son archivos de disco abrir todas las imágenes
    if (mInputFiles.filenames()->count() && !error)
    {
        for (int i=0; i<mInputFiles.filenames()->count(); i++)
        {
            FitImage* fi = FitImage::OpenFile(mInputFiles.filenames()->at(i));
            DINFO("Abriendo imagen: %s", PTQ(mInputFiles.filenames()->at(i)));
            if (fi->error()) error = true;
            images.append(fi);
        }
    }
    else if (mInputFiles.fitimages()->count() >= 2 && !error)
    {
        for (int i=0; i<mInputFiles.fitimages()->count(); i++)
            images.append( mInputFiles.fitimages()->at(i)->Clone() );
    }
    else (error = true);

    if (error)
    {
        QMessageBox::warning(this, "Error", "Unable to show blink window.\n\n Some images were not loaded properly or not enough images in source.");
        for (int i=0; i<images.count(); i++) delete images[i];
        return;
    }

    MdiFitBlink* mdf = new MdiFitBlink(this, &images);
    QMdiSubWindow* mBlinkWindow = MainWindow::mainwin->AddMdiSubWindow(mdf,":/graph/images/blink_32.png");
    mdf->SetMdiWindow(mBlinkWindow);

    //Todo ha ido bien, podemos aceptar el diálogo
    this->setResult(QDialog::Accepted);
    this->hide();
}


QString  BlinkDialog::FN_GET_ITEM_IMAGE(FitHeaderList* fh,const QString& /*filename*/)
{
    PlateConstants *pc = fh->LookForPlateConstants();
    if (pc)
    {
        delete pc;
        return ":/graph/images/rule.png";
    }
    if ((pc = fh->LookForPlateRATAN()))
    {
        delete pc;
        return ":/graph/images/rule.png";
    }
    return "";
}

