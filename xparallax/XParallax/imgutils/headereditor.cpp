#include "headereditor.h"

#include <QFileInfo>
#include <QScriptEngine>
#include <QScriptClass>
#include "core.h"
#include "xglobal.h"
#include "util.h"

HeaderEditor*   HeaderEditor::CURRENT_EDITOR = NULL;
QMutex          HeaderEditor::SORT_MUTEX;
FitHeaderList*  HeaderEditor::CURRENT_HDRLIST = NULL;


HeaderEditor::HeaderEditor() :
    BackgroundWorker("Editing headers")
{

}

HeaderEditor::~HeaderEditor()
{

}

bool HeaderEditor::doWork(WorkFileList* wf, FitHeaderList* add_hdr, FitHeaderList *remove_hdr, FitHeaderList* sort_hdr)
{
    mWorkFileList = wf;
    mAddHdr = add_hdr;
    mRemoveHdr = remove_hdr;
    mSortHdr = sort_hdr;
    mResults.clear();

    if (wf->fitimages()->count()) OnSetup(wf->fitimages()->count());
    else OnSetup(wf->filenames()->count());

    OnStart();

    if (wf->fitimages()->count())
    {
        for (int i=0; i<wf->fitimages()->count() && !cancelled(); i++)
        {
            OnProgress(i);
            ComputeImage(wf->fitimages()->at(i));
            mResults.append("");
        }
    }
    else
    {
        for (int i=0; i<wf->filenames()->count() && !cancelled(); i++)
        {
            OnProgress(i);
            OnTitleChange(QString("Processing file: <b>%1</b>").arg(
                      QFileInfo(wf->filenames()->at(i)).baseName()));
            INFO("Editing headers: %s", PTQ(QFileInfo(wf->filenames()->at(i)).baseName()));
            QString ok = ComputeImage(wf->filenames()->at(i));
            if (ok.length()) WARN(ok);
            mResults.append(ok);
        }
    }
    INFO(QString("Batch header editing done"));
    OnEnd();
    return true;
}

QString HeaderEditor::ComputeImage(const QString& filename)
{
    bool ok = true;
    QString dest_filename = mWorkFileList->overwrite() ? filename :
                                                         QString("%1/%2").arg(mWorkFileList->outputDir(),
                                                                              QFileInfo(filename).fileName());
    QString err;
    OnMsg(QString("Opening file"));
    FitImage* fi = FitImage::OpenFile(filename, &err);
    if (!fi) return err;
    ComputeImage(fi);
    OnMsg(QString("Saving file"));
    ok = fi->SaveToFile(dest_filename);
    delete fi;
    if (!ok) return QString("Error saving file: '%s'").arg(dest_filename);
    return err;
}

void HeaderEditor::ComputeImage(FitImage* img)
{
    mCurFile = img;
    AddHeaders(img->headers());
    DeleteHeaders(img->headers());
    SortHeaders(img->headers());
    img->headers()->SetDirty(true);
    img->SetDirty(true);
}

int  HeaderEditor::CountOK()
{
    int total = 0;
    for (int i=0; i<mResults.count(); i++)
        if (!mResults.at(i).length()) total++;
    return total;
}

int  HeaderEditor::CountKO()
{
    return mResults.count() - CountOK();
}

QString HeaderEditor::tr(QString cad, FitHeader* src_hdr)
{
    //Literales
    if (cad.startsWith('"') && cad.endsWith('"'))
    {
        cad = cad.mid(1, cad.length()-2);
    }
    //Evaluador de expresiones
    else if (cad.startsWith("="))
    {
        INFO("Expression: %s", PTQ(cad));
        double val = ParseExpression(mCurFile->headers(), cad);
        //Fixme: A la hora de añadir cabeceras  hay que arreglar esto
        if ((src_hdr && src_hdr->name().contains("DATE")) || true)
            cad = Util::JD2DateTime(val/(3600*24)).toString("yyyy-MM-ddTHH:mm:ss");
        else
            cad = QString("").sprintf("%f", val);
        INFO("\t%s",PTQ(cad));
    }

    return cad;
}

void HeaderEditor::AddHeaders(FitHeaderList* fhl)
{
    FitHeader srchdr;
    QString name, value, comments;
    OnMsg(QString("Adding headers"));
    for (int i=0; i<mAddHdr->count(); i++)
    {
        FitHeader fh = mAddHdr->get(i);
        name = fh.name();
        srchdr = fhl->Contains(fh.name());
        if (!srchdr.isInvalid() && !srchdr.isEmpty())
        {
            fhl->RemoveHeader(fh.name());
            value = tr(fh.value(), &srchdr);
            comments = tr(fh.comment(), &srchdr);
        }
        else
        {
            value = tr(fh.value(), NULL);
            if (!value.length()) continue;
            comments = tr(fh.comment(), NULL);
        }
        fhl->AddHeader(FitHeader(name, value, comments));
    }
}

void HeaderEditor::DeleteHeaders(FitHeaderList* fhl)
{
    OnMsg(QString("Removing headers"));
    int idx;
    for (int i=0; i<mRemoveHdr->count(); i++)
    {
        FitHeader fh = mRemoveHdr->get(i);
        if ((idx = fhl->indexOf(fh.name())) != -1)
            fhl->RemoveHeader(fh.name());
    }
}

int  HeaderEditor::CompareHeaders(FitHeaderList* fhl, FitHeader* hdr1, FitHeader* hdr2)
{
    //Las cabeceras main son siempre las primeras
    bool mainhdr1 = FitHeaderList::IsMainHeader(hdr1->name());
    bool mainhdr2 = FitHeaderList::IsMainHeader(hdr2->name());
    if (mainhdr1 && !mainhdr2) return -1;
    if (mainhdr2 && !mainhdr1) return 1;
    //Miramos la lista de ordenación
    int idx1 = mSortHdr->indexOf(hdr1->name());
    int idx2 = mSortHdr->indexOf(hdr2->name());
    if (idx1 < idx2) return -1;
    if (idx1 > idx2) return 1;
    //Devolvemos el orden de la lista original
    idx1 = fhl->indexOf(hdr1->name());
    idx2 = fhl->indexOf(hdr2->name());
    if (idx1 < idx2) return -1;
    if (idx1 > idx2) return 1;
    return 0;
}

int HeaderEditor::LessThan(FitHeader a, FitHeader b)
{
    return CURRENT_EDITOR->CompareHeaders(CURRENT_HDRLIST, &a, &b) < 0;
}

void HeaderEditor::SortHeaders(FitHeaderList* fhl)
{
    if (!mSortHdr->count()) return;
    //Dado que usamos elementos estáticos para la función de comparación
    //Debemos usar un mutex para asegurar la no-concurrencia
    SORT_MUTEX.lock();
    CURRENT_HDRLIST = fhl;
    CURRENT_EDITOR = this;
    fhl->Sort(LessThan);
    CURRENT_HDRLIST = NULL;
    CURRENT_EDITOR = NULL;
    SORT_MUTEX.unlock();
    //El método de la burbuja anterior era demasiado lento cuando hay muchas cabeceras
}


double HeaderEditor::ParseExpression(FitHeaderList* fhl,QString exp)
{
   if (exp.startsWith("=")) exp = exp.mid(1);
   //Primer paso sustitución de todas las variables en la lista de cabeceras
   QRegularExpression regex("\\[(?<NAME>[A-Z\\-_]{2,8})\\]");
   QRegularExpressionMatch mc = regex.match(exp);
   while (mc.hasMatch())
   {
        QString dhdr = mc.captured("NAME");
        FitHeader fh = fhl->Contains(dhdr);
        QString val = fh.value();
        if (fh.isValid())
        {
            //Si es una fecha tomamos otro valor
            if (dhdr.toUpper().contains("DATE"))
            {
                QDateTime dtt = Util::ParseDate(fh.value());
                val = dtt == INVALID_DATETIME ? "0" : QString("").sprintf("%f",Util::JulianDate(dtt)*3600*24);
            }
        } else val = "0.0";
        exp = exp.replace(QString("[%1]").arg(dhdr), val);
        mc = regex.match(exp);
   }
   //Evaluamos la expresión
   QScriptEngine engine;
   double val = engine.evaluate(exp).toNumber();
   return val;
}



