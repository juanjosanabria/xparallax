#include "batchheaderdialog.h"
#include "ui_batchheaderdialog.h"
#include "../mainwindow.h"
#include "../inputdialog.h"
#include "../ui_helpers/ProgressDialog.h"
#include <QPushButton>

#define BATCH_HEADERS_SECTION   "BATCH_HDR_EDIT"
const char* BatchHeaderDialog::BATCHHEADER_DIALOG_INI = "batchheaders_dialog.ini";

BatchHeaderDialog::BatchHeaderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BatchHeaderDialog)
{
    ui->setupUi(this);
    QSettings sett(mIniFile, QSettings::IniFormat);
    ui->widgetSourceFiles->AddOption(SelectFilesLayout::SFO_OVERWRITE_CHECKED);
    mInputFiles.Load(&sett, BATCH_HEADERS_SECTION);
    mHeaderEditor = new HeaderEditor();
    mIniFile = XGlobal::GetSettingsPath(BATCHHEADER_DIALOG_INI);
    mAddHdrs = new FitHeaderList();
    mAddHdrs->ReadFromIni(mIniFile, SECTION_HEADERS_ADD);
    LoadHeadersToView(mAddHdrs, ui->treeWidgetAdd);
    mRemoveHdrs = new FitHeaderList();
    mRemoveHdrs->ReadFromIni(mIniFile, SECTION_HEADERS_DELETE);
    LoadHeadersToView(mRemoveHdrs, ui->treeWidgetDelete);
    mSortHdrs = new FitHeaderList();
    mSortHdrs->ReadFromIni(mIniFile, SECTION_HEADERS_SORT);
    LoadHeadersToView(mSortHdrs, ui->treeWidgetSort);

    //Botones de exportar/importar
    connect(ui->buttonSaveSettings, SIGNAL(clicked()), this, SLOT(SaveToIni_Slot()));
    connect(ui->buttonLoadSettings, SIGNAL(clicked()), this, SLOT(LoadFromIni_Slot()));

    //Lista de cabeceras a añadir / modificar
    connect(ui->buttonAddAdd,   &QToolButton::clicked, this, [=]{AddHeader_Slot(ui->treeWidgetAdd, mAddHdrs);});
    connect(ui->buttonAddClear, &QToolButton::clicked, this, [=]{ClearHeaders_Slot(ui->treeWidgetAdd, mAddHdrs);});
    connect(ui->buttonAddDelete,&QToolButton::clicked, this, [=]{DeleteHeader_Slot(ui->treeWidgetAdd, mAddHdrs);});
    connect(ui->buttonAddUp,    &QToolButton::clicked, this, [=]{Up_Slot(ui->treeWidgetAdd, mAddHdrs);});
    connect(ui->buttonAddDown,  &QToolButton::clicked, this, [=]{Down_Slot(ui->treeWidgetAdd, mAddHdrs);});
    connect(ui->treeWidgetAdd,  &QTreeWidget::doubleClicked, this, [=]{EditHeader_Slot(ui->treeWidgetAdd, mAddHdrs);});
    connect(ui->treeWidgetAdd,  &QTreeWidget::currentItemChanged, this, [=]{ItemIndexChanged_Slot(ui->treeWidgetAdd);});
    ItemIndexChanged_Slot(ui->treeWidgetAdd);

    //Lista de cabeceras a eliminar
    connect(ui->buttonDeleteAdd,   &QToolButton::clicked, this, [=]{AddHeader_Slot(ui->treeWidgetDelete, mRemoveHdrs);});
    connect(ui->buttonDeleteClear, &QToolButton::clicked, this, [=]{ClearHeaders_Slot(ui->treeWidgetDelete, mRemoveHdrs);});
    connect(ui->buttonDeleteDelete,&QToolButton::clicked, this, [=]{DeleteHeader_Slot(ui->treeWidgetDelete, mRemoveHdrs);});
    connect(ui->buttonDeleteUp,    &QToolButton::clicked, this, [=]{Up_Slot(ui->treeWidgetDelete, mRemoveHdrs);});
    connect(ui->buttonDeleteDown,  &QToolButton::clicked, this, [=]{Down_Slot(ui->treeWidgetDelete, mRemoveHdrs);});
    connect(ui->treeWidgetDelete,  &QTreeWidget::doubleClicked, this, [=]{EditHeader_Slot(ui->treeWidgetDelete, mRemoveHdrs);});
    connect(ui->treeWidgetDelete,  &QTreeWidget::currentItemChanged, this, [=]{ItemIndexChanged_Slot(ui->treeWidgetDelete);});
    ItemIndexChanged_Slot(ui->treeWidgetDelete);

    //Lista de cabeceras a ordenar
    connect(ui->buttonSortAdd,   &QToolButton::clicked, this, [=]{AddHeader_Slot(ui->treeWidgetSort, mSortHdrs);});
    connect(ui->buttonSortClear, &QToolButton::clicked, this, [=]{ClearHeaders_Slot(ui->treeWidgetSort, mSortHdrs);});
    connect(ui->buttonSortDelete,&QToolButton::clicked, this, [=]{DeleteHeader_Slot(ui->treeWidgetSort, mSortHdrs);});
    connect(ui->buttonSortUp,    &QToolButton::clicked, this, [=]{Up_Slot(ui->treeWidgetSort, mSortHdrs);});
    connect(ui->buttonSortDown,  &QToolButton::clicked, this, [=]{Down_Slot(ui->treeWidgetSort, mSortHdrs);});
    connect(ui->treeWidgetSort,  &QTreeWidget::doubleClicked, this, [=]{EditHeader_Slot(ui->treeWidgetSort, mSortHdrs);});
    connect(ui->treeWidgetSort,  &QTreeWidget::currentItemChanged, this, [=]{ItemIndexChanged_Slot(ui->treeWidgetSort);});
    ItemIndexChanged_Slot(ui->treeWidgetSort);


    //Generales del diálogo
    connect(ui->buttonBox->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), this, SLOT(reject()));
    connect(ui->buttonBox->button(QDialogButtonBox::Ok), SIGNAL(clicked()), this, SLOT(accept()));

    XGlobal::APP_SETTINGS.ct_bhe++;
    MainWindow::DPIAware(this, true);
}

BatchHeaderDialog::~BatchHeaderDialog()
{
    delete mAddHdrs;
    delete mRemoveHdrs;
    delete mSortHdrs;
    delete ui;
}

void BatchHeaderDialog::LoadHeadersToView(FitHeaderList* list, QTreeWidget* tree)
{
    tree->clear();
    for (int i=0; i<list->count(); i++)
    {
       FitHeader hd = list->get(i);
       QTreeWidgetItem* item = new QTreeWidgetItem();
       item->setText(0,hd.name());
       if (tree == ui->treeWidgetAdd)
       {
           item->setText(1,hd.value());
           item->setText(2,hd.comment());
           if (hd.comment().length())
                 item->setToolTip(2,hd.comment());
       }
       else
       {
           item->setText(1, hd.comment());
       }
       tree->addTopLevelItem(item);
    }
}


void BatchHeaderDialog::LoadFromIni_Slot()
{
   QString filename = QFileDialog::getOpenFileName(this,
                                                   "Import from file",
                                                   "","Ini file (*.ini)");
   if (!filename.length()) return;
   if (!QFile(filename).exists())
   {
       QMessageBox::warning(this, "Error", "Selected file doesn't exits");
       return;
   }
   if (!mAddHdrs->ReadFromIni(filename, SECTION_HEADERS_ADD) ||
       !mRemoveHdrs->ReadFromIni(filename, SECTION_HEADERS_DELETE) ||
       !mSortHdrs->ReadFromIni(filename, SECTION_HEADERS_SORT))
       QMessageBox::warning(this, "Error", "Unable to load file");

   LoadHeadersToView(mAddHdrs, ui->treeWidgetAdd);
   LoadHeadersToView(mRemoveHdrs, ui->treeWidgetDelete);
   LoadHeadersToView(mSortHdrs, ui->treeWidgetSort);
}

void BatchHeaderDialog::accept()
{
    //Antes de nada guardamos los parámetros
    mAddHdrs->SaveToIni(mIniFile, SECTION_HEADERS_ADD);
    mRemoveHdrs->SaveToIni(mIniFile, SECTION_HEADERS_DELETE);
    mSortHdrs->SaveToIni(mIniFile, SECTION_HEADERS_SORT);
    QSettings sett(mIniFile, QSettings::IniFormat);
    mInputFiles.Save(&sett, BATCH_HEADERS_SECTION);

    MdiFitImage* fi = MainWindow::mainwin->ActiveFitImage();
    if (fi) INFO("Selected image: %s",PTQ(fi->filename()));

    //Obtener todos los parámetros
    if (!ui->widgetSourceFiles->CheckData(&mInputFiles)) return;

    //Ocultamos el diálogo para mostrar el de progreso
    this->hide();
    ProgressDialog prg(this,"Batch editing headers");
    prg.SetIcon(":/graph/images/icn_list.svg");
    prg.Connect(mHeaderEditor);
    prg.ExecInBackground(Background_HeaderEdit, this);

    if (mHeaderEditor->cancelled())
    {
        this->close();
        return;
    }

    //Si la astrometría ha fallado mostramos un mensaje
    if (mHeaderEditor->CountKO() && mInputFiles.fitimages()->count() == mHeaderEditor->CountKO() )
    {
        QMessageBox::warning(this,"Batch editing failed",
                             "Unable to edit files. Please, check input parameters");
    }
    else if (mHeaderEditor->CountKO())
    {
        QMessageBox::warning(this,"Batch editing  failed",
                             "Some files were not edited. Please check input parameters and retry.");
    }

    //Si todo ha ido bien podemos cerrar el diálogo, pero mientras tanto no
    else if (!mHeaderEditor->cancelled() && fi)
    {
        fi->CheckDirty();
    }

    XGlobal::APP_SETTINGS.ct_fbhe += mInputFiles.fitimages()->count()+mInputFiles.filenames()->count();
    this->close();
}

void BatchHeaderDialog::SaveToIni_Slot()
{
   QString filename = QFileDialog::getSaveFileName(this,
                                                   "Save headers to edit",
                                                   "","Ini file (*.ini)");
   if (!filename.length()) return;
   bool ok = mAddHdrs->SaveToIni(filename, SECTION_HEADERS_ADD) &&
             mRemoveHdrs->SaveToIni(filename, SECTION_HEADERS_DELETE) &&
             mSortHdrs->SaveToIni(filename, SECTION_HEADERS_SORT);

   if (!ok)
   {
       QMessageBox::warning(this, "Error", "Unable to save data");
       return;
   }
}

void BatchHeaderDialog::AddHeader_Slot(QTreeWidget* tree, FitHeaderList *list)
{
    InputDialog idialog(this);
    idialog.SetImage(QPixmap(":/graph/images/icn_plus.svg"));
    idialog.SetTitle("Add header to list");
    idialog.SetSubTitle("Please, fill in the name and value fields.");
    QStringList names, values, inputmask;
    if (list == mAddHdrs) names << "Name" << "Value" << "Comments";
    else names << "Name" << "Comments";;
    values  << "[UPPERCASE]" << "" << "";
    inputmask << "xxxxxxxx" << "" << "";

    QStringList result = idialog.ShowMultipleTexts(names, values, inputmask);
    if (result.count() == 0) return;
    //Normalizamos el nombre
    result[0] = result.at(0).trimmed().toUpper();
    if (result.length() >= 2) result[1] = result.at(1).trimmed();
    if (result.length() >= 3) result[2] = result.at(2).trimmed();

    if (!result.at(0).length())
    {
        QMessageBox::information(this,"Unable to add header.",
                             "Name is empty.\n\n Please, add a header name in the insert dialog box.",
                             QMessageBox::Ok
                             );
        return;
    }
    if (FitHeaderList::IsMainHeader(result[0]))
    {
        QMessageBox::information(this,"Insert header denied",
                                 QString("You can't add the header: '%1'.\n\n This header is reserved").arg(result[0]),
                                 QMessageBox::Ok
                                 );
        return;
    }
    //Si la cabecera ya existe preguntamos que si quiere sobreescribirla
    FitHeader fh = list->Contains(result.at(0));
    if (!fh.isInvalid() && fh.isData())
    {
        int dres =  QMessageBox::information(this,"Confirm replace",
                                               QString("Header '%1' already exists.\n\n Do you want to replace it with the new value?").arg(result[0]),
                                               QMessageBox::Yes | QMessageBox::No
                                               );
        if (dres == QMessageBox::No) return;
        //Reemplazar la cabecera con la nueva, seguimos adelante
    }
    //Crear una nueva o reemplazar la existente?
    QTreeWidgetItem* item;
    if (!fh.isInvalid() && fh.isData()) item = tree->topLevelItem(list->indexOf(result.at(0)));
    else item = new QTreeWidgetItem();

    //Si la cabecera no tiene comentarios, les metemos comentarios por defecto
    item->setText(0, result.at(0));
    if (tree == ui->treeWidgetAdd)
    {
        if (!result.at(2).length()) result[2] = FitHeaderList::HeaderComments(result[0]);
        item->setText(1, result.at(1));
        item->setText(2, result.at(2));
        if (result.at(2).length())
             item->setToolTip(2, result.at(2));
        list->AddHeader(result.at(0), result.at(1), result.at(2));
    }
    else
    {
         if (!result.at(1).length()) result[1] = FitHeaderList::HeaderComments(result[0]);
        item->setText(1, result.at(1));
        if (result.at(1).length())
             item->setToolTip(1,result.at(1));
        list->AddHeader(result.at(0), "dummy", result.at(1));
    }
    tree->addTopLevelItem(item);
    tree->setCurrentItem(item);
}

void  BatchHeaderDialog::DeleteHeader_Slot(QTreeWidget* tree, FitHeaderList *list)
{
    int index = tree->currentIndex().row();
    if (index < 0) return;
    delete tree->topLevelItem(index);
    list->RemoveAt(index);
    ItemIndexChanged_Slot(tree);
}

void BatchHeaderDialog::EditHeader_Slot(QTreeWidget* tree, FitHeaderList *list)
{
    int index = tree->currentIndex().row();
    if (index < 0) return;
    QTreeWidgetItem* item = tree->topLevelItem(index);
    InputDialog idialog(this);
    idialog.SetImage(QPixmap(":/graph/images/pen-32.png"));
    idialog.SetTitle("Edit header from list");
    idialog.SetSubTitle("Please, fill in the name and value fields.");
    QStringList names, values, inputmask;
    inputmask << "xxxxxxxx" << "" << "";
    FitHeader fh = list->get(index);
    if (list == mAddHdrs)
    {
        names << "Name" << "Value" << "Comments";
        values  << "[UPPERCASE]" +  fh.name() << fh.value() << fh.comment();
    }
    else
    {
        names << "Name" << "Comments";
        values  << "[UPPERCASE]" + fh.name() << fh.comment();
    }

    QStringList result = idialog.ShowMultipleTexts(names, values, inputmask);
    if (result.count() == 0) return;
    //Normalizamos el nombre
    result[0] = result.at(0).trimmed().toUpper();
    if (result.length() >= 2) result[1] = result.at(1).trimmed();
    if (result.length() >= 3) result[2] = result.at(2).trimmed();

    if (!result.at(0).length())
    {
        QMessageBox::information(this,"Unable to add header.",
                             "Name is empty.\n\n Please, add a header name in the insert dialog box.",
                             QMessageBox::Ok
                             );
        return;
    }
    if (FitHeaderList::IsMainHeader(result[0]))
    {
        QMessageBox::information(this,"Insert header denied",
                                 QString("You can't add the header: '%1'.\n\n This header is reserved").arg(result[0]),
                                 QMessageBox::Ok
                                 );
        return;
    }
    //Si la cabecera ya existe preguntamos que si quiere sobreescribirla
    int prev_idx = list->indexOf(result.at(0));
    if (prev_idx != -1 && prev_idx != index)
    {
        int dres =  QMessageBox::information(this,"Confirm replace",
                                               QString("Header '%1' already exists.\n\n Do you want to replace it with the new value?").arg(result[0]),
                                               QMessageBox::Yes | QMessageBox::No
                                               );
        if (dres == QMessageBox::No) return;
        //Borramos la otra cabecera
        delete tree->topLevelItem(prev_idx);
        list->RemoveAt(prev_idx);
        index = list->indexOf(fh.name());
    }
    //Si la cabecera no tiene comentarios, les metemos comentarios por defecto
    item->setText(0, result.at(0));
    if (tree == ui->treeWidgetAdd)
    {
        if (!result.at(2).length()) result[2] = FitHeaderList::HeaderComments(result[0]);
        item->setText(1, result.at(1));
        item->setText(2, result.at(2));
        if (result.at(2).length())
             item->setToolTip(2, result.at(2));
        list->ReplaceHeader(index,FitHeader(result.at(0), result.at(1), result.at(2)));
    }
    else
    {
        if (!result.at(1).length()) result[1] = FitHeaderList::HeaderComments(result[0]);
        item->setText(1, result.at(1));
        if (result.at(1).length())
             item->setToolTip(1,result.at(1));
        list->ReplaceHeader(index, FitHeader(result.at(0), "dummy", result.at(1)));
    }
    tree->addTopLevelItem(item);
    tree->setCurrentItem(item);
}

void BatchHeaderDialog::ClearHeaders_Slot(QTreeWidget* tree, FitHeaderList *list)
{
    tree->clear();
    list->Clear();
    ItemIndexChanged_Slot(tree);
}

void BatchHeaderDialog::Up_Slot(QTreeWidget* tree, FitHeaderList *list)
{
    int index = tree->currentIndex().row();
    if (index <= 0) return;
    FitHeader hd = list->get(index);
    list->RemoveAt(index);
    list->Insert(index - 1, hd);
    QTreeWidgetItem* item = tree->topLevelItem(index);
    QTreeWidgetItem* item2 = new QTreeWidgetItem();
    for (int i=0; i<item->columnCount(); i++)
        item2->setText(i, item->text(i));
    delete item;
    tree->insertTopLevelItem(index - 1, item2);
    tree->setCurrentItem(item2);
}

void BatchHeaderDialog::Down_Slot(QTreeWidget* tree, FitHeaderList *list)
{
    int index = tree->currentIndex().row();
    if (index < 0 || index == list->count()-1) return;
    FitHeader hd = list->get(index);
    list->RemoveAt(index);
    list->Insert(index + 1, hd);
    QTreeWidgetItem* item = tree->topLevelItem(index);
    QTreeWidgetItem* item2 = new QTreeWidgetItem();
    for (int i=0; i<item->columnCount(); i++)
        item2->setText(i, item->text(i));
    delete item;
    tree->insertTopLevelItem(index + 1, item2);
    tree->setCurrentItem(item2);
}

void BatchHeaderDialog::ItemIndexChanged_Slot(QTreeWidget* tree)
{
    QToolButton* btn_clear = tree == ui->treeWidgetAdd ? ui->buttonAddClear : tree == ui->treeWidgetSort  ? ui->buttonSortClear :  ui->buttonDeleteClear;
    QToolButton* btn_delete = tree == ui->treeWidgetAdd ? ui->buttonAddDelete : tree == ui->treeWidgetSort  ? ui->buttonSortDelete : ui->buttonDeleteDelete;
    QToolButton* btn_up = tree == ui->treeWidgetAdd ? ui->buttonAddUp : tree == ui->treeWidgetSort  ? ui->buttonSortUp : ui->buttonDeleteUp;
    QToolButton* btn_down = tree == ui->treeWidgetAdd ? ui->buttonAddDown : tree == ui->treeWidgetSort  ? ui->buttonSortDown : ui->buttonDeleteDown;
    int idx =  tree->currentIndex().row();
    btn_clear->setEnabled(tree->topLevelItemCount() > 0);
    btn_delete->setEnabled(idx >= 0);
    btn_up->setEnabled(idx > 0);
    btn_down->setEnabled(idx < tree->topLevelItemCount()-1 && idx > -1);
    ui->buttonSaveSettings->setEnabled(mAddHdrs->count() || mRemoveHdrs->count() || mSortHdrs->count());
}


int BatchHeaderDialog::Background_HeaderEdit(void *data)
{
   BatchHeaderDialog* dlg = (BatchHeaderDialog*)data;
   bool result = dlg->mHeaderEditor->doWork(&(dlg->mInputFiles), dlg->mAddHdrs, dlg->mRemoveHdrs, dlg->mSortHdrs);
   return result?0:1;
}






