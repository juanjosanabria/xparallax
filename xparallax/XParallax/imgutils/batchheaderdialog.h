#ifndef BATCHHEADERDIALOG_H
#define BATCHHEADERDIALOG_H

#include <QDialog>
#include <QTreeWidget>
#include "fitimage.h"
#include "progress.h"
#include "imgutils/headereditor.h"

namespace Ui {
class BatchHeaderDialog;
}

class BatchHeaderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BatchHeaderDialog(QWidget *parent = 0);
    ~BatchHeaderDialog();

private:
    Ui::BatchHeaderDialog *ui;

    WorkFileList           mInputFiles;
    FitHeaderList*         mAddHdrs;
    FitHeaderList*         mRemoveHdrs;
    FitHeaderList*         mSortHdrs;
    HeaderEditor*          mHeaderEditor;
    QString                mIniFile;


    void accept();

    void LoadHeadersToView(FitHeaderList* list, QTreeWidget* tree);

    static const char*     BATCHHEADER_DIALOG_INI;
    static int  Background_HeaderEdit(void *data);

private slots:
    void    LoadFromIni_Slot();
    void    SaveToIni_Slot();
    void    AddHeader_Slot(QTreeWidget* tree, FitHeaderList *list);
    void    DeleteHeader_Slot(QTreeWidget* tree, FitHeaderList *list);
    void    ClearHeaders_Slot(QTreeWidget* tree, FitHeaderList *list);
    void    ItemIndexChanged_Slot(QTreeWidget* tree);
    void    Up_Slot(QTreeWidget* tree, FitHeaderList *list);
    void    Down_Slot(QTreeWidget* tree, FitHeaderList *list);
    void    EditHeader_Slot(QTreeWidget* tree, FitHeaderList *list);

};

#endif // BATCHHEADERDIALOG_H
