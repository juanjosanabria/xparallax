#ifndef HEADEREDITOR_H
#define HEADEREDITOR_H

#include "progress.h"

class HeaderEditor
        : public BackgroundWorker
{
     Q_OBJECT

public:
    HeaderEditor();
    ~HeaderEditor();
     bool doWork(WorkFileList* wf, FitHeaderList* add_hdr, FitHeaderList *remove_hdr, FitHeaderList* sort_hdr);
     int CountOK();
     int CountKO();


private:
    WorkFileList*  mWorkFileList;
    FitHeaderList* mAddHdr;
    FitHeaderList* mRemoveHdr;
    FitHeaderList* mSortHdr;
    QStringList    mResults;
    FitImage*      mCurFile;

    /** Aplica a una cadena los cambios de macros */
    QString tr(QString cad, FitHeader* src_hdr);

    /** Aplica la transforamción de cabeceras a una imagen */
    void ComputeImage(FitImage* img);
    QString ComputeImage(const QString& filename);

    /** Operar sobre las cabeceras de una imagen real **/
    void AddHeaders(FitHeaderList* fhl);
    void DeleteHeaders(FitHeaderList* fhl);
    void SortHeaders(FitHeaderList* fhl);
    int  CompareHeaders(FitHeaderList* fhl, FitHeader* hdr1, FitHeader* hdr2);

    /** Edición de cabeceras. Dado que se realiza un QSort con métodos estáticos no puede haber concurrencia*/
    static QMutex         SORT_MUTEX;
    static FitHeaderList* CURRENT_HDRLIST;
    static HeaderEditor*  CURRENT_EDITOR;
    static int LessThan(FitHeader a, FitHeader b);
    static double ParseExpression(FitHeaderList* fhl, QString exp);

};

#endif // HEADEREDITOR_H
