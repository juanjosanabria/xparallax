#ifndef ASTROMETRYDIALOG_H
#define ASTROMETRYDIALOG_H

#include <QDialog>
#include "matcher.h"
#include "detection.h"
#include "vo.h"
#include "astrometry.h"
#include "mdifitimage.h"

namespace Ui {
    class AstrometryDialog;
}

class AstrometryDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit AstrometryDialog(QWidget *parent = 0);
    ~AstrometryDialog();
    static const char* ASTROMETRY_DIALOG_INI;

private:
    Ui::AstrometryDialog *  ui;
    MatchParameters         mMatchP;
    DetectionParameters     mDetectP;
    WorkFileList            mInputFiles;
    AstrometryComputer*     mAstrometryComputer;
    QSettings*              mSettings;

    void SaveParameters();

protected:
    virtual void accept();
    static int Background_Atrometry(void *data);

private slots:


private:  
    static  QString  FN_GET_ITEM_IMAGE(FitHeaderList* fh,const QString& filename);

};

#endif // ASTROMETRYDIALOG_H
