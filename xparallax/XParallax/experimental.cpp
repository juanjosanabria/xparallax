#include "experimental.h"
#include "mainwindow.h"
#include "matcher.h"
#include "ui_helpers/ProgressDialog.h"
#include "inputdialog.h"
#include "astrometrydialog.h"
#include "tools/coordinatetransform.h"
#include "tools/angulardistance.h"

QMenu*               Experimental::mExperimentalMenu = NULL;
StarDetector *       Experimental::mStarDetector = NULL;
DetectedStar*        Experimental::mDetectedStars = NULL;
int                  Experimental::mDetectedStarCount = 0;
FitImage*            Experimental::mImage = NULL;
MdiFitImage*         Experimental::mMdiFitImage = NULL;
bool                 Experimental::mShowExperimental = false;
MatchParameters*     Experimental::mMatchParameters = NULL;
DetectionParameters* Experimental::mDetectionParameters = NULL;
WorkFileList*        Experimental::mInputFiles = NULL;
PlateConstants*      Experimental::mPlateConstants = NULL;

Experimental::Experimental(QObject *parent) :
    QObject(parent)
{
    mSettings = XGlobal::GetSettings(EXP_INI_FILE);
}


void Experimental::CreateExperimentalMenu()
{
    mExperimentalMenu = MainWindow::mainwin->getMenuByName(MENU_EXPERIMENTAL, true, NULL, MENU_EXPERIMENTAL_PRIORITY);
    #ifndef QT_DEBUG
        mExperimentalMenu->setTitle("");
        mExperimentalMenu->setEnabled(false);
        mShowExperimental = false;
    #else
        mShowExperimental = true;
    #endif

    //Acciones experimentales
    mSearchStarsAction = mExperimentalMenu->addAction("Star detector");
    connect(mSearchStarsAction, SIGNAL(triggered()), this, SLOT(StarDetector_Slot()));

    mForceAlignAction = mExperimentalMenu->addAction("Force align");
    connect(mForceAlignAction, SIGNAL(triggered()), this, SLOT(ForceAlign_Slot()));

    //Herramienta de distancia angular
    mAngularDistance = mExperimentalMenu->addAction("Angular distance");
    connect(mAngularDistance, SIGNAL(triggered()), this, SLOT(Tool_AngularDistance_Slot()));

    //Herramienta de transformación de coordenadas
    mEpochConverter = mExperimentalMenu->addAction("Epoch converter");
    connect(mEpochConverter, SIGNAL(triggered()), this, SLOT(Tool_CoordTransform_Slot()));
}

int Experimental::StarDetector_Thread(void *)
{
    mStarDetector->parameters()->detect_thres_sigma = 3;
    //mStarDetector->parameters()->detect_min_radius = 20;
    mStarDetector->parameters()->detect_max_radius = 65;
    mStarDetector->parameters()->p_bkg.bkg_mesh_size = 64;
    mStarDetector->parameters()->deblend_nthres = 64;
    mDetectedStars = mStarDetector->DetectStars(0,0,mImage->width()-1,mImage->height()-1);
    return 0;
}

void Experimental::StarDetector_Slot()   //Detecta estrellas en la imagen actual
{
    mMdiFitImage = MainWindow::mainwin->ActiveFitImage();
    if (!mMdiFitImage) return;
    mImage = mMdiFitImage->fitimage();
    ProgressDialog prg(MainWindow::mainwin, "Star detector working");
    prg.SetIcon(":/graph/images/icn_stars.svg");
    DINFO("------------ STAR DETECTOR RESULTS --------------");
    DINFO("#FILENAME: %s", PTQ(QFileInfo(mMdiFitImage->filename()).baseName()));
    DINFO("#ID\tX\tY\tX_XP\tY_XP\tRA_DEG\tDEC_DEG");
    StarDetector *detector = new StarDetector(mImage->data(),mImage->width(),mImage->height());

    if (mStarDetector) delete mStarDetector;
    mStarDetector = detector;



    prg.Connect(detector);
    prg.Connect(detector->background());

    int ret = prg.ExecInBackground(StarDetector_Thread, mMdiFitImage);

    //DetectedStar* stars = detector->DetectStars(0,0,img->width(),img->height());
    if (!mDetectedStars) return;


    XOverlayLayer* layer = new XOverlayLayer("Detected starts");
    XOverlayStyle* mc_style = layer->addStyle(3);
    XOverlayStyle* deblend_Style = layer->addStyle(4);
    deblend_Style->setBorderColor(Qt::magenta);
    mc_style->setBorderColor(Qt::green);
    mc_style->setHoverBorderColor(Qt::yellow);
    PlateConstants* pt = mMdiFitImage->plate();


    QFile fout("c:\\str.txt");
    fout.open(QFile::WriteOnly);
    QTextStream ts(&fout);
    ts << "#ID X Y X_XP Y_XP RA DEC " << ret << "\n";
    for (int i=0; i<detector->count(); i++)
    {
        ts << i << " " << mDetectedStars[i].x() << " " << mDetectedStars[i].y() <<  " " << (mImage->height()- mDetectedStars[i].y()) << "\n";

        XOverlayCircle* cir = new XOverlayCircle(layer, mDetectedStars[i].hasFlag(DetectedStar::DF_DEBLENDED)?4:3);
        cir->setRadius(mDetectedStars[i].radius());
        cir->setImageAttatched(true);
        cir->setZoomAttatched(true);
        cir->setHoverable(true);
        cir->setBasePos(mDetectedStars[i].x(), mDetectedStars[i].y());
        if (!pt)
        {
            cir->setLabel(QString("").sprintf("XY: %0.2f / %0.2f", mDetectedStars[i].x(), mDetectedStars[i].y()));
        }
        else
        {
            double ra, dec;
            pt->map(mDetectedStars[i].x(), mDetectedStars[i].y(), &ra, &dec);
            cir->setLabel(QString("").sprintf("XY:%0.2f/%0.2f | r:%0.2f\nRA: %s\nDEC: %s\n\nFLUX: %0.3f"
                                              "\n\n a:%0.2f; b:%0.2f; ecc:%0.2f"
                                              "\n WX:%d; WY:%d"
                                              "\n CCX:%0.2f; CCY:%0.2f"
                                             //Primera línea
                                             ,mDetectedStars[i].x(), mDetectedStars[i].y(),
                                               mDetectedStars[i].radius(),
                                             PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)),
                                             mDetectedStars[i].fluxISO(),
                                             //Segunda línea
                                              mDetectedStars[i].a(),mDetectedStars[i].b(), mDetectedStars[i].eccentricity(),
                                              //Tercera línea
                                              mDetectedStars[i].limits().width(),mDetectedStars[i].limits().height(),
                                              //Tercera línea
                                              mDetectedStars[i].ccx(), mDetectedStars[i].ccy()
                          ));
            DINFO("%d\t%7.2f\t%7.2f\t%7.2f\t%7.2f\t%14.10f\t%14.10f",i,
                  mDetectedStars[i].x()+1, mDetectedStars[i].y()+1,
                  mDetectedStars[i].x(), mDetectedStars[i].y(),
                  ra * HOR2DEG , dec);
        }
    }
    mMdiFitImage->addXLayer(layer);
    fout.close();

    detector->TakeMemControlOfStars();
    mMdiFitImage->update();
    ret = 0;
}

void Experimental::ForceAlign_Slot()     //Fuerza una alineación
{
    mMdiFitImage = MainWindow::mainwin->ActiveFitImage();
    if (!mMdiFitImage) return;

    QStringList labels;
    labels.append("A_X"); labels.append("A_Y");
    labels.append("A_RA"); labels.append("A_DEC");
    labels.append("B_X"); labels.append("B_Y");
    labels.append("B_RA"); labels.append("B_DEC");

    double a_x = Parameters::ReadFloat(mSettings, FORCE_ALIGN_SECTION, "A_X",   0);
    double a_y = Parameters::ReadFloat(mSettings, FORCE_ALIGN_SECTION, "A_Y",   0);
    double a_ra = Parameters::ReadRA(mSettings,   FORCE_ALIGN_SECTION, "A_RA",  0);
    double a_dec = Parameters::ReadDEC(mSettings, FORCE_ALIGN_SECTION, "A_DEC", 0);

    double b_x = Parameters::ReadFloat(mSettings, FORCE_ALIGN_SECTION, "B_X",   0);
    double b_y = Parameters::ReadFloat(mSettings, FORCE_ALIGN_SECTION, "B_Y",   0);
    double b_ra = Parameters::ReadRA(mSettings,   FORCE_ALIGN_SECTION, "B_RA",  0);
    double b_dec = Parameters::ReadDEC(mSettings, FORCE_ALIGN_SECTION, "B_DEC", 0);


    bool ok = false;

    while (!ok){
        QStringList values;
        values.append(QString("%1").arg(a_x));values.append(QString("%1").arg(a_y));
        values.append(Util::RA2String(a_ra)); values.append(Util::DEC2String(a_dec));
        values.append(QString("%1").arg(b_x));values.append(QString("%1").arg(b_y));
        values.append(Util::RA2String(b_ra)); values.append(Util::DEC2String(b_dec));

        InputDialog id(MainWindow::mainwin);
        id.SetTitle("Please input forced align");
        id.SetSubTitle("Two stars, A and B, imge and equatorial coordinates for each one");
        values = id.ShowMultipleTexts(labels, values);
        if (!values.length()) return;
        mImage = mMdiFitImage->fitimage();



        a_x = QVariant(values[0]).toFloat(&ok);
        if (!ok){
            a_x = 0;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for A_X");
            continue;
        }
        a_y = QVariant(values[1]).toFloat(&ok);
        if (!ok){
            a_y = 0;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for A_Y");
            continue;
        }
        a_ra = Util::ParseRA(values[2]);
        if (IS_INVALIDF(a_ra)){
            a_ra = 0;
            ok = false;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for A_RA");
            continue;
        }
        a_dec = Util::ParseDEC(values[3]);
        if (IS_INVALIDF(a_dec)){
            a_dec = 0;
            ok = false;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for A_DEC");
            continue;
        }


        b_x = QVariant(values[4]).toFloat(&ok);
        if (!ok){
            b_x = 0;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for B_X");
            continue;
        }
        b_y = QVariant(values[5]).toFloat(&ok);
        if (!ok){
            b_y = 0;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for B_Y");
            continue;
        }
        b_ra = Util::ParseRA(values[6]);
        if (IS_INVALIDF(b_ra)){
            b_ra = 0;
            ok = false;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for B_RA");
            continue;
        }
        b_dec = Util::ParseDEC(values[7]);
        if (IS_INVALIDF(b_dec)){
            b_dec = 0;
            ok = false;
            QMessageBox::warning(MainWindow::mainwin, "Invalid value", "Invalid value for B_DEC");
            continue;
        }
    }

    //Guardar los parámetros para la siguiente
    Parameters::SaveFloat(mSettings, FORCE_ALIGN_SECTION, "A_X", a_x);
    Parameters::SaveFloat(mSettings, FORCE_ALIGN_SECTION, "A_Y", a_y);
    Parameters::SaveRA(mSettings, FORCE_ALIGN_SECTION, "A_RA", a_ra);
    Parameters::SaveDEC(mSettings, FORCE_ALIGN_SECTION, "A_DEC", a_dec);
    Parameters::SaveFloat(mSettings, FORCE_ALIGN_SECTION, "B_X", b_x);
    Parameters::SaveFloat(mSettings, FORCE_ALIGN_SECTION, "B_Y", b_y);
    Parameters::SaveRA(mSettings, FORCE_ALIGN_SECTION, "B_RA", b_ra);
    Parameters::SaveDEC(mSettings, FORCE_ALIGN_SECTION, "B_DEC", b_dec);

    //Cargamos los parámetros de astrometría de la última realizada
    QSettings* sett = XGlobal::GetSettings(AstrometryDialog::ASTROMETRY_DIALOG_INI);
    if (!mMatchParameters)  mMatchParameters = new MatchParameters();
    mMatchParameters->Load(sett, "MATCHER");
    mMatchParameters->image_width = mImage->width();
    mMatchParameters->image_height = mImage->height();
    mMatchParameters->ComputeScale();
    if (!mDetectionParameters) mDetectionParameters = new DetectionParameters();
    mDetectionParameters->Load(sett, "DETECTION");
    if (!mInputFiles) mInputFiles = new WorkFileList();
    mInputFiles->Clear();
    mInputFiles->fitimages()->append(mImage);
    delete sett;

    //Detectar las estrellas
    INFO("Detecting stars");
    StarDetector* detector = new StarDetector(mImage->data(), mImage->width(), mImage->height());
    if (mDetectedStars) delete mDetectedStars;
    detector->DetectStars(0,0, mImage->width() - 1, mImage->height() - 1);
    mDetectedStars = detector->TakeMemControlOfStars();
    mDetectedStarCount = detector->count();
    INFO("Detected %d stars", detector->count());

    //Leer de catálogo
    INFO("Downloading catalog stars");
    VizierReader* vr = new VizierReader();
    vr->SetCatalog(VizierCatalog::getCatalog(mMatchParameters->vizier_catalog));
    vr->SetServer(VizierServer::getServer(mMatchParameters->vizier_server));
    vr->webRead(mMatchParameters->center_ra, mMatchParameters->center_dec,
                mMatchParameters->fov_radius_deg * mMatchParameters->fov_extension, mMatchParameters->mag_limit);
    INFO("Downloaded: %d stars", vr->stars()->count());

    //Buscar una coincidencia
    Matcher* matcher = new Matcher();
    matcher->setParameters(*mMatchParameters);
    ok = matcher->MatchPair(mDetectedStars, mDetectedStarCount, vr->stars(),
                       a_x, a_y, Util::RA2String(a_ra), Util::DEC2String(a_dec),
                       b_x, b_y, Util::RA2String(b_ra), Util::DEC2String(b_dec)
                       );

    if (ok)
    {
        PlateConstants* pc = AstrometryComputer::ComputePlateConstants(matcher);
        AstrometryComputer::AddLspcHeader(mImage, pc);
        AstrometryComputer::AddStarndardHeader(mImage, pc);
        mMdiFitImage->LookForPlateInFit();
        mMdiFitImage->CheckDirty();
        mMdiFitImage->SetMatch(matcher, pc);
        delete pc;
    }


    delete detector;
    delete matcher;
    delete vr;
}

void Experimental::Tool_AngularDistance_Slot()
{
    AngularDistanceDialog* ad = new AngularDistanceDialog(MainWindow::mainwin);
    ad->show();
}

void Experimental::Tool_CoordTransform_Slot()
{
    CoordinateTransformDialog* cd = new CoordinateTransformDialog(MainWindow::mainwin);
    cd->show();
}





