#ifndef XOVERLAYITEM_H
#define XOVERLAYITEM_H

#include <QFlags>
#include <QPainter>
#include "core.h"
#include "xglobal.h"
#include "xoverlaystyle.h"

#define COS_45  0.70710678118654752440084436210485
#define SIN_45  COS_45

/**************************************************************
XOverlayItem
|
|
|_______XOverlayCircle
|_______XOverlayRect
|_______XOverlayArrow
|_______XOverlayCross
|_______XOverlaySegment
|
|__________________XOverlayLabel



****************************************************************/

class XOverlayLayer;

#define    XOVERLAY_RESIZE_BOX_SIZE         (8 * XGlobal::SCREEN_DPI)

enum XOverlaItemFlags_{
    //Valores de control
    XITEM_NULL_FLAGS = 0,
    XITEM_ALL_FLAGS = 0xFFFFFFFF,
    //Valores de estado
    XITEM_VISIBLE           = 1<<0,        //El item es visible
    XITEM_HOVERABLE         = 1<<1,        //Se capturan u toman acciones ante los eventos mouseHover
    XITEM_HOVER             = 1<<2,        //Indica si el cursor del mouse se encuentra o no sobre este elemento
    XITEM_SCREEN_ATTATCHED  = 1<<3,        //Indica que el item está fijo
    XITEM_RADEC_ATTATCHED   = 1<<4,        //Su posición se expresa en unidades de RA-Dec
    XITEM_IMAGE_ATTATCHED   = 1<<5,        //El item está anclado a las coordenadas de imagen
    XITEM_ZOOM_ATTATCHED    = 1<<6,        //Su tamaño está relacionado con el zoom de la imagen y expresado en unidades de píxeles
    XITEM_RESIZABLE         = 1<<7,        //El item se puede cambiar de tamaño
    XITEM_RESIZING          = 1<<8,        //Nos encontramos en una situación de redimensionado del item
    XITEM_MOBILE            = 1<<9,        //El item se puede arrastrar y mover
    XITEM_MOVING            = 1<<10,       //El item se tatá cambiando de posición
    XITEM_HAS_CONTEXTMENU   = 1<<11,       //El item puede mostar un menú de contexto
    XITEM_GROWMODE_RADIAL   = 1<<12,       //El item crece de manera radial desde un centro // Si no crece así crecerá rectangularmente
    XITEM_LABEL_HOVER       = 1<<13,       //La etiqueta que tuviera se mostrará solamente cuando el cursor del ratón esté encima
    XITEM_LABEL_MULTILINE   = 1<<14,       //La etiqueta posee varias líneas
    XITEM_LABEL_HTML        = 1<<15,       //La etiqueta contiene texto html
    XITEM_LABEL_FROM_POS    = 1<<16,       //Dibuja la etiqueta desde el punto de posicionado ignorando el rectángulo
    XITEM_LABEL_NEAR_MOUSE  = 1<<17,       //Dibuja la etiqueta allí donde estuviera el ratón
    XITEM_ALWAYS_ON_TOP     = 1<<18,       //Este elemento se mostrará siempre por delante
    XITEM_PAINT_LABEL       = 1<<19,       //Indica si se debe o no dibujar su etiqueta
    XITEM_SELECTABLE        = 1<<20,       //El item se puede seleccionar
    XITEM_SELECTED          = 1<<21,       //El item está seleccionado
    XITEM_DELETABLE         = 1<<22,       //El item puede ser borrado con la tecla supr
};

enum Resizebox{
    RESIZEBOX_NONE          = 0,
    RESIZEBOX_UPER_LEFT     = 1,
    RESIZEBOX_UPER_RIGHT    = 2,
    RESIZEBOX_LOWER_LEFT    = 3,
    RESIZEBOX_LOWER_RIGHT   = 4,
};

class XOverlayItem
{
    Q_DECLARE_FLAGS(XOverlaItemFlags, XOverlaItemFlags_)

private:
    //Control interno del item
    //************************************************************************
    quint16             mId;                //Identificador del item modificable y usable por el usuario
    quint8              mGroup;             //Agrupación de 8 bits
    quint8              mType;              //Tipo de item. Es responsabilidad inicializrlo por clases descendientes
    XOverlayStyle*      mStyle;             //Estilo que puede ser propio o controlado por la capa
    XOverlayLayer*      mLayer;             //Capa que contiene este item si la hubiera
    XOverlaItemFlags    mFlags;             //Flags del item
    QString             mLabel;             //Etiqueta para mostrar en el Item. Puede ser multilínea

    //Posicionado
    //************************************************************************
    QPointF             mPosition;          //Posición en pantalla que puede cambiar dinámicamente
    QPointF             mBasePos;           //Ascensión recta o dec si estuviera RADEC_ATTATCHED ó x,y para ImageAttatched
    static QPoint       mLastMousePos;      //Última posición del ratón

protected:
    inline void setFlag(XOverlaItemFlags flag, bool state=true){ state ? mFlags |= flag : mFlags &= ~flag;}
    inline void removeFlag(XOverlaItemFlags flag){setFlag(flag, false);}
    inline QRectF appendResizeBox(QRectF b){ return QRectF(b.x() - XOVERLAY_RESIZE_BOX_SIZE, b.y() - XOVERLAY_RESIZE_BOX_SIZE,  b.width() + XOVERLAY_RESIZE_BOX_SIZE*2, b.height() + XOVERLAY_RESIZE_BOX_SIZE * 2); }
    float zoom();
    void paintResizeBox(QPainter* painter);

public:

    //Constructores, destructores
    //************************************************************************
    explicit XOverlayItem(quint8 type, XOverlayLayer* layer = 0, quint8 group = 0);
    virtual ~XOverlayItem();

    //Generales del item, identificadores, agrupaciones etc
    //************************************************************************
    inline quint16 id(){return mId;}
    inline void setId(quint16 id){mId = id;}
    inline quint8 type(){return mType;}
    inline quint8 group(){return mGroup;}
    inline void setGroup(quint8 grp){mGroup = grp;}
    inline QString label() const{ return mLabel;}
    inline void setLabel(const QString &lab){mLabel = lab; setFlag(XITEM_LABEL_MULTILINE, mLabel.contains('\n'));}
    inline int hasLabel(){return mLabel.length();}
    inline int flags(){return mFlags;}
    inline bool multiLine(){return mFlags & XITEM_LABEL_MULTILINE;}
    inline bool html(){return mFlags & XITEM_LABEL_HTML;}
    inline void setHtml(bool ht){setFlag(XITEM_LABEL_HTML, ht);}
    inline XOverlayStyle* style(){return mStyle;}
    inline void setStyle(XOverlayStyle* st)
    { if (mStyle == st) return; if (mStyle && mStyle != XOverlayStyle::defaultStyle() && mStyle->single()) delete mStyle; mStyle = st;}
    inline bool alwaysOnTop(){return mFlags & XITEM_ALWAYS_ON_TOP;}
    inline void setAlwaysOnTop(bool at = true){ setFlag(XITEM_ALWAYS_ON_TOP, at);}

    //Posición y visionado
    //************************************************************************
    inline bool visible(){return mFlags & XITEM_VISIBLE;}
    inline void setVisible(bool vis){setFlag(XITEM_VISIBLE, vis);}
    inline QPointF pos() const{ return mPosition;}
    inline void setPos(const QPointF &pos){mPosition = pos;}
    inline void setPos(float px, float py){ mPosition = QPointF(px, py);}
    inline void setBasePos(const QPointF &pos){mBasePos = pos;}
    inline void setBasePos(float px, float py){ mBasePos = QPointF(px, py);}
    inline void setX(float px){mPosition.setX(px);}
    inline void setY(float py){mPosition.setY(py);}
    inline float x(){return mPosition.x();}
    inline float y(){return mPosition.y();}
    inline double bx(){return mBasePos.x();}
    inline double by(){return mBasePos.y();}
    inline bool hover(){return mFlags & XITEM_HOVER;}
    inline void setHover(bool hov){setFlag(XITEM_HOVER, hov);}
    inline bool hoverable(){return mFlags & XITEM_HOVERABLE;}
    inline void setHoverable(bool hv){setFlag(XITEM_HOVERABLE, hv);}
    inline void setLayer(XOverlayLayer* player){mLayer = player;}
    inline XOverlayLayer* layer(){return mLayer;}
    inline bool resizable(){return mFlags & XITEM_RESIZABLE;}
    inline bool resizing(){return mFlags & XITEM_RESIZING;}
    inline void setResizable(bool res){setFlag(XITEM_RESIZABLE, res);}
    inline void setResizing(bool res){setFlag(XITEM_RESIZING, res);}
    inline bool mobile(){return mFlags & XITEM_MOBILE;}
    inline bool moving(){return mFlags & XITEM_MOVING;}
    inline void setMobile(bool mov){setFlag(XITEM_MOBILE, mov);}
    inline void setMoving(bool mov){setFlag(XITEM_MOVING, mov);}
    inline void setPaintLabel(bool pl){setFlag(XITEM_PAINT_LABEL, pl);}
    inline void setLabelWhenHover(bool lh){setFlag(XITEM_LABEL_HOVER, lh);}
    inline void setLabelNearMouse(bool ln){setFlag(XITEM_LABEL_NEAR_MOUSE, ln);}
    inline void setLabelFromPos(bool lp){setFlag(XITEM_LABEL_FROM_POS, lp);}
    inline bool labelNearMouse(){return mFlags & XITEM_LABEL_NEAR_MOUSE;}
    inline bool selectable(){return mFlags & XITEM_SELECTABLE;}
    inline void setSelectable(bool sl){setFlag(XITEM_SELECTABLE, sl);}
    inline void setSelected(bool sl){setFlag(XITEM_SELECTED, sl);}
    inline bool selected(){return mFlags & XITEM_SELECTED;}
    inline void setDeletable(bool dl){setFlag(XITEM_DELETABLE, dl);}
    inline bool deletable(){return mFlags & XITEM_DELETABLE;}

    //Tipo de estructura de posicionado
    //************************************************************************
    inline float ra(){return mBasePos.x();}
    inline float dec(){return mBasePos.y();}
    inline bool raDecAttatched(){ return mFlags & XITEM_RADEC_ATTATCHED;}
    inline void setRaDec(double ra, double dec){mBasePos = QPointF(ra, dec); setRaDecAttatched(true);}
    inline void setRaDecAttatched(bool ra){setFlag(XITEM_RADEC_ATTATCHED, ra); if (ra) setFlag((XOverlaItemFlags)(XITEM_IMAGE_ATTATCHED|XITEM_SCREEN_ATTATCHED), false);}
    inline bool zoomAttatched(){return mFlags & XITEM_ZOOM_ATTATCHED;}
    inline void setZoomAttatched(bool za){setFlag(XITEM_ZOOM_ATTATCHED, za);}
    inline bool imageAttatched(){return mFlags & XITEM_IMAGE_ATTATCHED;}
    inline void setImageAttatched(bool ia){setFlag(XITEM_IMAGE_ATTATCHED, ia); if (ia) setFlag((XOverlaItemFlags)(XITEM_SCREEN_ATTATCHED|XITEM_RADEC_ATTATCHED), false);}
    inline bool screenAttatched(){return mFlags & XITEM_SCREEN_ATTATCHED;}
    inline void setScreenAttatched(bool sa){setFlag(XITEM_SCREEN_ATTATCHED, sa); if (sa) setFlag((XOverlaItemFlags)(XITEM_IMAGE_ATTATCHED|XITEM_RADEC_ATTATCHED), false);}
    inline bool growthsRadial(){return mFlags & XITEM_GROWMODE_RADIAL;}
    inline bool growthRectangular(){return !growthsRadial();}
    static inline void setLastMousePos(QPoint p){mLastMousePos = p;}
    static inline QPoint lastMousePos(){return mLastMousePos;}
    static XOverlayItem* parseItem(QString& cad);

    //Otros métodos de control y dibujado
    //************************************************************************
    Resizebox overResizeBox(int x, int y);          //Indica si el cursor se encuentra sobre un acaja de redimensionado
    void paintLabel(QPainter* painter);

    //Métodos virtuales abstractos
    //************************************************************************
    virtual void paint(QPainter* painter) = 0;      //Dibuja el objeto
    virtual QRectF rect() = 0;                       //Rectángulo que contiene al completo el objeto
    virtual QRectF rectPad() = 0;                    //Rectángulo externo para considerar que estamos dentro
    virtual bool inside(int x, int y) = 0;          //Comprueba si un puto cae dentro del objeto
    virtual void fitIn(QRect rct, QRect sqr) = 0;   //Encaja este item dentro del rectángulo dado redimensionándolo
    virtual void fitInRadius(float radius) = 0;     //Hace crecer/menguar el item para que se ajuste a un radio determinado

signals:


public slots:


};

#endif // XOVERLAYITEM_H
