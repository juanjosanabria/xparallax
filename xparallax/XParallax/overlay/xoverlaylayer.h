#ifndef XOVERLAYLAYER_H
#define XOVERLAYLAYER_H

#include <QObject>
#include <QWidget>
#include <QList>
#include <QWheelEvent>
#include <QMouseEvent>
#include "xoverlayitem.h"
#include "xoverlaystyle.h"

//Definición de funciones de coordenadas de imagen a coordenadas de pantalla
typedef void (* IMAGE_XY_TO_SCREEN_XY_FN)(QObject* obj, float imgx, float imgy, QPointF& scrxy);
typedef void (* SCREEN_XY_TO_IMAGE_XY_FN)(QObject* obj, float imgx, float imgy, QPointF& scrxy);
//Definición de funciones de coordenadas RA-DEC a coordenadas de imagen
typedef void (* RADEC_MAP_FN)(QObject* obj, double x, double y, double* ra, double* lat);
typedef void (* RADEC_IMAP_FN)(QObject* obj, double ra, double lat, double* x, double* y);

class XOverlayLayer : public QObject                                //Hereda de QObject para poder manejar eventos
{
    Q_OBJECT

private:
    bool                            mVisible;                       //Indica si la capa es o no visible
    QWidget*                        mWindow;                        //Ventana o widget sobre el que se dibuja esta capa
    QList<XOverlayItem*>            mItems;                         //Lista de items a dibujar

    float                           mZoom;                          //Factor de zoom en tanto por 1 para elementos anclados al zoom

    XOverlayItem*                   mPendingLabel;                  //Etiqueta pendiente de redibujado en primer plano
    XOverlayItem*                   mPendingitem;                   //Item pendiente de redibujado
    XOverlayItem*                   mResizeItem;                    //Item que se está redimensionando
    QPoint                          mResizeStartpoint;              //Punto pinchado cuando comenzó el redimensionado
    QRectF                          mResizeStartRect;               //Rectángulo que ocupaba el item antes de comenzar el redimensionado
    QRectF                          mClipRect;                      //Una capa puede tener un rectángulo de recortado

    //Hay que revisar estos elementos re redimensionado
    Resizebox                       mResizeBox;                     //Caja de redimensionado
    QPoint                          mResizeDiff;                    //Diferencia de posición del cursor respecto de la posición del objeto

    XOverlayItem*                   mMovingItem;                    //Item que se está moviendo
    QPoint                          mMovePointDiff;                 //Diferencia de posición entre el centro y el click del ratón
    QHash<quint8,XOverlayStyle*>    mStyles;                        //Almacena los estilos de los distintos grupos

    //Funciones de uso interno
    //*************************************************************************************************************
    XOverlayItem*               closestInside(int px, int py);  //Busca el item más cercano que incluye ese punto

    //Funciones de referenciado, transformación de coordenadas ECUATORIAL --> IMAGEN --> PANTALLA y viceversa
    //*************************************************************************************************************
    IMAGE_XY_TO_SCREEN_XY_FN    mFunc_ImageXYToScreenXY;
    SCREEN_XY_TO_IMAGE_XY_FN    mFunc_ScreenXYToImageXY;
    RADEC_MAP_FN                mFunc_Map;
    RADEC_IMAP_FN               mFunc_Imap;

public:
    explicit XOverlayLayer(const QString& name = "", QObject *parent = 0);
    virtual ~XOverlayLayer();

    inline void setFn(IMAGE_XY_TO_SCREEN_XY_FN img2Scr, SCREEN_XY_TO_IMAGE_XY_FN scr2Img, RADEC_MAP_FN map, RADEC_IMAP_FN imap)
    {mFunc_ImageXYToScreenXY = img2Scr; mFunc_ScreenXYToImageXY = scr2Img; mFunc_Map = map; mFunc_Imap = imap; if (mWindow) mWindow->update();}
    inline bool mapped(){return mFunc_ImageXYToScreenXY && mFunc_ScreenXYToImageXY && mFunc_Map && mFunc_Imap;}
    void addItem(XOverlayItem* it,quint8 group = 255 /* El item ya puede venir con un grupo */);
    void clearItems(quint8 group = 255);
    void removeItem(XOverlayItem* it, bool delete_it=true);
    XOverlayItem* overItem(quint8 group=255); /** Busca el item sobre el que está el cursor **/
    inline QList<XOverlayItem*>* items(){return &mItems;}

    inline QString name() const{return objectName();}
    inline bool visible(){return mVisible;}
    inline void setVisible(bool vis)
    {if(vis == mVisible) return; if((mVisible = vis)) imageMoveEvent(); else if (mPendingLabel) mPendingLabel = NULL;}
    inline void show(){setVisible(true);}
    inline void hide(){setVisible(false);}
    inline QWidget* window(){return mWindow;}
    inline void setWindow(QWidget* win){mWindow = win;}
    inline float zoom(){return mZoom;}
    inline void setZoom(float nz){ zoomChangeEvent(nz); }

    inline bool pending(){return mPendingLabel;}
    inline void paintPending(QPainter* pt) { if(mPendingLabel) mPendingLabel->paintLabel(pt); if (mPendingitem) mPendingitem->paint(pt); }

    inline QRectF clipRect(){return mClipRect;}
    inline bool hasClipping(){return mClipRect.width() > 0.01;}
    inline void setClipRect(QRectF rct){mClipRect = rct;}

    //Métodos que deben ser llamados por la ventana que gestiona esta capa
    //************************************************************************
    virtual bool paintEvent(QPaintEvent*, QPainter*);           /** Se produce un evento de dibujado en la ventana padre **/
    virtual bool wheelEvent(QWheelEvent*);
    virtual bool imageMoveEvent();                              /** La imagen se mueve. Implementado */
    virtual bool mousePressEvent(QMouseEvent* );                /** Se pulsa algún botón del ratón. Implementado **/
    virtual bool mouseReleaseEvent(QMouseEvent *);              /** Implementado **/
    virtual bool mouseMoveEvent(QMouseEvent *);                 /** Implementado **/
    virtual bool resizeEvent(QResizeEvent *);
    virtual bool contextMenuRequest(QPoint);
    virtual void zoomChangeEvent(float newzoom);                /** Cambia el zoom de pantalla**/
    virtual bool keyPressEvent(QKeyEvent* ke);                  /** Se pulsa una tecla **/

    //Métodos para el acceso a los estilos de cada grupo
    //************************************************************************
    inline QHash<quint8,XOverlayStyle*>*   styles(){return &mStyles;}
    XOverlayStyle*                         addStyle(quint8 group=0);
    void                                   setGroupStyle(quint8 group, XOverlayStyle* st);

signals:
    void MouseEnterItem_Signal(XOverlayLayer* ly, XOverlayItem* it);
    void MouseLeaveItem_Signal(XOverlayLayer* ly, XOverlayItem* it);
    void MousePressItem_Signal(XOverlayLayer* ly, XOverlayItem* it, QMouseEvent* me);
    void MouseReleaseItem_Signal(XOverlayLayer* ly, XOverlayItem* it, QMouseEvent* me);
    void ContextMenu_Signal(XOverlayLayer* ly, XOverlayItem* it, QMouseEvent* me);

public slots:



};

#endif // XOVERLAYLAYER_H
