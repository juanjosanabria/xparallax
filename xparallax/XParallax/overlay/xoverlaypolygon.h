#ifndef XOVERLAYPOLYGON_H
#define XOVERLAYPOLYGON_H

#include "xoverlay.h"

class XOverlayPolygon
        : public XOverlayItem
{
private:
    QVector<QPointF>    mPoly;

public:
    explicit XOverlayPolygon(XOverlayLayer* layer = 0, quint8 group = 0);

    int length(){return mPoly.count();}
    QPointF* points(){return mPoly.data();}
    void setPoly(const QVector<QPointF>& poly);
    void append(QPointF p){mPoly.append(p);}
    void removeAt(int i){mPoly.removeAt(i);}

    virtual void paint(QPainter* painter);
    virtual QRectF rect();                                      //Rectángulo que contiene al completo el objeto
    virtual QRectF rectPad();                                   //Rectángulo externo para considerar que estamos dentro
    virtual bool inside(int x, int y);
    virtual void fitIn(QRect /*rct*/, QRect /*sqr*/){;}   //Encaja este item dentro del rectángulo dado redimensionándolo
    virtual void fitInRadius(float /*radius*/){;}     //Hace crecer/menguar el item para que se ajuste a un radio determinado

};

#endif // XOVERLAYPOLYGON_H
