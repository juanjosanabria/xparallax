#ifndef XOVERLAYSTYLE_H
#define XOVERLAYSTYLE_H

#include <QObject>
#include <QPainter>

/**
 * @brief The XOverlayStyle class Identifica un estilo aplicado a elementos XOverlayItem.
 * Cuando un item tome un estilo lo tomará generalmente según el grupo al que pertenece.
 * Un item puede tener estilos simples (solo para él) y se ocupará de destruir el estilo o
 * estilos compartidos, que son asginados en función de su grupo.
 *
 */

enum XOverlayTetxAttatch{
    //Punto de anclaje a la etiqueta
    XA_PARENT_UPPER_LEFT   = 1<<0,
    XA_PARENT_UPPER_RIGHT  = 1<<1,
    XA_PARENT_LOWER_RIGHT  = 1<<2,
    XA_PARENT_LOWER_LEFT   = 1<<3,

    //Punto de control de la caja de texto, de momento no están implementados
    XA_LABEL_UPPER_LEFT    = 1<<4,      //Esquina superior izquierda
    XA_LABEL_UPPER_RIGHT   = 1<<5,      //Esquina superior derecha
    XA_LABEL_UPPER_MIDDLE  = 1<<6,      //Mitad del segmento superior
    XA_LABEL_LOWER_LEFT    = 1<<7,      //Esquina inferior izquierda
    XA_LABEL_LOWER_RIGHT   = 1<<8,      //Esquina inferior derecha
    XA_LABEL_LOWER_MIDDLE  = 1<<9,      //Mitad del segmento inferior
    XA_LABEL_LEFT_MIDDLE   = 1<<10,     //Mitad del segmento izquierdo
    XA_LABEL_RIGHT_MIDDLE  = 1<<11,     //Mitad del segmento derecho

    //Otros elementos de control
    XA_PAINT_CONNECTOR     = 1<<12,     //Se dibujará la línea conectora hasta el punto de conexionado
};


class XOverlayStyle
{

private:
    //Internos del estilo
    //************************************************************************
    QString                 mName;                 //Nombre del estilo
    bool                    mIsForSingleObject;    //Indica que el estilo se aplica solamente a un item y debe ser destruido con él

    //Dibujo del item, márgenes, colores, fuentes
    //************************************************************************
    QBrush                  mBackBrush;         //Relleno de fondo
    QBrush                  mHoverBackBrush;    //Relleno de fondo cuando el item está hover
    QPen                    mBorder;            //Estilo del borde
    QPen                    mHoverBorder;       //Estilo del borde cuando el item está hover

    QPen                    mBoxpen;

    QBrush                  mForeBack;          //Color del fondo cuando se dibujan etiquetas
    QPen                    mForeground;        //Color de la fuente
    QFont                   mFont;              //Tipo de fuente para mostrar cualquier tipo de texto
    QPen                    mForeBorder;        //Borde para el cuadro que contiene la fuente
    XOverlayTetxAttatch     mTextAttatch;       //Tipo de anclaje del texto

    quint8                  mLabelDist;         //Distancia a la etiqueta en píxeles
    quint8                  mPadding;           //Número de píxeles de padding
    quint8                  mHoverPad;          //Padding externo para considerar que se encuentra dentro del elemento

    //Estáticos del estilo
    //************************************************************************
    static XOverlayStyle   DEFAULT_STYLE;      //Estilo por defecto que deberían coger todos los elementos

public:
    XOverlayStyle(bool single=false);
    ~XOverlayStyle();
    static inline XOverlayStyle* defaultStyle(){return &DEFAULT_STYLE;}

    //Generales del estilo
    //************************************************************************
    inline QString              name() const{return mName;}
    inline void                 setName(QString& nm){ mName = nm;}
    inline bool                 single(){return mIsForSingleObject;}
    inline void                 setSingle(bool sing){mIsForSingleObject = sing;}
    inline quint8               labelDist(){return mLabelDist;}
    inline void                 setLabelDist(quint8 dist){mLabelDist = dist;}
    inline XOverlayTetxAttatch  textAttatch(){return mTextAttatch;}
    inline void                 setTextAttatch(XOverlayTetxAttatch att){mTextAttatch = att;}

    //Relleno y fondo
    //************************************************************************
    inline QColor   backgroundColor() const{ return mBackBrush.color();}
    inline QBrush   backgroundBrush() const{return mBackBrush;}
    inline void     setBackground(const QColor &bkg){mBackBrush = QBrush(bkg); }
    inline bool     backTransparent() const { return !mBackBrush.color().alpha(); }

    inline QColor   hoverBackgroundColor() const{ return mHoverBackBrush.color();}
    inline QBrush   hoverBackgroundBrush() const{return mHoverBackBrush;}
    inline void     setHoverBackground(const QColor &bkg){mHoverBackBrush = QBrush(bkg);}
    inline bool     hoverBackTransparent() const { return !mHoverBackBrush.color().alpha(); }

    //Borde y líneas
    //************************************************************************
    inline void     setBorder(QColor col,int width, Qt::PenStyle style){mBorder = QPen(col, width, style);}
    inline QColor   borderColor() const{ return mBorder.color();}
    inline void     setBorderColor(const QColor &brd){mBorder = QPen(brd,mBorder.width(), mBorder.style());}
    inline QPen     borderPen() const { return mBorder; }
    inline bool     borderTransparent() const { return !mBorder.color().alpha(); }
    inline int      borderWidth() const {return mBorder.width();}
    inline void     setBorderWidth(int wdth){mBorder = QPen(mBorder.color(), wdth, mBorder.style());}

    inline void     setHoverBorder(QColor col,int width, Qt::PenStyle style){mHoverBorder = QPen(col, width, style);}
    inline QColor   hoverBorderColor() const {return mHoverBorder.color();}
    inline void     setHoverBorderColor(const QColor &brd){mHoverBorder = QPen(brd,mBorder.width(), mBorder.style());}
    inline QPen     hoverBorderPen() const {return mHoverBorder; }
    inline bool     hoverBorderTransparent() const { return !mHoverBorder.color().alpha(); }

    inline void     setBoxBorder(QColor col,int width, Qt::PenStyle style){mBoxpen = QPen(col, width, style);}
    inline QColor   boxBorderColor() const {return mBoxpen.color();}
    inline void     setBoxBorderColor(const QColor &brd){mBoxpen = QPen(brd,mBorder.width(), mBorder.style());}
    inline QPen     boxBorderPen() const {return mBoxpen; }
    inline bool     boxBorderTransparent() const { return !mBoxpen.color().alpha(); }

    //Fuentes
    //************************************************************************
    inline QColor   foreColor() const{ return mForeground.color();}
    inline void     setForeColor(const QColor &fgd){mForeground = QPen(fgd, mForeground.width(), mForeground.style());}
    inline QPen     forePen() const { return mForeground; }
    inline QFont    font() const{ return mFont; }
    inline void     setFont(const QFont &fnt){mFont = fnt;}
    inline bool     foreTransparent(){return !mForeground.color().alpha();}
    inline QColor   foreBack() const {return mForeBack.color();}
    inline void     setForeBackColor(const QColor &fgd){mForeBack = QBrush(fgd);}
    inline QBrush   foreBrush() const {return mForeBack;}
    inline bool     foreBackTransparent(){return !mForeBack.color().alpha();}

    inline void     setForeBorder(const QColor &fb){mForeBorder = QPen(fb, mForeBorder.width(), mForeBorder.style());}
    inline void     setForeBorder(const QPen &fb){mForeBorder = fb;}
    inline bool     foreBorderTransparent(){return !mForeBorder.color().alpha();}

    //Espaciado, márgenes y dibujo
    //************************************************************************
    inline quint8   padding() const {return mPadding;}
    inline void     setPadding(quint8 pad){mPadding = pad;}
    inline quint8   hoverPadding() const {return mHoverPad;}
    inline void     setHoverPadding(quint8 pad){mHoverPad = pad;}
};

#endif // XOVERLAYSTYLE_H
