#ifndef XOVERLAY_H
#define XOVERLAY_H

//Tipos de elementos
//*************************************************************************************************
#define XTYPE_CIRCLE                (quint8)1
#define XTYPE_CROSS                 (quint8)2
#define XTYPE_SEGMENT               (quint8)3
#define XTYPE_LABEL                 (quint8)4
#define XTYPE_RECT                  (quint8)5
#define XTYPE_POLYGON               (quint8)6

//Defines de los tipos de objetos que podemos posicionar sobre las capas
//*************************************************************************************************
#define  XGROUP_NOT_DEFINED         0
#define  XGROUP_DETECTED_STAR       1
#define  XGROUP_CATALOG_STAR        4
#define  XGROUP_FIXED_LABEL         5
#define  XGROUP_EPHEMERID           6


//Nombres de las capas de dibujado y sus prioridades
//*************************************************************************************************
#define     XLAYER_MESH                         "RA-DEC mesh"
#define     XLAYER_MESH_PRIORITY                0
#define     XLAYER_INFORMATION                  "Information"
#define     XLAYER_INFORMATION_PRIORITY         1

#define     XLAYER_USER_DRAWING                 "User drawing"
#define     XLAYER_USER_DRAWING_PRIORITY        2
#define     XGROUP_TOOL_DISTANCE                1
#define     XGROUP_TOOL_GIF                     1
#define     XGROUP_MOVING_OBJECTS               2
#define     XGROUP_TOOL_LOCATE                  3

#define     XLAYER_DETECTED_STARS               "Detected stars"
#define     XLAYER_DETECTED_STARS_PRIORITY      3
#define     XGROUP_DETECTED_STAR                1
#define     XGROUP_DEBLENDED_STAR               2

#define     XLAYER_ASTROMETRY                   "Astrometric reduction"
#define     XLAYER_ASTROMETRY_PRIORITY          4
#define     XGROUP_ASTROMETRY_REFERENCE         0
#define     XGROUP_ASTROMETRY_REJECTED          1
#define     XGROUP_ASTROMETRY_ALIGN             2

#define     XLAYER_DOWNLAODED_STARS             "Downloaded catalog stars"
#define     XLAYER_DOWNLOADED_STARS_PRIORITY    5

#define     XLAYER_MPC_KNOWN_OBJECTS            "MPC Known objects"
#define     XLAYER_MPC_KNOWN_OBJECTS_PRIORITY   6

#define     XLAYER_MPC_REPORT                   "MPC Report objects"
#define     XLAYER_MPC_REPORT_PRIORITY          5
#define     XGROUP_MPC_REPORT_MEASURED          1   //El objeto ha sido medido e incluido en el informe
#define     XGROUP_MPC_REPORT_MEASURED_HOVER    2   //Marca que aparecerá sobre el objeto cuando se pase el ratón
#define     XGROUP_MPC_REPORT_MISS              3   //Aquí debería haber un objeto pero no lo hay


//Clases principales
//*************************************************************************************************
#include "xoverlaylayer.h"          //Capas
#include "xoverlayitem.h"           //Items contenidos en las capas
#include "xoverlaystyle.h"          //Estilos aplicados a los items


//Tipos de items dibujables
//**************************************************************************************************
#include "xoverlaylabel.h"          //Etiqueta de texto simple
#include "xoverlaycircle.h"         //Representa un círculo con un radio centrado en un punto
#include "xoverlaycross.h"          //Cruces de distintos tipos para marcar puntos de manera precisa
#include "xoverlaysegment.h"        //Dibujar líneas simples en pantalla
#include "xoverlayrect.h"           //Rectángulos ortogonales a los bordes

#endif // XOVERLAY_H
