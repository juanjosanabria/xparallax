#include "xoverlay.h"
#include "xoverlaysegment.h"
#include "util.h"
#include "core.h"
#include "geometry.h"
#include "matcher.h"

//Constantes usadas por el clipping
const int INSIDE = 0; // 0000
const int LEFT = 1;   // 0001
const int RIGHT = 2;  // 0010
const int BOTTOM = 4; // 0100
const int TOP = 8;    // 1000

XOverlaySegment::XOverlaySegment(XOverlayLayer *layer, quint8 group)
    :XOverlayItem(XTYPE_SEGMENT, layer, group)
{
    mArrowHead = 0;
}

void XOverlaySegment::paint(QPainter* painter)
{
    if (!visible()) return;
    painter->setPen( (hoverable() && hover()) ? style()->hoverBorderPen() : style()->borderPen() );
    painter->drawLine(mStart, mEnd);
    //Ángulo que forma el segmento con la horizontal
    double angle_ss = Geometry::SegmentAngle_AH(mStart, mEnd);
    //Si tenemos cabeza de flecha tenemos que pintarla
    if (mArrowHead)
    {
        QPointF p1,p2;
        int ah_len = mArrowHead&(~SEGMENT_ARROWHEAD_BOTH);
        if (mArrowHead & SEGMENT_ARROWHEAD_START)
        {
            p1 = mStart;
            p2 = QPointF(mStart.x()+ah_len, mStart.y());
            Geometry::RotateSegment(p1, p2, angle_ss + 45);
            painter->drawLine(p1, p2);

            p1 = mStart;
            p2 = QPointF(mStart.x()+ah_len, mStart.y());
            Geometry::RotateSegment(p1, p2, angle_ss - 45);
            painter->drawLine(p1, p2);
        }
        if (mArrowHead & SEGMENT_ARROWHEAD_END)
        {
            p1 = mEnd;
            p2 = QPointF(mEnd.x()-ah_len, mEnd.y());
            Geometry::RotateSegment(p1, p2, angle_ss + 45);
            painter->drawLine(p1, p2);

            p1 = mEnd;
            p2 = QPointF(mEnd.x()-ah_len, mEnd.y());
            Geometry::RotateSegment(p1, p2, angle_ss - 45);
            painter->drawLine(p1, p2);
        }
    }
    //Si está seleccionado mostramos un cuadro punteado alrededor
    if ((flags() & XITEM_SELECTED) && (!style()->boxBorderTransparent()))
    {
        painter->setPen(style()->boxBorderPen());
        painter->drawPolygon(poly(), 4);
    }
}


void XOverlaySegment::ComputePoly()
{
    //Ángulo con la horizontal y longitud del segmento
    double angle_ss = Geometry::SegmentAngle_AH(mStart, mEnd);
    double length = CART_DIST(mStart.x(), mStart.y(), mEnd.x(), mEnd.y());

    QPointF pz = QPointF(0, 0);
    mPoly[0] = QPointF(0, style()->padding()/2);
    Geometry::RotateSegment(pz, mPoly[0], angle_ss, QPointF(0,0));

    pz = QPointF(length, 0);
    mPoly[1] = QPointF(length, style()->padding()/2);
    Geometry::RotateSegment(pz, mPoly[1], angle_ss, QPointF(0,0));

    pz = QPointF(length, 0);
    mPoly[2] = QPointF(length, -style()->padding()/2);
    Geometry::RotateSegment(pz, mPoly[2], angle_ss, QPointF(0,0));

    pz = QPointF(0, 0);
    mPoly[3] = QPointF(0, -style()->padding()/2);
    Geometry::RotateSegment(pz, mPoly[3], angle_ss, QPointF(0,0));

    //Desplazamos todos los puntos calculados
    for (int i=0; i<4; i++)
        mPoly[i] = QPointF(mPoly[i].x() + mStart.x(), mPoly[i].y() + mStart.y());

    //Digamos que la posición será el punto central del segmento
    QPointF pzz = QPointF(0, 0);
    pz = QPointF(length/2.0, 0);
    Geometry::RotateSegment(pzz, pz, angle_ss, QPointF(0,0));
    setPos(pz.x() + mStart.x(), pz.y() + mStart.y());
}

QRectF XOverlaySegment::rect()
{
    //Este método no es relevante para un segmento, ya que el rectángulo que lo contiene está girado
    return QRectF(MINVAL(mStart.x(), mEnd.x()), MINVAL(mStart.y(), mEnd.y()),
                  fabs(mEnd.x() - mStart.x()), fabs(mEnd.y() - mStart.y()));
}

QRectF XOverlaySegment::rectPad()
{
    return rect();
}

bool XOverlaySegment::inside(int x, int y)
{
    return Geometry::PointInPolygon(QPointF(x, y), poly(), 4);
}

void XOverlaySegment::fitIn(QRect, QRect)
{

}

void XOverlaySegment::fitInRadius(float)
{

}

/**
 * @brief XOverlaySegment::clip Recorta el segmento dentro del rectángulo especificado usando el
 * algoritmo de Cohen–Sutherland de complejidad O(log N)
 * @param rct
 * @return true si hay alguna parte del segmento visible. False si no hay ninguna parte visible
 */
bool XOverlaySegment::clip(QRectF& rct)
{
    double x0 = mStart.x();
    double y0 = mStart.y();
    double x1 = mEnd.x();
    double y1 = mEnd.y();
    bool ok = CohenSutherlandLineClip(rct, x0, y0, x1, y1);
    if (!ok)
    {
        setVisible(false);
        return false;
    }
    mStart = QPointF(x0, y0);
    mEnd = QPointF(x1, y1);
    return ok;
}


// Cohen–Sutherland clipping algorithm clips a line from
// P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with
// diagonal from (xmin, ymin) to (xmax, ymax).
bool XOverlaySegment::CohenSutherlandLineClip(QRectF& rct, double &x0, double &y0, double &x1, double &y1)
{
    // compute outcodes for P0, P1, and whatever point lies outside the clip rectangle
    int outcode0 = ComputeOutCode(rct, x0, y0);
    int outcode1 = ComputeOutCode(rct, x1, y1);
    bool accept = false;

    while (true) {
        if (!(outcode0 | outcode1))
        { // Bitwise OR is 0. Trivially accept and get out of loop
            accept = true;
            break;
        }
        else if (outcode0 & outcode1)
        { // Bitwise AND is not 0. Trivially reject and get out of loop
            break;
        }
        else
        {
            // failed both tests, so calculate the line segment to clip
            // from an outside point to an intersection with clip edge
            double x, y;

            // At least one endpoint is outside the clip rectangle; pick it.
            int outcodeOut = outcode0 ? outcode0 : outcode1;

            // Now find the intersection point;
            // use formulas y = y0 + slope * (x - x0), x = x0 + (1 / slope) * (y - y0)
            if (outcodeOut & TOP) {           // point is above the clip rectangle
                x = x0 + (x1 - x0) * (rct.bottom() - y0) / (y1 - y0);
                y = rct.bottom();
            } else if (outcodeOut & BOTTOM) { // point is below the clip rectangle
                x = x0 + (x1 - x0) * (rct.y() - y0) / (y1 - y0);
                y = rct.y();
            } else if (outcodeOut & RIGHT) {  // point is to the right of clip rectangle
                y = y0 + (y1 - y0) * (rct.right() - x0) / (x1 - x0);
                x = rct.right();
            } else if (outcodeOut & LEFT) {   // point is to the left of clip rectangle
                y = y0 + (y1 - y0) * (rct.x() - x0) / (x1 - x0);
                x = rct.x();
            }

            // Now we move outside point to intersection point to clip
            // and get ready for next pass.
            if (outcodeOut == outcode0)
            {
                if (COMPF3(x0, x, 0.0001) == 0 && COMPF3(y0, y, 0.0001) == 0)
                    outcode0 = 0;
                else
                {
                    x0 = x; y0 = y;
                    outcode0 = ComputeOutCode(rct, x0, y0);
                }
            }
            else
            {
                if (COMPF3(x1, x, 0.0001) == 0 && COMPF3(y1, y, 0.0001) == 0)
                    outcode1 = 0;
                else
                {
                    x1 = x;  y1 = y;
                    outcode1 = ComputeOutCode(rct, x1, y1);
                }
            }
        }
    }
    return accept;
}

// Compute the bit code for a point (x, y) using the clip rectangle
// bounded diagonally by (rct.x(), rct.y()), and (rct.right(), rct.bottom())
int XOverlaySegment::ComputeOutCode(QRectF& rct, double sx, double sy)
{
    int code;
    code = INSIDE;          // initialised as being inside of clip window
    if (sx < rct.x())           // to the left of clip window
        code |= LEFT;
    else if (sx > rct.right())      // to the right of clip window
        code |= RIGHT;
    if (sy < rct.y())           // below the clip window
        code |= BOTTOM;
    else if (sy > rct.bottom())      // above the clip window
        code |= TOP;
    return code;
}
















