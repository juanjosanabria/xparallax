#include "xoverlay.h"
#include "xoverlaycircle.h"
#include "core.h"
#include <QStyleHintReturn>
#include <QGraphicsEllipseItem>

XOverlayCircle::XOverlayCircle(XOverlayLayer* layer, quint8 group) :
    XOverlayItem(XTYPE_CIRCLE, layer, group)
{
    mRadius = 10;
}


void  XOverlayCircle::paint(QPainter* painter)
{
    if (!visible()
            //Si no estamos encima o no es hoverable y todo es transparente
            || ((!hover() || !hoverable()) && style()->borderTransparent() && style()->backTransparent())
            //O si es hoverable y está en estado hover y hay algo que pintar
            || (hoverable() && hover() && style()->hoverBorderTransparent() && style()->hoverBackTransparent() )
            ) return;
    painter->setPen(hoverable() && hover() ? style()->hoverBorderPen() : style()->borderPen());
    painter->setBrush(hoverable() && hover() ? style()->hoverBackgroundBrush() : style()->backgroundBrush());
    painter->drawEllipse(pos(), mRadius * zoom(), mRadius * zoom());
    if (hover() && resizable()) paintResizeBox(painter);
}

QRectF XOverlayCircle::rect()
{
    float z = zoom();
    return QRectF(x() - mRadius * z, y() - mRadius * z, mRadius * 2 * z, mRadius * 2 * z);
}

QRectF XOverlayCircle::rectPad()
{
    float szrec = (mRadius + style()->hoverPadding()) * zoom();
    return QRectF(x() - szrec, y() - szrec, szrec * 2, szrec * 2);
}

bool  XOverlayCircle::inside(int px, int py)
{
    float xrad = (mRadius + style()->hoverPadding()) * zoom();
    if (resizable())
    {
        QRectF rct = appendResizeBox(rect());
        return rct.contains(px, py);
    }
    else return (CART_DIST(px, py, x(), y()) < xrad);
}

void XOverlayCircle::fitIn(QRect /*rct*/, QRect sqr)
{

    setX(sqr.x() + sqr.width() / 2.0);
    setY(sqr.y() + sqr.height() / 2.0);
    mRadius = MINVAL(sqr.width()/2.0, sqr.height()/2.0);
}

void XOverlayCircle::fitInRadius(float radius)
{
    mRadius = radius / zoom();
}






















