#include "xoverlay.h"
#include "xoverlaylayer.h"
#include "core.h"

XOverlayLayer::XOverlayLayer(const QString& name, QObject *parent) :
    QObject(parent)
{
    this->setObjectName(name);
    mVisible = true;
    mWindow = NULL;
    mMovingItem = NULL;
    mResizeItem = NULL;
    mResizeBox = RESIZEBOX_NONE;
    mZoom = 1.0f;
    mClipRect = QRectF(0, 0, 0, 0);

    //Funciones de transformación se establecen a NULL
    mFunc_ImageXYToScreenXY = NULL;
    mFunc_ScreenXYToImageXY = NULL;
    mFunc_Map = NULL;
    mFunc_Imap = NULL;
    mPendingLabel = mPendingitem = NULL;
}

void XOverlayLayer::addItem(XOverlayItem* it,quint8 group)
{
    int idx = mItems.indexOf(it);
    if (idx == -1 && it->group() == group) return;
    if (group == 255) group = it->group();
    else it->setGroup(group);
    if (idx == -1) mItems.append(it);
    it->setLayer(this);
    //Buscamos el estilo del grupo si no estuviera creamos uno nuevo
    if (!mStyles.contains(group))
       it->setStyle(addStyle(group));
    else
       it->setStyle(mStyles[group]);
}

void XOverlayLayer::clearItems(quint8 group)
{
    if (group == 255)
    {
        for (int i=0; i<mItems.count(); i++)
            delete mItems[i];
        mItems.clear();
    }
    else
    {
        for (int i=0; i<mItems.count(); i++)
        {
            if (mItems[i]->group() == group)
            {
                delete mItems[i];
                mItems.removeAt(i);
                i--;
            }
        }
    }
}
XOverlayItem* XOverlayLayer::overItem(quint8 group)
{
    //El valor 255 indica cualquier grupo
    for (int i=0; i<mItems.count(); i++)
    {
        XOverlayItem* it = mItems[i];
        if (!it->visible() || (group != 255 && it->group() != group)) continue;
        if (it->hover()) return it;
    }
    return NULL;
}

void XOverlayLayer::removeItem(XOverlayItem* it, bool delete_it)
{
    if (mItems.contains(it))
        mItems.removeOne(it);
    if (delete_it) delete it;
}

void XOverlayLayer::setGroupStyle(quint8 group, XOverlayStyle* st)
{
    XOverlayStyle* old = NULL;
    if (mStyles.contains(group) && (st != mStyles[group])) (old = mStyles[group]);
    mStyles.remove(group);
    mStyles[group] = st;
    for (int i=0; i<mItems.count(); i++)
        if (mItems[i]->group() == group && mItems[i]->style() == old)
            mItems[i]->setStyle(st);
    if (old && old != st && old != XOverlayStyle::defaultStyle()) delete old;
}

XOverlayStyle*  XOverlayLayer::addStyle(quint8 group)
{
    XOverlayStyle* old = NULL;
    if (mStyles.contains(group)) (old = mStyles[group]);
    XOverlayStyle* news = new XOverlayStyle();
    mStyles[group] = news;
    for (int i=0; i<mItems.count(); i++)
        if (mItems[i]->group() == group || mItems[i]->style() == old)
            mItems[i]->setStyle(news);
    if (old && old != XOverlayStyle::defaultStyle()) delete old;
    return news;
}

XOverlayLayer::~XOverlayLayer()
{
    //Eliminar todos los items
    for (int i=0; i<mItems.count(); i++)
        delete mItems[i];
    mItems.clear();
    //Eliminar todos los estilos
    QList<quint8> keys = mStyles.keys();
    for(int i=0;i<keys.count(); i++)
    {
        XOverlayStyle* st = mStyles[keys[i]];
        delete st;
    }
}

bool XOverlayLayer::paintEvent(QPaintEvent* pe, QPainter* painter)
{
    if (!visible() || !mWindow) return true;
    mPendingLabel = mPendingitem = NULL;                    //Item con etiqueta sobre el que estamos posicionado y que solo aparece en hover
    if (hasClipping()) painter->setClipRect(mClipRect);
    for (int i=0; i<mItems.count(); i++)
    {
        XOverlayItem* it = mItems[i];
        if (!it->visible()) continue;                       //A nivel de item también existe el ocultar

        if (!it->alwaysOnTop())  it->paint(painter);        //Solamente puede haber un item AlwaysOnTop
        else mPendingitem = it;                             //El Item está pendiente de redibujado

        if (it->hasLabel())                                 //Las etiquetas también se muestran ssiempre always On Top
        {
            if (!(it->flags() & XITEM_LABEL_HOVER))
                it->paintLabel(painter);                    //La etiqueta siempre se muestra
            else if (!mPendingLabel  && it->hover())
                mPendingLabel = it;                         //Solo se muestra si estamos encima
        }
    }
    //Restauro la zona de dibujado
    if (hasClipping()) painter->setClipRect(pe->rect());
    return true;
}

XOverlayItem* XOverlayLayer::closestInside(int px, int py)
{
    XOverlayItem* press_item = NULL;
    float press_dist = 10000000000.0f;
    for (int i=0; i<mItems.count(); i++)
    {
        XOverlayItem* it = mItems.at(i);
        if (it->inside(px, py))
        {
            float dist = CART_DIST(it->x(), it->y(), px, py);
            if (dist < press_dist)
            {
                press_item = it;
                press_dist = dist;
            }
        }
    }
    return press_item;
}

bool XOverlayLayer::mouseMoveEvent(QMouseEvent *ev)
{
    XOverlayItem::setLastMousePos(ev->pos());
    //Cuidado, si estoy moviendo un item no hago otra cosa que moverlo
    if (mMovingItem)
    {
       mMovingItem->setPos(ev->x() + mMovePointDiff.x(), ev->y() + mMovePointDiff.y());
       if (mWindow && mWindow->isVisible()) mWindow->update();
       return false;
    }
    //Si estoy redimensionando un item
    if (mResizeItem)
    {
        //De momento solamente tengo implementado crecimientos radiales
        if (mResizeItem->growthsRadial())
        {
            //Centro del item
            QPointF center = QPointF(mResizeStartRect.x() + mResizeStartRect.width()/2, mResizeStartRect.y() + mResizeStartRect.height() / 2);
            //Distancia al borde del rectángulo (radio)
            float initial_radius = (mResizeStartRect.right() - mResizeStartRect.x()) / 2.0f;
            //Distancia al centro del punto de inicio
            float dist_ini_center = CART_DIST(mResizeStartpoint.x(), mResizeStartpoint.y(), center.x(), center.y());
            //Diferencia que tiene que haber siempre del punto donde está el cursor al borde
            float req_dist = dist_ini_center - initial_radius;
            //Distancia al centro del punto actual
            float dist_act_center = CART_DIST(ev->x(), ev->y(), center.x(), center.y());
            //Distancia del punto de partida al borde del rectángulo que contenía
            float new_radius = dist_act_center - req_dist;
            mResizeItem->fitInRadius(new_radius);
        }
        mWindow->update();
        return false;
    }

    bool hover_change = false;
    int x = ev->x();
    int y = ev->y();
    for (int i=0; i<mItems.count(); i++)
    {
        XOverlayItem* it = mItems.at(i);
        if (it->inside(x, y))
        {
            bool last_hover = it->hover();
            it->setHover(true);
            if (!last_hover && it->visible())
            {
                hover_change = true;
                emit MouseEnterItem_Signal(this, it);
            }
        }
        else
        {
            bool last_hover = it->hover();
            it->setHover(false);
            if (last_hover && it->visible())
            {
                hover_change = true;
                emit MouseLeaveItem_Signal(this, it);
            }
        }
    }
    if (mWindow && hover_change && mWindow->isVisible()) mWindow->update();
    return true;
}


bool XOverlayLayer::mousePressEvent(QMouseEvent* ev)
{
    Resizebox rb;
    XOverlayItem* press_item = closestInside(ev->x(), ev->y());
    bool need_update = false;
    bool ret = true;
    if (press_item)
    {
        emit MousePressItem_Signal(this, press_item, ev);

        //Si es resizable y estamos sobre un resizebox lo movemos
        if (press_item->resizable() && (rb = press_item->overResizeBox(ev->x(), ev->y())))
        {
            mResizeBox = rb;
            mResizeItem = press_item;
            mResizeItem->setResizing(true);         //Marcamos el item como que está redimensionándose
            mResizeStartRect = mResizeItem->rect(); //Rectángulo que ocupaba el item al comenzar el redimensionado
            mResizeStartpoint = ev->pos();          //Punto pinchado al comenzar el redimensionado
            ret = false;
        }
        //Ponemos el item en movimiento si fuera movil
        else if (press_item->mobile())
        {
            mMovingItem = press_item;
            mMovingItem->setMoving(true);
            mMovePointDiff = QPoint(mMovingItem->x() - ev->x(), mMovingItem->y() - ev->y());
            ret = false;
        }
        else if (press_item->selectable())
        {
           press_item->setSelected(!press_item->selected());
           need_update = true;
        }
    } 

    //No se ha seleccionado ningún item. Si hubiera alguno seleccionado lo deselecciono
    for (int i=0; i<mItems.count(); i++)
    {
        XOverlayItem* it = mItems[i];
        if (it != press_item && it->selected() && it->selectable())
        {
            it->setSelected(false);
            need_update = true;
        }
    }

    if (need_update) mWindow->update();
    return ret;
}

bool XOverlayLayer::keyPressEvent(QKeyEvent* ke)
{
    bool need_update = false;
    if (ke->key() == Qt::Key_Delete)
    {
        for (int i=0; i<mItems.count(); i++)
        {
            XOverlayItem* it = mItems[i];
            if (it->visible() && it->selected() && it->selectable() && it->deletable())
            {
                removeItem(it);
                need_update = true;
            }
        }
    }
    else if (ke->key() == Qt::Key_Escape)
    {
        for (int i=0; i<mItems.count(); i++)
        {
            XOverlayItem* it = mItems[i];
            if (it->selected() && it->selectable())
            {
                it->setSelected(false);
                need_update = true;
            }
        }
    }
    if (need_update) mWindow->update();
    return true;
}

bool XOverlayLayer::mouseReleaseEvent(QMouseEvent* ev)
{
    //Si estoy moviendo un item pues no hago otra cosa que soltarlo
    if (mMovingItem)
    {
        mMovingItem->setMoving(false);
        mMovingItem = NULL;
        return false;
    }
    if (mResizeItem)
    {
        mResizeItem->setResizing(false);
        mResizeItem = NULL;
        return false;
    }


    XOverlayItem* release_item = closestInside(ev->x(), ev->y());
    if (release_item)
    {
        emit MouseReleaseItem_Signal(this, release_item, ev);
        return false;
    }
    return true;
}

bool XOverlayLayer::wheelEvent(QWheelEvent *)
{
    return true;
}

bool XOverlayLayer::resizeEvent(QResizeEvent *)
{
    return true;
}

bool XOverlayLayer::contextMenuRequest(QPoint)
{
    return true;
}

void XOverlayLayer::zoomChangeEvent(float newzoom)
{
    mPendingLabel = mPendingitem = NULL;
    mZoom = newzoom;
    if (visible() && mWindow)
    {
        imageMoveEvent();
        mWindow->update();
    }
}

/**
 * @brief XOverlayLayer::imageMoveEvent Se mueve la imagen.
 * Debemos recalcular la posición de pantalla de aquellos elementos anclados a coordenadas de IMAGEN o ECUATORIALES
 * @return
 */
bool XOverlayLayer::imageMoveEvent()
{
    if (!mapped() || !mWindow) return true;
    mPendingLabel = mPendingitem = NULL;
    QPointF pf; double x, y;
    bool need_update = false;
    for (int i=0; i<mItems.count(); i++)
    {
       XOverlayItem* it =  mItems.at(i);
       //En base a RA-DEC --> IMAGEN --> PANTALLA
       if (it->raDecAttatched())
       {
           mFunc_Imap(mWindow, it->ra(), it->dec(), &x, &y);
           mFunc_ImageXYToScreenXY(mWindow, x, y, pf);
           it->setPos(pf.x(), pf.y());
           need_update = true;
       }
       //En base a IMAGEN --> PANTALLA
       else if (it->imageAttatched())
       {
            mFunc_ImageXYToScreenXY(mWindow, it->bx(), it->by(), pf);
            it->setPos(pf);
            if (it->type() == XTYPE_SEGMENT)
            {
                 mFunc_ImageXYToScreenXY(mWindow,((XOverlaySegment*)it)->baseStart().x(), ((XOverlaySegment*)it)->baseStart().y(), pf);
                 ((XOverlaySegment*)it)->setStart(pf);
                 mFunc_ImageXYToScreenXY(mWindow,((XOverlaySegment*)it)->baseEnd().x(), ((XOverlaySegment*)it)->baseEnd().y(), pf);
                 ((XOverlaySegment*)it)->setEnd(pf);
            }
            need_update = true;
       }
       //Directamente en pantalla
       else
       {
            //no tenemos nada que hacer, las coordenadas ya son absolutas de pantalla
       }

       //Si hay alguno seleccionado lo deseleccionamos. De momento no haremos otra cosa
       if (it->visible() && it->selected() && it->selectable())
       {
           it->setSelected(false);
           need_update = true;
       }
    }
    if (need_update) mWindow->update();
    return true;
}











