#include "xoverlay.h"
#include "xoverlaycross.h"
#include "core.h"

XOverlayCross::XOverlayCross(XOverlayLayer* layer, quint8 group)
    :XOverlayItem(XTYPE_CROSS, layer, group)
{
    mRadius = (quint8)(10 * XGlobal::SCREEN_DPI);
    mHoleRadius = (quint8)(0 * XGlobal::SCREEN_DPI);
    mType = CROSSTYPE_PLUS;
}


XOverlayCross::~XOverlayCross()
{

}

QRectF XOverlayCross::rect()
{
    float z = zoom();
    if (mType == CROSSTYPE_PLUS)
        return QRectF(x() - (float)mRadius * z, y() - (float)mRadius * z,
                      (float)mRadius * 2 * z, (float)mRadius * 2 * z);
    else
    {
        double qsec = sqrt(POW2(mRadius*zoom())/2);
        return QRectF(x() - qsec, y() - qsec,
                      qsec * 2, qsec * 2);
    }
}

QRectF XOverlayCross::rectPad()
{
    float szrec = (mRadius + style()->hoverPadding()) * zoom();
    return QRectF(x() - szrec, y() - szrec, szrec * 2, szrec * 2);
}

bool XOverlayCross::inside(int px, int py)
{
    return rect().contains(px, py);
}

void XOverlayCross::paint(QPainter* painter)
{
    QRectF rct = rect();
    painter->setBackground(style()->backgroundBrush());
    painter->setPen( (hoverable() && hover()) ? style()->hoverBorderPen() : style()->borderPen() );
    if (!mHoleRadius)
    {
        switch (mType)
        {
            case CROSSTYPE_PLUS:
            painter->drawLine(QPointF(rct.x() + rct.width()/2.0, rct.y()), QPointF(rct.x() + rct.width()/2.0, rct.bottom()));
            painter->drawLine(QPointF(rct.x(), rct.y() + rct.height()/2.0), QPointF(rct.right(), rct.y() + + rct.height()/2.0));
            break;

            case CROSSTYPE_X:
            painter->drawLine(QPointF(rct.x(), rct.y()),     QPointF(rct.right(), rct.bottom()));
            painter->drawLine(QPointF(rct.right(), rct.y()), QPointF(rct.x(), rct.bottom()));
            break;
        }
    }
    else
    {
        float sp_size,rpri;
        //Rectángulo interno con el agujero
        float z = zoom();
        QRectF rh(x() - mHoleRadius * z, y() - mHoleRadius * z, mHoleRadius * 2 * z, mHoleRadius * 2 * z);
        switch (mType)
        {
            case CROSSTYPE_PLUS:
            sp_size = (rct.height() - rh.height())/2.0f;
            painter->drawLine(QPointF(rct.x() + rct.width()/2.0, rct.y()),
                              QPointF(rct.x() + rct.width()/2.0, rct.y() + sp_size));
            painter->drawLine(QPointF(rct.x() + rct.width()/2.0, rct.bottom() - sp_size),
                              QPointF(rct.x() + rct.width()/2.0, rct.bottom()));
            painter->drawLine(QPointF(rct.x(), rct.y() + rct.height()/2.0),
                              QPointF(rct.x() + rct.width()/2.0 - sp_size, rct.y() + rct.height()/2.0));
            painter->drawLine(QPointF(rct.right() - sp_size, rct.y() + rct.height()/2.0),
                              QPointF(rct.right(), rct.y() + rct.height()/2.0));
            break;

            case CROSSTYPE_X:
            rpri = sqrt(POW2(mHoleRadius*zoom())/2);            //Semilado del cuadrado para xcross
            //No implementado de momento...
            painter->drawLine(QPointF(rct.x(),                           rct.y()),
                              QPointF(rct.x()+rct.width()/2.0-rpri,      rct.y()+rct.width()/2.0-rpri));

            painter->drawLine(QPointF(rct.right(),      rct.y()),
                              QPointF(rct.x()+rct.width()/2.0+rpri,      rct.y()+rct.width()/2.0-rpri));

            painter->drawLine(QPointF(rct.x(),          rct.bottom()),
                              QPointF(rct.x()+rct.width()/2.0-rpri,      rct.y()+rct.width()/2.0+rpri));
            painter->drawLine(QPointF(rct.right(),      rct.bottom()),
                              QPointF(rct.x()+ rct.width()/2.0+rpri,     rct.y()+rct.width()/2.0+rpri));
            break;
        }
    }
    if (selectable() && selected() && !style()->boxBorderTransparent())
    {
        painter->setPen(style()->boxBorderPen());
        painter->drawRect(rectPad());
    }
}






