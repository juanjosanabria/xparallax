#ifndef XOVERLAYLABEL_H
#define XOVERLAYLABEL_H

#include "xoverlayitem.h"


class XOverlayLabel : public XOverlayItem
{   
public:
    enum XAnchorPoint : quint8 {AP_N,AP_S,AP_E,AP_W,AP_NE,AP_NW,AP_SE,AP_SW};
    explicit XOverlayLabel(XOverlayLayer* layer = 0, quint8 group = 0);
    XAnchorPoint anchor(){return mAnchorPoint;}
    void setAnchor(XAnchorPoint an){mAnchorPoint=an;}

    //Métodos virtuales abstractos implementados
    //************************************************************************
    virtual void  paint(QPainter* painter);
    virtual QRectF rect();
    virtual QRectF rectPad();
    virtual bool  inside(int x, int y);
    virtual void fitIn(QRect, QRect){;}
    virtual void fitInRadius(float){;}

private:
    XAnchorPoint    mAnchorPoint;

public slots:

};

#endif // XOVERLAYLABEL_H
