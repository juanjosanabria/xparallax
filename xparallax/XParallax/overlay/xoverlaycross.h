#ifndef XOVERLAYCROSS_H
#define XOVERLAYCROSS_H

#include "xoverlayitem.h"

enum XOverlayCrossType{
    CROSSTYPE_PLUS,
    CROSSTYPE_X
};

class XOverlayCross : public XOverlayItem
{
private:
    quint8              mRadius;            //Tamaño de la cruz
    quint8              mHoleRadius;        //Tamaño del radio interno o abertura interna
    XOverlayCrossType   mType;

public:
    explicit XOverlayCross(XOverlayLayer* layer = 0, quint8 group = 0);
    ~XOverlayCross();

    //Métodos propios
    //************************************************************************
    inline quint8 radius(){return mRadius;}
    inline void setRadius(quint8 rad){mRadius = rad;}
    inline quint8 holeRadius(){return mHoleRadius;}
    inline void setHoleRadius(quint8 hr){mHoleRadius = hr;}
    inline XOverlayCrossType type(){return mType;}
    inline void setType(XOverlayCrossType t){mType = t;}


    virtual void paint(QPainter* painter);                      //Dibuja el objeto
    virtual QRectF rect();                                      //Rectángulo que contiene al completo el objeto
    virtual QRectF rectPad();                                   //Rectángulo externo para considerar que estamos dentro
    virtual bool inside(int x, int y);                          //Comprueba si un puto cae dentro del objeto
    virtual void fitIn(QRect /*rct*/, QRect /*sqr*/){;}         //Encaja este item dentro del rectángulo dado redimensionándolo
    virtual void fitInRadius(float /*radius*/){;}               //Hace crecer/menguar el item para que se ajuste a un radio determinado

};

#endif // XOVERLAYCROSS_H
