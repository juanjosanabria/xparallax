#ifndef XOVERLAYRECT_H
#define XOVERLAYRECT_H

#include "xoverlayitem.h"

class XOverlayRect : public XOverlayItem
{
public:
    explicit XOverlayRect(XOverlayLayer* layer = 0, quint8 group = 0);
    void setRect(const QRectF& rct){setBasePos(rct.x(), rct.y());mWidth=rct.width();mHeight=rct.height();}
    void setRect(double x, double y, double width, double height){setBasePos(x,y);mWidth=width;mHeight=height;}

    virtual void paint(QPainter* painter);                      //Dibuja el objeto
    virtual QRectF rect();                                      //Rectángulo que contiene al completo el objeto
    virtual QRectF rectPad();                                   //Rectángulo externo para considerar que estamos dentro
    virtual bool inside(int x, int y);                          //Comprueba si un puto cae dentro del objeto
    virtual void fitIn(QRect /*rct*/, QRect /*sqr*/){;}         //Encaja este item dentro del rectángulo dado redimensionándolo
    virtual void fitInRadius(float /*radius*/){;}               //Hace crecer/menguar el item para que se ajuste a un radio determinado

private:
    double      mWidth;
    double      mHeight;
};

#endif // XOVERLAYRECT_H
