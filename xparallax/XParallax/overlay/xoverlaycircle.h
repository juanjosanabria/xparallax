#ifndef XOVERLAYCIRCLE_H
#define XOVERLAYCIRCLE_H

#include "xoverlayitem.h"

class XOverlayCircle : public XOverlayItem
{

private:
    float       mRadius;

public:
    explicit XOverlayCircle(XOverlayLayer* layer = 0, quint8 group = 0);

    inline float radius(){ return mRadius; }
    inline void setRadius(float rad){ mRadius = rad;}

    virtual void  paint(QPainter* painter);
    virtual QRectF rect();
    virtual QRectF rectPad();
    virtual bool  inside(int x, int y);
    virtual void  fitIn(QRect rct, QRect sqr);
    virtual void fitInRadius(float);

signals:

public slots:

};

#endif // XOVERLAYCIRCLE_H
