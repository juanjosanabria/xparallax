#include "xoverlay.h"
#include "xoverlaylabel.h"


XOverlayLabel::XOverlayLabel(XOverlayLayer* layer, quint8 group) :
    XOverlayItem(XTYPE_LABEL, layer, group)
{
    setPaintLabel(false);
    mAnchorPoint = AP_NW;
}

QRectF XOverlayLabel::rect()
{
    QFontMetricsF fm(style()->font());
    int width = fm.width(label());
    int height = fm.height();
    int padding = style()->padding();
    return QRectF(x() - padding, y() - padding, width + padding*2 , height + padding*2);
}

QRectF XOverlayLabel::rectPad()
{
    return rect();
}

void XOverlayLabel::paint(QPainter* painter)
{
    if (!visible() || !hasLabel()) return;
    QFontMetricsF fm(style()->font());
    fm.width(label());
    int height = fm.height();
    painter->setPen(style()->borderPen());
    painter->setBrush(style()->backgroundBrush());
    QRectF pr = rect();
    if (!style()->backTransparent()) painter->fillRect(pr, style()->backgroundColor());
    painter->setPen(style()->forePen());
    painter->drawText(x(), y() + height/2, label());
}

bool XOverlayLabel::inside(int px, int py)
{
    return rect().contains(px, py);
}
























