#include "xoverlayrect.h"
#include "xoverlay.h"

XOverlayRect::XOverlayRect(XOverlayLayer* layer, quint8 group)
    :XOverlayItem(XTYPE_RECT, layer, group)
{
    mWidth = 10;
    mHeight = 10;
}

void XOverlayRect::paint(QPainter* painter)
{
    if (!visible()
            //Si no estamos encima o no es hoverable y todo es transparente
            || ((!hover() || !hoverable()) && style()->borderTransparent() && style()->backTransparent())
            //O si es hoverable y está en estado hover y hay algo que pintar
            || (hoverable() && hover() && style()->hoverBorderTransparent() && style()->hoverBackTransparent() )
            ) return;
    painter->setPen(hoverable() && hover() ? style()->hoverBorderPen() : style()->borderPen());
    painter->setBrush(hoverable() && hover() ? style()->hoverBackgroundBrush() : style()->backgroundBrush());
    painter->drawRect(rect());
    //if (hover() && resizable()) paintResizeBox(painter);
}

QRectF XOverlayRect::rect()
{
    if (zoomAttatched()) return QRectF(pos().x(), pos().y(), mWidth*zoom(), mHeight*zoom());
    else return QRectF(pos().x(), pos().y(), mWidth, mHeight);
}

QRectF XOverlayRect::rectPad()
{
    if (zoomAttatched()) return QRectF(pos().x(), pos().y(), mWidth*zoom(), mHeight*zoom());
    else return QRectF(pos().x(), pos().y(), mWidth, mHeight);
}

bool XOverlayRect::inside(int x, int y)
{
    QRectF rct = rect();
    return x >= rct.x() && x<= rct.right() && y >= rct.y() && y <= rct.bottom();
}
