#include "xoverlaystyle.h"
#include "xglobal.h"

XOverlayStyle   XOverlayStyle::DEFAULT_STYLE;

XOverlayStyle::XOverlayStyle(bool single)
{
    mIsForSingleObject = single;
    mPadding = 15;
    mHoverPad = 1;
    mBackBrush = QBrush(Qt::transparent);
    mBorder = QPen(Qt::red);
    mForeground = QPen(Qt::white);
    mHoverBackBrush = QBrush(Qt::transparent);
    mHoverBorder = QPen(Qt::transparent);
    mForeBack = QBrush(QColor::fromRgb(0xac1919));
    mLabelDist = 13*XGlobal::SCREEN_DPI;
    mTextAttatch = (XOverlayTetxAttatch)(XA_PARENT_UPPER_RIGHT | XA_PAINT_CONNECTOR);
}


XOverlayStyle::~XOverlayStyle()
{

}
