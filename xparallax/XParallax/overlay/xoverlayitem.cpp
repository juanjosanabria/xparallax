#include <QTextDocument>
#include "xoverlayitem.h"
#include "xoverlaylayer.h"
#include "xoverlaycross.h"
#include "xoverlaypolygon.h"
#include "core.h"
#include "util.h"

QPoint XOverlayItem::mLastMousePos;

XOverlayItem::XOverlayItem(quint8 type, XOverlayLayer* layer, quint8 group)
{
    mLayer = layer;
    mId = 0;
    mType = type;
    mGroup = group;
    mStyle = XOverlayStyle::defaultStyle();
    mFlags = (XOverlaItemFlags)(XITEM_VISIBLE | XITEM_SCREEN_ATTATCHED | XITEM_GROWMODE_RADIAL | XITEM_LABEL_HOVER | XITEM_PAINT_LABEL);
    mPosition = QPointF(10, 20);
    if ((mLayer = layer)) mLayer->addItem(this);
}

XOverlayItem::~XOverlayItem()
{
    //Si es un estilo mío propio lo elimino
    if (mStyle != XOverlayStyle::defaultStyle() && mStyle->single())
        delete mStyle;
    //Lo elimino de la capa sin que la capa haga un delete (evidentemente)
    if (mLayer) mLayer->removeItem(this, false);
}

void XOverlayItem::paintResizeBox(QPainter* painter)
{
    QRectF rct = rect();

    painter->setBrush(QBrush(Qt::transparent));
    painter->setPen(QPen(Qt::yellow, 1, Qt::DotLine));

    //painter->drawRect(rct);
    //Cuadro superior izquierdo
    painter->drawRect(rct.x() - XOVERLAY_RESIZE_BOX_SIZE, rct.y() - XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE);
    //Cuadro inferior izquierdo
    painter->drawRect(rct.x() - XOVERLAY_RESIZE_BOX_SIZE, rct.y() + rct.height(), XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE);
    //Cuadro superior derecho
    painter->drawRect(rct.right(), rct.y() - XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE);
    //Cuadro inferior derecho
    painter->drawRect(rct.right() , rct.bottom(), XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE);
}

void XOverlayItem::paintLabel(QPainter* painter)
{
    /****************************
     *          ________
     *  ______ /| text  |
     * |      | |_______|
     * | ELEM |
     * |______|
     *
     *
     ****************************/
    if (!(mFlags&XITEM_PAINT_LABEL) || !mLabel.length() || (!hover() && (mFlags&XITEM_LABEL_HOVER))) return;
    QRectF rct = (mFlags&XITEM_LABEL_FROM_POS)   ? QRectF(mPosition.x(), mPosition.y(), 1, 1) :
                 (mFlags&XITEM_LABEL_NEAR_MOUSE) ? QRectF(mLastMousePos.x(), mLastMousePos.y(), 1, 1) : rect();

    QFontMetricsF fm = QFontMetricsF(style()->font(), mLayer->window());

    //Calculamos el rectángulo que contiene la etiqueta

    //Dibujamos el rectángulo que contiene la etiqueta
    painter->setBrush(style()->backgroundBrush());

    //Dibujamos el texto
    painter->setPen(style()->forePen());


    //Distancia a la que se encuentra el punto x de anclaje
    double dx  = COS_45 * mStyle->labelDist();
    double dy  = SIN_45 * mStyle->labelDist();
    QPointF set_point;
    XOverlayTetxAttatch attatch = style()->textAttatch();
    if (attatch & RESIZEBOX_UPER_LEFT)
        set_point = QPointF(rct.x() - dx, rct.y() - dy);
    else if (attatch & RESIZEBOX_UPER_RIGHT)
        set_point = QPointF(rct.right() + dx, rct.y() - dy);
    else if (attatch & RESIZEBOX_LOWER_LEFT)
        set_point = QPointF(rct.x() - dx, rct.bottom() + dy);
    else if (attatch & RESIZEBOX_LOWER_RIGHT)
        set_point = QPointF(rct.right() + dx, rct.bottom() + dy);


    if (html())
    {
        //Buscamos la línea más larga
        QStringList lines =  mLabel.split('\n');
        double maxlen = 0;
        for (int i=0; i<lines.count(); i++)
        {
            double len = fm.width(lines.at(i).trimmed());
            if (len > maxlen) maxlen = len;
        }
        double height = fm.height() * lines.count();
        int padding = style()->padding();
        QRectF rct_txt = QRectF(set_point.x() + padding, set_point.y() +padding - fm.height()/2.0, maxlen + padding, height + padding + fm.height()/2.0);
        QRectF rct_bkg = QRectF(set_point.x(), set_point.y(), maxlen + padding * 2, height + padding * 2);
        painter->setFont(style()->font());
        if (attatch & XA_PAINT_CONNECTOR) painter->drawLine(set_point.x(), set_point.y(), rct_bkg.x(), rct_bkg.y());
        painter->fillRect(rct_bkg, style()->foreBack());
        painter->drawRect(rct_bkg);
        //painter->drawText(rct_txt, mLabel);
        QTextDocument td;
        td.setHtml(mLabel.replace("\n","<br/>"));
        //td.setTextWidth(rct_txt.width());
        td.setUseDesignMetrics(true);
        td.drawContents(painter, rct_txt);

    }
    else if (!multiLine())
    {

        painter->drawText(set_point.x(), set_point.y(), mLabel);
    }
    else
    {
        //Buscamos la línea más larga
        QStringList lines =  mLabel.split('\n');
        double maxlen = 0;
        for (int i=0; i<lines.count(); i++)
        {
            double len = fm.width(lines.at(i).trimmed());
            if (len > maxlen) maxlen = len;
        }
        double height = fm.height() * lines.count();
        int padding = style()->padding();
        QRectF rct_txt = QRectF(set_point.x() + padding, set_point.y() +padding - fm.height()/2.0, maxlen + padding, height + padding + fm.height()/2.0);
        QRectF rct_bkg = QRectF(set_point.x(), set_point.y(), maxlen + padding * 2, height + padding * 2);
        painter->setFont(style()->font());
        if (attatch & XA_PAINT_CONNECTOR) painter->drawLine(set_point.x(), set_point.y(), rct_bkg.x(), rct_bkg.y());
        painter->fillRect(rct_bkg, style()->foreBack());
        painter->drawRect(rct_bkg);
        painter->drawText(rct_txt, mLabel);

    }
}

Resizebox XOverlayItem::overResizeBox(int x, int y)
{
    QRectF rct = rect();
    if (QRectF(rct.x() - XOVERLAY_RESIZE_BOX_SIZE, rct.y() - XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE).contains(x, y))
        return RESIZEBOX_UPER_LEFT;
    if (QRectF(rct.x() - XOVERLAY_RESIZE_BOX_SIZE, rct.y() + rct.height(), XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE).contains(x, y))
        return RESIZEBOX_LOWER_LEFT;
    if (QRectF(rct.right(), rct.y() - XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE).contains(x, y))
        return RESIZEBOX_UPER_RIGHT;
    if (QRectF(rct.right() , rct.bottom(), XOVERLAY_RESIZE_BOX_SIZE, XOVERLAY_RESIZE_BOX_SIZE).contains(x, y))
        return RESIZEBOX_LOWER_RIGHT;
    return RESIZEBOX_NONE;
}

float XOverlayItem::zoom()
{
   return (mLayer && zoomAttatched()) ? mLayer->zoom() : 1.0f;
}

/**
 * @brief XOverlayItem::parseItem
 * @param cad
 * La cadena de tipo de item contiene siempre: tipo;coordenadas;etiqueta
 * cross;[RADEC];[LABEL]
 *      ej) cross;12 30 21 +32 11 12.1;This is a label
 * cross;[XY];[LABEL]
 *      ej) cross;611.23 1223.12;This is a label
 *
 * @return XOverlayItem item parseado pero sin añadir a ninguna capa. NULL si no se pudo parsear
 */
XOverlayItem* XOverlayItem::parseItem(QString& cad)
{
    QStringList vec = cad.split(";");
    if (vec.length()) vec[0] = (vec.at(0)).toLower();
    else return NULL;

    //Parseo del tipo CROSS
    //--------------------------------------------------------------
    if (vec[0] == "cross")
    {
        XOverlayCross* cross = NULL;
        if (vec.length() < 2) return NULL;
        QVector<double> vradec = Util::ParseRADEC(vec[1]);
        if (vradec.length() == 2 && IS_VALIDF(vradec[0]) && IS_VALIDF(vradec[1]))
        {
            cross = new XOverlayCross();
            cross->setRaDec(vradec[0], vradec[1]);
        }
        QVector<double> vxy = Util::ParseRADEC(vec[1]);
        if (vxy.length() == 2 && IS_VALIDF(vxy[0]) && IS_VALIDF(vxy[1]))
        {
            cross = new XOverlayCross();
            cross->setBasePos(vxy[0], vxy[1]);
        }
        if (cross)
        {
            cross->setHoverable(true);
            cross->setSelectable(true);
            cross->setDeletable(true);
            cross->setRaDecAttatched(true);
            cross->setRadius(20);
            cross->setHoleRadius(8);
            //Poner la etiqueta
            if(vec.length() > 2 && vec.at(2).length())
            {
                QString label = vec[2];
                label = label.replace("\\n","\n").trimmed();
                if (label.length()) cross->setLabel(label);
            }
        }
        return cross;
    }
    else if (vec[0] == "poly")
    {
        XOverlayPolygon* polyv = NULL;
        QVector<QPointF> poly;
        QStringList sl_points = vec[1].split('/');
        for (int i=0; i<sl_points.count(); i++)
        {
            QStringList sl = sl_points.at(i).split(',');
            if (sl.count() != 2) continue;
            poly.append(QPointF(sl[0].toDouble(), sl[1].toDouble()));
        }
        if (sl_points.count() >= 3)
        {
            polyv = new XOverlayPolygon();
            polyv->setPoly(poly);
            polyv->setImageAttatched(true);
            polyv->setZoomAttatched(true);
            return polyv;
        }

    }
    return NULL;
}















