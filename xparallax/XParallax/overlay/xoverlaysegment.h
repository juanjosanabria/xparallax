#ifndef XOVERLAYSEGMENT_H
#define XOVERLAYSEGMENT_H

#include "xoverlayitem.h"

#define  SEGMENT_ARROWHEAD_START        128
#define  SEGMENT_ARROWHEAD_END           64
#define  SEGMENT_ARROWHEAD_BOTH     (128|64)

class XOverlaySegment : public XOverlayItem
{
public:
    explicit XOverlaySegment(XOverlayLayer *layer = 0, quint8 group = 0);

    //Acceso a miembros
    //*******************************************************************************************
    inline QPointF   start() const{return mStart;}
    inline void      setStart(const QPointF& st){mStart = st; ComputePoly();}
    inline QPointF   end() const {return mEnd;}
    inline void      setEnd(const QPointF& ed){mEnd = ed;  ComputePoly();}
    inline QPointF   baseStart() const{return mBaseStart;}
    inline void      setBaseStart(const QPointF& st){mBaseStart = st;}
    inline QPointF   baseEnd() const {return mBaseEnd;}
    inline void      setBaseEnd(const QPointF& ed){mBaseEnd = ed;}
    inline quint8    arrowHead(){return mArrowHead&(~SEGMENT_ARROWHEAD_BOTH);}
    inline quint8    arrowHeadSpec(){return mArrowHead&SEGMENT_ARROWHEAD_BOTH;}
    inline void      setArrowHead(quint8 ah, quint8 ah_spec=SEGMENT_ARROWHEAD_BOTH){mArrowHead = ah|ah_spec;}
    inline QPointF*  poly(){return mPoly;}    //Polígono que envuelve por completo al segmento

    //Recorta el segmento para que encaje dentor del rectángulo especificado
    bool clip(QRectF& rct);


    //Métodos virtuales
    //*******************************************************************************************
    virtual void paint(QPainter* painter);          //Dibuja el objeto
    virtual QRectF rect();                          //Rectángulo que contiene al completo el objeto
    virtual QRectF rectPad();                       //Rectángulo externo para considerar que estamos dentro
    virtual bool inside(int x, int y);              //Comprueba si un puto cae dentro del objeto
    virtual void fitIn(QRect rct, QRect sqr);       //Encaja este item dentro del rectángulo dado redimensionándolo
    virtual void fitInRadius(float radius);         //Hace crecer/menguar el item para que se ajuste a un radio determinado


private:
    QPointF     mStart;
    QPointF     mEnd;
    QPointF     mBaseStart;
    QPointF     mBaseEnd;
    quint8      mArrowHead;  //Tamaño de la cabeza de flecha si la tuviera en píxeles. Valores 128 y 64 no están permitidos
    QPointF     mPoly[4];    //Indican respectivamente que solo tendría cabeza al comienzo o el fin

    // Cohen–Sutherland clipping algorithm clips a line from
    // P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with
    // diagonal from (xmin, ymin) to (xmax, ymax).
    bool CohenSutherlandLineClip(QRectF& crt, double &x0, double &y0, double &x1, double &y1);

    // Compute the bit code for a point (x, y) using the clip rectangle
    // bounded diagonally by (xmin, ymin), and (xmax, ymax)
    int ComputeOutCode(QRectF& crt, double x, double y);

    void ComputePoly();
};

#endif // XOVERLAYSEGMENT_H



