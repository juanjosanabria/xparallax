#include "xoverlaypolygon.h"

XOverlayPolygon::XOverlayPolygon(XOverlayLayer* layer, quint8 group)
    :XOverlayItem(XTYPE_POLYGON, layer, group)
{

}

void XOverlayPolygon::setPoly(const QVector<QPointF>& poly)
{
    if (poly.count() == 0) return;
    setPos(poly[0]);
    mPoly.clear();
    for (int i=0; i<poly.count(); i++)
        mPoly.append(QPointF(poly[i].x(),poly[i].y()));
}


void XOverlayPolygon::paint(QPainter* painter)
{
    if (!visible()
            //Si no estamos encima o no es hoverable y todo es transparente
            || ((!hover() || !hoverable()) && style()->borderTransparent() && style()->backTransparent())
            //O si es hoverable y está en estado hover y hay algo que pintar
            || (hoverable() && hover() && style()->hoverBorderTransparent() && style()->hoverBackTransparent() )
            ) return;
    QVector<QPointF> pol;
    for (int i=0; i<mPoly.count(); i++)
        pol.append(QPointF(mPoly[i].x()*zoom() + pos().x(),mPoly[i].y()*zoom() + pos().y()));

    painter->setPen(hoverable() && hover() ? style()->hoverBorderPen() : style()->borderPen());
    painter->setBrush(hoverable() && hover() ? style()->hoverBackgroundBrush() : style()->backgroundBrush());
    painter->drawPolygon(pol);
}

QRectF XOverlayPolygon::rect()
{
    return QRectF();
}

QRectF XOverlayPolygon::rectPad()
{
    //TODO:
    return QRectF();
}

bool XOverlayPolygon::inside(int x, int y)
{
 //TODO:
    return false;
}
