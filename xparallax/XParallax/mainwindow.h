#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiArea>
#include <QSignalMapper>
#include <QMdiSubWindow>
#include <QMessageBox>
#include "types.h"
#include "experimental.h"
#include "mdifitimage.h"
#include "ui_mdi_childs/xpmdiwidget.h"
#include "ui_mdi_childs/mdilogger.h"
#include "ui_mdi_childs/mdimpc.h"

namespace Ui {
    class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    static MainWindow* mainwin;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void UpdateLabel(int idx,const QString& text);
    int SubWindowCount(const char* type);
    void UpdateMenus();
    void UpdateWindowMenu();

    XPMdiWidget* OpenFile(const QString& filename);
    MdiFitImage* OpenFit(const QString& filename);
    MdiMPC* OpenMpcReport(const QString& filename);
    MdiMPC* CreateMpcReport(MPCReport* rep);

    QDesktopWidget* desktop(){return mDesktopWidget;}

    //Acceso a las ventans hijas
    QMdiSubWindow* AddMdiSubWindow(QWidget* content, QString icon = "", QSize minsize = QSize(256,256));
    QList<QMdiSubWindow*> SubWindows(const char* classname);
    MdiFitImage* ActiveFitImage();
    MdiMPC*      ActiveMPC();
    MdiMPC*      OpenMPC();
    XPMdiWidget* ActiveXPMdiWidget();
    QList<MdiFitImage*> fitImages(bool fitblink=false);
    MdiLogger* ActiveLogger();                         //Devuelve la ventana de log si hay alguna abierta
    QMdiSubWindow* GetSubWindow(QWidget* content);


    static void SetIcoDPI(QMainWindow* mw);
    static void DPIAware(QDialog* dlg, bool fix_size);


protected:
    void dropEvent(QDropEvent *);
    void dragEnterEvent(QDragEnterEvent *);
    void keyPressEvent(QKeyEvent* ke);
    void closeEvent(QCloseEvent *);

private:
    Ui::MainWindow *ui;

    void CreateMenus();
    void CreateMDIArea();
    void CreateActions();
    void CreateToolBars();
    void CreateStatusBar();


    //MdiFit. Ventanas
    //****************************************************************************************************
    void MdiFit_Load();

    QMdiSubWindow *findMdiChild(const QString &fileName);

public:
    QMenu* getMenuByName(const QString& name, bool create=false, QWidget* parent=NULL, int priority=0);
    void sortActionItems(QObject* parent);
    static bool compareXPriority(const QObject *left,const QObject *right);
    QToolBar* getToolbarByName(const QString& name, bool create=false, int priority=0);

    void InterfaceMdiFit_Activate(XPMdiWidget *fi);                     //Se activa uan ventana MdiFit
    void InterfaceMdiFit_DeActivate(XPMdiWidget *fi);                   //Se desactiva una ventana MdiFit
    int InterfaceMdiFit_ShowStaticAction(QWidget* parent,QAction* ref, bool show);

    /**
     * @brief NoModalAdd Convierte un diálogo en no modal y se asegura de que es destruído al ser cerrado o al ser cerrada la aplicación
     * @param dlg Diálogo a cambiar
     */
    void NoModalAdd(QDialog* dlg);

private:
    //Elementos de uso interno y externo
    QSignalMapper *                 mWindowMapper;
    QMdiArea*                       mMDIArea;
    QMdiSubWindow*                  mActiveWindow;
    QDesktopWidget*                 mDesktopWidget;
    QList<QDialog*>                 mNoModalDialogs;

    //Statusbar, etiquetas de la barra
    QComboBox*                      mStatusScale;
    QLabel*                         mStatusXY;
    QLabel*                         mStatusFlux;

    //Menú de ayuda
    QMenu*   mHelpMenu;
    QAction* mAboutXParallax;
    //QAction* mAboutQT;
    QAction* mHelpIndexAction;
    QAction* mSearchForUpdatesAction;

    //Menú de archivo
    QMenu   *mFileMenu;
    QAction *mFileOpenAction;
    QAction *mExitAction;
    QAction *mSaveAction;
    QAction *mSaveAsAction;
    QAction *mCloseAction;

    //Menú de image analysis
    QMenu   *mImageMenu;
    QAction *mCalibrateAction;
    QAction *mAstrometryAction;
    QAction *mImageCatalogAction;
    QAction *mBlinkAction;
    QAction *mBatchHeader;

    //Menú de tools
    QMenu   *mToolsMenu;
    QAction *mToolAngularDistanceAction;
    QAction *mToolCoordinateConversion;

    //Barra de herramientas
    QToolBar* mMainToolbar;
    QToolBar* mFitToolBar;

    //Menú window
    QMenu*  mWindowMenu;
    QAction *mCloseAllAction;
    QAction *tileAct;
    QAction *cascadeAct;
    QAction *nextAct;
    QAction *previousAct;
    QAction *mSeparatorAct;

    //Menú experimental
    Experimental*   mExperimental;


#ifdef QT_DEBUG
    QMenu*         mTestMenu;

    QAction*       mTestAction2;
    QAction*       mTestAction3;
    QAction*       mTestAction4;
    QAction*       mTestAction5;
    QAction*       mTestAction6;
    QAction*       mTestAction7;
    QAction*       mTestAction8;
    QAction*       mTestAction9;
#endif

private: /* Acciones background */
    static int Background_Atrometry(void *data);

signals:
    void UpdateFound_Signal(bool found, bool user_requested);

public slots:
    //Acciones
    void ExitAction();
    void AboutAction();
    void ActiveSubwindowChanged_Slot(QMdiSubWindow* window);
    void SetActiveSubwindow_Slot(QWidget* widget);
    void ChildClosed_Slot(QObject* subwindow);
    void CheckForUpdates_Slot();
    void UpdateFound_Slot(bool found, bool user_requested);
    void HelpIndex_Slot();
    void LoggerWindow_Slot();
    void MessageAppReceived_Slot();

    //Acciones del menú de archivo
    void FileOpen_Slot();
    void Exit_Slot();

    //Acciones del menú de proceso por lotes de imagen
    void ImageCalibrate_Slot();
    void ImageAstrometry_Slot();
    void ImageCatalog_Slot();
    void ImageBlink_Slot();
    void BatchHeaders_Slot();

    //Acciones del menú de fit
    void FitScaleChange_Slot();     //Cambia la escala en el combo box

    //Acciones de tools
    void Tool_CoordinateTransform_Slot();

    //Pruebas
    void Test2_Slot();
    void Test3_Slot();
    void Test4_Slot();
    void Test5_Slot();
    void Test6_Slot();
    void Test7_Slot();
    void Test8_Slot();
    void Test9_Slot();

};

#endif // MAINWINDOW_H
