#ifndef XIMGTOOLMPCADDMEASURE_H
#define XIMGTOOLMPCADDMEASURE_H

#include "ximgtool.h"
#include "detection.h"
#include "mpc.h"

#define  MPC_MEASURE_BOX_SIZE   128

class XImgToolMPCAddMeasure : public XImgTool
{
    Q_OBJECT

public:
    explicit XImgToolMPCAddMeasure(MdiFitImage *parent);
    ~XImgToolMPCAddMeasure();

    bool activate();
    bool deactivate();
    bool mousePressEvent(QMouseEvent* );
    bool mouseReleaseEvent(QMouseEvent *);
    bool mouseMoveEvent(QMouseEvent *);

private:
    bool           checkPrevious();
    DetectedStar*  searchSource(int x, int y);
    float          getMag(double flux);
    bool           existsByID(const QString& id);
    bool           existsByRADEC(double ra, double dec);

    /**
     * @brief checkNewMeasure Comprueba que la medida que se va a añadir es consistente con las medidas
     * previas.
     * @param mea Medida que se va a añadir
     * @return true si es correcto añadirla y false si no lo es. Mostrará mensaje de error
     */
    bool           checkNewMeasure(MPCReportMeasure* mea);
    bool           selectObjectNumber(double ra, double dec,int*number, char* desig,char *aux,MPCReportMeasure::MeasureType* mt);

    StarDetector*  mDetector;
    QPoint         mPressXY;
    QPoint         mDetPoint;
    DetectedStar*  mDetectedStar;
    QDateTime      mDateObs;
    float          mData[MPC_MEASURE_BOX_SIZE*MPC_MEASURE_BOX_SIZE];


signals:

public slots:

};

#endif // XIMGTOOLMPCADDMEASURE_H
