#include "ximgtoolstretchdialog.h"
#include "ui_ximgtoolstretchdialog.h"
#include "xglobal.h"
#include "core_def.h"
#include "../mdifitimage.h"
#include <QPainter>
#include <QMouseEvent>

#define     MESH_WIDTH_X     (32 * XGlobal::SCREEN_DPI)     //Ancho de la malla
#define     MESH_WIDTH_Y     (20 * XGlobal::SCREEN_DPI)     //Alto de la malla
#define     Y_PADDING        (10 * XGlobal::SCREEN_DPI)     //Espaciado arriba y abajo, méramente estético
#define     X_PADDING        (5 * XGlobal::SCREEN_DPI)      //Espaciado a izquierda y derecha para dar margen de actuación
#define     MARK_SPACING     (4 * XGlobal::SCREEN_DPI)      //Distancia puntero y la marca para cambiar el cursor
#define     SZ_HISTOGRAM     512

XImgToolStretchDialog::XImgToolStretchDialog(MdiFitImage *parent, XImgToolStretch* tool) :
    QDialog(parent),
    ui(new Ui::XImgToolStretchDialog)
{
    mTool = tool;
    ui->setupUi(this);
    mMeshPen = QPen(QColor::fromRgb(216,234,244),1);
    mLinePen = QPen(QColor::fromRgb(16, 124, 244),1);
    mPolyBrush = QBrush(QColor::fromRgb(16, 124, 244,32));
    mLogaritmicY = true;
    mReseted = true;
    mHistoArea = false;
    mOverMark = mMovingMark = 0;
    mHistogram = NULL;
    mHistoSize = SZ_HISTOGRAM;
    mHistogram = new int[mHistoSize];
    mPolyLine = new QPointF[mHistoSize+2];
    //Conectamso las señales a los slots
    connect(ui->buttonZoom, SIGNAL(toggled(bool)), this, SLOT(SetZoom_Slot()));
    connect(ui->buttonReset, SIGNAL(clicked()), this, SLOT(Reset_Slot()));
    connect(this, SIGNAL(accepted()),this, SLOT(Accept_Slot()));
    connect(this, SIGNAL(rejected()), this, SLOT(Cancel_Slot()));
    ui->comboProfile->setVisible(false);
}

XImgToolStretchDialog::~XImgToolStretchDialog()
{
    if (mPolyLine) delete mPolyLine;
    mPolyLine = NULL;
    if (mHistogram) delete mHistogram;
    mHistogram = NULL;
    delete ui;
}

void XImgToolStretchDialog::setZoomed()
{
    ui->buttonZoom->setChecked(true);
}

double XImgToolStretchDialog::xToHistoIndexD(double x)
{
    double scale_width = (ui->frameGraph->width()-X_PADDING*2)/(double)mHistoSize;
    double histoin = (x - ui->frameGraph->x() - Y_PADDING) / scale_width;
    return histoin;
}

int XImgToolStretchDialog::xToHistoIndexI(double x)
{
   int idx = xToHistoIndexD(x);
   if (idx<0) return 0;
   if (idx >= mHistoSize) return mHistoSize-1;
   return idx;
}

double XImgToolStretchDialog::histoIndex2X(double i)
{
   double scale_width = (ui->frameGraph->width()-X_PADDING*2)/(double)mHistoSize;
   return ui->frameGraph->x() + X_PADDING + i*scale_width;
}

double XImgToolStretchDialog::flux2HistoIndex(double flux)
{
    return RANGECHANGE(flux, mFluxMin, mFluxMax, 0, mHistoSize);
}

double XImgToolStretchDialog::histoIndex2Flux(double idx)
{
    return RANGECHANGE(idx, 0, mHistoSize, mFluxMin, mFluxMax);
}


void XImgToolStretchDialog::computeHistogram(float min, float max)
{
    if (!mHistogram) mHistogram = new int[mHistoSize];
    memset(mHistogram, 0, sizeof(int)*mHistoSize);
    bool chk = ui->buttonZoom->isChecked();
    int phsize = parent()->fitimage()->histoSZ();
    double pscale;

    mFluxMin =  min; //MAXVAL(0, min);
    mFluxMax =  max;//MINVAL(65535, max);
    mLowerMarkH = flux2HistoIndex(parent()->cProfile()->min());
    mUpperMarkH = flux2HistoIndex(parent()->cProfile()->max());

    //Histograma de la imagen, posición de inicio y fin
    int start_big, end_big;
    if (!chk)
    {
        start_big = 0;
        end_big = phsize-1;
    }
    else
    {
        start_big = RANGECHANGE(parent()->cProfile()->min(), 0, phsize, mFluxMin, mFluxMax);
        end_big = RANGECHANGE(parent()->cProfile()->max(), 0, phsize, mFluxMin, mFluxMax);
        if (start_big <= 0) start_big = 0; else if (start_big >= phsize) start_big = phsize-1;
        if (end_big <= 0) end_big = 0; else if (end_big >= phsize) end_big = phsize-1;
    }
    pscale = (double)mHistoSize / (end_big-start_big-1);


    for (int i=start_big; i<=end_big; i++)
    {
        int idx = (i-start_big) * pscale;
        if (idx < 0) idx = 0;
        else  if (idx >= mHistoSize) idx = mHistoSize-1;
        mHistogram[idx] += parent()->fitimage()->histogram()[i];
    }
}


void XImgToolStretchDialog::computePoly(bool update_stats)
{
    if (update_stats)
    {
        mHistoMinVal = 1<<30;
        mHistoMaxVal = -1;
        for (int i=0; i<mHistoSize; i++)
        {
            if (mHistogram[i] > mHistoMaxVal) mHistoMaxVal = mHistogram[i];
            if (mHistogram[i] < mHistoMinVal) mHistoMinVal = mHistogram[i];
        }
    }

    //Calcular la polilínea
    double swidth = (ui->frameGraph->width() - X_PADDING*2) /(double)mHistoSize;
    double fheight = ui->frameGraph->height() - Y_PADDING*2;

    for (int i=0; i<mHistoSize; i++)
    {
        double pos_x = ui->frameGraph->x() + X_PADDING + i * swidth;

        double  pos_y =
         mLogaritmicY

         ?ui->frameGraph->y() + Y_PADDING +
                        RANGECHANGE(mHistogram[i]<1?0:log10(mHistogram[i]),
                                    (double)mHistoMinVal<1?0:log10(mHistoMinVal), (double)log10(mHistoMaxVal), (double)fheight, 0.0)

         :ui->frameGraph->y() + Y_PADDING +
                                RANGECHANGE(mHistogram[i],
                                            (double)mHistoMinVal, mHistoMaxVal, (double)fheight, 0.0);

        mPolyLine[i] = QPointF(pos_x, pos_y);
    }
    //Nos aseguramos que la línea cierra correctamente bajando hacia el borde inferior y volviendo atrás
    mPolyLine[mHistoSize]  = QPointF(ui->frameGraph->x() + X_PADDING + (mHistoSize-1) * swidth, fheight);
    mPolyLine[mHistoSize+1]= QPointF(ui->frameGraph->x() + X_PADDING, fheight);

    //Establecemos las marcas del histograma
    mLowerMarkH = flux2HistoIndex(parent()->cProfile()->min());
    mUpperMarkH = flux2HistoIndex(parent()->cProfile()->max());
}

void XImgToolStretchDialog::paintEvent(QPaintEvent* pe)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHints(QPainter::Antialiasing, false);
    painter.setViewport(window()->rect());
    paintGraph(&painter);
    painter.end();
    QDialog::paintEvent(pe);
}

void XImgToolStretchDialog::showEvent(QShowEvent *se)
{
    if (mReseted)
    {
        mLowerMarkH = flux2HistoIndex(mInitialMin = parent()->cProfile()->min());
        mUpperMarkH = flux2HistoIndex(mInitialMax = parent()->cProfile()->max());
        mReseted = false;
    }
    QDialog::showEvent(se);
}


void XImgToolStretchDialog::paintGraph(QPainter* painter)
{
    //Rctángulo de fondo y malla de fondo
    painter->fillRect(ui->frameGraph->x(), ui->frameGraph->y(), ui->frameGraph->width(), ui->frameGraph->height(), Qt::white);
    painter->setPen(mMeshPen);

    int st_x = ui->frameGraph->x() + (ui->frameGraph->width() - ((int)floor( ui->frameGraph->width()/MESH_WIDTH_X)) * MESH_WIDTH_X)/2;
    for (int x = st_x; x<ui->frameGraph->width(); x+= MESH_WIDTH_X)
        painter->drawLine(x, ui->frameGraph->y(), x, ui->frameGraph->y()+ui->frameGraph->height()-1);

    int st_y = ui->frameGraph->y() + (ui->frameGraph->height() - ((int)floor( ui->frameGraph->height()/MESH_WIDTH_Y)) * MESH_WIDTH_Y)/2;
    for (int y = st_y; y<ui->frameGraph->height(); y+= MESH_WIDTH_Y)
        painter->drawLine(ui->frameGraph->x(), y, ui->frameGraph->x() + ui->frameGraph->width()-1,  y);

    //Pintar el histograma
    if (!mHistogram) return;


    painter->setPen(mLinePen);
    painter->setBrush(mPolyBrush);
    //Dibujado en base a líneas. Se podría dibujar un área con painter->drawPolygon(mPolyLine, mHistoSize+3);
    int start = flux2HistoIndex(mFluxMin); if (start <= 0) start = 0;
    int end = flux2HistoIndex(mFluxMax);   if (end >= mHistoSize) end = mHistoSize-1;
    double scale_width = (ui->frameGraph->width() - X_PADDING*2)/(double)(mHistoSize);

    for (int i=0; i<=mHistoSize; i++)
    {
        if (mPolyLine[i].y()+Y_PADDING < ui->frameGraph->y()+ui->frameGraph->height()-1)
        painter->drawLine(QPointF(mPolyLine[i].x(), mPolyLine[i].y()+Y_PADDING),
                          QPointF(mPolyLine[i].x(), ui->frameGraph->y()+ui->frameGraph->height()-1));
    }

    //Marcas de límites menor y mayor
    painter->setPen(mOverMark == 1 ? Qt::red : Qt::black);
    painter->drawLine(QPointF(ui->frameGraph->x() + X_PADDING  + mLowerMarkH * scale_width,  ui->frameGraph->y()),
                      QPointF(ui->frameGraph->x() + X_PADDING  + mLowerMarkH * scale_width,
                              ui->frameGraph->y()+ui->frameGraph->height()-1));

    painter->setPen(mOverMark == 2 ? Qt::red : Qt::black);
    painter->drawLine(QPointF(ui->frameGraph->x() + X_PADDING  + mUpperMarkH * scale_width,  ui->frameGraph->y()),
                      QPointF(ui->frameGraph->x() + X_PADDING  + mUpperMarkH * scale_width,
                              ui->frameGraph->y()+ui->frameGraph->height()-1));
}

void XImgToolStretchDialog::mouseMoveEvent(QMouseEvent* me)
{
    double scale_width = (ui->frameGraph->width()-X_PADDING*2)/(double)mHistoSize;
    double lower_x = ui->frameGraph->x() + X_PADDING  + mLowerMarkH * scale_width;
    double upper_x = ui->frameGraph->x() + X_PADDING  + mUpperMarkH * scale_width;
    //¿Estoy dentro del área de frameGraph?
    if (!mMovingMark)
    {
        if (me->y() > ui->frameGraph->y() && me->y() < ui->frameGraph->y()+ui->frameGraph->height())
        {
            if (fabs(me->x()-upper_x) < MARK_SPACING && fabs(me->x()-upper_x) < fabs(me->x()-lower_x))
            {
                if (mOverMark != 2)
                {
                    ui->frameGraph->setCursor(QCursor(Qt::SizeHorCursor));
                    mOverMark = 2;
                    update();
                }
            }
            else if(fabs(me->x()-lower_x) < MARK_SPACING)
            {
                if (mOverMark != 1)
                {
                    ui->frameGraph->setCursor(QCursor(Qt::SizeHorCursor));
                    mOverMark = 1;
                    update();
                }
            }
            else if (mOverMark)
            {
                ui->frameGraph->setCursor(QCursor(Qt::CrossCursor));
                mOverMark = 0;
                update();
            }
        }
    }
    else if (mMovingMark)
    {
        double ant_pos = 0;
        //Calculamos la nueva posición de la marca.
        //Recordemos evitar que las marcas coincidan o sean muy cercanas
        double newpos = (me->x() - (ui->frameGraph->x()+X_PADDING)) / scale_width;
        if (newpos < 0) newpos = 0;
        if (newpos >= mHistoSize) newpos = mHistoSize;
        if (mMovingMark == 1)
        {
            ant_pos = mLowerMarkH;
            if (newpos < mUpperMarkH)
                mLowerMarkH = newpos;
            else newpos = mLowerMarkH = mUpperMarkH-1;
        }
        else
        {
            ant_pos = mUpperMarkH;
            if (newpos > mLowerMarkH)
                mUpperMarkH = newpos;
            else newpos = mUpperMarkH = mLowerMarkH+1;
        }
        if (fabs(ant_pos-newpos) > 0.5)
        {
            double flux_min = histoIndex2Flux(mLowerMarkH);
            double flux_max = histoIndex2Flux(mUpperMarkH);
            parent()->cProfile()->setMin(flux_min);
            parent()->cProfile()->setMax(flux_max);
            parent()->cProfile()->Converge(((MdiFitImage*)parent())->image());
            update();
            parent()->update();
        }
    }
}

void XImgToolStretchDialog::mousePressEvent(QMouseEvent* me)
{
    double scale_width = (ui->frameGraph->width()-X_PADDING*2)/(double)mHistoSize;
    double lower_x = ui->frameGraph->x() + X_PADDING + mLowerMarkH * scale_width;
    double upper_x = ui->frameGraph->x() + X_PADDING + mUpperMarkH * scale_width;
    if (mOverMark && !mMovingMark)
    {
        mMovingMark = mOverMark;
        mDespMark = (mMovingMark==1 ? lower_x : upper_x) - (me->x());
        update();
    }
}

void XImgToolStretchDialog::mouseReleaseEvent(QMouseEvent*)
{
    if (mMovingMark)
    {
        mMovingMark = 0;
        update();
    }
}

void  XImgToolStretchDialog::SetZoom_Slot()
{
  //  if (!isVisible()) return;
    if (ui->buttonZoom->isChecked())
    {
        double range = histoIndex2Flux(mUpperMarkH) - histoIndex2Flux(mLowerMarkH);
        double minval = histoIndex2Flux(mLowerMarkH) - range/3.0;
        double maxval = histoIndex2Flux(mUpperMarkH) + range/3.0;
        computeHistogram(MAXVAL(minval, mFluxMin), MINVAL(maxval, mFluxMax));
        computePoly(true);
        update();
    }
    else
    {
        computeHistogram(parent()->fitimage()->minpix(), parent()->fitimage()->maxpix());
        computePoly(true);
        update();
    }
}

void  XImgToolStretchDialog::Reset_Slot()
{
   parent()->cProfile()->SetOptimumFor(parent()->fitimage());
   parent()->cProfile()->Converge(((MdiFitImage*)parent())->image()); 
   computePoly(false);
   //Cuidado, si las marcas estamos mostrando zoom y las marcas se salen fuera
   if (ui->buttonZoom->isChecked() && (histoIndex2Flux(mLowerMarkH) < mFluxMin || histoIndex2Flux(mUpperMarkH) > mFluxMax))
       ui->buttonZoom->setChecked(false);
   else
    update();
   parent()->update();
}

void XImgToolStretchDialog::Cancel_Slot()
{
    mReseted = true;
    parent()->cProfile()->setMin(mInitialMin);
    parent()->cProfile()->setMax(mInitialMax);
    parent()->cProfile()->Converge(((MdiFitImage*)parent())->image());
    update();
    parent()->update();
}

void XImgToolStretchDialog::Accept_Slot()
{
    mReseted = true;
}



