#ifndef XIMGTOOL_H
#define XIMGTOOL_H

#include <QObject>
#include <QWidget>
#include <QDialog>

class MdiFitImage;
class XImgTool : public QObject
{
    Q_OBJECT

private:
    QDialog*    mDialog;
    bool        mFirstTime;
    bool        mFirstTimeDialog;
    QPoint      mDialogPos;
    QString     mToolConfigSection;

protected:
    void        setDialog(QDialog* d);
    QDialog*    dialog(){return mDialog;}
    void        saveDialogPos();
    void        recoverDialogPos();
    QString     cfgSection(){return mToolConfigSection;}

public:
    explicit XImgTool(MdiFitImage *parent, QString toolName);
    virtual ~XImgTool();

    //Métodos que deben ser llamados por la ventana que gestiona esta capa
    //************************************************************************
    virtual bool paintEvent(QPaintEvent*, QPainter*);           /** Se produce un evento de dibujado en la ventana padre **/
    virtual bool wheelEvent(QWheelEvent*);                      /** Movimiento de la rueda del ratón, la mayoría de las veces sin implementar*/
    virtual bool imageMoveEvent();                              /** La imagen se mueve. Implementado */
    virtual bool mousePressEvent(QMouseEvent* );                /** Se pulsa algún botón del ratón. Implementado **/
    virtual bool mouseReleaseEvent(QMouseEvent *);              /** Implementado **/
    virtual bool mouseMoveEvent(QMouseEvent *);                 /** Implementado **/
    virtual bool resizeEvent(QResizeEvent *);                   /** Evento de cambio de escala de la imagen */
    virtual bool contextMenuRequest(QPoint);
    virtual bool keyPressEvent(QKeyEvent* event);               /** Se pulsa una tecla **/
    virtual bool zoomChangeEvent(float newzoom);                /** Cambia el zoom de pantalla**/
    virtual bool activate();                                    /** Devuelve true si permitió la desactivación*/
    virtual bool deactivate();                                  /** Devuelve true si permitió la desactivación*/


    QString name() const {return objectName();}
    MdiFitImage* parent(){return (MdiFitImage*)QObject::parent();}

signals:

public slots:


};

#endif // XIMGTOOL_H
