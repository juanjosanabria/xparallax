#ifndef XIMGTOOLLOCATE_H
#define XIMGTOOLLOCATE_H

#include "ximgtool.h"
#include "../overlay/xoverlay.h"
#include "../inputdialog.h"

class XImgToolLocate : public XImgTool
{
    Q_OBJECT

private:
    XOverlayLayer*          mLayer;
    XOverlayStyle*          mStyle;
    InputDialog*            mInputDialog;

public:
    explicit XImgToolLocate(MdiFitImage *parent);
    ~XImgToolLocate();

    bool activate();

signals:


public slots:

};

#endif // XIMGTOOLLOCATE_H
