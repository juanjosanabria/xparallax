#include "ximgtoolgif.h"
#include "../types.h"
#include "../modext/blink/mdifitblink.h"
#include "../utilities/gif.h"
#include <QtConcurrent>
#include <QFileDialog>

XImgToolGif::XImgToolGif(MdiFitImage *parent) :
    XImgTool(parent, TOOL_GIF)
{
    mLayer = NULL;
    mRect = NULL;
    mStyle = NULL;
    mDrawing = false;
    mDrawn = true;
    mImageBuffer = NULL;
}

XImgToolGif::~XImgToolGif()
{
    if (mImageBuffer) delete mImageBuffer;
    mImageBuffer = NULL;
}

bool XImgToolGif::activate()
{
    //Si no hay una capa de "User drawing" la creamos
    for (int i=0; i<parent()->xLayers()->count() && !mLayer; i++)
    {
        if (parent()->xLayers()->at(i)->name() == XLAYER_USER_DRAWING)
            mLayer = parent()->xLayers()->at(i);
    }
    if (!mLayer)
    {
        mLayer = new XOverlayLayer(XLAYER_USER_DRAWING, parent());
        parent()->addXLayer(mLayer, XLAYER_USER_DRAWING_PRIORITY);
    }
    mLayer->setVisible(false);
    parent()->ShowHideLayer_Slot(mLayer);
    //Creamos el estilo si no lo hubiera
    if (!mLayer->styles()->contains(XGROUP_TOOL_GIF))
    {
        mStyle = mLayer->addStyle(XGROUP_TOOL_GIF);
        mStyle->setBorder(Qt::lightGray, 1, Qt::DashLine);
    }
    else mStyle = mLayer->styles()->value(XGROUP_TOOL_GIF);
    //Actualizamos la ventana padre
    parent()->update();
    return true;
}

bool XImgToolGif::deactivate()
{
    mDrawing = false;
    if (mRect) mLayer->removeItem(mRect);
    mRect = NULL;
    parent()->update();
    return true;
}

bool XImgToolGif::mousePressEvent(QMouseEvent* ev)
{
    if (ev->button() != Qt::LeftButton) return false;
    parent()->ScreenXYToImageXY(ev->x(), ev->y(), mPointImgStart);
    mPointImgEnd = mPointImgStart;
    //Creamos el segmento y lo inicializamos
    mRect = new XOverlayRect(mLayer, XGROUP_TOOL_GIF);
    mRect->setDeletable(true);
    mRect->setImageAttatched(true);
    mRect->setZoomAttatched(true);
    mRect->setRect(mPointImgStart.x(), mPointImgStart.y(), 0, 0);
    mRect->setPos(ev->pos());
    mDrawing = true;
    return false;
}

bool XImgToolGif::mouseReleaseEvent(QMouseEvent *ev)
{
    if (ev->button() != Qt::LeftButton) return false;
    if (!mDrawing) return true;
    //Ahora guardamos el GIF
    MdiFitBlink* pt = (MdiFitBlink*)parent();
    QString fname =  QFileDialog::getSaveFileName(pt,"Save GIF sequence","","GIF file (*.gif);;HTM webpage (*.htm)");
    mRect->setVisible(false);
    if (fname.length())
    {
        QFileInfo fi(fname);
        if (fi.suffix().toLower() == "gif")
            saveGif(mRect->rect(), fname);
        else if (fi.suffix().toLower() == "htm")
            saveHtm(mRect->rect(), fname);
    }
    //Deseleccionamos la herramienta
    pt->ScreenXYToImageXY(ev->x(), ev->y(), mPointImgEnd);
    pt->update();
    mDrawing = false;
    pt->DeselectCurrentTool_Slot();
    return false;
}

bool XImgToolGif::saveGif(QRectF rect, QString fname)
{
    MdiFitBlink* pt = (MdiFitBlink*)parent();
    int px = (int)rect.x();
    int py = (int)rect.y();
    int width = (int)rect.width();
    int height = (int)rect.height();
    if (mImageBuffer) delete mImageBuffer;
    mImageBuffer = new uint8_t[width*height*4];

    GIF::GifWriter writer;
    if (!GIF::GifBegin(&writer, fname.toLatin1().data(), width, height, 50))
    {
        QMessageBox::warning(pt, "Error", "Unable to create GIF output file");
        return false;
    }
    int old_idx = pt->currentIdx();
    QImage img(width, height, QImage::Format_ARGB32);
    for (int i=0; i<pt->images()->count(); i++)
    {
        QPainter painter(&img);
        pt->MoveTo(i);
        pt->render(&painter, QPoint(0, 0), QRegion(px, py, width, height));
        for (int y = 0; y<height; y++)
            memcpy(mImageBuffer + y*width*4, img.scanLine(y), width*4);
        GIF::GifWriteFrame(&writer, mImageBuffer, width, height, 50);
    }
    pt->MoveTo(old_idx);
    if (mImageBuffer) delete mImageBuffer;
    mImageBuffer = NULL;
    if (!GIF::GifEnd(&writer))
    {
        QMessageBox::warning(pt, "Error", "Unable to create GIF output file");
        return false;
    }
    return true;
}

bool XImgToolGif::saveHtm(QRectF rect, QString fname)
{
    MdiFitBlink* pt = (MdiFitBlink*)parent();
    int px = (int)rect.x();
    int py = (int)rect.y();
    int width = (int)rect.width();
    int height = (int)rect.height();
    QString id = QUuid::createUuid().toString();
    //Guardar imágenes en vertical
    int old_idx = pt->currentIdx();
    QImage img(width, height*pt->images()->count(), QImage::Format_RGB32);
    QPainter painter(&img);
    for (int i=0; i<pt->images()->count(); i++)
    {
        pt->MoveTo(i);
        pt->render(&painter, QPoint(0, i*height), QRegion(px, py, width, height));
    }
    pt->MoveTo(old_idx);
    //Guardar la imagen a un archivo temporal png
    QTemporaryFile tf; QFile fil(fname);
    if (!tf.open() || !img.save(&tf, "png", 8) || !fil.open(QFile::WriteOnly)) return false;
    //Crear el archivo htm
    fil.write(QString("<script type='text/javascript'>\n"
                "setInterval(function(){"
                 "if (typeof this.IPOS =='undefined'){this.IPOS=0;this.ICONT=%1;this.IHEIGHT=%2;}"
                 "this.IPOS=(this.IPOS+1)%this.ICONT;document.getElementById('%3').style.backgroundPositionY=(-this.IPOS*this.IHEIGHT)+'px';"
                 "}, 500);"
              "</script>\n").arg(pt->images()->count()).arg(height).arg(id).toLatin1().data()
              );
    tf.seek(0);
    QString base64str = QString(tf.readAll().toBase64());

    fil.write(QString("<div id='%1' style='display:block; width:%2; height:%3; background-image:url(data:image/png;base64,%4); background-position:0 0;'>\n")
              .arg(id).arg(width).arg(height).arg(base64str).toLatin1().data());


    fil.write("</div>\n");
    return true;
}

bool XImgToolGif::mouseMoveEvent(QMouseEvent *ev)
{
    if (!mDrawing) return true;
    mDrawn = true;
    parent()->ScreenXYToImageXY(ev->x(), ev->y(), mPointImgEnd);
    double min_x = MINVAL(mPointImgStart.x(), mPointImgEnd.x());
    double max_x = MAXVAL(mPointImgStart.x(), mPointImgEnd.x());
    double min_y = MINVAL(mPointImgStart.y(), mPointImgEnd.y());
    double max_y = MAXVAL(mPointImgStart.y(), mPointImgEnd.y());
    mRect->setRect(min_x, min_y, max_x-min_x, max_y - min_y);
    QPointF paux;
    parent()->ImageXYToScreenXY(QPointF(min_x, min_y),paux);
    mRect->setPos(paux);
    parent()->update();
    return false;
}
