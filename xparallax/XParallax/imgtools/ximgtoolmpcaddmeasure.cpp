#include "ximgtoolmpcaddmeasure.h"
#include "../types.h"
#include "../mdifitimage.h"
#include "../inputdialog.h"
#include "mpc.h"
#include "mainwindow.h"
#include <QMessageBox>

XImgToolMPCAddMeasure::XImgToolMPCAddMeasure(MdiFitImage *parent)
    : XImgTool(parent, TOOL_MPC_ADD_MEASURE)
{
    mDetector = NULL;
    mDetectedStar = NULL;
    mDateObs = MpcUtils::GetDateFrame(parent->fitimage()->headers());
}

XImgToolMPCAddMeasure::~XImgToolMPCAddMeasure()
{
    if (mDetector) delete mDetector;
    mDetector = NULL;
}

bool XImgToolMPCAddMeasure::checkPrevious()
{
    //Existe reducción astrométrica
    if (!parent()->fitimage()->plate())
    {
        QMessageBox::warning(
                    parent(), "Warning",
                    "No astrometric reduction found.\r\nA valid astrometric reduction is needed to place objects on the image.");
        return false;
    }
    //Existe una fecha válida para la imagen
    if (MpcUtils::GetDateFrame(parent()->fitimage()->headers()) == INVALID_DATETIME)
    {
        QMessageBox::warning(
                    parent(), "Warning",
                    "No valid date/time header found.<br/><br/>A valid <b>DATE-OBS</b> header is needed.");
        return false;
    }
    return true;
}

bool XImgToolMPCAddMeasure::activate()
{
    if (!checkPrevious()) return false;
    //Hago todos los items hoverables
    XOverlayLayer* layer  = parent()->getXlayer(XLAYER_MPC_REPORT);
    for (int i=0;layer && i<layer->items()->count(); i++)
        layer->items()->at(i)->setHoverable(true);
    return true;
}

bool XImgToolMPCAddMeasure::deactivate()
{
    //Hago que ningún item sea hoverable
    XOverlayLayer* layer  = parent()->getXlayer(XLAYER_MPC_REPORT);
    for (int i=0;layer && i<layer->items()->count(); i++)
        layer->items()->at(i)->setHoverable(false);
    return true;
}


bool XImgToolMPCAddMeasure::mousePressEvent(QMouseEvent* ev)
{
    mPressXY = ev->pos();
    return false;
}

bool XImgToolMPCAddMeasure::mouseReleaseEvent(QMouseEvent *ev)
{
    //Solamente respondo a clicks del botón izquierdo
    if (ev->button() != Qt::LeftButton) return true;
    //Si me he movido de posición durante el click no hago nada
    if (CART_DIST(mPressXY.x(), mPressXY.y(), ev->pos().x(), ev->pos().y()) > 5) return true;
    //Cálculo de las coordenadas del click
    QPointF imgxy;
    parent()->ScreenXYToImageXY(ev->pos(), imgxy);
    int x = (int)FROUND(imgxy.x());
    int y = (int)FROUND(imgxy.y());
    if (x < 0) x = 0; else if (x >= parent()->fitimage()->width()) x = parent()->fitimage()->width()-1;
    if (y < 0) y = 0; else if (y >= parent()->fitimage()->height()) y = parent()->fitimage()->height()-1;
    mDetPoint.setX(x); mDetPoint.setY(y);
    //Comprobar si estamos sobre alguna marca anterior
    XOverlayLayer* layer = parent()->getXlayer(XLAYER_MPC_REPORT);
    for (int i=0;layer && i<layer->items()->count(); i++)
    {
        if (layer->items()->at(i)->hover())
        {
            MainWindow::mainwin->ActiveMPC()->EditMeasureByCoords(layer->items()->at(i)->bx(),layer->items()->at(i)->by());
            return false;
        }
    }
    //Buscar una fuente alrededor de la zona de click
    DetectedStar* ds = searchSource(x, y);
    if (!ds)
    {
        QMessageBox::warning(
                    parent(), "Warning",
                    "No object detected in the selected position.\r\nMaybe there are no objects in the area.");

    }
    else
    {        
        //Añadimos la estrella al MPC REPORT
        float mag = getMag(ds->fluxISO());
        MPCReport *report = MainWindow::mainwin->ActiveMPC()->report();
        MPCReportMeasure mea;
        mea.setBand(IS_VALIDF(mag) ? 'R' : ' ');
        mea.setMag(IS_VALIDF(mag) ? mag : 0);
        mea.setObsCode(report->hdrByLine(0) && report->hdrByLine(0)->hdr() == "COD" ? report->hdrByLine(0)->text().trimmed() : "XXX");
        mea.setDateObs(mDateObs = MpcUtils::GetDateFrame(parent()->fitimage()->headers()));
        mea.setRa(ds->ra()); mea.setDec(ds->dec());
        if (existsByRADEC(ds->ra(), ds->dec()))
        {
            QMessageBox::warning(this->parent(), "Error", "Unable to add object.<br/><br/>"
            "A previous object exists in the position.<br/>RA: " + Util::RA2String(ds->ra())
                                 + " DEC: " + Util::DEC2String(ds->dec()));
            return false;

        }
        //Solicitamos al usuario el identificador del objeto
        char desig[32]; char aux; int number; MPCReportMeasure::MeasureType type;
        do
        {
            if (!selectObjectNumber(ds->ra(), ds->dec(), &number, desig, &aux, &type)) return false;
            if (!number && !strlen(desig))
             QMessageBox::warning(this->parent(), "Error",
             "Invalid object id. Please, use a number or packed ID for minor planets or "
             "a 7 character provisional designation.");
        }
        while (!number && !strlen(desig));
        if (type == MPCReportMeasure::MINOR_PLANET)
        {
            mea.setType(MPCReportMeasure::MINOR_PLANET);
            if (number)
            {
                //Cuidado, como no es designación temporal compruebo que exista
                if (!MpcOrb::findMP(MpcUtils::pack5Number(number)).valid())
                {
                    QMessageBox::warning(this->parent(), "Error", "Unknown minor planet number.<br/><br/>"
                    "Please, check your MP number or download an up to date version of <b>MPCORB.DAT</b>.<br/><br/>"
                    "For temporary designations of unknown objects, please, use an unique 7 character string."
                    , QMessageBox::Ok);
                    return false;
                }
                mea.setNumber(number);
            }
            else mea.setDesignation(desig);
        }
        else if (type == MPCReportMeasure::COMET)
        {
            mea.setType(MPCReportMeasure::COMET);
            mea.setComtOrbit(aux);
            if (number)
            {
                mea.setNumber(number);
                if (!MpcOrb::findComet(mea.id()).valid())
                {
                    QMessageBox::warning(this->parent(), "Error", "Unknown comet planet number.<br/><br/>"
                    "Please, check your comet number or download an up to date version of <b>MPC orbit files</b>.<br/><br/>"
                    "For temporary designations of unknown comets, please, use an unique 7 character string."
                    , QMessageBox::Ok);
                    return false;
                }
            }
            else mea.setDesignation(desig);
        }
        else //Natural satellite
        {
            mea.setType(MPCReportMeasure::NATURAL_SATELLITE);
            mea.setPlanet(aux);
            if (number)  mea.setNumber(number);
            else mea.setDesignation(desig);
        }
        if (existsByID(mea.id()))
        {
            QMessageBox::warning(this->parent(), "Error", "Unable to add object.<br/><br/>"
            "A previous object with the same number or designation exists in the image.");
            return false;
        }
        //Comprobamos que lo que vamos a añadir es correcto
        if (checkNewMeasure(&mea))
        {
            report->addMeasure(mea);
            MainWindow::mainwin->ActiveMPC()->UpdateTextArea();
        }
        //Marcamos en la pantalla la medida
        parent()->MPC_AddMark(&mea, ds->radius());
        MainWindow::mainwin->ActiveMPC()->UpdateMPCLayers();
    }
    return false;
}

bool XImgToolMPCAddMeasure::checkNewMeasure(MPCReportMeasure* mea)
{
    //No existe otra medida previa para este objeto y esta imagen

    return mea;
}

bool XImgToolMPCAddMeasure::mouseMoveEvent(QMouseEvent *)
{
    return true;
}

DetectedStar*  XImgToolMPCAddMeasure::searchSource(int ssx, int ssy)
{
    //Para evitar calcular fondos enormes solamente trabajamos en la caja MPC_MEASURE_BOX_SIZE
    int despx = ssx - MPC_MEASURE_BOX_SIZE/2;
    int despy = ssy - MPC_MEASURE_BOX_SIZE/2;
    for (int y=0; y<MPC_MEASURE_BOX_SIZE; y++)
    {
        int imgy = y + despy;
        for (int x=0; x<MPC_MEASURE_BOX_SIZE; x++)
        {
            int imgx = x + despx;
            mData[x + y*MPC_MEASURE_BOX_SIZE] =
                    imgx < 0 || imgy < 0 || imgx >= parent()->fitimage()->width() || imgy >= parent()->fitimage()->height() ?
                    0 :
                    parent()->fitimage()->data()[imgx + imgy*parent()->fitimage()->width()];
        }
    }
    if (mDetector) delete mDetector;
    mDetector = new StarDetector(mData, MPC_MEASURE_BOX_SIZE, MPC_MEASURE_BOX_SIZE);
    mDetector->setLog(false);
    DetectedStar* pt = mDetector->DetectStars(0, 0, MPC_MEASURE_BOX_SIZE-1, MPC_MEASURE_BOX_SIZE-1);
    mDetectedStar = NULL;
    //Buscar la estrella más cercana al punto pulsado por el usuario
    for (int i=0; i<mDetector->count(); i++)
    {
        //Desplazamos la estrella detectada a su posición real en la imagen
        double ra, dec;
        pt[i].setX(pt[i].x()+despx);
        pt[i].setY(pt[i].y()+despy);
        parent()->map(pt[i].x(), pt[i].y(), &ra, &dec);
        pt[i].setRa(ra);
        pt[i].setDec(dec);
        if (!mDetectedStar) mDetectedStar = pt+i;
        else
        {
            double old_dist = CART_DIST(ssx, ssy, mDetectedStar->x(), mDetectedStar->y());
            double new_dist = CART_DIST(ssx, ssy, pt[i].x(), pt[i].y());
            if (new_dist < old_dist) mDetectedStar = pt+i;
        }
    }
    return mDetectedStar;
}

float XImgToolMPCAddMeasure::getMag(double flux)
{
    FitHeader fh = parent()->fitimage()->headers()->Contains("MAGZEROP");
    if (fh.isInvalid()) return INVALID_FLOAT;
    bool ok = false;
    double magzerop = fh.value().toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    return (float)(magzerop - 2.5*log10(flux));
}

bool XImgToolMPCAddMeasure::existsByID(const QString& id)
{
    ASSERT(MainWindow::mainwin->ActiveMPC(),"MPC does not exits");
    //Obtener la fecha de mi imagen
    MPCReport* rep = MainWindow::mainwin->ActiveMPC()->report();
    //Comprobamos
    for (int i=0; i<rep->measures()->count(); i++)
    {
        if (abs(mDateObs.secsTo(rep->measures()->data()[i].dateObs())) > 2) continue;
        if (rep->measures()->data()[i].id() == id) return true;
    }
    return false;
}

bool XImgToolMPCAddMeasure::existsByRADEC(double ra, double dec)
{
    ASSERT(MainWindow::mainwin->ActiveMPC(),"MPC does not exits");
    MPCReport* rep = MainWindow::mainwin->ActiveMPC()->report();
    //Normalizamos el ID
    for (int i=0; i<rep->measures()->count(); i++)
    {
        if (abs(mDateObs.secsTo(rep->measures()->data()[i].dateObs())) > 2) continue;
        if (Util::Adist(ra, dec, rep->measures()->data()[i].ra(), rep->measures()->data()[i].dec()) < 8.33e-4) return true;
    }
    return false;
}


bool XImgToolMPCAddMeasure::selectObjectNumber(double ra, double dec,int* number, char* desig,char *aux, MPCReportMeasure::MeasureType* mt)
{
    bool ok;
    *desig = 0; *number = 0; *mt = MPCReportMeasure::INVALID;
    InputDialog id("", MainWindow::mainwin);
    id.SetImageBehaivour(QString("0;Minor Planet;:/graph/images/icn_asteroid.svg;0;Comet;:/graph/images/icn_comet.svg;0;Natural satellite;:/graph/images/icn_nsatellite.svg"));
    id.SetImage(QPixmap(":/graph/images/icn_asteroid.svg"));
    id.SetTitle(QString("Add measure"));
    id.SetSubTitle(QString("RA: %1 DEC:%2").arg(Util::RA2String(ra),Util::DEC2String(dec)));

    QStringList titles; titles << "Type" << "Number/Desig." << "Orbit" << "Planet";
    QStringList values; values << "" << "" << "" << "";
    QStringList mask; mask << "COMBO:Minor Planet;Comet;Natural satellite" << ""
                           << "COMBO:/0,Comet:-;C - long-period comet;P - short-period comet;D - defunct' comet;X - uncertain comet;A - MP given a cometary designation"
                           << "COMBO:/0,Natural satellite:-;J - Jupiter;S - Saturn;U - Uranus;N - Neptune";

    QStringList res = id.ShowMultipleTexts(titles, values, mask);
    if (res.length() != 4) return false;
    for (int i=0; i<res.count(); i++) res[i] = res[i].trimmed();

    if (res.at(0) == "Minor Planet")
    {
        *mt = MPCReportMeasure::MINOR_PLANET;
        if (res.at(1).toInt(&ok) > 0 && ok)
        {
            QString pack = MpcUtils::pack5Number(res.at(1).toInt());
            if (!pack.length())
            {
                QMessageBox::warning(
                            parent(), "Warning",
                            "Invalid minor planet number.<br/>Please, set the minor planet number to a valid value.");
                return false;

            }
            *number = res.at(1).toInt(&ok);
            return true;
        }
        else if (MpcUtils::isMinorPlanet(res.at(1)))
        {
            *number = MpcUtils::unpack5Number(res.at(1));
            return true;
        }
        else if (MpcUtils::isProvDesig(res.at(1)))
        {
            strcpy(desig, res.at(1).toLatin1().data());
            return true;
        }
        else
        {
            QMessageBox::warning(
                        parent(), "Warning",
                        "Invalid minor planet number.<br/>Please, set the minor planet number to a valid value.");
            return false;
        }
    }
    else if (res.at(0) == "Comet")
    {
        *mt = MPCReportMeasure::COMET;
        *number = res.at(1).toInt(&ok);
        if (*number && ok)
        {
            if (*number > 9999 || *number <= 0)
            {
                QMessageBox::warning(
                            parent(), "Warning",
                            "Invalid comet number.<br/>Please, set the comet number to a value between 0 and 9999.");
                return false;
            }
        }
        else
        {
            if (res.at(1).length() != 7)
            {
                QMessageBox::warning(
                            parent(), "Warning",
                            "Invalid comet provisional or temporary designation.<br/>Please, set the comet number to a 7 character value.");
                return false;
            }
             strcpy(desig, res.at(1).toLatin1().data());
        }
        if (res.at(2) == "-")
        {
            QMessageBox::warning(
                        parent(), "Warning",
                        "Invalid comet orbit.<br/>Please, select a comet orbit.");
            return false;
        }
        *aux = res.at(2).at(0).toLatin1();
    }
    else //Natural stellite
    {
        *mt = MPCReportMeasure::NATURAL_SATELLITE;
        *number = res.at(1).toInt(&ok);
        if (*number && ok)
        {
            if (*number > 999 || *number <= 0)
            {
                QMessageBox::warning(
                            parent(), "Warning",
                            "Invalid satellite number.<br/>Please, set the satellite number to a valid value.");
                return false;
            }
        }
        if (res.at(3) == "-")
        {
            QMessageBox::warning(
                        parent(), "Warning",
                        "Invalid planet.<br/>Please, select a valid planet.");
            return false;
        }
        *aux = res.at(3).at(0).toLatin1();
    }
    return true;
}
