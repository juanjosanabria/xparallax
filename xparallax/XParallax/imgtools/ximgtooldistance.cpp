#include "../mdifitimage.h"
#include "ximgtooldistance.h"

XImgToolDistance::XImgToolDistance(MdiFitImage *parent) :
    XImgTool(parent, TOOL_DISTANCE)
{
    mDrawing = false;
    mWarningShown = false;
    mDrawn = true;
    mLayer = NULL;
}

XImgToolDistance::~XImgToolDistance()
{

}

bool XImgToolDistance::activate()
{
    if (!parent()->fitimage()->plate() && !mWarningShown)
    {
        QMessageBox::warning(
                    parent(), "Warning",
                    "No astrometric reduction found.\r\nDistance will be shown in pixels");
        mWarningShown = true;
    }
    //Si no hay una capa de "User drawing" la creamos
    for (int i=0; i<parent()->xLayers()->count() && !mLayer; i++)
    {
        if (parent()->xLayers()->at(i)->name() == XLAYER_USER_DRAWING)
            mLayer = parent()->xLayers()->at(i);
    }
    if (!mLayer)
    {
        mLayer = new XOverlayLayer(XLAYER_USER_DRAWING, parent());
        parent()->addXLayer(mLayer, XLAYER_USER_DRAWING_PRIORITY);
    }
    mLayer->setVisible(false);
    parent()->ShowHideLayer_Slot(mLayer);
    //Creamos el estilo si no lo hubiera
    if (!mLayer->styles()->contains(XGROUP_TOOL_DISTANCE))
    {
        mStyle = mLayer->addStyle(XGROUP_TOOL_DISTANCE);
        mStyle->setBorderColor(Qt::red);
        mStyle->setHoverBorderColor(Qt::green);
        mStyle->setBoxBorder(Qt::gray, 1, Qt::DashLine);
    }
    else mStyle = mLayer->styles()->value(XGROUP_TOOL_DISTANCE);
    //Actualizamos la ventana padre
    parent()->update();
    return true;
}

bool XImgToolDistance::deactivate()
{
    mDrawing = false;
    parent()->update();
    return true;
}

bool XImgToolDistance::mousePressEvent(QMouseEvent* ev)
{
    parent()->ScreenXYToImageXY(ev->x(), ev->y(), mPointImgStart);
    mPointImgEnd = mPointImgStart;
    //Creamos el segmento y lo inicializamos
    mSegment = new XOverlaySegment(mLayer, XGROUP_TOOL_DISTANCE);
    mSegment->setHoverable(true);
    mSegment->setSelectable(true);
    mSegment->setDeletable(true);
    mSegment->setPaintLabel(true);
    mSegment->setLabelWhenHover(false);
    mSegment->setLabelFromPos(true);
    mSegment->setImageAttatched(true);
    mSegment->setStart(ev->pos());
    mSegment->setBaseStart(mPointImgStart);
    mSegment->setEnd(ev->pos());
    mSegment->setBaseEnd(mPointImgEnd);
    mSegment->setArrowHead(6);

    mDrawing = true;
    return false;
}

bool XImgToolDistance::mouseReleaseEvent(QMouseEvent *ev)
{
    if (!mDrawing) return true;
    parent()->ScreenXYToImageXY(ev->x(), ev->y(), mPointImgEnd);
    parent()->update();
    mDrawing = false;
    parent()->DeselectCurrentTool_Slot();
    return false;
}

bool XImgToolDistance:: mouseMoveEvent(QMouseEvent *ev)
{
    if (!mDrawing) return true;
    mDrawn = true;
    parent()->ScreenXYToImageXY(ev->x(), ev->y(), mPointImgEnd);
    mSegment->setEnd(ev->pos());
    mSegment->setBaseEnd(mPointImgEnd);
    QString dist;
    if (!parent()->plate())
    {
        dist = QString("").sprintf("%0.1f pix", CART_DIST(mPointImgStart.x(), mPointImgStart.y(), mPointImgEnd.x(), mPointImgEnd.y()));
    }
    else
    {
        //Distancia
        double dst = parent()->plate()->scale() * CART_DIST(mPointImgStart.x(), mPointImgStart.y(), mPointImgEnd.x(), mPointImgEnd.y());
        //Ángulo
        double pa =parent()->plate()->posAngXY(mPointImgStart.x(), mPointImgStart.y(), mPointImgEnd.x(), mPointImgEnd.y());
        //while (pa < 0) pa += 180;
        if (dst < 60) dist = QString("").sprintf("D=%0.1f\", φ=%0.2fº",dst, pa);
        else if (dst > 60 && dst < 3600) dist = QString("").sprintf("D=%0.1f', φ=%0.2fº",dst/60.0, pa);
        else dist = QString("").sprintf("D=%0.1fº, φ=%0.2fº", dst/3600, pa);

    }
    mSegment->setLabel(dist);
    parent()->update();
    return false;
}






















