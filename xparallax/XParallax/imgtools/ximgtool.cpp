#include "ximgtool.h"
#include "core.h"
#include "xglobal.h"
#include "../mdifitimage.h"
#include "../mainwindow.h"
#include <QDesktopWidget>

#define     CFG_DIALOG_POS      "dialog_pos"

XImgTool::XImgTool(MdiFitImage *parent, QString toolName) :
    QObject(parent)
{
    ASSERT(parent,"Error creating XImgTool. Parent can not be NULL");
    ASSERT(toolName.length(),"Error creating XImgTool. Name is NULL string");
    setObjectName(toolName);
    mToolConfigSection = "TOOL_" + toolName.toUpper().trimmed().replace(" ","_");
    mDialog = NULL;
    mFirstTime = true;
    mFirstTimeDialog = true;
}

XImgTool::~XImgTool()
{
    if (mDialog)
    Parameters::SavePoint(XGlobal::INI_SETTINGS, mToolConfigSection, CFG_DIALOG_POS, mDialogPos);

    if (mDialog) delete mDialog;
    mDialog = NULL;
}

void XImgTool::setDialog(QDialog*d)
{
    if(mDialog && mDialog != d)
        delete mDialog;
    if ((mDialog = d))
        MainWindow::DPIAware(mDialog, true);
}

bool XImgTool::paintEvent(QPaintEvent*, QPainter*)
{
    return true;
}

bool XImgTool::wheelEvent(QWheelEvent*)
{
    return true;
}

bool XImgTool::imageMoveEvent()
{
    return true;
}

bool XImgTool::mousePressEvent(QMouseEvent* )
{
    return true;
}

bool XImgTool::mouseReleaseEvent(QMouseEvent *)
{
    return true;
}

bool XImgTool::mouseMoveEvent(QMouseEvent *)
{
    return true;
}

bool XImgTool::keyPressEvent(QKeyEvent* ke)
{
    if (ke->key() == Qt::Key_Escape)
    {
        parent()->DeselectCurrentTool_Slot();
        return false;
    }
    return true;
}

bool XImgTool::resizeEvent(QResizeEvent *){return true;}

bool XImgTool::contextMenuRequest(QPoint){return true;}

bool XImgTool::zoomChangeEvent(float){return true;}

bool XImgTool::activate()
{
    return true;
}

bool XImgTool::deactivate()
{
    return true;
}

void XImgTool::saveDialogPos()
{
    if (mDialog)
    mDialogPos = mDialog->pos();
}

void XImgTool::recoverDialogPos()
{
    if (mDialog)
    {
        if (mFirstTimeDialog)
            mDialogPos = Parameters::ReadPoint(XGlobal::INI_SETTINGS, mToolConfigSection, CFG_DIALOG_POS, QPoint(-1000,-1000));
        if (mDialogPos.x() != -1000 && mDialogPos.y() != -1000 && mDialogPos.x() && mDialogPos.y())
        {
            //Nos aseguramos de que la posición del diálogo sea siempre visible en pantalla
            int swidth = MainWindow::mainwin->desktop()->width();
            int sheight = MainWindow::mainwin->desktop()->height();
            if (mDialogPos.x() + mDialog->width() > swidth)
                mDialogPos.setX(swidth - mDialog->width() - (mDialog->window()->height() - mDialog->height()));
            if (mDialogPos.x() < 0)
                mDialogPos.setX(0);
            if (mDialogPos.y() + mDialog->height() > sheight)
                mDialogPos.setY(sheight - mDialog->height() - (mDialog->window()->width() - mDialog->width()));
            if (mDialogPos.y() < 0)
                mDialogPos.setY(0);
            //Mover el diálogo a la posición previa o a la calculada
            mDialog->move(mDialogPos);
        }
        mFirstTimeDialog = false;
    }
}















