#ifndef XIMGTOOLSTRETCHDIALOG_H
#define XIMGTOOLSTRETCHDIALOG_H

#include <QDialog>
#include <QPen>
#include "core_def.h"

namespace Ui {
class XImgToolStretchDialog;
}

class XImgToolStretch;
class MdiFitImage;

class XImgToolStretchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit XImgToolStretchDialog(MdiFitImage *parent, XImgToolStretch* tool);
    ~XImgToolStretchDialog();

    void computeHistogram(float min, float max); //Calcula el histograma a partir de la imagen
    void computePoly(bool update_stats=true);    //Calcula los elementos de dibujado
    double fluxLeft(){return histoIndex2Flux(0);}
    double fluxRight(){return histoIndex2Flux(mHistoSize-1);}
    void setZoomed();
    inline int histoSize(){return mHistoSize;}

protected:
    void paintEvent(QPaintEvent* pe);
    void mouseMoveEvent(QMouseEvent* me);
    void mousePressEvent(QMouseEvent* me);
    void mouseReleaseEvent(QMouseEvent* me);
    void showEvent(QShowEvent *se);

private:
    bool                    mReseted;       //Para controlar el guardado de los límites
    XImgToolStretch*        mTool;
    QPen                    mMeshPen;
    QPen                    mLinePen;
    QBrush                  mPolyBrush;
    QPointF*                mPolyLine;      //Polilínea
    float                   mMode;
    int*                    mHistogram;     //Valor en cada slot del histograma (no somos propietarios de esta variable)
    int                     mHistoSize;     //Ancho del histograma o número de slots
    int                     mHistoMinVal;   //Punto mínimo en X del histograma
    int                     mHistoMaxVal;   //Punto máximo en X del histograma
    float                   mFluxMin;       //Valor de flujo máximo dentro de la imagen
    float                   mFluxMax;       //Valor de flujo mínimo dentro de la imagen
    QPointF*                mPoly;          //Polinomio que representa el perfil del histograma a dibujar
    bool                    mLogaritmicY;   //Indica si el eje Y tiene escala logarítmica
    bool                    mHistoArea;     //Indica si el histograma que se dibujará es un área o serán líneas verticales
    double                  mLowerMarkH;    //Índice de histograma donde se encuentra la marca menor
    double                  mUpperMarkH;    //Índice de histograma donde se encuentra la marca mayor

    quint8                  mOverMark;      //0=nunguna,1=menor,2=mayor
    quint8                  mMovingMark;    //0=nunguna,1=menor,2=mayor
    double                  mDespMark;      //Desplazamiento del ratón cuando se hizo click en la marca

    float                   mInitialMin;    //Flujo inicial donde se situaba el mínimo
    float                   mInitialMax;    //Flujo inicial donde se situaba el máximo



    void paintGraph(QPainter* p);


private:
    Ui::XImgToolStretchDialog *ui;

    MdiFitImage*    parent(){return (MdiFitImage*)QDialog::parent();}
    double          xToHistoIndexD(double x);
    int             xToHistoIndexI(double x);
    double          histoIndex2X(double i);
    double          flux2HistoIndex(double flux);
    double          histoIndex2Flux(double idx);


public slots:
    void            SetZoom_Slot();
    void            Reset_Slot();
    void            Cancel_Slot();
    void            Accept_Slot();

};

#endif // XIMGTOOLSTRETCHDIALOG_H
