#ifndef XIMGTOOLDISTANCE_H
#define XIMGTOOLDISTANCE_H

#include "ximgtool.h"
#include "../overlay/xoverlaysegment.h"
#include <QWidget>

class XImgToolDistance : public XImgTool
{
    Q_OBJECT

private:
    bool                    mDrawing;
    bool                    mPressed;
    QPointF                 mPointImgStart;
    QPointF                 mPointImgEnd;
    bool                    mWarningShown;
    bool                    mDrawn;
    XOverlayLayer*          mLayer;
    XOverlaySegment*        mSegment;
    XOverlayStyle*          mStyle;

public:
    explicit XImgToolDistance(MdiFitImage *parent);
    ~XImgToolDistance();

    bool activate();
    bool deactivate();
    bool mousePressEvent(QMouseEvent* );
    bool mouseReleaseEvent(QMouseEvent *);
    bool mouseMoveEvent(QMouseEvent *);

signals:

public slots:

};

#endif // XIMGTOOLDISTANCE_H
