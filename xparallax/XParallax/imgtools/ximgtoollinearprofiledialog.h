#ifndef XIMGTOOLLINEARPROFILEDIALOG_H
#define XIMGTOOLLINEARPROFILEDIALOG_H

#include <QDialog>
#include <QPen>


namespace Ui {
class XImgToolLinearProfileDialog;
}

class XImgToolLinearProfile;

class XImgToolLinearProfileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit XImgToolLinearProfileDialog(QWidget *parent, XImgToolLinearProfile* tool);
    ~XImgToolLinearProfileDialog();

    void paintEvent(QPaintEvent* pe);
    double scaleX();
    double scaleY();
    void setValues(QVector<double>* values);
    void setValues(double* values, int len);
    void updateInfo();
    QImage* image(){return &mImage;}
    void resizeIMage(int width, int height);
    void mouseMoveEvent(QMouseEvent* ev);

    QString lastSavePath(){return mLastSavePath;}
    void    setLastSavePath(QString s){mLastSavePath = s;}
    QString lastSaveFormat(){return mLastSaveFormat;}
    void    setLastSaveFormat(QString s){mLastSaveFormat = s;}

private:
    Ui::XImgToolLinearProfileDialog *ui;
    XImgToolLinearProfile*  mTool;
    QPen                    mMeshPen;
    QPen                    mLinePen;
    QBrush                  mPolyBrush;
    QPen                    mTextPen;
    QPen                    mAveragePen;            //Líneas de flujos medios
    QPointF*                mPolyLine;              //Polilínea
    QPointF*                mCentralLine;           //Linea central de píxeles de imagen
    QVector<double>         mHValues;
    QImage                  mImage;
    double                  mMin;
    double                  mMax;
    int                     mImgHeight;
    QVector<QRgb>           mColorTable;
    bool                    mInsideGraph;
    QString                 mLastSavePath;
    QString                 mLastSaveFormat;

    void paintMesh(QPainter* painter);
    void paintAdvise(QPainter* painter);
    void paintAdditional(QPainter* painter);
    void paintGraph(QPainter* painter);


public slots:
    void SpinHeightChaned_Slot(int val);
    void Export_Slot();
};

#endif // XIMGTOOLLINEARPROFILEDIALOG_H
