#ifndef XIMGTOOLLINEARPROFILE_H
#define XIMGTOOLLINEARPROFILE_H

#include <QObject>
#include <QWidget>
#include <QPen>
#include <QSpinBox>
#include "ximgtool.h"
#include "ximgtoollinearprofiledialog.h"
#include "matcher.h"

class XImgToolLinearProfile : public XImgTool
{
    Q_OBJECT

private:
    QPointF                     mPointStart;        //Punto de pantalla lógico de inicio (de pantalla)
    QPointF                     mPointEnd;          //Punto de pantalla lógico de fin
    QPointF                     mPointImgStart;
    QPointF                     mPointImgEnd;
    QPointF*                    mDrawPol;
    bool                        mDrawing;           //Indica que estamos en medio de una operación de dibujo
    int                         mLastDataWidth;
    int                         mHeight;
    double                      mWidth;             //Ancho real de la línea trazada del perfil
    QPen                        mPenMiddle; 
    QPen                        mPenContour;
    QPen                        mPenTrail;
    double                      mAngle;             //Ángulo (grados) con la horizontal en sentido antihorario e

    //Vector con la convolución bicúbica spine (Rotar en horizontal los datos)
    Transform2D_ER              mTransform;         //Transformación imagen -> slice
    float*                      mData;              //Matriz de datos rotados desde la imagen
    double*                     mAccumData;         //Datos acumulados
    int                         mDataWidth;
    int                         mDataHeight;
    int                         mDataCapacity;



    //Otros datos postcalculados
    float*                      mBkgData;          //Datos para calcular el fondo (alrededor del slice)
    int                         mBkgDataWidth;     //Tamaño de los datos de fondo
    int                         mBkgDataCapacity;  //Capacidad de los datos de fondo
    double                      mBkg;              //Intensidad del fondo
    double                      mBkgSigma;         //Desviación típica del fondo
    double                      mSigmaLevel;       //Factor de sigma para considerar un píxel como píxel objeto
    double                      mObjLevel;         //Nivel medio de los píxeles del objeto
    double                      mObjFlux;          //Flujo total del objeto (sin el fondo)
    int                         mBkgPixCount;      //Número de píxeles que pertenecen al fondo
    int                         mObjectPixCount;   //Número de píxeles que pertenecen a un objeto 3sigma
    //Información referente a un trail se se detectara
    QList<double>               mTrailPoints;      //Información sobre el trail detectado. Puntos de cambio de fondo a imagen
    QList<QPointF>              mTrailPointsProj;  //Mismos puntos anteriores pero proyectados
    QList<QPointF>              mTrailRADEC;       //Mismos puntos anteriores pero proyectados
    double                      mTrailA;           //Pendiente del trail si lo hubiera
    double                      mTrailB;           //Ordenada en el origen del trail si la hubierra
    QPointF                     mTrailXY;          //Coordenadas centrales de imagen del trail
    QPointF                     mTrailRaDec;       //Coordenadas centrales ecuatoriales del trail

    XImgToolLinearProfileDialog*dialog(){return (XImgToolLinearProfileDialog*)XImgTool::dialog();}
    void                        computeScreenPos();
    void                        computePoly();      //Calcula el polígono envolvente
    void                        dataProjection();   //Proyectar la imagen de los datos
    void                        computeBackground();//Calcular el fondo del slice
    void                        linearFunc(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, void* /*ptr*/);
    void                        linearFuncGrad(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, alglib::real_1d_array &grad, void* /*ptr*/);
    void                        lookForTrail();     //Busca si lo hubiera un trail
    void                        refineFlankPoint(int idx, double flank_level);
    void                        iterateSlice(
                                void* user_data,
                                void(*fun)(int xp,int yp, double x, double y, float val, bool is_obj, void* user_data));

public:
    explicit XImgToolLinearProfile(MdiFitImage *parent);
    ~XImgToolLinearProfile();

    inline QPointF pointImgStart(){return mPointImgStart;}
    inline QPointF pointImgEnd(){return mPointImgEnd;}

    bool activate();
    bool deactivate();
    bool paintEvent(QPaintEvent*, QPainter*);
    bool mousePressEvent(QMouseEvent* );                /** Se pulsa algún botón del ratón. Implementado **/
    bool mouseReleaseEvent(QMouseEvent *);              /** Implementado **/
    bool mouseMoveEvent(QMouseEvent *);
    bool imageMoveEvent();
    bool zoomChangeEvent(float newzoom);
    bool resizeEvent(QResizeEvent*);
    bool nullImgPoint(){return mPointImgStart.x()<0.01 && mPointImgStart.y()<0.01;}
    void refresh();

    int boxWidth(){return mDataWidth;}
    void setHeight(int height);
    int boxHeight(){return mHeight;}
    double bkg(){return mBkg;}
    double objLevel(){return mObjLevel;}
    double objFlux(){return mObjFlux;}
    double bkgStdev(){return mBkgSigma;}
    double sigmaLevel(){return mSigmaLevel;}
    int bkgPixCount(){return mBkgPixCount;}
    int objectPixCount(){return mObjectPixCount;}
    QList<double>* trailPoints(){return &mTrailPoints;}
    QList<QPointF>* trailPointsProj(){return &mTrailPointsProj;}
    QList<QPointF>* trailRADEC(){return &mTrailRADEC;}
    //Indica que hay datos válidos seleccionados en pantalla
    bool   dataValid(){return mDataHeight && mDataWidth && mData && mAccumData;}
    bool   trailAvailable(){return IS_VALIDF(mTrailA) && IS_VALIDF(mTrailB) && !nullImgPoint();}
    double trailA(){return mTrailA;}
    double trailB(){return mTrailB;}

signals:

public slots:

};

#endif // XIMGTOOLLINEARPROFILE_H
