#include "ximgtoollocate.h"
#include "../mdifitimage.h"
#include "../inputdialog.h"

XImgToolLocate::XImgToolLocate(MdiFitImage *parent) :
    XImgTool(parent, TOOL_LOCATE)
{
    mLayer = NULL;
    mStyle = NULL;
    mInputDialog = NULL;
}


XImgToolLocate::~XImgToolLocate()
{
    if (mInputDialog) delete mInputDialog;
}

bool XImgToolLocate::activate()
{
    if (mInputDialog) return true;
    if (!parent()->plate())
    {
        QMessageBox::warning(parent(), "Astrometic reduction not found",
                             "No astrometric reduction found.\r\nCan't locate objects in the image.",
                             QMessageBox::Ok);
        parent()->DeselectCurrentTool_Slot();
        if (mInputDialog) delete mInputDialog; mInputDialog = NULL;
        return true;
    }

    //;Mostrar el diálogo
    mInputDialog = new InputDialog(TOOL_LOCATE, parent());
    QVector<double> radec = mInputDialog->ShowRADec();
    if (IS_INVALIDF(radec[0]) || IS_INVALIDF(radec[1]))
    {
        parent()->DeselectCurrentTool_Slot();
        if (mInputDialog) delete mInputDialog; mInputDialog = NULL;
        return true;
    }

    //Si no hay una capa de "User drawing" la creamos
    for (int i=0; i<parent()->xLayers()->count() && !mLayer; i++)
    {
        if (parent()->xLayers()->at(i)->name() == XLAYER_USER_DRAWING)
            mLayer = parent()->xLayers()->at(i);
    }
    if (!mLayer)
    {
        mLayer = new XOverlayLayer(XLAYER_USER_DRAWING, parent());
        parent()->addXLayer(mLayer, XLAYER_USER_DRAWING_PRIORITY);
    }
    mLayer->setVisible(false);
    parent()->ShowHideLayer_Slot(mLayer);
    //Creamos el estilo si no lo hubiera
    if (!mLayer->styles()->contains(XGROUP_TOOL_LOCATE))
    {
        mStyle = mLayer->addStyle(XGROUP_TOOL_LOCATE);
        mStyle->setBorder(Qt::red, 3, Qt::DashLine);
        mStyle->setHoverBorder(Qt::green, 3, Qt::DashLine);
        mStyle->setBoxBorder(Qt::gray, 1, Qt::DashLine);
    }
    else mStyle = mLayer->styles()->value(XGROUP_TOOL_LOCATE);

    //Posicionamos una cruz
    double imgx, imgy;
    QPointF scrxy;
    parent()->plate()->imap(radec[0], radec[1], &imgx, &imgy);
    //Si está fuera de la imagen avisamos
    bool outside = imgx < 0 || imgy < 0 || imgx > parent()->fitimage()->width() || imgy > parent()->fitimage()->height();
    if (outside)
    {
        QMessageBox::warning(parent(), "Warning",
                             "The selected position is outside the image area", QMessageBox::Ok);
    }
    else
    {
        parent()->setImageCenterPixel(imgx, imgy);
    }

    parent()->ImageXYToScreenXY(imgx, imgy, scrxy);
    //Creamos el segmento y lo inicializamos
    XOverlayCross* elem = new XOverlayCross(mLayer, XGROUP_TOOL_LOCATE);
    elem->setHoverable(true);
    elem->setSelectable(true);
    elem->setDeletable(true);
    elem->setRaDecAttatched(true);
    elem->setBasePos(radec[0], radec[1]);
    elem->setPos(scrxy);
    elem->setRadius(20);
    elem->setHoleRadius(8);
    parent()->DeselectCurrentTool_Slot();
    if (mInputDialog) delete mInputDialog; mInputDialog = NULL;
    parent()->update();
    return true;
}
