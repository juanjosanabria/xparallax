#ifndef XIMGTOOLSTRETCH_H
#define XIMGTOOLSTRETCH_H

#include "ximgtool.h"
#include "ximgtoolstretchdialog.h"

class XImgToolStretch : public XImgTool
{
    Q_OBJECT

private:
    bool                        mFirstTime;
    float                       mLastLeftFlux;
    float                       mLastRightFlux;

    XImgToolStretchDialog*      dialog(){return (XImgToolStretchDialog*)XImgTool::dialog();}

public:
    explicit XImgToolStretch(MdiFitImage *parent);
    ~XImgToolStretch();

    bool activate();
    bool deactivate();

signals:

public slots:

};

#endif // XIMGTOOLSTRETCH_H
