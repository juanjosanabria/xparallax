#ifndef XIMGTOOLGIF_H
#define XIMGTOOLGIF_H

#include "ximgtool.h"
#include "../overlay/xoverlay.h"
#include <stdint.h>

/**
 * @brief The XImgToolGif class Herramienta usada por MdiFitBlink
 * para exportar una zona de la image como un GIF animado. Pensado para objetos móviles
 */
class XImgToolGif : public XImgTool
{
public:
    explicit XImgToolGif(MdiFitImage *parent);
    ~XImgToolGif();

    bool activate();
    bool deactivate();
    bool mousePressEvent(QMouseEvent* );
    bool mouseReleaseEvent(QMouseEvent *);
    bool mouseMoveEvent(QMouseEvent *);

private:
    QPointF                 mPointImgStart;
    QPointF                 mPointImgEnd;
    bool                    mDrawing;
    bool                    mDrawn;
    XOverlayLayer*          mLayer;
    XOverlayRect*           mRect;
    XOverlayStyle*          mStyle;
    uint8_t*                mImageBuffer;

    bool saveGif(QRectF rect, QString fname);
    bool saveHtm(QRectF rect, QString fname);

};

#endif // XIMGTOOLGIF_H
