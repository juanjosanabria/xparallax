#include "ximgtoolstretch.h"
#include "../mdifitimage.h"

XImgToolStretch::XImgToolStretch(MdiFitImage *parent) :
    XImgTool(parent, TOOL_STRETCH)
{
    mFirstTime = true;
    setDialog(new XImgToolStretchDialog(parent, this));
    dialog()->setWindowFlags(dialog()->windowFlags() | Qt::Tool);
    dialog()->setFixedSize(dialog()->width(), dialog()->height());
    dialog()->setWindowTitle(TOOL_STRETCH);
    mLastLeftFlux = parent->fitimage()->minpix();
    mLastRightFlux = parent->fitimage()->maxpix();
    connect(dialog(), SIGNAL(rejected()), parent, SLOT(DeselectCurrentTool_Slot()));
    connect(dialog(), SIGNAL(accepted()), parent, SLOT(DeselectCurrentTool_Slot()));
}

XImgToolStretch::~XImgToolStretch()
{
}

bool XImgToolStretch::activate()
{
    //Si es la primera vez, comprobamos si tenemos que mostrarlo zoomeado
    if (mFirstTime)
    {
        dialog()->computeHistogram(mLastLeftFlux, mLastRightFlux);
        //El cálculo se hace con un ancho aproximado (el del diálogo) que será parecido al del área de dibujo
        double idx_left =  RANGECHANGE(parent()->cProfile()->min(),
                                       parent()->fitimage()->minpix(), parent()->fitimage()->maxpix(), 0.0, 1.0)
                                       *(double)dialog()->width();

        double idx_right = RANGECHANGE(parent()->cProfile()->max(),
                                       parent()->fitimage()->minpix(), parent()->fitimage()->maxpix(), 0.0, 1.0)
                                       *(double)dialog()->width();
        if (idx_right - idx_left < 30)
        {
            float range = parent()->cProfile()->max() - parent()->cProfile()->min();
            mLastLeftFlux = MAXVAL(parent()->fitimage()->minpix(), parent()->cProfile()->min() - range/2.0);
            mLastRightFlux = MINVAL(parent()->fitimage()->maxpix(), parent()->cProfile()->max() + range/2.0);
            dialog()->setZoomed();
        }
        dialog()->computePoly();
    }
    dialog()->show();
    mFirstTime = false;
    recoverDialogPos();
    return true;
}

bool XImgToolStretch::deactivate()
{
    saveDialogPos();
    mLastLeftFlux = dialog()->fluxLeft();
    mLastRightFlux = dialog()->fluxRight();
    if (dialog()->isVisible()) dialog()->Cancel_Slot();
    dialog()->hide();
    return true;
}














