#include "ximgtoollinearprofile.h"
#include "../mdifitimage.h"
#include "core.h"
#include "xglobal.h"
#include "geometry.h"

#define  CFG_SAVE_PATH      "save_path"
#define  CFG_SAVE_FORMAT    "save_format"
#define  CFG_PROFILE_HEIGHT "profile_height"

XImgToolLinearProfile::XImgToolLinearProfile(MdiFitImage *parent) :
    XImgTool(parent, TOOL_LINEAR_PROFILE)
{
    mPointStart = QPoint(0,0);
    mPointEnd = QPoint(0,0);
    mDrawing = false;
    mLastDataWidth = mDataWidth = mDataHeight = 0;
    mDrawPol = new QPointF[4];
    mData = NULL; mAccumData = NULL;
    mBkgData = NULL; mBkgDataCapacity = mBkgDataWidth = 0;
    mPenMiddle = QPen(QColor::fromRgb(0xff, 0x00, 0x00, 128), 1, Qt::DashLine);
    mPenContour = QPen(QColor::fromRgb(0xff, 0x00, 0x00, 128), 1);
    mPenTrail = QPen(QColor::fromRgb(0xff, 0xff, 0x00, 128), 2);
    mSigmaLevel = 2;
    //Carga de la configuración
    mHeight = Parameters::ReadInt(XGlobal::INI_SETTINGS, cfgSection(), CFG_PROFILE_HEIGHT, 20);
    if (mHeight < 2 || mHeight > 100) mHeight = 20;
    //Crear el diálogo de resultados
    setDialog(new XImgToolLinearProfileDialog(parent, this));
    dialog()->setWindowFlags(dialog()->windowFlags() | Qt::Tool);
    dialog()->setFixedSize(dialog()->width(), dialog()->height());
    dialog()->setWindowTitle(TOOL_LINEAR_PROFILE);
    connect(dialog(), SIGNAL(rejected()), parent, SLOT(DeselectCurrentTool_Slot()));
    //Carga de configuración
    dialog()->setLastSavePath(Parameters::ReadString(XGlobal::INI_SETTINGS, cfgSection(), CFG_SAVE_PATH, ""));
    dialog()->setLastSaveFormat(Parameters::ReadString(XGlobal::INI_SETTINGS, cfgSection(), CFG_SAVE_FORMAT, ""));
}

XImgToolLinearProfile::~XImgToolLinearProfile()
{
    //Salvar los parámetros
    if (dialog()->lastSavePath().length())
    Parameters::SaveString(XGlobal::INI_SETTINGS, cfgSection(), CFG_SAVE_PATH, dialog()->lastSavePath());
    if (dialog()->lastSaveFormat().length())
    Parameters::SaveString(XGlobal::INI_SETTINGS, cfgSection(), CFG_SAVE_FORMAT, dialog()->lastSaveFormat());
    if (mHeight > 1)
    Parameters::SaveInt(XGlobal::INI_SETTINGS, cfgSection(), CFG_PROFILE_HEIGHT, mHeight);

    if (mData) delete mData;
    mData = NULL;
    if (mAccumData) delete mAccumData;
    mAccumData = NULL;
    if (mBkgData) delete mBkgData;
    mBkgData = NULL;
    delete mDrawPol;
}

void XImgToolLinearProfile::refresh()
{
    computePoly();
    dataProjection();
    computeBackground();
    lookForTrail();
    dialog()->updateInfo();
    dialog()->update();
}

bool XImgToolLinearProfile::activate()
{
    mDrawing = false;
    //Si había una línea anteriormente seleccionada la seleccionamos
    if (!nullImgPoint())
    {
        computeScreenPos();
        computePoly();
    }
    parent()->update();
    dialog()->show();
    dialog()->raise();
    recoverDialogPos();
    return true;
}

bool XImgToolLinearProfile::deactivate()
{
    saveDialogPos();
    mDrawing = false;
    parent()->update();
    dialog()->hide();
    return true;
}
void XImgToolLinearProfile::setHeight(int height)
{
    mHeight = height;
    computePoly();
    parent()->update();
}

void  XImgToolLinearProfile::computePoly()
{
    QPointF p1 = mPointStart.x() < mPointEnd.x() ? mPointStart : mPointEnd;
    QPointF p2 = mPointStart.x() < mPointEnd.x() ? mPointEnd : mPointStart;
    double hipo = CART_DIST(mPointStart.x(), mPointStart.y(),  mPointEnd.x(),  mPointEnd.y());
    double cosphi = (p1.y() - p2.y()) / hipo;
    double phi = acos(cosphi);
    mDrawPol[0] = QPointF(mHeight*parent()->scale()/2.0 * sin(90*DEG2RAD-phi) + p1.x(),
                          mHeight*parent()->scale()/2.0 * cos(90*DEG2RAD-phi) + p1.y());
    mDrawPol[1] = QPointF(mHeight*parent()->scale()/2.0 * sin(270*DEG2RAD-phi) + p1.x(),
                          mHeight*parent()->scale()/2.0 * cos(270*DEG2RAD-phi) + p1.y());
    mDrawPol[2] = QPointF(mDrawPol[1].x() - p1.x() + p2.x(), mDrawPol[1].y() - p1.y() + p2.y());
    mDrawPol[3] = QPointF(mDrawPol[0].x() - p1.x() + p2.x(), mDrawPol[0].y() - p1.y() + p2.y());

    //Ángulo en sentido antihorario  y ancho real (de imagen)
    mWidth = CART_DIST(mPointImgStart.x(), mPointImgStart.y(),  mPointImgEnd.x(),  mPointImgEnd.y());
    mAngle = Transform2D_ER::VectorAngle_AH(mPointEnd.x()-mPointStart.x(), mPointEnd.y()-mPointStart.y(),
                                            100, 0);
}

void XImgToolLinearProfile::computeScreenPos()
{
    parent()->ImageXYToScreenXY(mPointImgStart, mPointStart);
    parent()->ImageXYToScreenXY(mPointImgEnd, mPointEnd);
    computePoly();
}

bool XImgToolLinearProfile::paintEvent(QPaintEvent*, QPainter* painter)
{
    if (!mPointStart.x() && !mPointStart.y() && !mPointEnd.x() && !mPointEnd.y() || !dataValid()) return true;
    //Línea media   
    painter->setPen(mPenMiddle);
    painter->drawLine(mPointStart, mPointEnd);   
    //Rectángulo envolvente
    painter->setPen(mPenContour);
    painter->drawPolygon(mDrawPol,4);
    painter->setPen(Qt::yellow);
    painter->drawText(mPointStart.x()-5, mPointStart.y()+mHeight*parent()->scale()+5,
                     100,100,0,QString("").sprintf("%0.1fº",mAngle));
    //Recta de regresión si la hubiera
    if (trailAvailable())
    {
        QPointF pa, pb;
        parent()->ImageXYToScreenXY(QPointF(mPointImgStart.x(), mPointImgStart.x() * mTrailA + mTrailB),pa);
        parent()->ImageXYToScreenXY(QPointF(mPointImgEnd.x(), mPointImgEnd.x() * mTrailA + mTrailB), pb);
        if (CART_DIST(pa.x(), pa.y(), pb.x(), pb.y()) > 2)
        {
            painter->setPen(mPenTrail);
            painter->drawLine(pa, pb);
        }

        //Puntos del trail
        painter->setPen(Qt::red);
        for (int i=0;i<mTrailPointsProj.count(); i++)
        {
            parent()->ImageXYToScreenXY(mTrailPointsProj[i], pa);
            QPointF pstart(pa.x() - mDataHeight * parent()->scale(), pa.y());
            QPointF pend(pa.x() + mDataHeight * parent()->scale(), pa.y());
            Geometry::RotateSegment(pstart, pend, 90-atan(mTrailA)*RAD2DEG, pa);
            painter->drawLine(pstart ,pend);
        }
    }


    //Pintar todos los píxeles que pertenecen al objeto
    //Cálculo del sumatorio SUM((x_i-xmedia)*(y_i - ymedia)), SUM((xi-xmedia)^2)
    painter->setPen(Qt::red);
    void* udata[]={this, painter};
    if (false) //Desactivado de momento
    iterateSlice(udata,[](int/*xp*/,int/*yp*/,double xi, double yi, float/*val*/, bool is_obj, void* ud){
        if (is_obj)
        {
            QPointF scrxy;
            XImgToolLinearProfile* ts = (XImgToolLinearProfile*)(((void**)ud)[0]);
            ts->parent()->ImageXYToScreenXY(QPointF(xi, yi),scrxy);
            ((QPainter*)(((void**)ud)[1]))->drawEllipse(scrxy, 1, 1);
        }
    });

    return true;
}


bool XImgToolLinearProfile::mousePressEvent(QMouseEvent* ev)
{
    mDrawing = true;
    mPointStart = mPointEnd = ev->pos();
    //Puntos relativos a la imagen
    parent()->ScreenXYToImageXY(mPointStart, mPointImgStart);
    mPointImgEnd = mPointImgStart;
    return false;
}

bool XImgToolLinearProfile::mouseReleaseEvent(QMouseEvent *ev)
{
  mPointEnd = ev->pos();
  mDrawing = false;
  parent()->ScreenXYToImageXY(mPointEnd, mPointImgEnd);
  computePoly();
  dataProjection();
  computeBackground();
  lookForTrail();
  dialog()->updateInfo();
  dialog()->update();
  return false;
}

bool XImgToolLinearProfile::mouseMoveEvent(QMouseEvent *ev)
{
   if (!mDrawing) return true;
   mPointEnd = ev->pos();
   parent()->ScreenXYToImageXY(mPointEnd, mPointImgEnd);
   computePoly();
   dataProjection();
   computeBackground();
   lookForTrail();
   dialog()->updateInfo();
   dialog()->update();
   parent()->update();
   return false;
}

bool XImgToolLinearProfile::imageMoveEvent()
{
    if (mDrawing) return false;
    //Movemos los puntos de pantalla a punto de imagen
    computeScreenPos();
    return true;
}

bool XImgToolLinearProfile::zoomChangeEvent(float /* newzoom */)
{
    if (mDrawing) return false;
    //Movemos los puntos de pantalla a punto de imagen
    computeScreenPos();
    return true;
}

bool XImgToolLinearProfile::resizeEvent(QResizeEvent*)
{
    computeScreenPos();
    return true;
}

void XImgToolLinearProfile::dataProjection()
{
    //Reservar espacio si es necesario para los nuevos datos
    int width = (int)ceil(CART_DIST(mPointImgStart.x(), mPointImgStart.y(), mPointImgEnd.x(), mPointImgEnd.y()));
    if (width < 2) return;
    int height = mHeight;
    if (!mData || (mDataCapacity < width*height))
    {
        if (mData) delete mData;
        mData = new float[mDataCapacity = width*height];
    }
    if (!mAccumData || (mLastDataWidth < mWidth))
    {
        if (mAccumData) delete mAccumData;
        mAccumData = new double[mLastDataWidth = width];
    }
    mDataWidth = width;
    mDataHeight = height;
    //Transformación bicúbica. Trasladamos la línea de pantalla a una línea imaginaria horizontal que comienza en 0
    mTransform =
     Transform2D_ER::ComputeTransform(mPointImgStart.x(), mPointImgStart.y(), mPointImgEnd.x(), mPointImgEnd.y(),
                                      0, mHeight/2, mWidth, mHeight/2);

    float* pData = parent()->fitimage()->data();
    int pWidth = parent()->fitimage()->width();
    int pHeight = parent()->fitimage()->height();
    for (int i=0; i < height; i++)
    {
        for (int j=0; j < width; j++)
        {
            double xPrime, yPrime;
            double factor1, factor2, factor3, factor4;
            mTransform.imap(j, i, &xPrime, &yPrime);

            // There are four nearest original pixels, q11, q12, q21, and q22
            // While we may get away with using only four variables, this code
            // seperates out the x and y of each point for clarity reasons.
            // Most compilers should be capable of optimizing away the redundant
            // steps here.

            int q12x = (int)floor(xPrime);
            int q12y = (int)floor(yPrime);
            q12x = MAXVAL(0, q12x);
            q12y = MAXVAL(0, q12y);
            q12x = MINVAL(pWidth-1, q12x);
            q12y = MINVAL(pHeight-1, q12y);
            int q22x = (int)ceil(xPrime);
            int q22y = q12y;
            q22x = MINVAL(pWidth-1, q22x);
            q22x = MAXVAL(0, q22x);
            int q11x = q12x;
            int q11y = (int)ceil(yPrime);
            q11y = MINVAL(pHeight-1, q11y);
            q11y = MAXVAL(0, q11y);
            int q21x = q22x;
            int q21y = q11y;

            // We need to get the four nearest neighbooring pixels.
            // Pixels which are past the border of the image are clamped to the border already.
            double q11r = pData[q11x + q11y*pWidth];
            double q12r = pData[q12x + q12y*pWidth];
            double q21r = pData[q21x + q21y*pWidth];
            double q22r = pData[q22x + q22y*pWidth];

            // Here, it would be better to use a vector class to store the R,G and B values
            // to keep code consise. But they have been seperated out for maximum clarity
            // and simplicity.
            if ( q21x == q11x ) // special case to avoid divide by zero
            {
                factor1 = 1; // They're at the same X coordinate, so just force the calculatione to one point
                factor2 = 0;
            }
            else
            {
                factor1 = (((double)q21x - (double)xPrime)/((double)q21x - (double)q11x));
                factor2 = (((double)xPrime - (double)q11x)/((double)q21x - (double)q11x));
            }
            double R1r = factor1 * (double)q11r + factor2*(double)q21r;
            double R2r = factor1 * (double)q12r + factor2*(double)q22r;


            if (q12y == q11y) // special case to avoid divide by zero
            {
                factor3 = 1;
                factor4 = 0;
            }
            else
            {
                factor3 = ((double) q12y - yPrime)/((double)q12y - (double)q11y);
                factor4 = (yPrime - (double)q11y)/((double)q12y - (double)q11y);
            }
            //Resultado final
            mData[j + i*width] = (float)((factor3 * R1r) + (factor4*R2r));

        }
    }

    //Calcular los nuevos datos del gráfico
    for (int i=0; i<mDataWidth; i++)
    {
        mAccumData[i] = 0;
        for (int y=0; y<mDataHeight; y++)
            mAccumData[i] += mData[i + y*mDataWidth];
    }
    dialog()->setValues(mAccumData, mDataWidth);

    //Crear el perfil de convergencia y mostrar el perfil real sobre el diálogo
    dialog()->resizeIMage(mDataWidth, mDataHeight);
    ConvergeProfile cp;
    cp.SetData(mData, mDataWidth, mDataHeight);
    cp.SetDataAndRanges(mData, mDataWidth, mDataHeight, parent()->fitimage());
    cp.Converge(dialog()->image());
}

/**
 * @brief XImgToolLinearProfile::computeBackground Calcula el fondo del slice rotado horizontalmente.
 * Los datos se encuentran en:
 * float* mData;                //Matriz de entrada
 * int    mDataWidth;           //Ancho de la matriz de entrada
 * int    mDataHeight;          //Alto de la matriz de entrada
 */
void XImgToolLinearProfile::computeBackground()
{
    if (!dataValid()) return;
    //Creo un modelo de fondo alrededor del slice, con el ancho mismo del slice por todos los lados
    //Basta con seleccionar los suficientes píxeles alrededor de él
    int pWidth = parent()->fitimage()->width();
    int pHeight = parent()->fitimage()->height();
    float* pData = parent()->fitimage()->data();
    //Superficie del fondo es igual al box de fondo menos la superficie del slice objeto
    int estimated_size = (mDataHeight+mDataHeight*2)*(mDataWidth+mDataHeight*2) - mDataWidth*mDataHeight;
    if (!mBkgData || mBkgDataCapacity < estimated_size)
    {
        if (mBkgData) delete mBkgData;
        mBkgData = new float[mBkgDataCapacity = estimated_size];
    }
    mBkgDataWidth = 0;
    double ix, iy;
    for (int y = -mDataHeight; y<mDataHeight+mDataHeight && mBkgDataWidth < mBkgDataCapacity; y++)
    {
        for (int x = -mDataHeight; x<mDataWidth+mDataHeight && mBkgDataWidth < mBkgDataCapacity; x++)
        {
            //Si no estamos dentro del slice lo añadimos al fondo
            if ((x < 0 || x >= mDataWidth) && ( y < 0 || y >= mDataHeight ))
            {
                mTransform.imap(x, y, &ix, &iy);
                int px = FROUND(ix); int py = FROUND(iy);
                //Cuidado con no salirse de la imagen también
                if (px < 0 || px >= pWidth || py < 0 || py >= pHeight) continue;
                mBkgData[mBkgDataWidth] = pData[px + py * pWidth];
                mBkgDataWidth++;
            }
        }
    }

    //Calcular el valor de fondo y su desviación típica
    BackgroundMap bkg;
    BackgroundParameters bkgpar;
    bkg.Setup(bkgpar.bkg_method, mBkgDataWidth, 1,
                       mBkgDataWidth /*Toda la imagen, sin zonas*/,
                       0 /*Sin smooth*/,
                       bkgpar.bkg_lower_conv, bkgpar.bkg_upper_conv);
    bkg.Compute(mBkgData);
    mBkg = bkg.backentry(0, 0)->Back;
    mBkgSigma = bkg.backentry(0, 0)->StandardDev;

    //Calculamos el número de píxeles que pertenecen al fondo y los que pertenecen al objeto
        /*
    int n = mDataWidth*mDataHeight;
    double level = 2*mBkgSigma;
    mObjectPixCount = mBkgPixCount = 0;
    for (int i=0; i<n; i++)
       mData[i] > level ? mObjectPixCount++ : mBkgPixCount++;*/
}

void XImgToolLinearProfile::iterateSlice(void* user_data,
                                         void(*fun)(int xp,int yp, double x, double y, float val, bool is_obj, void* user_data))
{
    double x, y;
    //Recorremos los datos proyectados y los vamos deproyectando
    //int pWidth = parent()->fitimage()->width();
    //int pHeight = parent()->fitimage()->height();
    //float* pData = parent()->fitimage()->data();
    double level = mBkg + mBkgSigma*mSigmaLevel;
    //qDebug() << level;
    for (int xp = 0; xp <mDataWidth; xp++)
    {
        for (int yp = 0; yp<mDataHeight; yp++)
        {
            mTransform.imap(xp, yp, &x, &y);
            float val_proj = mData[yp * mDataWidth + xp];
            fun(xp, yp, x, y, val_proj, val_proj > level, user_data);
            /*
            int xi = FROUND(x);
            int yi = FROUND(y);
            if (xi > 0 && xi < pWidth && yi > 0 && yi < pHeight)
            {
                float val_proj = mData[yp * mDataWidth + xp];
                //float val_real = pData[yi * pWidth + xi];
                fun(xp, yp, x, y, val, val > level);
            }*/
        }
    }
}

//Función PSF: f(x,y) = c0 + c1*x + c2*y + c3*x^2 + c4*y^2 = LN I(x,y)+ LN I(0). Donde I(0) es el máximo de flujo para un píxel
void XImgToolLinearProfile::linearFunc(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, void* /*ptr*/)
{
    // f(x) = c0 + c1*x;
    double x = _x[0];
    func = c[0] + c[1]*x;
}

//Derivada en C0,1 de la función PSF, acelera el cálculo iterativo del ajuste
void XImgToolLinearProfile::linearFuncGrad(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, alglib::real_1d_array &grad, void* /*ptr*/)
{
    double x = _x[0];
    func = c[0] + c[1]*x;
    //Derivada en c0
    grad[0] = 1;
    //Derivada en c1
    grad[1] = x;
}

void XImgToolLinearProfile::lookForTrail()
{
    if (!dataValid()) return;
     //******************************************************************************************************
    //Calcular una recta de regresión por mínimos cuadrados. Esta parte está finalizada y testada
    //******************************************************************************************************
    //Regresión lineal por mínimos cuadrados (ponderada al flujo de cada píxel)
    //Solo se tendrán en cuenta los valores por encima del umbral de fondo 3sigma
    //http://www.stat.ufl.edu/~winner/qmb3250/notespart2.pdf
    mTrailA = mTrailB = INVALID_FLOAT;
    double a_aprox, b_aprox;
    //El valor aproximado de A y B se puede calcular desde los puntos inicial y final
    Geometry::rectPuntoPendiente(mPointImgStart, mPointImgEnd, &a_aprox, &b_aprox);
    if (IS_INVALIDF(a_aprox)) a_aprox = 0;
    else if (a_aprox == PLUS_INFINITE || a_aprox == MINUS_INFINITE) a_aprox = FLOAT_MAX;

    //Calculamos la media en [x] e [y] así como el [flujo total]
    double data[] = {0.0 /* [0]Media en x */,
                     0.0 /* [1]Media en y */,
                     0.0 /* [2]Flujo total del objeto*/,
                     0.0 /* [3]SUM((xi - xmedia)(yi - ymedia)) */,
                     0.0 /* [4]SUM((xi - xmedia)^2) */,
                     0.0 /* [5] data count número de píxeles de la rodaja */,
                     0.0 /* [6] Flujo medio total de la rodaja */,
                     0.0 /* [7] data count número de píxeles fondo */,
                     0.0 /* [8] Flujo medio del fondo */
                    };
    //Cálculo de los valores xmedia, ymedia y flujo total
    iterateSlice(data,[](int/*xp*/,int/*yp*/, double xi, double yi, float val, bool is_obj, void* ud){
        ((double*)ud)[5] += 1;
        ((double*)ud)[6] += val;
        if (is_obj)
        {
            ((double*)ud)[0] += xi * val;
            ((double*)ud)[1] += yi * val;
            ((double*)ud)[2] += val;
        }
        else
        {
            ((double*)ud)[7] += 1;
            ((double*)ud)[8] += val;
        }
    });
    data[0] /= data[2];     //Dividir por el flujo total. Esto no es una media /N sino por el flujo
    data[1] /= data[2];
    data[8] /= data[7];
    //mObjLevel = data[6] / data[5];
    mObjFlux = data[2] - (data[5]-data[7])*mBkg;

   // qDebug() << "MED TOTAL: " << mObjLevel;
   // qDebug() << "MED OBJ: " << data[2] /(data[5]-data[7]);
   // qDebug() << "TODOS OBJ: " << data[2] /(data[5]-data[7]);
   // qDebug() << "--------------------------------";
    mObjLevel = data[2] / (data[5]-data[7]);

    //Cálculo del sumatorio SUM((x_i-xmedia)*(y_i - ymedia)), SUM((xi-xmedia)^2)
    iterateSlice(data,[](int/*xp*/,int/*yp*/,double xi, double yi, float/*val*/, bool is_obj, void* ud){
        if (is_obj)
        {
            ((double*)ud)[3] += (xi - ((double*)ud)[0]) * (yi - ((double*)ud)[1]);
            ((double*)ud)[4] += POW2(xi - ((double*)ud)[0]);
        }
    });

    //Finalmente, pendiente y ordenada en el origen
    mTrailA = data[3] / data[4];            //Pendiente  b1 = (Sum((x_i - xmedia)(y_i - ymedia))/Sum((x_i - xmedia)^2)
    mTrailB = data[1] - mTrailA * data[0];  //Ord.Origen b0 =  ymedia - b1 *  xmedia


    //******************************************************************************************************
    //Detectar comienzo y final del trail
    //******************************************************************************************************
    //Valores preliminares de final de fondo y comienzo de fondo
    mTrailPoints.clear();
    mTrailPointsProj.clear();
    mTrailRADEC.clear();
    double bkg_out = mBkg + mBkgSigma*mSigmaLevel;
    double obj_in = mObjLevel - mBkgSigma*mSigmaLevel;
    double flank_level = (obj_in+bkg_out)/2.0;

    flank_level = (mBkg + mObjLevel) / 2.0;

    bool in_bkg = true; //Asumimos que comenzamos en el fondo
    for (int i=4; i<mDataWidth-4; i++)
    {
        if (in_bkg)
        {
            //Ya no hay nada de fondo hacia la derecha
            if (mAccumData[i] / mDataHeight > flank_level &&
                mAccumData[i+1] / mDataHeight > flank_level &&
                mAccumData[i+2] / mDataHeight > flank_level &&
                mAccumData[i+3] / mDataHeight > flank_level
                )
            {
                in_bkg = false;
                mTrailPoints.append(i);
            }
        }
        else
        {
            //Ya no hay objeto hacia la derecha
            if (mAccumData[i] / mDataHeight < flank_level &&
               mAccumData[i+1] / mDataHeight < flank_level &&
               mAccumData[i+2] / mDataHeight < flank_level &&
               mAccumData[i+3] / mDataHeight < flank_level
               )
            {
                in_bkg = true;
                mTrailPoints.append(i-1);
            }
        }
    }
    //A ver de qué manera se refinan estos puntos (que son discretos)

    //He pensado calcular dónde estaría el "flank_level"
    if (mTrailPoints.count() == 2)
    for (int i=0; i<mTrailPoints.count(); i++)
        refineFlankPoint(i, flank_level * mDataHeight);

    //Quito todos los trails excepto primero y últio
    while (mTrailPoints.count() > 2)
        mTrailPoints.removeAt(1);


    //Añado un punto medio
    if (mTrailPoints.count() == 2)
    {
        mTrailPoints.append((mTrailPoints[0] + mTrailPoints[1]) / 2.0);



        //Calcula las posiciones ecuatoriales!!!
        for (int i=0; i<mTrailPoints.count(); i++)
        {
            double imgx, imgy, ndy, tx, ty = mDataHeight/2.0;
            int cont = 0;
            do
            {
                mTransform.imap(mTrailPoints[i], ty, &imgx, &imgy);
                ndy = imgx * mTrailA + mTrailB;
                mTransform.map(imgx, ndy, &tx, &ty);
                cont++;
            }
            while (fabs(imgy - ndy) > 0.0001 && cont < 10);
            mTrailPointsProj.append(QPointF(imgx, imgy));
            if (parent()->fitimage()->plate())
            {
                double ra, dec;
                parent()->fitimage()->plate()->map(imgx, imgy, &ra, &dec);
                mTrailRADEC.insert(1, QPointF(ra, dec));
                if (i==2) DINFO("RA = %s  ; DEC = %s", PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)));
            }
            //double ra, dec;
            //parent()->fitimage()->plate()->map(imgx, imgy, &ra, &dec);

        }
    }
}

/**
 * @brief XImgToolLinearProfile::refineFlankPoint Refina un punto discreto del flanco detectado
 * mediante una interpolación lineal, determinando el punto preciso en x donde se produce el
 * cambio de fondo a objeto.
 * @param idx
 * @param flank_level
 */
void XImgToolLinearProfile::refineFlankPoint(int idx, double flank_level)
{
    int twidth = 3;
    QVector<QPointF> data_adj;
    int tp = (int) mTrailPoints[idx];
    int tstart = tp - twidth; if (tstart < 0) tstart = 0;
    int tend = tp + twidth; if (tend > mDataWidth) tend = mDataWidth-1;
    for (int i=tstart; i<=tend; i++)
    {
        //if (mAccumData[i]/mDataHeight > (mBkg+mBkgSigma) && mAccumData[i] < mObjLevel-mBkgSigma)
        data_adj.append(QPointF(i, mAccumData[i]));
    }
    double a, b;
    Util::LinearRegression(data_adj.data(), NULL, data_adj.count(), &a, &b);
    double px = (flank_level - b)/a;
    mTrailPoints[idx] = px;
}


























