#-------------------------------------------------
#
# Project created by QtCreator 2015-08-26T17:01:07
#
#-------------------------------------------------

QT       -= gui

TARGET = Sky
TEMPLATE = lib
DEFINES  += _CRT_SECURE_NO_WARNINGS

DEFINES += SKY_LIBRARY

SOURCES += sky.cpp \
    alt_az.cpp \
    astcheck.cpp \
    astephem.cpp \
    astfuncs.cpp \
    big_vsop.cpp \
    classel.cpp \
    colors.cpp \
    colors2.cpp \
    com_file.cpp \
    cospar.cpp \
    cosptest.cpp \
    date.cpp \
    de_plan.cpp \
    delta_t.cpp \
    dist_pa.cpp \
    eart2000.cpp \
    easter.cpp \
    elp82dat.cpp \
    get_time.cpp \
    getplane.cpp \
    gust86.cpp \
    htc20b.cpp \
    jpleph.cpp \
    jsats.cpp \
    lun_tran.cpp \
    lunar2.cpp \
    marstime.cpp \
    miscell.cpp \
    mpcorb.cpp \
    nutation.cpp \
    obliqui2.cpp \
    obliquit.cpp \
    pluto.cpp \
    precess.cpp \
    refract.cpp \
    refract4.cpp \
    relativi.cpp \
    riseset3.cpp \
    rocks.cpp \
    showelem.cpp \
    solseqn.cpp \
    spline.cpp \
    ssats.cpp \
    tables.cpp \
    triton.cpp \
    vislimit.cpp \
    vsopson.cpp

HEADERS += sky.h\
        sky_global.h \
    afuncs.h \
    colors.h \
    comets.h \
    date.h \
    gust86.h \
    jpl_int.h \
    jpleph.h \
    lun_tran.h \
    lunar.h \
    riseset3.h \
    showelem.h \
    vislimit.h \
    watdefs.h \
    mpcorb.h \
    earth2000.h \
    alt_az.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    deployment.pri \
    cospar.txt \
    get_test.out \
    get_test.txt \
    license.txt
