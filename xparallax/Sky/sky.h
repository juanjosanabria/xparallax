#ifndef SKY_H
#define SKY_H

#include <QDateTime>
#include "sky_global.h"

namespace Sky
{
    struct OrbElems
    {
           double perih_time, q, ecc, incl, arg_per, asc_node;
           double epoch,  mean_anomaly;
                    /* derived quantities: */
           double lon_per, minor_to_major;
           double perih_vec[3], sideways[3];
           double angular_momentum, major_axis, t0, w0;
           double abs_mag, slope_param, gm;
           int is_asteroid, central_obj;
    };

    SKYSHARED_EXPORT double DateToJD(int year, int month, double day);

    //http://www.minorplanetcenter.net/iau/info/MPOrbitFormat.html
    SKYSHARED_EXPORT bool   ParseMPElems(char *buffer, OrbElems* elems);

    //http://www.minorplanetcenter.net/iau/info/CometOrbitFormat.html
    SKYSHARED_EXPORT bool   ParseCometElems(char *buffer, OrbElems* elems);

    /**
    * \brief ObservedMag Calcula la magnitud nominal observada de un objeto según las posiciones del objeto, Sol y Tierra.
    * \param elem Elementos orbitales del objeto
    * \param obj_sun
    * \param obj_earth
    * \param earth_sun
    * \return
    **/
    SKYSHARED_EXPORT float ObservedMag(OrbElems *elem, double obj_sun, double obj_earth,double earth_sun);

    /**
    * \brief CometPos Calcula la posición aparente geocéntrica de un objeto del Sistema Solar
    * \param elems      Elementos orbitales obtenidos con ParseCometElems ó ParseMPElems
    * \param jdate      Fecha juliana
    * \param ra         Ascensión recta
    * \param dec
    * \param obsmag
    * \param elong      Enlongación con respecto al Sol
    * \param dist       Distancia al observador
    **/
    SKYSHARED_EXPORT void  CometPos(OrbElems* elems, double jdate,double* ra,double* dec,float* obsmag=NULL,double* elong=NULL,double* dist=NULL);

    SKYSHARED_EXPORT void CometPosAndVel(OrbElems* elems, double jdate, double* pos, double* vel);

    SKYSHARED_EXPORT double JulianDate(QDateTime dt);

    //No verificada
    SKYSHARED_EXPORT void AnnualAb(double jdate, double ra, double dec,double* ob_ra, double* ob_dec);

    SKYSHARED_EXPORT void TopoComet(double jd,double gra, double gdec, double d, double lat, double lng, double high, double* ra, double*dec);

    //----------------------------------------------------------------------------------------------------------------------
    //Funciones para aplicar la precesión a un par de coordenadas
    //----------------------------------------------------------------------------------------------------------------------

    /**
     * @brief Precession Realiza un cambio de coordenadas, del vector pos1,vel1 al vector pos2,vel2.
     * Uno de los dos debe ser J2000. Es decir, jd_tdb1 ó jd_tdb2 deben ser j2000
     * @param jd_tdb1 Fecha juliana de las primeras coordenadas
     * @param pos1 Vector de posición para la fecha jd_tdb1
     * @param vel1 Vector de velocidad para la fecha jd_tdb1
     * @param jd_tdb2 Fecha juliana de las coordenadas de salida. Si jd_tdb1 no son J2000, jd_tdb2 debe ser J2000 en fecha juliana.
     * @param pos2 Vector de posición para la época de salida. Si ninguna de las fechas es J2000 se devolverá NAN como error
     * @param vel2 Vector de velocidad para la época de salida. Si ninguna de las fechas es J2000 se devolverá NAN como error
     */
    SKYSHARED_EXPORT void Precession(double jd_tdb1, double* pos1, double* vel1, double jd_tdb2, double* pos2, double* vel2);

    /**
     * @brief Precession2J2000 Transforma coordenadas ecuatoriales de una época determinada a J2000.
     * @param jd Fecha juliana del equinocio de entrada
     * @param ra_epoch Ascensión recta de la fecha en horas
     * @param dec_epoch Declinación de la fecha en grados
     * @param ra_j2000 Ascensión recta de la fecha en horas
     * @param dec_j2000 Declinación de la fecha en grados
     */
    SKYSHARED_EXPORT void Precession2J2000(double jd, double ra_epoch, double dec_epoch, double* ra_j2000, double* dec_j2000);


    SKYSHARED_EXPORT void PrecessionFromJ2000(double jd, double ra_j2000, double dec_j2000, double* ra_epoch, double* dec_epoch);

    /**
     * @brief Equ2Ecl Transforma coordenadas ecuatoriales a eclípticas.
     * La precisión es muy buena, mejor que 50mas
     * @param jd    Fecha para calcular la oblicuidad de la eclíptica.
     * @param ra    Ascensión recta en horas para la fecha indicada en JD
     * @param dec   Declinación en grados para la fecha indicada en JD
     * @param lon   Longitud eclíptica
     * @param lat   Latitud eclíptica
     */
    SKYSHARED_EXPORT void Equ2Ecl(double jd, double ra, double dec, double* lon, double* lat);

    /**
     * @brief Ecl2Equ Transforma coordenadas eclípticas en coordenadas ecuatoriales para la fecha indicada
     * en el parámetro jd.
     * @param jd    Fecha para calcular la oblicuidad de la eclíptica.
     * @param lon   Longitud eclíptica
     * @param lat   Latitud eclíptica
     * @param ra    Ascensión recta en horas para la fecha indicada en JD
     * @param dec   Declinación en grados para la fecha indicada en JD
     */
    SKYSHARED_EXPORT void Ecl2Equ(double jd, double lon, double lat, double* ra, double* dec);
}

#endif // SKY_H

















