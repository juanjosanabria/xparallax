#ifndef SKY_MPCORB_H
#define SKY_MPCORB_H

#include "comets.h"

long extract_astorb_dat( ELEMENTS *elem, const char *buff);
long extract_mpcorb_dat( ELEMENTS *elem, const char *buff);
void do_remaining_element_setup( ELEMENTS *elem);

#endif // MPCORB

