#include <qmath.h>
#include "sky.h"
#include "watdefs.h"
#include "date.h"
#include "comets.h"
#include "afuncs.h"
#include "mpcorb.h"
#include "earth2000.h"
#include "afuncs.h"

#define LOG_10               2.302585
#define LAWOFCOSINES(a,b,c) ((.5*((a)*(a)+(b)*(b)-(c)*(c))/((a)*(b))))
#define SIN_OBLIQ_2000       0.397777155931913701597179975942380896684
#define COS_OBLIQ_2000       0.917482062069181825744000384639406458043
#define PI                   3.141592653589793238462643383279502884197169399375105
#define GAUSS_K              0.01720209895
#define SOLAR_GM             (GAUSS_K * GAUSS_K)
#define AU                   149597870700 //Unidad astronómica en metros
#define C_AUPERDAY           173.14463267424   //Velocidad de la Luz en AU/dia
#define  ASEC2RAD            4.848136811095359935899141e-6
#define JDJ2000              2451545.00000000

//Factores de conversión entre unidades de ángulos
#define DEG2RAD     0.0174532925199432957692369076848900        //2*PI/360
#define DEG2HOR     0.0666666666666666666666666666666700        //24/360
#define RAD2DEG     57.295779513082320876798154814105000        //360/(2*PI)
#define HOR2DEG     15.000000000000000000000000000000000        //360/24
#define HOR2RAD     0.2617993877991494365385536152732900        //2*PI/24
#define RAD2HOR     3.8197186342054880584532103209403000        //24/(2*PI)
#define RAD2ASEC    206264.80624709635515647335733078000        //3600*2*PI
#define XAMIN2RAD   0.1047197551196597746154214461093200        //Arcmin a rad
#define RADIN360    6.2831853071795864769252867665590000        //2*PI

namespace Sky {

double DateToJD(int year, int month, double day)
{
    return dmy_to_day( (int)floor(day), month, year, CALENDAR_GREGORIAN) + (day - floor(day)) - 0.5;
}

bool ParseMPElems(char *tbuff, OrbElems* class_elem)
{
   if( !extract_mpcorb_dat((ELEMENTS*)class_elem, tbuff))
        return false;
   return true;
}

//http://www.minorplanetcenter.net/iau/info/CometOrbitFormat.html
bool ParseCometElems(char *tbuff, OrbElems* elem)
{
    if (strlen(tbuff) < 160) return false;

    //Tiempo de paso por el perihelio
    int year = atoi(tbuff + 14);
    int month = atoi(tbuff + 19);
    double day = atof(tbuff + 22);
    elem->perih_time = DateToJD(year, month, day);

    elem->q = atof(tbuff + 30);                   //Distancia al perihelio
    elem->ecc = atof(tbuff + 41);
    elem->major_axis = elem->q / (1- elem->ecc);  //Cálculo del semieje mayor
    elem->arg_per = atof(tbuff + 51);
    elem->asc_node = atof(tbuff +61);
    elem->incl = atof(tbuff + 71);

    int date_epoc = atoi(tbuff + 81);
    int year_e = date_epoc / 10000;
    int month_e = (date_epoc - year_e*10000) / 100;
    int day_e = (date_epoc - year_e*10000 - month_e*100);
    elem->epoch = dmy_to_day( day_e, month_e, year_e, CALENDAR_GREGORIAN) - 0.5;

    elem->abs_mag = atof(tbuff + 91);
    elem->slope_param= atof(tbuff + 96);
    elem->mean_anomaly = (elem->epoch - elem->perih_time)/elem->t0;

    //Correcciones finales
    elem->mean_anomaly  *= PI / 180.;
    elem->arg_per       *= PI / 180.;
    elem->asc_node      *= PI / 180.;
    elem->incl          *= PI / 180.;
    derive_quantities((ELEMENTS*)elem, SOLAR_GM);
    elem->angular_momentum = sqrt( SOLAR_GM * elem->q * (1. + elem->ecc));
    elem->is_asteroid = 0;
    elem->central_obj = 0;
    elem->gm = SOLAR_GM;
    return true;
}

float ObservedMag(OrbElems *_elem,double obj_sun,double obj_earth,double earth_sun)
{
    ELEMENTS* elem = (ELEMENTS*)_elem;
    double magnitude;
    if( !elem->is_asteroid)
       magnitude = elem->slope_param * log( obj_sun);
    else
       {
       const double cos_phase_ang =
                   LAWOFCOSINES( obj_sun, obj_earth, earth_sun);
       const double half_phase_ang = acose( cos_phase_ang) / 2.;
       const double log_tan_half_phase = log( tan( half_phase_ang));
       const double phi1 = exp( -3.33 * exp( log_tan_half_phase * 0.63));
       const double phi2 = exp( -1.87 * exp( log_tan_half_phase * 1.22));

       magnitude = 5. * log( obj_sun)
                            -2.5 * log( (1. - elem->slope_param) * phi1
                                             + elem->slope_param * phi2);
       }
    magnitude += 5. * log( obj_earth);
    magnitude /= LOG_10;      /* cvt from natural logs to common (base 10) */
    magnitude += elem->abs_mag;
    return (float) magnitude;
}

void CometPos(OrbElems* elems, double t,double* ra,double* dec,float* obsmag,double* elong,double* distobs)
{
    double earth_loc[6];
    double asteroid_loc[4];
    double r1, r2, r3;
    ELEMENTS* class_elem = (ELEMENTS*) elems;
    /* The following function is in EART2000.CPP. */
    get_earth_loc( (t - 2451545.0) / 365250., earth_loc);
    r3 = -100.;    /* ensure at least one pass */

    /* To deal with light-time lag,  an iterative process */
    /* is necessary.  In truth,  I suspect this could be  */
    /* replaced with a "for( i = 0; i < 2; i++)" loop (in */
    /* other words,  do it twice.)                        */
    double dist = 0.;
    while( fabs( dist - r3) > .01)
    {
        r3 = dist;
        comet_posn(class_elem, t - dist / AU_PER_DAY, asteroid_loc);
        dist = 0.;
        for(int j = 0; j < 3; j++)
        {
            asteroid_loc[j] -= earth_loc[j];
            dist += asteroid_loc[j] * asteroid_loc[j];
        }
        dist = sqrt( dist);
    }
    double x = asteroid_loc[0];
    double y = asteroid_loc[1] * COS_OBLIQ_2000 - asteroid_loc[2] * SIN_OBLIQ_2000;
    double z = asteroid_loc[1] * SIN_OBLIQ_2000 + asteroid_loc[2] * COS_OBLIQ_2000;
    /* Now convert that to RA/dec... */
    *ra = atan2( y, x) * 180. / M_PI;
    if(*ra < 0.) *ra += 360.;
    *dec = asin(z / dist) * 180. / M_PI;
    //Pasar RA a horas
    *ra /= 15.0;
    //Magnitud observada y enlongación
    r1 = dist;                                 /* home-target dist */
    r2 = earth_loc[5];                         /* home-sun dist */
    r3 = asteroid_loc[3];                      /* target-sun dist */
    if (obsmag) *obsmag = ObservedMag(elems, r3, r1, r2);
    /* Get the elongation from the Sun,  using the law */
    /* of cosines.                                     */
    if (elong) *elong = acose(LAWOFCOSINES( r1, r2, r3));
    //Distancia al observador
    if (distobs) *distobs = r1;
}

void CometPosAndVel(OrbElems* elems, double t, double* pos, double* vel)
{
    double earth_loc[6];
    double asteroid_loc[4];
    double r3;
    ELEMENTS* class_elem = (ELEMENTS*) elems;
    /* The following function is in EART2000.CPP. */
    get_earth_loc( (t - 2451545.0) / 365250., earth_loc);
    r3 = -100.;    /* ensure at least one pass */

    /* To deal with light-time lag,  an iterative process */
    /* is necessary.  In truth,  I suspect this could be  */
    /* replaced with a "for( i = 0; i < 2; i++)" loop (in */
    /* other words,  do it twice.)                        */
    double dist = 0.;
    while( fabs( dist - r3) > .01)
    {
        r3 = dist;
        comet_posn_and_vel(class_elem, t - dist / AU_PER_DAY, asteroid_loc, vel);
        dist = 0.;
        for(int j = 0; j < 3; j++)
        {
            asteroid_loc[j] -= earth_loc[j];
            dist += asteroid_loc[j] * asteroid_loc[j];
        }
        dist = sqrt( dist);
    }
    /*
    double x = asteroid_loc[0];
    double y = asteroid_loc[1] * COS_OBLIQ_2000 - asteroid_loc[2] * SIN_OBLIQ_2000;
    double z = asteroid_loc[1] * SIN_OBLIQ_2000 + asteroid_loc[2] * COS_OBLIQ_2000;
    */
    pos[0] = asteroid_loc[0];
    pos[1] = asteroid_loc[1] * COS_OBLIQ_2000 - asteroid_loc[2] * SIN_OBLIQ_2000;
    pos[2] = asteroid_loc[1] * SIN_OBLIQ_2000 + asteroid_loc[2] * COS_OBLIQ_2000;

}

//http://www.astro.caltech.edu/~mcs/CBI/pointing/
void TopoComet(double jd, double gra_j2000, double gdec_j2000, double d, double lat, double lng, double high, double* ra, double*dec)
{
    double gra, gdec;
    PrecessionFromJ2000(jd, gra_j2000, gdec_j2000, &gra, &gdec);

    //Calculamos los parámetros que afectan a la declinación
    double r = (6371000 + high) / AU;
    double _g = lat*2*M_PI/360.0;
    double g = _g + 0.1924*DEG2RAD * sin(2*_g);

    double sin2g = sin(g)*sin(g);
    double cos2g = cos(g)*cos(g);

    double dec_delta = acos((d-r*cos(g))/sqrt(d*d-2*d*r*cos(g)+r*r*cos2g+r*r*sin2g));
    *dec = gdec - dec_delta*360.0/(2*M_PI);

    //double dec_delta2 = atan( cos(g) /( d/r - sin(g)) );
    //*dec = gdec - dec_delta2*360.0/(2*M_PI);

    //Calculamos los parámetros que afectan a la ascensión recta
    double day_frac = (jd+0.5) - floor(jd + 0.5);   //Fracción del día
    double ha = 15*(day_frac*24) + lng - gra*15;             //Ángulo horario en grados
    while (ha>360) ha -= 360; while (ha<0) ha += 360;
    g = ha*2*M_PI/24.0;
    double ra_delta = acos((d-r*cos(g))/sqrt(d*d-2*d*r*cos(g)+r*r*cos2g+r*r*sin2g));
    *ra = gra - ra_delta*24.0/(2*M_PI);

    double tra, tdec;
    Precession2J2000(jd, *ra, *dec, &tra, &tdec);
    *ra = tra;
    *dec = tdec;
}



void AnnualAb(double t, double ra, double dec,double* ob_ra, double* ob_dec)
{
    /*
    double earth_loc[6];
    OrbElems orbelems;
    orbelems.ecc = 0.01671123;
    orbelems.epoch = 2451545.0000;
    orbelems.incl = -0.00001531   * (2*M_PI)/360.0;
    orbelems.asc_node = 0 * (2*M_PI)/360.0;
    orbelems.lon_per = 102.93768193;
    CometPosAndVel(&orbelems, t, earth_loc, earth_loc+3);

    //0 - Valores previos precalculados sobre ra y dec
    double cos_ra = cos(ra * (2*M_PI) / 24.0);
    double sin_ra = sin(ra * (2*M_PI) / 24.0);
    double cos_dec = cos(dec * (2*M_PI) / 360.0);
    double sin_dec = sin(dec * (2*M_PI) / 360.0);
    //1 - Obtener el vector velocidad de la Tierra para la fecha dada
    get_earth_loc( (t - 2451545.0) / 365250.0, earth_loc);
    //2 - Vector velocidad reducido (dividir por c). Cuidado, las unidades anteriores están en AU/dia
    double v_x  = earth_loc[3] / C_AUPERDAY;
    double v_y = earth_loc[4] / C_AUPERDAY;
    double v_z = earth_loc[5] / C_AUPERDAY;
    double Vc = sqrt(v_x*v_x+v_y*v_y+v_z*v_z); //Módulo de la velocidad normalizada
    earth_loc[3] *=1731456.83680556000;
    earth_loc[4] *=1731456.83680556000;
    earth_loc[5] *=1731456.83680556000;
    double V2 = sqrt(earth_loc[3]*earth_loc[3]+earth_loc[4]*earth_loc[4]+earth_loc[5]*earth_loc[5]);
    //3 - Vector de dirección de la estrella a corregir
    double u_u = cos_dec * cos_ra;
    double u_v = cos_dec * sin_ra;
    double u_w = sin_dec;
    //4 - Ángulo entre estos dos vectores cos(phi) = (a*b)/(|a|*|b|) (producto escalar/producto módulos)
    double cosphi = (v_x*u_u + v_y*u_v + v_z*u_w) / (sqrt(v_x*v_x+v_y*v_y+v_z*v_z)*sqrt(u_u*u_u+u_v*u_v+u_w*u_w));
    double sinphi = sqrt(1 - cosphi*cosphi);
    //5 - Calcular la normal N = u * v <-- Producto vectorial
    double L = v_z * cos_dec*sin_ra - v_y * sin_dec;
    double M = v_x * sin_dec - v_z * cos_ra * cos_dec;
    double N = v_y * cos_dec*cos_ra  - v_x * cos_dec*sin_ra;
    //6 - Componentes de u' x',y',z' (nuevo vector de dirección de la estrella)
    double xp = cos_dec*sin_ra*cosphi+Vc*(M*sin_dec-N*cos_dec*sin_ra)*sinphi;
    double yp = cos_dec*sin_ra*cosphi+Vc*(N*cos_dec*cos_ra-L*cos_dec)*sinphi;
    double zp = sin_dec*cosphi+Vc*(L*cos_dec*sin_ra-M*cos_dec*cos_ra)*sinphi;
    //7 - Cálculo de la ascensión recta y declinación (ra',dec') aparentes x'=cos_dec'*sin_ra', y=cos_dec'*cos_ra',z=sin_dec'
    double dec_p = asin(zp);
    double ra_p = acos(yp / cos(dec_p));
    //8 - Pasamos a grados y horas el resultado final
    *ob_ra = ra_p * 24.0 / (2*M_PI);
    *ob_dec = dec_p * 360.0 / (2*M_PI);
    */
    throw 1;
}

void Precession(double jd_tdb1, double* pos1, double* vel1, double jd_tdb2, double* pos2, double* vel2)
{
   static  bool first_time = true;
   double t_last = 0.0;
   double xx = 0, yx = 0, zx = 0, xy = 0, yy = 0, zy = 0, xz = 0, yz = 0, zz = 0;
   double eps0 = 84381.406;
   static double t, psia, omegaa, chia, sa, ca, sb, cb, sc, cc, sd, cd;
   /*
      Check to be sure that either 'jd_tdb1' or 'jd_tdb2' is equal to T0.
   */
   //Q_ASSERT(!((fabs(jd_tdb1-JDJ2000)>0.01) && (fabs(jd_tdb2-JDJ2000)>0.01)),"ERROR", "Alguna de las fechas debe ser J2000");
   if ((fabs(jd_tdb1-JDJ2000)>0.01) && (fabs(jd_tdb2-JDJ2000)>0.01))
   {
       pos2[0] = pos2[1] = pos2[2] =
       vel2[0] = vel2[1] = vel2[2] = NAN;
       //throw 1; //new Exception("Alguna de las fechas debe ser J2000");
       return;
   }

   /*
      't' is time in TDB centuries between the two epochs.
   */

   t = (jd_tdb2 - jd_tdb1) / 36525.0;

   if (fabs(jd_tdb2-JDJ2000)<0.01)
       t = -t;

   if ((abs(t - t_last) >= 1.0e-15) || (first_time))
   {
       /*
          Numerical coefficients of psi_a, omega_a, and chi_a, along with
          epsilon_0, the obliquity at J2000.0, are 4-angle formulation from
          Capitaine et al. (2003), eqs. (4), (37), & (39).
       */
       psia = ((((-0.0000000951 * t
                    + 0.000132851) * t
                    - 0.00114045) * t
                    - 1.0790069) * t
                    + 5038.481507) * t;

       omegaa = ((((+0.0000003337 * t
                    - 0.000000467) * t
                    - 0.00772503) * t
                    + 0.0512623) * t
                    - 0.025754) * t + eps0;

       chia = ((((-0.0000000560 * t
                    + 0.000170663) * t
                    - 0.00121197) * t
                    - 2.3814292) * t
                    + 10.556403) * t;

       eps0 = eps0 * ASEC2RAD;
       psia = psia * ASEC2RAD;
       omegaa = omegaa * ASEC2RAD;
       chia = chia * ASEC2RAD;
       sa = sin(eps0);
       ca = cos(eps0);
       sb = sin(-psia);
       cb = cos(-psia);
       sc = sin(-omegaa);
       cc = cos(-omegaa);
       sd = sin(chia);
       cd = cos(chia);
       /*
          Compute elements of precession rotation matrix equivalent to
          R3(chi_a) R1(-omega_a) R3(-psi_a) R1(epsilon_0).
       */
       xx = cd * cb - sb * sd * cc;
       yx = cd * sb * ca + sd * cc * cb * ca - sa * sd * sc;
       zx = cd * sb * sa + sd * cc * cb * sa + ca * sd * sc;
       xy = -sd * cb - sb * cd * cc;
       yy = -sd * sb * ca + cd * cc * cb * ca - sa * cd * sc;
       zy = -sd * sb * sa + cd * cc * cb * sa + ca * cd * sc;
       xz = sb * sc;
       yz = -sc * cb * ca - sa * cc;
       zz = -sc * cb * sa + cc * ca;

       t_last = t;
       first_time = false;
   }
   if (fabs(jd_tdb2-JDJ2000)<0.01)
   {
       /* Perform rotation from epoch to J2000.0.*/
       pos2[0] = xx * pos1[0] + xy * pos1[1] + xz * pos1[2];
       pos2[1] = yx * pos1[0] + yy * pos1[1] + yz * pos1[2];
       pos2[2] = zx * pos1[0] + zy * pos1[1] + zz * pos1[2];
       if (vel2 != NULL && vel1 != NULL)
       {
           vel2[0] = xx * vel1[0] + xy * vel1[1] + xz * vel1[2];
           vel2[1] = yx * vel1[0] + yy * vel1[1] + yz * vel1[2];
           vel2[2] = zx * vel1[0] + zy * vel1[1] + zz * vel1[2];
       }
   }
   else
   {
       /*Perform rotation from J2000.0 to epoch.*/
       pos2[0] = xx * pos1[0] + yx * pos1[1] + zx * pos1[2];
       pos2[1] = xy * pos1[0] + yy * pos1[1] + zy * pos1[2];
       pos2[2] = xz * pos1[0] + yz * pos1[1] + zz * pos1[2];
       if (vel2 != NULL && vel1 != NULL)
       {
           vel2[0] = xx * vel1[0] + yx * vel1[1] + zx * vel1[2];
           vel2[1] = xy * vel1[0] + yy * vel1[1] + zy * vel1[2];
           vel2[2] = xz * vel1[0] + yz * vel1[1] + zz * vel1[2];
       }
   }
}

void Precession2J2000(double jd, double ra_epoch, double dec_epoch, double* ra_j2000, double* dec_j2000)
{
   double r, d, cra, sra, cdc, sdc, pmr, pmd, rvl, xyproj;
   double pos1[] = {0,0,0};
   double pos2[] = {0,0,0};
   double vel1[] = {0,0,0};
   double vel2[] = {0,0,0};
   /* Convert right ascension, declination, and parallax to position
      vector in equatorial system with units of AU. */
   r = ra_epoch * 54000.0 * ASEC2RAD;
   d = dec_epoch * 3600.0 * ASEC2RAD;
   cra = cos(r);
   sra = sin(r);
   cdc = cos(d);
   sdc = sin(d);
   pos1[0] = cdc * cra;
   pos1[1] = cdc * sra;
   pos1[2] = sdc;
   /* Convert proper motion and radial velocity to orthogonal components
      of motion, in spherical polar system at star's original position,
      with units of AU/day.*/
   pmr = 0;
   pmd = 0;
   rvl = 0;
   /* Transform motion vector to equatorial system.*/
   vel1[0] = -pmr * sra - pmd * sdc * cra + rvl * cdc * cra;
   vel1[1] = pmr * cra - pmd * sdc * sra + rvl * cdc * sra;
   vel1[2] = pmd * cdc + rvl * sdc;
   for (int j = 0; j < 3; j++)
   {
       pos2[j] = pos1[j];
       vel2[j] = vel1[j];
   }
   //Aplicar la precesión al vector
   Precession(jd, pos1, vel1, JDJ2000, pos2, vel2);

   /* Convert vectors back to angular components for output.
   From updated position vector, obtain star's new position expressed
   as angular quantities.*/
   xyproj = sqrt(pos2[0] * pos2[0] + pos2[1] * pos2[1]);

   if (xyproj > 0.0)
       r = atan2(pos2[1], pos2[0]);
   else
       r = 0.0;

   *ra_j2000 = r / ASEC2RAD / 54000.0;
   if (*ra_j2000 < 0.0)
       *ra_j2000 += 24.0;
   if (*ra_j2000 >= 24.0)
       *ra_j2000 -= 24.0;

   d = atan2(pos2[2], xyproj);
   *dec_j2000 = d / ASEC2RAD / 3600.0;
}

void PrecessionFromJ2000(double jd, double ra_j2000, double dec_j2000, double* ra_epoch, double* dec_epoch)
{
   double r, d, cra, sra, cdc, sdc, pmr, pmd, rvl, xyproj;
   double pos1[] = {0,0,0};
   double pos2[] = {0,0,0};
   double vel1[] = {0,0,0};
   double vel2[] = {0,0,0};
   /* Convert right ascension, declination, and parallax to position
      vector in equatorial system with units of AU.*/
   r = ra_j2000 * 54000.0 * ASEC2RAD;
   d = dec_j2000 * 3600.0 * ASEC2RAD;
   cra = cos(r);
   sra = sin(r);
   cdc = cos(d);
   sdc = sin(d);
   pos1[0] = cdc * cra;
   pos1[1] = cdc * sra;
   pos1[2] = sdc;
   /* Convert proper motion and radial velocity to orthogonal components
      of motion, in spherical polar system at star's original position,
      with units of AU/day.*/
   pmr = 0;
   pmd = 0;
   rvl = 0;
   /* Transform motion vector to equatorial system. */
   vel1[0] = -pmr * sra - pmd * sdc * cra + rvl * cdc * cra;
   vel1[1] = pmr * cra - pmd * sdc * sra + rvl * cdc * sra;
   vel1[2] = pmd * cdc + rvl * sdc;
   for (int j = 0; j < 3; j++)
   {
       pos2[j] = pos1[j];
       vel2[j] = vel1[j];
   }
   Precession(JDJ2000, pos1, vel1, jd, pos2, vel2);

   /* Convert vectors back to angular components for output.

   From updated position vector, obtain star's new position expressed
   as angular quantities.
   */

   xyproj = sqrt(pos2[0] * pos2[0] + pos2[1] * pos2[1]);

   if (xyproj > 0.0)
       r = atan2(pos2[1], pos2[0]);
   else
       r = 0.0;

   *ra_epoch = r / ASEC2RAD / 54000.0;
   if (*ra_epoch < 0.0)
       *ra_epoch += 24.0;
   if (*ra_epoch >= 24.0)
       *ra_epoch -= 24.0;

   d = atan2(pos2[2], xyproj);
   *dec_epoch = d / ASEC2RAD / 3600.0;
}


void Equ2Ecl(double jd, double _ra, double _dec, double* lon, double* lat)
{
    //Vector coordenadas ecuatoriales
    double ra = _ra * HOR2RAD;
    double dec = _dec * DEG2RAD;
    double xp = cos(dec) * cos(ra);
    double yp = cos(dec) * sin(ra);
    double zp = sin(dec);
    //Calcular julian centuries desde la época
    double T = (jd - 2451545.0) / 36525.0;
    //Oblicuidad de la eclíptica en grados
    double eps = 23.43929111 - (46.8150 / 3600.0) * T - (0.00059 / 3600.0) * T * T + (0.001813 / 3600.0) * T * T * T;
    //Coordenadas eclípticas
    double x = xp;
    double y = yp * cos(eps * DEG2RAD) + zp * sin(eps * DEG2RAD);
    double z = -yp * sin(eps * DEG2RAD) + zp * cos(eps * DEG2RAD);
    //Obtener latitud y longitud eclípticas
    double xyproj = sqrt(x * x + y * y);
    double e = (xyproj > 0.0) ? atan2(y, x) :  0.0;
    *lon = e * RAD2DEG;
    if (*lon < 0.0) *lon += 360.0;
    e = atan2(z, xyproj);
    *lat = e * RAD2DEG;
}

void Ecl2Equ(double jd, double _lon, double _lat, double* ra, double* dec)
{
    //Vector coordenadas eclípticas
    double lon = _lon * DEG2RAD;
    double lat = _lat * DEG2RAD;
    double xp = cos(lat) * cos(lon);
    double yp = cos(lat) * sin(lon);
    double zp = sin(lat);
    //Calcular julian centuries desde la época
    double T = (jd - 2451545.0) / 36525.0;
    //Oblicuidad de la eclíptica en grados
    double eps = 23.43929111 - (46.8150 / 3600.0) * T - (0.00059 / 3600.0) * T * T + (0.001813 / 3600.0) * T * T * T;
    //Coordenadas eclípticas
    double x = xp;
    double y = yp * cos(eps * DEG2RAD) - zp * sin(eps * DEG2RAD);
    double z = yp * sin(eps * DEG2RAD) + zp * cos(eps * DEG2RAD);
    //Obtener la ascensión recta y la declinación
    double xyproj = sqrt(x * x + y * y);
    double e = (xyproj > 0.0) ? atan2(y, x) :  0.0;
    *ra = e * RAD2HOR;
    if (*ra < 0.0) *ra += 24.0;
    e = atan2(z, xyproj);
    *dec = e * RAD2DEG;
}

}//namespace Sky




















