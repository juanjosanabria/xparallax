/* comets.h: header file for comet/asteroid functions
Copyright (C) 2010, Project Pluto

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.    */
#ifndef SKY_COMETS_H
#define SKY_COMETS_H
#include <stdint.h>

#define ELEMENTS struct elements

#pragma pack(4)
ELEMENTS
   {
   double perih_time, q, ecc, incl, arg_per, asc_node;
   double epoch,  mean_anomaly;
            /* derived quantities: */
   double lon_per, minor_to_major;
   double perih_vec[3], sideways[3];
   double angular_momentum, major_axis, t0, w0;
   double abs_mag, slope_param, gm;
   int is_asteroid, central_obj;
   };
#pragma pack( )

/* Note that in the above structure,  t0 = 1/n = time to move */
/* one radian in mean anomaly = orbital period / (2*pi).      */


// void calc_vectors( ELEMENTS *elem, const double sqrt_gm);
int calc_classical_elements( ELEMENTS *elem, const double *r,
                             const double t, const int ref);
int comet_posn_and_vel( ELEMENTS *elem, double t,double *loc, double *vel);
int comet_posn( ELEMENTS *elem, double t, double *loc);       /* astfuncs.c */
void derive_quantities( ELEMENTS *e, const double gm);
int setup_elems_from_ast_file(ELEMENTS *class_elem, const uint32_t *elem, const double t_epoch);


#endif
