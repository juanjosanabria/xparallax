#ifndef ALT_AZ_H
#define ALT_AZ_H
#include "afuncs.h"

void ra_dec_to_alt_az( const double hr_ang, const double dec,
                       double *alt, double*az, const double lat);

void  full_ra_dec_to_alt_az(const DPT*ra_dec,
                             DPT*alt_az,
                             DPT*loc_epoch, const DPT*latlon,
                             const double jd_utc, double*hr_ang);

void alt_az_to_ra_dec( double alt, double az,
                         double*hr_ang,
                         double*dec, const double lat);

void  full_alt_az_to_ra_dec( DPT*ra_dec,
                              const DPT*alt_az,
                              const double jd_utc, const DPT*latlon);

void  ra_dec_to_supergalactic( const double ra, const double dec,
                   double*glat, double*glon);

void  supergalactic_to_ra_dec( const double glat, double glon,
                    double*ra, double*dec);

void  ra_dec_to_galactic( const double ra, const double dec,
                   double*glat, double*glon);

void  galactic_to_ra_dec( const double glat, double glon,
                    double*ra, double*dec);


void  precess_pt( DPT*opt, const DPT*ipt,
                        double from_time, double to_time);
#endif // ALT_AZ_H

