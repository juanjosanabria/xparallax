#ifndef NOVAS_GLOBAL_H
#define NOVAS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NOVAS_LIBRARY)
#  define NOVASSHARED_EXPORT Q_DECL_EXPORT
#else
#  define NOVASSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // NOVAS_GLOBAL_H
