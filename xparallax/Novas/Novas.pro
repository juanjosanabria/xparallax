#-------------------------------------------------
#
# Project created by QtCreator 2015-01-15T12:35:07
#
#-------------------------------------------------

QT       -= gui

TARGET = Novas
TEMPLATE = lib

DEFINES += NOVAS_LIBRARY _CRT_SECURE_NO_WARNINGS
DEFINES  += _CRT_SECURE_NO_WARNINGS

SOURCES += \
    eph_manager.cpp \
    novas.cpp \
    novascon.cpp \
    nutation.cpp \
    readeph0.cpp \
    solsys3.cpp

HEADERS +=\
        novas_global.h \
    nutation.h \
    solarsystem.h \
    novascon.h \
    eph_manager.h \
    novas.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
