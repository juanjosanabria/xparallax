#include "core.h"

#include <QObject>
#include <QString>
#include <QMetaProperty>
#include <QFileInfo>
#include <QSettings>
#include <QStringList>
#include <QDate>
#include <QUuid>
#include <QPoint>
#include <signal.h>
#include "internal.h"
#include "util.h"


void Core::AssertionFailure(const char *exp,const char* file, int line)
{
    qDebug() << QFileInfo(file).baseName() << "[" << line << "] " <<  exp;
    raise(SIGINT);
}

bool Core::littleEndian()
{
   return Q_BYTE_ORDER == Q_LITTLE_ENDIAN;
}

int Core::randInt(int low, int high)
{
    if (low == -1 && high == -1)
    { low = 0; high = RAND_MAX;}
    else if (high == -1)
    {high = low; low = 0;}
    return qrand() % (high - low) + low;
}

float Core::randFloat(float low, float max)
{
    float f = (float) randInt() / (float)RAND_MAX;
    return low + f * (max-low);
}

Parameters::Parameters()
{

}

Parameters::~Parameters()
{

}

void Parameters::RemoveKey(QSettings* sett, const QString& section, const QString& name)
{
    sett->remove(QString("%1/%2").arg(section, name));
}

void Parameters::SaveFloat(QSettings* sett,const QString& section,const QString& name, float value, int decimal_places)
{
    sett->setValue(QString("%1/%2").arg(section, name), QString::number(value, 'f' , decimal_places) );
}

void Parameters::SaveDouble(QSettings* sett,const QString& section,const QString& name, double value, int decimal_places)
{
    sett->setValue(QString("%1/%2").arg(section, name), QString::number(value, 'f' , decimal_places) );
}

void Parameters::SaveString(QSettings* sett,const QString& section,const QString& name, const QString&  value)
{
    sett->setValue(QString("%1/%2").arg(section, name), value);
}

void Parameters::SaveRA(QSettings* sett,const QString& section,const QString& name, double value)
{
    QString str_ra = Util::RA2String(value);
    SaveString(sett, section, name, str_ra);
}

void Parameters::SaveDEC(QSettings* sett,const QString& section,const QString& name, double value)
{
    QString str_dec = Util::DEC2String(value);
    SaveString(sett, section, name, str_dec);
}


void Parameters::SaveBool(QSettings* sett,const QString& section,const QString& name, bool  value)
{
    sett->setValue(QString("%1/%2").arg(section, name), value);
}

void Parameters::SaveInt(QSettings* sett,const QString& section,const QString& name, int  value)
{
    sett->setValue(QString("%1/%2").arg(section, name), value);
}

void Parameters::SaveDate(QSettings* sett,const QString& section,const QString& name,const QDate& value)
{
    sett->setValue(QString("%1/%2").arg(section, name), value.toString("yyyy-MM-dd") );
}


int Parameters::ReadInt(QSettings* sett,const QString& section,const QString& name, int def_value)
{
    bool ok = false;
    QVariant var = sett->value(QString("%1/%2").arg(section, name), def_value);
    if (var.isNull()) return def_value;
    int val = var.toInt(&ok);
    if (!ok) return def_value;
    return val;
}

float Parameters::ReadFloat(QSettings* sett,const QString& section,const QString& name, float def_value)
{
    bool ok = false;
    QVariant var = sett->value(QString("%1/%2").arg(section, name), def_value);
    if (var.isNull()) return def_value;
    float val = var.toFloat(&ok);
    if (!ok) return def_value;
    return val;
}

double Parameters::ReadDouble(QSettings* sett,const QString& section,const QString& name, double def_value)
{
    bool ok = false;
    QVariant var = sett->value(QString("%1/%2").arg(section, name), def_value);
    if (var.isNull()) return def_value;
    double val = var.toDouble(&ok);
    if (!ok) return def_value;
    return val;
}


double Parameters::ReadRA(QSettings* sett,const QString& section,const QString& name, float def_value)
{
    QString str = ReadString(sett, section, name, "");
    if (!str.length()) return def_value;
    return Util::ParseRA(str);
}

double Parameters::ReadDEC(QSettings* sett,const QString& section,const QString& name, float def_value)
{
    QString str = ReadString(sett, section, name, "");
    if (!str.length()) return def_value;
    return Util::ParseDEC(str);
}

QString Parameters::ReadString(QSettings* sett,const QString& section,const QString& name, const QString& def_value)
{
    QVariant var = sett->value(QString("%1/%2").arg(section, name), def_value);
    if (var.isNull()) return def_value;
    return var.toString();
}

bool Parameters::ReadBool(QSettings* sett,const QString& section,const QString& name, bool def_value)
{
    QVariant var = sett->value(QString("%1/%2").arg(section, name), def_value);
    if (var.isNull()) return def_value;
    return var.toBool();
}


QDate Parameters::ReadDate(QSettings* sett,const QString& section,const QString& name, const QDate& def_value)
{
    QVariant var = sett->value(QString("%1/%2").arg(section, name), "");
    if (var.isNull()) return def_value;
    QStringList dtt = var.toString().split("-");
    if (dtt.count() != 3) return def_value;
    int day, month, year; bool ok;
    year = dtt[0].toInt(&ok);
    if (!ok || !year) return def_value;
    month = dtt[1].toInt(&ok);
    if (!ok || month < 1 || month > 12) return def_value;
    day = dtt[2].toInt(&ok);
    if (!ok || day < 1 || day > 31) return def_value;
    return QDate(year, month, day);
}

void Parameters::SaveGuid(QSettings* sett,const QString& section,const QString& name,const QUuid& value)
{
     sett->setValue(QString("%1/%2").arg(section, name), value.toString() );
}

QUuid Parameters::ReadGuid(QSettings* sett,const QString& section,const QString& name, const QUuid& def_value)
{
    QVariant var =  sett->value(QString("%1/%2").arg(section, name), "");
    QUuid uid = QUuid(var.toString());
    if (uid.isNull()) return def_value;
    return uid;
}

void Parameters::SaveSize(QSettings* sett,const QString& section,const QString& name,const QSize& value)
{
    sett->setValue(QString("%1/%2").arg(section, name), QString("%1x%2").arg(value.width()).arg(value.height()) );
}

QSize Parameters::ReadSize(QSettings* sett,const QString& section,const QString& name, const QSize& def_value)
{
    QVariant var =  sett->value(QString("%1/%2").arg(section, name), "0x0");
    QStringList sl = var.toString().split("x");
    if (sl.length() != 2) return def_value;
    int width , height;
    bool ok;
    width = sl.at(0).toInt(&ok);
    if (!ok) return def_value;
    height = sl.at(1).toInt(&ok);
    if (!ok) return def_value;
    return QSize(width, height);
}

void Parameters::SavePoint(QSettings* sett,const QString& section,const QString& name,const QPoint& value)
{
  sett->setValue(QString("%1/%2").arg(section, name), QString("%1:%2").arg(value.x()).arg(value.y()) );
}

QPoint Parameters::ReadPoint(QSettings* sett,const QString& section,const QString& name, const QPoint& def_value)
{
    QVariant var =  sett->value(QString("%1/%2").arg(section, name), "-1:-1");
    QStringList sl = var.toString().split(":");
    if (sl.length() != 2) return def_value;
    int x , y;
    bool ok;
    x = sl.at(0).toInt(&ok);
    if (!ok) return def_value;
    y = sl.at(1).toInt(&ok);
    if (!ok) return def_value;
    return QPoint(x, y);
}

void Parameters::SaveStringList(QSettings* sett,const QString& section,const QString& name,const QStringList& value)
{
    sett->setValue(QString("%1/%2").arg(section, name), value );
}

QStringList Parameters::ReadStringList(QSettings* sett,const QString& section,const QString& name, const QStringList& def_value)
{
    QVariant var =  sett->value(QString("%1/%2").arg(section, name), def_value);
    if (var.isNull() || !var.isValid()) return def_value;
    return var.toStringList();
}

void Parameters::SaveFont(QSettings* sett,const QString& section,const QString& name,const QFont& fnt)
{
    SaveString(sett, section, name, fnt.toString());
}

QFont Parameters::ReadFont(QSettings* sett,const QString& section,const QString& name, const QFont& def_value)
{
    QString str = ReadString(sett, section, name, "");
    if (!str.length()) return def_value;
    QFont fnt;
    bool ok = fnt.fromString(str);
    if (!ok) return def_value;
    return fnt;
}

EnviParameters::EnviParameters()
{
    SetDefaults();
}

EnviParameters::~EnviParameters()
{

}

void EnviParameters::SetDefaults()
{
    uid =  QUuid::createUuid();
    show_log_window = true;
    mainwindow_maximized = true;
    last_check_for_updates = QDate(1900,1,1);
    mainwindow_size = QSize(0,0);
    mainwindow_pos  = QPoint(0,0);
    caching_memory = false;
    caching_disk = true;
}

void EnviParameters::Save(QSettings* settings,const QString& section)
{
    SaveGuid(settings, section, "uid", uid);
    SaveBool(settings, section, "show_log_window", show_log_window);
    SaveBool(settings, section, "mainwindow_maximized", mainwindow_maximized);
    SaveDate(settings, section, "last_check_for_updates", last_check_for_updates);
    SaveSize(settings, section, "mainwindow_size", mainwindow_size );
    SavePoint(settings, section, "mainwindow_pos", mainwindow_pos );
    SaveString(settings, section, "last_fit_open_directory", last_fit_open_directory);
    SaveStringList(settings, section, "recent_fits" ,recent_fits);
    SaveInt(settings, section, "run_t", run_c);
    SaveDate(settings, section, "last_update_warn", last_update_warn);
    SaveBool(settings, section, "do_not_prompt_updates", do_not_prompt_updates);
    SaveBool(settings, section, "caching_memory", caching_memory);
    SaveBool(settings, section, "caching_disk", caching_disk);

    //Contadores de ejecución
    SaveInt(settings, section, "ct_ast", ct_ast);
    SaveInt(settings, section, "ct_fast", ct_fast);
    SaveInt(settings, section, "ct_cal", ct_cal);
    SaveInt(settings, section, "ct_fcal", ct_fcal);
    SaveInt(settings, section, "ct_bhe", ct_bhe);
    SaveInt(settings, section, "ct_fbhe", ct_fbhe);
    SaveInt(settings, section, "ct_ica", ct_ica);
    SaveInt(settings, section, "ct_fica", ct_fica);
    SaveInt(settings, section, "ct_mpc", ct_mpc);
}

void EnviParameters::Load(QSettings* settings,const QString& section)
{
    uid = ReadGuid(settings, section, "uid",  QUuid::createUuid());
#ifdef QT_DEBUG
    uid = QUuid(DEBUG_UID);
#endif
    show_log_window = ReadBool(settings, section, "show_log_window", true);
    mainwindow_maximized = ReadBool(settings, section, "mainwindow_maximized", true);
    last_check_for_updates = ReadDate(settings, section, "last_check_for_updates", QDate(1900,1,1));
    mainwindow_size = ReadSize(settings, section, "mainwindow_size", QSize(0,0));
    mainwindow_pos = ReadPoint(settings, section, "mainwindow_pos", QPoint(0,0));
    last_fit_open_directory = ReadString(settings, section, "last_fit_open_directory", "./");
    recent_fits = ReadStringList(settings, section, "recent_fits", QStringList());
    run_c = ReadInt(settings, section, "run_t", 0);
    last_update_warn = ReadDate(settings, section, "last_update_warn", QDate(1900,1,1));
    do_not_prompt_updates = ReadBool(settings, section, "do_not_prompt_updates", false);
    caching_memory = ReadBool(settings, section, "caching_memory", false);
    caching_disk = ReadBool(settings, section, "caching_disk", true);

    //Contadores de ejecución
    ct_ast = ReadInt(settings, section, "ct_ast", 0);
    ct_fast = ReadInt(settings, section, "ct_fast", 0);
    ct_cal = ReadInt(settings, section, "ct_cal", 0);
    ct_fcal = ReadInt(settings, section, "ct_fcal", 0);
    ct_bhe = ReadInt(settings, section, "ct_bhe", 0);
    ct_fbhe = ReadInt(settings, section, "ct_fbhe", 0);
    ct_ica = ReadInt(settings, section, "ct_ica", 0);
    ct_fica = ReadInt(settings, section, "ct_fica", 0);
    ct_mpc = ReadInt(settings, section, "ct_mpc", 0);
}






