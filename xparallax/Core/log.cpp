#include "log.h"
#include "xglobal.h"
#include <QThread>

Logger::Logger()
{
#ifdef QT_DEBUG
    mOutputDebug = true;
#else
    mOutputDebug = false;
#endif
    mOutputFile = NULL;
    mFil = NULL;
}


Logger::~Logger()
{
    if (mOutputFile) delete mOutputFile;
    mOutputFile = NULL;
    if (mFil) fclose(mFil);
    mFil = NULL;
}


void Logger::setOutputFile(char* filename)
{
    if (mFil) fclose(mFil); mFil = NULL;
    if (mOutputFile) delete mOutputFile; mOutputFile = NULL;
    mOutputFile = new char[strlen(filename)+1];
    strcpy(mOutputFile, filename);
}

void Logger::setOutputFile(QString filename)
{
    setOutputFile(filename.toLatin1().data());
}

void Logger::info(const char *fmt, ...)
{
    mMutex.lock();
    va_list argptr;
    va_start(argptr, fmt);
    vsnprintf(mBuffer, LOG_BUFFER_LEN, fmt, argptr);
    va_end(argptr);
    if (mOutputDebug) qDebug() << mBuffer;
    emit msg((qint64)QThread::currentThreadId(), (int)LOGTYPE_INFO, mBuffer);
    if (mOutputFile) logToFile(mBuffer);
    mMutex.unlock();
}

void Logger::warn(const char *fmt, ...)
{
    mMutex.lock();
    va_list argptr;
    va_start(argptr, fmt);
    vsnprintf(mBuffer, LOG_BUFFER_LEN, fmt, argptr);
    va_end(argptr);
    if (mOutputDebug) qDebug() << mBuffer;
    emit msg((qint64)QThread::currentThreadId(), (int)LOGTYPE_WARN, mBuffer);
    if (mOutputFile) logToFile(mBuffer);
    mMutex.unlock();
}

void Logger::error(const char *fmt, ...)
{
    mMutex.lock();
    va_list argptr;
    va_start(argptr, fmt);
    vsnprintf(mBuffer, LOG_BUFFER_LEN, fmt, argptr);
    va_end(argptr);
    if (mOutputDebug) qDebug() << mBuffer;
    emit msg((qint64)QThread::currentThreadId(), LOGTYPE_ERROR, mBuffer);
    if (mOutputFile) logToFile(mBuffer);
    mMutex.unlock();
}

void Logger::debug(const char *fmt, ...)
{
    if (!XGlobal::DEBUG) return;
    mMutex.lock();
    va_list argptr;
    va_start(argptr, fmt);
    vsnprintf(mBuffer, LOG_BUFFER_LEN, fmt, argptr);
    va_end(argptr);
    if (mOutputDebug) qDebug() << mBuffer;
    emit msg((qint64)QThread::currentThreadId(), LOGTYPE_DEBUG, mBuffer);
    if (mOutputFile) logToFile(mBuffer);
    mMutex.unlock();
}

void Logger::info(const QString& cad)
{
    info("%s",PTQ(cad));
}

void Logger::warn(const QString& cad)
{
    warn("%s",PTQ(cad));
}

void Logger::error(const QString& cad)
{
    error("%s",PTQ(cad));
}

void Logger::debug(const QString& cad)
{
    if (!XGlobal::DEBUG) return;
    debug("%s",PTQ(cad));
}

const Logger & Logger::operator<<(const QString & t)
{
   QByteArray b = t.toLatin1();
   info("%s",b.data());
   return *this;
}

void Logger::logToFile(char* cad)
{
    if (!mOutputFile) return;
    if (!mFil) mFil = fopen(mOutputFile, "w+t");
    if (!mFil) return;
    QDateTime dtt = QDateTime::currentDateTime();
    fprintf(mFil, "%04d-%02d-%02d %02d:%02d:%02d.%03d %s\n",
            dtt.date().year(),dtt.date().month(),dtt.date().day(),
            dtt.time().hour(),dtt.time().minute(),dtt.time().second(),dtt.time().msec(),
            cad);
    fflush(mFil);
}




