#ifndef VO_H
#define VO_H

#include <QString>
#include <QStringList>
#include <QSettings>
#include <QNetworkReply>
#include "util.h"

#include "core_global.h"
#include "core.h"


class CORESHARED_EXPORT VizierCatalog
{
public:
    VizierCatalog(int index){mCatIndex = index;}
    static void loadCatalogs(QSettings& settings);
    static QList<VizierCatalog*>& getCatalogs();
    static void DiscardCatalogs();
    static VizierCatalog* getCatalog(const QString& catname);
    static QList<VizierCatalog*>  SOURCE_CATALOGS;

    inline int objcount(){return mObjectCount;}
    inline QString& name(){return mName;}
    inline QString& desc(){return mDesc;}
    inline QString& path(){return mCatPath;}
    inline QStringList& fields(){return mFields;}
    inline QStringList& filters(){return mFilters;}
    inline float maxmag(){return mMagMax;}
    inline QString& magfilter(){return magFilter;}
    QString CreateMagnitudeFilter(float max_mag);
    inline int index(){return mCatIndex;}
    //Nuevos campos añadisos tras GAIA DR1
    //---------------------------------------------------------------------
    inline QString& iDFieldName(){return mIDFieldName;}
    inline QString& raFieldName(){return mRAFieldName;}
    inline QString& decFieldName(){return mDECFieldName;}
    inline float raDecEpoch(){return mRADECEpoch;}

private:
    int          mCatIndex;
    int          mObjectCount;
    QString      mIDFieldName;  //Identificador de la fuente
    QString      mRAFieldName;  //Nombre del campo que contiene la ascensión recta
    QString      mDECFieldName; //Nombre del campo que contiene la declinación
    float        mRADECEpoch;   //Época. Establecido a NAN ó <0 si es J2000
    QString      mName;
    QString      mDesc;
    QString      mCatPath;
    QStringList  mFields;
    QStringList  mFilters;
    QStringList  mCatalogs;      //Catálogos replicados en este servidor
    float        mMagMax;
    QString      magFilter;      //Constructor de la URL

};

class CORESHARED_EXPORT VizierServer
{
public:
    VizierServer(){;}
    static void LoadSourceServers(QSettings& settings);
    static void DiscardServers();
    static VizierServer* getServer(const QString& servname);
    static QList<VizierServer*> SOURCE_SERVERS;

    inline const QString& name(){return mName;}
    inline const QString& desc(){return mDesc;}
    inline const QString& url(){return mUrl;}
    inline QStringList& catalogs(){return mCatalogs;}

private:
    QString     mName;
    QString     mDesc;
    QString     mUrl;
    QStringList mCatalogs;

};

class CORESHARED_EXPORT VizierRecord
{
friend class VizierReader;

public:
    inline VizierRecord(){}
    inline VizierRecord(int catindex, QStringList fields){mCatIndex = catindex; mFields = fields;}
    inline ~VizierRecord(){;}

    inline QString id() const {return mFields.count() ? mFields.at(0) : "";}
    inline const QString& field(int i) const{return mFields.at(i);}
    inline const QString& field(const QString& name) const;


    float anyMag() const;
    float mag(const QString& pref) const;
    float averageMag(QStringList sl);
    double ra() const;
    double dec() const;
    double pmra() const;
    double pmdec() const;
    void setRa(double newra);
    void setDec(double newDec);

private:
    QStringList          mFields;
    int                  mCatIndex;     //Índice del catálogo en la lista de catálogos
    static const char*   MAGS[];
};





class CORESHARED_EXPORT NameRecord
{
friend class SesameReader;

public:
    NameRecord();
    ~NameRecord();

    inline double ra(){return mRA;}
    inline double dec(){return mDEC;}
    inline QString& name(){return mName;}
    inline int  entries(){return mEntries;}
    inline bool isnull(){return mIsNull;}
    inline QString& type(){return mType;}
    inline double pmra(){return mPMRA;}
    inline double pmdec(){return mPMDEC;}
    inline void setNull(bool null=true){mIsNull = null;}

private:
    QString mName;
    double  mRA;
    double  mDEC;
    int     mEntries;
    bool    mIsNull;
    QString mType;
    double  mPMRA;
    double  mPMDEC;

};

class CORESHARED_EXPORT VizierReader
{

public:
    VizierReader();
    ~VizierReader();

    inline VizierCatalog* catalog(){return mCatalog;}
    inline void SetCatalog(VizierCatalog* cat){mCatalog = cat;}
    inline VizierServer* server(){return mServer;}
    inline void SetServer(VizierServer* mirror){mServer = mirror;}
    inline void setUseCache(bool use) { useCache_ = use; }
    ///
    /// \brief WebRead      Realiza una lectura
    /// \param c_ra         Ascensión recta en horas
    /// \param c_dec        Declinación en grados
    /// \param rad_min      Radio en minutos de arco
    /// \param max_mag      Máxima magnitud
    /// http://vizier.u-strasbg.fr/viz-bin/asu-tsv?-source=I/317&-c=285.961394%2036.632900&-c.r=0.513498&-c.u=deg&-out.form=|&-out=PPMXL&-out=RAJ2000&-out=DEJ2000&-out=pmRA&-out=pmDE&-out=Jmag&-out=Hmag&-out=Kmag&-out=b1mag&-out=b2mag&-out=r1mag&-out=r2mag&-out=imag&r1mag=<15
    /// \return
    ///
    bool webRead(double c_ra, double c_dec, double rad_deg, float max_mag);
    bool WebRead2(double c_ra, double c_dec, double rad_deg, float max_mag);
    NameRecord ResolveName(const QString& name);

    static void loadConf();     //Carga las variables de configuración en memoria
    static void ClearConf();    //Descarga las variables de configuración de memoria

    inline QList<VizierRecord>* stars() { return &mEntries; }

private:
     VizierCatalog*             mCatalog;   //No tenemos el control de memoria
     VizierServer*              mServer;    //No tenemos el control de memoria
     bool                       useCache_;
     QList<VizierRecord>        mEntries;
     QStringList                mFieldNames;
     QStringList                mUnits;
     QString                    mErrorString;

     QString  CreateQueryUrl(double c_ra, double c_dec, double  rad_deg, float max_mag);

};

/**
 * @brief Lectura y búsqueda de objetos en el servicio de Sessame
 */
class CORESHARED_EXPORT SesameReader
        : public QObject
{
    Q_OBJECT

public:
    SesameReader();
    ~SesameReader();
    QString& errormsg(){return mErrorMessage;}
    NameRecord resolveName(const QString& name);
    void resolveNameAsync(const QString& name);
    static void loadConf();
    static void ClearConf();

signals:
    void Error_Slot(QString& );
    void InitRead_Signal();
    void EndRead_Signal(NameRecord& record);


private:
    QString             mErrorMessage;
    NameRecord         mNameRecord;

    static QString      SESAME_URL;

private slots:

    void finished_Slot(QNetworkReply* reply);


};



#endif // VO_H
