#include "projection.h"
#include <qmath.h>

Projection::Projection()
{

}

Projection::~Projection()
{

}

GnomonicProjection::GnomonicProjection()
{

}

GnomonicProjection::GnomonicProjection(double center_x, double center_y, double center_ra, double center_dec,
                                       double rot1, double rot2, double scale_x, double scale_y)
{
    CenterPixX = center_x;
    CenterPixY = center_y;
    Alphai = center_ra * HOR2DEG;
    Deltai = center_dec;
    Rotation1 = rot1;
    Rotation2 = rot2;
    ScaleX = scale_x / 3600.0;  //Pasamos a grados por píxel
    ScaleY = scale_y / 3600.0;
    ComputeIntermediate();
}

GnomonicProjection::~GnomonicProjection()
{

}

void GnomonicProjection::ComputeIntermediate()
{
    //Matriz de transformación directa. Aplica rotación y escalado a XY
    double rota = Rotation1 ;
    if (Rotation1 == 0) rota = Rotation2;
    else if (Rotation2 != 0) rota = (Rotation1+Rotation2 )/2 ;;
    CD[0][0] = ScaleX * cos((rota/180.0)*M_PI);
    CD[0][1] = ScaleY * sin((rota / 180.0) * M_PI);
    CD[1][0] = ScaleX * sin((rota / 180.0) * M_PI);
    CD[1][1] = -ScaleY * cos((rota / 180.0) * M_PI);
    //Parámetros de transformación gnomónica
    cdelz = cos(Deltai*DEG2RAD);
    sdelz = sin(Deltai*DEG2RAD);

    //Matriz de transformación inversa,
    double det = CD[0][0]* CD[1][1]-CD[0][1]*CD[1][0] ;
    ID[0][0] = CD[1][1] / det;
    ID[0][1] = -CD[0][1] / det;
    ID[1][0] = -CD[1][0] / det;
    ID[1][1] = CD[0][0] / det;
}

void GnomonicProjection::XYToRADec(double x, double y,float* ra, float* dec)
{
    double _ra, _dec;
    XYToRADec(x, y, &_ra,&_dec);
    *ra = (float)_ra;
    *dec = (float)_dec;
}

void GnomonicProjection::RADecToXY(double ra, double dec,float* x,float* y)
{
    double _x, _y;
    RADecToXY(ra, dec, &_x, &_y);
    *x = (float)_x;
    *y = (float)_y;
}

void GnomonicProjection::XYToRADec(double x, double y,double* ra,double* dec)
{
   //Aplicar la rotación con respecto al centro
   double x_obj =  (x + 1) - CenterPixX;               //Se supone que la imagen empieza en 1,1 (FALLO GORDO DE LOS CREADORES DEL WCS)
   double y_obj = CenterPixY - (y  + 1);
   double x_objr = (CD[0][0]*x_obj +CD[0][1]*y_obj);
   double y_objr = (CD[1][0]*x_obj +CD[1][1]*y_obj);
   x_objr *= DEG2RAD ;
   y_objr *= DEG2RAD;
   double  yy = y_objr ;
   double deno = cdelz-yy*sdelz;
   double d_al = atan(x_objr/deno);
   *dec = (180.0/M_PI)*atan(cos(d_al)*(sdelz +yy*cdelz) / deno ) ;
   *ra = Alphai + d_al*RAD2DEG;
   if (deno < 0.0)
   {
      *ra += 180.0;
      *dec = -*dec;
   }
   *ra *= DEG2HOR;
}


void GnomonicProjection::RADecToXY(double ra, double dec, double* x,double* y)
{
    double al = ra  * HOR2DEG;
    double del = dec;

    double dalpha =  (al- Alphai)*DEG2RAD;
    double cos_del = cos(del*DEG2RAD);
    double sin_del = sin(del*DEG2RAD);
    double sin_dalpha = sin(dalpha);
    double cos_dalpha = cos(dalpha);
    double x_tet_phi = cos_del * sin_dalpha;
    double y_tet_phi = sin_del * cdelz -  cos_del * sdelz * cos_dalpha ;

    if (dalpha > M_PI )   dalpha = -2*M_PI +dalpha ;
    if (dalpha < -M_PI )  dalpha = + 2*M_PI +dalpha ;
    if ((-sin_del * sdelz)/(cos_del * cdelz) > 1  )
    {
       //throw new Exception("Outside the projection") ;
        *x = *y = NAN;
        return;
    }
    else if (((-sin_del * sdelz)/(cos_del * cdelz) > -1 )&& (fabs(dalpha) > acos((-sin_del * sdelz)/(cos_del * cdelz)) ))
    {
       //throw new Exception("Outside the projection") ;
       *x = *y = NAN;
       return;
    }

   double den  = sin_del * sdelz + cos_del * cdelz *cos_dalpha;
   double x_stand =  x_tet_phi / den ;
   double y_stand =  y_tet_phi / den ;

   x_stand *= RAD2DEG;
   y_stand *= RAD2DEG;

   double x_obj = (ID[0][0] * x_stand + ID[0][1] * y_stand);
   double y_obj = (ID[1][0]*x_stand +ID[1][1]* y_stand);

   *x = (x_obj - 1) + CenterPixX;
   *y = CenterPixY - (y_obj + 1);
}



AstrometricReduction::AstrometricReduction()
{
    mX0 = mY0 = 0;
}

AstrometricReduction::~AstrometricReduction()
{

}


void AstrometricReduction::Equ2Std(double ra, double dec, double *x, double *y)
{
   double cos_d = cos(dec * DEG2RAD);
   double sin_d = sin(dec * DEG2RAD);
   double cos_d0 = cos(mDEC0 * DEG2RAD);
   double sin_d0 = sin(mDEC0 * DEG2RAD);
   double sin_a_a0 = sin( (ra - mRA0)* HOR2RAD );
   double cos_a_a0 = cos( (ra - mRA0)* HOR2RAD );

   *x = - (cos_d*sin_a_a0) / (cos_d0*cos_d*cos_a_a0 + sin_d*sin_d0);
   *y = - (sin_d0*cos_d*cos_a_a0-cos_d0*sin_d) / (cos_d0*cos_d*cos_a_a0 + sin_d*sin_d0);
}

void AstrometricReduction::Std2Equ(double x, double y, double *ra, double *dec)
{
    double cos_d0 = cos(mDEC0 * DEG2RAD);
    double sin_d0 = sin(mDEC0 * DEG2RAD);

    *ra = mRA0 + (atan(-x / cos_d0 - y * sin_d0) * RAD2HOR);
    *dec = asin((sin_d0 + y * cos_d0 ) / sqrt(1.0 + x*x + y*y)) * RAD2DEG;
}





















