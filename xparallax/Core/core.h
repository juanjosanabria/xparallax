#ifndef CORE_H
#define CORE_H

#include "core_global.h"
#include "core_def.h"
#include "msvc.h"
#include <QObject>
#include <QSettings>
#include <QDebug>
#include <QStringList>
#include <QUuid>
#include <QDate>
#include <QSize>
#include <QPoint>
#include <QFont>

#include "core_def.h"

class CORESHARED_EXPORT Core
{
public:
    //Métodos del cálculo del fondo
    enum BackgroundMethod {
                        BKG_KAPPA_SIGMA,        //Iterativo y computacionalmente lento, es el óptimo **Implementado};
                        BKG_MEAN_MEDIAN,        //Media-Mediana

                        BKG_AVERAGE,            //El fondo es la media de todos los píxeles (mirar docu) **Implementado
                        BKG_MEDIAN              //Selección de la mediana No recomendado
                  };



    enum BackgroundLevel {
        BKGLV_TESSEL,
        BKGLV_BICUBIC_SPLINE,
        BKGLV_BILINEAR_SPLINE
    };

    //Algoritmos para el rechazo de puntos
    enum PixRejectAlgorithm {
                        PIXREJECT_NONE,
                        PIXREJECT_SIGMA,
                        PIXREJECT_MINMAX
    };

    //Tipos de imágenes a la hora de calibrar
    enum FrameType{
                    FRAME_LIGHT,
                    FRAME_BIAS,
                    FRAME_DARK,
                    FRAME_FLAT
    };

    enum ImarithOp{
                    IMARITH_ADD,
                    IMARITH_SUBSTRACT,
                    IMARITH_DIVIDE
    };

    enum ApertureType{
                APERTURE_CONNECTED,
                APERTURE_RADIUS,
                APERTURE_FIXED
    };

    enum PixelConectivityType{
            CONNECTIVITY_FOUR,
            CONNECTIVITY_EIGHT
    };

    enum StarCenterType{
        STARCENTER_PEAK     = 1, //Coger el píxel con el máximo flujo
        STARCENTER_ISO      = 2, //Baricentro de los píxeles conectados
        STARCENTER_WINDOWED = 3, //Proceso iterativo sobre una apertura (definido por sextractor) Mejor opción casi siempre
        STARCENTER_PSF      = 4  //Ajuste psf del tipo ln(I(x,y)) + ln(I0) = c0 + c1*x + c2*y + c3*x^2 + c4*y^2
    };

public:

    ///
    /// Indica si la máqina es o no little endian
    /// \brief littleEndian
    /// \return true si la máquina es little endian
    ///
    static bool littleEndian();


    static int randInt(int low=-1,int max=-1);
    static float randFloat(float low=0,float max=1);
    static void AssertionFailure(const char *exp,const char* file, int line);
};

#ifdef QT_DEBUG
    #define ASSERT(C,MSG)  (C) ? qt_noop() : Core::AssertionFailure(#MSG, __FILE__, __LINE__)
#else
    #define ASSERT(C,MSG) qt_noop()
#endif

class CORESHARED_EXPORT Parameters
{

public:
    Parameters();
    virtual ~Parameters();

    virtual void SetDefaults() = 0;
    virtual void Save(QSettings* settings,const QString& section) = 0;
    virtual void Load(QSettings* settings,const QString& section) = 0;

    static void RemoveKey(QSettings* sett, const QString& section, const QString& name);
    static void SaveFloat(QSettings* sett,const QString& section,const QString& name, float value,int decimal_places=3);
    static void SaveDouble(QSettings* sett,const QString& section,const QString& name, double value,int decimal_places=10);
    static void SaveString(QSettings* sett,const QString& section,const QString& name, const QString&  value);
    static void SaveRA(QSettings* sett,const QString& section,const QString& name, double value);
    static void SaveDEC(QSettings* sett,const QString& section,const QString& name, double value);
    static void SaveInt(QSettings* sett,const QString& section,const QString& name, int  value);
    static void SaveBool(QSettings* sett,const QString& section,const QString& name, bool  value);
    static void SaveDate(QSettings* sett,const QString& section,const QString& name,const QDate& value);
    static void SaveGuid(QSettings* sett,const QString& section,const QString& name,const QUuid& value);
    static void SaveSize(QSettings* sett,const QString& section,const QString& name,const QSize& value);
    static void SavePoint(QSettings* sett,const QString& section,const QString& name,const QPoint& value);
    static void SaveStringList(QSettings* sett,const QString& section,const QString& name,const QStringList& value);
    static void SaveFont(QSettings* sett,const QString& section,const QString& name,const QFont& fnt);

    static int ReadInt(QSettings* sett,const QString& section,const QString& name, int def_value);
    static float ReadFloat(QSettings* sett,const QString& section,const QString& name, float def_value);
    static double ReadDouble(QSettings* sett,const QString& section,const QString& name, double def_value);
    static QString ReadString(QSettings* sett,const QString& section,const QString& name, const QString& def_value);
    static bool ReadBool(QSettings* sett,const QString& section,const QString& name, bool def_value);
    static QDate ReadDate(QSettings* sett,const QString& section,const QString& name, const QDate& def_value);
    static QUuid ReadGuid(QSettings* sett,const QString& section,const QString& name, const QUuid& def_value);
    static QSize ReadSize(QSettings* sett,const QString& section,const QString& name, const QSize& def_value);
    static QPoint ReadPoint(QSettings* sett,const QString& section,const QString& name, const QPoint& def_value);
    static QStringList ReadStringList(QSettings* sett,const QString& section,const QString& name, const QStringList& def_value);
    static double ReadRA(QSettings* sett,const QString& section,const QString& name, float def_value = INVALID_FLOAT);
    static double ReadDEC(QSettings* sett,const QString& section,const QString& name, float def_value = INVALID_FLOAT);
    static QFont ReadFont(QSettings* sett,const QString& section,const QString& name, const QFont& def_value);

private:

};


class CORESHARED_EXPORT EnviParameters :
        public Parameters
{
public:
    EnviParameters();
   ~EnviParameters();
    void SetDefaults();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);

    QUuid       uid;
    bool        show_log_window;
    bool        mainwindow_maximized;
    QSize       mainwindow_size;
    QPoint      mainwindow_pos;
    QDate       last_check_for_updates;
    QString     last_fit_open_directory;
    QStringList recent_fits;
    int         run_c;                      //Número de veces que se ha ejecutado la aplicación
    QDate       last_update_warn;           //Última vez que se avisó para la actualización
    bool        do_not_prompt_updates;      //No mostrar mensajes de actualización
    bool        caching_memory;             //Indica si se activa el cacheado de memoria de URLs
    bool        caching_disk;               //Indica si se activa el chacheado de memoria en disco para URLs

    //Contadores de números de ejecuciones
    int         ct_ast;                     //Número de veces que se ejecutó la astrometría
    int         ct_fast;                    //Número de ficheros procesados por la astrometría
    int         ct_cal;                     //Número de veces que se ejecutó la calibración de imágenes
    int         ct_fcal;                    //Número de archivos calibrados
    int         ct_bhe;                     //Número de veces que se ha ejecutado el batch header editing
    int         ct_fbhe;                    //Número de ficheros procesados por batch header editing
    int         ct_ica;                     //Número de veces ejecutado image catalog
    int         ct_fica;                    //Número de archivos catalogados
    int         ct_mpc;                     //Número de envíos al mpc
};



#endif // CORE_H


























