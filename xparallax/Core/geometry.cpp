#include <qmath.h>
#include "geometry.h"
#include "core.h"


double Geometry::VectorAngle_AH(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
{
    return VectorAngle_AH(x2-x1, y2-y1, x4-x3, y4-y3);
}

double Geometry::VectorAngle_AH(QPointF p1, QPointF p2, QPointF p3, QPointF p4)
{
    return VectorAngle_AH(p2.x()-p1.x(), p2.y()-p1.y(), p4.x()-p3.x(), p4.y()-p3.y());
}

double Geometry::VectorAngle_AH(double u1, double u2,double v1,double v2)
{
    //Comenzamos calculando el ángulo menor entre ambos vectores, pero cuidado, necesitamos devolver el ángulo antihorario
    double angle = VectorAngle(u1, u2, v1, v2);
    //Coordenada z del producto vectorial. (http://es.wikipedia.org/wiki/Producto_vectorial), si es positiva hemos girado
    //en sentido antihorario, si wz es negativa hemos girado en sentido horario por la regla del sacacorchos
    double wz = u1*v2 - u2*v1;
    return wz > 0 ? angle : 360 - angle;
}

double  Geometry::VectorAngle_AH(QPointF v1, QPointF v2)
{
    return VectorAngle_AH(v1.x(), v1.y(), v2.x(), v2.y());
}


double Geometry::VectorAngle(double u1, double u2,double v1,double v2)
{
    double num = u1*v1+u2*v2;
    double den = sqrt( u1*u1 + u2*u2 ) * sqrt(v1*v1 + v2*v2 );
    double angle =  acos(num / den) * RAD2DEG;
    return angle;
}

double Geometry::VectorAngle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
{
    //Calculamos los vectores movidos a 0, 0 y devolvemos su ángulo
    return VectorAngle(x2 - x1, y2 - y1, x4 - x3, y4 - y3);
}

double  Geometry::VectorAngle(QPointF u, QPointF v)
{
    return VectorAngle(u.x(), u.y(), v.x(), v.y());
}

double  Geometry::VectorAngle(QPointF p1, QPointF p2, QPointF p3, QPointF p4)
{
    return VectorAngle(p2.x()-p1.x(), p2.y()-p1.y(), p4.x()-p3.x(), p4.y()-p3.y());
}

double  Geometry::SegmentAngle(double x1, double y1, double x2, double y2)
{
    return VectorAngle(0, 0, 1, 0, 0, 0, x2-x1, y2-y1);
}

double  Geometry::SegmentAngle(QPointF p1, QPointF p2)
{
    return SegmentAngle(p1.x(), p1.y(), p2.x(), p2.y());
}

double  Geometry::SegmentAngle_AH(double x1, double y1, double x2, double y2)
{
    return VectorAngle_AH(x1, y1, x2, y2, x1, y1, x1+100, y1);
}

double  Geometry::SegmentAngle_AH(QPointF p1, QPointF p2)
{
    return SegmentAngle_AH(p1.x(), p1.y(), p2.x(), p2.y());
}

void  Geometry::RotateSegment(QPointF& p1, QPointF& p2, double angle)
{
    RotateSegment(p1, p2, angle, QPointF(INVALID_FLOAT, INVALID_FLOAT));
}

void  Geometry::RotateSegment(QPointF& p1, QPointF& p2, double angle, QPointF pivot)
{
    if (IS_INVALIDF(pivot.x())) pivot =  p1;
    double x1 = p1.x() - pivot.x();
    double y1 = p1.y() - pivot.y();
    double x2 = p2.x() - pivot.x();
    double y2 = p2.y() - pivot.y();
    double cosphi = cos(-angle * DEG2RAD);
    double sinphi = sin(-angle * DEG2RAD);
    p1.setX(x1 * cosphi - y1 * sinphi + pivot.x());
    p1.setY(x1 * sinphi + y1 * cosphi + pivot.y());
    p2.setX(x2 * cosphi - y2 * sinphi + pivot.x());
    p2.setY(x2 * sinphi + y2 * cosphi + pivot.y());
}


bool Geometry::PointInPolygon(QPointF point, QPointF* points, int nvert)
{
  bool c = false;
  for(int i = 0, j = nvert - 1; i < nvert; j = i++)
  {
    if( ( (points[i].y() >= point.y() ) != (points[j].y() >= point.y()) ) &&
        (point.x() <= (points[j].x() - points[i].x()) * (point.y() - points[i].y()) / (points[j].y() - points[i].y()) + points[i].x())
      )
      c = !c;
  }
  return c;
}



//Devuelve la pendiente y ordenada en el origen de una recta que pasa por dos puntos del plano
void Geometry::rectPuntoPendiente(const QPointF& pa,const QPointF& pb,double *m, double *b)
{
    if (fabs(pa.x() - pb.x()) < 0.000000001)
    {
        *m = PLUS_INFINITE;
        return;
    }
    *m = (pb.y() - pa.y()) / (pb.x() - pa.x());
    *b = pb.y() - *m*pb.x();
}

//Devuelve la ecuación canónica ax + by + c = 0 de la recta que pasa por dos puntos del plano
void Geometry::rectABC(const QPointF& pa,const QPointF& pb,double *a, double *b, double *c)
{
    double _m, _b;
    //Partiendo de la ecuación punto pendiente
    rectPuntoPendiente(pa, pb, &_m, &_b);
    *a = _m;
    *b = -1;
    *c = _b;
}

/**
 * @brief distPointRect Distancia de un punto a una recta según su ecuación canónoca ax + by + c = 0
 * @param pa Punto desde el que se desea medir la distancia
 * @param a (ax + by + c = 0)
 * @param b (ax + by + c = 0)
 * @param c (ax + by + c = 0)
 * @return
 */
double Geometry::distPointRect(const QPointF& p, double a, double b, double c)
{
    return fabs(a*p.x()+b*p.y()+c)/sqrt(POW2(a)+POW2(b));
}

/**
 * @brief Geometry::distPointRect Distancia de un punto a una recta definida por 2 puntos
 * @param p
 * @param A
 * @param B
 * @return
 */
double Geometry::distPointRect(const QPointF& p, const QPointF& A, const QPointF& B)
{
    double a, b, c;
    rectABC(A, B, &a, &b, &c);
    return distPointRect(p, a, b, c);
}

/**
 * @brief nearPointRect Devuelve el punto de la recta canónica ax + by + c = 0 que es más cercano al punto P
 * @param p Punto fuera de la recta, del que se desea buscar el más cerdano a la misma
 * @param a
 * @param b
 * @param c
 * @return
 */
QPointF Geometry::nearPointRect(const QPointF& p, double a, double b, double c)
{
    double x = (b * (b*p.x() - a*p.y())-a*c)/(POW2(a) + POW2(b));
    double y = (a * (-b*p.x() + a*p.y())-b*c)/(POW2(a) + POW2(b));
    return QPointF(x, y);
}

bool Geometry::pointInsideRectangle(const QPointF& p, QPointF* rect)
{
    double dist_AB = CART_DIST(rect[0].x(), rect[0].y(), rect[1].x(), rect[1].y());
    double dist_DA = CART_DIST(rect[3].x(), rect[3].y(), rect[0].x(), rect[0].y());
    if (distPointRect(p, rect[0], rect[1]) > dist_DA) return false;
    if (distPointRect(p, rect[2], rect[3]) > dist_DA) return false;
    if (distPointRect(p, rect[3], rect[0]) > dist_AB) return false;
    if (distPointRect(p, rect[1], rect[2]) > dist_AB) return false;
    return true;
}


