#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "core_global.h"
#include "core.h"
#include "progress.h"

#define BKG_HISTOGRAM_SIZE  256
#define BKG_SIGMA_LOWER     3
#define BKG_SIGMA_UPPER     4

typedef struct
{
    float Average;          //Media (mean ó average) de la última iteración del fondo
    float StandardDev;      //Desviación típica  de la última iteración del fondo
    float Mode;             //Moda (Valor que mas se repite)
    float Median;            //Mediana. Valor que deja a ambos lados del histograma el mismo número de valores
    float Back;             //Valor computado como de fondo
    float Skew;             //Desviación del fondo en tanto por uno

public:
    void init();


} BackgroundMapEntry;


class CORESHARED_EXPORT BackgroundParameters
        : public Parameters
{
public:
    BackgroundParameters();
   ~BackgroundParameters();

   Core::BackgroundMethod   bkg_method;       //Algoritmo para calcular el fondo. (def kappa-sima clipping)
   int                      bkg_mesh_size;    //Tamaño de la malla de fondo. (def 64)
   int                      bkg_smooth;       //Suavizado por filtro de mediana. Número de iteraciones (def 0)
   float                    bkg_upper_conv;   //Convolución superior en unidades de desviación típica (def 3.0)
   float                    bkg_lower_conv;   //Convolución inferior en unidades de desviación típica (def 2.0)
   Core::BackgroundLevel    bkg_level;         //Indica si se generará un mapa con suavizado bicúbico
   float                    bkg_stdev_stop;   //Indica cuándo se parará si la desviación típica converge menos. En tanto por 1 (def 0.08)

   void SetDefaults();
   void Save(QSettings* settings,const QString& section);
   void Load(QSettings* settings,const QString& section);

};


class CORESHARED_EXPORT BackgroundMap
    : public BackgroundWorker
{

public:
    BackgroundMap();
    ~BackgroundMap();
    void Clear();
    void Setup(Core::BackgroundMethod method, int width, int height, int mesh_size,
               int conv_iters, float sigma_lower, float sigma_upper, bool lower_conv=true);


    void Compute(float* data);
    inline BackgroundMapEntry *backentry(int x,int y)
    {
         ASSERT((x / mMeshSz) + ( y / mMeshSz ) * mPWidth < mPWidth*mPHeight,"Invalid background entry element");
         return &mMatrix[ (x / mMeshSz) + ( y / mMeshSz ) * mPWidth ];
    }

    /**
     * @brief back Devuelve un valor de fondo calculado para un píxel determinado
     * @param x Posición en x del píxel
     * @param y Posición en y del píxel
     * @return Valor de fondo estimado para ese píxel
     */
    inline float back(int x,int y)
    {
        ASSERT(x<mWidth&&y<mHeight&&x>=0&&y>=0,"Invalid background back element");
        if (!mBack) return backentry(x,y)->Back;
        else return mBack[x + y*mWidth];
    }

    /**
     * @brief stdev Devuelve un valor de la desviación típica calculada para un písel determinado
     * @param x Posición en x del píxel
     * @param y Posición en y del píxel
     * @return  Valor de desviación típica para ese píxel
     */
    float stdev(int x,int y)
    {
        ASSERT(x<mWidth&&y<mHeight&&x>=0&&y>=0,"Invalid background stdev element");
        if (!mStdev) return backentry(x,y)->StandardDev;
        else return mStdev[x + y*mWidth];
    }

    /**
     * @brief backdata devuelve la matriz del fondo calculado.
     * @return Matriz del fondo filtrado bicúbicamente. Cuidado, puede ser NULL si no se especificó que se calculara
     */
    float*  backdata(){return mBack;}


    /**
     * @brief stdevdata devuelve la matriz de desviación típica calculada
     * @return Matriz de desviación típica filtrada bicúbicamente. Hay que tener cuidado porque puede ser NULL
     */
    float*  stdevdata(){return mStdev;}

private:
    void                     initData(float* data,int width, int height,int conv_count);    //Se llamará al principio del cálculo
    void                     clearData();                                                   //Se llamará al final del cálculo

    Core::BackgroundMethod   mMethod;        //Algoritmo para el cálculo del background
    int                      mMeshSz;        //Tamaño de la malla para el cálculo
    int                      mConvIters;     //Número de iteraciones de convolución
    double                   mSigmaLower;
    double                   mSigmaUpper;

    float*                   mData;          //Datos de la imagen
    bool                     mOwnerData;     //Indica si la matriz de datos nos pertenece o es externa
    BackgroundMapEntry*      mMatrix;        //Matriz con los datos del fondo
    int                      mWidth;         //Tamaño de la imagen en píxeles
    int                      mHeight;        //Tamaño de la imagen en píxeles
    int                      mPWidth;        //Número de frames de background de ancho
    int                      mPHeight;       //Número de frames de background de alto

    float*                   mBack;          //Mapa del fondo calculado
    float*                   mStdev;         //Mapa de la desviación típica calculada

    bool                     mLowerConv;    //Indica si habrá convergencia desde abajo hacia el fondo

    void ComputeBackground(int x,int y);
    void CreateBackMap();

    inline float  pixval(int x,int y){ return (x < mWidth && y < mHeight) ? mData[x + y * mWidth] : 0;}

    static float backval(BackgroundMap *obj,int x, int y);
    static float stdevval(BackgroundMap *obj,int x, int y);

};



#endif // BACKGROUND_H













