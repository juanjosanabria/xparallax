#include "background.h"
#include "calibration.h"

#include <QString>
#include <QtConcurrent>
#include <qmath.h>

#include "ap.h"
#include "interpolation.h"
#include "xglobal.h"


/*********** PARÁMETROS DE DETECCIÓN DE FONDO POR DEFECTO ****************/
#define DEF_BKG_bkg_method          Core::BKG_KAPPA_SIGMA
#define DEF_BKG_bkg_mesh_size       128
#define DEF_BKG_bkg_smooth          1
#define DEF_BKG_bkg_upper_conv      5
#define DEF_BKG_bkg_lower_conv      4
#define DEF_BKG_bkg_level           Core::BKGLV_BICUBIC_SPLINE
#define DEF_BKG_bkg_stdev_stop      0.08f



#define          KAPPA_SIGMA_MAX_ITERS  8
#define          KAPPA_SIGMA_PRECISSION 0.005f             //1% de precisión


BackgroundParameters::BackgroundParameters()
{
    SetDefaults();
}

BackgroundParameters::~BackgroundParameters()
{
}

void BackgroundParameters::SetDefaults()
{
    bkg_method = DEF_BKG_bkg_method;
    bkg_mesh_size = DEF_BKG_bkg_mesh_size;
    bkg_smooth = DEF_BKG_bkg_smooth;
    bkg_upper_conv = DEF_BKG_bkg_upper_conv;
    bkg_lower_conv = DEF_BKG_bkg_lower_conv;
    bkg_level = DEF_BKG_bkg_level;
    bkg_stdev_stop = DEF_BKG_bkg_stdev_stop;
}

void BackgroundParameters::Save(QSettings* settings,const QString& section)
{
    SaveInt(settings, section, "bkg_method", bkg_method);
    SaveInt(settings, section, "bkg_mesh_size", bkg_mesh_size);
    SaveInt(settings, section, "bkg_smooth", bkg_smooth < 1 || bkg_smooth > 4 ? DEF_BKG_bkg_smooth : bkg_smooth);

    SaveFloat(settings, section, "bkg_upper_conv", bkg_upper_conv);
    SaveFloat(settings, section, "bkg_lower_conv", bkg_lower_conv);
    SaveFloat(settings, section, "bkg_stdev_stop", bkg_stdev_stop);

    SaveInt(settings, section, "bkg_level", (int)bkg_level);
}

void BackgroundParameters::Load(QSettings* settings,const QString& section)
{
    bkg_method = (Core::BackgroundMethod) ReadInt(settings, section, "bkg_method", (int)DEF_BKG_bkg_method);
    bkg_mesh_size = ReadInt(settings, section, "bkg_mesh_size", DEF_BKG_bkg_mesh_size);
    bkg_smooth = ReadInt(settings, section, "bkg_smooth", DEF_BKG_bkg_smooth);
    if (bkg_smooth < 1 || bkg_smooth > 4) bkg_smooth = DEF_BKG_bkg_smooth;

    bkg_upper_conv = ReadFloat(settings, section, "bkg_upper_conv", DEF_BKG_bkg_upper_conv);
    bkg_lower_conv = ReadFloat(settings, section, "bkg_lower_conv", DEF_BKG_bkg_lower_conv);
    bkg_stdev_stop = ReadFloat(settings, section, "bkg_stdev_stop", DEF_BKG_bkg_stdev_stop);

    bkg_level = (Core::BackgroundLevel) ReadInt(settings, section, "bkg_bicubic",(int) DEF_BKG_bkg_level);
}

void BackgroundMapEntry::init()
{
    this->Average = 0;
    this->StandardDev = 0;
    this->Mode = 0;
    this->Median= 0;
    this->Back = 0;
    this->Skew = 0;
}


BackgroundMap::BackgroundMap()
 : BackgroundWorker("BackgroundCalculation working")
{
    mMatrix = NULL;
    mBack = NULL;
    mStdev = NULL;
    mData = NULL;
}

BackgroundMap::~BackgroundMap()
{
   Clear();
   clearData();
}

void BackgroundMap::Clear()
{
    if (mBack) delete mBack;
    mBack = NULL;
    if (mStdev) delete mStdev;
    mStdev = NULL;
}

void BackgroundMap::Setup(Core::BackgroundMethod method,
                          int width, int height,
                          int mesh_size, int conv_iters,
                          float sigma_lower = BKG_SIGMA_LOWER, float sigma_upper = BKG_SIGMA_UPPER, bool lower_conv)
{
    Clear();
    mMethod = method;
    mMeshSz = mesh_size;
    mConvIters  = conv_iters;
    mLowerConv = lower_conv;
    mSigmaLower = sigma_lower;
    mSigmaUpper = sigma_upper;

    mWidth = width;
    mHeight = height;
    mPWidth = mWidth / mMeshSz + (mWidth % mMeshSz != 0 ? 1 : 0);
    mPHeight = mHeight / mMeshSz + (mHeight % mMeshSz != 0 ? 1 : 0);

    OnSetup(mPHeight * mPWidth * (mMeshSz/4));
}

void BackgroundMap::initData(float* data,int width, int height, int conv_count)   //Se llamará al principio del cálculo
{
    clearData();
    //Si no hay convolución (nada recomendado) se asigna el mismos
    if (mConvIters == 0)
    {
        mData = data;
        mOwnerData = false;
        return;
    }

    mOwnerData = true;
    mData = new float[width*height];
    if (mConvIters == 1)
    {
        for (int y=0;y<height;y++)
        {
            for (int x=0;x<width;x++)
            {
                if (y == 0 || y == height-1 || x == 0 || x == width-1)
                {
                    mData[y*width+x] = data[y*width+x];
                }
                else
                {
                    double val =
                            data[y*width+x] +       //Píxel en cuestión
                            data[(y-1)*width+x]+    //Arriba
                            data[(y+1)*width+x]+    //Abajo
                            data[y*width+x-1]+      //Izquierda
                            data[y*width+x+1]+      //Derecha
                            data[(y-1)*width+x-1]+  //Arriba, izquierda
                            data[(y-1)*width+x+1]+  //Arriba, derecha
                            data[(y+1)*width+x-1]+  //Abajo izquierda
                            data[(y+1)*width+x+1];  //Abajo derecha
                    mData[y*width+x] = val / 9.0;
                }
            }
        }
    }
    //Para más de 1 convolución necesitaremos más memoria ¿Qué se le va a hacer?
    else
    {
        float* aux = new float[width*height];
        memcpy(mData, data, sizeof(float)*width*height);
        for (int i=0; i<conv_count; i++)
        {
            memcpy(aux, mData, sizeof(float)*width*height);
            for (int x=0;x<width;x++)
            {
                for (int y=0;y<height;y++)
                {
                    if (y == 0 || y == height-1 || x == 0 || x == width-1)
                    {
                        mData[y*width+x] = data[y*width+x];
                    }
                    else
                    {
                        double val =
                                aux[y*width+x] +       //Píxel en cuestión
                                aux[(y-1)*width+x]+    //Arriba
                                aux[(y+1)*width+x]+    //Abajo
                                aux[y*width+x-1]+      //Izquierda
                                aux[y*width+x+1]+      //Derecha
                                aux[(y-1)*width+x-1]+  //Arriba, izquierda
                                aux[(y-1)*width+x+1]+  //Arriba, derecha
                                aux[(y+1)*width+x-1]+  //Abajo izquierda
                                aux[(y+1)*width+x+1];  //Abajo derecha
                        mData[y*width+x] = val / 9.0;
                    }
                }
            }
        }
        delete aux;
    }
}

void BackgroundMap::clearData()    //Se llamará al final del cálculo
{
    if (mData && mOwnerData) delete mData;
    mData = NULL;
}


void BackgroundMap::Compute(float* data)
{
    initData(data, mWidth, mHeight, mConvIters);

    int prog_scale = mMeshSz/4;
    OnStart();
    OnMsg(QString("Computing background mesh: %1x%2").arg(mMeshSz).arg(mMeshSz));

    //Crear las variables e inicializarlas
    if (mMatrix) delete mMatrix;
    mMatrix = new BackgroundMapEntry[mPWidth * mPHeight];
    for (int i=0; i<mPWidth*mPHeight; i++)
        mMatrix[i].init();

    //Cálculo del fondo para cada tesela de fondo
    QVector<QPoint> vec; vec.clear();
    for (int tessel_y = 0; tessel_y < mPHeight; tessel_y++)
    {
      for (int tessel_x = 0; tessel_x < mPWidth; tessel_x++)
          vec.append(QPoint(tessel_x, tessel_y));
    }
    //Optimización de paralelismo. Unas 5 veces mas rápido
    QtConcurrent::blockingMap(vec, [this](QPoint p) -> void
    {
        this->ComputeBackground(p.x(), p.y());
    });

    clearData();
    //Creamos el mapa de fondo por interpolación bicúbica
    OnMsg("Creating background map");
    CreateBackMap();
    OnProgress(mPHeight * mPWidth *  prog_scale);

    OnEnd();
}


void BackgroundMap::ComputeBackground(int tessel_x, int tessel_y)
{
    //Calcular los límites de la zona afectada
    int img_min_x = tessel_x * mMeshSz;
    int img_max_x = img_min_x + mMeshSz - 1;
    if (img_max_x >= mWidth)
    {
        img_min_x = mWidth - mMeshSz;
        if (img_min_x < 0) img_min_x = 0;
        img_max_x = mWidth - 1;
    }
    int img_min_y = tessel_y * mMeshSz;
    int img_max_y = img_min_y + mMeshSz - 1;
    if (img_max_y >= mHeight)
    {
        img_min_y = mHeight - mMeshSz;
        if (img_min_y < 0) img_min_y = 0;
        img_max_y = mHeight - 1;
    }
    //Primera estadística de la tesela afectada
    float min_thres = FLOAT_MIN;
    float max_thres = FLOAT_MAX;
    ImStat start_stat = ImStat::ComputeRect(mData, mWidth, img_min_x, img_min_y, img_max_x, img_max_y, min_thres, max_thres);

    //Para los valores calculados
    double back = start_stat.mean();
    double skew = 0;

    ImStat ant_stat = start_stat;
    ImStat cur_stat = start_stat;
    for (int i=0; i<KAPPA_SIGMA_MAX_ITERS; i++)
    {
        max_thres = ant_stat.mean() + mSigmaLower * ant_stat.stdev();
        if (mLowerConv) min_thres = ant_stat.mean() - mSigmaUpper * ant_stat.stdev();
        cur_stat = ImStat::ComputeRect(mData, mWidth, img_min_x, img_min_y, img_max_x, img_max_y, min_thres, max_thres);
        //Si no hay desviación típica (es cero) cuidado, salimos puesto que el fondo es la moda
        if (cur_stat.stdev() == 0)
        {
            back = cur_stat.mode();
            break;
        }
        double xmode = 2.5 * cur_stat.median() - 1.5 * cur_stat.mean();
        skew = ( cur_stat.mode() - xmode ) / xmode;

        //Si la desviación típica cambia menos de un 8% consideramos que hay pocas estrellas
        //y no afectan significativamente al cálculo, el fondo será la media
        double dif_stdev = fabs(ant_stat.stdev()-cur_stat.stdev()) / cur_stat.stdev();
        if (dif_stdev < 0.08)
        {
            back = cur_stat.mean();
            break;
        }
        ant_stat = cur_stat;
    }

    int idx = tessel_x + tessel_y * mPWidth;
    mMatrix[idx].Average = cur_stat.mean();
    mMatrix[idx].StandardDev = cur_stat.stdev();
    mMatrix[idx].Median = cur_stat.median();
    mMatrix[idx].Mode = cur_stat.mode();
    mMatrix[idx].Back = back;
    mMatrix[idx].Skew = skew;
}


void BackgroundMap::CreateBackMap()
{
       return;
    if (mBack) delete mBack;
    mBack = new float[mWidth * mHeight];


   //Creamos la matriz de destino
   alglib_impl::ae_matrix dst_matrix;
   alglib_impl::ae_state state;
   alglib_impl::ae_state_init(&state);
   alglib_impl::ae_matrix_init(&dst_matrix, mHeight, mWidth, alglib_impl::DT_REAL, &state, false);

   //Creamos la matri de origen
   alglib_impl::ae_matrix src_matrix;
   alglib_impl:: ae_state_clear(&state);
   alglib_impl::ae_matrix_init(&src_matrix, mPHeight, mPWidth, alglib_impl::DT_REAL, &state, false);

   //Copiamos e interpolamos el background
   for (int y=0; y< mPHeight; y++)
       for (int x=0; x<mPWidth; x++)
           src_matrix.ptr.pp_double[y][x] =  this->mMatrix[x + y * mPWidth].Back;
   alglib_impl::ae_state_clear(&state);
   spline2dresamplebicubic(&src_matrix, mPHeight, mPWidth, &dst_matrix, mHeight, mWidth, &state);

   //Copiamos la matriz generada
   for (int y=0; y< mHeight; y++)
       for (int x=0; x<mWidth; x++)
            mBack[x + y * mWidth] = dst_matrix.ptr.pp_double[y][x];

   return;
   //Lo mismo para la desviación típica
   if (mStdev) delete mStdev;
   mStdev = new float[mWidth * mHeight];

   for (int y=0; y< mPHeight; y++)
       for (int x=0; x<mPWidth; x++)
           src_matrix.ptr.pp_double[y][x] =  this->mMatrix[x + y * mPWidth].StandardDev;
   alglib_impl::ae_state_clear(&state);
   alglib_impl::spline2dresamplebicubic(&src_matrix, mPHeight, mPWidth, &dst_matrix, mHeight, mWidth, &state);
   for (int y=0; y< mHeight; y++)
       for (int x=0; x<mWidth; x++)
            mStdev[x + y * mWidth] = dst_matrix.ptr.pp_double[y][x];


   alglib_impl::ae_matrix_clear(&src_matrix);
   alglib_impl::ae_matrix_clear(&dst_matrix);
}


float BackgroundMap::backval(BackgroundMap *obj,int x, int y)
{
    if (x<0) x = 0; else if (x>=obj->mPWidth) x = obj->mPWidth-1;
    if (y<0) y = 0; else if (y>=obj->mPHeight) y = obj->mPHeight-1;
    return obj->mMatrix[x + y * obj->mPWidth].Back;
}

float BackgroundMap::stdevval(BackgroundMap *obj,int x, int y)
{
    if (x<0) x = 0; else if (x>=obj->mPWidth) x = obj->mPWidth-1;
    if (y<0) y = 0; else if (y>=obj->mPHeight) y = obj->mPHeight-1;
    return obj->mMatrix[x + y * obj->mPWidth].StandardDev;
}


