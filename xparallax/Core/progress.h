#ifndef PROGRESSMANAGER_H
#define PROGRESSMANAGER_H

#include "core_global.h"
#include "fitimage.h"
#include "core.h"
#include <QVector>
#include <QString>
#include <QMutex>
#include <QObject>
#include <QDateTime>
#include <QStringList>


class ProgressManager;
class ProgressElement;

struct CORESHARED_EXPORT WorkFileResult {
    int     retVal;      //Valor de retorno
    QString retMsg;      //Mensaje de retorno
    WorkFileResult(int rv, QString msg){retVal=rv;retMsg=msg;}
    WorkFileResult(){retVal = 0;}
};

class CORESHARED_EXPORT WorkFileList : public Parameters{


public:
    WorkFileList();
    ~WorkFileList();
    void Clear();
    QStringList*            filenames(){return &mFilenames;}
    QList<FitImage*>*       fitimages(){return &mFitImages;}
    QList<WorkFileResult>*  results(){return &mResult;}
    bool                    overwrite(){return mOverwrite;}
    QString                 outputDir(){return mOutputDir;}
    QString                 lastSearchPath(){return mLastSearchPath;}

    void SetDefaults();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);

    void setOutputDir(const QString& dir){mOutputDir = dir;}
    void setOverwrite(bool ow){mOverwrite = ow;}

    //Gestión del resultado de procesado de la lista de archivos
    void clearResults(){mResult.clear();}
    void addResult(int ret, QString res=""){mResult.append(WorkFileResult(ret,res));}

private:
    QStringList           mFilenames;
    QList<FitImage*>      mFitImages;
    bool                  mOverwrite;
    QString               mOutputDir;
    QString               mLastSearchPath;
    //Errores y valores de retorno para cada archivo
    QList<WorkFileResult> mResult;
};


class CORESHARED_EXPORT ProgressManager
{
public:
    ProgressManager();
    virtual ~ProgressManager();
    ProgressElement* AddProgres(const QString &name,int value);
    ProgressElement* AddNullProgress();                                //Agrega un elemento inactivo, que no tendrá efecto pero al que se puede llarmar y usar
    int position(){return mPosition;}                                  //Devuelve la posición absoluta
    int progress(){return mPosition*100/mValue;}                       //Devuelve el progreso actual en %
    int remain();                                                      //Estima en segundos el tiempo que queda para acabar
    void cancel();
    bool cancelled(){return false;}
    void UpdatePosition(ProgressElement *source);
    void UpdateStatus(ProgressElement *source);


protected:
    virtual void OnStart() = 0;
    virtual void OnEnd() = 0;
    virtual void OnPositionChanged(int old_position, int new_position) = 0;
    virtual void OnProgressChanged(int ol_dprogress,int new_progress) = 0;
    virtual void OnStatusChanged(const QString &status) = 0;
    virtual void OnElementChanged(ProgressElement* affected) = 0;
    virtual void OnRemainTimeChanged(int oldremain,int newremain) = 0;

private:
    ProgressElement*          mCurProgElement;
    ProgressElement*          mNullElement;
    QVector<ProgressElement*> mElements;
    QMutex                    mMutex;
    int                       mPosition;
    int                       mValue;
    bool                      mCancelled;
    bool                      mEnd;
    QDateTime                 mStartDate;
    int                       mLastRemainSecs;

};


class CORESHARED_EXPORT ProgressElement
{

public:
    ProgressElement(ProgressManager* parent,const QString &name,int value);
    ~ProgressElement();

    inline QString  name(){return mName;}
    inline QString  status(){return mStatus;}
    inline int      value(){return mValue; }
    inline int      position(){ return mPosition; }
    inline bool     done(){return mDone;}
    inline bool     cancelled(){return mCancelled;}
    inline void     cancel(){mCancelled = true;}

    void SetStatus(const QString &status);
    void SetPosition(int p);

private:
    ProgressManager*    mParent;
    QString             mName;
    QString             mStatus;
    int                 mValue;
    int                 mPosition;
    bool                mDone;
    bool                mCancelled;

};

class CORESHARED_EXPORT VoidProgressManager
        : public ProgressManager
{
public:
    VoidProgressManager(){;}
    ~VoidProgressManager(){;}

protected:
    void OnStart(){;}
    void OnEnd(){;}
    void OnPositionChanged(int, int){;}
    void OnProgressChanged(int,int){;}
    void OnStatusChanged(const QString &){;}
    void OnElementChanged(ProgressElement*){;}
    void OnRemainTimeChanged(int,int ){;}

};

class  CORESHARED_EXPORT BackgroundWorker
        : public QObject
{
    Q_OBJECT

public:
     BackgroundWorker(const QString& name);
     virtual ~BackgroundWorker();
     inline QString& name(){return mName;}
     inline void setName(QString nm){mName = nm;}
     inline int size(){return mSize;}
     inline int position(){return mPosition;}
     inline bool ended(){return mEnded;}
     inline bool cancelled(){return mCancelled;}
     inline void setLog(bool log){mLogMsgs = log;}
     inline bool logMsgs(){return mLogMsgs;}

    //Las clases hijas no accederán a las señales directamente sino a estas funciones
     void OnStart();
     void OnSetup(int size);
     void OnEnd();
     void OnError(QString desc);
     void OnMsg(QString msg);
     void OnProgress(int position);
     void OnTitleChange(QString title);
     void OnMainTitleChange(QString title);

public slots:
     virtual void cancel();

protected:
     QString    mName;
     int        mSize;
     int        mLastPosition;
     int        mPosition;
     bool       mEnded;
     bool       mCancelled;
     bool       mStarted;
     bool       mLogMsgs;

signals:
     void Start_Signal(BackgroundWorker*wk);
     void End_Signal(BackgroundWorker *wk);
     void Error_Signal(BackgroundWorker *wk, QString desc);
     void Setup_Signal(BackgroundWorker* wk, int size);
     void Msg_Signal(BackgroundWorker* wk, QString msg);
     void MainTitleChange_Signal(BackgroundWorker* wk, QString msg);
     void TitleChange_Signal(BackgroundWorker* wk, QString msg);
     void Progress_Signal(BackgroundWorker* wk,int position);
     void Cancel_Signal(BackgroundWorker* wk);

};


#endif // PROGRESSMANAGER_H
