#include "core_global.h"
#include "fitimage.h"
#include "core.h"
#include "util.h"
#include "calibration.h"
#include "plateconstants.h"
#include "xglobal.h"
#include "astrometry.h"
#include <qmath.h>
#include <stdio.h>
#include <QFile>
#include <QBitArray>
#include <QDataStream>
#include <QImage>
#include <QFileInfo>
#include <QRgb>
#include <QColor>
#include <QStringList>

//Constantes internas del módulo de imágenes FIT
#define     FIT_HISTO_SIZE      16384 //Histograma inicial de alta resolución

//Orden de las cabeceras principales
#define NUM_HEADERS_ORDER   9
const char * FitHeaderList::S_HEADER_ORDER[NUM_HEADERS_ORDER+1] = {
    "SIMPLE", "BITPIX", "NAXIS","NAXIS1","NAXIS2","EXTEND","BZERO","BSCALE", "PROGRAM",NULL
};

//Cabeceras principales que no son modificables por el usuario
#define NUM_MAIN_HEADERS 8
const char* FitHeaderList::S_HEADERS_MAIN[NUM_MAIN_HEADERS+1] = {
    "SIMPLE", "BITPIX", "NAXIS", "NAXIS1", "NAXIS2", "EXTEND", "BZERO", "BSCALE", NULL
};

//Cabeceras que hay que borrar, no las queremos en el fit
#define NUM_HEADERS_DELETE  1
const char* FitHeaderList::S_HEADERS_DELETE[NUM_HEADERS_DELETE+1] = {
    "PROGRAM", NULL
};

//Cabeceras de astrometría de XParallax
const char* FitHeaderList::S_HEADERS_XASTROMETRY[] = {
    "LSPC-A", "LSPC-B" , "LSPC-C", "LSPC-D", "LSPC-E", "LSPC-F", "LSPC-RA", "LSPC-DEC", NULL
};

//Cabeceras de astrometría a eliminar si hiciéramos astrometría
const char* FitHeaderList::S_HEADERS_ASTROMETRY[] ={
    "EQUINOX",
    "A_ORDER", "B_ORDER", "AP_ORDER", "BP_ORDER",
    "CTYPE1", "CTYPE2","CRPIX1", "CRPIX2",
    "CUNIT1", "CUNIT2","CRVAL1", "CRVAL2",
    "CROTA1", "CROTA2","CDELT1","CDELT2",
    "CD1_1", "CD1_2", "CD2_1", "CD2_2",
    "A_0_0", "A_0_1", "A_0_2", "A_0_3",
    "A_1_0", "A_1_1", "A_1_2", "A_1_3",
    "A_2_0", "A_2_1", "A_2_2", "A_2_3",
    "A_3_0", "A_3_1", "A_3_2", "A_3_3",
    "B_0_0", "B_0_1", "B_0_2", "B_0_3",
    "B_1_0", "B_1_1", "B_1_2", "B_1_3",
    "B_2_0", "B_2_1", "B_2_2", "B_2_3",
    "B_3_0", "B_3_1", "B_3_2", "B_3_3",
    "AP_0_0", "AP_0_1", "AP_0_2", "AP_0_3",
    "AP_1_0", "AP_1_1", "AP_1_2", "AP_1_3",
    "AP_2_0", "AP_2_1", "AP_2_2", "AP_2_3",
    "AP_3_0", "AP_3_1", "AP_3_2", "AP_3_3",
    "BP_0_0", "BP_0_1", "BP_0_2", "BP_0_3",
    "BP_1_0", "BP_1_1", "BP_1_2", "BP_1_3",
    "BP_2_0", "BP_2_1", "BP_2_2", "BP_2_3",
    "BP_3_0", "BP_3_1", "BP_3_2", "BP_3_3",
    NULL
};

const char* FitHeaderList::S_HEADERS_ASTROMETRY_EXTRA[] = {
    "WCSAXIS", "EQUINOX", "LONPOLE", "LATPOLE", "WCSAXES", "IMAGEW", "IMAGEH",
    NULL
};

const char* FitHeaderList::S_HEADERS_DATEFRAME[] = {
  "UTDATE",  "UT", "UT-DATE", "UT_DATE",
  "UTSTART", "UT_START", "UT-START", "DATE-STA","DATE_STA"
  "UTMID",   "UT_MID",   "UT-MID",   "DATE-MID",  "DATE_MID",
  "UTEND",   "UT_END",   "UT-END",   "DATE-END",  "DATE_END",
  "DATE_LOC","DATE-LOC", "DATE",
  "DATE-OBS","DATE_OBS",
  NULL
};

const char* FitHeaderList::S_HEADERS_STANDARD[] = {
    "AUTHOR", "BITPIX", "BLANK", "DATE-OBS", "EXPOSURE", "INSTRUME", "OBJECT"
    "OBSERVER", "HISTORY", "PROGRAM", "SET-TEMP", "TELESCOP",
    NULL
};

const char* FitHeaderList::S_HEADERS_COMMENTS[] = {
    /** Cabeceras estándar */
    "AUTHOR",   "String identifying who compiled the information in the data associated with this file",
    "BITPIX",   "Number of bits per data pixel. Possitive values (8, 16, 32) or negative (-32) for 32 bit floating point IEEE",
    "BLANK",    "Columns 1-8 contain the string, 'BLANK   ' (ASCII blanks in columns 6-8).  The value field shall contain an integer that specifies the representation of array values whose physical values are undefined",
    "DATE-OBS", "Observation date in format YYYY-MM-DDThh:mm:ss",
    "EXPOSURE", "Exposure time in seconds",
    "INSTRUME", "Camera Information",
    "OBJECT",   "Name for the object observed",
    "OBSERVER", "Observer who acquired the data",
    "ORIGIN",   "Organization responsible for the data",
    "HISTORY",  "Indicates the processing history of the image. This keyword may be repeated as many times as necessary",
    "PROGRAM",  "Software used to process this image",
    "SET-TEMP", "CCD temperature setpoint",
    "TELESCOP", "Telescope information",

    /** Cabeceras no estándar */
    "EQUINOX",  "Equinox of coordinates",
    "APTDIA",   "Diameter of the telescope in millimeters",
    "APTAREA",  "Aperture area of the telescope in square millimeters. This value includes the effect of the central obstruction",
    "CBLACK",   "Indicates the black point used when displaying the image (screen stretch)",
    "CCD-TEMP", "Actual measured sensor temperature at the start of exposure in degrees C",
    "CWHITE",   "Indicates the white point used when displaying the image (screen stretch)",
    "DATAMAX",  "Pixel values above this level are considered saturated",
    "EXPTIME",  "Duration of exposure in seconds",
    "DARKTIME", "Dark current integration time, if recorded. May be longer than exposure time",
    "EGAIN",    "Electronic gain in photoelectrons per ADU",
    "FILTER",   "Name of selected filter, if filter wheel is connected",
    "FLIPSTAT", "Status of pier flip for German Equatorial mounts",
    "FOCALLEN", "Focal length of the telescope in millimeters",
    "FOCUSPOS", "Focuser position in steps, if focuser is connected",
    "FOCUSSZ",  "Focuser step size in microns, if available",
    "FOCUSTEM", "Focuser temperature readout in degrees C, if available",
    "IMAGETYP", "Type of image: Light Frame, Bias Frame, Dark Frame, Flat Frame, or Tricolor Image",
    "ISOSPEED", "ISO camera setting, if camera uses ISO speeds",
    "JD_GEO",   "Records the geocentric Julian Day of the start of exposure",
    "JD_HELIO", "Records the Heliocentric Julian Date at the exposure midpoint",
    "NOTES",    "User-entered information free-form notes",
    "OBJECT",   "Name or designation of object being imaged",
    "OBJCTALT", "Nominal altitude of center of image",
    "OBJCTAZ",  "Nominal azimuth of center of image",
    "OBJCTDEC", "Declination of object being imaged, string format DD MM SS, if available. Note: this is an approximate field center value only",
    "OBJCTHA",  "Nominal hour angle of center of image",
    "OBJCTRA",  "Right Ascension of object being imaged, string format HH MM SS, if available. Note: this is an approximate field center value only",
    "OBSERVAT", "Name of the observatory",
    "PEDESTAL", "Add this value to each pixel value to get a zero-based ADU",
    "PIERSIDE", "Indicates side-of-pier status when connected to a German Equatorial mount",
    "READOUTM", "Records the selected Readout Mode (if any) for the camera",
    "ROTATANG", "Rotator angle in degrees, if focal plane rotator is connected",
    "SBSTDVER", "String indicating the version of the SBIG FITS extensions supported",
    "SET-TEMP", "CCD temperature setpoint in degrees C",
    "SITELAT",  "Latitude of the imaging site in degrees, if available. Uses the same format as OBJECTDEC",
    "SITELONG", "Longitude of the imaging site in degrees, if available. Uses the same format as OBJECTDEC",
    "SWCREATE", "String indicating the software used to create the file",
    "SWMODIFY", "String indicating the software that modified the file. May be multiple copies",
    "TRAKTIME", "Exposure time of the autoguider used during imaging",
    "UT",       "Universal time YYYY-MM-DDThh:mm:ss",
    "UT-START", "Universal start time of the exposure. YYYY-MM-DDThh:mm:ss",
    "UT_START", "Universal start time of the exposure. YYYY-MM-DDThh:mm:ss",
    "UTSTART",  "Universal start time of the exposure. YYYY-MM-DDThh:mm:ss",
    "UT-END",   "Universal end time of the exposure. YYYY-MM-DDThh:mm:ss",
    "UT_END",   "Universal end time of the exposure. YYYY-MM-DDThh:mm:ss",
    "UTEND",    "Universal end time of the exposure. YYYY-MM-DDThh:mm:ss",
    "XBINNING", "Binning factor used on X axis",
    "XORGSUBF", "Subframe origin on X axis",
    "XPIXSZ",   "Physical X dimension of the sensor's pixels in microns",
    "YBINNING", "Binning factor used on Y axis",
    "YORGSUBF", "Subframe origin on Y axis",
    "YPIXSZ",   "Physical Y dimension of the sensor's pixels in microns",

    /** Cabeceras propias */
    "LSPC-A",   "Least square plate constant A computed by " XPROGRAM "",
    "LSPC-B",   "Least square plate constant A computed by " XPROGRAM "",
    "LSPC-C",   "Least square plate constant A computed by " XPROGRAM "",
    "LSPC-D",   "Least square plate constant A computed by " XPROGRAM "",
    "LSPC-E",   "Least square plate constant A computed by " XPROGRAM "",
    "LSPC-F",   "Least square plate constant A computed by " XPROGRAM "",
    "LSPC-RA",  "Right Ascension of the image center used to provide an astrometic solution (in hours)",
    "LSPC-DEC", "Declination of the image center used to provide an astrometic solution (in degrees)",
    NULL
};


const FitHeader FitHeaderList::S_EMPTY_HEADER = FitHeader("","","");
int   FitHeader::S_UNIQID = 1;

FitHeader::FitHeader()
{
     mName = "";
     mValue = "";
     mComment = "";
     mIsDotted = false;
     mIsHierarch = false;
     mUniqueID = S_UNIQID++;
}

FitHeader::FitHeader(const FitHeader &hdr)
    : FitHeader()
{
    mName = hdr.mName;
    mValue = hdr.mValue;
    mComment = hdr.mComment;
    mIsDotted = hdr.mIsDotted;
    mIsHierarch = hdr.mIsHierarch;
    mUniqueID = hdr.mUniqueID;
}

FitHeader::FitHeader(const QString &name)
    : FitHeader()
{
    mName = name.toUpper();
}

FitHeader::FitHeader(const QString &name,const QString &value)
    : FitHeader()
{
    QString val = value.trimmed();
    mName = name.toUpper();
    mIsDotted = val.startsWith("'") && val.endsWith("'");
    mValue = (!mIsDotted) ? val : val.mid(1,val.length()-2);
}

FitHeader::FitHeader(const QString &name,const QString &value,const QString &comment)
    : FitHeader()
{
    QString val = value.trimmed();
    mName = name.toUpper();
    mComment = comment;
    mIsDotted = val.startsWith("'") && val.endsWith("'");
    mValue = (!mIsDotted) ? val : val.mid(1,val.length()-2);
}

FitHeader::FitHeader(const QString &name, int value)
    : FitHeader()
{
    mName = name.toUpper();
    mValue = mValue.sprintf("%d",value);
}

FitHeader::FitHeader(const QString &name, int value,const QString &comment)
    : FitHeader()
{
    mName = name.toUpper();
    mValue = mValue.sprintf("%d",value);
    mComment = comment;
}

FitHeader::FitHeader(const QString &name, float value)
    : FitHeader()
{
    char buffer[81];
    sprintf(buffer,FIT_DOUBLE_FORMAT,value);
    mName = name.toUpper();
    mValue = buffer;
}

FitHeader::FitHeader(const QString &name, float value, const QString &comment)
    : FitHeader()
{
    char buffer[81];
    sprintf(buffer,FIT_DOUBLE_FORMAT,value);
    mName = name.toUpper();
    mValue = buffer;
    mComment = comment;
}

FitHeader::FitHeader(const QString &name, double value)
    : FitHeader()
{
    char buffer[81];
    sprintf(buffer,FIT_DOUBLE_FORMAT,value);
    mName = name.toUpper();
    mValue = buffer;
}

FitHeader::FitHeader(const QString &name, double value,const QString &comment)
    : FitHeader()
{
    char buffer[81];
    sprintf(buffer,FIT_DOUBLE_FORMAT,value);
    mName = name.toUpper();
    mValue = buffer;
    mComment = comment;
}

void FitHeader::toString(char *buffer)
{
    if (isInvalid())
    {
        memset(buffer,' ', 32);
        return;
    }
    QByteArray   b_value  = mValue.toLatin1();
    QByteArray   b_name  = mName.toLatin1();
    QByteArray   b_comment  = mComment.toLatin1();
    const char *c_value = b_value.data();
    const char *c_name = b_name.data();
    const char *c_comment = b_comment.data();

    if (isEmpty())
    {
        //No hacer nada
        *buffer = 0;
    }
    //Cabeceras con nombre que no tienen signo igual
    if (isHistory() || isComment())
    {
        if (mComment.length())
        {
            if (mIsDotted) snprintf(buffer,80,"%-8s '%s' / %s",c_name, c_value, c_comment);
            else snprintf(buffer,80,"%-8s %s / %s",c_name, c_value, c_comment);
        }
        else
        {
            if (mIsDotted) snprintf(buffer,80,"%-8s '%s'",c_name, c_value);
            else snprintf(buffer,80,"%-8s %s",c_name, c_value);
        }
    }
    //Cabecera de datos con signo igual. Especiales Hierarch
    else if (mIsHierarch)
    {
        if (mComment.length())
        {
            if (mIsDotted) snprintf(buffer,80,"HIERARCH %s = '%s' / %s",c_name, c_value, c_comment);
            else snprintf(buffer,80,"HIERARCH %s = %s / %s",c_name, c_value, c_comment);
        }
        else
        {
            if (mIsDotted) snprintf(buffer,80,"HIERARCH %s = '%s'",c_name, c_value);
            else snprintf(buffer,80,"HIERARCH %s = %s",c_name, c_value);
        }
    }
    //Cabecera de datos con signo igual
    else
    {
        if (mComment.length())
        {
            if (mIsDotted) snprintf(buffer,80,"%-8s= '%s' / %s",c_name, c_value, c_comment);
            else snprintf(buffer,80,"%-8s= %s / %s",c_name, c_value, c_comment);
        }
        else
        {
            if (mIsDotted) snprintf(buffer,80,"%-8s= '%s'",c_name, c_value);
            else snprintf(buffer,80,"%-8s= %s",c_name, c_value);
        }
    }
    int len = (int) strlen(buffer);
    if (len < 80)
        memset(buffer+len,(int)' ',80-len);
    buffer[80]=0;
}



FitHeader::~FitHeader()
{

}


const FitHeader FitHeader::Parse(QString txt)
{
    FitHeader h("","","");
    QString name;
    QString value;
    QString comment;
    h.mIsDotted = false;

    //Es un comentario
    if (txt.startsWith("COMMENT "))
    {
        name = "COMMENT";
        value = txt.mid(7);
    }
    //Es una cabecera final
    else if (txt.startsWith("END"))
    {
        name = "END";
    }
    //Cabecera vacía
    else if (txt.startsWith("                                                                               "))
    {
        name = "--------";
    }
    //Es otro tipo de cabecera
    else
    {
        if (txt.startsWith("HIERARCH "))
        {
            h.mIsHierarch = true;
            txt = txt.right(txt.length()-9);
        }
        int idx = txt.startsWith("HISTORY ") ? txt.indexOf(" ") : txt.indexOf("=");
        if (idx > 0)
        {   name = txt.mid(0,idx);
            int idx_com1 = txt.indexOf("'");
            int idx_com2 = idx_com1>0?txt.indexOf("'",idx_com1+1):0;
            int idxcmd = txt.indexOf("/");

            //El valor estrá entrecomillado entre comillas simples
            if (idx_com1 > 0 && idx_com2 > 0 && idx_com2 > idx_com1)
            {
                h.mIsDotted = true;
                value = txt.mid(idx_com1+1, idx_com2 - idx_com1 - 1);
                idxcmd = txt.indexOf("/", idx_com2+1);
            }
            //Contiene comentario
            if (idxcmd > 0)
            {
                if (!value.length()) value = txt.mid(idx+1,idxcmd-idx-1);
                comment = txt.mid(idxcmd+1);
            }
            //No contiene comentario
            else
            {
                value = txt.mid(idx+1);
            }
        }
    }
    name = name.trimmed();
    value = value.trimmed();

    //Mucho cuidado, porque el valor puede venir entrecomillado
    if (value.startsWith('\'') && value.endsWith('\''))
    {
        value = value.mid(1, value.length()-2);
        h.mIsDotted = true;
        value = value.trimmed();
    }

    comment = comment.trimmed();

    h.setName(name);
    h.setValue(value);
    h.setComment(comment);
    return h;
}

void FitHeader::setName(const QString &name)
{
    mName = name;
}

void FitHeader::setValue(const QString &value)
{
    mValue = value;
}

void FitHeader::setComment(const QString &comment)
{
    mComment = comment;
}

bool FitHeader::operator== ( const FitHeader & b )
{
    if (!isData() && !const_cast<FitHeader&>(b).isData())
        return uniqID() == const_cast<FitHeader&>(b).uniqID();
    else
        return mName == b.mName;
}

bool FitHeader::operator!=(const FitHeader & b)
{
    return mName != b.mName;
}

bool FitHeader::operator>(const FitHeader & b)
{
    return mName > b.mName;
}

bool FitHeader::operator<(const FitHeader & b)
{
    return mName < b.mName;
}

bool FitHeader::operator>=(const FitHeader & b)
{
    return mName >= b.mName;
}

bool FitHeader::operator<=(const FitHeader & b)
{
    return mName <= b.mName;
}


FitHeaderList::FitHeaderList()
{
    mWidth = 0;
    mHeight = 0;
    mBitPix = 0;
    mBzero = 0;
    mBscale = 1;
    mExpTime = 0;
    mDirty = false;
    mList = QList<FitHeader>();
}

void FitHeaderList::ReplaceHeader(int idx,const FitHeader &hdr)
{
    if (idx < mList.count())
    {
        mList.replace(idx, hdr);
        mDirty = true;
    }
}

void FitHeaderList::RemoveAt(int idx)
{
    ASSERT(idx >= 0 && idx < mList.count(),"Error in  FitHeaderList::RemoveAt, array index out of bounds");
    mList.removeAt(idx);
    mDirty = true;
}

void FitHeaderList::Insert(int idx, const FitHeader &hdr)
{
    ASSERT(idx >= 0 && idx <= mList.count(),"Error in  FitHeaderList::Insert, array index out of bounds");
    mList.insert(idx, hdr);
    mDirty = true;
}

void FitHeaderList::UpdateHeader(const FitHeader &hdr)
{   
    //Buscamos la cabecera que tenga ese ID
    for (int i=0;i<mList.count(); i++)
    {
        if (const_cast<FitHeader&>(mList.at(i)).uniqID() == const_cast<FitHeader&>(hdr).uniqID())
        {
            mList.replace(i, hdr);
            return;
        }
    }
    //Si es una cabecera de datos y no hemos podido añadirla la añadimos
    if (const_cast<FitHeader&>(hdr).isData())
    {
        AddHeader(hdr);
        return;
    }
}

void FitHeaderList::Sort(int (*lessThan)(FitHeader a, FitHeader b))
{
    qSort(mList.begin(), mList.end(), lessThan);
}

void FitHeaderList::Switch(int idx1, int idx2)
{
    FitHeader aux = mList.at(idx2);
    mList[idx2] = mList[idx1];
    mList[idx1] = aux;
}

int FitHeaderList::invalidCount()
{
    int cnt = 0;
    for (int i=0;i<mList.count(); i++)
        if (const_cast<FitHeader&>(mList.at(i)).isInvalid()) cnt++;
    return cnt;
}

int FitHeaderList::dataCount()
{
    int cnt = 0;
    for (int i=0;i<mList.count(); i++)
        if (const_cast<FitHeader&>(mList.at(i)).isData()) cnt++;
    return cnt;
}

int FitHeaderList::commentCount()
{
    int cnt = 0;
    for (int i=0;i<mList.count(); i++)
        if (const_cast<FitHeader&>(mList.at(i)).isComment()) cnt++;
    return cnt;
}

int FitHeaderList::historyCount()
{
    int cnt = 0;
    for (int i=0;i<mList.count(); i++)
        if (const_cast<FitHeader&>(mList.at(i)).isHistory()) cnt++;
    return cnt;
}

int FitHeaderList::emptyCount()
{
    int cnt = 0;
    for (int i=0;i<mList.count(); i++)
        if (const_cast<FitHeader&>(mList.at(i)).isEmpty()) cnt++;
    return cnt;
}

int FitHeaderList::validCount()
{
  return mList.count() - invalidCount();
}

FitHeaderList::ImageType FitHeaderList::getImageType()
{
    ImageType it = Unknown;
    FitHeader fh = Contains("IMAGETYP");
    if (!fh.isInvalid())
    {
        if (fh.value().toLower() == "bias") it = Bias;
        else if (fh.value().toLower() == "dark") it = Dark;
        else if (fh.value().toLower() == "object" || fh.value().toLower() == "light") it = Object;
        else if (fh.value().toLower() == "flat") it = Flat;
    }
    //Si todavía no sabemos el tipo de imagen recorremos los comentarios
    for (int i=0; i<mList.count() && it == Unknown; i++)
    {
        if (mList.at(i).isComment())
        {
            if (mList[i].value().toLower() == "bias") it = Bias;
            else if (mList[i].value().toLower() == "dark") it = Dark;
            else if (mList[i].value().toLower() == "object" || mList[i].value().toLower() == "light") it = Object;
            else if (mList[i].value().toLower() == "flat") it = Flat;
        }
    }
    return it;
}

int FitHeaderList::AddHeader(FitHeader hdr)
{
    mList.removeOne(hdr);

    if (!hdr.comment().length())
    {
        //Añadimos comentarios estándar a las cabeceras estándar que no tienen comentarios
        if (hdr.name() == "SIMPLE") hdr.setComment(SIMPLE_COMMENT);
        else if (hdr.name() == "BITPIX") hdr.setComment(BITPIX_COMMENT);
        else if (hdr.name() == "NAXIS") hdr.setComment(NAXIS_COMMENT);
        else if (hdr.name() == "NAXIS1") hdr.setComment(NAXIS1_COMMENT);
        else if (hdr.name() == "NAXIS2") hdr.setComment(NAXIS2_COMMENT);
        else if (hdr.name() == "EXTEND") hdr.setComment(EXTEND_COMMENT);
        else if (hdr.name() == "BZERO") hdr.setComment(BZERO_COMMENT);
        else if (hdr.name() == "BSCALE") hdr.setComment(BSCALE_COMMENT);

        //Cabeceras no estándar
    }

    //Si es SIMPLE insertamos la primera siempre
    if (hdr.name() == "SIMPLE")
    {
        if (!mList.count()) mList.append(hdr);
        else mList.insert(mList.begin(),hdr);
    }
    else
    {
        mList.append(hdr);
    }
    if (BadHeaderOrder())
        ReorderHeaders();
    mDirty = true;
    UpdateValues();
    return hdr.uniqID();
}

int FitHeaderList::RemoveMatches(QRegularExpression name, QRegularExpression value)
{
    int cont = 0;
    for (int i=0; i<mList.count(); i++)
    {
        if ( name.match(mList[i].name()).hasMatch() && value.match(mList[i].value()).hasMatch() )
        {
            mList.removeAt(i);
            cont++;
            i--;
        }
    }
    return cont;;
}

void FitHeaderList::RemoveHeader(const QString &name)
{
    for (int i=0; i<mList.count(); i++)
    {
        if ( mList[i].name() == name)
        {
            mList.removeAt(i);
            //En el caso de cabeceras recurrentes debemos borrarlas todas
            if (name != "COMMENT" && name != "HISTORY" && name != "PROGRAM") return;
            else i--;
        }
    }
}

void FitHeaderList::RemoveHeader(const FitHeader &hdr)
{
    mList.removeOne(hdr);
}

void FitHeaderList::RemoveHeaders(const char* list[])
{
    for (int i=0; list[i]; i++)
    {
        FitHeader hr = Contains(list[i]);
        if (!hr.isInvalid()) RemoveHeader(QString(list[i]));
    }
}

double FitHeaderList::ra()
{
    FitHeader fh_ra = Contains("OBJCTRA");
    if (fh_ra.isInvalid()) fh_ra = Contains("RA");
    if (fh_ra.isInvalid()) return INVALID_FLOAT;
    double ra = Util::ParseRA(fh_ra.value());
    if (IS_VALIDF(ra)) return ra;
    bool ok;
    ra = fh_ra.value().trimmed().toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    if (ra > 24 || fh_ra.comment().toLower().contains("deg")) ra *= DEG2HOR;
    if (ra > -0.00001 && ra < 24.00001) return ra;
    return INVALID_FLOAT;
}

double FitHeaderList::dec()
{
    FitHeader fh_dec = Contains("OBJCTDEC");
    if (fh_dec.isInvalid()) fh_dec = Contains("DEC");
    if (fh_dec.isInvalid())  return INVALID_FLOAT;
    double dec = Util::ParseDEC(fh_dec.value());
    if (IS_VALIDF(dec)) return dec;
    bool ok;
    dec = fh_dec.value().trimmed().toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    if (dec > -90.00001 && dec < 90.00001) return dec;
    return INVALID_FLOAT;
}

double FitHeaderList::pixSize()
{
    FitHeader fh_pxsize = Contains("PIXSIZE");
    if (fh_pxsize.isInvalid()) fh_pxsize = Contains("PIXSCALE");
    if (fh_pxsize.isInvalid())  return INVALID_FLOAT;
    double pxsize = Util::ParseDEC(fh_pxsize.value());
    if (IS_VALIDF(pxsize)) return pxsize;
    bool ok;
    pxsize = fh_pxsize.value().trimmed().toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    if (pxsize > 0.001 && pxsize < 10.00) return pxsize;
    return INVALID_FLOAT;
}

int FitHeaderList::AddHeader(const QString &name)
{
    return AddHeader(FitHeader(name));
}

int FitHeaderList::AddHeader(const QString &name,const  QString &value)
{
     return AddHeader(FitHeader(name, value));
}

int FitHeaderList::AddHeader(const QString &name,const QString &value,const  QString &comment)
{
    return AddHeader(FitHeader(name, value, comment));
}

int FitHeaderList::AddHeader(const QString &name, int value)
{
     return AddHeader(FitHeader(name, value));
}

int FitHeaderList::AddHeader(const QString &name, int value,const  QString &comment)
{
     return AddHeader(FitHeader(name, value, comment));
}

int FitHeaderList::AddHeader(const QString &name, float value)
{
     return AddHeader(FitHeader(name, value));
}

int FitHeaderList::AddHeader(const QString &name, float value,const  QString &comment)
{
     return AddHeader(FitHeader(name, value, comment));
}

int FitHeaderList::AddHeader(const QString &name, double value)
{
     return AddHeader(FitHeader(name, value));
}

int FitHeaderList::AddHeader(const QString &name, double value,const  QString &comment)
{
     return AddHeader(FitHeader(name, value, comment));
}

FitHeaderList::~FitHeaderList()
{
    mList.clear();
}

void FitHeaderList::Clear()
{
    mList.clear();
}

FitHeaderList FitHeaderList::ReadFromFile(const QString &filename)
{
    QFile fi(filename);
    if (!fi.exists()) return FitHeaderList();
    if (!fi.open(QIODevice::ReadOnly)) return FitHeaderList();
    FitHeaderList fhl = ReadFromFile(&fi);
    fi.close();
    return fhl;
}

bool FitHeaderList::IsMainHeader(const QString &hdrname)
{
    for (char** hdr=(char**)S_HEADERS_MAIN; *hdr; hdr++)
        if (hdrname == *hdr) return true;
    return false;
}

bool FitHeaderList::IsStandardHeader(const QString& hdrname)
{
    for (char** hdr=(char**)S_HEADERS_STANDARD; *hdr; hdr++)
        if (hdrname == *hdr) return true;
    return false;
}

QString FitHeaderList::HeaderComments(const QString& hdrname)
{
    for (char** hdr=(char**)S_HEADERS_COMMENTS; *hdr; hdr+=2)
        if (hdrname == *hdr) return *(hdr+1);
    return "";
}

void FitHeaderList::UpdateValues()
{
    FitHeader hdrs = Contains("BITPIX");
    mBitPix = hdrs.value().toInt();
    hdrs = Contains("NAXIS1");
    mWidth = hdrs.value().toInt();
    hdrs = Contains("NAXIS2");
    mHeight = hdrs.value().toInt();
    hdrs = Contains("BZERO");
    if (hdrs.isInvalid())
        mBzero = mBitPix == 16 ? 32768 : (mBitPix==8 ? 127 : (mBitPix == -32 ? 0 : 32768));
    else
        mBzero = hdrs.value().toFloat();
    hdrs = Contains("BSCALE");
    mBscale = hdrs.value().toFloat();

    hdrs = Contains("EXPTIME");
    float exp = hdrs.value().toFloat();
    if (exp == 0)
    {
        hdrs = Contains("EXPOSURE");
        exp = hdrs.value().toFloat();
    }
    mExpTime = exp;
}

FitHeaderList FitHeaderList::ReadFromFile(QFile *fil)
{
    if (fil->pos()) fil->seek(0);

    bool end_found = false;
    int total_read = 0;
    char hdr[81]; hdr[80] = 0;
    FitHeaderList lst;

    while ( !end_found && !fil->atEnd())
    {
        int  numread = (int) fil->read(hdr,80);
        if (numread >= 0) hdr[numread] = 0;
        else
        {
            ERR("Error reading file header: '%s'", PTQ(fil->fileName()));
            lst.Clear();
            return lst;
        }
        total_read += numread;
        FitHeader fh = FitHeader::Parse(hdr);
        if (!fh.name().length() || (fh.name().length() > 8 && !fh.isHierarch()))
        {
            //Error de lectura de la cabecera, continuaremos leyendo, muy a nuestro pesar
            WARN("Bad image header at position %d", fil->pos()-80);
            WARN("Text read: '%s'",PTQ(QString(hdr)));
        }
        else if (fh.name() == "END")
            end_found = true;
        else
            lst.AddHeader(fh);
    }

    if (total_read % 2880 != 0)
    {
      int to_seek = 2880 - (total_read % 2880);
      int newpos = fil->pos() + to_seek;
      fil->seek(newpos);
    }

    //Actualizamos los valores de la cabecera
    lst.UpdateValues();

    return lst;
}


bool FitHeaderList::ReadFromIni(const QString &filename,const QString &_section)
{
    QFile fi(filename);
    if (!fi.exists()) return false;
    if (!fi.open(QIODevice::ReadOnly)) return false;
    fi.close();
    Clear();
    QString section = _section.toUpper();
    QSettings sett(filename, QSettings::IniFormat);
    for (int i=0;;i++)
    {
        QString st_name = QString("").sprintf("%s/HEADER_%03d", PTQ(section), i);
        QString st_value = QString("").sprintf("%s/VALUE_%03d", PTQ(section), i);
        QString st_comment = QString("").sprintf("%s/COMMENT_%03d", PTQ(section), i);
        if (!sett.contains(st_name)) break;
        QString name = sett.value(st_name, "").toString();
        if (!name.length()) break;
        QString value = sett.value(st_value, "").toString();
        QString comment = sett.value(st_comment, "").toString();
        AddHeader(name, value, comment);
    }
    return true;
}

bool FitHeaderList::SaveToIni(const QString &filename,  const QString &_section)
{
    QString section = _section.toUpper();
    QSettings sett(filename, QSettings::IniFormat);
    if (!sett.isWritable())
    {
        WARN("Unable to write file %s", PTQ(filename));
        return false;
    }
    QStringList list = sett.allKeys();
    for (int i=0; i<list.count(); i++)
        if (list.at(i).startsWith(section+"/", Qt::CaseInsensitive)) sett.remove(list.at(i));
    for (int i=0; i<count(); i++)
    {
        FitHeader fh = get(i);
        QString name = QString("").sprintf("%s/HEADER_%03d", PTQ(section), i);
        QString value = QString("").sprintf("%s/VALUE_%03d", PTQ(section), i);
        QString comment = QString("").sprintf("%s/COMMENT_%03d", PTQ(section), i);
        sett.setValue(name, fh.name());
        sett.setValue(value, fh.value());
        if (fh.comment().length()) sett.setValue(comment, fh.comment());
    }
    return true;
}

bool FitHeaderList::BadHeaderOrder()
{
    bool found[NUM_HEADERS_ORDER];
    memset(found,0,sizeof(bool)*NUM_HEADERS_ORDER);

    for (int i=0; i<mList.count(); i++)
    {
        //Calculo la posición en la que tiene que estar esta cabecera
        int cpos=0;
        for (;S_HEADER_ORDER[cpos] && const_cast<FitHeader &>(mList.at(i)).name() != S_HEADER_ORDER[cpos]; cpos++);
        if (!S_HEADER_ORDER[cpos]) continue;
        found[cpos] = true;
        //Comprobamos si hay alguna antes
        for (int c=0;c<cpos;c++)
            if (found[c]) return true;
    }
    return false;
}

void FitHeaderList::ReorderHeaders()
{
    //Reordenamos
    QList<FitHeader> newlist;
    QList<FitHeader> start_of_headers;

    for (int i=0;i<NUM_HEADERS_ORDER;i++)
    {
        FitHeader hdr = Contains(S_HEADER_ORDER[i]);
        if (hdr.name().length())
        {
            start_of_headers.append(hdr);
            RemoveHeader(QString((char*)S_HEADER_ORDER[i]));
        }
    }
    //Añadir las cabeceras primarias
    for (int i=0; i<start_of_headers.count(); i++)
        newlist.append(start_of_headers.at(i));
    //Añadir todas las demás
    for (int i=0; i<mList.count(); i++)
         newlist.append(mList.at(i));
    mList = newlist;
}

const FitHeader& FitHeaderList::Contains(const QString &name)
{
    for (int i=0;i<mList.count(); i++)
    {
        if (const_cast<FitHeader&>(mList.at(i)).name() == name)
            return mList.at(i);
    }
    return S_EMPTY_HEADER;
}

int FitHeaderList::indexOf(const QString &name)
{
    for (int i=0;i<mList.count(); i++)
    {
        if (const_cast<FitHeader&>(mList.at(i)).name() == name)
            return i;
    }
    return -1;
}

const FitHeader& FitHeaderList::get(int idx)
{
    return mList.at(idx);
}

const FitHeader& FitHeaderList::getById(int id)
{
    for (int i=0;i<mList.count(); i++)
    {
        if (const_cast<FitHeader&>(mList.at(i)).uniqID() == id) return mList.at(i);
    }
    return S_EMPTY_HEADER;
}

QString FitHeaderList::CheckFormat()
{
    bool ok;
    //Cabecera SIMPLE
    FitHeader simple = Contains("SIMPLE");
    if (simple.isInvalid() || simple.value() != "T")
        return QString("Header SIMPLE has not a valid value.");

    //Comprobamos bitpix
    FitHeader bitpix = Contains("BITPIX");
    if (bitpix.isInvalid()) return "Invalid bits per pixel";
    int bb = bitpix.value().toInt();
    if (bb != 8 && bb != 16 && bb != 32 && bb != -32)
        return QString("Invalid bits per pixel value (%1). Bits per pixel should be 8, 16, 32, -32.").arg(bitpix.value());

    //Comprobamos las dimensiones de la imagen
    FitHeader naxis = Contains("NAXIS");
    if (naxis.value().toInt() != 2)
        return QString("Image contains %1 axes. This software is only able to open one-plane images.").arg(naxis.value());

    FitHeader naxis1 = Contains("NAXIS1");
    FitHeader naxis2 = Contains("NAXIS2");

    int width = naxis1.value().toInt();
    int height = naxis2.value().toInt();
    if (!width || naxis1.isInvalid() || !height || naxis2.isInvalid())
        return QString("Invalid image dimensions %1x%2").arg(naxis1.value(), naxis2.value());

    FitHeader sat = Contains("SATURATED");
    if (!sat.isEmpty() && !sat.isInvalid() && sat.value().toFloat(&ok) > 0 && ok)
        mSatLevel = sat.value().toFloat();
    else
        mSatLevel = FLOAT_MAX;

    //Si hemos terminado bien no hay problema
    return "";
}

QDateTime FitHeaderList::GetDate()
{
    for (int i=0; S_HEADERS_DATEFRAME[i]; i++)
    {
        FitHeader fh = Contains(S_HEADERS_DATEFRAME[i]);
        if (fh.isInvalid()) continue;
        QDateTime dt = Util::ParseDate(fh.value());
        if (dt.isNull() || !dt.isValid()) continue;
        //Fechas y horas locales
        if (fh.name().contains("LOC")) dt = dt.toUTC();
        dt.setTimeSpec(Qt::UTC);

        QString comments = fh.comment().toUpper();
        //Es fecha de comienzo de la exposición o de fin
        if (comments.contains("START") ||  fh.name().contains("END"))
            dt = dt.addSecs(exptime() / 2.0);
        else if (comments.contains("END") || fh.name().contains("END"))
            dt = dt.addSecs(-exptime() / 2.0);
        return dt;
    }
    return INVALID_DATETIME;
}

char* FitHeaderList::GetHeaderBuffer()
{
    int sz = (mList.count()+1)*80+1;
    char* buff = new char[sz];
    memset(buff,0,sz);
    for (int i=0; i<mList.count(); i++)
        mList[i].toString(buff+i*80);
    strcpy(buff+(mList.count())*80, "END");
    memset(buff+(mList.count())*80+3, ' ', 77);
    for (int i=0; i<sz-1; i++)
        if (buff[i] == 0) buff[i] = ' ';
    buff[sz-1] = 0;
    return buff;
}

FitImage::FitImage()
{
    mData = NULL;
    mDirty = false;
    mPlate = NULL;
    mInvalidPixels = 0;
    mHistogram = NULL;
}

FitImage::~FitImage()
{
    if (mData) delete mData;
    mData = NULL;
    if (mPlate) delete mPlate;
    mPlate = NULL;
    if (mHistogram) delete mHistogram;
    mHistogram= NULL;
}


FitImage* FitImage::OpenFile(const QString &filename, QString* err)
{
     //Comprobamos si es otro tipo de imagen y si es así usamos la otra funcón
     QFileInfo fix(filename);
     QString ext = fix.suffix().toLower();
     if (ext == "fit" || ext == "fits" || ext == "fts")
         return OpenFitFile2(filename, err);
     else if (ext == "jpg" || ext == "jpeg" || ext == "tif" || ext == "tiff" || ext == "png" || ext == "bmp" || ext == "gif" || ext == "bmp")
         return OpenNonFitFile(filename);
     else if (ext == "st6")
         return OpenSbigFile(filename);
     return NULL;
}

FitImage* FitImage::OpenFitFile(const QString &filename, QString* err)
{
    FitImage* fi = new FitImage();
    fi->mFilename = filename;
    float sat_level = 65535;            //Nivel de saturación
    //Histograma de alta resolución
    fi->mHistogram = new int[fi->mHistoSize = FIT_HISTO_SIZE];
    memset(fi->mHistogram, 0, sizeof(fi->mHistogram[0])*fi->mHistoSize);
    double bh_scale = fi->mHistoSize / 65535.0;

    QFile file(filename);
    if ( file.size() <= 2880  || !file.open(QIODevice::ReadOnly))
    {
       if (err) *err = fi->errorMsg();
       delete fi;
       return NULL;
    }

   //Leemos Todas las cabeceras
   fi->mHeaders  = FitHeaderList::ReadFromFile(&file);
   if (fi->mHeaders.count() == 0) fi->mErrorMsg = "Invalid file header";
   fi->mErrorMsg = fi->mHeaders.CheckFormat();

   //Guardamos datos adicionales desde las cabeceras
   fi->mBitPix = fi->mHeaders.bitpix();

   //Comprobamos el tamaño total de la imagen
   if (!fi->error())
   {
       int total_size  = fi->width()*fi->height();
       int stimated_size = file.pos() + total_size * (abs(fi->bitpix())/8);
       int filesize = (int) file.size();
       if (filesize < stimated_size)
           fi->mErrorMsg = "File size does not match with header description";
   }
   else
   {
       ERR("Error opening: '%s' : %s", PTQ(filename), PTQ(fi->errorMsg()));
       if (err) *err = fi->errorMsg();
       delete fi;
       return NULL;
   }
   double avg = 0, stdv = 0;
   fi->mMinpix = FLOAT_MAX;
   fi->mMaxPix = FLOAT_MIN;


   //Crear la matriz de datos y leerla al completo
   if (!fi->error())
   {
       bool little_endian = Core::littleEndian();
       fi->mData = new float[fi->numpix()];
       quint8 buffread[4];
       int np = fi->numpix();
       int bitpix = fi->bitpix();
       int bytepix = abs(bitpix)/8;
       float bzero = fi->mHeaders.bzero();
       float bscale = fi->mHeaders.bscale();
       if (bscale == 0) bscale = 1.0f;

       //Valores para la lectura
       short sval;              //Valor de 16 bits
       int ival;                //Valor de 32 bits
       float fval;              //Valor de -32 bits
       char *pt = NULL;

       switch(bitpix)
       {
           case 8: pt = NULL; break;
           case 16: pt = (char*)&sval; break;
           case 32: pt = (char*)&ival; break;
           case -32: pt = (char*)&fval; break;
           default: fi->mErrorMsg = "Invalid bit per pixel value."; break;
       }

       if (!fi->error())
       for (int p = 0; p<np; p++)
       {
           int numread = (int) file.read((char*)buffread, bytepix);
           if (numread != bytepix)
           {
               fi->mErrorMsg = "Can not read image from file";
               break;
           }
           switch (bitpix)
           {
               case 8:
               fi->mData[p] = (float) buffread[0] * bscale + bzero;
               break;
               case 16:
               {
                   if (little_endian)
                   {
                       pt[1] = buffread[0];
                       pt[0] = buffread[1];
                   }
                   else
                   {
                       pt[1] = buffread[1];
                       pt[0] = buffread[0];
                   }
                   //Asignación + Control de nivel de saturación
                   if ((fi->mData[p] = (float)sval * bscale + bzero) < 0)
                   {
                       fi->mData[p] = 65535 + sval;
                       if (fi->mData[p] < sat_level) sat_level = fi->mData[p];
                       if (sat_level < fi->headers()->satLevel()) fi->headers()->setSatLevel(sat_level);
                   }
               }
               break;
               case -32:
               case 32:
               {
                   if (little_endian)
                   {
                       pt[0] = buffread[3];
                       pt[1] = buffread[2];
                       pt[2] = buffread[1];
                       pt[3] = buffread[0];
                   }
                   else
                   {
                       pt[0] = buffread[0];
                       pt[1] = buffread[1];
                       pt[2] = buffread[2];
                       pt[3] = buffread[3];
                   }
                   fi->mData[p] =  (bitpix == 32) ? ival * bscale + bzero: fval * bscale + bzero;
               }
               break;
           }
           //Control del número de píxeles no válidos
           if ((bitpix < 0 && fi->mData[p] < 0.000000001f)
                   || (bitpix > 0 && fi->mData[p] < 0.001) || IS_INVALIDF(fi->mData[p]))
           {
               fi->mData[p] = 0.0f;
               fi->mInvalidPixels++;
           }
           //Estadísticas acumuladas sobre la imagen, media, stdv, max, min, histograma
           else
           {
               avg += fi->mData[p];
               stdv += POW2(fi->mData[p]);
               if (fi->mData[p] < fi->mMinpix) fi->mMinpix = fi->mData[p];
               if (fi->mData[p] > fi->mMaxPix) fi->mMaxPix = fi->mData[p];
               int histo_pos = (int)(fi->mData[p] * bh_scale);
               if (histo_pos < 0) histo_pos = 0;
               else if (histo_pos >= FIT_HISTO_SIZE) histo_pos = FIT_HISTO_SIZE-1;
               fi->mHistogram[histo_pos]++;
           }
       }
       //Fin para el cálculo de las estadísticas. Media y desviación típica
       avg /= (double)(np - fi->mInvalidPixels);
       stdv = sqrt(stdv / (double)(np - fi->mInvalidPixels) - POW2(avg) < 0 ? 0 : stdv / (double)(np - fi->mInvalidPixels) - POW2(avg));
       fi->mAveragePix = avg;
       fi->mStandardDev = stdv;
       //Cálculo el máximo del histograma (moda)
       fi->mMaxHistoIdx = 0;
       for (int i=0; i<FIT_HISTO_SIZE; i++)
            if (fi->mHistogram[fi->mMaxHistoIdx] < fi->mHistogram[i]) fi->mMaxHistoIdx = i;
       fi->mMode = RANGECHANGE(fi->mMaxHistoIdx, 0, FIT_HISTO_SIZE, 0, 65535);
       //Cerramos el fichero y establecemos la imagen a NO modificada
       file.close();
       fi->SetDirty(false);
   }
   if (err) *err = fi->errorMsg();
   return fi;
}

FitImage* FitImage::OpenFitFile2(const QString &filename, QString* err)
{
    FitImage* fi = new FitImage();
    quint8* buff_line = NULL;
    quint8* buffread = NULL;

    fi->mFilename = filename;
    float sat_level = 65535;            //Nivel de saturación
    //Histograma de alta resolución
    fi->mHistogram = new int[fi->mHistoSize = FIT_HISTO_SIZE];
    memset(fi->mHistogram, 0, sizeof(fi->mHistogram[0])*fi->mHistoSize);
    double bh_scale = fi->mHistoSize / 65535.0;

    QFile file(filename);
    if ( file.size() <= 2880  || !file.open(QIODevice::ReadOnly))
    {
       if (err) *err = fi->errorMsg();
       delete fi;
       return NULL;
    }

   //Leemos Todas las cabeceras
   fi->mHeaders  = FitHeaderList::ReadFromFile(&file);
   if (fi->mHeaders.count() == 0) fi->mErrorMsg = "Invalid file header";
   fi->mErrorMsg = fi->mHeaders.CheckFormat();

   //Guardamos datos adicionales desde las cabeceras
   fi->mBitPix = fi->mHeaders.bitpix();

   //Comprobamos el tamaño total de la imagen
   if (!fi->error())
   {
       int total_size  = fi->width()*fi->height();
       int stimated_size = file.pos() + total_size * (abs(fi->bitpix())/8);
       int filesize = (int) file.size();
       if (filesize < stimated_size)
           fi->mErrorMsg = "File size does not match with header description";
   }
   else
   {
       ERR("Error opening: '%s' : %s", PTQ(filename), PTQ(fi->errorMsg()));
       if (err) *err = fi->errorMsg();
       delete fi;
       return NULL;
   }
   double avg = 0, stdv = 0;
   fi->mMinpix = FLOAT_MAX;
   fi->mMaxPix = FLOAT_MIN;


   //Crear la matriz de datos y leerla al completo
   if (!fi->error())
   {
       bool little_endian = Core::littleEndian();
       fi->mData = new float[fi->numpix()];

       int np = fi->numpix();
       int bitpix = fi->bitpix();
       int bytepix = abs(bitpix)/8;
       float bzero = fi->mHeaders.bzero();
       float bscale = fi->mHeaders.bscale();
       if (bscale == 0) bscale = 1.0f;

       //Valores para la lectura
       buff_line = new quint8[bytepix*fi->mHeaders.width()];
       short sval;              //Valor de 16 bits
       int ival;                //Valor de 32 bits
       float fval;              //Valor de -32 bits
       char *pt = NULL;

       switch(bitpix)
       {
           case 8: pt = NULL; break;
           case 16: pt = (char*)&sval; break;
           case 32: pt = (char*)&ival; break;
           case -32: pt = (char*)&fval; break;
           default: fi->mErrorMsg = "Invalid bit per pixel value."; break;
       }

       int width = fi->width();
       int height = fi->height();
       int p=0;

       if (!fi->error())
       for (int y=0; y<height; y++)
       {
           int numread = (int) file.read((char*)buff_line, bytepix*fi->mHeaders.width());
           if (numread != bytepix*fi->mHeaders.width())
           {
               fi->mErrorMsg = "Can not read image from file";
               break;
           }

           for (int x=0; x<width;x++,p++)
           {
               buffread = buff_line+(bytepix*x);
               switch (bitpix)
               {
                   case 8:
                   fi->mData[y*width+x] = (float) buffread[0] * bscale + bzero;
                   break;
                   case 16:
                   {
                       if (little_endian)
                       {
                           pt[1] = buffread[0];
                           pt[0] = buffread[1];
                       }
                       else
                       {
                           pt[1] = buffread[1];
                           pt[0] = buffread[0];
                       }
                       //Asignación + Control de nivel de saturación
                       if ((fi->mData[p] = (float)sval * bscale + bzero) < 0)
                       {
                           fi->mData[p] = 65535 + sval;
                           if (fi->mData[p] < sat_level) sat_level = fi->mData[p];
                           if (sat_level < fi->headers()->satLevel()) fi->headers()->setSatLevel(sat_level);
                       }
                   }
                   break;
                   case -32:
                   case 32:
                   {
                       if (little_endian)
                       {
                           pt[0] = buffread[3];
                           pt[1] = buffread[2];
                           pt[2] = buffread[1];
                           pt[3] = buffread[0];
                       }
                       else
                       {
                           pt[0] = buffread[0];
                           pt[1] = buffread[1];
                           pt[2] = buffread[2];
                           pt[3] = buffread[3];
                       }
                       fi->mData[p] =  (bitpix == 32) ? ival * bscale + bzero: fval * bscale + bzero;
                   }
                   break;
               }
               //Control del número de píxeles no válidos
               if ((bitpix < 0 && fi->mData[p] < 0.000000001f)
                       || (bitpix > 0 && fi->mData[p] < 0.001) || IS_INVALIDF(fi->mData[p]))
               {
                   fi->mData[p] = 0.0f;
                   fi->mInvalidPixels++;
               }
               //Estadísticas acumuladas sobre la imagen, media, stdv, max, min, histograma
               else
               {
                   avg += fi->mData[p];
                   stdv += POW2(fi->mData[p]);
                   if (fi->mData[p] < fi->mMinpix) fi->mMinpix = fi->mData[p];
                   if (fi->mData[p] > fi->mMaxPix) fi->mMaxPix = fi->mData[p];
                   int histo_pos = (int)(fi->mData[p] * bh_scale);
                   if (histo_pos < 0) histo_pos = 0;
                   else if (histo_pos >= FIT_HISTO_SIZE) histo_pos = FIT_HISTO_SIZE-1;
                   fi->mHistogram[histo_pos]++;
               }
           }
       }
       //Fin para el cálculo de las estadísticas. Media y desviación típica
       avg /= (double)(np - fi->mInvalidPixels);
       stdv = sqrt(stdv / (double)(np - fi->mInvalidPixels) - POW2(avg) < 0 ? 0 : stdv / (double)(np - fi->mInvalidPixels) - POW2(avg));
       fi->mAveragePix = avg;
       fi->mStandardDev = stdv;
       //Cálculo el máximo del histograma (moda)
       fi->mMaxHistoIdx = 0;
       for (int i=0; i<FIT_HISTO_SIZE; i++)
            if (fi->mHistogram[fi->mMaxHistoIdx] < fi->mHistogram[i]) fi->mMaxHistoIdx = i;
       fi->mMode = RANGECHANGE(fi->mMaxHistoIdx, 0, FIT_HISTO_SIZE, 0, 65535);
       //Cerramos el fichero y establecemos la imagen a NO modificada
       file.close();
       fi->SetDirty(false);
   }

   if (err) *err = fi->errorMsg();
   if (buff_line) delete buff_line;
   return fi;
}

void FitImage::ComputeStats()
{
   //Histograma de alta resolución
   if (mHistogram == NULL)
       mHistogram = new int[mHistoSize = FIT_HISTO_SIZE];
   memset(mHistogram, 0, sizeof(mHistogram[0])*mHistoSize);
   double bh_scale = mHistoSize / 65535.0;


  double avg = 0, stdv = 0;
  mMinpix = FLOAT_MAX;
  mMaxPix = FLOAT_MIN;

  mInvalidPixels = 0;
  int np = width()*height();
  int bp = headers()->bitpix();
  for (int p=0; p<np; p++)
  {
       //Control del número de píxeles no válidos
       if ((bp < 0 && mData[p] < 0.000000001f)
               || (bp > 0 && mData[p] < 0.001) || IS_INVALIDF(mData[p]))
       {
           mData[p] = 0.0f;
           mInvalidPixels++;
       }
       //Estadísticas acumuladas sobre la imagen, media, stdv, max, min, histograma
       else
       {
           avg += mData[p];
           stdv += POW2(mData[p]);
           if (mData[p] < mMinpix) mMinpix = mData[p];
           if (mData[p] > mMaxPix) mMaxPix = mData[p];
           int histo_pos = (int)(mData[p] * bh_scale);
           if (histo_pos < 0) histo_pos = 0;
           else if (histo_pos >= FIT_HISTO_SIZE) histo_pos = FIT_HISTO_SIZE-1;
           mHistogram[histo_pos]++;
       }
  }

   //Fin para el cálculo de las estadísticas. Media y desviación típica
   avg /= (double)(np - mInvalidPixels);
   stdv = sqrt(stdv / (double)(np - mInvalidPixels) - POW2(avg) < 0 ? 0 : stdv / (double)(np - mInvalidPixels) - POW2(avg));
   mAveragePix = avg;
   mStandardDev = stdv;
   //Cálculo el máximo del histograma (moda)
   mMaxHistoIdx = 0;
   for (int i=0; i<FIT_HISTO_SIZE; i++)
        if (mHistogram[mMaxHistoIdx] < mHistogram[i]) mMaxHistoIdx = i;
   mMode = RANGECHANGE(mMaxHistoIdx, 0, FIT_HISTO_SIZE, 0, 65535);
   SetDirty(false);
}

FitImage* FitImage::OpenNonFitFile(const QString &filename)
{
    QImage img(filename);
    if (!img.width() || !img.height()) return NULL;

    int width = img.width();
    int height = img.height();

    float* data = new float[width*height];
    for (int y=0; y<height; y++)
    {
        for (int x=0; x<width; x++)
        {
            QColor rgb = QColor(img.pixel(x, y));
            data[x + y * width] = ((float)rgb.red() + rgb.green() + rgb.blue())/3.0;
        }
    }
    FitImage* fii = new FitImage();
    fii->mData = data;
    fii->headers()->AddHeader("SIMPLE", "T");
    fii->headers()->AddHeader("BITPIX", "16");
    fii->headers()->AddHeader("NAXIS",  "2");
    fii->headers()->AddHeader("NAXIS1", width);
    fii->headers()->AddHeader("NAXIS2", height);
    fii->headers()->AddHeader("EXTEND", "T");
    fii->headers()->AddHeader("BZERO",  "32768");
    fii->headers()->AddHeader("BSCALE", "1");
    fii->mHeaders.CheckFormat();


    //Calcular las estadísticas de la imagen
    ImStat stat = ImStat::Compute(fii->mData, width, height);
    fii->mAveragePix = stat.mean();
    fii->mStandardDev = stat.stdev();
    fii->mMinpix = stat.min();
    fii->mMaxPix = stat.max();
    fii->mFilename = filename;
    return fii;
}

FitImage* FitImage::OpenSbigFile(const QString &filename)
{
    FitImage* fii = new FitImage();
    fii->headers()->AddHeader("SIMPLE", "T");
    fii->headers()->AddHeader("BITPIX", "16");
    fii->headers()->AddHeader("NAXIS",  "2");
    fii->headers()->AddHeader("NAXIS1", 0);
    fii->headers()->AddHeader("NAXIS2", 0);
    fii->headers()->AddHeader("EXTEND", "T");
    fii->headers()->AddHeader("BZERO",  "32768");
    fii->headers()->AddHeader("BSCALE", "1");

    bool format_ok = true;
    bool compressed = false;
    QDateTime date_img;

    //Mapa de SBIG-FIT
    QMap<QString,QString> mp;  QMap<QString,QString> comments;
    mp.insert("Exposure",     "EXPTIME");   comments.insert("Exposure", "Indicates the exposure time in 1/100ths of a second.");
    mp.insert("Focal_length", "FOCALLEN");  comments.insert("Focal_length", "The focal length in inches of the telescope used to capture the image");
    mp.insert("Width",        "NAXIS1");    comments.insert("Width", "Width of the image in pixels");
    mp.insert("Height",       "NAXIS2");    comments.insert("Height", "Width of the image in pixels");
    mp.insert("Temperature",  "CCD-TEMP");  comments.insert("Temperature", "The temperature of the CCD in celsius degrees at the end of the exposure");
    mp.insert("Observer",     "OBSERVER");  comments.insert("Observer", "The name of the observer who captured the image");
    mp.insert("X_pixel_size", "XPIXSZ");    comments.insert("X_pixel_size", "The width in millimeters of the pixels in the image");
    mp.insert("Y_pixel_size", "YPIXSZ");    comments.insert("Y_pixel_size","The height in millimeters of the pixels in the image");
    mp.insert("Pedestal",     "PEDESTAL");  comments.insert("Pedestal","Indicates any pedestal (constant value) that has been subtracted from each pixel in the image");
    mp.insert("E_gain",       "EGAIN");     comments.insert("E_gain", "Gives the conversion factor between pixel values and electrons of charge in the CCD. The units are e-/count");
    mp.insert("File_version", "FILEVER");   comments.insert("File_version", "Sbig source file version");
    mp.insert("Data_version", "DATAVER");   comments.insert("Data_version", "Sbig source file data version");

    QFile fil(filename);
    if (!fil.open(QIODevice::ReadOnly))
    {
        delete fii;
        return NULL;
    }
    QDataStream stream( &fil );
    stream.setByteOrder(QDataStream::LittleEndian);

    QByteArray buffer;
    //Leer toda la cabecera hasta CTRL+Z
    char c; bool ok;
    while ((ok = fil.getChar(&c)))
    {
        if (c == 26) break;         //CTRL+Z marca final de la cabecera
        if (c == '\r') continue;
        if (c == '\n')
        {
            if (!buffer.length()) continue;
            QString cad(buffer);
            buffer.clear();
            if (cad == "ST-6 Compressed Image")
            {
                format_ok = compressed = true;
                continue;
            }
            else if (cad == "ST-6 Image")
            {
                format_ok = true; compressed = false;
                continue;
            }
            QStringList hlist = cad.split('=');
            if (hlist.length() != 2) continue;
            hlist[0] = hlist.at(0).trimmed();
            hlist[1] = hlist.at(1).trimmed();

            if (hlist.at(0) == "Date")
            {
                QStringList slist = hlist.at(1).split('/');
                if (slist.length() == 3)
                {
                    int month = slist.at(0).toInt(&ok);
                    int day = slist.at(1).toInt(&ok);
                    int year = 2000 + slist.at(2).toInt(&ok);
                    if (ok)  date_img.setDate(QDate(year, month, day));
                    else date_img.setDate(QDate(0,0,0));
                }
            }
            else if (hlist.at(0) == "Time")
            {
                QStringList slist = hlist.at(1).split(':');
                if (slist.length() == 3)
                {
                    int hour = slist.at(0).toInt(&ok);
                    int minute = slist.at(1).toInt(&ok);
                    int sec =  slist.at(2).toInt(&ok);
                    if (ok)  date_img.setTime(QTime(hour,minute,sec));
                    else date_img.setTime(QTime(0,0,0));
                }
            }
            else if (hlist.at(0) == "Aperture")
            {
                bool convok;
                float ap = hlist.at(1).toFloat(&convok);    //Apertura en pulgadas cuadradas
                if (!convok) continue;
                float ap_rad_inches = sqrt(ap/M_PI);        //Radio de la apertura en pulgadas
                fii->headers()->AddHeader("APERTURE", QString("").sprintf("%0.2f", 2 * ap_rad_inches * 2.54 * 10), "Aperture diameter in milimeters");
                continue;
            }

            if (mp.contains(hlist.at(0)))
            {
                QString hname = mp[hlist.at(0)];
                fii->headers()->AddHeader(hname, hlist.at(1), comments[hlist.at(0)]);
            }
        }
        buffer.append(c);

    }
    FitHeader filever = fii->headers()->Contains("FILEVER");
    FitHeader dataver = fii->headers()->Contains("DATAVER");
    FitHeader pedestalh = fii->headers()->Contains("PEDESTAL");
    fii->mErrorMsg = fii->headers()->CheckFormat();
    if (!ok || !fii->error()
            || filever.isInvalid()
            || dataver.isInvalid()
            || pedestalh.isInvalid()
            || filever.value() != "3"
            || dataver.value() != "1"
       )
    {
        //Error leyendo sbig file
        delete fii;
        fil.close();
        return NULL;
    }
    if (date_img.date().year())
    {
        QString dt;
        dt = dt.sprintf("%04d-%02d-%02dT%02d:%02d:%02d",
                   date_img.date().year(), date_img.date().month(), date_img.date().day(),
                   date_img.time().hour(), date_img.time().minute(), date_img.time().second()
                   );
        fii->headers()->AddHeader("DATE-OBS", dt, "The date and time when this image was taken");
    }
    //Parece que está todo bien, podemos comenzar a leer el archivo
    int width = fii->width();
    int height = fii->height();
    float pedestal = (float) pedestalh.value().toDouble();
    float* data = new float[width * height];

    //http://www.sbig.de/download/aplication/file.format.pdf
    stream.device()->seek(2048);
    if (compressed)
    {
        for (int y=0; y<height; y++)
        {
            int x = 0;
            unsigned short line_size;
            unsigned short ant_value;
            signed char delta;
            stream >> line_size;
            int cont = 0;
            //Ya sabemos el tamaño de la línea, leemos hasta el final
            do
            {
                stream >> ant_value; cont+=2;
                data[y * width + x] = (float)ant_value + pedestal;
                x++;
                while ( cont < line_size && x < width )
                {
                    stream >> delta; cont++;
                    if (delta == -128) break;
                    ant_value += (signed short)delta;
                    data[y * width + x] = (float)ant_value + pedestal;
                    x++;
                }
            }
            while (cont < line_size && x < width);
            x = 0;
        }
    }
    else
    {
        for (int y=0; y<height; y++)
        {
            for (int x=0; x<width; x++)
            {
                unsigned short uval;
                stream >> uval;
                data[y * width + x] = (float)uval + pedestal;
            }
        }
    }
    fil.close();
    fii->mData = data;
    fii->SetDirty(false);
    fii->headers()->AddHeader("COMMENT","Data recovered from sbig file format");

    //Por último Calcular las estadísticas de la imagen para poder mostrarla correctamente
    ImStat stat = ImStat::Compute(fii->mData, width, height);
    fii->mAveragePix = stat.mean();
    fii->mStandardDev = stat.stdev();
    fii->mMinpix = stat.min();
    fii->mMaxPix = stat.max();
    fii->mFilename = filename;
    if (!format_ok) fii->mErrorMsg = "Invalid image format";
    return fii;
}

bool FitImage::SaveToFile(const QString &filename)
{
    return SaveToFile(filename, mHeaders.bitpix());
}

bool FitImage::SaveToFile(const QString &filename,int bitpix)
{
    //Tener cuidado aquí si lo estamos guardando en algún otro formato
   return FitImage::SaveToFile(mData,&mHeaders,filename,bitpix);
}

FitImage*  FitImage::Clone()
{
     //Clonar datos y cabeceras
     FitImage *fi = new FitImage();
     fi->mHeaders = mHeaders;
     fi->mInvalidPixels = mInvalidPixels;
     fi->mData = new float[fi->width() * fi->height()];
     memcpy(fi->mData, mData, fi->width() * fi->height() * sizeof(float));

     //Calcular estadísticas de la imagen
     ImStat stat = ImStat::Compute(fi->mData, fi->width(), fi->height());
     fi->mAveragePix = stat.mean();
     fi->mStandardDev = stat.stdev();
     fi->mMinpix = stat.min();
     fi->mMaxPix = stat.max();
     fi->mBitPix = mBitPix;
     fi->SetDirty(false);
     return fi;
}


FitImage* FitImage::CloneBinned(int bin)
{
     //Clonar datos y cabeceras
     FitImage *fi = new FitImage();
     fi->mHeaders = mHeaders;

     int width = fi->width() / bin;
     int height = fi->height() /bin;

     FitHeader fh_width = fi->headers()->Contains("NAXIS1");
     fh_width.setValue(QString().sprintf("%d", width));
     fi->headers()->AddHeader(fh_width);
     FitHeader fh_height = fi->headers()->Contains("NAXIS2");
     fh_height.setValue(QString().sprintf("%d", height));
     fi->headers()->AddHeader(fh_height);

     fi->mData = new float[width * height];
     fi->mHistogram = NULL;

     //Necesitamos la imagen exáctamente igual de centrada
     double midval = (bin-1)/2.0;
     double bin_start = -midval;
     double bin_end = midval+0.000001;

     for (int y=0; y<height; y++)
     {
         for (int x=0; x<width; x++)
         {
            fi->mData[y * width + x] = 0;
            for (double j=bin_start; j<bin_end; j+=1)
            {
                for (double i=bin_start; i<bin_end; i+=1)
                {
                    double py = y*bin+i;
                    double px = x*bin+j;
                    fi->mData[y * width + x] += BilinearConvolution(px, py);
                }
            }
         }
     }

     //Si tenía plate constants las cambiamos
     if (this->plate())
     {
        PlateConstants pc(width, height);
        pc.Initialize(plate()->ra(),plate()->dec(),
                      plate()->a()/bin,plate()->b()/bin,plate()->c()/bin,
                      plate()->d()/bin,plate()->e()/bin,plate()->f()/bin
                      );
        AstrometryComputer::AddStarndardHeader(fi, &pc);
        fi->LookForPlateRATAN();
     }
     fi->ComputeStats();
     return fi;
}

double FitImage::BilinearConvolution(double xPrime, double yPrime)
{
    double factor1, factor2, factor3, factor4;
    float* data = this->data();
    int width = this->width();
    int height = this->height();
    // There are four nearest original pixels, q11, q12, q21, and q22
    // While we may get away with using only four variables, this code
    // seperates out the x and y of each point for clarity reasons.
    // Most compilers should be capable of optimizing away the redundant
    // steps here.

    int q12x = (int)floor(xPrime);
    int q12y = (int)floor(yPrime);
    q12x = MAXVAL(0, q12x);
    q12y = MAXVAL(0, q12y);
    q12x = MINVAL(width-1, q12x);
    q12y = MINVAL(height-1, q12y);
    int q22x = (int)ceil(xPrime);
    int q22y = q12y;
    q22x = MINVAL(width-1, q22x);
    q22x = MAXVAL(0, q22x);
    int q11x = q12x;
    int q11y = (int)ceil(yPrime);
    q11y = MINVAL(height-1, q11y);
    q11y = MAXVAL(0, q11y);
    int q21x = q22x;
    int q21y = q11y;

    // We need to get the four nearest neighbooring pixels.
    // Pixels which are past the border of the image are clamped to the border already.
    double q11r = data[q11x + q11y*width];
    double q12r = data[q12x + q12y*width];
    double q21r = data[q21x + q21y*width];
    double q22r = data[q22x + q22y*width];

    // Here, it would be better to use a vector class to store the R,G and B values
    // to keep code consise. But they have been seperated out for maximum clarity
    // and simplicity.
    if ( q21x == q11x ) // special case to avoid divide by zero
    {
        factor1 = 1; // They're at the same X coordinate, so just force the calculatione to one point
        factor2 = 0;
    }
    else
    {
        factor1 = (((double)q21x - (double)xPrime)/((double)q21x - (double)q11x));
        factor2 = (((double)xPrime - (double)q11x)/((double)q21x - (double)q11x));
    }
    double R1r = factor1 * (double)q11r + factor2*(double)q21r;
    double R2r = factor1 * (double)q12r + factor2*(double)q22r;

    if (q12y == q11y) // special case to avoid divide by zero
    {
        factor3 = 1;
        factor4 = 0;
    }
    else
    {
        factor3 = ((double) q12y - yPrime)/((double)q12y - (double)q11y);
        factor4 = (yPrime - (double)q11y)/((double)q12y - (double)q11y);
    }
    double value = ((factor3 * R1r) + (factor4*R2r));
    return value;
}

FitScanner::FitScanner(QString filename)
{
    mFile = new QFile(filename);
    bool ok = mFile->open(QIODevice::ReadOnly);
    if (!ok)
    {
        delete mFile;
        return;
    }
    mHeaders = FitHeaderList::ReadFromFile(mFile);
    if (!mHeaders.count())
    {
        delete mFile;           //Lo liberamos
        mFile = NULL;           //Indico error
        return;
    }
    //Ya tenemos el archivo listo para comenzar la lectura
}

FitScanner::~FitScanner()
{
    if (mFile)
    {
        if (mFile->isOpen())
            mFile->close();
        delete mFile;
    }
}

void FitScanner::ReadScanLine(float* scanline)
{
    if (!mFile || !mFile->isOpen()) return;
    char buffread[4];
    int bitpix = abs(mHeaders.bitpix());
    int bytepix = bitpix/8;
    bool little_endian = Core::littleEndian();
    //Valores para la lectura
    short sval;
    int ival;
    float fval;
    char *pt;
    int width = mHeaders.width();
    switch(bitpix)
    {
        case 16: pt = (char*)&sval; break;
        case 32: pt = (char*)&ival; break;
        case -32: pt = (char*)&fval; break;
        default:
            ERR("FATAL ERROR - Invalid BITPIX header in image %d", bitpix);
            return;
    }
    float bzero  = mHeaders.bzero();
    for (int p=0; p<width; p++)
    {
        if (!mFile->read(buffread,bytepix))
        {
            delete mFile;
            mFile = NULL;
            break;
        }
        switch (bitpix)
        {
            case 8:
            scanline[p] = (float) buffread[0] + bzero;
            break;
            case 16:
            {
                if (little_endian)
                {
                    pt[1] = buffread[0];
                    pt[0] = buffread[1];
                }
                else
                {
                    pt[1] = buffread[1];
                    pt[0] = buffread[0];
                }
                scanline[p] = sval + bzero;
            }
            break;
            case -32:
            case 32:
            {
                if (little_endian)
                {
                    pt[0] = buffread[3];
                    pt[1] = buffread[2];
                    pt[2] = buffread[1];
                    pt[3] = buffread[0];
                }
                else
                {
                    pt[0] = buffread[0];
                    pt[1] = buffread[1];
                    pt[2] = buffread[2];
                    pt[3] = buffread[3];
                }
                scanline[p] =  (bitpix == 32) ? ival + bzero: fval + bzero;
            }
            break;
        }
    }
}

bool FitImage::SaveToFile(float* data,FitHeaderList* headers,const QString &filename,int bitpix, int norm_level)
{
   FitHeaderList mHeaders = *headers;
   int width = mHeaders.width();
   int height = mHeaders.height();

   float bzero = mHeaders.bzero();
   FitHeader oldbzero = headers->Contains("BZERO");
   FitHeader oldbscale = headers->Contains("BSCALE");
   float bscale = oldbscale.isInvalid()?1:oldbscale.value().toFloat();

   QFile fi(filename);
   if (!fi.open(QIODevice::WriteOnly | QIODevice::Truncate)) return false;

   if (bitpix != headers->bitpix())
   {
       //Recalculamos bzero
       switch (bitpix)
       {
        case 32:
           bzero = 2147483648.0f;
           bscale = 1.0f;
           mHeaders.SetHeader("BZERO",bzero);
           mHeaders.SetHeader("BSCALE",bscale);
           break;
        case 16:
           bzero = 32768.0f;
           bscale = 1.0f;
           mHeaders.SetHeader("BZERO",bzero);
           mHeaders.SetHeader("BSCALE",bscale);
           break;
        case -32:
           bzero = 0;
           bscale = 1;
           mHeaders.RemoveHeader("BSCALE");
           mHeaders.RemoveHeader("BZERO");
           break;
       }
   }
   //Cuidado, si hubiera que normalizar habrá que calcular el nivel de normalización
   float norm_mul = 1.0;
   if (norm_level)
   {
        ImStat imstat = ImStat::Compute(data, width, height);
        norm_mul = (float)norm_level / imstat.mean();
   }

   //Comenzamos guardando las cabeceras
   char header[81];
   memset(header,0,81);

   mHeaders.AddHeader("BITPIX",bitpix,BITPIX_COMMENT);
   mHeaders.RemoveMatches(QRegularExpression("^PROGRAM$"),QRegularExpression("^" XPROGRAM ".*$"));
   mHeaders.AddHeader("PROGRAM",QString("'%1 %2'").arg(XPROGRAM).arg(XVERSION), FitHeaderList::HeaderComments("PROGRAM"));

   int total_written = 0;                  //Número de cabeceras escritas
   for (int i=0;i<mHeaders.count();i++)
   {
       FitHeader hdr = mHeaders.get(i);
       if (hdr.isInvalid() || hdr.isEmpty()) continue;  //Este tipo de cabeceras no se guardan
       hdr.toString(header);
       fi.write(header, 80);
       total_written++;

   }
   //Escribimos la cabecera final
   fi.write("END                                                                             ", 80);
   total_written++;
   //Hay que dejarlo como múltiplo de 36
   while (total_written % 36 != 0)
   {
       memset(header,' ',80);
       fi.write(header, 80);
       total_written++;
   }

   //Comenzamos a pintar los datos
   int n = width * height;
   bool little_endian = Core::littleEndian();

   for (int p=0; p<n; p++)
   {
       switch (bitpix)
       {
           case 8:
           {
               char cval = data[p] * norm_mul - bzero;
               fi.write(&cval,1);
           }
           break;
           case 16:
           {
               short shortval = (short)(data[p] * norm_mul - bzero);
               char *buffer = (char*)&shortval;
               if (little_endian)
               {
                   char aux = buffer[1];
                   buffer[1] = buffer[0];
                   buffer[0] = aux;
               }
               fi.write(buffer, 2);
           }
           break;
           case -32:
           case 32:
           {
               float floatval = data[p] * norm_mul;
               int intval = (int)(floatval - bzero);
               char *buffer = bitpix == 32 ? (char*)&intval : (char*)&floatval;
               if (little_endian)
               {
                   char aux = buffer[3];
                   buffer[3] = buffer[0];
                   buffer[0] = aux;
                   aux = buffer[2];
                   buffer[2] = buffer[1];
                   buffer[1] = aux;
               }
               fi.write(buffer, 4);
           }
           break;
       }
   }
   //Dejar las cabeceras como estaban
   mHeaders.AddHeader(oldbzero);
   mHeaders.AddHeader(oldbscale);

   //Añadir el padding al final del archivo hasta que sea múltiplo de 2880
   int padsz = 2880 - (int)(fi.size() % 2880);
   if (padsz != 2880)
   {
       QByteArray ba = QByteArray(padsz,0);
       fi.write(ba);
   }
   fi.close();
   return true;
}

bool FitImage::SaveToFile(float* data,int width,int height,const QString &filename,int bitpix, int norm_level)
{
    FitHeaderList hl = FitHeaderList();
    hl.SetHeader("SIMPLE", "T", SIMPLE_COMMENT);
    hl.SetHeader("BITPIX", bitpix, BITPIX_COMMENT);
    hl.SetHeader("NAXIS", "2", NAXIS_COMMENT);
    hl.SetHeader("NAXIS1", width, NAXIS1_COMMENT);
    hl.SetHeader("NAXIS2", height, NAXIS2_COMMENT);
    if (bitpix == 16)
        hl.SetHeader("BZERO", "32768", BZERO_COMMENT);
    else if (bitpix == 32)
        hl.SetHeader("BZERO", "2147483648", BZERO_COMMENT);
    hl.SetHeader("BSCALE", "1", BSCALE_COMMENT);
    return SaveToFile(data, &hl, filename, bitpix, norm_level);
}

QDateTime FitImage::GetDate()
{
    return headers()->GetDate();
}

PlateConstants* FitImage::LookForPlateRATAN()
{
    if (mPlate) delete mPlate;
    return mPlate = mHeaders.LookForPlateRATAN();
}

PlateConstants* FitImage::LookForPlateROTA()
{
    if (mPlate) delete mPlate;
    return mPlate = mHeaders.LookForPlateROTA();
}

PlateConstants* FitHeaderList::LookForPlateRATAN()
{
    bool ok;
    const char *xhead[] = {"CTYPE1", "CTYPE2", /** "CUNIT1", "CUNIT2" **/ "CRVAL1", "CRVAL2",
                           "CRPIX1", "CRPIX2", "CD1_1", "CD1_2", "CD2_1", "CD2_2", NULL};

    QString unit_1="deg", unit_2 = "deg";

    FitHeader fh;
    for (int i=0; xhead[i]; i++)
    {
        fh = this->Contains(xhead[i]);
        if ( fh.isInvalid()) return NULL;
    }

    //Tipo de proyección. De momento reconocemos las correciones SIP sin aplicarlas
    fh = this->Contains("CTYPE1");
    if (fh.isInvalid()) return NULL;
    if (fh.value() != "RA---TAN" && fh.value() != "RA---TAN-SIP" ) return NULL;

    fh = this->Contains("CTYPE2");
    if (fh.isInvalid()) return NULL;
    if (fh.value() != "DEC--TAN" && fh.value() != "DEC--TAN-SIP") return NULL;

    //Unidades de los ejes
    fh = this->Contains("CUNIT1");
    if (!fh.isInvalid()) unit_1  = fh.value().toLower();
    if (unit_1 != "deg") return NULL;

    fh = this->Contains("CUNIT2");
    if (!fh.isInvalid()) unit_2  = fh.value().toLower();
    if (unit_2 != "deg") return NULL;

    //Ascensión recta en el punto de referencia
    fh = this->Contains("CRVAL1");
    if (fh.isInvalid()) return NULL;
    double RA0 = fh.value().toDouble(&ok) * DEG2HOR;
    if (!ok || isnan(RA0)) return NULL;

    fh = this->Contains("CRVAL2");
    if (fh.isInvalid()) return NULL;
    double DEC0 = fh.value().toDouble(&ok);
    if (!ok || isnan(DEC0)) return NULL;

    //Píxel de referencia
    fh = this->Contains("CRPIX1");
    if (fh.isInvalid()) return NULL;
    double X0 = fh.value().toDouble(&ok);
    if (!ok || isnan(X0)) return NULL;

    fh = this->Contains("CRPIX2");
    if (fh.isInvalid()) return NULL;
    double Y0 = fh.value().toDouble(&ok);
    if (!ok || isnan(Y0)) return NULL;

    //Matriz de rotación y escalado
    fh = this->Contains("CD1_1");
    if (fh.isInvalid()) return NULL;
    double CD1_1 = fh.value().toDouble(&ok);
    if (!ok || isnan(CD1_1)) return NULL;

    fh = this->Contains("CD1_2");
    if (fh.isInvalid()) return NULL;
    double CD1_2 = fh.value().toDouble(&ok);
    if (!ok || isnan(CD1_2)) return NULL;

    fh = this->Contains("CD2_1");
    if (fh.isInvalid()) return NULL;
    double CD2_1 = fh.value().toDouble(&ok);
    if (!ok || isnan(CD2_1)) return NULL;

    fh = this->Contains("CD2_2");
    if (fh.isInvalid()) return NULL;
    double CD2_2 = fh.value().toDouble(&ok);
    if (!ok || isnan(CD2_2)) return NULL;


    double a, b, c, d, e, f;

    a = - CD1_1 * DEG2RAD;
    b = - CD1_2 * DEG2RAD;
    d = CD2_1 * DEG2RAD;
    e = CD2_2 * DEG2RAD;
    c =  - a * (X0-1) - b * (Y0-1);     //Siempre que se haya elegido el mismo punto de referencia
    f =  - d * (X0-1) - e * (Y0-1);     //Esto funcionará y como hemos elegido el centro funciona

    PlateConstants* pc = new PlateConstants(this->width(), this->height());
    pc->Initialize(RA0, DEC0, a, b, c, d, e, f);

    return pc;
}

PlateConstants* FitHeaderList::LookForPlateROTA()
{
    bool ok;
    const char *xhead[] = {"CTYPE1", "CTYPE2", /** "CUNIT1", "CUNIT2" **/ "CRVAL1", "CRVAL2",
                           "CRPIX1", "CRPIX2", "CROTA1", "CROTA2", "CDELT1", "CDELT2", NULL};

    QString unit_1="deg", unit_2 = "deg";

    //Comprobamos las cabeceras
    FitHeader fh;
    for (int i=0; xhead[i]; i++)
    {
        fh = this->Contains(xhead[i]);
        if ( fh.isInvalid()) return NULL;
    }

    //Tipo de proyección. De momento reconocemos las correciones SIP sin aplicarlas
    fh = this->Contains("CTYPE1");
    if (fh.isInvalid()) return NULL;
    if (fh.value() != "RA---TAN" && fh.value() != "RA---TAN" ) return NULL;

    fh = this->Contains("CTYPE2");
    if (fh.isInvalid()) return NULL;
    if (fh.value() != "DEC--TAN" && fh.value() != "DEC--TAN") return NULL;

    //Unidades de los ejes
    fh = this->Contains("CUNIT1");
    if (!fh.isInvalid()) unit_1  = fh.value().toLower();
    if (unit_1 != "deg") return NULL;

    fh = this->Contains("CUNIT2");
    if (!fh.isInvalid()) unit_2  = fh.value().toLower();
    if (unit_2!= "deg") return NULL;

    //Ascensión recta en el punto de referencia
    fh = this->Contains("CRVAL1");
    if (fh.isInvalid()) return NULL;
    double RA0 = fh.value().toDouble(&ok) * DEG2HOR;
    if (!ok || isnan(RA0)) return NULL;

    fh = this->Contains("CRVAL2");
    if (fh.isInvalid()) return NULL;
    double DEC0 = fh.value().toDouble(&ok);
    if (!ok || isnan(DEC0)) return NULL;

    //Píxel de referencia
    fh = this->Contains("CRPIX1");
    if (fh.isInvalid()) return NULL;
    double X0 = fh.value().toDouble(&ok);
    if (!ok || isnan(X0)) return NULL;

    fh = this->Contains("CRPIX2");
    if (fh.isInvalid()) return NULL;
    double Y0 = fh.value().toDouble(&ok);
    if (!ok || isnan(Y0)) return NULL;

    //Rotación de los ejes
    fh = this->Contains("CROTA1");
    if (fh.isInvalid()) return NULL;
    double rotaX = fh.value().toDouble(&ok);
    if (!ok || isnan(rotaX)) return NULL;

    fh = this->Contains("CROTA2");
    if (fh.isInvalid()) return NULL;
    double rotaY = fh.value().toDouble(&ok);
    if (!ok || isnan(rotaY)) return NULL;

    //Traslación de la rotación
    fh = this->Contains("CDELT1");
    if (fh.isInvalid()) return NULL;
    double incA = fh.value().toDouble(&ok);
    if (!ok || isnan(incA)) return NULL;

    fh = this->Contains("CDELT2");
    if (fh.isInvalid()) return NULL;
    double incD = fh.value().toDouble(&ok);
    if (!ok || isnan(incD)) return NULL;


    //No estamos preparados para rotaciones distintas en los ejes
    double rota = (rotaX+rotaY)/2.0;
    if (unit_1 == "deg") rota *= DEG2RAD;

    double CD1_1 = incA*cos(rota);
    double CD1_2 = -incD*sin(rota);
    double CD2_1 = incA*sin(rota);
    double CD2_2 = incD*cos(rota);

    double a, b, c, d, e, f;

    a = - CD1_1 * DEG2RAD;
    b = - CD1_2 * DEG2RAD;
    d = CD2_1 * DEG2RAD;
    e = CD2_2 * DEG2RAD;
    c =  - a * (X0-1) - b * (Y0-1);     //Siempre que se haya elegido el mismo punto de referencia
    f =  - d * (X0-1) - e * (Y0-1);     //Esto funcionará y como hemos elegido el centro funciona

    PlateConstants* pc = new PlateConstants(this->width(), this->height());
    pc->Initialize(RA0, DEC0, a, b, c, d, e, f);


    return pc;
}

void FitImage::setPlate(PlateConstants* pt)
{
    if (mPlate) delete mPlate;
    mPlate = pt;
}

PlateConstants* FitImage::LookForPlateConstants()
{
    if (mPlate) delete mPlate;
    return mPlate = mHeaders.LookForPlateConstants();
}

PlateConstants*  FitImage::LookForPlate()
{
    if (mPlate) delete mPlate;
    return mPlate = mHeaders.LookForPlate();
}

PlateConstants* FitHeaderList::LookForPlate()
{
    PlateConstants* pt = LookForPlateConstants();
    if (!pt) pt = LookForPlateRATAN();
    if (!pt) pt = LookForPlateROTA();
    return pt;
}

PlateConstants* FitHeaderList::LookForPlateConstants()
{
   bool ok;
   const char *headers[] = {"LSPC-A", "LSPC-B" , "LSPC-C", "LSPC-D", "LSPC-E", "LSPC-F", "LSPC-RA", "LSPC-DEC",  NULL};
   FitHeader fh;
   for (int i=0; headers[i]; i++)
   {
       fh = this->Contains(headers[i]);
       if ( fh.isInvalid()) return NULL;
   }

   fh =  this->Contains("LSPC-A");
   double a = fh.value().toDouble(&ok);
   if (!ok || isnan(a)) return NULL;

   fh =  this->Contains("LSPC-B");
   double b = fh.value().toDouble(&ok);
   if (!ok || isnan(b)) return NULL;

   fh =  this->Contains("LSPC-C");
   double c = fh.value().toDouble(&ok);
   if (!ok || isnan(c)) return NULL;

   fh =  this->Contains("LSPC-D");
   double d = fh.value().toDouble(&ok);
   if (!ok || isnan(d)) return NULL;

   fh =  this->Contains("LSPC-E");
   double e = fh.value().toDouble(&ok);
   if (!ok || isnan(e)) return NULL;

   fh =  this->Contains("LSPC-F");
   double f = fh.value().toDouble(&ok);
   if (!ok || isnan(f)) return NULL;

   fh =  this->Contains("LSPC-RA");
   double ra = fh.value().toDouble(&ok);
   if (!ok || isnan(ra)) return NULL;

   fh =  this->Contains("LSPC-DEC");
   double dec = fh.value().toDouble(&ok);
   if (!ok || isnan(dec)) return NULL;

   PlateConstants* pc = new PlateConstants(this->width(), this->height());
   pc->Initialize(ra, dec, a, b, c, d, e, f);
   return pc;
}






ConvergeProfile::ConvergeProfile()
{
    mColorTableSize = CP_COLOR_TABLE_SZ;
    mColorTable = new quint8[mColorTableSize];
    mType = SQUARE_ROOT;
    mData = NULL;
    //Inicializamos la tabla de contraste
    mContrastTable = new float[256];
    for (int i = 0; i < 256; i++)
       mContrastTable[i] = 1.0f;
    mBright = 1.0f;
}

ConvergeProfile::~ConvergeProfile()
{
   if (mColorTable) delete mColorTable;
   mColorTable = NULL;
   if (mContrastTable) delete mContrastTable;
   mContrastTable = NULL;
}

void ConvergeProfile::SetImage(FitImage* im)
{
    mDataMin = im->minpix();
    mDataMax = im->maxpix();
    mBackLevel = im->mode();
    mBackStdev = im->stdevpix();
    mData = im->data();
    mWidth = im->width();
    mHeight = im->height();
}

void ConvergeProfile::SetDataAndRanges(float* data, int width, int height, FitImage* im)
{
    SetImage(im);
    mData = data;
    mWidth = width;
    mHeight = height;
    switch (mType)
    {
        case ConvergeProfile::HISTOGRAM_ECU:
        mMin = mBackLevel;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::LINEAR:
        mMin = mBackLevel;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::LOGARITMIC:
        break;

        case ConvergeProfile::SQUARE_ROOT:
        mMin = mBackLevel;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::POWER:
        break;

        case ConvergeProfile::EXPONENTIAL:
        break;
    }
    if (mMin < mDataMin) mMin = 0;
    if (mMax > mDataMax) mMax = mDataMax;
}

void ConvergeProfile::SetData(float* data, int width, int height)
{
    mInvalidColorTable = true;
    //Calculamos los niveles
    mData = data;
    mWidth = width;
    mHeight = height;
    mDataMin = 0;
    mDataMax = 65535;
    int mode = 0;
    int mHistogram[CP_HISTOGRAM_SZ];
    memset(mHistogram, 0, sizeof(mHistogram[0])*CP_HISTOGRAM_SZ);

    //Calculamos la media/stdv/moda
    double scale_histo = (mDataMax-mDataMin)/CP_HISTOGRAM_SZ;
    int npix = width*height;
    int pixcount = 0;
    double avg=0, stdv=0;
    for (int n = 0; n<npix; n++)
    {
        if (!IS_VALIDF(mData[n]) || mData[n] < 0.01) continue;   //No cosideramos píxeles de fondo ni de estrellas muy saturadas
        float lvl = mData[n];                                    //Solamente consideramos píxeles de estrella
        avg += lvl;
        stdv += lvl*lvl;
        //Cálculos de histograma
        int idx_histo = (int)(lvl*scale_histo);
        if (idx_histo < 0) idx_histo = 0; else if (idx_histo >= CP_HISTOGRAM_SZ) idx_histo = CP_HISTOGRAM_SZ-1;
        mHistogram[idx_histo]++;
        if (mHistogram[mode] < mHistogram[idx_histo]) mode = idx_histo;
        pixcount++;
    }
    avg /= (double)pixcount;
    stdv = sqrt(stdv / pixcount  - POW2(avg) < 0 ? 100 :  stdv / pixcount  - POW2(avg));

    mBackLevel = (float) mode / scale_histo;
    mBackStdev = (float) stdv;
}

void ConvergeProfile::CreateColorTable()
{
    for (int i = 0; i < mColorTableSize; i++)
    {
        double v = 0;
        if (mType == ConvergeProfile::LOGARITMIC)
        {
        }
        else if (mType == ConvergeProfile::LINEAR)
        {
            v = RANGECHANGE(i,0,mColorTableSize,0,255);  //i * 255.0f / mColorTableSize;
        }
        else if (mType == ConvergeProfile::SQUARE_ROOT)
        {
            float vs = i / (float) mColorTableSize;
            v = sqrt(vs)*255;
        }
        else if (mType == ConvergeProfile::HISTOGRAM_ECU)
        {

        }
        //Guardar el color en la tabla de colores
        if (v > 255) v = 255;
        else if (v < 0) v = 0;
        mColorTable[i] = (uchar)FROUND(v);
    }
    mInvalidColorTable = false;
}

void ConvergeProfile::SetOptimumFor(float* data, int width, int height)
{
    SetData(data, width, height);
    switch (mType)
    {
        case ConvergeProfile::HISTOGRAM_ECU:
        mMin = mBackLevel;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::LINEAR:
        mMin = mBackLevel;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::LOGARITMIC:
        break;

        case ConvergeProfile::SQUARE_ROOT:
        mMin = mBackLevel;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::POWER:
        break;

        case ConvergeProfile::EXPONENTIAL:
        break;
    }
    if (mMin < mDataMin) mMin = 0;
    if (mMax > mDataMax) mMax = mDataMax;
}

void ConvergeProfile::SetOptimumFor(FitImage *im)
{
    SetImage(im);
    switch (mType)
    {
        case ConvergeProfile::HISTOGRAM_ECU:
        mMin = mBackLevel - mBackStdev  / 10.0;;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::LINEAR:
        mMin = mBackLevel;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::LOGARITMIC:
        break;

        case ConvergeProfile::SQUARE_ROOT:
        mMin = mBackLevel - mBackStdev  / 10.0;;
        mMax = mBackLevel + mBackStdev * 3;
        mBright = 1.0f;
        break;

        case ConvergeProfile::POWER:
        break;

        case ConvergeProfile::EXPONENTIAL:
        break;
    }
    if (mMin < mDataMin) mMin = mDataMin;
    if (mMax > mDataMax) mMax = mDataMax;
}

void ConvergeProfile::Converge(QImage *pic)
{
    switch (pic->format())
    {
        case QImage::Format_Indexed8:
        Converge_Indexed8(pic);
        return;
        case QImage::Format_RGB32:
        Converge_RGB32(pic);
        break;
        default:
        ASSERT(1==0, "Invalid image format to converge");
        break;
    }
}

void ConvergeProfile::Converge_Indexed8(QImage* pic)
{
    if (mInvalidColorTable)
        CreateColorTable();

    uchar* scanline;
    int p = 0;

    float range = mMax - mMin;

    for (int y=0; y<mHeight; y++)
    {
        scanline = pic->scanLine(y);
        for (int x=0; x<mWidth; x++)
        {
            float fval = mData[p];
            if (fval < mMin) scanline[x] = 0;
            else if (fval > mMax) scanline[x] = 255;
            else  scanline[x] = (uint)((fval-mMin) / range * 256.0f );
            p++;
        }
    }
}

void ConvergeProfile::Converge_RGB32(QImage* pic)
{
    if (mInvalidColorTable)
        CreateColorTable();

    quint32* scanline;
    int p = 0;

    float range = mMax - mMin;

    for (int y=0; y<mHeight; y++)
    {
        //Así evitaremos operaciones del tipo setPixel
        scanline = (quint32*)pic->scanLine(y);
        for (int x=0; x<mWidth; x++)
        {
            float fval = mData[p];
            if (fval < mMin) scanline[x] = 0xFF000000;
            else if (fval > mMax) scanline[x] = 0xFFFFFFFF;
            else
            {
                quint8 graylevel = mColorTable[(uint)( (fval-mMin) / range * 256.0f)];
                scanline[x] = 0xFF000000|graylevel|graylevel<<8|graylevel<<16;
            }
            p++;
        }
    }
}


FitImage* FitImage::ExtractSlice(QPointF a, QPointF b, int height)
{
    FitImage* fi = new FitImage();
    fi->mHeaders = FitHeaderList();
    fi->mHeaders.AddHeader("SIMPLE","T");
    fi->mHeaders.AddHeader("BITPIX",16);
    fi->mHeaders.AddHeader("NAXIS","2");
    int width = (int)sqrt(POW2(b.x()-a.x())+POW2(b.y()-a.y()));
    int n = width * height;
    fi->mHeaders.AddHeader("NAXIS1",width);
    fi->mHeaders.AddHeader("NAXIS2",height);
    fi->mHeaders.AddHeader("EXTEND","T");
    fi->mHeaders.AddHeader("BZERO","32768");
    fi->mHeaders.AddHeader("BSCALE","1");
    fi->mData = new float[n];
    for (int i=0;i<n; i++) fi->mData[n] = 1;
    fi->mHeaders.AddHeader("COMMENT","This is a slice extracted from another image");

    return fi;
}























