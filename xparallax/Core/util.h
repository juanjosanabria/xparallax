#ifndef UTIL_H
#define UTIL_H

#include "core_global.h"
#include "progress.h"
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QStringBuilder>
#include <QFile>
#include <QColor>
#include <QTcpSocket>
#include <QCryptographicHash>
#include <QSslSocket>

class CORESHARED_EXPORT Util
{
public:
    Util();

    /** *********************************************** **/
    /** Operaciones generales con coordenadas esféricas **/
    /** *********************************************** **/
    static double           ParseRA(const QString& ra);
    static double           ParseDEC(const QString& lat);
    static QVector<double>  ParseRADEC(const QString& radec, bool get_dec_pos = false);       //Debería devolver dos números más. (número de decimales en cada coordenada)
    static QVector<double>  ParseXY(const QString& xy);
    static double           ParseLon(const QString& lng);
    static double           ParseLat(const QString& lat);
    static QDateTime        ParseDate(const QString& dtstr);
    static double           ParseEquinox(const QString& epoch);
    static QString          HttpDate(const QDateTime& dtt);
    static QTime            ParseTime(const QString& tstr);
    static double           JulianDate(int year, int month, int day);
    static double           JulianDate(int year, int month, int day, int hour, int minute, int second, int millisecond=0);
    static double           JulianDate(const QDateTime& dtt);
    static double           JulianDate(const QDate& dtt);
    static QDate            JD2Date(double jd);                         //Fecha juliana a fecha
    static QDateTime        JD2DateTime(double jd);                     //Fecha juliana a  fecha/hora
    static QString          RA2String(double ra);
    static QString          RA2String(double ra,const char* fmt);       //Formateo indicando el formato como cadena
    static QString          RA2String(double ra,int deci_places);       //Formateo indicando el número de lugares decimales
    static QString          DEC2String(double dec);
    static QString          DEC2String(double dec,const char* fmt);     //Formateo indicando el formato como cadena
    static QString          DEC2String(double dec,int deci_places);     //Formateo indicando el número de lugares decimales
    static QString          LON2tring(double lat);
    static QString          LAT2String(double lat);
    static QString          LON2tring(double lat,const char* fmt);
    static QString          LAT2String(double lat,const char* fmt);
    static QString          Epoch2String(double jd_epoch);
    static QString          Date2MPCString(QDateTime dt,int dec_places=5);
    static QString          Equinox2String(double equinox);
    static QColor           WebColor(QString cl);                      //Obtener un color a partir de un color web
    //Distancia angular en dos puntos de la esfera expresada en grados
    static double           Adist(double ra1, double dec1, double ra2, double dec2);
    //Ángulo de posición entre dos puntos
    static double           PosAngle(double ra1, double dec1, double ra2, double dec2);


    /** *********************************************** **/
    /** Operaciones de propósito general                **/
    /** *********************************************** **/
    static char*   TrimRight(char* cad);
    static char*   TrimLeft(char* cad);
    static char*   Trim(char* cad);
    /**
     * @brief LinearRegression Regresión lineal por mínimos cuadrados
     * @param data Datos, coordenadas X,Y de los puntos
     * @param weight Peso de los datos si lo tuvieran. NULL si todos pesan igual.
     * @param datalen Longitud de los datos
     * @param a Pendiente de la recta
     * @param b Ordenada en el origen
     */
    static void    LinearRegression(QPointF* data, double* weight, int datalen, double* a, double* b);

    static void Line(double Point1x, double Point1Y, double Point2x, double Point2y, double* m, double*b);
    //static void Line(QPointF a, QPointF b, double* m, double*b){Line(a.x(),a.y(),b.x(),b.y(),a,b);}


    static QString HttpRequest(const QString& url, bool* ok=NULL);
    static QString HttpRequest(const QString& url, const QStringList& post_vars, bool* ok=NULL);
    static void WriteAllText(const QString filename, const QString& text, bool* err = NULL);
    static void WriteAllLines(const QString filename, const QStringList& text, bool* err = NULL);

private:
    static const char RA_FMT[];
    static const char DE_FMT[];
    static const char LON_FMT[];
    static const char LAT_FMT[];
};

/**
 * @brief The FMatrix class Matriz 2D de doubles con operaciones básicas
 */
class CORESHARED_EXPORT FMatrix{
public:
    FMatrix();
    FMatrix(int height, int width);
    ~FMatrix();
   inline int width(){return mWidth;}
   inline int height(){return mHeight;}
   void setSize(int height, int width);
   double get(int i, int j);
   void set(int i, int j, double val);
   void copy(FMatrix* a);
   void debug();

private:
    double*     mData;
    int         mWidth;
    int         mHeight;
};

class CORESHARED_EXPORT FVector{
public:
   FVector(){;}
   FVector(int count){for(int i=0;i<count;i++) mData.append(0);}
   inline int count(){return mData.count();}
   inline double get(int i){return mData.at(i);}
   inline void set(int i, double val){*(mData.data() + i) = val;}
   inline double* data(int i){return mData.data() + i;}

private:
   QVector<double>  mData;

};


class CORESHARED_EXPORT WebReader
        : public BackgroundWorker
{
   Q_OBJECT

public:
    enum WebReadermethod{GET,POST_FORM,POST_MULTIPART,POST_TEXT};

public:
    WebReader();
    virtual ~WebReader();
    void clear();
    void cancel();
    inline bool readFromCache(){return mReadFromCache;}
    inline void setUrl(QString url){mUrl=url;}
    inline QString& url(){return mUrl;}
    inline void setMethod(WebReadermethod m){mMethod=m;}
    inline void setPostRawData(const QByteArray& rd){mPostRawData=rd;}
    inline WebReadermethod method(){return mMethod;}
    inline int error(){return mError;}
    inline void addParam(QString name, QString value){mQueryStrings.append(name); mQueryStrings.append(value);}
    inline QStringList* resp(){return &mBuffer;}
    int doRequest(bool usecaches = false);
    static QString formatError(int err);

private:
    WebReadermethod         mMethod;
    int                     mError;
    QString                 mUrl;
    QStringList             mBuffer;
    QStringList             mQueryStrings;
    QMutex*                 mMutex;
    QNetworkAccessManager*  mManager;
    QNetworkReply*          mReply;
    QByteArray              mPostRawData;
    bool                    mUseCaches;
    bool                    mReadFromCache;

    //Elementos de caché
    static QMutex                     sCacheMutex;
    static QHash<QString,QStringList> sCache;
    bool                    readCache(const QString& url);
    void                    setCache(const QString& url);           //Guarda un dato leído en la caché de memoria o disco
    //Otras funciones
    QString                 getParamString();

private slots:
    void ReadProgress_Slot(qint64 total, qint64 prog);
    void ReadyRead_Slot();
    static bool FileIsOlderThan(QString fa, QString fb);

};


#define BITMATRIX_DATASIZE 16384
class CORESHARED_EXPORT BitMatrix
{
public:
    BitMatrix(int start_x = 0,int start_y = 0, int width = 8, int height = 8);
    ~BitMatrix();
    void Reset();
    void MoveTo(int x,int y);
    //Devuelven coordenadas de la matriz
    inline int left(){return mStartX;}
    inline int top(){return mStartY;}
    inline int right(){return mStartX+mWidth-1;}
    inline int bottom(){return mStartY+mHeight-1;}
    inline int width(){return mWidth;}
    inline int height(){return mHeight;}
    //Indica si la matriz contiene un píxel
    inline bool InRect(int x,int y){ return y >= top() && y <= bottom() && x >= left() && x <= right(); }
    //Devuelve o setea los valores de la matriz
    bool get(int x,int y);
    void set(int x,int y,bool value);

    bool intersects(BitMatrix* b);

    void assign(BitMatrix* _b);
    void appendBits(BitMatrix* b);

private:
    int     mStartX;
    int     mStartY;
    int     mWidth;
    int     mHeight;
    int     mByteCount;
    uchar*  mData;

    uchar   mStatic[BITMATRIX_DATASIZE];   //Memoria estática que prevalecera en uso
    uchar*  mDinamic;                      //Memoria reservada si hubiera que hacerlo. Se usará

};

class CORESHARED_EXPORT WebSocket
        : public QObject {

public:
    WebSocket();
    ~WebSocket();

    bool connect(QString& url,QString method="GET",QStringList addhdr = QStringList());
    void close();
    int read(char* buffer, int max);
    QString readLine();
    bool connected(){return mReply && mReply->isOpen();}
    inline bool eof(){return mBytesRead == mContentLength;}
    inline int timeout(){return mTimeOut;}
    inline void setTimeOut(int to){mTimeOut = to;}
    inline int bytesRead(){return mBytesRead;}
    inline int contentLength(){return mContentLength;}
    inline QStringList* headers(){return &mHeaders;}
    inline QString& userAgent(){return mUserAgent;}
    inline void setUserAgent(QString& ua){mUserAgent = ua;}
    inline QString& error(){return mError;}
    inline int httpStatus(){return mHttpStatus;}
    static int getSize(QString& url);

private:
    QStringList mHeaders;
    QString     mUserAgent;
    QString     mError;
    int         mHttpStatus;
    int         mTimeOut;
    int         mBytesRead;
    int         mContentLength;

    QNetworkAccessManager * mManager;
    QNetworkRequest*        mRequest;
    QNetworkReply*          mReply;
};

#define FILEDOWNLOADER_DATASIZE (1024*1024/2)
class CORESHARED_EXPORT FileDownloader
        : public BackgroundWorker
{
   Q_OBJECT

public:
    FileDownloader();
    ~FileDownloader();
    void setFiles(QString url, QString file);
    void setFiles(QStringList urls, QStringList output_files);
    virtual void cancel();
    inline bool cancelled(){return mCancelled;}
    inline QString& error(){return mError;}
    inline bool atomicDownload(){return mAtomicDownload;}
    inline void setAtomicDownload(bool ad){mAtomicDownload = ad;}
    inline void setOnlyHeaders(bool oh){mOnlyHeaders = oh;}
    inline bool onlyHeaders(){return mOnlyHeaders;}
    inline QVector<QStringList>* headers(){return &mHeaders;}
    inline QVector<QStringList>* addHeaders(){return &mAdditionalHeaders;}
    inline int httpStatus(int idx){return (int)mHttpStatus[idx];}
    inline int fileSize(int idx){return (int)mFileSizes[idx];}
    static int Background_Download(void *data);


private:
    QStringList             mUrls;
    QStringList             mOutputFiles;
    QString                 mError;
    QNetworkAccessManager*  mManager;
    QNetworkReply*          mReply;
    QString                 mCurrentFilename;
    QFile*                  mOutputFile;
    QEventLoop              loop;
    qint64                  mCurrentFileBytes;
    char                    mBuffer[FILEDOWNLOADER_DATASIZE];
    qint64                  mContentLength;     //Valor de todas las cabecera Content-Length
    QVector<int>            mFileSizes;
    QVector<QStringList>    mHeaders;
    QVector<QStringList>    mAdditionalHeaders; //Cabeceras adicionales para enviar con cada archivo
    QVector<int>            mHttpStatus;
    bool                    mCancelled;
    bool                    mAtomicDownload;    //Los ficheros se descarga o no usando un directorio temporal
    bool                    mOnlyHeaders;

    bool doWork();
    bool downloadFile(QString url, QString dstfile,QStringList addHdrs);

    QString readLine(QTcpSocket* sk);
    void writeLine(QTcpSocket* sk,QString ln);

public slots:
    void Cancel_Slot();

};

#endif // UTIL_H
