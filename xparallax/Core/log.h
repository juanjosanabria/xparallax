#ifndef LOG_H
#define LOG_H

#include <QObject>
#include <QMutex>
#include "core.h"
#include "core_global.h"

#define LOG_BUFFER_LEN      2048
#define LOG_SEPARATOR       "--------------------------------------------------------------------"

class CORESHARED_EXPORT Logger
        : public QObject
{
    Q_OBJECT
public:
    enum LogType{ LOGTYPE_INFO, LOGTYPE_WARN, LOGTYPE_ERROR, LOGTYPE_DEBUG};

public:
    Logger();
    ~Logger();
    void info(const char *fmt, ...);
    void warn(const char *fmt, ...);
    void error(const char *fmt, ...);
    void debug(const char *fmt, ...);

    void info(const QString& cad);
    void warn(const QString& cad);
    void error(const QString& cad);
    void debug(const QString& cad);

    void setOutputDebug(bool od){mOutputDebug=od;}
    void setOutputFile(QString filename);
    void setOutputFile(char* filename);

    const Logger &operator<<(const QString & t);

private:
    char    mBuffer[LOG_BUFFER_LEN];
    char*   mOutputFile;
    FILE*   mFil;
    QMutex  mMutex;
    bool    mOutputDebug;
    void    logToFile(char* cad);

signals:
    void msg(qint64 thread_id, int log_type, QString txt);
};

#endif // LOG_H
