#ifndef PROFILING_H
#define PROFILING_H

#include "core.h"
#include <QElapsedTimer>

#define PROFILE_THIS                  __PROFILEFUNC__;
#define PROFILE_BLOCK_START(name)     __PROFILEBLOCKS__(name);
#define PROFILE_BLOCK_END(name)       __PROFILEBLOCKE__(name)

#define END_PROFILE      if(Profiling::PROF) Profiling::PROF->endProfile();Profiling::PROF=NULL;
#ifdef QT_DEBUG
#define __PROFILEFUNC__        ProfilingFunc _pff = ProfilingFunc(__FUNCTION__);
#define __PROFILEBLOCKS__(name) _pff.startBlock(name);
#define __PROFILEBLOCKE__(name) _pff.endBlock(name);
#else
#define __PROFILEFUNC__          {;} //NOOP
#define __PROFILEBLOCKS__(name)  {;} //NOOP
#define __PROFILEBLOCKE__(name)  {;} //NOOP
#endif

struct PROFILE_STRUCT{
    QString fn_name;
    double  millis_total;
    quint64 exec_count;
};



class CORESHARED_EXPORT ProfilingFunc
{
private:
    QElapsedTimer           mTimer;
    QString                 mName;
    quint64                 mEllapsed;
    QVector<ProfilingFunc*> mBlocks;

public:
    ProfilingFunc(const char* name);
    ~ProfilingFunc();
    inline const QString& name(){return mName;}
    inline const quint64 ellapsedNano(){return mEllapsed;}
    void startBlock(char* name);
    void endBlock(char* name);
    void stop();
};

class CORESHARED_EXPORT Profiling
{
private:
    QElapsedTimer           mTimer;
    QVector<ProfilingFunc*> mCallStack;
    QVector<PROFILE_STRUCT> mProf;

public:
    static Profiling* PROF;
    Profiling();

    void startFunc(ProfilingFunc* f);
    void endFunc(ProfilingFunc* f);

    static void startProfile();
    static void endProfile();

};


#endif // PROFILING_H
