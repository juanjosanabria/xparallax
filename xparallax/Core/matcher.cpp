#include "matcher.h"
#include "util.h"
#include "projection.h"
#include "xglobal.h"
#include "specialfunctions.h"
#include "core.h"
#include "novas.h"
#include <qmath.h>

using namespace  alglib;

/******************** DEFINES PARA LOS MATCH PARAMETERS ***********************/
#define DEF_MATCH_date_frame            QDate::currentDate()
#define DEF_MATCH_pm_correct            PMDATE_FROM_FIT
#define DEF_MATCH_center_ra             "19 04 09.87"
#define DEF_MATCH_center_dec            "+36 37 57.3"
#define DEF_MATCH_pixel_size_um         NAN
#define DEF_MATCH_binning               1
#define DEF_MATCH_focal_length_mm       NAN
#define DEF_MATCH_pixel_scale_arcsec    NAN
#define DEF_MATCH_use_scale_by_fl_ps    true          //Usar como escala preferida la longitud focal+px size
#define DEF_MATCH_fov_radius_deg        NAN
#define DEF_MATCH_image_width           NAN
#define DEF_MATCH_image_height          NAN
#define DEF_MATCH_focal_reducer         1.0f
#define DEF_MATCH_vizier_catalog        VizierCatalog::SOURCE_CATALOGS[0]->name()
#define DEF_MATCH_vizier_server         VizierServer::SOURCE_SERVERS[0]->name()
#define DEF_MATCH_mag_limit             14
#define DEF_MATCH_preferred_center      0            //Usar el centro por defecto
#define DEF_MATCH_dist_tolerance        4.0f         //Tolerancia de distancia en valor absoluto. Se ha demostrado como buen valor
#define DEF_MATCH_fov_extension         3            //Descargamos un 300% del campo
#define DEF_MATCH_flip_field            false        //Hará un flip del campo antes de nada
#define DEF_MATCH_rotation              NAN          //No se especifica la rotación de la imagen
#define DEF_MATCH_rotation_tolerance    2            //+- 2 grados
#define DEF_MATCH_scale_filter          true         //En realidad no se contempla desactivar este filtro
#define DEF_MATCH_scale_tolerance       0.05f        //Máxima diferencia entre escalas 2%
#define DEF_MATCH_mag_filter            true
#define DEF_MATCH_mag_tolerance         0.1f         //Máxima diferencia entre magnitudes 10%
#define DEF_MATCH_num_align_stars       15           //Número de estrellas usadas para alineación
/******************** DEFINES PARA VALORES INTERNOS DEL MÓDULO ***********************/
#define MATCH_PROB_SUCCESS              1e-9         //1 entre mil mill. Si la prob. de una alineación es menor que esta, se considerará buena
#define MATCH_PROB_ERROR                1e-4         //1 entre 10mil. Si la prob. de una alineación es mayor no se dará por buena


Transform2D::Transform2D()
{

}

Transform2D::~Transform2D()
{

}

void Transform2D_ER::reset()
{
    m_theta = m_trans_x = m_trans_y = 0;
    m_cos_theta = m_scale_x = m_scale_y = 1;
    m_sin_theta = 0;
}

void Transform2D_ER::rotate(double angle_deg)
{
    m_theta = angle_deg;
    m_cos_theta = cos(angle_deg*DEG2RAD);
    m_sin_theta = sin(angle_deg*DEG2RAD);
}

void Transform2D_ER::translate(double tx, double ty)
{
    m_trans_x = tx;
    m_trans_y = ty;
}

void Transform2D_ER::scale(double s)
{
    m_scale_x = m_scale_y = s;
}

void Transform2D_ER::scale(double sx, double sy)
{
    m_scale_x = sx;
    m_scale_y = sy;
}

void Transform2D_ER::map(double x, double y, double *rx, double* ry)
{
    *rx = (x * m_cos_theta - y * m_sin_theta + m_trans_x) * m_scale_x;
    *ry = (x * m_sin_theta + y * m_cos_theta + m_trans_y) * m_scale_y;
}

void Transform2D_ER::imap(double x, double y, double *rx, double* ry)
{
    x = x/m_scale_x - m_trans_x;
    y = y/m_scale_y - m_trans_y;

    *rx = x * m_cos_theta + y * m_sin_theta;
    *ry = - x * m_sin_theta + y * m_cos_theta;
}


QPointF Transform2D_ER::map(QPointF p)
{
    double x, y;
    map(p.x(), p.y(), &x, &y);
    return QPointF(x,y);
}

QPointF Transform2D_ER::imap(QPointF p)
{
    double x, y;
    imap(p.x(), p.y(), &x, &y);
    return QPointF(x,y);
}


double Transform2D_ER::VectorAngle(double u1, double u2,double v1,double v2)
{
    double num = u1*v1+u2*v2;
    double den = sqrt( u1*u1 + u2*u2 ) * sqrt(v1*v1 + v2*v2 );
    double angle =  acos(num / den) * RAD2DEG;
    return angle;
}


double Transform2D_ER::VectorAngle_AH(double x1, double y1,double x2,double y2,
                            double x3, double y3,double x4,double y4)
{
   //Calculamos los vectores movidos a 0, 0 y devolvemos su ángulo
   return VectorAngle_AH(x2 - x1, y2 - y1, x4 - x3, y4 - y3);
}


double Transform2D_ER::VectorAngle_AH(double u1, double u2,double v1,double v2)
{
    //Comenzamos calculando el ángulo menor entre ambos vectores, pero cuidado, necesitamos devolver el ángulo antihorario
    double angle = VectorAngle(u1, u2, v1, v2);
    //Coordenada z del producto vectorial. (http://es.wikipedia.org/wiki/Producto_vectorial), si es positiva hemos girado
    //en sentido antihorario, si wz es negativa hemos girado en sentido horario por la regla del sacacorchos
    double wz = u1*v2 - u2*v1;
    return wz > 0 ? angle : 360 - angle;
}


double Transform2D_ER::VectorAngle(double x1, double y1,double x2,double y2,
                            double x3, double y3,double x4,double y4)
{
   //Calculamos los vectores movidos a 0, 0 y devolvemos su ángulo
   return VectorAngle(x2 - x1, y2 - y1, x4 - x3, y4 - y3);
}

void Transform2D_ER::ComputeTransform(double a1x, double a1y, double a2x, double a2y,double b1x, double b1y, double b2x, double b2y, Transform2D_ER* tf)
{
    double scale = CART_DIST(b1x, b1y, b2x, b2y) / CART_DIST(a1x, a1y, a2x, a2y);
    tf->reset();
    //Calculamos el ángulo que forman los vectores en sentido antihorario
    double angle = VectorAngle_AH(a1x, a1y, a2x, a2y, b1x, b1y, b2x , b2y);
    tf->rotate(angle);
    tf->scale(scale, scale);

    double auxx, auxy;
    tf->map(a1x, a1y, &auxx, &auxy);
    double translate_x = (b1x - auxx);
    double translate_y = (b1y - auxy);
    tf->translate(translate_x/scale , translate_y/scale);
    tf->m_scale_x = tf->m_scale_y = scale;
}

Transform2D_ER  Transform2D_ER::ComputeTransform(double a1x, double a1y, double a2x, double a2y,double b1x, double b1y, double b2x, double b2y)
{
    Transform2D_ER tf;
    ComputeTransform(a1x, a1y, a2x, a2y, b1x, b1y, b2x, b2y, &tf);
    return tf;
}


void Transform2D_LS::reset()
{
    mListA.clear();
    mListB.clear();
    A=B=C=D=E=F=G=H=I=J=K=L=0;
}

void Transform2D_LS::initialize(double a, double b, double c, double d, double e, double f)
{
    A=a; B=b; C=c; D=d; E=e; F=f;
    computeInverse();
}

double Transform2D_LS::theta()
{
    return ROT;
}

void Transform2D_LS::map(double x, double y, double *rx, double* ry)
{
    *rx = A * x + B * y + C;
    *ry = D * x + E * y + F;
}

void Transform2D_LS::imap(double x, double y, double *rx, double* ry)
{
    *rx = G * x + H * y + I;
    *ry = J * x + K * y + L;
}

void Transform2D_LS::addCtrlPoint(QPointF& a, QPointF& b)
{
    ASSERT(a.x() > 0.0001 && b.x() > 0.0001,"Transform2D_LS: Can't add ctrlpoint with zero values");
    mListA.append(a);
    mListB.append(b);
}

void Transform2D_LS::addCtrlPoint(double ax, double ay, double bx, double by)
{
    mListA.append(QPointF(ax, ay));
    mListB.append(QPointF(bx, by));
}

void Transform2D_LS::Compute()
{
    ASSERT(mListA.count()>=3,"Attempt to compute plate constants with less than 3 points");
    int pcount = mListA.count();

    double* XX = new double [pcount];
    double* YY = new double [pcount];
    FMatrix AA(pcount, 5);
    double* S = new double [3];

    //Calculamos las coordenadas estándard desde las estrellas de referencia
    int j = 0;
    for (int i = 0; i < pcount; i++)
    {
        XX[i] = mListB[i].x(); YY[i] = mListB[i].y();
        AA.set(j, 0, mListA[i].x()); AA.set(j, 1, mListA[i].y()); AA.set(j, 2, 1.0);
        AA.set(j, 3, XX[i]); AA.set(j, 4, YY[i]);
        j++;
    }
    int NREF = j;

    //Calculamos las constantes PLATE A,B,C
    Lsqfit(&AA, NREF, 3, S); A = S[0]; B = S[1]; C = S[2];

    //Calcular las constantes D,E,F
    for (int i = 0; i < NREF; i++)
        AA.set(i, 3 , AA.get(i, 4));    //Copia la columna AA[*,4] -> AA[*,3];
    Lsqfit(&AA, NREF, 3, S); D = S[0]; E = S[1]; F = S[2];

    computeInverse();
}

void Transform2D_LS::computeInverse()
{
    //Calculamos las constantes para la transformación inversa G,H,I,J,K,L
    G = (-E)/(D*B-E*A);
    H = B / (D*B-E*A);
    I = (E*C -F*B)/(D*B-E*A);
    J = (-D)/(A*E - D*B);
    K = A / (A*E - D*B);
    L = (D*C - A*F)/(A*E - D*B);

    //Calcular la rotación de la imagen
    double CD1_1 = -A * RAD2DEG;
    double CD1_2 = -B * RAD2DEG;
    double CD2_1 = D *  RAD2DEG;
    double CD2_2 = E * RAD2DEG;

    double incA = sqrt(CD1_1*CD1_1+CD1_2*CD1_2)*(CD1_1/fabs(CD1_1)) ;
    double incD = sqrt(CD2_1*CD2_1+CD2_2*CD2_2)*(CD2_2/fabs(CD2_2)) ;

    ROT = atan2(CD1_2/incA, CD2_2/incD)*RAD2DEG;

    int s_sin = SIGN(D);
    int s_cos = SIGN(A);

    //Ahora normalizamos el ángulo al cuadrante que le corresponda
    //Cuadrante 1 no se hace nada, el ángulo es el mismo
    if (s_cos > 0 && s_sin > 0) ROT = -ROT;
    //Cuadrante 2, seno positivo, coseno negativo
    else if (s_cos < 0 && s_sin > 0) ROT = 180 - ROT;
    //Cuadrante 3, seno y cosenos negatios
    else if (s_cos < 0 && s_sin < 0) ROT = 180 - ROT;
    //Cuadrante 4, coseno positivo, seno negativo
    else if (s_cos > 0 && s_sin < 0) ROT = 360 - ROT;

    if (ROT < 0) ROT += 360;
}

/**
 * @brief PlateConstants::Lsqfit Resuelve el sistema lineal de ecuaciones:  A[i,1]*s[1]+.... A[i,m] - A[i,m+1] = 0;     i=1,....n
 * @param _A Vector de entrada
 * @param S Vector donde se almacenarán las soluciones
 */

void Transform2D_LS::Lsqfit(FMatrix* _A,int N, int M, double* S)
{
    FMatrix A;
    A.copy(_A);

    double p = 0, q = 0, h = 0;
    for (int j = 0; j < M; j++)
    {
        for (int i = j + 1; i < N; i++)
        {
            if (A.get(i,j) != 0.0)
            {
                //Calcular p,q y el nuevo A.get(j,j)
                if (fabs(A.get(j, j)) < EPS * fabs(A.get(i, j)))
                {
                    p = 0.0; q = 1.0; A.set(j, j, -A.get(i, j)); A.set(i, j, 0.0);
                }
                else
                {
                    h = sqrt(A.get(j, j) * A.get(j, j) + A.get(i, j) * A.get(i, j));
                    if (A.get(j, j) < 0.0) h = -h;
                    p = A.get(j, j) / h; q = -A.get(i, j) / h; A.set(j, j,  h); A.set(i, j, 0.0);
                }
            }
            //Calcular el resto de la línea
            for (int k = j + 1; k <= M; k++)
            {
                h = p * A.get(j, k) - q * A.get(i, k);
                A.set(i, k, q * A.get(j, k) + p * A.get(i, k));
                A.set(j, k, h);
            }
        }
    }

    //Sustitución y cálculo de las incógnitas
    for (int i = M - 1; i >= 0; i--)
    {
        h = A.get(i, M);
        for (int k = i + 1; k < M; k++)
            h = h - A.get(i, k) * S[k];
        S[i] =  h / A.get(i, i);
    }
}


MatchParameters::MatchParameters()
{
    SetDefaults();
}

MatchParameters::~MatchParameters()
{
    image_height = 0;
    image_height = 0;
    center_ra = INVALID_FLOAT;
    center_dec = INVALID_FLOAT;
}

void MatchParameters::ComputeScale()
{
    ASSERT(!isnan(center_ra)&&!isnan(center_dec)&&!isnan(image_width)&&!isnan(image_height),
           "Can't compute scale. Some arguments are NAN");

    if (focal_reducer == 0 || isnan(focal_reducer)) focal_reducer = 1.0;
    double eff_fl = focal_length_mm * focal_reducer;
    //Calcular en base a la longitud focal
    if (use_scale_by_fl_ps)
    {
        if (!isnan(focal_length_mm) && !isnan(pixel_size_um))
            pixel_scale_arcsec = 206.265 * (pixel_size_um * binning) / eff_fl;
    }
    else
    {
        //La escala de la placa ya viene calculada
        if (isnan(focal_length_mm) && !isnan(pixel_size_um) && !isnan(pixel_scale_arcsec))
        {
            eff_fl = (206.265 * (pixel_size_um * binning)  ) / pixel_scale_arcsec ;
            focal_length_mm = eff_fl / focal_reducer;
        }
        else if (!isnan(focal_length_mm) && !isnan(pixel_size_um) && isnan(pixel_scale_arcsec))
            pixel_size_um = pixel_scale_arcsec * eff_fl / 206.265 / binning;
    }
   //Calcular el radio de visión que tenemos disponible
   fov_radius_deg = (MAXVAL(image_height, image_width) * pixel_scale_arcsec / 3600.0) / 2.0;
}

void MatchParameters::SetDefaults()
{
    date_frame =            DEF_MATCH_date_frame;
    pm_correct =            DEF_MATCH_pm_correct;
    center_ra =             Util::ParseRA(DEF_MATCH_center_ra);
    center_dec =            Util::ParseDEC(DEF_MATCH_center_dec);
    pixel_size_um =         DEF_MATCH_pixel_size_um;
    binning =               DEF_MATCH_binning;
    focal_length_mm =       DEF_MATCH_focal_length_mm;
    pixel_scale_arcsec =    DEF_MATCH_pixel_scale_arcsec;
    use_scale_by_fl_ps =    DEF_MATCH_use_scale_by_fl_ps;
    fov_radius_deg =        DEF_MATCH_fov_radius_deg;
    image_width =           DEF_MATCH_image_width;
    image_height =          DEF_MATCH_image_height;
    flip_field =            DEF_MATCH_flip_field;

    rotation = DEF_MATCH_rotation;
    rotation_tolerance = DEF_MATCH_rotation_tolerance;

    focal_reducer =         DEF_MATCH_focal_reducer;

    vizier_catalog =        DEF_MATCH_vizier_catalog;
    vizier_server =         DEF_MATCH_vizier_server;
    mag_limit =             DEF_MATCH_mag_limit;
    preferred_center =      DEF_MATCH_preferred_center;

    dist_tolerance =        DEF_MATCH_dist_tolerance;           //Tolerancia de distancia en valor absoluto

    fov_extension =         DEF_MATCH_fov_extension;            //Descargamos un 20% más de campo

    scale_filter =          DEF_MATCH_scale_filter;
    scale_tolerance =       DEF_MATCH_scale_tolerance;         //Máxima diferencia entre escalas 2%
    mag_filter =            DEF_MATCH_mag_filter;
    mag_tolerance =         DEF_MATCH_mag_tolerance;           //Máxima diferencia entre magnitudes 10%

    num_align_stars  =      DEF_MATCH_num_align_stars;

    //Elementos volátiles
    image_invalid_pix =     0;
}

void MatchParameters::Save(QSettings* settings,const QString& section)
{
     SaveString(settings, section, "center_ra", Util::RA2String(center_ra));
     SaveString(settings, section, "center_dec",Util::DEC2String(center_dec));

     //Escala definida por longitud focal y tamaño físico del píxel
     if (IS_VALIDF(pixel_size_um) && IS_VALIDF(focal_length_mm) && IS_VALIDF(focal_reducer) && binning != 0)
     {
        SaveFloat(settings, section, "pixel_size_um", pixel_size_um);
        SaveFloat(settings, section, "focal_length_mm", focal_length_mm);
        SaveFloat(settings, section, "focal_reducer", focal_reducer);
        SaveInt(settings, section, "binning", binning);
     }
     else
     {
        RemoveKey(settings, section, "pixel_size_um");
        RemoveKey(settings, section, "focal_length_mm");
        RemoveKey(settings, section, "focal_reducer");
        RemoveKey(settings, section, "binning");
     }

     //Rotación no definida
     if (IS_INVALIDF(rotation) || IS_INVALIDF(rotation_tolerance))
     {
         RemoveKey(settings, section, "rotation");
         RemoveKey(settings, section, "rotation_tolerance");
     }

     //Escala definida directamente por escal de la placa
     if (IS_VALIDF(pixel_scale_arcsec))
        SaveFloat(settings, section, "pixel_scale_arcsec" , pixel_scale_arcsec);
     else
         RemoveKey(settings, section, "pixel_scale_arcsec");
     SaveBool(settings, section, "use_scale_by_fl_ps", use_scale_by_fl_ps);
     //Siguientes valores
     SaveBool(settings, section, "flip_field", flip_field);
     SaveFloat(settings, section, "rotation", rotation);
     SaveFloat(settings, section, "rotation_tolerance", rotation_tolerance);
     SaveString(settings, section, "vizier_catalog", vizier_catalog);
     SaveString(settings, section, "vizier_server", vizier_server);
     SaveFloat(settings, section, "mag_limit", mag_limit);
     SaveInt(settings, section, "preferred_center", preferred_center);
     SaveFloat(settings, section, "dist_tolerance", dist_tolerance);
     SaveFloat(settings, section, "fov_extension", fov_extension);
     SaveBool(settings, section, "scale_filter", scale_filter);
     SaveFloat(settings, section, "scale_tolerance", scale_tolerance);
     SaveBool(settings, section, "mag_filter", mag_filter);
     SaveFloat(settings, section, "mag_tolerance", mag_tolerance);
     SaveInt(settings, section, "num_align_stars", num_align_stars);
     SaveInt(settings, section, "pm_correct", (int) pm_correct);
     SaveDate(settings, section, "date_frame", date_frame);
}

void MatchParameters::Load(QSettings* settings,const QString& section)
{
    center_ra = Util::ParseRA(ReadString(settings, section, "center_ra",    DEF_MATCH_center_ra));
    center_dec = Util::ParseDEC(ReadString(settings, section, "center_dec", DEF_MATCH_center_dec));
    pixel_size_um = ReadFloat(settings, section, "pixel_size_um",           DEF_MATCH_pixel_size_um);
    focal_length_mm = ReadFloat(settings, section, "focal_length_mm",       DEF_MATCH_focal_length_mm);
    focal_reducer = ReadFloat(settings, section, "focal_reducer",           DEF_MATCH_focal_reducer);
    binning = ReadInt(settings, section, "binning",                         DEF_MATCH_binning);
    use_scale_by_fl_ps = ReadBool(settings, section, "use_scale_by_fl_ps",  DEF_MATCH_use_scale_by_fl_ps);
    pixel_scale_arcsec =ReadFloat(settings, section, "pixel_scale_arcsec",  DEF_MATCH_pixel_scale_arcsec);
    vizier_catalog = ReadString(settings, section, "vizier_catalog",        DEF_MATCH_vizier_catalog);
    vizier_server =  ReadString(settings, section, "vizier_server",         DEF_MATCH_vizier_server);
    mag_limit = ReadFloat(settings, section, "mag_limit",                   DEF_MATCH_mag_limit);
    preferred_center = ReadInt(settings, section, "preferred_center",       DEF_MATCH_preferred_center);
    dist_tolerance = ReadFloat(settings, section, "dist_tolerance",         DEF_MATCH_dist_tolerance);
    flip_field = ReadBool(settings, section,        "flip_field",           DEF_MATCH_flip_field);
    rotation = ReadFloat(settings, section,        "rotation",              DEF_MATCH_rotation);
    rotation_tolerance = ReadFloat(settings, section,"rotation_tolerance",  DEF_MATCH_rotation_tolerance);
    fov_extension = ReadFloat(settings, section, "fov_extension",           DEF_MATCH_fov_extension);
    scale_filter = ReadBool(settings, section, "scale_filter",              DEF_MATCH_scale_filter);
    scale_tolerance = ReadFloat(settings, section, "scale_tolerance",       DEF_MATCH_scale_tolerance);
    mag_filter = ReadBool(settings, section, "mag_filter",                  DEF_MATCH_mag_filter);
    mag_tolerance = ReadFloat(settings, section, "mag_tolerance",           DEF_MATCH_mag_tolerance);
    num_align_stars = ReadInt(settings, section, "num_align_stars",         DEF_MATCH_num_align_stars);
    pm_correct = (MatchParameters::PMTDateType) ReadInt(settings, section, "pm_correct", (int)DEF_MATCH_pm_correct);
    date_frame = ReadDate(settings, section, "date_frame", DEF_MATCH_date_frame);
}

Matcher::Matcher()
    : BackgroundWorker("Background worker matcher")
{
    mCandidateA1 = mCandidateA2 = mCandidateB1 = mCandidateB2 = NULL;
    Clear();
}


Matcher::~Matcher()
{
    Clear();
}

void Matcher::Clear()
{
    for (int i=0; i<mMatchDetOrd.count(); i++)
        delete  mMatchDetOrd.at(i);
    mSourceStarsDet.clear();
    mSourceStarsCat.clear();
    mMatchDet.clear();
    mMatchCat.clear();
    mCoincidences.clear();
    mPairDet.clear(); mPairCat.clear();
    mBestMatch.clear();
    mCandidateA1 = mCandidateA2 = mCandidateB1 = mCandidateB2 = NULL;
}

void Matcher::ComputeProjection()
{
    mProjection = GnomonicProjection(
                                    mParameters.image_width / 2.0, mParameters.image_height / 2.0,
                                    mParameters.center_ra, mParameters.center_dec, 0, 0,
                                    mParameters.pixel_scale_arcsec, mParameters.pixel_scale_arcsec);
}


int Matcher::catStarsInImage(Transform2D* t2d)
{
    int ci = 0;
    for (int i=0; i<mMatchCat.count(); i++)
    {
       double img_x, img_y;
       t2d->imap(mMatchCat.at(i).x,mMatchCat.at(i).y, &img_x, &img_y);
       if (img_x >= 0 && img_x < mParameters.image_width
               && img_y >= 0 && img_y < mParameters.image_height)
       {
           ci++;
       }
    }
    return ci;
}

double  Matcher::intersectPercent(Transform2D* t2d)
{
    double parea = 100.00001;
    int outside_corners = 0;
    double cdist;
    double rad_pixels = MAXVAL(mParameters.image_width, mParameters.image_height)*
                        mParameters.fov_extension;

    //Calculamos todas las esquinas del rectángulo y comprobamos si están dentro del catálogo
    double cx, cy;
    t2d->map(0, 0, &cx, &cy);
    cdist = CART_DIST(0,0,cx,cy);
    if (cdist > rad_pixels) outside_corners++;

    t2d->map(mParameters.image_width, 0, &cx, &cy);
    cdist = CART_DIST(0,0,cx,cy);
    if (cdist > rad_pixels) outside_corners++;

    t2d->map(0, mParameters.image_height, &cx, &cy);
    cdist = CART_DIST(0,0,cx,cy);
    if (cdist > rad_pixels) outside_corners++;

    t2d->map(mParameters.image_height, mParameters.image_height, &cx, &cy);
    cdist = CART_DIST(0,0,cx,cy);
    if (cdist > rad_pixels) outside_corners++;

    if (outside_corners > 1) return 0;
    if (outside_corners == 1) return 75;
    //Por el momento devolveremos 0 con que haya una sola esquina fuera del área del catálogo

    return parea;
}

bool Matcher::stopConditionLessStars(int needed_stars)
{
    //Si las estrellas machean al 90%
    if ((mMatchCount >= needed_stars * 0.9 && needed_stars > 4)
            || (needed_stars >= 6 && mMatchCount >= needed_stars * 0.8)
            || (needed_stars >= 8 && mMatchCount >= needed_stars * 0.7)
            || (needed_stars >= 15 && mMatchCount >= needed_stars * 0.6)
            || (needed_stars >= 30 && mMatchCount >= needed_stars * 0.5))
    //Succesfull match
    {
        return true;
    }
    return false;
}

bool Matcher::stopConditionManyStars(int needed_stars)
{
    //Si las estrellas machean al 80%
    if ((mMatchCount >= needed_stars * 0.8)
            || (mMatchCount >= needed_stars)
    //O si las estrellas machean al 70% per"o el RMS es menor que la mitad de la tolerancia de distancia
            || (mMatchCount >= needed_stars * 0.6)

            || (mMatchCount >= mMatchDet.count()*0.6)
            )
    //Succesfull match
    {
        return true;
    }
    return false;
}

bool Matcher::stopConditionMuchMoreStars(int needed_stars)
{
    //Si las estrellas machean al 90%
    if ((mMatchCount >= 200)
            || (mMatchCount >= needed_stars * 0.3)
    //O si las estrellas machean al 70% per"o el RMS es menor que la mitad de la tolerancia de distancia
            || (mMatchCount >= needed_stars * 0.3) )
    //Succesfull match
    {
        return true;
    }
    return false;
}

quint64 fact(int n)
{
    quint64 result = 1;
    for (int i=2; i<=n; i++)
        result *= i;
    return result;
}

double bino(int n, int N, double p)
{
    double sum = 0;
    for (int k = n+1; k<=N; k++)
    {
        quint64 Nfact = fact(N);
        quint64 nfact = fact(k);
        quint64 NnFact = fact(N-k);
        double result = (Nfact / (nfact * (double)(nfact - NnFact))) *  pow(p, k) * pow(1-p, N-k);
        sum += result;
    }
    return sum;
}

double binox(int x, int N, double p)
{
    quint64 Nfact = fact(N);
    quint64 nfact = fact(x);
    quint64 NnFact = fact(N-x);
    double result = (Nfact *  pow(p, x) * pow(1-p, N-x) / (nfact * (double)(nfact - NnFact)));
    return result;
}

/**
 * @brief Matcher::certainty Calcula la certeza de que una alineación sea buena entre 0-1
 * @param cat_on_image Número de estrellas de catálogo que hay sobre la imagen para la alineación actual
 * @param detecteds Número de estrellas detectadas usadas para buscar la alineación
 * @param coincidences Número de coincidencias encontradas
 * @return
 */
double Matcher::probAlign(int cat_on_image, int detecteds, int align_stars_det, int align_stars_cat, int coincidences)
{
    //Distribución binomial
    // p = Probabilidad de que ocurra un evento (coincidencia)
    // x = Número de ocurrencias del evento (coincidences)
    // n = Número de tiradas. Número de veces que se intenta = cat_on_image
    // http://hyperphysics.phy-astr.gsu.edu/hbasees/math/disfcn.html#c2
    // http://mathworld.wolfram.com/BinomialDistribution.html
    // http://www.alglib.net/specialfunctions/distributions/binomial.php

    //Probar con redO130414_0072.fits
    //HP Número de píxeles donde situar una estrella daría una coincidencia (casos faborables)
    double HP = M_PI * POW2(mParameters.dist_tolerance) * detecteds;
    //MP Número de píxeles totales de la imagen (casos posibles)
    int MP = mParameters.image_width * mParameters.image_height;
    //Probabilidad de que un píxel al azar caiga sobre una estrella de las detectadas
    double p = HP/MP;
    //Casos que se han dado (como se ha forzado la alineación con 2 estrellas hay que poner 2)
    int x = coincidences - 2;
    int n = cat_on_image;

    DINFO("CHECKING CERTAINTY: CAT_STARS: %d; DET_STARS: %d; MATCHED: %d; IMG_W:%d; IMG_H:%d; MATCH_RADIUS:%0.2f",
           cat_on_image, detecteds, coincidences, mParameters.image_width, mParameters.image_height,
           mParameters.dist_tolerance);
    DINFO("BINOMIAL DIST: (p: %f; N: %d; k: %d;)", p, n, x);

    /************************ Binomial de  una sola alineación **************************/
    alglib_impl::ae_state state;
    alglib_impl::ae_state_init(&state);
    double prob_ge_x = alglib_impl::binomialcdistribution(x-1, n, p, &state);
    alglib_impl::ae_state_clear(&state);

    /************************ Binomial del total de alineaciones probadas  **************/
    n = align_stars_det*(align_stars_det+1)/2 * align_stars_cat * align_stars_cat;
    alglib_impl::ae_state_init(&state);
    double prob_ge_t = alglib_impl::binomialcdistribution(0, n, prob_ge_x, &state);
    alglib_impl::ae_state_clear(&state);

    //Informar de probabilidades en la ventana de log
    DINFO("   Probabilites: 1 STAR MATCH:[%6.2e]; CURRENT MATCH:[<%6.0e] CURRENT SET:[<%6.0e]", p, prob_ge_x, prob_ge_t);
    return prob_ge_t;
}

bool Matcher::stopCondition(int test_case)
{
    //Necesitamos al menos 4 puntos para calcular los Plate Constants
    if (mMatchCount < 4) return false;

    //01-Calculamos una alineación por mínimos cuadrados que seguramente dará más matches
    //--------------------------------------------------------------------------------------
    mTransform_LS.reset();
    MatchCount2(&mMatchDet, &mMatchCat, &mTransform_ER, &mTransform_LS);
    if (mTransform_LS.ctrlPointCount() >= 4)
    {
        mTransform_LS.Compute();
        int mc = MatchCount2(&mMatchDet, &mMatchCat, &mTransform_LS);
        if (mc > mMatchCount) mMatchCount = mc;
    }

    //02-Cálculo de contadores de estrellas. Dentro de la imagen, Matches necesarios etc
    //--------------------------------------------------------------------------------------
    //Calculamos el número de estrellas del catálogo que descansan sobre la imagen
    int over_image = catStarsInImage(&mTransform_LS);
    if (over_image < 4) return false;   //Esto es imposible que se dé (hay más de 4 matches), pero en fin...
    double intersect_percent = intersectPercent(&mTransform_LS);
    if (intersect_percent < 79) return false;

    //Calculamos el área descargada de catálogo y el área total de la imagen
    double img_area = mParameters.image_width * mParameters.image_height - mParameters.image_invalid_pix;

    //Calculamos en base a la densidad del catálogo el número de estrellas del catálogo que caen dentro de la imagen
    double needed_stars = 0;
    double img_density = mMatchDet.count() / img_area;
    double cat_density = over_image / img_area;
    if (cat_density < img_density)
        needed_stars = mMatchDet.count() * cat_density/img_density;
    else
        needed_stars = over_image * img_density/cat_density;
    //Evidentemente no podemos requerir más matches que las estrellas detectadas
    if (needed_stars > mSourceStarsDet.count()) needed_stars = mSourceStarsDet.count();

    //03-Comprobación del match
    //--------------------------------------------------------------------------------------
    bool succeed = false;
    if ((!test_case && mSourceStarsDet.count() < 150) || test_case == 1)
        succeed = stopConditionLessStars((int)ceil(needed_stars));
    else if (((!test_case && mSourceStarsDet.count() > 2500) || (needed_stars > 350)) || (test_case == 2))
        succeed = stopConditionMuchMoreStars((int)ceil(needed_stars));
    else
        succeed = stopConditionManyStars((int)ceil(needed_stars));

    double palign = probAlign(over_image, mMatchDet.count(), mPairDet.count(), mPairCat.count(), mMatchCount);
    if (palign < MATCH_PROB_SUCCESS || (needed_stars > 5 && needed_stars <= mMatchCount)) succeed = true;
    else if (palign > MATCH_PROB_ERROR) succeed = false;


    if (succeed)
    {
        INFO("    **Succeed** |NEEDED:%4d; MATCHED:%4d[%4.1f%%]; PROB:[<%6.0e] RMS:%4.3f; ROT:%7.3fº; TRANS:%7.1f/%7.1f; REL.SCALE:%4.3f",
                  (int)needed_stars, mMatchCount, mMatchCount*100.0/needed_stars,
                  palign,
                  mRMS,
                  mTransform_LS.theta(), mTransform_ER.transX(), mTransform_ER.transY(), mTransform_ER.getScale());
        mMatchProb = palign;
    }
    else
    {
        INFO("    Match tested|NEEDED:%4d; MATCHED:%4d[%4.1f%%]; PROB:[<%6.0e] RMS:%4.3f; ROT:%7.3fº; TRANS:%7.1f/%7.1f; REL.SCALE:%4.3f",
                  (int)needed_stars, mMatchCount, mMatchCount*100.0/needed_stars,
                  palign,
                  mRMS,
                  mTransform_LS.theta(), mTransform_ER.transX(), mTransform_ER.transY(), mTransform_ER.getScale());
    }

    //Caso contraro unsuccesful match!
    return succeed;
}

bool Matcher::DetectedStar_OrderX(MatchStar a, MatchStar b)
{
    return a.x > b.x;
}


bool Matcher::DetectedStar_greaterThan(DetectedStar a, DetectedStar b)
{
    return a.fluxISO() > b.fluxISO();
}

bool Matcher::VizierRecord_greaterThan(VizierRecord a, VizierRecord b)
{
    return a.anyMag() < b.anyMag();
}

void Matcher::SetSuccess(MatchStar* a1,MatchStar* a2, MatchStar* b1, MatchStar* b2, QVector<MatchStar>* _det_s, QVector<MatchStar>* _cat_s)
{
    mCandidateA1 = a1;
    mCandidateA2 = a2;
    mCandidateB1 = b1;
    mCandidateB2 = b2;
    if ((mMatchSuccess = (a1 && a2 && b1 && b2 && _det_s && _cat_s)))
        TestMatch(a1, a2, b1, b2, _det_s, _cat_s);
}

void Matcher::ProperMotinCorrection(VizierRecord* star)
{
    double pmra, pmdec;
    if (  mParameters.pm_correct != MatchParameters::PMDATE_NO_CORRECTION &&
         !mParameters.date_frame.isNull() &&
          mParameters.date_frame.isValid() &&
          IS_VALIDF( pmra = star->pmra() ) &&
          IS_VALIDF( pmdec = star->pmdec() ))
    {
        //Probada conversión de PM frente a LIBNOVAS(USNO). Correcta!
        double depoch =  2000.0 + (mParameters.date_frame.toJulianDay() - JD_J2000) / 365.25;
        mEpochDiff = depoch - 2000.0;
        double newdec = star->dec() + (pmdec * mEpochDiff) / (3600000.0);
        double cosdec = cos(star->dec() * DEG2RAD);
        double newra = star->ra() + (((pmra * mEpochDiff) / 3600000.0) / cosdec) * DEG2HOR;
        star->setRa(newra);
        star->setDec(newdec);
    }
}

/**
 * @brief Matcher::ConstructLists Construye las listas de estrellas óptimas para realizar el match.
 * Desecha estrellas de ambos conjuntos en base a las densidades de estrellas.
 * @param stars Estrellas detectadas en la imagen
 * @param stcount Número de estrellas detectadas en la imagen
 * @param catentris Estrellas de catálogo obtenidas de Vizier (o local)
 */
void Matcher::ConstructListsHeuristic(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris)
{
    int max_star_x = 0, max_star_y = 0;
    Clear();
    //Ordenar la lista de estrellas por brillo y a la vez calculamos el máximo en x, y
    mSourceStarsDet.clear();
    for (int i=0; i<stcount; i++)
    {
        mSourceStarsDet.append(stars[i]);
        if (stars[i].px() > max_star_x) max_star_x = (int)ceil(stars[i].px());
        if (stars[i].py() > max_star_y) max_star_y = (int)ceil(stars[i].py());
    }
    qSort(mSourceStarsDet.begin(), mSourceStarsDet.end(), Matcher::DetectedStar_greaterThan);


    //Agregar estrellas  las de macheo a la vez que las corrijo (si es necesario de proper motion)
    mSourceStarsCat.clear();;
    for (int i=0; i<catentris->count(); i++)
    {
       VizierRecord vr = catentris->at(i);
       ProperMotinCorrection(&vr);
       mSourceStarsCat.append(vr);
    }
    qSort(mSourceStarsCat.begin(), mSourceStarsCat.end(), Matcher::VizierRecord_greaterThan);

    //Creamos (seleccionamos) la lista de estrellas para machear de catálogo
    for (int i=0; i<mSourceStarsCat.count(); i++)
    {
        MatchStar ms;
        mProjection.RADecToXY( mSourceStarsCat.at(i).ra(), mSourceStarsCat.at(i).dec(), &ms.x, &ms.y );
        if (mParameters.flip_field) ms.x = (mParameters.image_width/2.0) - ms.x;
        ms.src_id = i;
        ms.flux = pow(10, (mSourceStarsCat.at(i).anyMag() - 26) / (-2.5) );
        mMatchCat.append(ms);
    }

    //Agregamos las estrellas a la lista pero respetando las proporciones de áreas
    //Calculamos el área descargada de catálogo y el área total de la imagen
    double cat_area = POW2(MAXVAL(mParameters.image_width, mParameters.image_height)*mParameters.fov_extension/2.0) * M_PI;
    double img_area = mParameters.image_width * mParameters.image_height;

    //Calculamos en base a la densidad del catálogo el número de estrellas del catálogo que caen dentro de la imagen
    double stars_needed = mSourceStarsCat.count() * (img_area/cat_area) * 2;
   //FIXME
    if (stars_needed < mParameters.num_align_stars) stars_needed = mParameters.num_align_stars;

    for (int i=0; i<stars_needed && i<mSourceStarsDet.count(); i++)
    {
        MatchStar ms;
        ms.src_id = i;
        ms.x = mSourceStarsDet[i].px();
        ms.y = mSourceStarsDet[i].py();
        ms.flux = mSourceStarsDet[i].fluxISO();
        mMatchDet.append(ms);
    }

   //Llegados a este punto tenemos construidas las dos listas de estrellas listas para machear y todas proyectadas
    //Construimos la lista ordenada por coordenadaa X
    for (int x=0; x <= max_star_x + 1; x++)
    {
        QVector<MatchStar>* listx = new QVector<MatchStar>();
        mMatchDetOrd.append( listx );
    }
    //Agregamos las estrellas
    for (int i=0; i<mMatchDet.count(); i++)
    {
        int coordx  = (int)mMatchDet.at(i).x;
        if (coordx < 0) coordx = 0;
        mMatchDetOrd[coordx]->append(mMatchDet.at(i));
    }
    //Ordenamos todas las listas
    for (int x=0; x <= (int)ceil(max_star_x); x++)
        qSort(mMatchDetOrd[x]->begin(), mMatchDetOrd[x]->end(), Matcher::DetectedStar_OrderX);

    //Listas de parjeas
    //Intento coger la misma densidad en las estrellas del catálogo

   //double num_stars_cat = (cat_density / img_density) * (cat_area / img_area) * mParameters.num_align_stars * 1.2;

    for (int i=0; i<mMatchDet.count() && i < mParameters.num_align_stars; i++)
        mPairDet.append(mMatchDet.at(i));

    double cat_pairing_stars = mPairDet.count() * (cat_area/img_area) * 1.2;

    for (int i=0; i<mMatchCat.count() && i < cat_pairing_stars; i++)
        mPairCat.append(mMatchCat.at(i));
}

void Matcher::ConstructListFull(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris)
{
    int max_star_x = 0, max_star_y = 0;
    Clear();
    //Ordenar la lista de estrellas por brillo y a la vez calculamos el máximo en x, y
    mSourceStarsDet.clear();
    for (int i=0; i<stcount; i++)
    {
        mSourceStarsDet.append(stars[i]);
        if (stars[i].px() > max_star_x) max_star_x = (int)ceil(stars[i].px());
        if (stars[i].py() > max_star_y) max_star_y = (int)ceil(stars[i].py());
    }
    qSort(mSourceStarsDet.begin(), mSourceStarsDet.end(), Matcher::DetectedStar_greaterThan);


    //Agregar estrellas  las de macheo a la vez que las corrijo (si es necesario de proper motion)
    mSourceStarsCat.clear();;
    for (int i=0; i<catentris->count(); i++)
    {
       VizierRecord vr = catentris->at(i);
       ProperMotinCorrection(&vr);
       mSourceStarsCat.append(vr);
    }
    qSort(mSourceStarsCat.begin(), mSourceStarsCat.end(), Matcher::VizierRecord_greaterThan);

    //Creamos (seleccionamos) la lista de estrellas para machear de catálogo
    for (int i=0; i<mSourceStarsCat.count(); i++)
    {
        MatchStar ms;
        mProjection.RADecToXY( mSourceStarsCat.at(i).ra(), mSourceStarsCat.at(i).dec(), &ms.x, &ms.y );
        if (mParameters.flip_field) ms.x = (mParameters.image_width/2.0) - ms.x;
        ms.src_id = i;
        ms.flux = pow(10, (mSourceStarsCat.at(i).anyMag() - 26) / (-2.5) );
        mMatchCat.append(ms);
    }

    for (int i=0; i<mSourceStarsDet.count(); i++)
    {
        MatchStar ms;
        ms.src_id = i;
        ms.x = mSourceStarsDet[i].px();
        ms.y = mSourceStarsDet[i].py();
        ms.flux = mSourceStarsDet[i].fluxISO();
        mMatchDet.append(ms);
    }

   //Llegados a este punto tenemos construidas las dos listas de estrellas listas para machear y todas proyectadas
    //Construimos la lista ordenada por coordenadaa X
    for (int x=0; x <= max_star_x + 1; x++)
    {
        QVector<MatchStar>* listx = new QVector<MatchStar>();
        mMatchDetOrd.append( listx );
    }
    //Agregamos las estrellas
    for (int i=0; i<mMatchDet.count(); i++)
    {
        int coordx  = (int)mMatchDet.at(i).x;
        if (coordx<0) coordx = 0;
        mMatchDetOrd[coordx]->append(mMatchDet.at(i));
    }
    //Ordenamos todas las listas
    for (int x=0; x <= (int)ceil(max_star_x); x++)
        qSort(mMatchDetOrd[x]->begin(), mMatchDetOrd[x]->end(), Matcher::DetectedStar_OrderX);

    //Listas de parjeas
    //Intento coger la misma densidad en las estrellas del catálogo

   //double num_stars_cat = (cat_density / img_density) * (cat_area / img_area) * mParameters.num_align_stars * 1.2;

    for (int i=0; i<mMatchDet.count() && i < mParameters.num_align_stars; i++)
        mPairDet.append(mMatchDet.at(i));

    for (int i=0; i<mMatchCat.count(); i++)
        mPairCat.append(mMatchCat.at(i));
}

void Matcher::ConstructLists(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris)
{
    ConstructListsHeuristic(stars, stcount, catentris);
   // ConstructListFull(stars, stcount, catentris);
}

int Matcher::MatchCount(QVector<MatchStar>* det_s, QVector<MatchStar>* cat_s, Transform2D* tr)
{
    double trans_x, trans_y;
    int cont = 0;
    //Computamos el número de coincidencias
    double dist_tolerance_2 = POW2(mParameters.dist_tolerance);
    for (int i = 0; i < det_s->count(); i++)
    {
        MatchStar *a = det_s->data()+i;
        bool found = false;
        for (int j = 0; j < cat_s->count() && !found; j++)
        {
            MatchStar *b = cat_s->data()+j;
            tr->map(a->x, a->y, &trans_x, &trans_y);
            double dist2 = POW2(trans_x - b->x) + POW2(trans_y - b->y);
            if (dist2 < dist_tolerance_2) found = true;
        }
        if (found) cont++;
    }
    return cont;
}

int Matcher::MatchCount2(QVector<MatchStar>* /*det_s*/, QVector<MatchStar>* cat_s, Transform2D* tr, Transform2D_LS* ls)
{
    double trans_x, trans_y;
    int cont = 0;

    //Para el cálculo de la condición de parada
    mRMS = 0;
    mCoinMinX = mCoinMinY = FLOAT_MAX;
    mCoinMaxX = mCoinMaxY = FLOAT_MIN;
    if (ls) ls->reset();

    //Computamos el número de coincidencias
    double dist_tolerance_2 = POW2(mParameters.dist_tolerance);
    //Recorremos las estrellas de catálogo proyectadas
    for (int i = 0; i < cat_s->count(); i++)
    {
        MatchStar *b = cat_s->data()+i;
        bool found = false;
        tr->imap(b->x, b->y, &trans_x, &trans_y);

        int px1  = (int)floor(trans_x - dist_tolerance_2);  if (px1 < 0) px1 = 0;
        int px2 = (int)ceil(trans_x + dist_tolerance_2);   if (px2 >= mMatchDetOrd.count()) px2 = mMatchDetOrd.count()-1;

        for (int x=px1; x<=px2 && !found; x++)
        {
            for (int j=0; j<mMatchDetOrd[x]->count() && !found; j++)
            {
                double dist2 = POW2(trans_x - mMatchDetOrd[x]->at(j).x) + POW2(trans_y - mMatchDetOrd[x]->at(j).y);
                if (dist2 < dist_tolerance_2)
                {
                    found = true;
                    cont++;
                    mRMS += POW2(dist2);
                    if (mMatchDetOrd[x]->at(j).x > mCoinMaxX) mCoinMaxX = mMatchDetOrd[x]->at(j).x;
                    if (mMatchDetOrd[x]->at(j).x < mCoinMinX) mCoinMinX = mMatchDetOrd[x]->at(j).x;
                    if (mMatchDetOrd[x]->at(j).y > mCoinMaxY) mCoinMaxY = mMatchDetOrd[x]->at(j).y;
                    if (mMatchDetOrd[x]->at(j).y < mCoinMinY) mCoinMinY = mMatchDetOrd[x]->at(j).y;
                    if (ls) ls->addCtrlPoint(mMatchDetOrd[x]->at(j).x, mMatchDetOrd[x]->at(j).y, b->x, b->y);
                }
            }
        }
    }
    mRMS = sqrt(mRMS/cont);
    return cont;
}


int Matcher::TestMatch(MatchStar* a1, MatchStar* a2, MatchStar* b1, MatchStar* b2,QVector<MatchStar>* _det_s, QVector<MatchStar>* _cat_s)
{
    //Comprobamos la escala
    if (mParameters.scale_filter)
    {
        double dist_a = CART_DIST(a1->x, a1->y, a2->x, a2->y);
        double dist_b = CART_DIST(b1->x, b1->y, b2->x, b2->y);
        double rdiff =  ECOMP(dist_a, dist_b);
        if (rdiff > mParameters.scale_tolerance) return 0;
        //Una restricción es que tampoco estén demasiado cerca a1 - a2
    }

    //Comprobamos la magnitud
    if (mParameters.mag_filter)
    {
        //ZP1 arbitrario, calculamos el zero point del catálogo
        double mag_1 = 26 - 2.5 * log(a1->flux);
        double zpb = mag_1 + 2.5 * log(b1->flux);
        double mag_a2 = 26 - 2.5 * log(a2->flux);
        double mag_b2 = zpb - 2.5 * log(b2->flux);

        double mdiff = ECOMP(mag_a2, mag_b2);
        if (mdiff > mParameters.mag_tolerance) return 0;
    }

    //Calculamos la transformación
    Transform2D_ER::ComputeTransform(a1->x, a1->y,a2->x, a2->y, b1->x, b1->y, b2->x, b2->y, &mTransform_ER);

    //Restricción de rotación. Acelera la búsqueda muchísimo. Para rotaciones conocidas
    if (IS_VALIDF(mParameters.rotation) && IS_VALIDF(mParameters.rotation_tolerance))
    {
        double ang_deg = mTransform_ER.theta();
        double rot_min = mParameters.rotation - mParameters.rotation_tolerance;
        double rot_max = mParameters.rotation + mParameters.rotation_tolerance;
        if (rot_min < 0){rot_min += 360; rot_max += 360;}
        if ((ang_deg < rot_min || ang_deg > rot_max) &&
           (ang_deg+360 < rot_min || ang_deg+360 > rot_max))
            return 0;
    }

    //Comprobamos que el catálogo intersecta con la imagen en un espacio suficiente
    double intersection = intersectPercent(&mTransform_ER);
    if (intersection < 50) return 0;

    int cont_m = MatchCount2(_det_s, _cat_s, &mTransform_ER);
    return cont_m;
}

QVector<MatchStar> Matcher::MatchBruteForce(QVector<MatchStar>* _det_s, QVector<MatchStar>* _cat_s)
{
    INFO("Looking for match");
    mMatchSuccess = false;
    mMatchCount = 0;

    MatchStar* det_s = mPairDet.data();
    int det_c = mPairDet.count();
    MatchStar* cat_s = mPairCat.data(); // mPairCat.data();
    int cat_c = mPairCat.count();
    INFO("Total match detected stars: %d", _det_s->count());
    INFO("Total match catalog stars: %d", _cat_s->count());
    INFO("Detected pairing stars: %d", det_c);
    INFO("Catalog pairing stars : %d", cat_c);
    int total_steps = POW2(det_c)*POW2(cat_c);

    for (int i = 0; i < det_c && !cancelled(); i++)
    {
        for (int j = i+1; j < det_c && !cancelled(); j++)
        {
            mProgress = 100 - (det_c-i)*(det_c-j)*POW2(cat_c)*100/total_steps;
            for (int k = 0; k < cat_c && !cancelled(); k++)
            {
                for (int l = 0; l < cat_c; l++)
                {
                    if (k == l) continue;
                    MatchStar *a1 = det_s+i;
                    MatchStar *a2 = det_s+j;
                    MatchStar *b1 = cat_s+k;
                    MatchStar *b2 = cat_s+l;
                    int cont_m = TestMatch(a1, a2, b1, b2, _det_s, _cat_s);
                    if (cont_m > mMatchCount)
                    {
                        mMatchCount = cont_m;
                        mBestMatch.clear();
                        mBestMatch.append(*a1); mBestMatch.append(*a2); mBestMatch.append(*b1); mBestMatch.append(*b2);
                        if (stopCondition())
                        {
                            mCoincidences.clear();
                            SetSuccess(a1, a2, b1, b2, _det_s, _cat_s);
                            ConstructPlatePointList();
                            //Si la lista construida tiene menos de 4 estrellas continuamos
                            if (mCoincidences.count() < 4)
                            {
                                SetSuccess(NULL, NULL, NULL, NULL, NULL, NULL);
                                continue;
                            }
                            return mBestMatch;
                        }
                    }
                }
            }
        }
    }


    //Hemos finalizado. Hacemos un último intento por coger el Best_match
    //En campos poco poblados las restricciones son más restricitvas
    if (mBestMatch.count() > 4 && !mCancelled)
    {
        mMatchCount= TestMatch(mBestMatch.data()+0,mBestMatch.data()+1,mBestMatch.data()+2,mBestMatch.data()+3, _det_s, _cat_s);
        if (stopCondition(2))
        {
            mCoincidences.clear();
            SetSuccess(mBestMatch.data()+0, mBestMatch.data()+1, mBestMatch.data()+2, mBestMatch.data()+3, _det_s, _cat_s);
            ConstructPlatePointList();
            //Si la lista construida tiene menos de 4 estrellas continuamos
             mProgress = 100;
            if (mCoincidences.count() >= 4) return mBestMatch;
        }
    }

    //La alineación ha fallado?
    if (mBestMatch.count() <= 3) mBestMatch.clear();
    return mBestMatch;
}

bool Matcher::stopEndMagnitudeCorr()
{
    //Calcular la magnitud media de las estrellas del match
    double mmag = 0;
    for (int i=0; i<mCoincidences.count(); i++)
        mmag += mSourceStarsCat.at(mCoincidences.at(i).src_cat).anyMag();
    mmag /= mCoincidences.count();

    QList<double> fluxeslog;
    double magf = 0;
    for (int i=0; i<mCoincidences.count(); i++)
    {
        double df = -2.5 * log(mSourceStarsDet[mCoincidences.at(i).src_det].fluxISO());
        magf += df;
        fluxeslog.append(df);
    }
   magf /= mCoincidences.count();
   double zero_p = mmag - magf;

   for (int i=0; i<mCoincidences.count(); i++)
   {
       double df = -2.5 * log(mSourceStarsDet[mCoincidences.at(i).src_det].fluxISO()) + zero_p;
       mmag += mSourceStarsCat.at(mCoincidences.at(i).src_cat).anyMag();
       INFO("%4.2f %4.2f", df, mmag);
   }
   return false;
}

VizierRecord  Matcher::getCatSrcStar(double x, double y)
{
   double cur_dist = FLOAT_MAX;
   int cur_idx = -1;
   for (int i=0; i<mMatchCat.count(); i++)
   {
       double dist = CART_DIST(mMatchCat[i].x, mMatchCat[i].y, x, y);
       if (dist < cur_dist)
       {
           cur_idx = i;
           cur_dist = dist;
       }
   }
   if (cur_idx == -1) return VizierRecord();
   return mSourceStarsCat.at(cur_idx);
}

DetectedStar  Matcher::getDetSrcStar(double x, double y)
{
   double cur_dist = FLOAT_MAX;
   int cur_idx = -1;
   for (int i=0; i<mMatchDet.count(); i++)
   {
       double dist = CART_DIST(mMatchDet[i].x, mMatchDet[i].y, x, y);
       if (dist < cur_dist)
       {
           cur_idx = i;
           cur_dist = dist;
       }
   }
   return mSourceStarsDet.at(cur_idx);
}

MatchStar*  Matcher::getMatchStarInList(double x, double y, QVector<MatchStar>* list)
{
    double dist = FLOAT_MAX;
    MatchStar* candidate = NULL;
    for (int i=0; i<list->count(); i++)
    {
        double cdist = CART_DIST(x, y, list->at(i).x, list->at(i).y);
        if (cdist < dist)
        {
            dist = cdist;
            candidate = list->data()+i;
        }
    }
    if (dist > 4) return NULL;
    return candidate;
}

double Matcher::AngularDistance(double ra1,double dec1,double ra2,double dec2)
{
    ra1 *= HOR2RAD;
    dec1 *= DEG2RAD;
    ra2 *= HOR2RAD;
    dec2 *= DEG2RAD;

    double ro = 0, d1, d2, d3, d4, a1;
    double dal=fabs(ra2-ra1);
    if(dal>M_PI) dal=2.0*M_PI-dal;
    double ddel=fabs(dec2-dec1);

    //Para posiciones razonablemente alejadas se usará la distancia sobre la esfera
    if(dal > 0.0029 || ddel > 0.0029)
    {
        a1=cos(dal);
        d1=cos(dec1);
        d2=cos(dec2);
        d3=sin(dec1);
        d4=sin(dec2);
        ro = acos(d3 * d4 + d1 * d2 * a1);
    }
    //Para posiciones muy cercanas se usará la distancia cartesiana mucho mas rápida
    else
    {
        d1=cos((dec1+dec2)/2.0);
        ro=sqrt(d1*d1*dal*dal+ddel*ddel);
    }
    return (ro+0.000000001) * RAD2DEG;
}


MatchStar*  Matcher::getCatMatchStar(double ra, double dec)
{
    double dist = FLOAT_MAX;
    int candidate_idx = -1;
    for (int i = 0; i< mSourceStarsCat.count(); i++)
    {
        double xdist = AngularDistance(ra, dec, mSourceStarsCat[i].ra(), mSourceStarsCat[i].dec());
        if (xdist < dist)
        {
            candidate_idx = i;
            dist = xdist;
        }
    }
    if (candidate_idx == -1) return NULL;
    return mMatchCat.data() + candidate_idx;
}

MatchStar*  Matcher::getDetMatchStar(double x, double y)
{
    return getMatchStarInList(x, y, &mMatchDet);
}

void Matcher::TestMyStars(double x1, double y1, QString _ra1,QString _dec1,double x2, double y2, QString _ra2,QString _dec2)
{
    double ra1 = Util::ParseRA(_ra1);
    double dec1 = Util::ParseDEC(_dec1);
    double ra2 = Util::ParseRA(_ra2);
    double dec2 = Util::ParseDEC(_dec2);

    double dist_st = CART_DIST(x1, y1, x2, y2);
    double ang_dist = Util::Adist(ra1, dec1, ra2, dec2);
    qDebug() << "DIST_ST" << dist_st;
    qDebug() << "DIST_ANG" << ang_dist * 3600;

    MatchStar* a1 = getDetMatchStar(x1, y1);
    MatchStar* a2 = getDetMatchStar(x2, y2);
    MatchStar* b1 = getCatMatchStar(ra1, dec1);
    MatchStar* b2 = getCatMatchStar(ra2, dec2);

    if (!a1 || !a2 || !b1 || !b2) return;

    dist_st = CART_DIST(a1->x, a1->y, a2->x, a2->y);
    dist_st = CART_DIST(b1->x, b1->y, b2->x, b2->y);


    Transform2D_ER::ComputeTransform(a1->x, a1->y,a2->x, a2->y, b1->x, b1->y, b2->x, b2->y, &mTransform_ER);
    INFO("    MY TRANSFORM: rot: %0.3f -- %0.3fº; scale: %0.3f \"/pix", mTransform_ER.theta(), 360 - mTransform_ER.theta(), mTransform_ER.getScale() * mParameters.pixel_scale_arcsec);

    ASSERT(a1 && a2 && b1 && b2, "No se encontraron las estrellas para el test");

    int cont = TestMatch(a1, a2, b1, b2, &mMatchDet, &mMatchCat);
    mMatchCount = cont;

    stopCondition();
    SetSuccess(a1, a2, b1, b2, &mMatchDet, &mMatchCat);
    ConstructPlatePointList();
}


bool Matcher::Match(DetectedStar* stars, int stcount, QList<VizierRecord>* catentri)
{
    mProgress = 0;
    INFO("Star align process start with %d sources found and %d catalog entries ", stcount, catentri->count());
    //Construimos las listas
    ConstructLists(stars, stcount, catentri);
    //TestMyStars(143.43, 51.02, "23 46 56", "51 46 33", 55.22, 122.83, "23 46 47", "51 45 19");
    //return true;

    //Comenzamos el macheo de estrellas
    QTime tstart; tstart.start();
    QVector<MatchStar> res = MatchBruteForce(&mMatchDet, &mMatchCat);

    if (matchCount() > 3 && mMatchSuccess)
    {
        VizierRecord b1 = getCatSrcStar(res[2].x, res[2].y);
        VizierRecord b2 = getCatSrcStar(res[3].x, res[3].y);
        INFO("    MATCH COMPUTING TIME : %d ms", tstart.elapsed());
        INFO("    STAR MATCH FOUND A1 X: %0.2f; Y: %0.2f; /  A2 X: %0.2f; Y: %0.2f;", res[0].x, res[0].y,  res[1].x, res[1].y);
        INFO("    STAR MATCH FOUND B1 RA: %s; DEC: %s; / B2 RA: %s; DEC: %s;", PTQ( Util::RA2String(b1.ra())) , PTQ(Util::DEC2String(b1.dec())), PTQ( Util::RA2String(b2.ra())) , PTQ(Util::DEC2String(b2.dec())));
        INFO("    STAR MATCH COUNT: %d RMS: %f", matchCount(), mRMS);
        INFO("    TRANSFORM: rot: %0.3fº; scale: %0.3f \"/pix *** Use this rotation to accelerate search ***",mTransform_ER.theta(), mTransform_ER.getScale() * mParameters.pixel_scale_arcsec);
    }
    else if (res.count() >= 4)
    {
        VizierRecord b1 = getCatSrcStar(res[2].x, res[2].y);
        VizierRecord b2 = getCatSrcStar(res[3].x, res[3].y);
        INFO("    Astrometry failed. Presenting best match");
        INFO("    Computing time : %d ms", tstart.elapsed());
        INFO("    Best match A1 X: %0.2f; Y: %0.2f; /  A2 X: %0.2f; Y: %0.2f;", res[0].x, res[0].y,  res[1].x, res[1].y);
        INFO("    Best match B1 RA: %s; DEC: %s; / B2 RA: %s; DEC: %s;", PTQ( Util::RA2String(b1.ra())) , PTQ(Util::DEC2String(b1.dec())), PTQ( Util::RA2String(b2.ra())) , PTQ(Util::DEC2String(b2.dec())));
        INFO("    Best match count: %d;", matchCount());
        INFO("    Transform: rot: %0.2fº; scale: %0.2f \"/pix",mTransform_ER.theta(), mTransform_ER.getScale() * mParameters.pixel_scale_arcsec);
    }
    else
    {
        INFO("    Astrometry failed. Not enough stars");
    }

 //   mSourceStarsDet.clear();
//    mSourceStarsCat.clear();
    mMatchDetOrd.clear();
    return mMatchSuccess;
}

bool Matcher::MatchPair(DetectedStar* stars, int stcount, QList<VizierRecord>* catentri,
                        float a_x, float a_y, QString a_ra, QString a_dec,
                        float b_x, float b_y, QString b_ra, QString b_dec)
{
    INFO("Star align process start with %d sources found and %d catalog entries ", stcount, catentri->count());
    //Construimos las listas
    ConstructLists(stars, stcount, catentri);
    TestMyStars(a_x, a_y, a_ra, a_dec, b_x, b_y, b_ra, b_dec);
    //return true;

    if (matchCount() > 3)
    {
        INFO("    STAR MATCH COUNT: %d RMS: %f", matchCount(), mRMS);
        INFO("    TRANSFORM: rot: %0.2fº; scale: %0.2f \"/pix",mTransform_ER.theta(), mTransform_ER.getScale() * mParameters.pixel_scale_arcsec);
    }
    else
    {
        INFO("    Best match count: %d;", matchCount());
        INFO("    Transform: rot: %0.2fº; scale: %0.2f \"/pix",mTransform_ER.theta(), mTransform_ER.getScale() * mParameters.pixel_scale_arcsec);

    }

 //   mSourceStarsDet.clear();
//    mSourceStarsCat.clear();
    mMatchDetOrd.clear();
    return mMatchSuccess;
}

/**
 * @brief Matcher::LoadCoincidences Carga todas las coincidencias producidas por la transformación
 * pasada por parámetro en la lista mMatchCoincidences. Elimina coincidencias duplicadas y
 * devuelve el número de coincidencias encontradas
 * @param tr
 */
int Matcher::LoadCoincidences(int dist_tolerance, Transform2D* tr)
{
    double trans_x, trans_y;
    mCoincidences.clear();
    double dist_tolerance_2 = POW2(dist_tolerance);
    for (int i = 0; i < mMatchCat.count(); i++)
    {
        MatchStar *b = mMatchCat.data()+i;
        bool found = false;
        tr->imap(b->x, b->y, &trans_x, &trans_y);

        int px1  = (int)floor(trans_x - dist_tolerance_2);  if (px1 < 0) px1 = 0;
        int px2 = (int)ceil(trans_x + dist_tolerance_2);   if (px2 >= mMatchDetOrd.count()) px2 = mMatchDetOrd.count()-1;

        double best_distance  = 2000;
        MatchStar* a = NULL;

        for (int x=px1; x<=px2 && !found; x++)
        {
            for (int j=0; j<mMatchDetOrd[x]->count() && !found; j++)
            {
                double dist2 = POW2(trans_x - mMatchDetOrd[x]->at(j).x) + POW2(trans_y - mMatchDetOrd[x]->at(j).y);
                if (dist2 < best_distance)
                {
                    a =  mMatchDetOrd[x]->data() + j;
                    best_distance = dist2;
                }
            }
        }

        //Ahora, si hemos encontrado la estrella la añadimos a la lista para calcular los plate constants
        if (a && best_distance < dist_tolerance_2)
        {
            MatchCoincidence mc;
            mc.ra = mSourceStarsCat.at(b->src_id).ra();
            mc.dec = mSourceStarsCat.at(b->src_id).dec();
            mc.a_x = a->x;
            mc.a_y = a->y;
            mc.src_cat = b->src_id;
            mc.src_det = a->src_id;
            mc.b_x = b->x;
            mc.b_y = b->y;
            mc.rejected = false;
            mCoincidences.append(mc);
        }
    }


    //Trabajamos sobre las coincidencias eliminando aquellas que son el mismo punto
    for (int i=0; i<mCoincidences.count(); i++)
    {
        for (int j=0; j<mCoincidences.count(); j++)
        {
            if (i == j) continue;
            if (mCoincidences.at(i).src_cat == mCoincidences.at(j).src_cat
                    || mCoincidences.at(i).src_det == mCoincidences.at(j).src_det)
            {
                mCoincidences.remove(i);
                i = 0; j = 0;
            }
        }
    }

    return mCoincidences.count();
}

void Matcher::ConstructPlatePointList()
{
    int primari_coin = LoadCoincidences(mParameters.dist_tolerance, &mTransform_ER);
    DINFO("**PlateMatchCount1: %d", primari_coin);

    //Segundo cómputo más preciso puede añadir bastantes estrellas en imágenes con baja relación SN
    if (primari_coin >= 4)
    {
        Transform2D_LS ls;
        for (int i=0; i<mCoincidences.count(); i++)
           ls.addCtrlPoint(mCoincidences.at(i).a_x, mCoincidences.at(i).a_y, mCoincidences.at(i).b_x, mCoincidences.at(i).b_y);
        ls.Compute();

        DINFO("**PlateMatchCount2: %d", LoadCoincidences(mParameters.dist_tolerance, &ls));
    }
}
