#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <QStringList>
#include <QVector>
#include "core_global.h"
#include "fitimage.h"
#include "background.h"
#include "progress.h"
#include "core.h"
#include "util.h"

#define IMSTAT_MAX_HISTO_SIZE   4096    //Tamaño del histograma


///
/// \brief Lista de calibración. Ayuda a gestionar una lista de archivos de calibración.
///
class CORESHARED_EXPORT CalibrationList
{

public:
    CalibrationList();
    CalibrationList(const QStringList &list);
    ~CalibrationList();

    void Append(const QStringList &list);
    void Clear();

    inline bool error(){return mError;}
    inline int size(){return mTotalList.length();}
    inline QString value(int i){ return mTotalList.value(i);}
    //Devuelve el índice de un archivo
    int idx(QString name);
    FitHeaderList header(int i);
    FitHeaderList header(QString name);
    static Core::FrameType GetType(const QString &filename);
    QStringList GetAll();
    QStringList GetAll(Core::FrameType type);
    QStringList GetFramesByExpTime(Core::FrameType type, int exptime);
    inline bool Contains(QString filename){ return mTotalList.contains(filename); }
    //Devuelve los distintos tiempos de exposición (sin contar 0) que podemos encontrar en los frames
    QVector<int> ExpTimes();

    //Acceso a los contadores de archivos
    inline int biasCount(){return mBiasList.count();}
    inline int darkCount(){return mDarkList.count();}
    inline int lightCount(){return mLightList.count();}
    inline int flatCount(){return mFlatList.count();}

private:
    QStringList                 mTotalList;
    QStringList                 mLightList;
    QStringList                 mDarkList;
    QStringList                 mFlatList;
    QStringList                 mBiasList;
    QLinkedList<FitHeaderList>  mHeaders;
    bool                        mError;

    int     IndexOf(QString fname);
};




class CORESHARED_EXPORT CalibrationParameters :
        public Parameters
{

public:
    CalibrationParameters();
    ~CalibrationParameters();

    BackgroundParameters     p_bkg;

    Core::PixRejectAlgorithm reject_algorithm;   //Algoritmo para el rechazo de píxeles
    float                    reject_sigma_high;
    float                    reject_sigma_low;
    float                    reject_min;
    float                    reject_max;
    float                    norm_level;         //Parámetro de normalización (<0 indica que no habrá normalizado)
    int                      bit_pp;             //Número de bits por píxel para guardar las imágenes -1 (igual que a la entrada) 8,16,32,-32

    bool                     save_master_flat;
    bool                     save_master_dark;
    bool                     save_master_bias;

    //Corrección de hot pixels. Valores negativos significan no hacer esta corrección
    Core::PixelConectivityType  hot_cold_connectivity;
    float                       hot_pixel_sigma;
    float                       dead_pixel_sigma;
    float                       cosmic_ray_sigma;
    float                       cold_pixel_sigma;

    //Datos que no hay que guardar entre calibraciones
    QString                     output_dir;

    void SetDefaults();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);

};



class CORESHARED_EXPORT ImStat
{
public:
    ImStat(int histosz = IMSTAT_MAX_HISTO_SIZE);
    ~ImStat();

    inline float mean(){return mAverage;}
    inline float stdev(){return mStdev;}
    inline float min(){return mMin;}
    inline float max(){return mMax;}
    inline float median(){return mMedian;}
    inline float mode(){return mMode;}


    /**
     * @brief Devuelve el número de píxeles usados para computar las estadísticas
     * @return Valor entero con el número de píxeles usados para el cómputo
     */
    inline int n(){return mN;}
    inline bool isNull(){return !mN;}
    inline float histolow(){return mHistoLow;}
    inline float histomax(){return mHistoHigh;}
    inline float histoval(int index){return RANGECHANGE(index,0,mHistoSize,mHistoLow,mHistoHigh);}
    inline int   histocount(int index){return mHisto[index];}
    inline int   histsize(){return mHistoSize;}

    static ImStat Compute(float* data,int width, int height);
    static ImStat ComputeRect(float *data,int width, int min_x, int min_y, int max_x, int max_y);

    /**
     * @brief ComputeRect
     * @param data          Matriz de datos de entrada
     * @param width         Ancho de la matriz de datos de entraada
     * @param min_x         Comienzo del rectángulo en X
     * @param min_y         Comienzo del rectángulo de cómputo en Y
     * @param max_x         Fin del rectángulod e cómputo en X (inclusive)
     * @param max_y         Fin del rectángulod e cómputo en Y (inclusive)
     * @param ignore_min    Umbral mínimo bajo el cual se ignorarán los píxeles
     * @param ignore_max    Umbral máximo bajo el que se ignorarán los píxeles
     * @param histo_sz      Tamaño del histograma 0 no se computará histograma
     * @return Estadísticas de los píxeles. Será Imstat::isNull si se computaron 0 píxeles
     */
    static ImStat ComputeRect(float *data, int width, int min_x, int min_y, int max_x, int max_y,
                              float ignore_min, float ignore_max, int histo_sz=IMSTAT_MAX_HISTO_SIZE);




private:
    float   mAverage;                        //Media de los valores procesados
    float   mVariance;                       //Varianza de los valores procesados
    float   mStdev;                          //Desviación estándard de los valores procesados
    float   mMin;                            //Mínimo valor encontrado
    float   mMax;                            //Máximo valor encontrado
    int     mN;                              //Número de píxeles procesados

    float   mMedian;                         //Mediana del histograma
    float   mMode;                           //Moda del histograma

    float   mHistoLow;                       //Valor inferior del histograma
    float   mHistoHigh;                      //Valor superior del histograma
    int     mHistoSize;                      //Tamaño del histograma
    int     mHisto[IMSTAT_MAX_HISTO_SIZE];   //Contador de valores en el histograma

    float   mThresMin;                       //Umbral mínimo usado para el cálculo
    float   mThresMax;                       //Umbral máximo usado para el cálculo

    /**
     * @brief Calcula la mediana de un histograma los datos agrupados mediante semejanza de triángulos
     * @param histo     histograma con los datos
     * @param histo_sz  tamaño del histograma
     * @param min_histo valor real que se corresponde con la posición 0 del histograma
     * @param max_histo valor real que se corresponde con la última posición del histograma
     * @return mediana de los datos
     */
    static float Median(int* histo,int histo_sz, float min_histo, float max_histo);

};

class CORESHARED_EXPORT CalibrationManager
{
public:
    static QString Calibrate(CalibrationList files, CalibrationParameters parameters, ProgressManager *progress_mg);
    static  bool ImCombineAverage(
                   QStringList files,
                   float* result,float** substract,
                   Core::PixRejectAlgorithm rejectalg,
                   Core::BackgroundMethod bkgmethod, int bkg_mesh_size,int bkg_smooth,
                   float sigma_upper,  float sigma_lower, float reject_min, float reject_max,
                   int normalize_level
                    );
    static void Imarith(float* a,float* b, Core::ImarithOp oper, float *output, int width, int height);
    static void NormalizeMinMax(float* data, float level, int width, int height);
    /**
     * @brief NormalizeAverage
     * @param data          Datos a normalizar
     * @param level         Nivel de normalización
     * @param width         Ancho de la imagen
     * @param height        Alto de la imagen
     * @param reject_under  Si se especifica, indica los valores por debajo de los cuales se rechazará el píxel
     * @param reject_to     Indica el valor al que se establecerá el píxel una vez se rechace
     */
    static void NormalizeAverage(float* data, int level,int width, int height,float reject_under = INVALID_FLOAT, float reject_to = INVALID_FLOAT);
    static void AvoidNegativeValues(float* data,int width, int height);
    static void RestoreSourceValues(float* src, float* dst, int width, int height,float min_val=1, float sat_val=60000,float rep_min=0, float rep_max=65535);

    static void RemoveHotAndColdPixels(float* data,Core::PixelConectivityType conec, BitMatrix* to_replace);
    static void DetectHotAndColdPixels(float* data,Core::PixelConectivityType conec,
                                       BitMatrix* bm, float sigma_hot, float sigma_cold);
    static void RemoveCosmicRaysImpacts(float* data,Core::PixelConectivityType conec, float sigma_hot, float sigma_cold,int width, int height);
};

class CORESHARED_EXPORT CalibrationComputer
   : public  BackgroundWorker
{
public:
    CalibrationComputer();
    ~CalibrationComputer();
    bool    doWork(CalibrationParameters* par, CalibrationList* list);


private:
    int     mWidth;
    int     mHeight;
    int     mBitpp;

    float*      m_master_bias_data;
    float*      m_master_flat_data;
    float**     m_master_dark_data;
    int         m_master_dark_count;
    float**     m_substract;
    float*      m_outp;
    BitMatrix** m_hot_pixels;
    BitMatrix*  m_cold_pixels;
    BitMatrix*  m_nan_pixels;
    void    ClearMemory();
    QString calibrate(CalibrationParameters* par, CalibrationList* list);

};











#endif // CALIBRATION_H
