#ifndef PLATECONSTANTS_H
#define PLATECONSTANTS_H

#include "core.h"
#include "util.h"

struct PlateCtrlPoint
{
    int           cat_idx;
    float         x;
    float         y;
    double        ra;
    double        dec;
    double        delta;
    double        deltaRA;
    double        deltaDEC;
};


class CORESHARED_EXPORT PlateConstants
{
public:
    PlateConstants(int img_width=-1, int img_heidht=-1);
    ~PlateConstants();

    //Transformación directa
    inline double a(){return A;}
    inline double b(){return B;}
    inline double c(){return C;}
    inline double d(){return D;}
    inline double e(){return E;}
    inline double f(){return F;}
    //Transformación inversa
    inline double g(){return G;}
    inline double h(){return H;}
    inline double i(){return H;}
    inline double j(){return J;}
    inline double k(){return K;}
    inline double l(){return L;}
    //Parámetros de la placa
    inline double ra(){return RA0 * DEG2HOR;}
    inline double dec(){return DEC0;}
    inline double focal(){return FL;}               //Distancia focal efectiva calculada
    inline double scale(){return SCALE;}            //Escala calculada
    inline double rot(){return ROT;}                //Rotación con respecto al norte
    inline float equinox(){return mEquinox;}

    inline void setA(double a){A = a; mDirty = true;}
    inline void setB(double b){B = b; mDirty = true;}
    inline void setC(double c){C = c; mDirty = true;}
    inline void setD(double d){D = d; mDirty = true;}
    inline void setE(double e){E = e; mDirty = true;}
    inline void setF(double f){F = f; mDirty = true;}
    inline void setRA(double ra){RA0 = ra * HOR2DEG; mDirty = true;}
    inline void setDEC(double dec){DEC0 = dec; mDirty = true;}
    inline void setEquinox(float eq){mEquinox=eq;}

    //Ángulo de posición (en grados) entre dos puntos de la imagen
    double posAngXY(double x1, double y1, double x2, double y2);
    static double posAngRADec(double ra1, double dec1, double ra2, double dec2);

    //Acceso a los puntos de control y a sus estadísticas
    inline double deltaavg(){return mDeltaAvg;}
    inline double deltastdev(){return mDeltaStdev;}
    inline double deltaavgRA(){return mDeltaAvgRA;}
    inline double deltastdevRA(){return mDeltaStdevRA;}
    inline double deltaavgDEC(){return mDeltaAvgDEC;}
    inline double deltastdevDEC(){return mDeltaStdevDEC;}

    inline PlateCtrlPoint* ctrlPoints(int i){return mCtrlPoints.data() + i;}
    inline PlateCtrlPoint* rejectedPoints(int i){return mRejectedPoints.data() + i;}
           void            rejectPoint(int i);
           void            rejectPoint(PlateCtrlPoint* p);
           void            rejectPoint(double x, double y);

    inline QVector<PlateCtrlPoint>* ctrlpoints(){return &mCtrlPoints;}          //Puntos usados para la astrometría
    inline QVector<PlateCtrlPoint>* rejecteds(){return &mRejectedPoints;}       //Puntos rechazados para la astrometría

    inline int ctrlcount(){return mCtrlPoints.count();}
    inline int rejectcount(){return mRejectedPoints.count();}

    /**
     * @brief setCenter Establece el centro de la placa en ascensión recta y declinación
     * @param ra Ascensión recta en horas
     * @param dec Declinación en horas
     */
    inline void setCenter(double ra, double dec){RA0 = ra * HOR2DEG; DEC0 = dec; }

    /**
     * @brief Initialize Inicializa las constantes directamente para hacer el cálculo
     * @param ra
     * @param dec
     * @param a
     * @param b
     * @param c
     * @param d
     * @param e
     * @param f
     * @param focal     Distancia focal efectiva
     * @param scale     Escala en segundos de arco por píxel
     */
    void Initialize(double ra, double dec, double a, double b, double c, double d, double e, double f);

    void map(double xt, double yt,double* ra,double* dec);
    void imap(double ra, double dec,double* xt,double* yt);

    inline bool dirty(){return mDirty;}
    void AddCtrlPoint(float x, float y, double ra, double dec);
    void ClearCtrlPoints();
    void RemoveCtrlPoint(int idx);
    void Compute();

    /**
     * @brief rejected Indica si la estrella situada en la coordenada x,y ha sido rechazada.
     * aplica una distancia cartesiana con diferencia de un píxel.
     * @param x Coordenada de imagen x
     * @param y Coordenada de imagen y
     * @return true si la estrella ha sido rechazada
     */
    bool rejected(float x, float y);        //Indica si una está en la lista de rechazadas

    //Computa las estadísticas de los puntos de control
    void ComputeStats();

    /**
     * @brief Refine Realiza un refiniamiento iterativo en unidades de desviación típica respecto a los residuos
     * @param stdev_s
     * @return Devuelve true si el refinamiento podría continuar aún mas bajo
     */
    bool Refine(double stdev_s);

private:
    //Parámetros de cálculo
    double A;
    double B;
    double C;
    double D;
    double E;
    double F;
    double RA0;             //Internamente tenemos el centro en grados
    double DEC0;            //Internamente tenemos el centro en grados

    double FL;              //Distancia focal efectiva
    double SCALE;           //Escala (En grados por píxel)
    double ROT;             //Rotación (En grados)

    //Constantes para la transformación inversa (post-calculadas)
    double G;
    double H;
    double I;
    double J;
    double K;
    double L;

    //Bordes de la placa
    int    mImageWidth;
    int    mImageHeight;
    int    mMinX;
    int    mMaxX;
    int    mMinY;
    int    mMaxY;

    double mDeltaAvg;
    double mDeltaStdev;
    //Error parcial en ascensión recta y declinación
    double mDeltaAvgRA;
    double mDeltaStdevRA;
    double mDeltaAvgDEC;
    double mDeltaStdevDEC;

    float  mEquinox;

    //Parámetros internos
    bool                    mDirty;
    QVector<PlateCtrlPoint> mCtrlPoints;
    QVector<PlateCtrlPoint> mRejectedPoints;

    /**
     * @brief WorstDelta Devuelve el índice de la entrada que tiene el peor delta (mayor)
     * @return
     */
    int WorstDelta();
    void ComputeInverse();
    void ComputeScaleFLRot();

    static const double RAD;
    static const double ARC;
    static void Lsqfit(FMatrix* A,int N, int M, double* S);


    //Funciónes trigonométricas en grados
    static double CS(double v);
    static double SN(double v);
    static double ASN(double X);
    static double ATN(double X);
    //Convierte coordenadas estándard a coordenadas esféricas ecuatoriales en proyección gnomónica RA--TAN
    static void StdEqu(double RA0, double DEC0, double XX, double YY,double* RA, double* DEC);
    static void EquStd(double RA0, double DEC0, double RA, double DEC, double* XX, double* YY);

};





#endif // PLATECONSTANTS_H
