#ifndef XSTRINGBUILDER_H
#define XSTRINGBUILDER_H

#include "core.h"
#include <QString>

#define     XSTRINGBUILDER_INITIAL_CAPACITY     128

/**
 * @brief The XStringBuilder class Buffer óptimo de acumulación de cadenas con
 * operaciones para todos los tipos de datos.
 */
class CORESHARED_EXPORT XStringBuilder
{
private:
    char        mStatic[XSTRINGBUILDER_INITIAL_CAPACITY+1];
    char*       mPointer;
    int         mCapacity;
    int         mCount;
    quint64     mLastGrowOP;        //Última operación de crecimiento en milisegundos

public:
    inline int count(){return mCount;}
    inline int capacity(){return mCapacity;}

    XStringBuilder();
    XStringBuilder(int capacity);
    XStringBuilder(char *cad);
    XStringBuilder(const QString& cad);
    ~XStringBuilder();
    void clear();
    void setCapacity(int cap);
    void grow(int min_capacity);
    void rtrim();
    void ltrim();
    inline void trim(){rtrim();ltrim();}
    void ungrow();
    void append(const QString& cad);
    void append(const QByteArray& ba);
    void append(const QLatin1String& ba);
    void append(const char *c);
    void append(char ch);
    void append(const QChar& ch);
    void append(float f);
    void append(double f);
    void append(signed int i);
    void append(unsigned int i);
    void append(signed short i);
    void append(unsigned short i);
    void append(signed long i);
    void append(unsigned long i);
    void append(qint64 i);
    void append(quint64 i);
    void append(const void* ptr);
    void appendFormat(const char *fmt, ...);
    inline char* buffer(){return mPointer;}
    inline QString toString(){return mPointer;}

    inline XStringBuilder &operator<<(const QString &s){append(s); return *this;}
    inline XStringBuilder &operator<<(const QByteArray& ba){append(ba); return *this;}
    inline XStringBuilder &operator<<(const QLatin1String& ls){append(ls); return *this;}
    inline XStringBuilder &operator<<(const char *c){append(c); return *this;}
    inline XStringBuilder &operator<<(QChar ch){append(ch); return *this;}
    inline XStringBuilder &operator<<(char ch){append(ch); return *this;}
    inline XStringBuilder &operator<<(float f){append(f); return *this;}
    inline XStringBuilder &operator<<(double f){append(f); return *this;}
    inline XStringBuilder &operator<<(signed int i){append(i); return *this;}
    inline XStringBuilder &operator<<(unsigned int i){append(i); return *this;}
    inline XStringBuilder &operator<<(signed short i){append(i); return *this;}
    inline XStringBuilder &operator<<(unsigned short i){append(i); return *this;}
    inline XStringBuilder &operator<<(signed long i){append(i); return *this;}
    inline XStringBuilder &operator<<(unsigned long i){append(i); return *this;}
    inline XStringBuilder &operator<<(qint64 i){append(i); return *this;}
    inline XStringBuilder &operator<<(quint64 i){append(i); return *this;}
    inline XStringBuilder &operator<<(const void* ptr){append(ptr); return *this;}

};

#endif // XSTRINGBUILDER_H
