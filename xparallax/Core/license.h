#ifndef LICENSE_H
#define LICENSE_H

#include "core_def.h"
#include "core.h"
#include <QDateTime>

#define SECRET_KEY "T1RrNU9EWTFPVFEyTVRNeE5qUTFOems0TXpNeE5qRTFOREkzT1RnME5UUTI="


enum LIC_RESTRICTION { ALL_RESTRICTIONS = -1, NO_RESTRICTION=0, SLOW=1, ULTRA_SLOW=2, SUPER_ULTRA_SLOW=3,
                       ADD_SMALL_ERRORS=4,  ADD_MEDIUM_ERROR=5, ADD_BIG_ERROR=6,
                       SLOW_MODE_BB=7
                     };
class License;

class CORESHARED_EXPORT LicBase
{

public:
    LicBase();
    LicBase(QString& filename, QString rootName);
    static QString   md5(const QString& data);
    static QString   base64Encode(const QString& cad);
    static QString   base64Decode(const QString& cad);
    static License   checkLicense(QString* msg = NULL);

    QString   field(const QString& name);
    QString   field(int idx){return mFields[idx][1];}
    QString   fieldName(int idx){return mFields[idx][0];}
    int       fieldCount(){return mFields.count();}
    QString   hash();
    bool      validSignature();
    QString   secretSig(){return mSecretSig;}
    QString   errorMsg(){return mErrorMsg;}
    void      setSignature(const QString& sig){mSecretSig = sig;}
    void      setField(const QString& name, const QString& value);
    bool      isValid(){return mValid;}
    bool      invalid(){return !mValid;}
    bool      isNull(){return mIsNull;}
    void      setNull(bool null){mIsNull = null;}


protected:
    bool                mValid;
    bool                mIsNull;
    QList<QStringList>  mFields;
    QString             mErrorMsg;
    QString             mSecretSig;


};

class CORESHARED_EXPORT LicID : public LicBase {

public:
    LicID(){;}
    LicID(QString& filename) : LicBase(filename, "MachineId"){ mValid = (validMac() && validHostName() && validUser()); }
    bool validMac();
    bool validHostName();
    bool validUser();
    static LicID search(const QString& path);
};


class CORESHARED_EXPORT License : public LicBase {

public:
    License(){;}
    License(QString& filename) : LicBase(filename, "License"){;}
    static License search(const QString& path, const QString& id);


};


#endif // LICENSE_H
