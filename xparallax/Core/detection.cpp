#include "detection.h"
#include "core.h"
#include "xglobal.h"
#include "ap.h"
#include "interpolation.h"
#include "plateconstants.h"
#include "xstringbuilder.h"
#include <qmath.h>
#include <memory.h>
#include <QVector>
#include <QFileInfo>
#include <QDir>
#include <QtConcurrent>

/********** PARÁMETROS POR CÓDIGO DE DETECCIÓN **********************/
#define DETECT_ON_BORDERS                   true
/********** PARÁMETROS POR DEFECTO DE DETECCIÓN **********************/
#define DEF_DETECT_detect_thres_sigma       3.0f
#define DEF_DETECT_detect_min_radius        2
#define DEF_DETECT_detect_max_radius        35
#define DEF_DETECT_detect_min_area          16
#define DEF_DETECT_detect_ecc_max           0.80f
#define DEF_DETECT_deblend_stars            true
#define DEF_DETECT_deblend_nthres           32
#define DEF_DETECT_deblend_upper_sigma      30.0f
#define DEF_DETECT_deblend_lower_sigma      3.0f
#define DEF_DETECT_deblend_contrast         0.01f
#define DEF_DETECT_detection_flags          (StarDetector::COMPUTE_ISO_CENTER|StarDetector::COMPUTE_WINDOWED_CENTER)

DetectionParameters::DetectionParameters()
{
    SetDefaults();
}

DetectionParameters::~DetectionParameters()
{

}

void DetectionParameters::SetDefaults()
{
    p_bkg.SetDefaults();

    detect_thres_sigma =    DEF_DETECT_detect_thres_sigma;
    detect_min_radius =     DEF_DETECT_detect_min_radius;
    detect_max_radius =     DEF_DETECT_detect_max_radius;
    detect_min_area =       DEF_DETECT_detect_min_area;
    detect_ecc_max =        DEF_DETECT_detect_ecc_max;
    //Deblending
    deblend_stars =         DEF_DETECT_deblend_stars;
    deblend_nthres =        DEF_DETECT_deblend_nthres;
    deblend_upper_sigma =   DEF_DETECT_deblend_upper_sigma;
    deblend_lower_sigma =   DEF_DETECT_deblend_lower_sigma;
    deblend_contrast    =   DEF_DETECT_deblend_contrast;
    flags               =   DEF_DETECT_detection_flags;
}

void DetectionParameters::Save(QSettings* settings,const QString& section)
{
    p_bkg.Save(settings, section);

    SaveFloat(settings, section, "detect_thres_sigma",  detect_thres_sigma);
    SaveInt(settings, section, "detect_min_radius",     detect_min_radius);
    SaveInt(settings, section, "detect_max_radius",     detect_max_radius);
    SaveInt(settings, section, "detect_min_area",       detect_min_area);
    SaveBool(settings, section, "deblend_stars",        deblend_stars);
    SaveFloat(settings, section, "detect_ecc_max",      detect_ecc_max);

    SaveBool(settings, section, "deblend_stars",        deblend_stars);
    SaveInt(settings, section, "deblend_nthres",        deblend_nthres);
    SaveFloat(settings, section, "deblend_upper_sigma", deblend_upper_sigma);
    SaveFloat(settings, section, "deblend_lower_sigma", deblend_lower_sigma);
    SaveFloat(settings, section, "deblend_contrast",    deblend_contrast);

}

void DetectionParameters::Load(QSettings* settings,const QString& section)
{
    p_bkg.Load(settings, section);

    detect_thres_sigma = ReadFloat(settings, section, "detect_thres_sigma", DEF_DETECT_detect_thres_sigma);
    detect_min_radius = ReadInt(settings, section, "detect_min_radius", DEF_DETECT_detect_min_radius);
    detect_max_radius = ReadInt(settings, section, "detect_max_radius", DEF_DETECT_detect_max_radius);
    detect_min_area = ReadInt(settings, section, "detect_min_area", DEF_DETECT_detect_min_area);
    deblend_stars = ReadBool(settings, section, "deblend_stars", DEF_DETECT_deblend_stars);
    detect_ecc_max = ReadFloat(settings, section, "detect_ecc_max", DEF_DETECT_detect_ecc_max);

    deblend_stars = ReadBool(settings, section, "deblend_stars", DEF_DETECT_deblend_stars);
    deblend_nthres = ReadInt(settings, section, "deblend_nthres", DEF_DETECT_deblend_nthres);
    deblend_upper_sigma = ReadFloat(settings, section, "deblend_upper_sigma", DEF_DETECT_deblend_upper_sigma);
    deblend_lower_sigma = ReadFloat(settings, section, "deblend_lower_sigma", DEF_DETECT_deblend_lower_sigma);
    deblend_contrast = ReadFloat(settings, section, "deblend_contrast", DEF_DETECT_deblend_contrast);
}

StarDetector::StarDetector()
 : BackgroundWorker("Detecting stars")
{
    mStars = NULL;
    mSmat = NULL;
    mBackground = NULL;
    mDeblendMat = NULL;
    mStarCapacity = 0;
    mStarCount = 0;
    mDeblendStarCount = 0;
    mImageMask = NULL;
    mExtBackground = NULL;
}

StarDetector::StarDetector(float* data, int width, int height)
 : StarDetector()
{
    setData(data, width, height);
}

StarDetector::StarDetector(float* data,int width,int height,DetectionParameters parameters)
    : StarDetector(data, width, height)
{
    mParameters = parameters;
}

StarDetector::~StarDetector()
{
    Clear();
}

void StarDetector::setData(float *data, int width, int height)
{
    Clear();
    mData = data;
    mWidth = width;
    mHeight = height;
    mBackground = NULL;
    mImageMask = NULL;
    mCancelled = false;
    mStarCapacity = 0;
    mStarCount = 0;
    mDeblendStarCount = 0;
}

void StarDetector::Clear()
{
    if (mStars) delete mStars;
    mStars = NULL;
    if (mSmat) delete mSmat;
    mSmat = NULL;
    if (mDeblendMat) delete mDeblendMat;
    mDeblendMat = NULL;
    mStarCount = 0;
    mStarCapacity = 0;
    for (int i=0; i<mDeblenLevelStars.length();i++)
        delete mDeblenLevelStars.at(i);
    mDeblenLevelStars.clear();
    if (mBackground && (mBackground != mExtBackground))
    {
        mBackground->Clear();
        delete mBackground;        
    }
    mBackground = mExtBackground;
    if (mImageMask) delete mImageMask;
    mImageMask = NULL;
    //Poner todo a cero
    mStarCapacity = 0;
    mStarCount = 0;
    mDeblendStarCount = 0;
}

void StarDetector::setExtBackground(BackgroundMap* bkg)
{
    if (mBackground && (mBackground != mExtBackground))
    {
        mBackground->Clear();
        delete mBackground;
    }
    mBackground = mExtBackground = bkg;
}


DetectedStar* StarDetector::AddStar()
{
    //Siempre dejamos una estrella libre que estará al final y seteada a DetectedStar::IsNull
    if ((mStarCapacity == mStarCount) || !mStars)
    {
        //Redimensiono al doble de la capacidad
        if (mStars) mStarCapacity *= 2;
        else mStarCapacity = MAX_STARTING_STARS_IN_IMAGE;
        DetectedStar* dsnew = new DetectedStar[mStarCapacity];
        //Si ya hubiera estrellas anteriormente copiarlas al nuevo vector
        if (mStars)
        {
            memcpy(dsnew, mStars, mStarCount * sizeof(DetectedStar));
            delete mStars;
        }
        mStars = dsnew;
    }
    mStarCount++;
    mStars[mStarCount-1].mFlags = DetectedStar::DF_NOFLAG;
    return  &mStars[mStarCount-1];
}

DetectedStar* StarDetector::AddDeblendStar()
{
    //Siempre dejamos una estrella libre que estará al final y seteada a DetectedStar::IsNull
    ASSERT(mDeblendStarCount < MAX_DEBLEND_BUFFER,"Deblending list overflow");
    mDeblendStarCount++;
    return  &mDeblendBuffer[mDeblendStarCount-1];
}


DetectedStar* StarDetector::DetectStars(int min_x, int min_y, int max_x, int max_y)
{
    ASSERT(min_x>=0&&min_y>=0&&max_x<mWidth&&max_y<mHeight,"Error, out of image indexes");

    //Configuramos el progreso del fondo y de las estrellas
    int scanline_size = (max_x - min_x);
    OnSetup((max_y - min_y) * scanline_size);
    Clear();

    if (mImageMask) delete mImageMask;
    mImageMask = new BitMatrix(min_x, min_y, max_x-min_x+1, max_y-min_y+1);

    //Primero calcular el fondo común
    if (mBackground == NULL || mBackground != mExtBackground)
    {
        if (mBackground) delete mBackground;
        mBackground = new BackgroundMap();
        mBackground->Setup(mParameters.p_bkg.bkg_method, mWidth, mHeight,
                           mParameters.p_bkg.bkg_mesh_size, mParameters.p_bkg.bkg_smooth,
                           mParameters.p_bkg.bkg_lower_conv, mParameters.p_bkg.bkg_upper_conv
                           );
        //Comenzamos calculando el fondo de la imagen
        mBackground->Compute(mData);
    }

    //Crear la matriz de backtracking recursivo (booleana)
    int matdim = mParameters.detect_max_radius % 2 == 1 ? (mParameters.detect_max_radius + 1) * 2 : mParameters.detect_max_radius * 2;
    if (mSmat) delete mSmat;
    mSmat = new BitMatrix(0, 0, matdim, matdim);
    if (mDeblendMat) delete mDeblendMat;
    mDeblendMat = new BitMatrix(0, 0, matdim, matdim);
    mImageMask->Reset();

    //Comienza la búsqueda de estrellas
    int last_count = 0;
    for (int y=min_y, linecounter = 0; y<max_y; y++, linecounter++)
    {
        for (int x=min_x; x<max_x; x++)
        {
            if (mImageMask->get(x, y)) continue;
            float e_back = mBackground->back(x, y); float e_stdv = mBackground->stdev(x, y);
            float stdev = e_stdv > 0 ? e_stdv : e_stdv;
            //Cuidado!! tenemos que evitar desviaciones estándar muy pequeñas o incongruentes
            if (stdev < e_stdv/1000.0) stdev = e_stdv;

            float thres = (float)(e_back  + stdev * mParameters.detect_thres_sigma);
            DetectedStar* star = ConnectedSearchStarOn(x, y, thres);
            //Calcular parámetros adicionales de la estrella
            if (star && !IsInStar(star->x(), star->y()))
            {
                mImageMask->appendBits(mSmat);
                //Cuidado!!! ¿Hay opción de deblending?
                DetectedStar stsave = *star;
                QList<DetectedStar*>* deblended = RecursiveDeblend(&stsave);
                if (deblended)
                {
                     for (int i=0; i<deblended->count(); i++)
                          NewStarDetected(deblended->at(i));
                }
                else
                  NewStarDetected(&stsave) ;
            }
        }

        OnProgress( (linecounter+1) * scanline_size );
        if (last_count < mStarCount)
        {
            OnMsg(QString("Stars found: %1").arg(mStarCount));
            last_count = mStarCount;
        }
        if (cancelled()) break;
    }

    //TODO: Se va a aplicar un filtro al centroide de segundo orden para relaciones de 2.5x
    if (false)
    for (int i=0; i<mStarCount && mStars; i++)
    {
        if (
             //Elementos de un solo píxel o dos de ancho y mucha diferencia en el momento de segundo órden
             mStars[i].ccx() < 0.01 || mStars[i].ccy() < 0.01 ||
             (MAXVAL(mStars[i].ccx(), mStars[i].ccy()) /  MINVAL(mStars[i].ccx(), mStars[i].ccy()) > 2.5
             )
                //Fuera de los límites de detección
                || mStars[i].radius() < mParameters.detect_min_radius
                || mStars[i].radius() > mParameters.detect_max_radius
              //  || mStars[i].eccentricity() > mParameters.detect_ecc_max
                )
        {
            for (int j=i; j<mStarCount-1; j++)
                mStars[j] = mStars[j+1];
            mStarCount--;
            i--;
        }
    }

    //Provisional.. Quitar estrellas que son lineas verticales
    for (int i=0; i<mStarCount && mStars; i++)
    {
        if (MAXVAL(mStars[i].ccx(), mStars[i].ccy()) /  MINVAL(mStars[i].ccx(), mStars[i].ccy()) > 6)
        {
            for (int j=i; j<mStarCount-1; j++)
                mStars[j] = mStars[j+1];
            mStarCount--;
            i--;
        }
    }

    //Se realiza el cálculo del centro PSF
    if (mParameters.flags & COMPUTE_PSF_CENTER)
    {
        OnMsg("Psf fitting");
        for (int i=0; i<mStarCount && mStars; i++)
        {
            mStars[i].addFlag(DetectedStar::DF_PSF_CENTER);
            ComputePsfCenter(mStars+i);
            ComputePsfSigma(mStars+i);
        }
    }

    //Se realiza el cálculo del flujo de radio
    /*
    if (mParameters.flags & COMPUTE_FLUX_ISO)
    {
        for (int i=0; i<mStarCount && mStars; i++)
        {
            DetectedStar* dsa = mStars+i;
            dsa->mFluxISO = 0;
            IterateAllPointsOfStar(dsa, dsa, ISOFluxFunc);
             dsa->mFluxISO += 0;
        }
    }
    */

    //Indicamos cual es la última estrella por si quedara duda
    if (mStars && mStarCount)
        mStars[mStarCount].mFlags = DetectedStar::DF_ISNULL;
    OnEnd();
    if (logMsgs()) INFO("Star detector: %d stars detected",mStarCount);
    return cancelled() ? NULL : mStars;
}

bool StarDetector::PhotometryFunc(StarDetector* detector, int x, int y, float pdata, void* user_data)
{
    DetectedStar* ds = (DetectedStar*)user_data;
    double real_data = detector->mData[x + y * detector->mWidth];
    double back_data = detector->background()->back(x, y);
    double factor = pdata/real_data;
    ds->mFluxPhoto += (real_data-back_data)*factor;
    return true;
}

struct Search_struct{
    int count_points;
    int min_x;
    int min_y;
    int max_x;
    int max_y;
    BitMatrix* avoid;
};

DetectedStar* StarDetector::ConnectedSearchStarOn(int x,int y, float thres, BitMatrix* avoid_pixels)
{
    //Inicializaoms la estructura de conteo
    Search_struct str;
    memset(&str, 0, sizeof(Search_struct));
    str.max_x = str.min_x = x;
    str.max_y = str.min_y = y;
    str.avoid = avoid_pixels;

    //Iteramos todos los puntos de la estrella
    if (!IterateAllPointsOfStar(x, y, thres, &str, SearchStarFunc)) return NULL;

    //Seteamos parámetros que conocemos hasta el momento de la estrella
    if (str.count_points > mParameters.detect_min_area)
    {
        mAuxStar.mSeed.setX(x);
        mAuxStar.mSeed.setY(y);
        mAuxStar.mIsoCentroid.setX((str.min_x + str.max_x) / 2.0f);
        mAuxStar.mIsoCentroid.setY((str.min_y + str.max_y) / 2.0f);
        mAuxStar.mLimits.setRect(str.min_x, str.min_y, str.max_x - str.min_x + 1, str.max_y - str.min_y + 1);
        mAuxStar.mRadius = (float)sqrt( (MAXVAL(mAuxStar.limits().width(),mAuxStar.limits().height())/2.0) * (MAXVAL(mAuxStar.limits().width(),mAuxStar.limits().height())/2.0));
        mAuxStar.mThres = thres;
        mAuxStar.mPixinStar = str.count_points;
        mAuxStar.mBack = mBackground->back(x, y);
        mAuxStar.mBackStdev = mBackground->stdev(x, y);
        CalculateBaricenterAccurate(&mAuxStar);
        return & mAuxStar;
    }
    return NULL;
}

bool StarDetector::IsInStar(double x, double y)
{
    if (!mStars) return false;
    for (int i=0; i<mStarCount; i++)
    {
        double vx = mStars[i].mIsoCentroid.x() - x;
        double vy = mStars[i].mIsoCentroid.y() - y;
        double dist_2 = vx * vx + vy * vy;
        if (dist_2 < mStars[i].radius() * mStars[i].radius())
             return true;
    }
    return false;
}


bool StarDetector::IterateAllPointsOfStar(int xp, int yp, float thres,void *user_data, bool(*pfunction)(StarDetector* detector,int x,int y, float val,  void* user_data))
{
   float back = mBackground->back(xp, yp);
   int matdim = mParameters.detect_max_radius % 2 == 1 ? (mParameters.detect_max_radius + 1) * 2 : mParameters.detect_max_radius * 2;
   //BitMatrix matrix = new BitMatrix(xp - matdim/2, yp - matdim/2, matdim, matdim);
   mSmat->MoveTo(xp - matdim / 2, yp - matdim / 2);
   mSmat->Reset();
   return DoIterate(ITERATEDIRECTION_START, mSmat, back , thres, xp, yp, user_data, pfunction);
}

bool StarDetector::IterateAllPointsOfStar(DetectedStar* star,void *user_data, bool(*pfunction)(StarDetector* detector, int x,int y, float val, void* user_data))
{
    int matdim = mParameters.detect_max_radius % 2 == 1 ? (mParameters.detect_max_radius + 1) * 2 : mParameters.detect_max_radius * 2;
    //BitMatrix matrix = new BitMatrix(xp - matdim/2, yp - matdim/2, matdim, matdim);
    mSmat->MoveTo(star->mSeed.x() - matdim / 2, star->mSeed.y() - matdim / 2);
    mSmat->Reset();
    return DoIterate(ITERATEDIRECTION_START, mSmat, star->back() , star->thres(), star->mSeed.x(), star->mSeed.y(), user_data, pfunction);
}


bool StarDetector::IterateAllPointsOfCircle(float xc, float yc, float radius,void *user_data, bool(*pfunction)(StarDetector* detector,int x,int y,float val, void* user_data))
{
    int min_x = (int)(xc - radius - 1);
    int max_x = (int)(xc + radius + 1);
    int min_y = (int)(yc - radius - 1);
    int max_y = (int)(yc + radius + 1);

    for (int y = min_y; y<= max_y; y++)
    {
        for (int x = min_x; x<= max_x; x++)
        {
            if (x < 0 || y < 0 || x >= mWidth || y >= mHeight) continue;
            double diff = radius - CART_DIST(xc, yc, x, y);
            //Para valores mayores que 1
            if (diff > 1) pfunction(this, x, y, mData[x + y * mWidth], user_data);
            else if (diff > 0 && diff < 1)
                pfunction(this, x, y, mData[x + y * mWidth] * diff, user_data);
        }
    }
    return true;
}


bool StarDetector::DoIterate(IterateDirection direction, BitMatrix* matrix, float back, float thres, int xp, int yp, void *user_data, bool(*pfunction)(StarDetector* detector, int x,int y,float val, void* user_data))
{
  //Fuera de la imagen no hacemos nada, PARAR EL PROCESO, ALCANZADOS LOS LÍMITES
  if (xp < 0 || yp < 0 || xp >= mWidth || yp >=  mHeight)
      return DETECT_ON_BORDERS;
  //Fuera de la matriz no hacemos nada, PARAR EL PROCESO
  if (xp < matrix->left() || yp < matrix->top() || xp > matrix->right() || yp > matrix->bottom())
      return false;
  //Puntos ya procesados (miro en la matriz de bits) no hago nada, SEGUIR PROCESANDO
  if (matrix->get(xp, yp)) return true;
  //Si el umbral no es bueno no hacemos nada , SEGUIR PROCESANDO
  if (mData[xp + yp *  mWidth] < thres) return true;

  if (direction == ITERATEDIRECTION_START)
  {
      //Operar sobe el punto de inicio
      matrix->set(xp, yp, true);                 //Indicar que ya hemos pasado por este punto
      if (mData[xp + yp *  mWidth] > thres)
      {
          pfunction(this, xp, yp, mData[xp + yp *  mWidth] - back, user_data);  //Procesar el punto, pues pertenece a la estrella
          if (!DoIterate(ITERATEDIRECTION_RIGHT, matrix, back, thres, xp + 1, yp, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_LEFT,  matrix, back, thres, xp - 1, yp, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_UP,    matrix, back, thres, xp, yp - 1, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_DOWN,  matrix, back, thres, xp, yp + 1, user_data, pfunction)) return false;
      }
      else return true;  //No hay nada que hacer con este punto, no pertenece a una estrella, el proceso continua

  }
  //Movernos hacia la derecha hasta topar
  else if (direction == ITERATEDIRECTION_RIGHT)
  {
      int x = xp;
      int max_x = MINVAL(matrix->right(),  mWidth-1);
      for (; x <= max_x; x++)
      {
          if (matrix->get(x, yp)) break;
          matrix->set(x, yp , true);
          if (mData[x + yp *  mWidth] > thres) pfunction(this, x, yp, mData[x + yp *  mWidth] - back, user_data);       //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }

      //Volver hacia atrás lanzando iteraciones arriba y abajo
      for (x-- ; x >= xp; x--)
      {
          if (!DoIterate(ITERATEDIRECTION_UP, matrix, back, thres, x, yp - 1, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_DOWN, matrix, back, thres, x, yp + 1, user_data, pfunction)) return false;
      }
  }
  //Movernos hacia la izquierda hasta topar
  else if (direction == ITERATEDIRECTION_LEFT)
  {
      int x = xp;
      int min_x = MAXVAL(matrix->left(), 0);
      for (; x >= min_x; x--)
      {
          if (matrix->get(x, yp)) break;
          matrix->set(x, yp, true);
          if (mData[x + yp * mWidth] > thres) pfunction(this, x, yp, mData[x + yp  * mWidth] - back , user_data);    //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }

      //Volver hacia atrás lanzando iteraciones arriba y abajo
      for (x++ ; x <= xp; x++)
      {
          if (!DoIterate(ITERATEDIRECTION_UP, matrix, back, thres, x, yp - 1, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_DOWN, matrix, back, thres, x, yp + 1,user_data, pfunction)) return false;
      }
  }
  //Movernos hacia arriba hasta topar con el borde de la matriz o de la imagen
  else if (direction == ITERATEDIRECTION_UP)
  {
      int y = yp;
      int min_y = MAXVAL(matrix->top(), 0);
      for (; y >= min_y; y--)
      {
          if (matrix->get(xp, y)) break;
          matrix->set(xp, y, true);
          if (mData[xp + y * mWidth] > thres) pfunction(this, xp, y, mData[xp + y * mWidth] - back, user_data);    //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }

      //Volver hacia atrás lanzando iteraciones izquierda y derecha
      for ( y++ ; y <= yp; y++)
      {
          if (!DoIterate(ITERATEDIRECTION_RIGHT, matrix, back, thres, xp + 1, y, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_LEFT, matrix, back, thres, xp - 1, y, user_data, pfunction)) return false;
      }
  }
  //Movernos hacia abajo
  else if (direction == ITERATEDIRECTION_DOWN)
  {
      int y = yp;
      int max_y = MINVAL(matrix->bottom(), mHeight-1);
      for (; y <= max_y; y++)
      {
          if (matrix->get(xp, y)) break;
          matrix->set(xp, y, true);
          if (mData[xp + y  * mWidth] > thres) pfunction(this, xp, y, mData[xp + y  * mWidth] - back, user_data);       //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }

      //Volver hacia atrás lanzando iteraciones izquierda y derecha
      for (y--; y >= yp; y--)
      {
          if (!DoIterate(ITERATEDIRECTION_RIGHT, matrix, back, thres, xp + 1, y, user_data, pfunction))  return false;
          if (!DoIterate(ITERATEDIRECTION_LEFT, matrix, back, thres, xp - 1, y, user_data, pfunction)) return false;
      }
  }
  return true;
}

/*
bool StarDetector::DoIterate(IterateDirection direction, BitMatrix* matrix, float back, float thres, int xp, int yp, void *user_data, bool(*pfunction)(StarDetector* detector, int x,int y,float val, void* user_data))
{
  //Fuera de la imagen no hacemos nada, PARAR EL PROCESO, ALCANZADOS LOS LÍMITES
  if (xp < 0 || yp < 0 || xp >= mWidth || yp >=  mHeight) return false;

  //Fuera de la matriz no hacemos nada, PARAR EL PROCESO
  if (xp < matrix->left() || yp < matrix->top() || xp > matrix->right() || yp > matrix->bottom())
      return false;
  //Puntos ya procesados (miro en la matriz de bits) no hago nada, SEGUIR PROCESANDO
  if (matrix->get(xp, yp)) return true;
  //Si el umbral no es bueno no hacemos nada , SEGUIR PROCESANDO
  if (mData[xp + yp *  mWidth] < thres) return true;

  if (direction == ITERATEDIRECTION_START)
  {
      //Operar sobe el punto de inicio
      matrix->set(xp, yp, true);                 //Indicar que ya hemos pasado por este punto
      if (mData[xp + yp *  mWidth] > thres)
      {
          pfunction(this, xp, yp, mData[xp + yp *  mWidth] - back, user_data);  //Procesar el punto, pues pertenece a la estrella
          if (!DoIterate(ITERATEDIRECTION_RIGHT, matrix, back, thres, xp + 1, yp, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_LEFT,  matrix, back, thres, xp - 1, yp, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_UP,    matrix, back, thres, xp, yp - 1, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_DOWN,  matrix, back, thres, xp, yp + 1, user_data, pfunction)) return false;
      }
      else return true;  //No hay nada que hacer con este punto, no pertenece a una estrella, el proceso continua

  }
  //Movernos hacia la derecha hasta topar
  else if (direction == ITERATEDIRECTION_RIGHT)
  {
      int x = xp;
      int max_x = MINVAL(matrix->right(),  mWidth-1);
      for (; x <= max_x; x++)
      {
          if (matrix->get(x, yp)) break;
          matrix->set(x, yp , true);
          if (mData[x + yp *  mWidth] > thres) pfunction(this, x, yp, mData[x + yp *  mWidth] - back, user_data);       //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }
      //Si se ha alcanzado un borde derecho de la matriz hemos alcanzado el límite nuestro cuadrado. No hacemos nada con ella
      //La parada debe ser siempre por estar por debajo del umbral
      if (x > max_x)
          return DETECT_ON_BORDERS ? max_x == mWidth-1 : false;

      //Volver hacia atrás lanzando iteraciones arriba y abajo
      for (x-- ; x >= xp; x--)
      {
          if (!DoIterate(ITERATEDIRECTION_UP, matrix, back, thres, x, yp - 1, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_DOWN, matrix, back, thres, x, yp + 1, user_data, pfunction)) return false;
      }
  }
  //Movernos hacia la izquierda hasta topar
  else if (direction == ITERATEDIRECTION_LEFT)
  {
      int x = xp;
      int min_x = MAXVAL(matrix->left(), 0);
      for (; x >= min_x; x--)
      {
          if (matrix->get(x, yp)) break;
          matrix->set(x, yp, true);
          if (mData[x + yp * mWidth] > thres) pfunction(this, x, yp, mData[x + yp  * mWidth] - back , user_data);    //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }
      //Si se ha alcanzado un borde izquierdo de la matriz hemos alcanzado el límite nuestro cuadrado. No hacemos nada con ella
      //La parada debe ser siempre por estar por debajo del umbral
      if (x < min_x)
          return DETECT_ON_BORDERS ? min_x == 0 : false;

      //Volver hacia atrás lanzando iteraciones arriba y abajo
      for (x++ ; x <= xp; x++)
      {
          if (!DoIterate(ITERATEDIRECTION_UP, matrix, back, thres, x, yp - 1, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_DOWN, matrix, back, thres, x, yp + 1,user_data, pfunction)) return false;
      }
  }
  //Movernos hacia arriba
  else if (direction == ITERATEDIRECTION_UP)
  {
      int y = yp;
      int min_y = MAXVAL(matrix->top(), 0);
      for (; y >= min_y; y--)
      {
          if (matrix->get(xp, y)) break;
          matrix->set(xp, y, true);
          if (mData[xp + y * mWidth] > thres) pfunction(this, xp, y, mData[xp + y * mWidth] - back, user_data);    //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }
      //Si se ha alcanzado un superior de la matriz hemos alcanzado el límite nuestro cuadrado. No hacemos nada con ella
      //La parada debe ser siempre por estar por debajo del umbral
      if (y < min_y)
          return DETECT_ON_BORDERS ? min_y == 0 : false;

      //Volver hacia atrás lanzando iteraciones izquierda y derecha
      for ( y++ ; y <= yp; y++)
      {
          if (!DoIterate(ITERATEDIRECTION_RIGHT, matrix, back, thres, xp + 1, y, user_data, pfunction)) return false;
          if (!DoIterate(ITERATEDIRECTION_LEFT, matrix, back, thres, xp - 1, y, user_data, pfunction)) return false;
      }
  }
  //Movernos hacia abajo
  else if (direction == ITERATEDIRECTION_DOWN)
  {
      int y = yp;
      int max_y = MINVAL(matrix->bottom(), mHeight-1);
      for (; y <= max_y; y++)
      {
          if (matrix->get(xp, y)) break;
          matrix->set(xp, y, true);
          if (mData[xp + y  * mWidth] > thres) pfunction(this, xp, y, mData[xp + y  * mWidth] - back, user_data);       //Procesar el punto si perteneciera
          else break;                                                     //Fin del bucle, hemos encontrado fondo
      }
      //Si se ha alcanzado un inferior de la matriz hemos alcanzado el límite nuestro cuadrado. No hacemos nada con ella
      //La parada debe ser siempre por estar por debajo del umbral
      //if ((y == max_y && !DETECT_ON_BORDERS) || (y != mHeight-1 && DETECT_ON_BORDERS)) return false;
      if (y > max_y)
          return DETECT_ON_BORDERS ? max_y == mHeight-1 : false;


      //Volver hacia atrás lanzando iteraciones izquierda y derecha
      for (y--; y >= yp; y--)
      {
          if (!DoIterate(ITERATEDIRECTION_RIGHT, matrix, back, thres, xp + 1, y, user_data, pfunction))  return false;
          if (!DoIterate(ITERATEDIRECTION_LEFT, matrix, back, thres, xp - 1, y, user_data, pfunction)) return false;
      }
  }
  return true;
}
*/




bool StarDetector::SearchStarFunc(StarDetector* /*detector*/, int x,int y,float /*val*/, void* user_data)
{
    Search_struct* sdata = (Search_struct*)user_data;
    //Si hay que evitar este píxel paramos la iteración
    if (sdata->avoid && sdata->avoid->get(x, y))
        return false;
    sdata->count_points++;
    if (x < sdata->min_x) sdata->min_x = x;
    if (x > sdata->max_x) sdata->max_x = x;
    if (y < sdata->min_y) sdata->min_y = y;
    if (y > sdata->max_y) sdata->max_y = y;
    return true;
}

struct BariCenterStruct
{
    double avg_x;
    double avg_y;
    double avg_x2;
    double avg_y2;
    double avg_xy;
    DetectedStar *rs;
    inline BariCenterStruct(){avg_x = avg_y = avg_x2 = avg_y2 = avg_xy = 0;}
} ;

void StarDetector::CalculateBaricenterAccurate(DetectedStar *star)
{
    BariCenterStruct bar_st;
    bar_st.rs = star;
    star->mPeak = 0;
    star->mPixinStar = 0;
    star->mFluxISO = 0;
    IterateAllPointsOfStar(star, &bar_st, BaricenterFunc);


    //Promediar el baricentro
    star->mIsoCentroid.setX((float)(bar_st.avg_x / star->mFluxISO));
    star->mIsoCentroid.setY((float)(bar_st.avg_y / star->mFluxISO));
    //Promediar los momentos de segundo orden
    star->mCentroid2.setX((float)(bar_st.avg_x2 / star->mFluxISO - star->mIsoCentroid.x() * star->mIsoCentroid.x()));
    star->mCentroid2.setY((float)(bar_st.avg_y2 / star->mFluxISO - star->mIsoCentroid.y() * star->mIsoCentroid.y()));
    star->mXYMoment = (float)(bar_st.avg_xy / star->mFluxISO - star->mIsoCentroid.x() * star->mIsoCentroid.y());
    if (star->mCentroid2.x() < 0)
    {
        star->mCentroid2.setX(0);
        star->addFlag(DetectedStar::DF_BADECC);
    }
    if (star->mCentroid2.y() < 0)
    {
        star->mCentroid2.setY(0);
        star->addFlag(DetectedStar::DF_BADECC);
    }

    //Calcular a, b theta
    double fp1 = (star->mCentroid2.x() + star->mCentroid2.y()) / 2.0;
    double fp2 = (star->mCentroid2.x() - star->mCentroid2.y()) / 2.0;

    star->ma = (float) sqrt(fp1 + sqrt(fp2 * fp2 + POW2(star->mXYMoment)));       //Semieje mayor
    star->mb = (float) sqrt(fp1 - sqrt(fp2 * fp2 + POW2(star->mXYMoment)));       //Semieje menor
    //En algunos momentos el semieje puede incluso llegar a ser 0, mb será nan
    if (isnan(star->mb))  star->mb = 0.00001f;
    else if (isnan(star->ma))  star->ma = 0.00001f;
    if (star->mCentroid2.x() != star->mCentroid2.y())
    {
      star->mTheta = (float) atan(2 * star->mXYMoment / (star->mCentroid2.x() - star->mCentroid2.y())) / 2;
      if (SIGN(star->mTheta) != SIGN(star->mXYMoment)) star->mTheta = -star->mTheta;
    }
    else
    {
      star->ma = star->mb = star->mRadius;
      star->mTheta = 0;
    }
    ASSERT(!isnan(star->ma) && !isnan(star->mb), "Mal cálculo de los parámetros de la elipse");
    //Normalizar theta
    if (star->mTheta < 0) star->mTheta += (float) (M_PI*2);
    else if (star->mTheta > (2*M_PI)) star->mTheta -= (float) (2*M_PI);
}

bool StarDetector::BaricenterFunc(StarDetector*, int x,int y, float pdata, void* user_data)
{
    BariCenterStruct* bs = (BariCenterStruct*)user_data;
    bs->rs->mFluxISO += pdata;
    if (pdata > bs->rs->mPeak)
    {
        bs->rs->mPeak = pdata;
        bs->rs->mPeakCenter = QPointF(x, y);
    }
    double px_val = ((double)x);
    double py_val = ((double)y);

    //Momentos deprimer orden
    bs->avg_x += (double)pdata * px_val;
    bs->avg_y += (double)pdata * py_val;

    //Momentos de segundo orden
    bs->avg_x2 += (double)pdata * px_val * px_val;
    bs->avg_y2 += (double)pdata * py_val * py_val;
    bs->avg_xy += (double)pdata * px_val * py_val;

    bs->rs->mPixinStar++;
    return true;
}



struct WindowedCenterStruct
{
    double w_flux;
    float  radius;
    float c_x;
    float c_y;
    double avg_x;
    double avg_y;
    double avg_x2;
    double avg_y2;
    double avg_xy;
    int numpix;
    double d50;
    DetectedStar *rs;

    double sum_numer_x;
    double sum_numer_y;
    double sum_denom;

    inline WindowedCenterStruct(){avg_x = avg_y = avg_x2 = avg_y2 = avg_xy = w_flux =
                numpix = sum_numer_x = sum_numer_y = sum_denom = 0;}
} ;


float StarDetector::Fwhm(float* /*histo*/,int /*start_idx*/, int /*end_idx*/)
{
    //Comenzamos contando el total de flujo
    /*
    double sum = 0, sum2 = 0;
    double flux_total = 0;
    for (int i=start_idx; i<=end_idx; i++)
    {
        sum += i * histo[i];
        sum2 += histo[i]*histo[i];
    }
*/


    //double midflux = flux_total/2.0;
/*
    //Recorremos de izquierda a derecha hasta que alcancemos midval
    int izq = -1;
    int accu_izq = 0;
    for (; izq<histo_sz && accu_izq+histo[izq+1] < midval; izq++, accu_izq += histo[izq] );
    //Recorremos de derecha a izquierda
    int der = histo_sz;
    int accu_der = n;
    for (; der > izq && der > 1 && accu_der-histo[der-1] > midval; der--, accu_der -= histo[der]);
    //Ahora calculamos la mediana
    double median = (midval - accu_izq)
            / (accu_der - accu_izq)
            * (der - izq);
    return (float) RANGECHANGE(median, 0, histo_sz, min_histo, max_histo);
    */
    return 0;
}


bool StarDetector::WindowedCenterFunc(StarDetector*, int x,int y,float pdata, void* user_data)
{
    WindowedCenterStruct* ws = (WindowedCenterStruct*)user_data;
    double px_val = ((double)x);
    double py_val = ((double)y);

    ws->w_flux += pdata;

    //Momentos deprimer orden
    ws->avg_x += (double)pdata * px_val;
    ws->avg_y += (double)pdata * py_val;

    //Momentos de segundo orden
    ws->avg_x2 += (double)pdata * px_val * px_val;
    ws->avg_y2 += (double)pdata * py_val * py_val;
    ws->avg_xy += (double)pdata * px_val * py_val;


    double s_win = ws->d50 / FWHM_CTE;
    double r_i = sqrt( POW2(x - ws->c_x) + POW2(y - ws->c_y) );
    double w_i = exp( POW2(r_i) / (2*POW2(s_win))  );

    ws->sum_numer_x += w_i * pdata * (x - ws->c_x);
    ws->sum_numer_y += w_i * pdata * (y - ws->c_y);
    ws->sum_denom += w_i * pdata;


    ws->numpix++;
    return true;
}

void StarDetector::IterateWindowCenter(WindowedCenterStruct* ws)
{
    ws->sum_numer_x =  ws->sum_numer_y =  ws->sum_denom = 0;
    ws->avg_x = ws->avg_y = ws->avg_x2 = ws->avg_y2 = 0;
    IterateAllPointsOfCircle(ws->c_x, ws->c_y, ws->radius, ws, WindowedCenterFunc);

    //Calculamos las constantes que se añadiran a "c_x" y "c_y"
    double ct_x = 2 * ws->sum_numer_x / ws->sum_denom;
    double ct_y = 2 * ws->sum_numer_y / ws->sum_denom;

    //Aplicamos los cálculos
    ws->avg_x /= ws->w_flux;
    ws->avg_y /= ws->w_flux;

    //Desviación típica en x e y
    ws->avg_x2 = ws->avg_x2 / ws->w_flux - POW2(ws->avg_x);
    ws->avg_y2 = ws->avg_y2 / ws->w_flux - POW2(ws->avg_y);

    //Fwhm
    ws->d50 = (ws->avg_x2 + ws->avg_y2) / 2.0 * FWHM_CTE;

    //Corrijo la x y la y
    ws->c_x += ct_x;
    ws->c_y += ct_y;
}

void StarDetector::CalculateWindowedCenterAccurate(DetectedStar* star)
{
    star->mWCentroid.setX(star->x());
    star->mWCentroid.setY(star->y());

    WindowedCenterStruct ws;
    ws.radius = star->radius();
    ws.c_x = star->x();
    ws.c_y = star->y();
    ws.d50 = (star->mCentroid2.x() + star->mCentroid2.y())/2.0 * FWHM_CTE;

    //Inicializamos los valores del centro de ventana
    double last_distance = 10e20;
    QPointF last_center(star->x(), star->y());
    int max_iter = 8;
    int i =0;
    do
    {
        IterateWindowCenter(&ws);
        double dist_centers = CART_DIST(last_center.x(), last_center.y(), ws.c_x, ws.c_y);

        if (dist_centers > last_distance)
            break;
        last_center.setX(ws.c_x);
        last_center.setY(ws.c_y);
        if (isnan(last_center.x()) || isnan(last_center.y()) || isinf(last_center.y()) || isinf(last_center.y())
                || CART_DIST(last_center.x(), last_center.y(), star->x(), star->y()) > 1 )
            break;
        if (dist_centers < 0.0002)
            break;
        i++;
    }
    while (i < max_iter);

    if (isnan(last_center.x()) || isnan(last_center.y()) || isinf(last_center.y()) || isinf(last_center.y())
            || CART_DIST(last_center.x(), last_center.y(), star->x(), star->y()) > 1)
    {
        star->mWCentroid.setX(star->x());
        star->mWCentroid.setY(star->y());
    }
    else
    {
        star->mWCentroid.setX(last_center.x());
        star->mWCentroid.setY(last_center.y());
    }
}

DetectedStar* StarDetector::NewStarDetected(DetectedStar* st)
{
  // ASSERT(!isnan(st->mb)&&!isnan(st->ma),"Se ha detectado un semieje erroneo");
   DetectedStar* newstar = AddStar();
   *newstar = *st;
   if (mParameters.flags & COMPUTE_WINDOWED_CENTER)
   {
       CalculateWindowedCenterAccurate(newstar);
       newstar->addFlag(DetectedStar::DF_WINDOWED_CENTER);
   }

   newstar->removeFlag((DetectedStar::DetectedFlag)(DetectedStar::DF_PREFERRED_CENTER_PEAK|DetectedStar::DF_PREFERRED_CENTER_WINDOWED
                       |DetectedStar::DF_PREFERRED_CENTER_PSF|DetectedStar::DF_PREFERRED_CENTER_ISO));
   if (mParameters.flags & DEFAULT_CENTER_WINDOWED)
       newstar->addFlag(DetectedStar::DF_PREFERRED_CENTER_WINDOWED);
   else if (mParameters.flags & DEFAULT_CENTER_PEAK)
       newstar->addFlag(DetectedStar::DF_PREFERRED_CENTER_PEAK);
   else if (mParameters.flags & DEFAULT_CENTER_PSF)
       newstar->addFlag(DetectedStar::DF_PREFERRED_CENTER_PSF);
   else
       newstar->addFlag(DetectedStar::DF_PREFERRED_CENTER_ISO);
   return newstar;
}

bool StarDetector::StarInList(QList<DetectedStar*>* list, DetectedStar* star)
{
    for (int i=0; i<list->count(); i++)
    {
        DetectedStar* st = list->at(i);
        if (CART_DIST(st->x(), st->y(), star->x(), star->y()) < 5) return true;
    }
    return false;
}

bool StarDetector::StarInList(DetectedStar* star, DetectedStar* list, int sz)
{
    for (int i=0; i<sz; i++)
    {
        if (CART_DIST(list[i].x(), list[i].y(), star->x(), star->y()) < 5) return true;
    }
    return false;
}

void StarDetector::AllocDeblendLevels(int deblend_nthres)
{
    while (mDeblenLevelStars.count() < deblend_nthres)
    {
        QList<DetectedStar*>* list = new QList<DetectedStar*>();
        mDeblenLevelStars.append(list);
    }
    //LImpiamos todas las listas
    //Recorremos los niveles de deblending
    for (int i=0; i< deblend_nthres; i++)
        mDeblenLevelStars.at(i)->clear();    
    //Indicamos que no hay reservada memoria para estrellas de deblending ni matrices
    mDeblendStarCount = 0;
}

QList<DetectedStar*>* StarDetector::RecursiveDeblend(DetectedStar* star)
{
    //Para empezar, es inutil desdoblar estrellas si no tienen minArea*2 al menos
    if (star->pixinstar() < mParameters.detect_min_area*2) return NULL;

    //Calculamos los niveles de deblending
    float back = mBackground->back(star->sx(), star->sy());
    float stdev = mBackground->stdev(star->sx(), star->sy());

    float min_thres = back + stdev * mParameters.deblend_lower_sigma;
    float max_thres =  back + stdev * mParameters.deblend_upper_sigma;
    float step = (max_thres - min_thres) / mParameters.deblend_nthres;
    min_thres += step;

    AllocDeblendLevels(mParameters.deblend_nthres);
    QList<DetectedStar*>* deb = RecursiveDeblend(0, min_thres, step, star->fluxISO(),
                                                star->x(), star->y(), star->radius());

    //Condición de salida si no ha sido satisfactorio
    if (!deb || (deb->count() <= 1)) return NULL;

    //Ahora comprobamos que ninguna de las estrellas desdobladas esté en la lista de existentes
    for (int i=0; i<deb->count(); i++)
    {
        if (IsInStar(deb->at(i)->x(), deb->at(i)->y()))
        {
            deb->removeAt(i);
            i--;
        }
    }

    //Devolvemos la lista de estrllas desdobladas
    return (deb->count() <= 1) ? NULL : deb;
}

QList<DetectedStar*>* StarDetector::RecursiveDeblend(int level,float thres,float step,
                                                     float last_flux, float star_x, float star_y, float radius)
{
    //Condición de parada de la recursividad
    if (level >= mParameters.deblend_nthres) return NULL;
    //Cuidado, si el área disponible es menor que el doble del mínimo área, es imposible que encontremos una pareja
    if (mParameters.detect_min_area*2 > M_PI*POW2(radius)) return NULL;
    //No podemos intentar desdoblar si tenemos la lista de deblending llena
    if (mDeblendStarCount >= MAX_DEBLEND_BUFFER) return NULL;

    //Obtenemos una lista con la que operar
    QList<DetectedStar*>* mylist = mDeblenLevelStars.at(level);
    mylist->clear();

    int min_x = (int)ceil(star_x - radius);
    if (min_x < 0) min_x = 0;
    int max_x = (int)floor(star_x + radius);
    if (max_x >= mWidth) max_x = mWidth-1;
    int min_y = (int)ceil(star_y - radius);
    if (min_y < 0) min_y = 0;
    int max_y = (int)floor(star_y + radius);
    if (max_y >= mHeight) max_y = mHeight-1;

    //Inicializamos la matriz de píxeles
    mDeblendMat->MoveTo(min_x-1, min_y-1);
    mDeblendMat->Reset();

    float total_flux = 0;

    for (int y = min_y; y<= max_y; y++)
    {
        for (int x = min_x; x<= max_x; x++)
        {
            //Tengo cuidado, si no se pueden meter mas estrellas de deblending
            if (mDeblendStarCount >= MAX_DEBLEND_BUFFER) continue;
            //Solamente lo intentamos si estamos dentro del radio y el píxel no está ocupado por otra estrella
            if (!mDeblendMat->get(x,y) && (CART_DIST(x, y, star_x, star_y) < radius))
            {
                 DetectedStar* st = ConnectedSearchStarOn(x, y, thres, mDeblendMat);
                 if (!st) continue;
                 //Comprobación de flujo
                 if (st->fluxISO() < mParameters.deblend_contrast*last_flux) continue;
                 //Comprobamos que no esté ya en la lista de deblending
                 bool in_list = false;
                 for (int j=0; j<mylist->count() && !in_list; j++)
                     in_list = CART_DIST(mylist->at(j)->x(), mylist->at(j)->y(), st->x(), st->y()) < 2;
                 if (in_list) continue;
                 //Finalmente la estrella ha sido detectada, agregamos la estrella a la lista
                 DetectedStar* newstar = AddDeblendStar();
                 *newstar = *st;
                 newstar->addFlag(DetectedStar::DF_DEBLENDED);
                 mylist->append(newstar);
                 //Marcamos los píxeles de la estrella como ocupados
                 mDeblendMat->appendBits(mSmat);
                 total_flux += newstar->fluxISO();
            }
        }
    }

    //Llegados aquí podemos no tener ninguna estrella, si es así salimos
    if (!mylist->count()) return NULL;

    //Ahora para cada estrella seguimos intentando hacer el deblending
    for (int i=0; i<mylist->count(); i++)
    {
        DetectedStar* to_deblend = mylist->at(i);
        QList<DetectedStar*>* child_list = RecursiveDeblend(level+1, thres+step, step, total_flux,
                                                            to_deblend->x(), to_deblend->y(), to_deblend->radius());
        if (!child_list) continue;
        //Si solamente hay una estrella continuamos, no hay nada que hacer.
        //Esto no debería ocurrir, NUNCA se debe devolver una sola estrella
        if (child_list->count() == 1) continue;
        ASSERT(child_list->count() > 1, "Error en 'detection', la lista devuelta debería tener más de 1 estrella");


        //Si hay mas de una es nuestro turno, sutituimos la estrella actual por las otras
        int num_inserted = 0;
        mylist->removeAt(i);
        for (int j=0; j<child_list->count(); j++)
        {
            if (!StarInList(mylist, child_list->at(j)))
            {
                mylist->insert(i,child_list->at(j));
                num_inserted++;
            }
        }
        if (num_inserted > 1) i += num_inserted - 1;
    }
    if (mylist->count() == 1)
    {
        mylist->clear();
        RemoveLastDeblendStar();
        return NULL;
    }
    return mylist;
}

QList<DetectedStar*>* StarDetector::DeblendStar(DetectedStar* star)
{
    //Para empezar, es inutil desdoblar estrellas si no tienen minArea*2 al menos
    if (star->pixinstar() < mParameters.detect_min_area*2) return NULL;


    //Límites de la zona afectada por la estrella.
    QRect limits = star->limits();    
    mDeblendMat->MoveTo(mSmat->left(), mSmat->top());

    //Calculamos los niveles de deblending
    float back = mBackground->back(star->sx(), star->sy());
    float stdev = mBackground->stdev(star->sx(), star->sy());

    float min_thres = back + stdev * mParameters.deblend_lower_sigma;
    float max_thres =  back + stdev * mParameters.deblend_upper_sigma;
    float step = (max_thres - min_thres) / mParameters.deblend_nthres;
    min_thres += step;


    AllocDeblendLevels(mParameters.deblend_nthres);

    //Todos los píxeles detectados se irán añadiendo a la matriz  mDeblendMat
    mDeblendMat->Reset();

    //Nivel de deblending candidato con sus estrellas
    QList<DetectedStar*>* candidate_level = NULL;
    int candidate_stars = 1;

    //Realizamos el cómputo de las estrellas en cada nivel de deblending
    float last_flux = star->fluxISO();
    float thres = min_thres;
    for (int i=0; i<mParameters.deblend_nthres; i++, thres += step )
    {
        QList<DetectedStar*>* cur_level = mDeblenLevelStars.at(i);
        mDeblendMat->Reset();
        float total_flux = 0;   //Acumulamos el flujo de todas las estrellas del nivel
        int stars_in_level = 0;
        for (int y = limits.top(); y<=limits.bottom(); y++)
        {
            for (int x = limits.left(); x<=limits.right(); x++)
            {
                if (mDeblendMat->get(x, y)) continue;
                DetectedStar* st = ConnectedSearchStarOn(x, y, thres);
                //Condición de contraste. Tiene que tener un mínimo de flujo la detectada
                if (st && (st->fluxISO() < mParameters.deblend_contrast*last_flux)) continue;
                //Calcular parámetros adicionales de la estrella
                bool exists = false;

                for (int j=0; st  && !exists && j<cur_level->count(); j++)
                {
                    double dist = CART_DIST(cur_level->at(j)->x(),cur_level->at(j)->y(),
                                            st->x(), st->y());
                    //if (dist < cur_level->at(j)->radius() + st->radius()) exists = true;
                    if (dist < mParameters.detect_min_radius * 2) exists = true;
                }
                if (!exists && st && (!mDeblendMat->intersects(mSmat)) )
                {
                    DetectedStar* newstar = AddDeblendStar();
                    *newstar = *st;
                    newstar->addFlag(DetectedStar::DF_DEBLENDED);
                    //Añadimos a este nivel la nueva estrella y la nueva matriz
                    mDeblenLevelStars.at(i)->append(newstar);
                    mDeblendMat->appendBits(mSmat);
                    total_flux += newstar->fluxISO();
                    stars_in_level++;                    
                }
            }
        }
        //Si en el nivel actual no se ha detectado ninguna estrella mal asunto, hemos bajado mucho el thres.
        //Paramos porque no va a haber más
        if (!mDeblenLevelStars.at(i)->count()) break;
        last_flux = total_flux;
        //Comprobamos si hay que cambiar de nivel candidato
        if (stars_in_level > candidate_stars)
        {
            candidate_level = mDeblenLevelStars.at(i);
            candidate_stars = stars_in_level;
        }
    }

    //Devolvemos el nivel con más estrellas. A igual número de estrellas devolvemos el menor nivel
    return candidate_level;
}

//Función PSF: f(x,y) = c0 + c1*x + c2*y + c3*x^2 + c4*y^2 = LN I(x,y)+ LN I(0). Donde I(0) es el máximo de flujo para un píxel
void StarDetector::psf_func(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, void* /*ptr*/)
{
    // f(x,y) = c0 + c1*x + c2*y + c3*x^2 + c4*y^2
    double x = _x[0];
    double y = _x[1];
    func = c[0] + c[1]*x + c[2]*y + c[3]*POW2(x) + c[4]*POW2(y);
}

//Función PSF: f(x,y) = c0 + c1*x + c2*y + c3*x^2 + c4*y^2 = LN I(x,y)+ LN I(0). Donde I(0) es el máximo de flujo para un píxel
void StarDetector::sigma_func(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, void* data)
{
    //Extraer los valores de data
    double x0 = ((double*)data)[0];
    double y0 = ((double*)data)[1];
    //Obtener el valor de la función
    double x = _x[0];
    double y = _x[1];
    double r = CART_DIST(x, y, x0, y0);
    func = c[0]*POW2(r);
}

//Derivada en C0,1,2,3,4 de la función PSF, acelera el cálculo iterativo del ajuste
void StarDetector::psf_grad(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, alglib::real_1d_array &grad, void* /*ptr*/)
{
    double x = _x[0];
    double y = _x[1];
    func = c[0] + c[1]*x + c[2]*y + c[3]*POW2(x) + c[4]*POW2(y);
    //Derivada en c0
    grad[0] = 1;
    //Derivada en c1
    grad[1] = x;
    //Derivada en c2
    grad[2] = y;
    //Derivada en c3
    grad[3] = POW2(x);
    //Derivada en c4
    grad[4] = POW2(y);
}

void StarDetector::ComputePsfCenter(DetectedStar* ds)
{
    //De momento no podemos hacer mucho con esto
    ds->mPsfCenter = (mParameters.flags&COMPUTE_WINDOWED_CENTER) ? ds->mWCentroid : ds->mIsoCentroid;

    //Parámetros de la gausiana
    int min_x = ds->x() - ds->radius();
    if (min_x < 0) min_x = 0;
    if (min_x >= mWidth) min_x = mWidth-1;
    int max_x = ds->x() + ds->radius();
    if (max_x < 0) max_x = 0;
    if (max_x >= mWidth) max_x = mWidth-1;
    int min_y = ds->y() - ds->radius();
    if (min_y < 0) min_y = 0;
    if (min_y >= mHeight) min_y = mHeight-1;
    int max_y = ds->y() + ds->radius();
    if (max_y < 0) max_y = 0;
    if (max_y >= mHeight) max_y = mHeight-1;

    int icont=0;
    for (int y=min_y; y<=max_y; y++)
    {
        for (int x=min_x; x<=max_x; x++)
        {
            if (CART_DIST(ds->mIsoCentroid.x(), ds->mIsoCentroid.y(), x, y)>ds->radius()) continue;
            //NO añadimos píxeles saturados
            if (mData[x + y*mWidth] > 65000) continue;
            float flux_data = mData[x + y*mWidth]-ds->back();
            if (flux_data < 0.01) continue;
            icont++;
        }
    }

    alglib::real_2d_array src_points;   //Coordenadas
    alglib::real_1d_array src_obsdata;  //Valores observacionales
    alglib::real_1d_array c;            //Soluciones del ajuste c0, c1, c2, c3, c4
    src_points.setlength(icont, 2);
    src_obsdata.setlength(icont);

    if (icont < 4) return;

    int i=0;
    for (int y=min_y; y<=max_y; y++)
    {
        for (int x=min_x; x<=max_x; x++)
        {
            if (CART_DIST(ds->mIsoCentroid.x(), ds->mIsoCentroid.y(), x, y)>ds->radius()) continue;
            if (mData[x + y*mWidth] > 65000) continue;
            float flux_data = mData[x + y*mWidth]-ds->back();
            if (flux_data < 0.01) continue;
            src_points[i][0] = x;
            src_points[i][1] = y;
            src_obsdata[i] = log(flux_data) - log(ds->mPeak);
            i++;
        }
    }

    //Inicializar Cj con los valores aproximados
    double c0 = log(ds->mPeak);
    double c3 = 0.5;
    double c4 = 0.5;
    double c1 = -ds->mPsfCenter.x();
    double c2 = -ds->mPsfCenter.y();

    c.setlength(5); c[0] = c0; c[1] = c1; c[2] = c2; c[3] = c3; c[4] = c4;

    double epsf = 0;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitstate state;
    alglib::lsfitreport rep;
    double diffstep = 0.0001;

    alglib::lsfitcreatef(src_points, src_obsdata, c, diffstep, state); //Inicialización del ajuste
    alglib::lsfitsetcond(state, epsf, epsx, maxits);                   //Condiciones de parada del ajuste
    alglib::lsfitfit(state, psf_func, psf_grad);                       //Cálculo de los coeficientes
    alglib::lsfitresults(state, info, c, rep);                         //Cuidado!! hay que comprobar si hubo convergencia

    c0 = c[0];
    c1 = c[1];
    c2 = c[2];
    c3 = c[3];
    c4 = c[4];

    double psf_x = -c1 / (2*c3);
    double psf_y = -c2 / (2*c4);
    //INFO("%5.9f\t%5.9f\t--\t%5.9f\t%5.9f",ds->mWCentroid.x(), ds->mWCentroid.y(), psf_x, psf_y);
    ds->mPsfCenter = QPointF(psf_x, psf_y);

    //Una salida de rango muy grande puede indicar un mal cálculo de la psf. Cogemos el Wcenter si está disponible. Si no, el iso
    if (CART_DIST(ds->mIsoCentroid.x(), ds->mIsoCentroid.y(), psf_x, psf_y) < 3)
        ds->mPsfCenter = QPointF(psf_x, psf_y);
}

void StarDetector::ComputePsfSigma(DetectedStar* ds)
{
    //Parámetros de la gausiana
    int min_x = ds->x() - ds->radius();
    if (min_x < 0) min_x = 0;
    if (min_x >= mWidth) min_x = mWidth-1;
    int max_x = ds->x() + ds->radius();
    if (max_x < 0) max_x = 0;
    if (max_x >= mWidth) max_x = mWidth-1;
    int min_y = ds->y() - ds->radius();
    if (min_y < 0) min_y = 0;
    if (min_y >= mHeight) min_y = mHeight-1;
    int max_y = ds->y() + ds->radius();
    if (max_y < 0) max_y = 0;
    if (max_y >= mHeight) max_y = mHeight-1;

    int icont=0;
    for (int y=min_y; y<=max_y; y++)
    {
        for (int x=min_x; x<=max_x; x++)
        {
            if (CART_DIST(ds->mPsfCenter.x(), ds->mPsfCenter.y(), x, y)>ds->radius()) continue;
            //NO añadimos píxeles saturados
            if (mData[x + y*mWidth] > 65000) continue;
            float flux_data = mData[x + y*mWidth]-ds->back();
            if (flux_data < 0.01) continue;
            icont++;
        }
    }

    alglib::real_2d_array src_points;   //Coordenadas
    alglib::real_1d_array src_obsdata;  //Valores observacionales
    alglib::real_1d_array c;            //Soluciones del ajuste c0, c1, c2, c3, c4
    src_points.setlength(icont, 2);
    src_obsdata.setlength(icont);

    if (icont < 4) return;
    int i=0;
    for (int y=min_y; y<=max_y; y++)
    {
        for (int x=min_x; x<=max_x; x++)
        {
            if (CART_DIST(ds->mPsfCenter.x(), ds->mPsfCenter.y(), x, y)>ds->radius()) continue;
            if (mData[x + y*mWidth] > 65000) continue;
            float flux_data = mData[x + y*mWidth]-ds->back();
            if (flux_data < 0.01) continue;
            src_points[i][0] = x;
            src_points[i][1] = y;
            src_obsdata[i] = log(flux_data) - log(ds->mPeak);
            i++;
        }
    }

    //Inicializar Cj con los valores aproximados
    double c0 = -0.03;
    c.setlength(1); c[0] = c0;

    double epsf = 0;
    double epsx = 0.000001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitstate state;
    alglib::lsfitreport rep;
    double diffstep = 0.0001;

    double data[] = {ds->mPsfCenter.x(), ds->mPsfCenter.y()};
    alglib::lsfitcreatef(src_points, src_obsdata, c, diffstep, state); //Inicialización del ajuste
    alglib::lsfitsetcond(state, epsf, epsx, maxits);                   //Condiciones de parada del ajuste
    alglib::lsfitfit(state, sigma_func, NULL, data);                       //Cálculo de los coeficientes
    alglib::lsfitresults(state, info, c, rep);                         //Cuidado!! hay que comprobar si hubo convergencia

    double b = c[0];
    double sigma = sqrt(-1/(2*b));
    double fwhm = sigma* FWHM_CTE;
    ds->mFWHM = fwhm;
}

void StarDetector::ComputePsfAccurate(DetectedStar* ds)
{
    //De momento no podemos hacer mucho con esto
    ds->mPsfCenter = (mParameters.flags&COMPUTE_WINDOWED_CENTER) ? ds->mWCentroid : ds->mIsoCentroid;
    double sigma = FLOAT_MAX;
    //Número de píxeles con mayor desviación típica en el residuo
    int over_3_sigma = 0;
    int under_3_sigma = 9;
    double kappa = 2;
    BitMatrix bm(FROUND(ds->mPsfCenter.x()-ds->radius()*4), FROUND(ds->mPsfCenter.y()-ds->radius()*4), ds->radius()*8, ds->radius()*8);

    //Inicializar C con los valores aproximados
    double c0 = log(ds->mPeak);
    double c1 = - ds->mPsfCenter.x();
    double c2 = - ds->mPsfCenter.y();
    double c3 = 0.5;
    double c4 = 0.5;
    QPoint highest_pt;

    do
    {
        //Parámetros de la gausiana
        int min_x = ds->mPsfCenter.x() - ds->radius();
        if (min_x < 0) min_x = 0;
        if (min_x >= mWidth) min_x = mWidth-1;
        int max_x = ds->mPsfCenter.x() + ds->radius();
        if (max_x < 0) max_x = 0;
        if (max_x >= mWidth) max_x = mWidth-1;
        int min_y = ds->mPsfCenter.y() - ds->radius();
        if (min_y < 0) min_y = 0;
        if (min_y >= mHeight) min_y = mHeight-1;
        int max_y = ds->mPsfCenter.y() + ds->radius();
        if (max_y < 0) max_y = 0;
        if (max_y >= mHeight) max_y = mHeight-1;

        //Calcular el número de píxd
        int icont=0;
        for (int y=min_y; y<=max_y; y++)
        {
            for (int x=min_x; x<=max_x; x++)
            {
                if (bm.get(x, y)) continue;
                if (CART_DIST(ds->mPsfCenter.x(), ds->mPsfCenter.y(), x, y)>ds->radius()) continue;
                //NO añadimos píxeles saturados
                if (mData[x + y*mWidth] > 65000) continue;
                float flux_data = mData[x + y*mWidth]-ds->back();
                double e_obs = log(flux_data)+log(ds->mPeak);
                double e_theo = c0 + c1*x + c2*y + c3*POW2(x) + c4*POW2(y);
                if (fabs(e_theo-e_obs) > kappa*sigma) continue;
                if (flux_data < 0.01) continue;
                icont++;
            }
        }

        alglib::real_2d_array src_points;   //Coordenadas
        alglib::real_1d_array src_obsdata;  //Valores observacionales
        alglib::real_1d_array c;            //Soluciones del ajuste c0, c1, c2, c3, c4
        src_points.setlength(icont, 2);
        src_obsdata.setlength(icont);

        if (icont < 4) return;

        int i=0;
        for (int y=min_y; y<=max_y; y++)
        {
            for (int x=min_x; x<=max_x; x++)
            {
                if (bm.get(x, y)) continue;
                if (CART_DIST(ds->mPsfCenter.x(), ds->mPsfCenter.y(), x, y)>ds->radius()) continue;
                if (mData[x + y*mWidth] > 65000) continue;
                float flux_data = mData[x + y*mWidth]-ds->back();
                if (flux_data < 0.01) continue;
                double e_obs = log(flux_data) + log(ds->mPeak);
                double e_theo = c0 + c1*x + c2*y + c3*POW2(x) + c4*POW2(y);
                if (fabs(e_theo-e_obs) > kappa*sigma) continue;

                src_points[i][0] = x;
                src_points[i][1] = y;
                src_obsdata[i] = e_obs;
                i++;
            }
        }

        c.setlength(5); c[0] = c0; c[1] = c1; c[2] = c2; c[3] = c3; c[4] = c4;

        double epsf = 0;
        double epsx = 0.0000001;
        alglib::ae_int_t maxits = 0;
        alglib::ae_int_t info;
        alglib::lsfitstate state;
        alglib::lsfitreport rep;
        double diffstep = 0.000001;

        alglib::lsfitcreatef(src_points, src_obsdata, c, diffstep, state); //Inicialización del ajuste
        alglib::lsfitsetcond(state, epsf, epsx, maxits);                   //Condiciones de parada del ajuste
        alglib::lsfitfit(state, psf_func, psf_grad);                       //Cálculo de los coeficientes
        alglib::lsfitresults(state, info, c, rep);                         //Cuidado!! hay que comprobar si hubo convergencia

        c0 = c[0];
        c1 = c[1];
        c2 = c[2];
        c3 = c[3];
        c4 = c[4];

        double psf_x = -c1 / (2*c3);
        double psf_y = -c2 / (2*c4);
        //INFO("%5.9f\t%5.9f\t--\t%5.9f\t%5.9f",ds->mWCentroid.x(), ds->mWCentroid.y(), psf_x, psf_y);
        ds->mPsfCenter = QPointF(psf_x, psf_y);

        //Contar el número de píxeles que están sobre sigma
        double avg_next = 0;
        i=0;
        for (int y=min_y; y<=max_y; y++)
        {
            for (int x=min_x; x<=max_x; x++)
            {
                if (bm.get(x, y)) continue;
                if (CART_DIST(ds->mPsfCenter.x(), ds->mPsfCenter.y(), x, y)>ds->radius()) continue;
                if (mData[x + y*mWidth] > 65000) continue;
                float flux_data = mData[x + y*mWidth]-ds->back();
                if (flux_data < 0.01) continue;
                double e_obs = log(flux_data) + log(ds->mPeak);
                double e_theo = c0 + c1*x + c2*y + c3*POW2(x) + c4*POW2(y);
                if (fabs(e_theo-e_obs) > kappa*sigma) continue;
                avg_next += fabs(e_obs-e_theo);
                i++;
            }
        }
        avg_next /= i;
        double stdev_next = 0;
        for (int y=min_y; y<=max_y; y++)
        {
            for (int x=min_x; x<=max_x; x++)
            {
                if (bm.get(x, y)) continue;
                if (CART_DIST(ds->mPsfCenter.x(), ds->mPsfCenter.y(), x, y)>ds->radius()) continue;
                if (mData[x + y*mWidth] > 65000) continue;
                float flux_data = mData[x + y*mWidth]-ds->back();
                if (flux_data < 0.01) continue;
                double e_obs = log(flux_data) + log(ds->mPeak);
                double e_theo = c0 + c1*x + c2*y + c3*POW2(x) + c4*POW2(y);
                if (fabs(e_theo-e_obs) > kappa*sigma) continue;
                stdev_next += POW2(avg_next - fabs(e_obs-e_theo));
            }
        }
        sigma = sqrt(stdev_next / i);
        //Contar númerod e puntos desviados en mas de 3sigma
        over_3_sigma = 0;
        under_3_sigma = 0;
        float highest_val = -100;
        for (int y=min_y; y<=max_y; y++)
        {
            for (int x=min_x; x<=max_x; x++)
            {
                if (bm.get(x, y)) continue;
                if (CART_DIST(ds->mPsfCenter.x(), ds->mPsfCenter.y(), x, y)>ds->radius()) continue;
                if (mData[x + y*mWidth] > 65000) continue;
                float flux_data = mData[x + y*mWidth]-ds->back();
                if (flux_data < 0.01) continue;
                double e_obs = log(flux_data) + log(ds->mPeak);
                double e_theo = c0 + c1*x + c2*y + c3*POW2(x) + c4*POW2(y);
                if (fabs(e_theo-e_obs) > kappa*sigma && fabs(e_theo-e_obs) > highest_val)
                {
                    highest_val = fabs(e_theo-e_obs);
                    highest_pt.setX(x);
                    highest_pt.setY(y);
                    over_3_sigma++;
                }
                else under_3_sigma++;
            }
        }
        if (highest_pt.x() && highest_pt.y() && highest_val > 0)
            bm.set(highest_pt.x(), highest_pt.y(), true);
    }
    while (over_3_sigma >= 1 && under_3_sigma > 9 && highest_pt.x() && highest_pt.y());

    QPointF acc_cnt = (mParameters.flags&COMPUTE_WINDOWED_CENTER) ? ds->mWCentroid : ds->mIsoCentroid;
    //Una salida de rango muy grande puede indicar un mal cálculo de la psf. Cogemos el Wcenter si está disponible. Si no, el iso
    if (CART_DIST(acc_cnt.x(), acc_cnt.y(), ds->mPsfCenter.x(), ds->mPsfCenter.y()) > 3)
        ds->mPsfCenter = acc_cnt;
}



DetectedStar* StarDetector::TakeMemControlOfStars()
{
    DetectedStar* ds = mStars;
    mStars = NULL;
    return ds;
}


StarDetectorParallel::StarDetectorParallel(float* data, int width, int height, DetectionParameters parameters)
    : StarDetector(data, width, height, parameters)
{
    mSize = 6;
}


StarDetectorParallel::~StarDetectorParallel()
{
    if (mBackground) delete mBackground;
    mBackground = NULL;
    for (int i=0; i<mDetectors.count(); i++)
        delete mDetectors.at(i);
    mDetectors.clear();
}

DetectedStar* StarDetectorParallel::DetectStars(int min_x, int min_y, int max_x, int max_y)
{
    int sz = mSize;

    mDetRows = sz;
    mDetCols = sz;
    mDetcount = mDetRows*mDetCols;
    OnSetup(mDetcount);

    int iwidth = max_x-min_x+1;
    int iheight = max_y-min_y+1;

    int min = MINVAL(iwidth, iheight);
    int max = MAXVAL(iwidth, iheight);
    int tw = (int)ceil(min / (float)mSize);
    while (mSize > 1 && tw < mParameters.detect_max_radius*3)
    {
        sz--;
        tw = (int)ceil(min / (float)mSize);
    }

    int tessel_width = (int)ceil(iwidth / (float)mDetCols);
    int tessel_height = (int)ceil(iheight / (float)mDetRows);

    //Primero calcular el fondo común
    if (mBackground) delete mBackground;
    mBackground = new BackgroundMap();
    mBackground->Setup(mParameters.p_bkg.bkg_method, mWidth, mHeight,
                       mParameters.p_bkg.bkg_mesh_size, mParameters.p_bkg.bkg_smooth,
                       mParameters.p_bkg.bkg_lower_conv, mParameters.p_bkg.bkg_upper_conv
                       );
    mBackground->Compute(mData);

    //Crear los detectores
    for (int i=0; i<mDetcount; i++)
    {
        StarDetector* det = new StarDetector(mData, mWidth, mHeight, mParameters);
        det->setLog(false);
        mDetectors.append(det);
    }

    int finished = 0;
    int total_stars = 0;
    QMutex mutex;

    //Optimización de paralelismo. Unas 5 veces mas rápido
    QtConcurrent::blockingMap(mDetectors, [&](StarDetector* det) -> void
    {
        //Calcular el índice
        int idx = 0; for (idx=0; idx<mDetectors.count() && det != mDetectors.at(idx); idx++);
        det->setExtBackground(mBackground);

        int row = idx / mDetCols;
        int col = idx % mDetCols;

        int min_x = col * tessel_width;
        int max_x = min_x + tessel_width;
        int min_y = row * tessel_height;
        int max_y = min_y+ tessel_height;
        if (max_x >= mWidth) max_x = mWidth-1;
        if (max_y >= mHeight) max_y = mHeight-1;

        det->DetectStars(min_x, min_y, max_x, max_y);

        mutex.lock();
        finished++;
        total_stars += det->count();
        for (int s=0; s<det->count(); s++)
        {
            DetectedStar* star = det->stars()+s;
            if (!StarInList(star, mStars , mStarCount))
            {
                DetectedStar* star_new = AddStar();
                *star_new = *star;
            }
        }
        OnProgress(finished);
        mutex.unlock();
    });


    //Indicamos cual es la última estrella por si quedara duda
    if (mStars && mStarCount)
        mStars[mStarCount].mFlags = DetectedStar::DF_ISNULL;
    OnEnd();
    if (logMsgs()) INFO("Star detector: %d stars detected",mStarCount);
    return cancelled() ? NULL : mStars;
}
















