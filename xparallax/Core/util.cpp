#include "util.h"
#include "core.h"
#include "xglobal.h"
#include "novas.h"
#include "xstringbuilder.h"
#include <qmath.h>
#include <QStringList>
#include <QRegularExpression>
#include <QThread>
#include <QSemaphore>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QDir>
#include <QColor>
#include <QUrlQuery>


#define UT_IS_BLANK(ch)	((ch==' ')||(ch=='\t')||(ch=='\r')||(ch=='\n'))
#define MEM_CACHE_MAX_REQUESTS      5   //Número máximo de solicitudes webreader almacenadas en memoria
#define MAX_DAYS_DISK_CACHE         5
#define MAX_CACHE_FILES           100   //Archivos máximos de cache
#define REGEX_CONTENT_LENGTH      "^Content\\-Length[^\\d]*(<?CL>\\d+)[^\\d]*$"


const char Util::RA_FMT[] = "%02d %02d %05.2f";
const char Util::DE_FMT[] = "%c%02d %02d %04.1f";
const char Util::LON_FMT[] = "%02d %02d %02.0f %c";
const char Util::LAT_FMT[] = "%02d %02d %04.1f %c";
QMutex                      WebReader::sCacheMutex;
QHash<QString,QStringList>  WebReader::sCache;

Util::Util()
{
}

double Util::ParseRA(const QString& ra)
{
    QString cad = const_cast<QString&>(ra).toLower()
                .replace(':',' ')
                .replace('h', ' ')
                .replace('m', ' ')
                .replace("s", "");

    QStringList slist = cad.trimmed().split(" ",QString::SkipEmptyParts);
    if (slist.count() != 3) return INVALID_FLOAT;
    bool ok = true;
    //Parseamos por individual cada uno de los elementos
    double hour = slist.at(0).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    double min = slist.at(1).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    double sec = slist.at(2).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    //Comprobamos los límites de cada número
    if (hour > 24 || hour < 0 || min >= 60 || min < 0 || sec >= 60 || sec < 0) return INVALID_FLOAT;
    //Componemos la ascensión recta final
    return hour + min/60.0 + sec/3600.0;
}

double Util::ParseDEC(const QString& dec)
{
    QString cad = const_cast<QString&>(dec).toLower()
                .replace("º"," ")
                .replace("°"," ")
                .replace("\t"," ").replace("\r"," ").replace("\n"," ")
                .replace('\'', ' ')
                .replace(':',' ')
                .replace('"',' ')
                .replace('d', ' ').replace('h', ' ').replace('m', ' ').replace("s", "");
    while (cad.contains("  ")) cad = cad.replace("  "," ");
    cad = cad.replace("- ","-").replace("+ ","+");
    QStringList slist = cad.trimmed().split(" ",QString::SkipEmptyParts);
    if (slist.count() != 3) return INVALID_FLOAT;
    bool ok = true, sign = false;
    //Parseamos por individual cada uno de los elementos
    double deg = slist.at(0).toDouble(&ok);
    sign = (deg < 0 || slist.at(0).startsWith('-'));
    deg = fabs(deg);
    if (!ok) return INVALID_FLOAT;
    double min = slist.at(1).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    double sec = slist.at(2).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    //Comprobamos los límites de cada número
    if (deg > 90 || deg < -90 || min >= 60 || min < 0 || sec >= 60 || sec < 0) return INVALID_FLOAT;
    //Componemos la ascensión recta final
    return (deg + min/60.0 + sec/3600.0)*(sign?-1:1);
}

QVector<double>  Util::ParseRADEC(const QString& radec, bool get_dec_pos)
{
     QVector<double> rd(2);
     rd[0] = rd[1] = INVALID_FLOAT;
     QString cad = const_cast<QString&>(radec).toLower()
                 .replace("º"," ").replace("°"," ")
                 .replace('d', ' ').replace('h', ' ').replace('m', ' ').replace("s", "")
                 .replace("\t"," ").replace("\r"," ").replace("\n"," ")
                 .replace('\'', ' ').replace('/', ' ')
                 .replace('`', ' ').replace('"',' ').replace('\'',' ')
                 .replace(':',' ').trimmed();
     while (cad.contains("  ")) cad = cad.replace("  "," ");
     QStringList sl = cad.split(' ');
     if (sl.length() != 6) return rd;
     QString ra_str = QString("%1 %2 %3").arg(sl[0], sl[1], sl[2]);
     QString dec_str = QString("%1 %2 %3").arg(sl[3], sl[4], sl[5]);
     rd[0] = ParseRA(ra_str);
     rd[1] = ParseDEC(dec_str);
     if (IS_INVALIDF(rd[0]) || IS_INVALIDF(rd[1]))
         rd[0] = rd[1] = INVALID_FLOAT;
     //Intentar sacar el número de decimales obtenidos en ascensión recta y dclinación
     if (get_dec_pos)
     {
         int deci_ra = 0;
         int idx_pra = sl[2].indexOf('.');
         if (idx_pra > 0) deci_ra = sl[2].length()-idx_pra-1;
         if (deci_ra < 0) deci_ra = 0;
         int deci_dec = 0;
         int idx_pdec = sl[5].indexOf('.');
         if (idx_pdec > 0) deci_dec = sl[5].length()-idx_pdec-1;
         if (deci_dec < 0) deci_dec = 0;
         rd.append(deci_ra);
         rd.append(deci_dec);
     }
     return rd;
}

QVector<double> Util::ParseXY(const QString& xy)
{
    QVector<double> rd(0);
    QString cad = const_cast<QString&>(xy).toLower()
                .replace(";"," ")
                .replace(':',' ').trimmed();
    while (cad.contains("  ")) cad = cad.replace("  "," ");
    QStringList sl = cad.split(' ');
    if (sl.length() != 2) return rd;
    bool ok;
    double d1 =  sl.at(0).toDouble(&ok); if (!ok) return rd;
    double d2 = sl.at(1).toDouble(&ok); if (!ok) return rd;
    rd.append(d1); rd.append(d2);
    return rd;
}

double Util::ParseLat(const QString& ra)
{
    QString cad = const_cast<QString&>(ra).toLower()
                .replace('h', ' ')
                .replace('d', ' ')
                .replace('m', ' ')
                .replace("º"," ")
                .replace("°"," ")
                .replace("\t"," ")
                .replace('\'', ' ')
                .replace('`', ' ')
                .replace(':',' ').trimmed().toUpper();
    bool negative = cad.endsWith('S');
    cad = cad.replace('S',' ').replace('N',' ');
    QStringList slist = cad.split(" ",QString::SkipEmptyParts);
    if (slist.count() != 3) return INVALID_FLOAT;
    bool ok = true;
    //Parseamos por individual cada uno de los elementos
    double deg = slist.at(0).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    double min = slist.at(1).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    double sec = slist.at(2).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    //Comprobamos los límites de cada número
    if (deg > 90 || deg < -90 || min >= 60 || min < 0 || sec >= 60 || sec < 0) return INVALID_FLOAT;
    //Componemos la ascensión recta final
    return  (deg + min/60.0 + sec/3600.0)*(negative?-1:1);
}

double Util::ParseLon(const QString& ra)
{
    QString cad = const_cast<QString&>(ra).toLower()
                .replace('h', ' ')
                .replace('d', ' ')
                .replace('m', ' ')
                .replace("s", "")
                .replace("º"," ")
                .replace("°"," ")
                .replace('\'', ' ')
                .replace(':',' ').trimmed().toUpper();
    bool negative = cad.endsWith('W') || cad.endsWith('O');
    cad = cad.replace('W',' ').replace('O',' ').replace('E',' ');
    QStringList slist = cad.split(" ",QString::SkipEmptyParts);
    if (slist.count() != 3) return INVALID_FLOAT;
    bool ok = true;
    //Parseamos por individual cada uno de los elementos
    double deg = slist.at(0).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    double min = slist.at(1).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    double sec = slist.at(2).toDouble(&ok);
    if (!ok) return INVALID_FLOAT;
    //Comprobamos los límites de cada número
    if (deg > 90 || deg < -90 || min >= 60 || min < 0 || sec >= 60 || sec < 0) return INVALID_FLOAT;
    double total = (deg + min/60.0 + sec/3600.0);
    if (total > 180)
    {
        total = 360-total;
        negative = true;
    }
    //Componemos la ascensión recta final
    return  (deg + min/60.0 + sec/3600.0)*(negative?-1:1);
}

double Util::ParseEquinox(const QString& equinox)
{
    QString ep = equinox.trimmed().toUpper();
    if (ep == "J1991.25") return 1991.25;
    if (ep == "J2000" || ep == "J2000.0" || ep == "J2000.00") return 2000.0;
    if (ep == "B1875" || ep == "B1875.0" || ep == "B1875.00") return 1875.0;
    if (ep == "B1950" || ep == "B1950.0" || ep == "J1950.00") return 1950.0;
    if (ep == "B1900" || ep == "B1900.0" || ep == "B1900.00") return 1900.0;
    if (ep == "J2015" || ep == "J2015.0" || ep == "J2015.00") return 2015.0;
    if (ep.startsWith('J')) ep = ep.mid(1);
    bool ok;
    double epd = ep.toDouble(&ok);
    if (IS_INVALIDF(epd) || !ok || epd < 1900) return INVALID_FLOAT;
    return epd;
}

double Util::JulianDate(int year, int month, int day)
{
    return Novas::julian_date(year, month, day, 0);
}

double Util::JulianDate(const QDate& dtt)
{
    return Novas::julian_date(dtt.year(), dtt.month(), dtt.day(), 0);
}

double Util::JulianDate(const QDateTime& dtt)
{
    double hour_frac = dtt.time().hour() + dtt.time().minute()/60.0 + dtt.time().second()/3600.0 + dtt.time().msec()/3600000.0;
    return Novas::julian_date(dtt.date().year(), dtt.date().month(), dtt.date().day(), hour_frac);
}

double Util::JulianDate(int year, int month, int day, int hour, int minute, int second, int millisecond)
{
    double hour_frac = hour +minute/60.0 + second/3600.0 + millisecond/3600000.0;
    return Novas::julian_date(year, month, day, hour_frac);
}

QDate Util::JD2Date(double jd)
{
    short year, month, day;
    double hour;
    Novas::cal_date(jd, &year, &month, &day, &hour);
    return QDate(year, month, day);
}

QDateTime Util::JD2DateTime(double jd)
{
    short year, month, day;
    double d_hour;
    Novas::cal_date(jd, &year, &month, &day, &d_hour);
    int h = (int)floor(d_hour);
    d_hour -= h;
    int m = (int)floor(d_hour*60);
    d_hour -= m/60.0;
    int s = (int)floor(d_hour*3600);
    d_hour -= s/3600.0;
    int ms = (int)floor(d_hour*3600000);
    return QDateTime(QDate(year, month, day), QTime(h, m, s, ms));
}

QString Util::Epoch2String(double jd_epoch)
{
    if (COMPF2(jd_epoch, JD_J2000) == 0) return "J2000";
    if (COMPF2(jd_epoch, JD_HIPPARCOS) == 0) return "J1991.25";
    if (COMPF2(jd_epoch, JD_J1950) == 0) return "J1950";
    if (COMPF2(jd_epoch, JD_J1900) == 0) return "J1900";
    short year, month, day; double hour;
    Novas::cal_date(jd_epoch, &year, &month, &day, &hour);
    if (month == 1 && day == 1 && hour == 12) return QString("").sprintf("J%04d", year);
    //A partir de aquí no se ha probado y debería probarse
    int hr = (int)floor(hour);
    int min = (int)floor((hour - hr)*60);
    int secs = (int)floor((hour - hr - min/60.0)*3600);
    int msec = (int)floor((hour - hr - min/60.0 - secs/3600.0)*3600000);
    QDateTime dt1(QDate(year, month, day), QTime(hr, min, secs, msec));       //Día del año
    QDateTime start_of_year(QDate(year, 1, 1), QTime(00, 00, 00));      //Inicio del año
    quint64 total_ms_year = (quint64)(JULIAN_YEAR * 24 * 3600 * 1000);
    quint64 total_ms_date = dt1.toMSecsSinceEpoch() - start_of_year.toMSecsSinceEpoch();
    double year_frac = (double)total_ms_date/(double)total_ms_year;
    return QString("").sprintf("J%04d.%d", year, (int)round(year_frac*100));
}

QString Util::RA2String(double ra)
{
    return RA2String(ra,RA_FMT);
}

QString Util::DEC2String(double dec)
{
    return DEC2String(dec,DE_FMT);
}

QString Util::RA2String(double ra,const char* fmt)
{
    char buffer[32];
    //Descomponer en horas minutos y segundos
    int hor = (int)floor(ra);
    ra = (ra-hor)*60.0;
    int min = (int)floor(ra);
    ra = (ra - min) * 60.0;
    double secs = ra;
    //Nos hemos quedado solamente con segundos
    if (secs >= 59.99) { min += 1; secs = 0; }
    if (min >= 60) { hor += 1; min = 0; }
    //Aplicar el fomato y salir
    snprintf(buffer, 32, fmt, hor, min, secs);
    return buffer;
}

QString  Util::RA2String(double ra,int deci_places)
{
    char fmt[32];
    strcpy(fmt,"%02d %02d ");
    sprintf(fmt+strlen(fmt),"%%0%d.%df", deci_places+3, deci_places);
    return RA2String(ra, fmt);
}


QString Util::DEC2String(double dec,const char* fmt)
{
  char buffer[32];
  bool sgn = false;
  if (dec < 0)
  {
      dec = -dec;
      sgn = true;
  }
  //Descomponer en grados y segundos
  int deg = (int)floor(dec);
  dec = (dec - deg) * 60.0;
  int min = (int)floor(dec);
  dec = (dec - min) * 60.0;
  double secs = dec;
  //Post comprobaciones de que no guardamos 60 en lugares que no corresponde
  if (secs >= 59.95)
  {
      secs = 0;
      min++;
  }
  if (min >= 60)
  {
      min = 0;
      deg++;
  }
  //Aplicar el fomato y salir
  snprintf(buffer, 32 , fmt, sgn?'-':'+', deg, min, secs);
  return buffer;
}

QString Util::DEC2String(double dec, int deci_places)
{
    char fmt[32];
    strcpy(fmt,"%c%02d %02d ");
    sprintf(fmt+strlen(fmt), deci_places ? "%%0%d.%df" : "%%02.0f", deci_places+3, deci_places);
    return DEC2String(dec, fmt);
}

QString Util::LON2tring(double lon)
{
    return LON2tring(lon, LON_FMT);
}

QString Util::LAT2String(double lat)
{
    return LAT2String(lat, LAT_FMT);
}

QString Util::LON2tring(double lon,const char* fmt)
{
    char buffer[32];
    bool sgn = false;
    if (lon < 0)
    {
        lon = -lon;
        sgn = true;
    }
    //Descomponer en grados y segundos
    int deg = (int)floor(lon);
    lon = (lon - deg) * 60.0;
    int min = (int)floor(lon);
    lon = (lon - min) * 60.0;
    double secs = lon;
    //Post comprobaciones de que no guardamos 60 en lugares que no corresponde
    if (secs >= 59.95)
    {
        secs = 0;
        min++;
    }
    if (min >= 60)
    {
        min = 0;
        deg++;
    }
    //Aplicar el fomato y salir
    snprintf(buffer, 32 , fmt, deg, min, secs, sgn?'W':'E');
    return buffer;
}

QString Util::LAT2String(double lat,const char* fmt)
{
    char buffer[32];
    bool sgn = false;
    if (lat < 0)
    {
        lat = -lat;
        sgn = true;
    }
    //Descomponer en grados y segundos
    int deg = (int)floor(lat);
    lat = (lat - deg) * 60.0;
    int min = (int)floor(lat);
    lat = (lat - min) * 60.0;
    double secs = lat;
    //Post comprobaciones de que no guardamos 60 en lugares que no corresponde
    if (secs >= 59.95)
    {
        secs = 0;
        min++;
    }
    if (min >= 60)
    {
        min = 0;
        deg++;
    }
    //Aplicar el fomato y salir
    snprintf(buffer, 32 , fmt, deg, min, secs, sgn?'S':'N');
    return buffer;
}

QString Util::Date2MPCString(QDateTime dt,int dec_places)
{
    QString dtd =  dt.toString("yyyy MM");
    double dayfrac = dt.date().day() + dt.time().hour() / 24.0 + dt.time().minute() / (24.0*60) + dt.time().second() / (24.0*3600)
            + dt.time().msec() / (24.0*3600*1000);
    char pfmt[16];
    sprintf(pfmt,"%%s %%%02d.%df",dec_places+3,dec_places);
    return QString("").sprintf(pfmt/*"%s %08.5f"*/, PTQ(dtd), dayfrac);
}

QString Util::Equinox2String(double equinox)
{
    if (COMPF2(equinox,2000.0f) == 0) return "J2000";
    if (COMPF2(equinox,1875.0f) == 0) return "B1875";
    if (COMPF2(equinox,1950.0f) == 0) return "B1950";
    if (COMPF2(equinox,1991.25f) == 0) return "J1991.25";   //Hipparcos
    if (COMPF2(equinox,1900.0f) == 0) return "B1900";
    if (COMPF2(equinox,2015.0f) == 0) return "J2015";
    if (IS_INVALIDF(equinox)) return "";
    return QString::number(equinox, 'f' , 2);
}

QColor Util::WebColor(QString cl)
{
    double r,g,b;
    if (cl.startsWith("#")) cl = cl.mid(1);
    else if (cl.startsWith("0x", Qt::CaseInsensitive)) cl = cl.mid(2);
    QString cr = cl.left(2);
    QString cg = cl.mid(2,2);
    QString cb = cl.right(2);
    r = cr.toInt(NULL, 16) / 255.0;
    g = cg.toInt(NULL, 16) / 255.0;
    b = cb.toInt(NULL, 16) / 255.0;
    return QColor::fromRgbF(r, g, b, 1);
}

char* Util::TrimRight(char* cad)
{
    for (int i=(int)strlen(cad)-1;i>=0 && UT_IS_BLANK(cad[i]);i--) cad[i] = 0;
    return cad;
}

char* Util::TrimLeft(char* cad)
{
    int pos = 0;
    for (; cad[pos] && (UT_IS_BLANK(cad[pos])); pos++);
    strcpy(cad, cad+pos);
    return cad;
}

char* Util::Trim(char* cad)
{
    Util::TrimRight(cad);
    Util::TrimLeft(cad);
    return cad;
}

//Hay una fórmula mejorada en wikipedia "Great circle distance"
double Util::Adist(double ra1, double dec1, double ra2, double dec2)
{
    ra1 *= HOR2RAD;
    dec1 *= DEG2RAD;
    ra2 *= HOR2RAD;
    dec2 *= DEG2RAD;

    double ro = 0, d1, d2, d3, d4, a1;
    double dal=fabs(ra2-ra1);
    if(dal>M_PI) dal=2.0*M_PI-dal;
    double ddel=fabs(dec2-dec1);

    //Para posiciones razonablemente alejadas (> 1asec) se usará la distancia sobre la esfera
    if(dal > 2.8e-4 || ddel > 2.8e-4)
    {
        a1=cos(dal);
        d1=cos(dec1);
        d2=cos(dec2);
        d3=sin(dec1);
        d4=sin(dec2);
        ro = acos(d3 * d4 + d1 * d2 * a1);
    }
    //Para posiciones muy cercanas se usará la distancia cartesiana mucho mas rápida
    else
    {
        d1=cos((dec1+dec2)/2.0);
        ro=sqrt(d1*d1*dal*dal+ddel*ddel);
    }
    return (ro+0.000000001) * RAD2DEG;
}

double Util::PosAngle(double ra1, double dec1, double ra2, double dec2)
{
    double diff_ra = ra2 - ra1;
    double diff_dec = fabs(dec2 - dec1);
    double cosc2 = cos(dec2 * DEG2RAD);
    double num = cosc2 *  sin(diff_ra * HOR2RAD);
    double den = sin(dec2 * DEG2RAD) * cos(dec1 * DEG2RAD)
         - cosc2 * sin(dec1 * DEG2RAD) * cos(diff_ra * HOR2RAD);
    double angle = (diff_ra==0.0 && diff_dec==0.0)?-1000:(den==0.0)?90.0:atan2(num, den)*RAD2DEG;
    if (angle < 0) angle += 360.0;
    return angle;
}

void Util::LinearRegression(QPointF* data, double* weight, int datalen, double* a, double* b)
{
    /* Cálculo de la media en X y media en Y */
    double x_med = 0, y_med = 0;
    double total_weight = 0.0;
    for (int i=0; i<datalen; i++)
    {
        if (weight)
        {
            x_med += data[i].x() * weight[i];
            y_med += data[i].y() * weight[i];
            total_weight += weight[i];
        }
        else
        {
            x_med += data[i].x();
            y_med += data[i].y();
        }
    }
    x_med /= weight ? total_weight : datalen;
    y_med /= weight ? total_weight : datalen;

    /* SUM((xi - xmedia)(yi - ymedia)) */
    double sum_xim_yim = 0;
    /* SUM((xi - xmedia)^2) */
    double sum_xim_2 = 0;
    for (int i=0; i<datalen; i++)
    {
        sum_xim_yim += (data[i].x()-x_med)*(data[i].y()-y_med);
        sum_xim_2 += POW2(data[i].x()-x_med);
    }
    //Pendiente a = (Sum((x_i - xmedia)(y_i - ymedia))/Sum((x_i - xmedia)^2)
    *a = sum_xim_yim/sum_xim_2;
    //Ord.Origen b =  ymedia - a *  xmedia
    *b = y_med - (*a) * x_med;
}



QDateTime Util::ParseDate(const QString& dtstr)
{
    QDateTime dt;
    QRegularExpressionMatch mc;
    //TIPO: 2013-01-27T12:21:00.321 y 2013-01-27 12:21:00.333
    mc = QRegularExpression("(\\d{4}\\-\\d{2}\\-\\d{2}(\\s|T)?\\d{2}:\\d{2}:\\d{2}(\\.\\d{1,3})?)")
            .match(dtstr);
    if (mc.hasMatch())
    {
        QString grpdate = mc.captured(1);
        dt = QDateTime::fromString(grpdate, Qt::ISODate);
        return dt.isValid() ? dt : INVALID_DATETIME;
    }
    //TIPO: 2013-01-27T12:21:00 y 2013-01-27 12:21:00
    mc = QRegularExpression("(\\d{4}\\-\\d{2}\\-\\d{2}(\\s|T)?\\d{2}:\\d{2}:\\d{2})")
            .match(dtstr);
    if (mc.hasMatch())
    {
        QString grpdate = mc.captured(1);
        dt = QDateTime::fromString(grpdate, Qt::ISODate);
        return dt.isValid() ? dt : INVALID_DATETIME;
    }
    //TIPO: 2013-01-27
    mc = QRegularExpression("(\\d{4}\\-\\d{2}\\-\\d{2})")
            .match(dtstr);
    if (mc.hasMatch())
    {
        QString grpdate = mc.captured(1);
        dt = QDateTime::fromString(grpdate, "yyyy-MM-dd");
        return dt.isValid() ? dt : INVALID_DATETIME;
    }
    //TIPO: 2013/01/27
    mc = QRegularExpression("(\\d{4}/\\d{2}/\\d{2})")
            .match(dtstr);
    if (mc.hasMatch())
    {
        QString grpdate = mc.captured(1);
        dt = QDateTime::fromString(grpdate, "yyyy-MM-dd");
        return dt.isValid() ? dt : INVALID_DATETIME;
    }
    //TIPO WEB: Fri, 07 Aug 2015 02:28:36 GMT
    mc = QRegularExpression("(Mon|Tue|Wed|Thu|Fri|Sat|Sun)[, ]*(?<DAY>\\d\\d) *"
                            "(?<MONTH>Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) *"
                            "(?<YEAR>\\d{4}) *"
                            "(?<HOUR>\\d\\d:\\d\\d:\\d\\d) *GMT"
                            )
            .match(dtstr);
    if (mc.hasMatch())
    {
        int day = mc.captured("DAY").toInt();
        QString month_str = mc.captured("MONTH").toUpper();
        int month = month_str == "JAN" ? 1  : month_str == "FEB" ? 2  : month_str == "MAR" ? 3 :
                    month_str == "APR" ? 4  : month_str == "MAY" ? 5  : month_str == "JUN" ? 6 :
                    month_str == "JUL" ? 7  : month_str == "AUG" ? 8  : month_str == "SEP" ? 9 :
                    month_str == "OCT" ? 10 : month_str == "NOV" ? 11 : 12;
        int year = mc.captured("YEAR").toInt();
        QString hour = mc.captured("HOUR");
        QString dtf = QString("").sprintf("%04d-%02d-%02d %s", year, month, day, hour.toLatin1().data());
        return Util::ParseDate(dtf);
    }
    return INVALID_DATETIME;
}

QString Util::HttpDate(const QDateTime& dtt)
{
    //Fri, 07 Aug 2015 02:28:36 GMT
    return QString("%1, %2 %3 %4 %5")
    .arg(dtt.date().dayOfWeek() == 2 ? "Mon" : dtt.date().dayOfWeek() == 3 ? "Tue" : dtt.date().dayOfWeek() == 4 ? "Wed" :
         dtt.date().dayOfWeek() == 5 ? "Thu" : dtt.date().dayOfWeek() == 6 ? "Fri" : dtt.date().dayOfWeek() == 7 ? "Sat" : "Sun")
    .arg(dtt.date().toString("dd"))
    .arg(dtt.date().month() == 1 ? "Jan" : dtt.date().month() == 2 ? "Feb" : dtt.date().month() == 3 ? "Mar" :
         dtt.date().month() == 4 ? "Apr" : dtt.date().month() == 5 ? "May" : dtt.date().month() == 6 ? "Jun" :
         dtt.date().month() == 7 ? "Jul" : dtt.date().month() == 8 ? "Aug" : dtt.date().month() == 9 ? "Sep" :
         dtt.date().month() == 10 ? "Oct" : dtt.date().month() == 11 ? "Nov" : "Dec")
    .arg(dtt.date().toString("yyyy"))
    .arg(dtt.time().toString("HH:mm:ss")) + " GMT";
}

QTime Util::ParseTime(const QString& tstr)
{
    QTime dt;
    QRegularExpressionMatch mc;
    //TIPO: 12:21:00.321
    mc = QRegularExpression("\\d{2}:\\d{2}:\\d{2}(\\.\\d{1,3})").match(tstr);
    if (mc.hasMatch())
    {
        QString grpdate = mc.captured(0);
        dt = QTime::fromString(grpdate, "hh:mm:ss.zzz");
        return dt.isValid() ? dt : INVALID_TIME;
    }
    //TIPO: 12:21:00
    mc = QRegularExpression("\\d{2}:\\d{2}:\\d{2}").match(tstr);
    if (mc.hasMatch())
    {
        QString grpdate = mc.captured(0);
        dt = QTime::fromString(grpdate, "hh:mm:ss");
        return dt.isValid() ? dt : INVALID_TIME;
    }
    return INVALID_TIME;
}

QString Util::HttpRequest(const QString& url, bool* ok)
{
    QNetworkAccessManager manager;
    QNetworkRequest request(url);
    QString answer;
    QEventLoop loop;

    if (ok) *ok = false;
    QNetworkReply* reply = NULL;
    QObject::connect(&manager, &QNetworkAccessManager::finished,
           [&]() {
                if (ok) *ok = !reply->error();
                answer = reply->error() ? reply->errorString() : reply->readAll();
                if (*ok) *ok = true;
            }
        );
    reply = manager.get(request);
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    return answer;
}

QString Util::HttpRequest(const QString& url, const  QStringList& post, bool* ok)
{
    QNetworkAccessManager manager;
    QNetworkRequest request(url);
    QString answer;
    QEventLoop loop;

    if (ok) *ok = false;
    QNetworkReply* reply = NULL;
    QObject::connect(&manager, &QNetworkAccessManager::finished,
           [&]() {
                if (ok) *ok = !reply->error();
                answer = reply->error() ? reply->errorString() : reply->readAll();
                if (*ok) *ok = true;
            }
        );
    QUrlQuery postData;
    for (int i=0; i<post.length()-1; i++)
        postData.addQueryItem(post[i], post[i+1]);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    reply = manager.post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    return answer;
}


void Util::WriteAllText(const QString filename, const QString& text, bool* err)
{
    if (err) *err = true;
    QFileInfo fi(filename);
    if (fi.exists()) QFile(filename).remove();
    QFile file(filename);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    if (file.error()) return;
    QTextStream out(&file);
    out.setCodec("UTF-8");
    out << text;
    file.close();
    if (err) *err = false;
}

void Util::WriteAllLines(const QString filename, const  QStringList& text, bool* err)
{
    if (err) *err = true;
    QFileInfo fi(filename);
    if (fi.exists()) QFile(filename).remove();
    QFile file(filename);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    if (file.error()) return;
    QTextStream out(&file);
    out.setCodec("UTF-8");
    for (int i=0; i<text.count(); i++)
    {
        out << text[i] << "\n";
    }
    file.close();
    if (err) *err = false;
}

WebReader::WebReader() : BackgroundWorker("Web reader")
{
    mMutex = new QMutex();
    mManager = NULL;
    mReply = NULL;
    mMethod = GET;
    mUseCaches = false;
}

WebReader::~WebReader()
{
    mMutex->lock();
    if (mManager) mManager->deleteLater();
    mManager = NULL;
    if (mReply) mReply->deleteLater();
    mReply = NULL;
    mMutex->unlock();
    delete mMutex;
}

void WebReader::cancel()
{
    mMutex->lock();
    BackgroundWorker::cancel();
    if (mReply)
    {
        emit mReply->abort();
        while (mReply->isRunning()) QThread::sleep(15);
        mReply->deleteLater();
    }
    if (mManager) mManager->deleteLater();
    mReply = NULL;
    mManager = NULL;
    mMutex->unlock();
}

void WebReader::clear()
{
    mQueryStrings.clear();
    mBuffer.clear();
}

bool WebReader::readCache(const QString& url)
{
    if (!mUseCaches) return false;
    bool ret = false;
    QString md5 = QString(QCryptographicHash::hash(url.toLatin1(), QCryptographicHash::Md5).toHex());
    if (XGlobal::APP_SETTINGS.caching_memory)
    {
        sCacheMutex.lock();
        if (sCache.contains(md5))
        {
            mBuffer = sCache[md5];
            ret = true;
        }
        sCacheMutex.unlock();
    }
    if (!ret && XGlobal::APP_SETTINGS.caching_disk)
    {
        //Cálculo del md5 de los datos (nos aseguramos la integridad del archivo)
        QCryptographicHash hash(QCryptographicHash::Md5);
        QString filename = QDir::cleanPath(QString("%1/%2.cache").arg(XGlobal::CACHE_PATH).arg(md5));
        if (QFile::exists(filename))
        {
            QFile fil(filename);
            if (fil.open(QFile::ReadOnly))
            {
                QTextStream ts(&fil);
                QString fl_md5 = ts.readLine();    //Hash indicado del archivo completo
                sCacheMutex.lock();
                mBuffer.clear();
                while (!ts.atEnd())
                {
                    QString ln = ts.readLine();
                    hash.addData(ln.toLatin1());
                    mBuffer.append(ln);
                }
                sCacheMutex.unlock();
                fil.close();
                QString md5_got = hash.result().toHex();     //Hash real de lo que hemos leido
                if (md5_got != fl_md5)
                {
                    ret = false;
                    fil.remove();
                } else ret = true;
            }
        }
    }
    return ret;
}

void WebReader::setCache(const QString& url)
{
    QString md5_url = QString(QCryptographicHash::hash(url.toLatin1(), QCryptographicHash::Md5).toHex());
    //Cacheo de memoria
    if (XGlobal::APP_SETTINGS.caching_memory)
    {
        sCacheMutex.lock();
        if (sCache.contains(md5_url)) sCache.remove(md5_url);
        if (sCache.count() >= MEM_CACHE_MAX_REQUESTS)
        {
            QStringList lst = sCache.keys();
            sCache.remove(lst.at(0));
        }
        sCache.insert(md5_url, mBuffer);
        sCacheMutex.unlock();
    }
    //Cacheo de disco
    if (XGlobal::APP_SETTINGS.caching_disk)
    {
        QString filename = QDir::cleanPath(QString("%1/%2.cache").arg(XGlobal::CACHE_PATH).arg(md5_url));
        if (!QFile::exists(filename))
        {
            QCryptographicHash hash(QCryptographicHash::Md5);
            for (int i=0; i<mBuffer.length(); i++)
                hash.addData(mBuffer[i].toLatin1());
            QString res_md5 = hash.result().toHex();
            QFile fil(filename);
            if (fil.open(QFile::WriteOnly))
            {
                fil.write(res_md5.toLatin1());
                fil.write("\r\n");
                for (int i=0; i<mBuffer.length(); i++)
                {
                    fil.write(mBuffer[i].toLatin1());
                    fil.write("\r\n");
                }
                fil.close();
            }
        }

        //Limpieza de cacheo de disco: 10% de las veces
        if (rand() % 10 == 0)
        {
            QDir dir(XGlobal::CACHE_PATH);
            if (dir.exists())
            {
                QStringList files = dir.entryList(QDir::NoDotAndDotDot);
                for (int i=0; i<files.length(); i++)
                {
                    QString filename = QString("%1/%2.cache").arg(XGlobal::CACHE_PATH).arg(files.at(i));
                    QFileInfo fil(filename);
                    if (fil.lastModified().daysTo(QDateTime::currentDateTime()) > MAX_DAYS_DISK_CACHE)
                    {
                        DINFO("Deleting cache file: %s", PTQ(filename));
                        QFile fil(filename); fil.remove();
                    }
                }

                //Si excede en un 50% el máximo de archivos se eliminan
                if (files.length()*1.5 > MAX_CACHE_FILES)
                {
                    qSort(files.begin(), files.end(), FileIsOlderThan);
                    for (int i=0; i<MAX_CACHE_FILES; i++)
                        QFile(files[i]).remove();
                }
            }
        }
    }
}

bool WebReader::FileIsOlderThan(QString fa, QString fb)
{
    QFileInfo fia(QString("%1/%2.cache").arg(XGlobal::CACHE_PATH).arg(fa));
    QFileInfo fib(QString("%1/%2.cache").arg(XGlobal::CACHE_PATH).arg(fb));
    return fia.lastModified() < fib.lastModified();
}

QString WebReader::getParamString()
{
    QString buff;
    for (int i=0; i<mQueryStrings.length(); i+=2)
        buff = buff % (buff.length()?"&":"") % mQueryStrings.at(i) % "=" % mQueryStrings[i+1].replace(" ","+");
    return buff;
}

void WebReader::ReadProgress_Slot(qint64 recv, qint64 total)
{
    OnMsg(QString("Downloading data: %1 bytes").arg((int)recv));
    if ((size() != total) && (total > 0) && !size()) OnSetup((int)total);
    if (recv > 0) OnProgress((int)recv);
}

void WebReader::ReadyRead_Slot()
{
    OnMsg("Reading data...");
}


int WebReader::doRequest(bool usecaches)
{   
    mUseCaches = usecaches;
    OnSetup(0);
    OnStart();
    if (usecaches && readCache(mUrl))
    {
        mReadFromCache = true;
        OnEnd();
        return 0;
    }
    else mReadFromCache = false;


    mMutex->lock();
    if (mReply)
    {
        mMutex->unlock();
        return -1;
    }
    mError = 0;
    if (mManager) mManager->deleteLater();
    mManager = new QNetworkAccessManager();
    QString params = getParamString();
    QEventLoop loop;
    OnMsg("Waiting for response");
    if (mMethod == POST_FORM)
    {
        QNetworkRequest req_p(QUrl(mUrl+""));
        req_p.setHeader(QNetworkRequest::UserAgentHeader, XUSER_AGENT);
        req_p.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        mReply = mManager->post(req_p, params.toLatin1());
    }
    else if (mMethod == POST_MULTIPART)
    {
        QNetworkRequest req_p(QUrl(mUrl+""));
        req_p.setHeader(QNetworkRequest::UserAgentHeader, XUSER_AGENT);
        req_p.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data; boundary=----------------------------77c15d4cf174");
        QByteArray ba;
        ba.append("------------------------------77c15d4cf174\r\n");
        ba.append("Content-Disposition: form-data; name=\"source\"\r\n\r\n");
        ba.append(mPostRawData);
        mReply = mManager->post(req_p, ba);
    }
    else if (mMethod == POST_TEXT)
    {
        QNetworkRequest req_p(QUrl(mUrl+""));
        req_p.setHeader(QNetworkRequest::UserAgentHeader, XUSER_AGENT);
        req_p.setHeader(QNetworkRequest::ContentTypeHeader,"text/plain");
        mReply = mManager->post(req_p, mPostRawData);
    }
    else
    {
        QNetworkRequest req_g(QUrl(params.length() ? mUrl + "?" + params : mUrl));
        req_g.setHeader(QNetworkRequest::UserAgentHeader, XUSER_AGENT);
        mReply = mManager->get(req_g);
    }
    connect(mReply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(mReply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(ReadProgress_Slot(qint64,qint64)));
    //connect(mReply, SIGNAL(readyRead()), this, SLOT(ReadyRead_Slot()));

    mMutex->unlock();

    //Esperar a que acabe
    loop.exec();
    mMutex->lock();
    //Comprobacmos si hemos acabado por cancelación
    if (!mReply)
    {
        OnEnd();
        mMutex->unlock();
        return mError = QNetworkReply::OperationCanceledError;;
    }
    else mError = (int)mReply->error();

    OnMsg("Reading response");
    if (cancelled() || !mReply)
    {
        mError = QNetworkReply::OperationCanceledError;
        QString mErrorString = formatError(mError);
        ERR("Error in mpc request: %s", PTQ(mErrorString));
        OnEnd();
        mReply->deleteLater();
        mReply = NULL;
        mMutex->unlock();
        return mError;
    }

    if (mError != QNetworkReply::NoError)
    {
        QString mErrorString = formatError(mError);
        ERR("Error in mpc request: [%03d]:%s",mError, PTQ(mErrorString));
        OnEnd();
        mReply->deleteLater();
        mReply = NULL;
        mMutex->unlock();
        return mError;
    }

    char line[4192];
    int readc = 0;

    while ((readc = mReply->readLine(line, 4192)) > 0)
    {
        line[readc] = 0;
        while (readc > 0 && ((line[readc--] == '\r') || (line[readc] == '\n')))
            line[readc] = 0;
        mBuffer.append(line);
    }
    OnEnd();
    mReply->deleteLater();
    mReply = NULL;
    mMutex->unlock();
    if (mUseCaches) setCache(mUrl);
    return mError;
}

QString WebReader::formatError(int err)
{
    switch  ((QNetworkReply::NetworkError)err)
    {
        case QNetworkReply::NoError: return "No Error";
        // network layer errors [relating to the destination server] (1-99):
        case QNetworkReply::ConnectionRefusedError: return "Connection refused"; break;
        case QNetworkReply::RemoteHostClosedError: return "Remote host closed the connection"; break;
        case QNetworkReply::HostNotFoundError: return "Host not found"; break;
        case QNetworkReply::TimeoutError: return "Timeout"; break;
        case QNetworkReply::OperationCanceledError: return "Operation was cancelled"; break;
        case QNetworkReply::SslHandshakeFailedError: return "SSL handshake failed"; break;
        case QNetworkReply::TemporaryNetworkFailureError: return "Temporary network vailure"; break;
        case QNetworkReply::NetworkSessionFailedError: return "Network session failed"; break;
        case QNetworkReply::BackgroundRequestNotAllowedError: return "Background request not allowed";
        case QNetworkReply::UnknownNetworkError: return "Unknown network error";
        // proxy errors (101-199):
        case QNetworkReply::ProxyConnectionRefusedError: return "Proxy connection refused";
        case QNetworkReply::ProxyConnectionClosedError: return "Proxy connection closed";
        case QNetworkReply::ProxyNotFoundError: return "Proxy not found";
        case QNetworkReply::ProxyTimeoutError: return "Proxy timeout";
        case QNetworkReply::ProxyAuthenticationRequiredError: return "Proxy authentication required";
        case QNetworkReply::UnknownProxyError: return "Unknown proxy error";
        // content errors (201-299):
        case QNetworkReply::ContentAccessDenied: return "Content access denied";
        case QNetworkReply::ContentOperationNotPermittedError: return "Content operation not permited";
        case QNetworkReply::ContentNotFoundError: return "Content not found";
        case QNetworkReply::AuthenticationRequiredError: return "Authentication required";
        case QNetworkReply::ContentReSendError: return "Content resend";
        case QNetworkReply::UnknownContentError: return "Unknown content";
        // protocol errors
        case QNetworkReply::ProtocolUnknownError: return "Unknown error";
        case QNetworkReply::ProtocolInvalidOperationError: return "Protocol invalid";
        case QNetworkReply::ProtocolFailure: return "Protocol failure";
        case QNetworkReply::ContentConflictError: return "Content conflict";
        case QNetworkReply::ContentGoneError: return "Content gone";
        case QNetworkReply::InternalServerError: return "Content gone";
        case QNetworkReply::OperationNotImplementedError: return "Operation not implemented";
        case QNetworkReply::ServiceUnavailableError: return "Service unavailable";
        case QNetworkReply::UnknownServerError: return "Unknown server";
    }

    return QString("").sprintf("Unknown error (%d)", err);
}

FMatrix::FMatrix(int height, int width)
{
    mData = new double[width * height];
    for (int i=0; i<width*height; i++) mData[i] = 0;
    mWidth = width;
    mHeight = height;
}

FMatrix::FMatrix()
{
    mWidth = mHeight = 0;
    mData = NULL;
}

FMatrix::~FMatrix()
{
    if (mData) delete mData;
    mData = NULL;
}

void FMatrix::setSize(int height, int width)
{
    if (mData)
        delete mData;
    mData = new double[width * height];
    for (int i=0; i<width*height; i++) mData[i] = 0;
    mWidth = width;
    mHeight = height;
}

double FMatrix::get(int i, int j)
{
    ASSERT(i<mHeight&&i>=0&&j<mWidth&&j>=0,"FMatrix index out of bounds");
    return mData[i * mWidth + j];
}

void FMatrix::set(int i, int j, double val)
{
    ASSERT(i<mHeight&&i>=0&&j<mWidth&&j>=0,"FMatrix index out of bounds");
    mData[i * mWidth + j] = val;
}

void FMatrix::copy(FMatrix* a)
{
    setSize(a->height(), a->width());
    memcpy(mData, a->mData, sizeof(double) * mWidth * mHeight);
}

void FMatrix::debug()
{
    QString line;
    for (int i = 0; i<mHeight; i++)
    {
        line = "";
        for (int j=0; j<mWidth; j++)
        {
            if (j == 0)
                line = QString("%1").arg(get(i, j));
            else
                line = QString("%1, %2").arg(line).arg(get(i, j));
        }
        qDebug() << line;
    }
}

BitMatrix::BitMatrix(int start_x,int start_y, int width, int height)
{
   mStartX = start_x;
   mStartY = start_y;

   mHeight = height;
   mWidth = width;

   //Calcular el número de bits y bytes
   int bitCount = mHeight * mWidth;
   mByteCount = (bitCount >> 3) + 1;  //No cuesta nada poner un byte adicional
   if (bitCount % 8 != 0)
       mByteCount++;

   //Reserva espacio de memoria para los datos si fuera necesari
   if (mByteCount <= BITMATRIX_DATASIZE)
   {
       //No reservamos, usamos el array estático
       mData = mStatic;
       mDinamic = NULL;
   }
   else
   {
       //Reservamos, usamos el array dinámico
       mDinamic  = new uchar[mByteCount];
       mData = mDinamic;
   }
   Reset();
}

BitMatrix::~BitMatrix()
{
    if (mDinamic) delete mDinamic;
    mDinamic = NULL;
}


void BitMatrix::Reset()
{
    memset(mData, 0, mByteCount);
}

void BitMatrix::MoveTo(int x,int y)
{
    mStartX = x;
    mStartY = y;
}

bool BitMatrix::get(int x,int y)
{
    if (x < left() || x > right() || y < top() || y > bottom()) return false;
    int pos = (y - mStartY) * mWidth + (x - mStartX);
    int index = pos % 8;
    pos >>= 3;
    ASSERT(pos<mByteCount,"BitMatrix index out of bounds in get");
    return (mData[pos] & (1 << index)) != 0;
}

void BitMatrix::set(int x,int y,bool value)
{
    ASSERT(x>=left()&&x<=right()&&y>=top()&&y<=bottom(),"BitMatrix index out of bounds in set");
    int pos = (y - mStartY) * mWidth + (x - mStartX);
    int index = pos % 8;
    pos >>= 3;
    mData[pos] &= (uchar)(~(1 << index));
    if (value)
        mData[pos] |= (uchar)(1 << index);
}

void BitMatrix::assign(BitMatrix* b)
{
    //Tamaño antiguo
    int bitCount = this->mHeight * this->mWidth;
    int oldsize = bitCount >> 3;
    if (bitCount % 8 != 0) oldsize++;
    //Tamaño nuevo
    bitCount = b->mHeight * b->mWidth;
    int newsize = bitCount >> 3;
    if (bitCount % 8 != 0) newsize++;
    //Asignar todos los valores
    this->mByteCount = b->mByteCount;
    this->mHeight = b->mHeight;
    this->mWidth = b->mWidth;
    this->mStartX = b->mStartX;
    this->mStartY = b->mStartY;
    //Caso de que haya que reservar mas memoria de la que tenemos
    if (newsize > oldsize && newsize > BITMATRIX_DATASIZE)
    {
        if (mDinamic) delete mDinamic;
        mDinamic = new uchar[newsize];
        mData = mDinamic;
    }
    //Copia efectiva de los datos
    memcpy(this->mData, b->mData, newsize * sizeof(uchar));
}

void BitMatrix::appendBits(BitMatrix* b)
{
    //Calculamos los límites de intersección
    int min_left = MAXVAL(this->left(), b->left());
    int max_right = MINVAL(this->right(), b->right());
    int min_top = MAXVAL(this->top(), b->top());
    int max_bottom= MINVAL(this->bottom(), b->bottom());
    //Como se supone que son iguales hacemos un OR entre las dos matrices
    for (int y=min_top; y <= max_bottom; y++)
        for (int x=min_left; x<= max_right; x++)
            if (b->get(x,y)) set(x,y,true);
}

bool BitMatrix::intersects(BitMatrix  *_b)
{
    BitMatrix* a = this;
    BitMatrix* b = _b;
    if (a->mWidth * a->mHeight > b->mWidth * b->mHeight)
    {
      BitMatrix* aux = a;
      a = b;
      b = aux;
    }
    for (int y = a->top(); y <= a->bottom(); y++)
      for (int x = a->left(); x <= a->right(); x++)
          if (a->get(x, y) && b->get(x, y)) return true;
    return false;
}

WebSocket::WebSocket()
{
    mTimeOut = 15000;
    mUserAgent = XUSER_AGENT;
    mRequest = NULL;
    mReply = NULL;
    mManager = new QNetworkAccessManager(this);
}

WebSocket::~WebSocket()
{
    close();
}

bool WebSocket::connect(QString& curl,QString method,QStringList addHdrs)
{
    mBytesRead = 0;
    mHeaders.clear();
    if (mReply) mReply->deleteLater();
    mReply = NULL;
    if (mRequest) delete mRequest;
    mRequest = NULL;
    if (mManager) mManager->deleteLater();
    mManager = new QNetworkAccessManager();

    DINFO("Current DIR: %s", PTQ(QDir().absolutePath()));
    QEventLoop loop;

    //Agregar cabeceras adicionales
    mRequest = new QNetworkRequest(curl);
    for (int i=0; i<addHdrs.count(); i+=2)
        mRequest->setRawHeader(addHdrs.at(i).toLatin1(), addHdrs.at(i+1).toLatin1());

    mReply = (method == "GET") ? mManager->get(*mRequest) : mManager->get(QNetworkRequest(*mRequest));
    QObject::connect(mReply, SIGNAL(readyRead()) , &loop, SLOT(quit()));
    QObject::connect(mReply, SIGNAL(finished()) , &loop, SLOT(quit()));
    loop.exec();    

    mHttpStatus = mReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (mReply->error() != QNetworkReply::NoError)
    {
        mError = mReply->errorString();
        return false;
    }

    if (method == "HEAD" && mReply)
        mReply->close();


    QList<QByteArray> hdrs = mReply->rawHeaderList();
    for (int i=0; i<hdrs.count(); i++)
    {
        QString hdr = QString(hdrs[i]);
        QString val = mReply->rawHeader(hdrs[i]);
        mHeaders.append(hdr);
        mHeaders.append(val);
        if (hdr.toLower().trimmed() == "content-length")
            mContentLength = val.toInt();
        if (hdr.toLower().trimmed() == "")
            INFO("..");

    }
    return true;
}

int WebSocket::getSize(QString& url)
{
    WebSocket ws;
    if (!ws.connect(url,"HEAD") && !ws.connect(url, "GET")) return -1;
    ws.close();
    return ws.contentLength();
}

void WebSocket::close()
{
    if (mManager) mManager->deleteLater();
    mManager = NULL;
    if (mReply) mReply->deleteLater();
    mReply = NULL;
    if (mRequest) delete mRequest;
    mRequest = NULL;
}

int WebSocket::read(char* buffer, int max)
{
    int avail = (int)mReply->bytesAvailable();
    if (avail == 0)
    {
        QEventLoop loop;
        QObject::connect(mReply, SIGNAL(readyRead()) , &loop, SLOT(quit()));
        loop.exec();
        avail = (int)mReply->bytesAvailable();
    }
    if (avail > max) avail = max;
    int br = (int) mReply->read(buffer, avail);
    if (br < 0) return br;
    mBytesRead += br;
    return br;
}

QString WebSocket::readLine()
{
    QByteArray ba = mReply->readLine();
    return QString(ba);
}

FileDownloader::FileDownloader()
    : BackgroundWorker("File downloader")
{
    mManager = NULL;
    mReply = NULL;
    mOutputFile = NULL;
    mAtomicDownload = true;
    mOnlyHeaders = false;
}

FileDownloader::~FileDownloader()
{
    if (mManager) mManager->deleteLater();
    mManager = NULL;
    mReply = NULL;  //No necesita ser eliminada ya que lo elimina el padre!!!
    if (mOutputFile) mOutputFile->close();
    mOutputFile = NULL;
}

void FileDownloader::setFiles(QString url, QString file)
{
    QStringList urls; urls << url;
    QStringList ofiles; ofiles << file;
    setFiles(urls, ofiles);
}

void FileDownloader::setFiles(QStringList urls, QStringList output_files)
{
    mUrls = urls;
    mOutputFiles = output_files;
}

bool FileDownloader::doWork()
{
    mCancelled = false;
    mError = "";
    OnStart();
    OnSetup(100);
    //Comprobar en primea instancia el tamaño de todos los archivos si hubiera varios
    mHeaders.clear();
    mHttpStatus.clear();
    mFileSizes.clear();
    mContentLength = 0;
    mCurrentFileBytes = 0;
    if (mUrls.count() > 1 && !mOnlyHeaders)
    {
        OnMsg("Checking file sizes...");
        for (int i=0; i<mUrls.count() && !mCancelled;i++)
        {
            OnTitleChange(mUrls[i]);
            int fs = WebSocket::getSize(mUrls[i]);
            if (fs <= 0)
            {
              mError = "Error getting file size:\n " + mUrls[i];
              OnEnd();
              return false;
            }
            mContentLength += fs;
            mFileSizes.append(fs);
        }
    }
    for (int i=0; i<mUrls.count() && !mCancelled;i++)
    {
        if (!downloadFile(mUrls[i], mOutputFiles[i],
                          mAdditionalHeaders.count() > i ? mAdditionalHeaders[i] : QStringList()))
        {
            mError = "Error downloading file: " + mUrls[i] + " " + mError;
            OnEnd();
            return false;
        }
    }
    OnProgress(mUrls.count() * 100);
    OnEnd();
    return true;
}

QString FileDownloader::readLine(QTcpSocket* sk)
{
    *mBuffer = 0;
    if (!sk->bytesAvailable() && !sk->waitForReadyRead())
    {
        mError = "Error 1 reading from socket";
        return "";
    }
    qint64 numread = sk->readLine(mBuffer, FILEDOWNLOADER_DATASIZE);
    if (numread <= 0)
    {
        mError = "Error 2 reading from socket";
        return "";
    }
    return QString(mBuffer).trimmed();
}

bool FileDownloader::downloadFile(QString url, QString _dstfile, QStringList addHdrs)
{
    OnTitleChange(url);
    QString dstfile = mAtomicDownload ? QDir::tempPath() + "/" + QFileInfo(_dstfile).fileName() : _dstfile;
    mCurrentFilename = _dstfile;

    //Apertura del archivo
    mOutputFile = new QFile(dstfile);
    if (!mOutputFile->open(QFile::WriteOnly | QFile::Truncate))
    {
        delete mOutputFile; mOutputFile = NULL;
        mError = QString("Unable to open output temp file: '%1'").arg(dstfile);
        return false;
    }
    //Conexióna la URL y el socket
    OnMsg("Connecting...");
    WebSocket sock;
    if (!sock.connect(url, mOnlyHeaders ? "HEAD" : "GET", addHdrs) && !sock.connect(url, "GET", addHdrs))
    {
        delete mOutputFile; mOutputFile = NULL;
        mError = QString("Unable to connect to url: '%1'").arg(url);
        return false;
    }
    mHeaders.append(*sock.headers());
    mHttpStatus.append(sock.httpStatus());
    mFileSizes.append(sock.contentLength());
    if (mOnlyHeaders)
    {
        sock.close();
        mCurrentFileBytes++;
        OnProgress(mHeaders.count() * 100 / mUrls.count());
        return true;
    }
    if (!sock.contentLength())
    {
        mError = "Error 0 reading from socket";
        mOutputFile->close();
        return false;
    }
    if (mUrls.count() == 1)
        mContentLength = sock.contentLength(); 
    int last_percent = -1;
    //Lectura del archivo
    while (sock.connected() && !sock.eof())
    {
        int numread = sock.read(mBuffer, FILEDOWNLOADER_DATASIZE);
        if (numread < 0)
        {
            mError = "Error 4 reading from socket";
            mOutputFile->close();
            return false;
        }
        mOutputFile->write(mBuffer, numread);
        mCurrentFileBytes += numread;
        //Mostrar el progreso actual
        int percent = (int)((double)mCurrentFileBytes * 100.0 / (double)mContentLength);
        if (percent > 100) percent = 100;
        if (last_percent != percent && percent < 100)
        {
            OnMsg(QString("").sprintf("Downloading data: %0.2f MB / %0.2f MB",
                                      mCurrentFileBytes/1024.0/1024,mContentLength /1024.0/1024));
            OnProgress(percent);
            last_percent = percent;
        }
        if (sock.eof()) break;
        if (mCancelled) break;
    }
    mOutputFile->close();
    delete mOutputFile; mOutputFile = NULL;
    if (_dstfile != dstfile && !mCancelled && sock.eof())
    {
        QFile(_dstfile).remove();
        if (!QFile(dstfile).rename(_dstfile))
        {
            //Como no se ha podido renombrar, intentamos copiarlo directamente
            bool ok = QFile(dstfile).copy(_dstfile);
            QFile(dstfile).remove();
            if (!ok)
            {
                mError = "Unable to create output file: " + _dstfile;
                return false;
            }
        }
    }
    else if (mCancelled) QFile(dstfile).remove();
    return true;
}

int FileDownloader::Background_Download(void *data)
{
   FileDownloader* dld = (FileDownloader*)data;
   bool result = dld->doWork();
   return result?0:1;
}


void FileDownloader::cancel()
{
    if (mManager) mManager->deleteLater();
    mManager = NULL;
    mCancelled = true;
}


void FileDownloader::Cancel_Slot()
{
     mCancelled = true;
}









