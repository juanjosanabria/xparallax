#ifndef CATALOG_H
#define CATALOG_H

#include "core.h"
#include "detection.h"

class CORESHARED_EXPORT CatalogParameters
        : public Parameters
{
public:
    CatalogParameters();
    ~CatalogParameters();
    void SetDefaults();
    void SetDefaultsCat();
    void SetDefaultsFlux();
    void SetDefaultFilter();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);
    void SortFields(QStringList* par, const char** sort_order);
    bool needAstrometry();                  //Indica si los parámetros seleccionados obligan a tener astrometría de las imágenes

    //Catálogo de fuentes detectadas
    bool        cat_output;                  //Indica si hay que generar los archivos de catálogo
    QStringList cat_fields;                  //Campos de salida en los archivos de catálogo y su orden
    QStringList cat_fields_order;            //Orden seleccionado de los campos de catálogo (para mostrar en próximas aperturas del diálogo)
    QString     cat_fmt;                     //Formato de salida de los archivos de catálogo
    bool        cat_sort;                    //Indica cómo ordenar las fuentes
    QString     cat_sort_field;              //Indica el campo de ordenación
    bool        cat_sort_order;              //Indica ascendente=true, descendente=false
    //Tabla de flujo
    bool        flux_output;                 //Indica si se generarán las tablas de flujo
    QStringList flux_fields;                 //Campos a incluir delante de cada fuente
    QStringList flux_fields_order;           //Orden seleccionado de los campos de tabla de flujo (para mostrar en próximas aperturas del diálogo)
    QString     flux_type;                   //Tipo de flujo a incluir en el archivo
    QString     flux_fmt;                    //Indica el formato de salida del archivo de flujo
    int         flux_heading;                //Tipo de encabezado 0=filename, 1=datetime, 2=seconds, 3=POS
    bool        flux_sort;                   //Indica cómo ordenar las fuentes
    QString     flux_sort_field;             //Indica el campo de ordenación
    bool        flux_sort_order;             //Indica ascendente=true, descendente=false
    //Filtrado de fuentes
    bool        filt_output;                 //Indica si se hará un filtrado de fuentes
    QStringList filt_sources;                //Fuentes, coordenadas separadas por coma, fuentes separadas por punto y coma
    float       filt_separation;

    static QString getDescCatField(const QString& field);
    static QString getDescFluxField(const QString& field);  //Devuelve la descripción de un cmapo de la tabla de flujo
    static QString getDescFormat(const QString& fmt);       //Devuelve la descripción de un formato de salida
    static QString getFormatFromDesc(const QString& desc);  //Devuelve el tipo de un formato de salida a partir de su descripción
    static const char*  AVAILABLE_CAT_FIELDS[];             //Campos disponibles para incluir en el catálogo junto con su descripción
    static const char*  AVAILABLE_FLUX_FIELDS[];            //Campos disponibles para incluir en las tablas de flujo
    static const char*  OUTPUT_FORMATS[];                   //Tipos de formatos de salida y sus descripciones
    static const char*  SORTABLE_FIELDS[];                  //Campos por los que se pueden ordenar las fuentes

private:
    static CatalogParameters*   SORT_PARAMETERS;
    static QMutex               SORT_MUTEX;
    static QStringList          SORT_ORDER;
    static int                  LessThan(QString a, QString b);
};


struct CatFilteredSource{
    QString name;
    double  ra;
    double  dec;
};

class CORESHARED_EXPORT CatalogComputer :
    public BackgroundWorker
{
     Q_OBJECT

public:
    CatalogComputer();
    ~CatalogComputer();
     bool doWork(WorkFileList* wf, DetectionParameters* detpar, CatalogParameters* catpar);
    inline StarDetector*         detector(){return mDetector;}
private:
    WorkFileList*                 mInputFiles;            //Archivos de entrada
    DetectionParameters*          mDetectPar;             //Paraámetros de detector de fuentes
    CatalogParameters*            mCatPar;                //Parámetros para la salida del catálogo
    FitImage*                     mCurrentImage;          //Imagen actual en proceso
    bool                          mCurrentImageControl;   //Indica si tenemos el control sobre la imagen actual
    QList<QVector<DetectedStar>*> mDetectedStars;         //Todas las listas de estrellas detectadas
    StarDetector*                 mDetector;              //Detector de fuentes
    char**                        mOutputCatFields;       //Campos de salida formato char* para agilizar
    char**                        mOutputFluxFields;      //Campos para introducir en la tabla de flujo
    int                           mDetectorSize;
    int                           mCurFile;
    QList<QDateTime>              mImageDates;
    int                           mDatacount;             //Contador de datos. Número de datos
    QString                       mCurOutputFormat;       //CSV,TXT,VOT
    QVector<CatFilteredSource>    mFilteredSources;       //Fuentes filtradas

    void                        CreateFilterList();       //Parsear la lista de fuentes a filtrar
    CatFilteredSource*          GetFiltered(double ra, double lat);
    bool                        ComputeCatalog();
    void                        ClearMemory();
    bool                        needSave();                        //Indica si hay que generar algún archivo de salida
    int                         steps();
    QVector<DetectedStar>*      Detect(QString filename);
    QVector<DetectedStar>*      Detect(FitImage* fitimage);
    void                        Sort(QVector<DetectedStar>* stars, QString field, bool ascending);
    QString                     SaveCatalog(QString filename, QVector<DetectedStar>* list);
    void                        SaveCatalogTXT(QTextStream* stream,  QVector<DetectedStar>* list);
    void                        SaveCatalogCSV(QTextStream* stream,  QVector<DetectedStar>* list);
    void                        SaveCatalogVOT(QTextStream* stream,  QVector<DetectedStar>* list);
    //Cómputo de la tabla de flujos
    QString                     SaveFluxTable(QString filename, QList<QVector<DetectedStar>*>* lists);
    void                        SaveFluxTableTXT(QTextStream* stream, QVector<DetectedStar>* mainlist, QList<QVector<DetectedStar>*>* lists);
    void                        SaveFluxTableCSV(QTextStream* stream, QVector<DetectedStar>* mainlist, QList<QVector<DetectedStar>*>* lists);
    void                        SaveFluxTableVOT(QTextStream* stream, QVector<DetectedStar>* mainlist, QList<QVector<DetectedStar>*>* lists);
    /**
     * @brief Contains Realiza una búsqueda dicotómica en una lista de fuentes ordenadas por DECLINACIÓN.
     * @param list Lista de fuentes donde se desea buscar
     * @param star Estrella a buscar en la lista
     * @param insert_point Opcionalmente, la función devuelve el punto de inserción donde debería ser insertada la fuente
     * dentro de la lista.
     * @param separation Separación angular en segundos de arco para considerar dos fuentes como la misma. No usar más de 5
     * @return Estrella dentro de la lista que se corresponde con la buscada
     */
    DetectedStar*               Contains(QVector<DetectedStar>* list, DetectedStar* star, int* insert_point=NULL, double separation=SAME_SOURCE_SEPARATION);

    /**
     * @brief GetStarField      Devuelve una cadena formateada de una estrella detectada. Por necesidades de velocidad para evitar la
     *                          comparación de QStrings y el paso de parámetros.
     * @param field_name        Nombre del campo a obtener
     * @param star_idx          Índice de la estrella dentro de la matriz
     * @param star              Puntero a los datos de la estrella
     * @param buffer_out        Buffer para almacenar los datos de salida
     * @param sep               Separador o cadena separadora
     * @return
     */
    char*                       GetStarField(char* field_name, int star_idx, DetectedStar* star, char* buffer_out, const char *sep="    ");
    static int                  LessThan(DetectedStar a, DetectedStar b);
    static int                  GreatherThan(DetectedStar a, DetectedStar b);

    static CatalogComputer*     CURRENT_CATALOG_COMPUTER;
    static QString              SORT_FIELD;
    static QMutex               SORT_MUTEX;

private slots:
    void DetectorSetup_Slot(BackgroundWorker* detector, int size);
    void DetectorProgress_Slot(BackgroundWorker* detector, int position);

};


#endif // CATALOG_H
