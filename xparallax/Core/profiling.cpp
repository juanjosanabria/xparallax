#include "profiling.h"

#ifdef QT_DEBUG
Profiling* Profiling::PROF = new Profiling();
#else
Profiling* Profiling::PROF = NULL;
#endif


Profiling::Profiling()
{
    startProfile();
    mTimer.start();
}

void Profiling::startProfile()
{

}

void Profiling::endProfile()
{
    double ms_total = Profiling::PROF->mTimer.elapsed()/1000000.0;
    //Crear el fichero con el análisis
    FILE* file = fopen("profileapp.log","wt");
    fprintf(file,"END OF PROGRAM WITH: %0.3f ms\n", ms_total);
    fprintf(file,"-----------------------------------------------\n");
    for (int i=0; i< Profiling::PROF->mProf.count(); i++)
    {
        fprintf(file, "FUNCTION    : %s\n",  Profiling::PROF->mProf.data()[i].fn_name.toLatin1().data());
        fprintf(file, "\t RUN COUNT: %lu\n",  Profiling::PROF->mProf.data()[i].exec_count);
        fprintf(file, "\t TOTAL MS : %0.3f\n",  Profiling::PROF->mProf.data()[i].millis_total);
        fprintf(file, "\t AVG MS   : %0.3f\n", (double) Profiling::PROF->mProf.data()[i].millis_total/ Profiling::PROF->mProf.data()[i].exec_count);
    }
    fclose(file);
}

void Profiling::startFunc(ProfilingFunc* f)
{
    mCallStack.append(f);
}

void Profiling::endFunc(ProfilingFunc* f)
{
    mCallStack.removeLast();
    PROFILE_STRUCT* pe = NULL;
    for (int i=0; i<mProf.count() && !pe; i++)
       if (mProf.data()[i].fn_name == f->name()) pe = mProf.data()+i;
    if (!pe)
    {
        mProf.append(PROFILE_STRUCT());
        pe = mProf.data() + (mProf.count()-1);
        pe->fn_name = f->name();
        pe->exec_count = pe->millis_total = 0;
    }
    pe->millis_total += f->ellapsedNano() / 1000000.0;
    pe->exec_count++;
}

ProfilingFunc::ProfilingFunc(const char* name)
{
    mTimer.start();
    mName = name;
    Profiling::PROF->startFunc(this);
}

ProfilingFunc::~ProfilingFunc()
{
    mEllapsed = mTimer.nsecsElapsed();
    Profiling::PROF->endFunc(this);
}


void ProfilingFunc::startBlock(char* name)
{
    QString mnn = mName + "//" + name;
    ProfilingFunc* pf = new ProfilingFunc(ProfilingFunc(mnn.toLatin1().data()));
    mBlocks.append(pf);
    Profiling::PROF->startFunc(pf);
}

void ProfilingFunc::endBlock(char* name)
{
    for (int i=mBlocks.count()-1; i>=0; i--)
    {
        ProfilingFunc* pf = mBlocks[i];
        if (pf->name().endsWith(name))
        {
            delete pf;
            mBlocks.removeAt(i);
            return;
        }
    }
}
