#ifndef G2D_H
#define G2D_H

/**
  FUNCIONES GEOMÉTRICAS EN 2D
  ------------------------------------------------
  Juan José Sanabria
  ------------------------------------------------

**/

#include "core.h"
#include "core_def.h"
#include <QPointF>


class CORESHARED_EXPORT G2D_Line
{
    private:
        double mM;   //Pendiente y=mx+b
        double mB;   //Ordenada en el origen y=m*x+b
        double mp;  //Pendiente conjugada x=mp*y+bp
        double bp;  //Ordenada en el origen conjugada x=mp*y+bps
        double mRCoeff; //Coeficiente de correlación de pearson
        double mSCoeff; //Coeficiente de correlación de spearman

    public:
        G2D_Line(){mRCoeff=0; mM=INVALID_FLOAT; mB = INVALID_FLOAT;}
        G2D_Line(double mM, double mB);
        G2D_Line(QPointF a, QPointF mB);
        G2D_Line(double p1x,double p1y,double p2x,double p2y){init(QPointF(p1x,p1y),QPointF(p2x,p2y));}
        bool isVertical(){return !COMPF2(mp,0);}
        bool isHorizontal(){return !COMPF2(mM,0.0);}
        double m(){return mM;}
        double b(){return mB;}
        double r(){return mRCoeff;}
        double s(){return mSCoeff;}
        void setR(double rc){mRCoeff=rc;}
        void setS(double sc){mSCoeff=sc;}
        void init(QPointF a, QPointF mB);
        void init(double mM, double mB);
        void initc(double mp,double bp);
        void map(double x, double* y);      //Devuelve la ordenada en un punto
        void mapc(double y, double* x);     //Devuelve la abcisa para una ordenada
        G2D_Line perpendicular(QPointF p);  //Devuelve una recta perpendicular a esta
        void rotate(double deg);
        inline bool isNull(){return IS_INVALIDF(mM)|| IS_INVALIDF(mB);}
        bool IntersectsSegment(QPointF p1, QPointF p2);

        /**
         * @brief pointFarFrom Devuelve los dos puntos p1,p2 contenidos en la recta perpendicular
         * que pasa por "p"  y que distan que distan "d" unidades  del punto "p", perteneciente
         * a la recta actual.
         * @param p     Punto de referencia por donde pasará la perpendicular a esta recta
         * @param dist  Distancia exigida
         * @return
         */
        QVector<QPointF> pointFarFrom(QPointF p, double d);

        double distance(QPointF p);
};




#endif // G2D_H
