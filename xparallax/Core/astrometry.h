#ifndef ASTROMETRY_H
#define ASTROMETRY_H

#include "progress.h"
#include "matcher.h"
#include "detection.h"
#include "vo.h"
#include "detection.h"
#include "plateconstants.h"

//Rechazo de fuentes cuyo resudio excede en unidades de desviación típica
#define STDEV_PLATE_REJECT  3

class CORESHARED_EXPORT AstrometryComputer
        : public BackgroundWorker
{
public:
    AstrometryComputer();
    ~AstrometryComputer();
    bool doWork(WorkFileList* wf, DetectionParameters* dp, MatchParameters* mp);
    inline Matcher*        matcher(){return matcher_;}
    inline StarDetector*   detector(){return detector_;}
    inline VizierReader*   vizier(){return vizierReader_;}
    inline PlateConstants* platec(){return plateConstants_;}

    virtual void cancel();
    int  CountOK();
    int  CountKO();

    static PlateConstants* ComputePlateConstants(Matcher* mc);                           //Calcula los PC a partir de una coincidencia
    static PlateConstants* ComputePlateConstants_ANUAL_BB(Matcher* mc, FitHeaderList* fh);
    static void AddRaDecHeader(FitImage* fitimage, PlateConstants* pc);
    static void AddLspcHeader(FitImage* fitimage, PlateConstants* pc);                   //Agrega cabeceras LSPC a una imagen
    static void AddStarndardHeader(FitImage* fitimage, PlateConstants* pc);              //Agrega cabecera WCS a una imagen
    static void AddCommentsHeader(FitImage* fitimage, PlateConstants* pc, Matcher* mc);
    static bool SaveReferenceStars(PlateConstants* pc, QString output_filename, QString input_filename);
    //Cálculo del zeroP fotométrico
    static double ComputeMagZeroP(QString band, Matcher* mcher,  StarDetector* det_stars, VizierReader* cat_stars);

private:
    WorkFileList*        workList_;
    DetectionParameters* detectParams_;
    MatchParameters*     matchParams_;


    //Creados por la propia clase que tendrán que ser borrados
    VizierReader*       vizierReader_;
    StarDetector*       detector_;
    Matcher*            matcher_;
    PlateConstants*     plateConstants_;

    //Internos de uso común
    bool                singleProcess_;
    QString ComputeAstrometry(FitImage* fitimage);
    QString ComputeAstrometry(const QString& filename);


public slots:
    void Cancel_Slot();

};

#endif // ASTROMETRY_H













