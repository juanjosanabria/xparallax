#ifndef DETECTION_H
#define DETECTION_H

#include "core_global.h"
#include "core.h"
#include "background.h"
#include "progress.h"
#include "util.h"
#include "ap.h"
#include <QPoint>
#include <QPointF>
#include <QRect>
#include <QVector>

#define  SAME_SOURCE_SEPARATION         3.0         //Separación en segundos de arco para considerar una fuente como la misma

class StarDetector;
class PlateConstants;

class CORESHARED_EXPORT DetectionParameters
        : public Parameters
{
public:
    DetectionParameters();
    ~DetectionParameters();
    void SetDefaults();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);

public:
    //Parámetros para el cálculo del fondo
    BackgroundParameters     p_bkg;

    //Parámetros para la detección de una estrella
    float                    detect_thres_sigma;  //Nivel para considerar un píxel perteneciente a una estrella en unidades de desviación típica del fondo
    int                      detect_min_radius;   //Mínimo radio para una estrella
    int                      detect_max_radius;   //Máximo radio para una estrella
    int                      detect_min_area;     //Número mímino de píxeles dentro de una estrella

    //Parámetros pra el deblending y el rechazo de estrellas
    float                    detect_ecc_max;       //Indica, después del deblending con qué excentricidad rechazar

    //Parámetros de deblending
    bool                     deblend_stars;      //Indica si habrá que intentad deblending o no
    int                      deblend_nthres;     //Número de niveles de deblending
    float                    deblend_upper_sigma;//Límite para el umbral en unidades de desviación típica sobre el fondo
    float                    deblend_lower_sigma;//Límite para el umbral en unidades de desviación típica sobre el fondo
    float                    deblend_contrast;   //Contraste mínimo para tomar una fuente como buena

    //Otros parámetros que no serán volcados a disco
    int                      flags;             //Tipos de centros que hay que calcular (DetectionFlags)

};


class CORESHARED_EXPORT DetectedStar
{
friend class StarDetector;
friend class StarDetectorParallel;

public:
enum DetectedFlag{
    DF_NOFLAG                   = 0,
    DF_ISNULL                   = 1<<0,
    DF_SOEXCENTRIC              = 1<<1,
    DF_SATURATED                = 1<<2,
    DF_DEBLENDED                = 1<<3,         //Ha sido desdoblada
    DF_BADECC                   = 1<<4,
    DF_NOT_SUIT_FOR_ASTROMETRY  = 1<<5,
    DF_NOT_SUIT_FOR_PHOTOMETRY  = 1<<6,
    DF_ALIGN_STAR               = 1<<7,         //Estrella de alineación usada para la astrometría
    DF_REFERENCE_STAR           = 1<<8,         //Estrella usada para calcular una solución astrométrica
    DF_PLATE_REJECTED           = 1<<9,         //Estrella rechazada para la astrometría
    DF_WINDOWED_CENTER          = 1<<10,        //Indica que se calculó el centro windowed
    DF_PSF_CENTER               = 1<<11,        //Indica que se calculó el centro psf

    //Centro preferido
    DF_PREFERRED_CENTER_WINDOWED= 1<<12,
    DF_PREFERRED_CENTER_PSF     = 1<<13,
    DF_PREFERRED_CENTER_ISO     = 1<<14,
    DF_PREFERRED_CENTER_PEAK    = 1<<15,
};

public:
    inline DetectedStar(){ mFlags = (DetectedFlag)(DF_ISNULL|DF_PREFERRED_CENTER_WINDOWED);
                           mFluxPhoto = mFluxISO = mFluxWin = 0;}

    //Baricenter ISO connected center
    inline float x(){return mIsoCentroid.x();}
    inline float y(){return mIsoCentroid.y();}
    //Windowed center (centro de apertura)
    inline float wx(){return mWCentroid.x();}
    inline float wy(){return mWCentroid.y();}
    //Centro de la psf
    inline float gx(){return mPsfCenter.x();}
    inline float gy(){return mPsfCenter.y();}
    //Centro del pico de la estrella
    inline float kx(){return mPsfCenter.x();}
    inline float ky(){return mPsfCenter.y();}
    //Centro inteligente. Devuelve el cetnro según el parámetro estático PCENTER_BARI
    inline float px()
    {
       if ((mFlags&DF_PREFERRED_CENTER_WINDOWED) && (mFlags&DF_WINDOWED_CENTER)) return mWCentroid.x();
       if ((mFlags&DF_PREFERRED_CENTER_PSF) && (mFlags&DF_PSF_CENTER)) return mPsfCenter.x();
       if (mFlags&DF_PREFERRED_CENTER_PEAK) return mPeakCenter.x();
       return mIsoCentroid.x();
    }
    inline float py()
    {
        if ((mFlags&DF_PREFERRED_CENTER_WINDOWED) && (mFlags&DF_WINDOWED_CENTER)) return mWCentroid.y();
        if ((mFlags&DF_PREFERRED_CENTER_PSF) && (mFlags&DF_PSF_CENTER)) return mPsfCenter.y();
        if (mFlags&DF_PREFERRED_CENTER_PEAK) return mPeakCenter.y();
        return mIsoCentroid.y();
    }
    //Centroide de segundo orden
    inline float ccx(){return mCentroid2.x();}
    inline float ccy(){return mCentroid2.y();}
    //Centro semilla o píxel que se usó para detectar la estrella
    inline int sx(){return mSeed.x();}
    inline int sy(){return mSeed.y();}
    //Valor de fondo usado para la estrella si se usó un fondo de tesela de malla
    inline float back(){return mBack;}
    inline float backStdev(){return mBackStdev;}
    //Semieje mayor, menor y ángulo de rotación de la elipse envolvente
    inline float a(){return ma;}
    inline float b(){return mb;}
    inline float theta(){return mTheta;}
    //Rectángulo que contiene la estrella
    inline QRect limits(){return mLimits;}
    inline int pixinstar(){return mPixinStar;}
    inline float radius(){return mRadius;}
    inline int flags(){return mFlags;}
    inline float thres(){return mThres;}
    inline double fluxISO(){return mFluxISO;}
    inline double fluxPhoto(){return mFluxPhoto;}
    inline double fluxWin(){return mFluxWin;}
    inline void addFlag(DetectedFlag flag){ mFlags = (DetectedFlag)(mFlags | flag);}
    inline void removeFlag(DetectedFlag flag){ mFlags = (DetectedFlag)(mFlags & ~flag);}
    inline bool hasFlag(DetectedFlag flag){return mFlags&flag;}
    inline float fwhm(){return mFWHM;}
    inline float eccentricity(){return 1.0f - (mb/ma);}
    //Funciones de ascensión recta y declinación
    inline double ra(){return mRaDec.x();}
    inline double dec(){return mRaDec.y();}
    inline void setRaDec(double ra, double dec){mRaDec = QPointF(ra, dec);}
    inline void setRa(double ra){mRaDec.setX(ra);}
    inline void setDec(double dec){mRaDec.setY(dec);}
    inline void setX(float x){mIsoCentroid.setX(x);}
    inline void setY(float y){mIsoCentroid.setY(y);}
    inline void setRadius(float r){mRadius = r;}

protected:
    DetectedFlag  mFlags;         //Atributos de la estrella
    QPoint        mSeed;          //Píxel semilla desde el que se detectó la estrella
    QPointF       mIsoCentroid;   //Centroide ISO(conectado) calculado para la estrella en X
    QPointF       mWCentroid;     //Windowed center
    QPointF       mPsfCenter;     //Centro de la PSF
    QPointF       mPeakCenter;    //Pico o píxel con mayor valor
    float         mBack;          //Valor de fondo calculado para la estrella
    float         mBackStdev;     //Valor de desviación típica del fondo
    float         mPeak;          //Máximo valor de flujo para un píxel (restado de fondo)
    float         ma;             //Semieje mayor en píxeles
    float         mb;             //Semieje menor en píxeles
    float         mTheta;         //Ángulo con la horizontal en radianes
    QRect         mLimits;        //Límites que contienen a la estrella
    quint16       mPixinStar;     //Número de píxeles detectados por encima del umbral threshold
    float         mRadius;        //Radio estimado para la estrella en píxeles
    float         mThres;         //Umbral de detección
    double        mFluxISO;       //Flujo total de la estrella
    double        mFluxWin;       //Flujo dentro del radio de la estrella
    double        mFluxPhoto;     //Flujo dentro del radio de la estrella
    QPointF       mCentroid2;     //Centroide con momentos de segundo orden
    float         mXYMoment;      //Momento de segundo orden en XY
    float         mFWHM;          //Full width half maximun
    QPointF       mRaDec;         //Ascensión recta y declinación si necesitáramos guardarlas

};

#define MAX_STARTING_STARS_IN_IMAGE     4096    //Como un primer límite superior
#define MAX_DEBLEND_BUFFER               512

struct WindowedCenterStruct;


class CORESHARED_EXPORT StarDetector
        : public BackgroundWorker
{
private:
    enum IterateDirection{
        ITERATEDIRECTION_START, ITERATEDIRECTION_UP, ITERATEDIRECTION_DOWN, ITERATEDIRECTION_RIGHT, ITERATEDIRECTION_LEFT
    };

public:

    enum DetectionFlags{
        COMPUTE_ISO_CENTER          = 1<<0,
        COMPUTE_WINDOWED_CENTER     = 1<<1,
        COMPUTE_PSF_CENTER          = 1<<2,
        DEFAULT_CENTER_PEAK         = 1<<3,
        DEFAULT_CENTER_WINDOWED     = 1<<4,
        DEFAULT_CENTER_PSF          = 1<<5,
        COMPUTE_FLUX_ISO            = 1<<6,
        COMPUTE_FWHM                = 1<<7,
        COMPUTE_SNR                 = 1<<8,
    };


    StarDetector();
    StarDetector(float* data,int width,int height);
    StarDetector(float* data,int width,int height,DetectionParameters parameters);
    virtual ~StarDetector();
    //Devuelve el número de estrellas detectadas
    inline int count(){return mStarCount;}
    inline DetectedStar* stars(){return mStars;}
    void setData(float *data, int width, int height);
    inline void setParameters(DetectionParameters parameters){mParameters = parameters;}

    //Detecta estrellas en el área indicada
    virtual DetectedStar* DetectStars(int min_x, int min_y, int max_x, int max_y);

    //Pasa el control al procedimiento llamante de la memoria ocupada por las estrellas detectadas, obligándole a liberarlo cuando sea necesario
    DetectedStar* TakeMemControlOfStars();
    bool IterateAllPointsOfStar(int xp, int yp, float thres, void *user_data, bool(*pfunction)(StarDetector* detector,int x,int y,float val, void* user_data));
    bool IterateAllPointsOfStar(DetectedStar* star,void *user_data, bool(*pfunction)(StarDetector* detector,int x,int y,float val, void* user_data));

    bool IterateAllPointsOfCircle(float xc, float yc, float radius, void *user_data, bool(*pfunction)(StarDetector* detector,int x,int y,float val, void* user_data));

    inline BackgroundMap* background(){return mBackground;}
    inline DetectionParameters* parameters(){return &mParameters;}
    void setExtBackground(BackgroundMap* bkg);

protected:
    float*                  mData;
    int                     mWidth;
    int                     mHeight;
    DetectionParameters     mParameters;

    //Control de las estrellas encontradas
    DetectedStar*           mStars;
    int                     mStarCount;

    BitMatrix*              mImageMask;
    BitMatrix*              mSmat;
    BackgroundMap*          mExtBackground;     //Me pasan el fondo ya calculado
    BackgroundMap*          mBackground;        //Cuidado, solo se crea una vez y se borra al final
    int                     mStarCapacity;
    DetectedStar            mAuxStar;


    DetectedStar* AddStar();

    void Clear();
    DetectedStar* ConnectedSearchStarOn(int x,int y, float thres, BitMatrix* avoid_pixels=NULL);

    //Determina si un puto está dentro de una estrella
    bool IsInStar(double x, double y);
    //Funciones para la iteración dentro de una estrella
    bool DoIterate(IterateDirection direction, BitMatrix* matrix, float back, float thres, int xp, int yp,void *user_data, bool(*pfunction)(StarDetector* detector,int x,int y,float val,  void* user_data));

    void CalculateBaricenterAccurate(DetectedStar* star);
    void CalculateWindowedCenterAccurate(DetectedStar* star);
    void IterateWindowCenter(WindowedCenterStruct* ws);

    static bool SearchStarFunc(StarDetector* detector, int x,int y,float val, void* user_data);
    static bool BaricenterFunc(StarDetector* detector, int x,int y,float val, void* user_data);
    static bool PhotometryFunc(StarDetector*, int x,int y, float pdata, void* user_data);

    static bool WindowedCenterFunc(StarDetector*, int x,int y,float pdata, void* user_data);
    static float Fwhm(float* histo,int start_idx, int end_idx);

    /**
     * @brief StarInList Indica si una estrella se encuentra en una lista.
     * @param list lista de estrellas
     * @param star estrella a buscar. Los centros deben estar separados menos de 3 píxeles
     * @return
     */
    static bool StarInList(QList<DetectedStar*>* list, DetectedStar* star);
    bool StarInList(DetectedStar* star, DetectedStar* list, int sz);



    //Funciones y datos usados para deblending
    DetectedStar            mDeblendBuffer[MAX_DEBLEND_BUFFER];         //Buffer de estrellas para deblending
    int                     mDeblendStarCount;
    BitMatrix*              mDeblendMat;

    DetectedStar*           AddDeblendStar();
    inline void             RemoveLastDeblendStar(){mDeblendStarCount--;}
    QList<DetectedStar*>*   DeblendStar(DetectedStar*);
    void                    AllocDeblendLevels(int deblend_nthres);
    bool                    InDeblendList(int level, BitMatrix* bitm);


    QList<DetectedStar*>* RecursiveDeblend(int level,float thres,float step,
                                                         float last_flux, float star_x, float star_y, float radius);
    QList<DetectedStar*>* RecursiveDeblend(DetectedStar* star);
    void ComputePsfCenter(DetectedStar* ds);
    void ComputePsfSigma(DetectedStar* ds);
    void ComputePsfAccurate(DetectedStar* ds);
    static void psf_func(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, void *ptr);
    static void psf_grad(const alglib::real_1d_array &c, const alglib::real_1d_array &x, double &func, alglib::real_1d_array &grad, void*ptr);
    static void sigma_func(const alglib::real_1d_array &c, const alglib::real_1d_array &_x, double &func, void* data_helper);
    QList<QList<DetectedStar*>* >  mDeblenLevelStars;

    /**
     * @brief NewStarDetected  inserta una nueva estrella e la lista de estrellas registradas. Calcula el centro de apertura más preciso.
     * @param Datos de la estrella a detectar
     * @return Puntero a la posición de memoria donde se encuentra la nueva estrella
     */
    DetectedStar* NewStarDetected(DetectedStar* st);

    QList<DetectedStar*>* EccTest(DetectedStar *st);

};


class CORESHARED_EXPORT StarDetectorParallel
        : public StarDetector
{
private:
    int  mSize;
    int  mDetRows;
    int  mDetCols;
    int  mDetcount;
    QList<StarDetector*> mDetectors;

public:
    StarDetectorParallel(float* data,int width,int height,DetectionParameters parameters);
    virtual ~StarDetectorParallel();

    virtual DetectedStar* DetectStars(int min_x, int min_y, int max_x, int max_y);


};


#endif // DETECTION_H


















