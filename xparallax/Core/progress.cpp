#include "progress.h"
#include "core.h"
#include  <stdlib.h>
#include <QFileInfo>

WorkFileList::WorkFileList()
{
   mOverwrite = false;
   mOutputDir = "";
}

WorkFileList::~WorkFileList()
{
}

void WorkFileList::Clear()
{
    this->mFilenames.clear();
    this->mFitImages.clear();
    this->mOutputDir = "";
    this->mLastSearchPath = "";
}

void WorkFileList::SetDefaults()
{
    Clear();
}

void WorkFileList::Save(QSettings* settings,const QString& section)
{
    if (mFilenames.count())
    {
        SaveString(settings, section, "last_search_path", QFileInfo(mFilenames.at(0)).absolutePath());
        SaveBool(settings, section, "overwrite", mOverwrite);
        SaveString(settings, section, "output_dir", mOutputDir);
    }
}

void WorkFileList::Load(QSettings* settings,const QString& section)
{
    mLastSearchPath = ReadString(settings, section, "last_search_path", "");
    mOverwrite = ReadBool(settings, section, "overwrite", false);
    mOutputDir = ReadString(settings, section, "output_dir", "");
    mResult.clear();
}

ProgressManager::ProgressManager()
{
    mPosition = 0;
    mCurProgElement = NULL;
    mEnd = false;
    mCancelled = true;
    mValue = 0;
    mNullElement = NULL;
    mLastRemainSecs = -1;
}


ProgressManager::~ProgressManager()
{
    for (int i=0; i<mElements.count(); i++)
        delete mElements[i];
    if (mNullElement) delete mNullElement;
    mElements.clear();
}


ProgressElement* ProgressManager::AddProgres(const QString &name,int value)
{
    ProgressElement *pe = new ProgressElement(this, name, value);
    mElements.append(pe);
    mValue += value;
    return pe;
}

ProgressElement* ProgressManager::AddNullProgress()
{
    if (!mNullElement) mNullElement = new ProgressElement(this, "", 0);
    return mNullElement;
}

void ProgressManager::cancel()
{
    mCancelled = true;
    for (int i=0; i<mElements.count(); i++)
        mElements[i]->cancel();
}

void ProgressManager::UpdatePosition(ProgressElement *source)
{
    if (mEnd || (source == mNullElement)) return;
    mMutex.lock();
    int cur_pos = 0;
    //Busco la posición de mi progreso. Todos los anteriores se suponen terminados
    for (int i=0; i<mElements.count() && mElements[i] != source; i++)
        cur_pos += mElements[i]->value();
    cur_pos += source->position();
    if (cur_pos > mValue)
        cur_pos = mValue;

    //Comprobamos el comienzo
    if (!mCurProgElement)
    {
        mStartDate = QDateTime::currentDateTime();
        OnStart();
    }

    //Controlamos si hemos cambiado de elemento de progreso
    if (mCurProgElement != source)
    {
        OnElementChanged(source);
        OnStatusChanged(source->status());
    }

    if (cur_pos != mPosition)
    {
        OnPositionChanged(mPosition, cur_pos);
        int new_percent = cur_pos * 100 / mValue;
        int old_percent = mPosition * 100 / mValue;
        if (new_percent != old_percent)
            OnProgressChanged(old_percent, new_percent);
        mPosition = cur_pos;

        int remainsecs = remain();
        if (mLastRemainSecs != remainsecs)
        {
            OnRemainTimeChanged(mLastRemainSecs, remainsecs);
            mLastRemainSecs = remainsecs;
        }
    }

    //Controlamos el final
    if (mPosition == mValue)
    {
        mEnd = true;
        OnEnd();
    }

    //Siempre actualizamos el elemento último de llamada
    mCurProgElement = source;
    mMutex.unlock();
}

int ProgressManager::remain()
{
    int ellapsed =  (int) ((QDateTime::currentDateTime().toMSecsSinceEpoch() - mStartDate.toMSecsSinceEpoch())/1000);
    if (mPosition == 0) return -1;
    return (mValue - mPosition) * ellapsed / mPosition;
}

void ProgressManager::UpdateStatus(ProgressElement *source)
{
    if (mEnd || source == mNullElement) return;
    mMutex.lock();
    OnStatusChanged(source->status());
    mMutex.unlock();
}

//--------------------------------------------------------------------------------------
// PROGRESS ELEMENT
//--------------------------------------------------------------------------------------

ProgressElement::ProgressElement(ProgressManager* parent,const QString &name,int value)
{
    mParent = parent;
    mName = name;
    mValue = value;
    mStatus = "";
    mPosition = 0;
    mDone = false;
    mCancelled = false;
}

ProgressElement::~ProgressElement()
{

}

void ProgressElement::SetPosition(int position)
{
    if (position == mPosition) return;
    mPosition = position;
    mParent->UpdatePosition(this);
}

void ProgressElement::SetStatus(const QString &status)
{
    if (status == mStatus) return;
    mStatus = status;
    mParent->UpdateStatus(this);
}


BackgroundWorker::BackgroundWorker(const QString& name)
{
    mName = name;
    mEnded = false;
    mPosition = 0;
    mSize = 0;
    mCancelled = false;
    mStarted = false;
    mLogMsgs = true;
}

BackgroundWorker::~BackgroundWorker()
{

}

void BackgroundWorker::OnStart()
{
    mCancelled = false;
    mEnded = false;
    mLastPosition = -1;
    emit Start_Signal(this);
}

void BackgroundWorker::OnSetup(int size)
{
    mCancelled = false;
    mSize = size;
    mPosition = 0;
    emit Setup_Signal(this, size);
}

void BackgroundWorker::OnEnd()
{
    mEnded = true;
    emit End_Signal(this);
}

void BackgroundWorker::OnError(QString desc)
{
   emit Error_Signal(this, desc);
}

void BackgroundWorker::OnMsg(QString msg)
{
    emit Msg_Signal(this, msg);
}

void BackgroundWorker::OnTitleChange(QString title)
{
    mName = title;
    emit TitleChange_Signal(this, title);
}

void BackgroundWorker::OnMainTitleChange(QString title)
{
    emit MainTitleChange_Signal(this, title);
}

void BackgroundWorker::OnProgress(int position)
{
    if (mEnded) return;
    if (!mStarted)
    {
        mLastPosition = -1;
        emit Start_Signal(this);
    }
    mPosition = MINVAL(mSize, position);
    if (mPosition == mLastPosition) return;
    mLastPosition = position;
    emit Progress_Signal(this, mPosition);
    if (mPosition == mSize) OnEnd();
}

void BackgroundWorker::cancel()
{
    mCancelled = true;
    emit Cancel_Signal(this);
}




