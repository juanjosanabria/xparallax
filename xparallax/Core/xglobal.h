#ifndef XGLOBAL_H
#define XGLOBAL_H

#include "core_global.h"
#include "log.h"
#include <QSettings>
#include <QUuid>


class CORESHARED_EXPORT XGlobal
{
public:
    static QString          BIN_PATH;
    static QString          CONF_PATH;
    static QString          FONT_PATH;
    static QString          LIC_PATH;
    static QString          PLUGIN_PATH;
    static QString          CACHE_PATH;
    static QString          DATA_PATH;
    static QString          OS_TYPE;
    static int              OS_BITS;
    static double           SCREEN_DPI;
    static bool             DEBUG;
    static QSettings*       INI_SETTINGS;
    static Logger*          LOGGER;
    static EnviParameters   APP_SETTINGS;
    static int              LIC_REST;
    static int              LIC_QUOTA;

public:
    XGlobal();

    static void initialize();
    static void End();
    static void Save();
    static void SetDefaultSettings();
    /**
     * @brief GetSettingsPath Devuelve una ruta completa al directorio de configuración de la aplicación
     * @param filename Nombre del archivo del que se solicita la ruta
     * @return Cadena de texto con la ruta completa
     */
    static QString    GetSettingsPath(const QString& filename);
    /**
     * @brief GetDataPath Devuelve una ruta completa al directorio de datos de la aplicación
     * @param filename Nombre del archivo del que se solicita la ruta
     * @return Cadena de texto con la ruta completa
     */
    static QString    GetDataPath(const QString& filename);
    /**
     * @brief GetSettings Obtiene el objeto de configuración indicado. Una vez destruído se guardará.
     * @param filename Nombre del archivo de configuración que se situará en la ruta de configuración por defecto.
     * @return Objeto que deberá ser destruído por el llamante.
     */
    static QSettings* GetSettings(const QString& filename);


};





#endif // XGLOBAL_H

