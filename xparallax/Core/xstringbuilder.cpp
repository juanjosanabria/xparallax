#include "xstringbuilder.h"
#include <string>

#define DIFF_MILLIS_TO_GROW        200

XStringBuilder::XStringBuilder()
{
    mPointer = mStatic;
    *mPointer = 0;
    mCount = 0;
    mLastGrowOP = 0;
    mCapacity = XSTRINGBUILDER_INITIAL_CAPACITY;
}

XStringBuilder::XStringBuilder(char *cad) : XStringBuilder()
{
    append(cad);
}

XStringBuilder::XStringBuilder(const QString& cad) : XStringBuilder()
{
    append(cad);
}

XStringBuilder::XStringBuilder(int capacity) : XStringBuilder()
{
    setCapacity(capacity);
}

void XStringBuilder::rtrim()
{
    for (int i=mCount-1;i>=0&&(mPointer[i]==' '||mPointer[i]=='\t'||mPointer[i]=='\r'||mPointer[i]=='\n');i--)
        mPointer[i]=0;
}

void XStringBuilder::ltrim()
{
    int i = 0;
    for (;i<mCount||mPointer[i]==' '||mPointer[i]=='\t'||mPointer[i]=='\r'||mPointer[i]=='\n';i++);
    i--;
    if (i<0&&i<mCount)
        strcpy(mPointer,mPointer+i);
}

void XStringBuilder::setCapacity(int cap)
{
    if (cap < mCount + 1) cap = mCount+1;
    mPointer[mCapacity] = 0;
    if (cap <= XSTRINGBUILDER_INITIAL_CAPACITY)
    {
        strcpy(mStatic, mPointer);
        if (mPointer != mStatic) delete mPointer;
        mPointer = mStatic;
        mCapacity = XSTRINGBUILDER_INITIAL_CAPACITY;
    }
    else
    {
        char *newdata = new char[cap+1];
        strcpy(newdata, mPointer);
        if (mPointer != mStatic) delete mPointer;
        mPointer = newdata;
        mCapacity = cap;
    }
    mLastGrowOP = QDateTime::currentMSecsSinceEpoch();
}

void XStringBuilder::grow(int min_capacity)
{
    quint64 mseceiff = QDateTime::currentMSecsSinceEpoch() - mLastGrowOP;
    if (mseceiff < DIFF_MILLIS_TO_GROW) min_capacity *=2;
    setCapacity(min_capacity);
}

void XStringBuilder::ungrow()
{
    ASSERT(mCount==strlen(mPointer),"Error MBUFFER con tamaño incorrecto");
    quint64 mseceiff = QDateTime::currentMSecsSinceEpoch() - mLastGrowOP;
    if (mseceiff < DIFF_MILLIS_TO_GROW*2) return;
    if (mPointer == mStatic) return;
    int newcapaicty = mCapacity/2;
    if (newcapaicty < mCount+1) return;
    if (newcapaicty <= XSTRINGBUILDER_INITIAL_CAPACITY)
    {
        strcpy(mStatic, mPointer);
        delete mPointer;
        mPointer = mStatic;
        mCapacity = XSTRINGBUILDER_INITIAL_CAPACITY;
    }
    else
    {
        char* newdata = new char[newcapaicty+1];
        strcpy(newdata, mPointer);
        delete mPointer;
        mPointer = newdata;
        mCapacity = newcapaicty;
    }
    mLastGrowOP = QDateTime::currentMSecsSinceEpoch();
}

void XStringBuilder::clear()
{
    mCount = 0;
    *mPointer = 0;
    ungrow();
}

XStringBuilder::~XStringBuilder()
{
    if (mPointer != mStatic) delete mPointer;
}

void XStringBuilder::append(const char *c)
{
    int newlen = mCount + (int)strlen(c);
    if (mCapacity < newlen + 1) grow(newlen*2);
    strcat(mPointer + mCount, c);
    mCount = newlen;
    ungrow();
}

void XStringBuilder::append(const QString& cad)
{
    append(cad.toLatin1().data());
}

void XStringBuilder::append(const QByteArray& cad)
{
    append((char*)cad.data());
}

void XStringBuilder::append(const QLatin1String& cad)
{
    append((char*)cad.data());
}

void XStringBuilder::append(char ch)
{
    char buff[2] = {ch, 0};
    append(buff);
}

void XStringBuilder::append(const QChar& ch)
{
    char buff[2] = {ch.toLatin1(), 0};
    append(buff);
}

void XStringBuilder::append(float f)
{
    char buff[34];
    sprintf(buff, "%f", f);
    append(buff);
}

void XStringBuilder::append(double f)
{
    char buff[34];
    sprintf(buff, "%f", f);
    append(buff);
}

void XStringBuilder::append(signed int i)
{
    char buff[22];
    sprintf(buff, "%d", i);
    append(buff);
}

void XStringBuilder::append(unsigned int i)
{
    char buff[22];
    sprintf(buff, "%u", i);
    append(buff);
}

void XStringBuilder::append(signed short i)
{
    char buff[22];
    sprintf(buff, "%d", i);
    append(buff);
}

void XStringBuilder::append(unsigned short i)
{
    char buff[22];
    sprintf(buff, "%u", i);
    append(buff);
}

void XStringBuilder::append(signed long i)
{
    char buff[22];
    sprintf(buff, "%ld", i);
    append(buff);
}

void XStringBuilder::append(unsigned long i)
{
    char buff[22];
    sprintf(buff, "%lu", i);
    append(buff);
}

void XStringBuilder::append(qint64 i)
{
    char buff[32];
    sprintf(buff, "%lld", i);
    append(buff);
}

void XStringBuilder::append(quint64 i)
{
    char buff[32];
    sprintf(buff, "%llu", i);
    append(buff);
}

void XStringBuilder::append(const void *ptr)
{
    char buff[32];
    if (sizeof(void*) == 32) sprintf(buff, "0x%lx", (long unsigned int)ptr);
    else sprintf(buff, "0x%llx", (long long unsigned int)ptr);
    append(buff);
}

void XStringBuilder::appendFormat(const char *fmt, ...)
{
    va_list args;
    va_start (args, fmt);
    int tot = mCapacity - mCount - 1;                          //Total de hueco disponible
    int vapp = vsnprintf(mPointer + mCount, tot, fmt, args);   //Formateamos a ver qué tal
    int gcont = 0;
    while (vapp < 0)                                           //Si no ha habido éxito reintentamos
    {
        grow(mCapacity*2);
        tot = mCapacity - mCount;
        vapp = vsnprintf(mPointer + mCount, tot, fmt, args);
        gcont++;
        ASSERT(gcont < 10, "Too many grow operations in XStringBuilder");
    }
    mCount += vapp;
    va_end(args);
}
















