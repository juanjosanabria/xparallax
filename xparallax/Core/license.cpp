#include "license.h"
#include "xglobal.h"
#include "util.h"
#include <QFile>
#include <QTextStream>
#include <QCryptographicHash>
#include <QDomDocument>
#include <QDirIterator>
#include <QNetworkInterface>
#include <QHostInfo>

#define SIG "Sig"

LicBase::LicBase()
{
    mIsNull = false;
}

LicBase::LicBase(QString& filename, QString rootName)
{
    mIsNull = true; mValid = false;
    QFile fil(filename);
    if (!fil.open(QFile::OpenModeFlag::ReadOnly)) return;
    QDomDocument dom;
    bool ok = dom.setContent(&fil, &mErrorMsg);
    QDomElement root = dom.documentElement();
    if (root.nodeName() != rootName)
    {
        mErrorMsg = "Invalid root element: '" + root.nodeName() + "' does not match with '" + rootName + "'";
        return;
    }
    QDomNodeList lst = root.childNodes();
    for (int i=0; i<lst.count(); i++)
    {
        QDomNode node = lst.at(i);
        setField(node.nodeName(), node.childNodes().at(0).toText().nodeValue());
    }
    mSecretSig = base64Decode(SECRET_KEY);
    mSecretSig = base64Decode(mSecretSig);
    if (!ok) return;
    fil.close();
    mIsNull = false;
    mValid = validSignature();
    return;
}

bool LicBase::validSignature()
{
    QString hs = hash();
    QString fl = field(SIG).toLower();
    DINFO("Signature: %s; IN XML: %s", PTQ(hs), PTQ(fl));
    return hs == fl;
}


QString LicBase::hash()
{
   QString sb = "";
   for (int i=0; i<fieldCount(); i++)
   {
       QString fname = fieldName(i);
       if (fname == SIG) continue;
       QString val = field(fname);
       if (i) sb += ",";
       sb += fname + "|" + val;
   }
   DINFO("Lcense full stack: %s", PTQ(sb));
   sb = QString(QCryptographicHash::hash((sb+","+mSecretSig).toLatin1(),QCryptographicHash::Md5).toHex()).toLower();
   return sb;
}


QString LicBase::base64Encode(const QString& cad)
{
    return QString(cad.toLatin1().toBase64());
}

QString LicBase::base64Decode(const QString& cad)
{
    return QString(QByteArray::fromBase64(cad.toLatin1()));
}

License LicBase::checkLicense(QString* msg)
{
    License invalid; invalid.setNull(true); invalid.setSignature("");
    XGlobal::LIC_REST = LIC_RESTRICTION::ALL_RESTRICTIONS;
    LicID lid = LicID::search(XGlobal::LIC_PATH);
    if (lid.isNull() || lid.invalid())
    {
        *msg = "Machine identifier not found";
        return invalid;
    }
    License lcs = License::search(XGlobal::LIC_PATH, lid.field("ID"));
    if (lcs.isNull())
    {
        *msg = "Not a valid license found";
        return invalid;
    }
    //Restricción
    int restriction = 0;
    QString res_str = lcs.field("Restriction");
    if (res_str != "") restriction = res_str.toInt();
    XGlobal::LIC_REST = restriction;
    //Restricción de imágenes
    XGlobal::LIC_QUOTA = 2000;
    if (lcs.field("Quota").count() > 0)
    {
        bool ok = false;
        int quota = lcs.field("Quota").toInt(&ok, 10);
        if (ok && quota > 0) XGlobal::LIC_QUOTA = quota;
    }
    return lcs;
}

void LicBase::setField(const QString& name, const QString& value)
{
    for (int i=0; i<mFields.count(); i++)
    {
        if (mFields[i][0] == name)
        {
            mFields[i][1] = value;
            return;
        }
    }
    QStringList lst;
    lst << name << value;
    mFields.append(lst);
}


QString  LicBase::field(const QString& name)
{
    for (int i=0; i<mFields.count(); i++)
        if (mFields[i][0] == name) return mFields[i][1];
    return "";
}

bool LicID::validMac()
{
    QString lic_mac = field("MacAddr").toUpper();
    if (lic_mac.count() == 0) return true;
    QStringList macs;
    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
    {
         QString addr = netInterface.hardwareAddress().toUpper();
         if (addr == lic_mac) return true;
    }
    return false;
}


bool LicID::validUser()
{
    QString lic_user = field("User").toLower();
    if (lic_user.count() == 0) return true;
    QString usr = qgetenv("USER");
    if (usr.count() == 0) usr = qgetenv("USERNAME");
    usr = usr.toLower();
    if (usr.indexOf("\\") > 0) usr = usr.mid(usr.indexOf("\\")+1);
    return usr == lic_user;
}


bool LicID::validHostName()
{
    QString lic_host_name = field("HostName").toLower();
    if (lic_host_name.count() == 0) return true;
    QString host_name = QHostInfo::localHostName().toLower();
    return host_name == lic_host_name;
}

LicID LicID::search(const QString& path)
{
    QDirIterator iter(path);
    while (iter.hasNext())
    {
        QString fpath = iter.next();
        QString fname = QFileInfo(fpath).fileName();
        if (fname.startsWith('.')) continue;
        QString ext = QFileInfo(fpath).suffix();
        if (ext.toLower() != "id") continue;
        DINFO("Checking ID file: '%s'", PTQ(fpath));
        LicID id = LicID(fpath);
        if (!id.isNull()) return id;
    }
    LicID li_null; li_null.mIsNull = true;
    return li_null;
}



License License::search(const QString& path, const QString& sid)
{
    QVector<License> vlic;
    QDirIterator iter(path);
    while (iter.hasNext())
    {
        QString fpath = iter.next();
        QString fname = QFileInfo(fpath).fileName();
        if (fname.startsWith('.')) continue;
        QString ext = QFileInfo(fpath).suffix();
        if (ext.toLower() != "lic") continue;
        DINFO("Checking ID file: '%s'", PTQ(fpath));
        License lic = License(fpath);
        if (!lic.isNull() && lic.isValid() && lic.field("Identity") == sid)
            vlic.append(lic);
    }
    //Eliminar las licencias inválidas del vector
    QDateTime today = QDateTime::currentDateTime();
    for (int i=0; i<vlic.count(); i++)
    {
        //Fecha y hora
        QDateTime from = Util::ParseDate(vlic[i].field("ValidFrom")).addDays(-1);
        QDateTime to = Util::ParseDate(vlic[i].field("ValidTo")).addDays(+1);
        if (today < from || today > to)
        {
            vlic.removeAt(i);
            i--;
        }
    }
    if (!vlic.count())
    {
        License lic; lic.mIsNull = true;
        return lic;
    }
    return vlic[0];
}



/*
License::License()
{
    setField("ValidFrom","");
    setField("ValidTo","");
    mValid = true;
    mLicRestriction = NO_RESTRICTION;
}

bool License::expired()
{
    QDateTime current = QDateTime::currentDateTime();
    return current < mValidFrom || current > mValidTo;
}

bool License::saveToFile(const QString& filename)
{
    QFile fil(filename);
    if (!fil.open(QFile::OpenModeFlag::WriteOnly)) return false;
    QTextStream ts(&fil);
    QString sig = hash();
    ts << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
       << "<License xmlns=\"s3t\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"s3t NewXMLSchema.xsd\">\n";
    for (int i=0; i<mFields.count(); i++)
        ts << "\t<" << mFields[i][0] << ">" << mFields[i][1] << "</"  << mFields[i][0] << ">\n";
    ts << "\t<" SIG ">" << sig << "</" SIG ">\n";
    ts << "</License>\n";
    fil.close();
    return true;
}


License License::loadFromFile(const QString& filename)
{
    License lic; lic.mValid = false;
    QFile fil(filename);
    if (!fil.open(QFile::OpenModeFlag::ReadOnly)) return lic;

    QDomDocument dom;
    bool ok = dom.setContent(&fil,&lic.mErrorMsg);
    QDomElement root = dom.documentElement();

    if (root.nodeName() != "License")
    {
        lic.mErrorMsg = "Invalid root element: " + root.nodeName();
        return lic;
    }
    QDomNodeList lst = root.childNodes();
    for (int i=0; i<lst.count(); i++)
    {
        QDomNode node = lst.at(i);
        lic.setField(node.nodeName(), node.childNodes().at(0).toText().nodeValue());
    }
    //Comprobar campos requeridos
    lic.mValidFrom = Util::ParseDate(lic.field("ValidFrom"));
    if (lic.mValidFrom == INVALID_DATETIME)
    {
        lic.mErrorMsg = "Invalid start date: " + lic.field("ValidFrom");
        return lic;
    }
    lic.mValidTo = Util::ParseDate(lic.field("ValidTo"));
    if (lic.mValidTo == INVALID_DATETIME)
    {
        lic.mErrorMsg = "Invalid start date: " + lic.field("ValidFrom");
        return lic;
    }
    if (lic.field("Restriction").length())
    {
        lic.mLicRestriction = (LIC_RESTRICTION)lic.field("Restriction").toInt(&ok);
    }
    else
    {
        lic.mLicRestriction = NO_RESTRICTION;
    }
    lic.mSecretSig = License::base64Decode("MTgyOTkyMDMxMjA0OTEyODM4MTIzOTk5MjE4MjkyMDE5MjM4ODEyODM3NTAwMTIwMTI5Mg==");
    if (!ok) return lic;
    fil.close();
    lic.mValid = true;
    return lic;
}*/
