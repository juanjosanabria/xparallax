#ifndef MSVC_H
#define MSVC_H

#include <qmath.h>
#include <float.h>

//********************************************************************************/
// INCLUDES NECESARIOS PARA CUANDO SE COMPILA CON MSVC Ó MINGW
//********************************************************************************/

#ifdef _MSC_VER
    //Disable CRT warnings

    //Tener defines de MATH
    #define _USE_MATH_DEFINES

    //Función redondeo
    /*
    #ifndef round
    #define round(number) ( number < 0.0 ? ceil(number - 0.5) : floor(number + 0.5))
    #endif
    */

    //Definición de NANs e infinitos
    #ifndef NAN
    static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
    #define NAN (*(const float *) __nan)
    #endif

    #ifndef isnan
    #define isnan _isnan
    #endif
    #ifndef isinf
    #define isinf(a) (!_finite(a))
    #endif

    #define INFINITO   (HUGE)
    #define _INFINITO  (-HUGE)
    #define snprintf _snprintf
#else //Versiones Linux
    #define INFINITO   (HUGE_VALF)
    #define _INFINITO  (-HUGE_VALF)
    #define MAXINT      0xEFFFFFFF
    #define MININT      ((int)~MAXINT)
    #ifndef isnan
    #define isnan _isnan
    #endif
    #ifndef isinf
    #define isinf(a) (!_finite(a))
    #endif
#endif

#endif // MSVC_H
