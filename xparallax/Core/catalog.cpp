#include "catalog.h"
#include "xglobal.h"
#include "plateconstants.h"
#include "xstringbuilder.h"
#include <QFileInfo>
#include <QDir>

/********** PARÁMETROS POR DEFECTO DE CREACIÓN DE CATÁLOGOS***********/
#define DEF_cat_output                          true
#define DEF_cat_fields                      QStringList() << CATFIELD_POS << CATFIELD_IMG_X << CATFIELD_IMG_Y  \
                                                          << CATFIELD_RA_HOUR << CATFIELD_DEC_DEG << CATFIELD_FLUX_ISO
#define DEF_cat_fmt                         "TXT"
#define DEF_cat_sort                        false
#define DEF_cat_sort_field                  "FLUX_ISO"
#define DEF_cat_sort_order                  false
#define DEF_flux_output                     false
#define DEF_flux_fields                     QStringList() << CATFIELD_POS << CATFIELD_DATA_COUNT << CATFIELD_RA_HMS << CATFIELD_DEC_DMS
#define DEF_flux_type                       "FLUX_ISO"
#define DEF_flux_fmt                        "TXT"
#define DEF_flux_heading                    2
#define DEF_flux_sort                       false
#define DEF_flux_sort_field                 "FLUX_ISO"
#define DEF_flux_sort_order                 false
#define DEF_filt_output                     false
#define DEF_filt_sources                    QStringList()
#define DEF_filt_separation                 2.0f
/********** CAMPOS DISPONIBLES PARA USO EN  CATÁLOGOS O TABLA DE FLUJO  ***********/
#define CATFIELD_POS                        "POS"
#define CATFIELD_NAME                       "NAME"
#define CATFIELD_DATA_COUNT                 "DATA_COUNT"
#define CATFIELD_IMG_X                      "IMG_X"
#define CATFIELD_IMG_Y                      "IMG_Y"
#define CATFIELD_RA_DEG                     "RA_DEG"
#define CATFIELD_DEC_DEG                    "DEC_DEG"
#define CATFIELD_RA_HOUR                    "RA_HOUR"
#define CATFIELD_RA_HMS                     "RA_HMS"
#define CATFIELD_DEC_DMS                    "DEC_DMS"
#define CATFIELD_FLUX_ISO                   "FLUX_ISO"
#define CATFIELD_FLUX_WIN                   "FLUX_WIN"
#define CATFIELD_FLUX_PHOTO                 "FLUX_PHOTO"
#define CATFIELD_RADIUS_PIX                 "RADIUS_PIX"
#define CATFIELD_RADIUS_ARCSEC              "RADIUS_ARCSEC"
#define CATFIELD_FWHM_PIX                   "FWHM_PIX"
#define CATFIELD_FWHM_ARCSEC                "FWHM_ARCSEC"
#define CATFIELD_AREA                       "AREA"
#define CATFIELD_BKG                        "BKG"
#define CATFIELD_BKG_STDEV                  "BKG_STDV"
/********** FORMATOS DISPONIBLES PARA LA ORDENACIÓN EN CATÁLOGO ****/
#define CATSORT_X                           "X"
#define CATSORT_Y                           "Y"
#define CATSORT_RA                          "RA"
#define CATSORT_DEC                         "DEC"
#define CATSORT_FLUX_ISO                    "FLUX_ISO"
#define CATSORT_FLUX_WIN                    "FLUX_WIN"
#define CATSORT_FLUX_PHOTO                  "FLUX_PHOTO"
#define CATSORT_AREA                        "AREA"
#define CATSORT_RADIUS                      "RADIUS"
/********** FORMATOS DISPONIBLES PARA LA SALIDA EN CATÁLOGOS ******/
#define CATFMT_TXT                          "TXT"
#define CATFMT_VOT                          "VOT"
#define CATFMT_CSV                          "CSV"
/********** FORMATOS DISPONIBLES PARA LA SALIDA EN CATÁLOGOS ******/


const char* CatalogParameters::AVAILABLE_CAT_FIELDS[] = {
    CATFIELD_POS,          "Source position",
    CATFIELD_IMG_X,        "Image X coordinate",
    CATFIELD_IMG_Y,        "Image Y coordinate",
    CATFIELD_RA_DEG,       "Right ascension in degrees",
    CATFIELD_DEC_DEG,      "Declination in degrees",
    CATFIELD_RA_HOUR,      "Right ascension in hours",
    CATFIELD_RA_HMS,       "Right ascension in HH MM SS.SS format",
    CATFIELD_DEC_DMS,      "Declination in DD MM SS.SS format",
    CATFIELD_FLUX_ISO,     "Iso flux of the source",
    //CATFIELD_FLUX_WIN,     "Flux of the source inside the radius",
    //CATFIELD_FLUX_PHOTO,   "Photometric flux",
    CATFIELD_RADIUS_PIX,   "Radius of the source in pixels",
    CATFIELD_RADIUS_ARCSEC,"Radius of the source in arcsec",
    //CATFIELD_FWHM_PIX,     "Full width half maximum in pixels",
    //CATFIELD_FWHM_ARCSEC,  "Full width half maximum in arcsec",
    CATFIELD_AREA,         "Area of the source in pixels",
    CATFIELD_BKG,          "Background level",
    CATFIELD_BKG_STDEV,    "Standard deviation of the background level",
    NULL
};

const char* CatalogParameters::AVAILABLE_FLUX_FIELDS[] ={
    CATFIELD_POS,          "Source position",
    CATFIELD_RA_HMS,       "Right ascension in HH MM SS.SS format",
    CATFIELD_DEC_DMS,      "Declination in DD MM SS.S format",
    CATFIELD_NAME,         "Source name",
    CATFIELD_DATA_COUNT,   "Number of images where the source was detected",
    CATFIELD_RA_DEG,       "Right ascension in degrees",
    CATFIELD_DEC_DEG,      "Declination in degrees",
    CATFIELD_RA_HOUR,      "Right ascension in hours",
    NULL
};

const char* CatalogParameters::SORTABLE_FIELDS[] ={
   CATFIELD_FLUX_ISO,
   //CATSORT_FLUX_WIN, CATFIELD_FLUX_PHOTO, -- No implementados de momento
   CATSORT_RADIUS,
   CATSORT_X, CATSORT_Y, CATSORT_RA, CATSORT_DEC ,CATSORT_AREA,
   NULL
};

const char* CatalogParameters::OUTPUT_FORMATS[] = {
    CATFMT_TXT,          "Text file",
    CATFMT_CSV,          "Semicolon delimited csv",
    //CATFMT_VOT,          "VO table", No implementado de momento
    NULL
};

QMutex              CatalogParameters::SORT_MUTEX;
CatalogParameters*  CatalogParameters::SORT_PARAMETERS;
QStringList         CatalogParameters::SORT_ORDER;
CatalogComputer*    CatalogComputer::CURRENT_CATALOG_COMPUTER = NULL;
QMutex              CatalogComputer::SORT_MUTEX;
QString             CatalogComputer::SORT_FIELD;


CatalogParameters::CatalogParameters()
{
    SetDefaults();
}

CatalogParameters::~CatalogParameters()
{

}

void CatalogParameters::SetDefaults()
{
    SetDefaultsCat();
    SetDefaultsFlux();
    SetDefaultFilter();
}

void CatalogParameters::SetDefaultsCat()
{
    //Catálogo de la imagen
    cat_output = DEF_cat_output;      //Se generará catálogo de cada imagen?
    cat_fields = DEF_cat_fields;      //Campos de salida en los archivos de catálogo
    cat_fmt = DEF_cat_fmt;            //Formato de salida de los archivos de catálogo
    //Opciones de ordenación
    cat_sort = DEF_cat_sort;                            //Indica cómo ordenar las fuentes
    cat_sort_field = DEF_cat_sort_field;                //Indica el campo de ordenación
    cat_sort_order = DEF_cat_sort_order;                //Ascendente=true o descendente=false
}

void CatalogParameters::SetDefaultsFlux()
{
    flux_output = DEF_flux_output;
    flux_type = DEF_flux_type;                  //Indica si se generarán las tablas de flujo
    flux_fmt = DEF_flux_fmt;                    //Tipo de flujo a incluir en el archivo
    flux_heading = DEF_flux_heading;            //Indica el tipo de encabezado para cada flujo
    flux_sort = DEF_flux_sort;                  //Indica si se ordenarán o no las fuentes
    flux_sort_field = DEF_flux_sort_field;      //Indica el campo de ordenaión de las fuentes
    flux_sort_order = DEF_flux_sort_order;      //Indica el orden, ascendente o descendetne de la ordenación
}

void CatalogParameters::SetDefaultFilter()
{
    //Filtrado de fuentes
    filt_output = DEF_filt_output;          //Indica si se hará un filtrado de fuentes
    filt_sources = DEF_filt_sources;        //Fuentes, coordenadas separadas por coma, fuentes separadas por punto y coma
    filt_separation = DEF_filt_separation;  //Separación en segundos de arco para detectar dos fuentes como iguales
}

void CatalogParameters::Save(QSettings* settings,const QString& section)
{
    //Catálogo de la imagen
    SaveBool(settings, section, "cat_output", cat_output);
    SaveStringList(settings, section, "cat_fields", cat_fields);
    SaveStringList(settings, section, "cat_fields_order", cat_fields_order);
    SaveString(settings, section, "cat_fmt", cat_fmt);
    SaveBool(settings, section, "cat_sort", cat_sort);
    SaveString(settings, section, "cat_sort_field", cat_sort_field);
    SaveBool(settings, section, "cat_sort_order", cat_sort_order);
    //Tabla de flujo
    SaveBool(settings, section, "flux_output", flux_output);
    SaveStringList(settings, section, "flux_fields", flux_fields);
    SaveStringList(settings, section, "flux_fields_order", flux_fields_order);
    SaveString(settings, section, "flux_type", flux_type);
    SaveString(settings, section, "flux_fmt", flux_fmt);
    SaveInt(settings, section, "flux_heading", flux_heading);
    SaveBool(settings, section, "flux_sort", flux_sort);
    SaveString(settings, section, "flux_sort_field", flux_sort_field);
    SaveBool(settings, section, "flux_sort_order", flux_sort_order);
    //Filtrado de fuentes
    SaveBool(settings, section, "filt_output", filt_output);
    SaveStringList(settings, section, "filt_sources", filt_sources);
    SaveFloat(settings, section, "filt_separation", filt_separation);
}

void CatalogParameters::Load(QSettings* settings,const QString& section)
{
    //Catálogo de la imagen
    cat_output = ReadBool(settings, section, "cat_output", DEF_cat_output);
    cat_fields = ReadStringList(settings, section, "cat_fields", DEF_cat_fields);
    cat_fields_order = ReadStringList(settings, section, "cat_fields_order", QStringList());
    for (int i=0;AVAILABLE_CAT_FIELDS[i];i+=2)
        if (cat_fields_order.indexOf(AVAILABLE_CAT_FIELDS[i]) == -1) cat_fields_order.append(AVAILABLE_CAT_FIELDS[i]);
    cat_fmt = ReadString(settings, section, "cat_fmt", DEF_cat_fmt);
    cat_sort = ReadBool(settings, section, "cat_sort", DEF_cat_sort);
    cat_sort_field = ReadString(settings, section,"cat_sort_field", DEF_cat_sort_field);
    cat_sort_order = ReadBool(settings, section, "cat_sort_order", DEF_cat_sort_order);
    //Tabla de flujo
    flux_output = ReadBool(settings, section, "flux_output", DEF_flux_output);
    flux_fields = ReadStringList(settings, section, "flux_fields", DEF_flux_fields);
    flux_fields_order = ReadStringList(settings, section, "flux_fields_order", QStringList());
    for (int i=0;AVAILABLE_FLUX_FIELDS[i];i+=2)
        if (flux_fields_order.indexOf(AVAILABLE_FLUX_FIELDS[i]) == -1) flux_fields_order.append(AVAILABLE_FLUX_FIELDS[i]);
    flux_type = ReadString(settings, section, "flux_type", DEF_flux_type);
    flux_fmt = ReadString(settings, section, "flux_fmt", DEF_flux_fmt);
    flux_heading = ReadInt(settings, section, "flux_heading", DEF_flux_heading);
    flux_sort = ReadBool(settings, section, "cat_sort", DEF_flux_sort);
    flux_sort_field = ReadString(settings, section,"flux_sort_field", DEF_flux_sort_field);
    flux_sort_order = ReadBool(settings, section, "flux_sort_order", DEF_flux_sort_order);
    //Filtrado de fuentes
    filt_output = ReadBool(settings, section, "filt_output", DEF_filt_output);
    filt_sources = ReadStringList(settings, section, "filt_sources", QStringList());
    filt_separation = ReadInt(settings, section, "filt_separation", DEF_filt_separation);
}


void CatalogParameters::SortFields(QStringList* par, const char** source_list)
{
    SORT_MUTEX.lock();
    SORT_PARAMETERS = this;
    SORT_ORDER = cat_fields;
    for (int i=0; source_list[i]; i++)
        if (SORT_ORDER.indexOf(source_list[i]) == -1)
            SORT_ORDER.append(source_list[i]);
    qSort(par->begin(), par->end(), LessThan);
    SORT_MUTEX.unlock();
}


int CatalogParameters::LessThan(QString a, QString b)
{
    int idxa = SORT_ORDER.indexOf(a);
    int idxb = SORT_ORDER.indexOf(b);
    return idxa < idxb;
}

bool CatalogParameters::needAstrometry()
{
    return cat_fields.contains(CATFIELD_RA_DEG) ||
            cat_fields.contains(CATFIELD_DEC_DEG) ||
            cat_fields.contains(CATFIELD_RA_HMS) ||
            cat_fields.contains(CATFIELD_DEC_DMS) ||
            cat_fields.contains(CATFIELD_RADIUS_ARCSEC) ||
            cat_fields.contains(CATFIELD_FWHM_ARCSEC) ||
            flux_output ||
            filt_sources.length() ||
            (cat_sort && (cat_sort_field == CATSORT_RA || cat_sort_field == CATSORT_DEC)) ||
            (flux_sort && (flux_sort_field == CATSORT_RA || flux_sort_field == CATSORT_DEC))
            ;
}

QString CatalogParameters::getDescCatField(const QString& field)
{
    for (int i=0; AVAILABLE_CAT_FIELDS[i]; i+=2)
        if (field == AVAILABLE_CAT_FIELDS[i]) return AVAILABLE_CAT_FIELDS[i+1];
    return "";
}

QString CatalogParameters::getDescFluxField(const QString &field)
{
    for (int i=0; AVAILABLE_FLUX_FIELDS[i]; i+=2)
        if (field == AVAILABLE_FLUX_FIELDS[i]) return AVAILABLE_FLUX_FIELDS[i+1];
    return "";
}

QString CatalogParameters::getDescFormat(const QString& fmt)
{
    for (int i=0; OUTPUT_FORMATS[i]; i+=2)
        if (fmt == OUTPUT_FORMATS[i]) return OUTPUT_FORMATS[i+1];
    return "";
}

QString CatalogParameters::getFormatFromDesc(const QString& desc)
{
     for (int i=0; OUTPUT_FORMATS[i]; i+=2)
        if (desc == OUTPUT_FORMATS[i+1]) return OUTPUT_FORMATS[i];
    return "";
}

CatalogComputer::CatalogComputer() : BackgroundWorker("Creating image catalog")
{
    mDetector = NULL;
    mOutputCatFields = NULL;
    mOutputFluxFields = NULL;
    mCurrentImage = NULL;
    mCurrentImageControl = false;
}

CatalogComputer::~CatalogComputer()
{
    ClearMemory();
}

int CatalogComputer::steps()
{
    return MAXVAL(mInputFiles->fitimages()->count(), mInputFiles->filenames()->count()) *
                (mCatPar->flux_output ? 2 : 1);
}

bool CatalogComputer::needSave()
{
    if (mInputFiles->filenames()->count()) return true;
    if (mInputFiles->fitimages()->length() == 1) return false;
    return true;
}

void  CatalogComputer::ClearMemory()
{
   for (int i=0; i<mDetectedStars.count(); i++)
        delete mDetectedStars[i];
   mDetectedStars.clear();
   if (mDetector) delete mDetector;
   mDetector = NULL;
   if (mOutputCatFields)
   {
       for (int i=0; mOutputCatFields[i]; i++)
           delete mOutputCatFields[i];
       mOutputCatFields = NULL;
   }
   if (mOutputFluxFields)
   {
       for (int i=0; mOutputFluxFields[i]; i++)
           delete mOutputFluxFields[i];
       mOutputFluxFields = NULL;
   }
   if (mCurrentImage && mCurrentImageControl) delete mCurrentImage;
   mCurrentImage = NULL;
}

void CatalogComputer::Sort(QVector<DetectedStar>* stars, QString field, bool ascending)
{
    SORT_MUTEX.lock();
    CURRENT_CATALOG_COMPUTER = this;
    SORT_FIELD = field;
    qSort(stars->begin(), stars->end(), ascending ? LessThan : GreatherThan);
    CURRENT_CATALOG_COMPUTER = NULL;
    SORT_MUTEX.unlock();
}

int CatalogComputer::LessThan(DetectedStar a, DetectedStar b)
{
    if (SORT_FIELD == CATSORT_FLUX_WIN)
        return a.fluxISO() > b.fluxISO();
    else if (SORT_FIELD == CATSORT_FLUX_ISO)
        return a.fluxISO() > b.fluxISO();
    else if (SORT_FIELD == CATSORT_FLUX_PHOTO)
        return a.fluxPhoto() > b.fluxPhoto();
    else if (SORT_FIELD == CATSORT_X)
        return a.x() > b.x();
    else if (SORT_FIELD == CATSORT_Y)
        return a.y() > b.y();
    else if (SORT_FIELD == CATSORT_RADIUS)
        return a.radius() > b.radius();
    else if (SORT_FIELD == CATSORT_AREA)
        return a.pixinstar() > b.pixinstar();
    else if (SORT_FIELD == CATSORT_RA)
        return a.ra() < b.ra();
    else if (SORT_FIELD == CATSORT_DEC)
        return a.dec() < b.dec();
    return 0;
}

int CatalogComputer::GreatherThan(DetectedStar a, DetectedStar b)
{
    return LessThan(a, b) ?  0 : 1;
}

bool CatalogComputer::doWork(WorkFileList* wf, DetectionParameters* detpar, CatalogParameters* catpar)
{
    ClearMemory();
    mInputFiles = wf;
    mDetectPar = detpar;
    mCatPar = catpar;
    mImageDates.clear();

    //Reorganizamos parámetros
    CreateFilterList();
    mDetectPar->flags |= StarDetector::COMPUTE_FLUX_ISO;

    if (needSave())
    {
        //Generar los campos de salida del catálogo
        mOutputCatFields = new  char*[catpar->cat_fields.length()+1];
        for (int i=0; i<catpar->cat_fields.length(); i++)
        {
           mOutputCatFields[i] = new char[catpar->cat_fields[i].length()+1];
           strcpy(mOutputCatFields[i], catpar->cat_fields[i].toLatin1().data());
        }
        mOutputCatFields[catpar->cat_fields.length()] = 0;

        //Generar los campos de salida de la tabla de flujo
        mOutputFluxFields = new char*[catpar->flux_fields.length()+1];
        for (int i=0; i<catpar->flux_fields.length(); i++)
        {
           mOutputFluxFields[i] = new char[catpar->flux_fields[i].length()+1];
           strcpy(mOutputFluxFields[i], catpar->flux_fields[i].toLatin1().data());
        }
        mOutputFluxFields[catpar->flux_fields.length()] = 0;
    }

    OnSetup(needSave() ? steps() * 100 : 100);
    OnStart();
    bool ok = ComputeCatalog();
    OnEnd();
    return ok;
}

void CatalogComputer::CreateFilterList()
{
    mFilteredSources.clear();
    if (!mCatPar->filt_output) return;
    mFilteredSources.reserve(mCatPar->filt_sources.count());
    for (int i=0; i<mCatPar->filt_sources.count(); i++)
    {
        QStringList lst = mCatPar->filt_sources.at(i).split(',');
        if (lst.count() != 3) continue;
        CatFilteredSource fs;
        fs.name = lst[0].trimmed();
        fs.ra = Util::ParseRA(lst[1]);
        fs.dec = Util::ParseDEC(lst[2]);
        if (IS_INVALIDF(fs.ra) || IS_INVALIDF(fs.dec)) continue;
        mFilteredSources.append(fs);
    }
}

CatFilteredSource* CatalogComputer::GetFiltered(double ra, double dec)
{
    for (int i=0; i<mFilteredSources.count(); i++)
    {
        double dist = fabs(Util::Adist(ra, dec, mFilteredSources.data()[i].ra, mFilteredSources.data()[i].dec))
                * 3600;
        if (dist < mCatPar->filt_separation) return mFilteredSources.data() + i;
    }
    return NULL;
}

bool CatalogComputer::ComputeCatalog()
{
    for (int i=0; i< MAXVAL(mInputFiles->fitimages()->count(), mInputFiles->filenames()->count()) && !cancelled(); i++)
    {
        mCurFile = i;
        QVector<DetectedStar>* list = NULL;
        QString catalog_filename = "";
        QString filename = "";
        QString basename = "";
        //Detectar las fuentes
        OnProgress(i*100);
        if (mInputFiles->fitimages()->count())
        {
            filename = mInputFiles->fitimages()->at(i)->filename();
            basename = QFileInfo(filename).baseName();
            INFO("Processing file: %s", PTQ(mInputFiles->fitimages()->at(i)->filename()));
            OnTitleChange(QString("Processing file: <b>%1</b>").arg(basename));
            list = Detect(mInputFiles->fitimages()->at(i));
        }
        else
        {
            filename = mInputFiles->filenames()->at(i);
            basename = QFileInfo(filename).baseName();
            INFO("Processing file: %s", PTQ(mInputFiles->filenames()->at(i)));
            OnTitleChange(QString("Processing file: <b>%1</b>").arg(basename));
            list = Detect(mInputFiles->filenames()->at(i));
        }
        //Ordenarlas si es necesario
        if (!list) return false;
        mDetectedStars.append(list);
        if (mCatPar->cat_sort)
        {
            OnMsg("Sorting sources");
            Sort(list, mCatPar->cat_sort_field, mCatPar->cat_sort_order);
            if (!mCatPar->cat_sort_order)
            {
                for (int i=0, j=list->length()-1; i<j && i<list->length(); i++, j--)
                {
                    DetectedStar aux = list->at(j);
                    list->replace(j, list->at(i));
                    list->replace(i, aux);
                }
            }
        }
        //Guardar en archivo
        if (mCatPar->cat_output && needSave())
        {
            OnMsg("Saving catalog");
            catalog_filename = QDir(mInputFiles->outputDir()).absoluteFilePath(basename + "."
                                    + mCatPar->cat_fmt.toLower());
            QString ok = SaveCatalog(catalog_filename, list);
            if (ok.length())
            {
                ERR("Unable to save catalog file %s with error: %s", PTQ(catalog_filename), PTQ(ok));
                return false;
            }
        }
    }
    if (cancelled()) return false;
    if (mCatPar->flux_output && needSave())
    {
        QString flux_filename =
            QDir(mInputFiles->outputDir()).absoluteFilePath("flux_table."
                                                            + mCatPar->flux_fmt.toLower());
        INFO("\tSaving flux table: %s", PTQ(flux_filename));
        SaveFluxTable(flux_filename, &mDetectedStars);
    }
    INFO("Create catalog process finished");
    OnProgress(steps() * 100);
    return true;
}

QVector<DetectedStar>* CatalogComputer::Detect(QString filename)
{
    if (mCurrentImage && mCurrentImageControl) delete mCurrentImage;
    mCurrentImage = FitImage::OpenFile(filename);
    mCurrentImageControl = true;
    if (!mCurrentImage)
    {
        ERR("Error opening file: %s", PTQ(filename));
        return NULL;
    }
    QVector<DetectedStar>* ds = Detect(mCurrentImage);
    //No hay que borrar la imagen
    return ds;
}

QVector<DetectedStar>* CatalogComputer::Detect(FitImage* fitimage)
{
    mImageDates.append(fitimage->GetDate());
    //Antes de nada preparar parámetros de detección
    //if (mCatPar->cat_output_fields.contains(CATFIELD_FLUX_RADI))
    //Preparar la imagen para la detección y el detector
    if (mCurrentImage && mCurrentImageControl && mCurrentImage != fitimage) delete mCurrentImage;
    mCurrentImage = fitimage;
    if (!mCurrentImage->plate()) mCurrentImage->LookForPlate();
    QVector<DetectedStar>* detstars = new QVector<DetectedStar>();

    if (mDetector) delete mDetector;
    mDetector = new StarDetectorParallel(fitimage->data(), fitimage->width(), fitimage->height(), *mDetectPar);
    connect(mDetector, SIGNAL(Setup_Signal(BackgroundWorker*,int)), this, SLOT(DetectorSetup_Slot(BackgroundWorker*,int)));
    connect(mDetector, SIGNAL(Progress_Signal(BackgroundWorker*,int)), this, SLOT(DetectorProgress_Slot(BackgroundWorker*,int)));
    connect(this, SIGNAL(Cancel_Signal(BackgroundWorker*)), mDetector, SLOT(cancel()));
    //Detectar las fuentes
    mDetector->DetectStars(0, 0, fitimage->width()-1, fitimage->height()-1);

    detstars->reserve(mDetector->count());
    for (int i=0;i<mDetector->count();i++)
    {
        detstars->append(mDetector->stars()[i]);
        //Antes de agregarlas a la lista calculamos sus coordenadas ecuatoriales si las tuvieran
        if (fitimage->plate())
        {
            double ra, dec;
            fitimage->plate()->map(((detstars->data()+i))->x(), ((detstars->data()+i))->y(), &ra, &dec);
            ((detstars->data()+i))->setRaDec(ra, dec);
        }
    }
    INFO("\tDetectd %d sources", mDetector->count());
    return detstars;
}

QString CatalogComputer::SaveCatalog(QString filename, QVector<DetectedStar>* list)
{
    QFile fil(filename);
    if (!fil.open(QFile::WriteOnly)) return "Unable to open file for writting";
    QTextStream ts(&fil);
    if (mCatPar->cat_fmt == "TXT")
        SaveCatalogTXT(&ts, list);
    else if (mCatPar->cat_fmt == "CSV")
        SaveCatalogCSV(&ts, list);
    else if (mCatPar->cat_fmt == "VOT")
        SaveCatalogVOT(&ts, list);
    fil.close();
    return "";
}

void CatalogComputer::DetectorSetup_Slot(BackgroundWorker* /* detector */, int size)
{
    mDetectorSize = size;
}

void CatalogComputer::DetectorProgress_Slot(BackgroundWorker* detector , int position)
{
    static int s_last_position = 0;
    static int s_last_num_stars = 0;
    double porcen = position / (double)mDetectorSize;
    double pos = (mCurFile * 100) + (100 * porcen);
    int ipos = (int)pos;
    if (ipos == s_last_position) return;
    OnProgress(s_last_position = ipos);
    if (s_last_num_stars != ((StarDetector*)detector)->count())
    {
        s_last_num_stars = ((StarDetector*)detector)->count();
        OnMsg(QString("Detected: %1 sources").arg(s_last_num_stars));
    }
}

void CatalogComputer::SaveCatalogTXT(QTextStream* stream,  QVector<DetectedStar>* list)
{
    mCurOutputFormat = "TXT";
    char buffer[128];
    const char* separator = "    ";
    //Texto introductorio
    *stream << "#Generated by " << XPROGRAM << " v" << XVERSION << " : [" << list->count() << "] sources detected" <<  "\n";
    //Cabecera de la imagen
    for (int i=0; i<mCatPar->cat_fields.count() && list->count(); i++)
    {
        //Calculamos el tamaño del campo
        int dtlen = QString(GetStarField(mOutputCatFields[i], i, list->data()+i, buffer, separator)).length() - (int)strlen(separator);
        QString field = QString(i?" ":" ") + mCatPar->cat_fields[i];
        while (field.length() < (i?dtlen:dtlen-1)) field = " " + field;
        if (!i) field = "#" + field;
        *stream << field << separator;
    }
    *stream << "\n";
    //Todas las fuentes de la lista
    for (int i=0; i<list->count(); i++)
    {
        DetectedStar ds = list->at(i);
        //Si se está aplicando un filtro comprobamos que la fuente esté dentro del mismo
        if (mCatPar->filt_output && !GetFiltered(ds.ra(),ds.dec())) continue;
        //Sacamos los campos correspondientes
        for (int f=0; mOutputCatFields[f]; f++)
            *stream << GetStarField(mOutputCatFields[f], i, &ds, buffer, separator);
        *stream << "\n";
    }
}

//Devuelve el valor de un campo formateado para una estrella
char*  CatalogComputer::GetStarField(char* field_name, int star_idx, DetectedStar* star, char* out, const char*sep)
{
    *out = 0;
    if (!strcmp(field_name,"POS"))
        sprintf(out, "%5d", star_idx);
    else if (!strcmp(field_name, CATFIELD_IMG_X))
        sprintf(out, "%8.3f", star->x());
    else if (!strcmp(field_name, CATFIELD_IMG_Y))
        sprintf(out, "%8.3f", star->y());
    else if (!strcmp(field_name, CATFIELD_RA_DEG) && mCurrentImage->plate())
        sprintf(out, "%18.13lf", star->ra() * HOR2DEG);
    else if (!strcmp(field_name, CATFIELD_RA_HOUR) && mCurrentImage->plate())
        sprintf(out, "%18.13lf", star->ra());
    else if (!strcmp(field_name, CATFIELD_DEC_DEG) && mCurrentImage->plate())
        sprintf(out, "%18.13lf", star->dec());
    else if (!strcmp(field_name, CATFIELD_RA_HMS) && mCurrentImage->plate())
    {
        QString ra_str = Util::RA2String(star->ra(), mCurOutputFormat == "TXT" ?   "%02d:%02d:%05.2f" :
                                                     mCurOutputFormat == "CSV" ? "\"%02d %02d %05.2f\"" :
                                                                                   "%02d %02d %05.2f" );
        strcpy(out, ra_str.toLatin1().data());
    }
    else if (!strcmp(field_name, CATFIELD_DEC_DMS) && mCurrentImage->plate())
    {
        QString dec_str = Util::DEC2String(star->dec(),
                                           mCurOutputFormat == "TXT" ?   "%c%02d:%02d:%04.1f" :
                                           mCurOutputFormat == "CSV" ? "\"%c%02d %02d %04.1f\"" :
                                                                         "%c%02d %02d %04.1f" );
        strcpy(out, dec_str.toLatin1().data());
    }
    else if (!strcmp(field_name, CATFIELD_FLUX_ISO))
        sprintf(out, "%11.2f", star->fluxISO());
    else if (!strcmp(field_name, CATFIELD_FLUX_WIN))
        sprintf(out, "%11.2f", star->fluxISO());
    else if (!strcmp(field_name, CATFIELD_FLUX_PHOTO))
        sprintf(out, "%11.2f", star->fluxPhoto());
    else if (!strcmp(field_name, CATFIELD_RADIUS_PIX))
        sprintf(out, "%11.2f", star->radius());
    else if (!strcmp(field_name, CATFIELD_RADIUS_ARCSEC) &&  mCurrentImage->plate())
        sprintf(out, "%14.2f", star->radius() *  mCurrentImage->plate()->scale());
    else if (!strcmp(field_name, CATFIELD_FWHM_PIX))
        sprintf(out, "%10.2f", star->fwhm());
    else if (!strcmp(field_name, CATFIELD_FWHM_ARCSEC) &&  mCurrentImage->plate())
        sprintf(out, "%12.2f", star->fwhm() *  mCurrentImage->plate()->scale());
    else if (!strcmp(field_name, CATFIELD_AREA))
        sprintf(out, "%5d", star->pixinstar());
    //Utilizados en tablas de flujo
    else if (!strcmp(field_name, CATFIELD_DATA_COUNT))
        sprintf(out, "%10d", mDatacount);
    else if (!strcmp(field_name, CATFIELD_NAME))
    {
        CatFilteredSource* fs = GetFiltered(star->ra(), star->dec());
        if (!fs) sprintf(out, "%12s", fs?fs->name.toLatin1().data():"");
        else
        {
            QString nameout = mCurOutputFormat == "TXT" ? fs->name.replace(" ", "_").replace("\t","_") :
                              mCurOutputFormat == "CSV" ? "\""+fs->name+"\"" : fs->name;
            sprintf(out, "%12s", nameout.toLatin1().data());
        }
        if (!out[0]) strcpy(out, "            ");
    }
    else if (!strcmp(field_name, CATFIELD_BKG))
        sprintf(out, "%6.2f", star->back());
    else if (!strcmp(field_name, CATFIELD_BKG_STDEV))
        sprintf(out, "%9.2f", star->backStdev());
    else
    {
        //Jamás debería darse esto puesto que hay un ASSERT que lo controla.
        ERR("Unknown field in CatalogComputer::GetStarField: %s", PTQ(QString(field_name)));
    }
    ASSERT(*out, "Unknown field in CatalogComputer::GetStarField");
    if (strlen(out)) strcat(out, sep);
    return out;
}

void  CatalogComputer::SaveCatalogCSV(QTextStream* stream,  QVector<DetectedStar>* list)
{
    mCurOutputFormat = "CSV";
    char buffer[128];
    const char* separator = ";";
    //Comenzar por las cabeceras
    for (int i=0; i<mCatPar->cat_fields.count() && list->count(); i++)
        *stream << mCatPar->cat_fields.at(i) << separator;
    *stream << "\n";
    //Seguir con todas las fuentes, pero tener en cuenta que tienen que estar ordenadas
    for (int i=0; i<list->count(); i++)
    {
        DetectedStar ds = list->at(i);
        //Si se está aplicando un filtro comprobamos que la fuente esté dentro del mismo
        if (mCatPar->filt_output && !GetFiltered(ds.ra(),ds.dec())) continue;
        //Sacamos los campos correspondientes
        for (int f=0; mOutputCatFields[f]; f++)
            *stream << Util::Trim(GetStarField(mOutputCatFields[f], i, &ds, buffer, separator));
        *stream << "\n";
    }
}

void  CatalogComputer::SaveCatalogVOT(QTextStream* /*stream*/,  QVector<DetectedStar>* /*list*/)
{
    mCurOutputFormat = "VOT";
    //Formato no implementado por el momento
}

//Búsqueda semi-dicotómica en fuentes ordenadas por declinación
DetectedStar*  CatalogComputer::Contains(QVector<DetectedStar>* list, DetectedStar* star, int *insert_point, double max_sep)
{
    DetectedStar* data = list->data();
    int cont = list->count();
    //Buscamos la fuente de declinación más cercana(que no tiene porqué ser la buena)
    int mid = 0, left = 0, right = cont-1;
    while (right-left > 2)
    {
        mid = (right+left)/2;
        if (star->dec() < data[mid].dec()) right = mid;
        else left = mid;
    }
    if (insert_point) *insert_point = mid;
    DetectedStar* cur_star = NULL;
    double cur_sep = FLOAT_MAX;
    double ip_sep = FLOAT_MAX;
    //Nos movemos hacia la izquierda hasta que haya demasiada separación de declinaciones
    for (int i=mid; (i>=0) && ((data[i].dec() - star->dec()) < max_sep); i--)
    {
        double sep = Util::Adist(star->ra(), star->dec(), data[i].ra(), data[i].dec()) * 3600.0;
        if (sep < cur_sep && sep < max_sep)
        {
            cur_sep = sep;
            cur_star = data+i;
        }
        if (insert_point && cur_sep < ip_sep)
        {
            ip_sep = cur_sep;
            *insert_point = i;
        }
    }
    //Nos movemos hacia la derecha hasta que haya demasiada separación de declinaciones
    for (int i=mid+1; (i<cont) && (fabs(data[i].dec() - star->dec()) < max_sep); i++)
    {
        double sep = Util::Adist(star->ra(), star->dec(), data[i].ra(), data[i].dec()) * 3600.0;
        if (sep < cur_sep && sep < max_sep)
        {
            cur_sep = sep;
            cur_star = data+i;
        }
        if (insert_point && cur_sep < ip_sep)
        {
            ip_sep = cur_sep;
            *insert_point = i;
        }
    }
    //Calculamos el punto exacto de inserción moviéndonos hacia abajo o arriba si es necesario
    if (insert_point)
    {
        while (*insert_point>=0 && data[*insert_point].dec() > star->dec()) (*insert_point)--;
        while (*insert_point<cont && data[*insert_point].dec() < star->dec()) (*insert_point)++;
    }
    return cur_star;
}

QString  CatalogComputer::SaveFluxTable(QString filename, QList<QVector<DetectedStar>*>* lists)
{
    OnTitleChange("Creating <B>flux table<b>");
    QFile fil(filename);
    if (!fil.open(QFile::WriteOnly)) return "Unable to open file for writting";
    QTextStream ts(&fil);
    //Es necesario obtener una lista con las estrellas únicas por coordenada ecuatorial
    //------------------------------------------------------------------------------------------
    OnMsg("Creating list of sources");
    DINFO("Creating main linst");
    //Creamos una lista con todas las fuentes de la primera de las listas
    QVector<DetectedStar> mainlist;
    mainlist.reserve((int)(lists->at(0)->count()*1.5));
    for (int j=0; j<lists->at(0)->count() && !cancelled(); j++)
    {
        if (mCatPar->filt_output && !GetFiltered(lists->at(0)->data()[j].ra(), lists->at(0)->data()[j].dec())) continue;
        mainlist.append(lists->at(0)->at(j));
    }
    Sort(&mainlist, CATSORT_DEC, true);
    #ifdef QT_DEBUG
    for (int i=0;i<mainlist.count()-1;i++) ASSERT(mainlist.data()[i].dec() < mainlist.data()[i+1].dec(), "Error sorting list");
    #endif
    DINFO("Adding coincidences");
    for (int i=1; i<lists->count() && !cancelled(); i++)
    {
        OnProgress(size()/2 + (size()/2 * (i / (double)lists->count()))/2);
        QVector<DetectedStar>* vec = lists->at(i);
        for (int j=0; j<vec->count(); j++)
        {
            int insert_point;
            DetectedStar* compstar = vec->data()+j;
            if (!Contains(&mainlist, compstar, &insert_point))
                mainlist.insert(insert_point, *compstar);
        }
    }
    DINFO("Preparing data");
    //Ahora se ordena por declinación todas las listas para que la posterior búsqueda sea mas rápida
    //------------------------------------------------------------------------------------------
    OnMsg("Preparing data");
    for (int i=0; i<lists->count() && !cancelled(); i++)
        Sort(lists->at(i), CATSORT_DEC, true);
    if (mCatPar->flux_sort)
        Sort(&mainlist, mCatPar->flux_sort_field, mCatPar->flux_sort_order);

    INFO("\tDetected %d unique sources to save in flux table", mainlist.count());
    OnMsg("Saving file");
    if (mCatPar->flux_fmt == "TXT")
        SaveFluxTableTXT(&ts, &mainlist, lists);
    else if (mCatPar->flux_fmt == "CSV")
        SaveFluxTableCSV(&ts, &mainlist, lists);
    else if (mCatPar->flux_fmt == "VOT")
        SaveFluxTableVOT(&ts, &mainlist, lists);
    fil.close();
    return "";
}

void CatalogComputer::SaveFluxTableTXT(QTextStream* stream, QVector<DetectedStar>* mainlist, QList<QVector<DetectedStar>*>* lists)
{
    mCurOutputFormat = "TXT";
    DetectedStar* dstar;
    char buffer[128];
    const char* separator = ";";
    int flux_type = mCatPar->flux_type == CATFIELD_FLUX_ISO ? 0 :
                    mCatPar->flux_type == CATFIELD_FLUX_WIN ? 1 : 2;
    //Encabecados a incluir en la primera de las líneas
    for (int i=0; i<mCatPar->flux_fields.count() && !cancelled(); i++)
         *stream << mCatPar->flux_fields[i] << ";";
    for (int i=0; i<lists->count() && !cancelled(); i++)
    {
        switch (mCatPar->flux_heading)
        {
            //Nombre del archivo
            case 0: *stream << QFileInfo(mInputFiles->fitimages()->count()
                                         ? mInputFiles->fitimages()->at(i)->filename() : mInputFiles->filenames()->at(i)).baseName();
                    break;
            //Datetime
            case 1: *stream << mImageDates[i].toString("yyyy-mm-dd hh:mm:ss"); break;
            //Relative seconds
            case 2: *stream << (mImageDates[i].toTime_t() - mImageDates[0].toTime_t()); break;
            //Position
            case 3: *stream << i; break;
            default:
            break;
        }
        *stream << ";";
    }
    *stream << "\n";

    XStringBuilder sb;
    //Formato por columnas
    for (int i=0; i<mainlist->count() && !cancelled(); i++)
    {
        sb.clear();
        DetectedStar *mstar = mainlist->data()+i;
        if (mCatPar->filt_output && !GetFiltered(mstar->ra(), mstar->dec())) continue;
        OnProgress(size()/2 + (size()/2 + size()/2 * (i / (double)mainlist->count()))/2);
        //Flujo de cada fuente
        mDatacount = 0;
        for (int j=0; j<lists->count(); j++)
        {
            if ((dstar = Contains(lists->at(j), mstar)))
            {
                switch (flux_type)
                {
                    case 0: sprintf(buffer, "%0.2f", dstar->fluxISO()); break;
                    case 1: sprintf(buffer, "%0.2f", dstar->fluxWin()); break;
                    case 2: sprintf(buffer, "%0.2f", dstar->fluxPhoto()); break;
                    default: *buffer = 0;
                }
                mDatacount++;
            }
            else *buffer = 0;
            sb << buffer << ";";
        }
        //Encabezados de la línea (primeras columnas)
        for (int f=0; f<mCatPar->flux_fields.count(); f++)
             *stream << Util::Trim(GetStarField(mOutputFluxFields[f], i, mstar, buffer, separator));
        //Y finalmente los datos de flujo de cada estrella
        *stream << sb.buffer() << "\n";
    }
}


void CatalogComputer::SaveFluxTableCSV(QTextStream* stream, QVector<DetectedStar>* mainlist, QList<QVector<DetectedStar>*>* lists)
{
    mCurOutputFormat = "CSV";
    DetectedStar* dstar;
    char buffer[128];
    const char* separator = ";";
    int flux_type = mCatPar->flux_type == CATFIELD_FLUX_ISO ? 0 :
                    mCatPar->flux_type == CATFIELD_FLUX_WIN ? 1 : 2;
    //Encabecados a incluir en la primera de las líneas
    for (int i=0; i<mCatPar->flux_fields.count() && !cancelled(); i++)
         *stream << mCatPar->flux_fields[i] << ";";
    for (int i=0; i<lists->count() && !cancelled(); i++)
    {
        switch (mCatPar->flux_heading)
        {
            //Nombre del archivo
            case 0: *stream << QFileInfo(mInputFiles->fitimages()->count()
                                         ? mInputFiles->fitimages()->at(i)->filename() : mInputFiles->filenames()->at(i)).baseName();
                    break;
            //Datetime
            case 1: *stream << mImageDates[i].toString("yyyy-mm-dd hh:mm:ss"); break;
            //Relative seconds
            case 2: *stream << (mImageDates[i].toTime_t() - mImageDates[0].toTime_t()); break;
            //Position
            case 3: *stream << i; break;
            default:
            break;
        }
        *stream << ";";
    }
    *stream << "\n";

    XStringBuilder sb;
    //Formato por columnas
    for (int i=0; i<mainlist->count() && !cancelled(); i++)
    {
        sb.clear();
        DetectedStar *mstar = mainlist->data()+i;
        if (mCatPar->filt_output && !GetFiltered(mstar->ra(), mstar->dec())) continue;
        OnProgress(size()/2 + (size()/2 + size()/2 * (i / (double)mainlist->count()))/2);
        //Flujo de cada fuente
        mDatacount = 0;
        for (int j=0; j<lists->count(); j++)
        {
            if ((dstar = Contains(lists->at(j), mstar)))
            {
                switch (flux_type)
                {
                    case 0: sprintf(buffer, "%0.2f", dstar->fluxISO()); break;
                    case 1: sprintf(buffer, "%0.2f", dstar->fluxWin()); break;
                    case 2: sprintf(buffer, "%0.2f", dstar->fluxPhoto()); break;
                    default: *buffer = 0;
                }
                mDatacount++;
            }
            else *buffer = 0;
            sb << buffer << ";";
        }
        //Encabezados de la línea (primeras columnas)
        for (int f=0; f<mCatPar->flux_fields.count(); f++)
             *stream << Util::Trim(GetStarField(mOutputFluxFields[f], i, mstar, buffer, separator));
        //Y finalmente los datos de flujo de cada estrella
        *stream << sb.buffer() << "\n";
    }
}

void CatalogComputer::SaveFluxTableVOT(QTextStream* /*stream*/, QVector<DetectedStar>* /*mainlist*/, QList<QVector<DetectedStar>*>* /*lists*/)
{
    mCurOutputFormat = "VOT";
    //Formato no implementado por el momento
}










