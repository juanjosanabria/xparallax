#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "core_global.h"
#include "core_def.h"
#include <QPointF>

class CORESHARED_EXPORT Geometry
{

public:
    //-------------------------------------------------------------------------------------------------------------------
    //Ángulos entre vectores
    //-------------------------------------------------------------------------------------------------------------------
    /**
     * @brief VectorAngle Menor ángulo entre dos vectores
     * @return
     */
    static double  VectorAngle(double v1x, double v1y, double v2x, double v2y);

    /**
     * @brief VectorAngle Menor ángulo entre dos vectores definidos por 4 puntos.
     * @return
     */
    static double  VectorAngle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);

    /**
     * @brief VectorAngle Menor ángulo entre dos vectores
     * @return Menor ángulo entre los dos vectore en grados.
     */
    static double  VectorAngle(QPointF u, QPointF v);

    /**
     * @brief VectorAngle Menor ángulo entre dos vectores.
     * @return Menor ángulo entre dos vectores en grados.
     */
    static double  VectorAngle(QPointF p1, QPointF p2, QPointF p3, QPointF p4);

    /**
     * @brief VectorAngle Cálculo del ángulo en sentido antihorario entre dos vectores. de 0 a 360 en dirección de a -> b,
     * ángulo devuelto en grados. Los vectores parten de (x1,y1)->(x2,y2) // (x3,y3)->(x4,y4)
     * @param v1x
     * @param v1y
     * @param v2x
     * @param v2y
     * @return
     */
    static double  VectorAngle_AH(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);

    /**
     * @brief VectorAngle Cálculo del ángulo en sentido antihorario entre dos vectores. de 0 a 360 en dirección de a -> b,
     * ángulo devuelto en grados. Los vectores parten de (x1,y1)->(x2,y2) // (x3,y3)->(x4,y4)
     * @param p1
     * @param p2
     * @param p3
     * @param p4
     * @return
     */
    static double  VectorAngle_AH(QPointF p1, QPointF p2, QPointF p3, QPointF p4);

    /**
     * @brief VectorAngle Cálculo del ángulo en sentido antihorario entre dos vectores. de 0 a 360 en dirección de a -> b,
     * ángulo devuelto en grados. Los vectores van del punto 0,0 a las coordenadas indicadas.
     * @param v1x
     * @param v1y
     * @param v2x
     * @param v2y
     * @return
     */
    static double  VectorAngle_AH(double v1x, double v1y,double v2x, double v2y);
    /**
     * @brief VectorAngle_AH  Cálculo del ángulo en sentido antihorario entre dos vectores. de 0 a 360 en dirección de a -> b,
     * ángulo devuelto en grados. Los vectores van del punto 0,0 a las coordenadas indicadas.
     * @param v1x
     * @param v1y
     * @param v2x
     * @param v2y
     * @return
     */
    static double  VectorAngle_AH(QPointF v1, QPointF v2);

    /**
     * @brief Calcula el ángulo de un segmento con la horizontal.
     * @return Menor ángulo en grados con el vector unidad del eje x
     */
    static double  SegmentAngle(double x1, double y1, double x2, double y2);

    /**
     * @brief Calcula el ángulo de un segmento con la horizontal.
     * @return Menor ángulo en grados con el vector unidad del eje x
     */
    static double  SegmentAngle(QPointF p1, QPointF p2);

    /**
     * @brief Calcula el ángulo de un segmento con la horizontal.
     * @return Menor ángulo en grados con el vector unidad del eje x
     */
    static double  SegmentAngle_AH(double x1, double y1, double x2, double y2);

    /**
     * @brief Calcula el ángulo de un segmento con la horizontal.
     * @return Menor ángulo en grados con el vector unidad del eje x
     */
    static double  SegmentAngle_AH(QPointF p1, QPointF p2);

    //-------------------------------------------------------------------------------------------------------------------
    //Rotaciones, traslaciones y diversos cálculos con ángulos y segmtneos
    //-------------------------------------------------------------------------------------------------------------------
    /**
     * @brief RotateSegment Rota un segmento un número de grados en sentido antihorario con un punto de pivotaje.
     * @param p1 Punto inicial del segmento
     * @param p2 Punto final del segmento
     * @param angle Ángulo de rotación en grados sentido antihorario (positivo) u horario (negativo)
     * @param pivot Punto de pivotaje
     * @return Nuevo segmento
     */
    static void  RotateSegment(QPointF& p1, QPointF& p2, double angle, QPointF pivot);

    /**
     * @brief RotateSegment Rota un segmento un número de grados en sentido antihorario usando como punto de pivitaje el comienzo
     * sel segmento
     * @param p1 Punto inicial del segmento y punto de pivotaje
     * @param p2 Punto final del segmento
     * @param angle Ángulo de rotación en grados sentido antihorario (positivo) u horario (negativo)
     */
    static void  RotateSegment(QPointF& p1, QPointF& p2, double angle);

    /**
     * @brief PointInPolygon Indica si un punto está dentro de un polígono cerrado
     * @param point Punto a testear
     * @param polygon Puntos del polígono
     * @param nvert Número de vértices del polígono. Número de elementos del vector "polygon"
     * @return
     */
    static bool PointInPolygon(QPointF point, QPointF* polygon, int nvert);

    //-------------------------------------------------------------------------------------------------------------------
    //Rectas del plano
    //-------------------------------------------------------------------------------------------------------------------

    /**
     * @brief rectPuntoPendiente Devuelve la ordenada en el origen y la pendiente de la recta marcada por los puntos pa, pb
     * @param pa Punto por el que pasa la recta
     * @param pb Punto por el que pasa la recta
     * @param m Salida. Pendiente
     * @param b Salida. Ordenada en el origen
     */
    static void rectPuntoPendiente(const QPointF& pa,const QPointF& pb,double *m, double *b);

    /**
     * @brief rectABC Devuelve la ecuación canónica de la recta que pasa por dos puntos ax + bx + c = 0
     * @param pa Primer punto por el que pasa la recta
     * @param pb Segundo punto por el que pasa la recta
     * @param a Salida.
     * @param b Salida.
     * @param c Salida.
     */
    static void rectABC(const QPointF& pa,const QPointF& pb,double *a, double *b, double *c);

    /**
     * @brief distPointRect Distancia de un punto a una recta según su ecuación canónoca ax + bx + c = 0
     * @param pa Punto desde el que se desea medir la distancia
     * @param a (ax + bx + c = 0)
     * @param b (ax + bx + c = 0)
     * @param c (ax + bx + c = 0)
     * @return
     */
    static double distPointRect(const QPointF& p, double a, double b, double c);

    /**
     * @brief distPointRect Distancia de un punto a la recta definida por AB
     * @param p Punto desde el que se desea medir la distancia
     * @param A punto que define la recta
     * @param B punto que define la recta
     * @return
     */
    static double distPointRect(const QPointF& p, const QPointF& A, const QPointF& B);

    /**
     * @brief nearPointRect Devuelve el punto de la recta canónica ax + bx + c = 0 que es más cercano al punto P
     * @param p Punto fuera de la recta, del que se desea buscar el más cerdano a la misma
     * @param a
     * @param b
     * @param c
     * @return
     */
    static QPointF nearPointRect(const QPointF& p, double a, double b, double c);

    /**
     * @brief pointInsideRectangle Indica si un punto está dentro de un rectángulo
     * @param p
     * @param rect
     * @return
     */
    static bool pointInsideRectangle(const QPointF& p, QPointF* rect);
};

#endif // GEOMETRY_H




