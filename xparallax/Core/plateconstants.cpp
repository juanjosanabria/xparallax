#include "plateconstants.h"
#include <qmath.h>
#include <QFileInfo>
#include "xglobal.h"


PlateConstants::PlateConstants(int img_width, int img_height)
{
    mDirty = true;
    mImageWidth = img_width;
    mImageHeight = img_height;
    mEquinox = 2000.0f;
}


PlateConstants::~PlateConstants()
{
}

void PlateConstants::Initialize(double ra, double dec, double a, double b, double c, double d, double e, double f)
{
    A=a; B=b; C=c; D=d; E=e; F=f;
    RA0=ra * HOR2DEG; DEC0=dec;

    ComputeScaleFLRot();

    ComputeInverse();
    mDirty = false;
}


/**
 * @brief PlateConstants::ComputeScaleAndFL Calcula la longitud focal, la escala y la rotación de la imagen
 */
void PlateConstants::ComputeScaleFLRot()
{
    //Calcular la distancia focal
    double DET = A * E - D * B;
    FL = 1.0 / sqrt(fabs(DET));
    SCALE = RAD2ASEC / FL;

    //Calcular la rotación de la imagen
    double CD1_1 = -A * RAD2DEG;
    double CD1_2 = -B * RAD2DEG;
    double CD2_1 = D *  RAD2DEG;
    double CD2_2 = E * RAD2DEG;

    double incA = sqrt(CD1_1*CD1_1+CD1_2*CD1_2)*(CD1_1/fabs(CD1_1)) ;
    double incD = sqrt(CD2_1*CD2_1+CD2_2*CD2_2)*(CD2_2/fabs(CD2_2)) ;

    ROT = atan2(CD1_2/incA, CD2_2/incD)*RAD2DEG;

    int s_sin = SIGN(D);
    int s_cos = SIGN(A);

    //Ahora normalizamos el ángulo al cuadrante que le corresponda
    //Cuadrante 1 no se hace nada, el ángulo es el mismo
    if (s_cos > 0 && s_sin > 0) ROT = -ROT;
    //Cuadrante 2, seno positivo, coseno negativo
    else if (s_cos < 0 && s_sin > 0) ROT = 180 - ROT;
    //Cuadrante 3, seno y cosenos negatios
    else if (s_cos < 0 && s_sin < 0) ROT = 180 - ROT;
    //Cuadrante 4, coseno positivo, seno negativo
    else if (s_cos > 0 && s_sin < 0) ROT = 360 - ROT;
    //Finalmente normalizamos el centro
    if (ROT < 0) ROT += 360;
}

/**
 * @brief PlateConstants::map Transformación directa
 * @param xt Coordenada de imagen en píxeles. El origen de coordenadas es el píxel superior izquierdo, comenzando en 0
 * @param yt Coordenada de imagen en píxeles. El origen de coordenadas es el píxel superior izquierdo, comenzando en 0
 * @param ra Ascensión recta en horas
 * @param dec Declinación en grados
 */
void PlateConstants::map(double xt, double yt,double* ra, double* dec)
{
   if (mDirty) Compute();
   double RA_OBS = 0, DEC_OBS = 0;
   double xx = A * xt + B * yt + C;
   double yy = D * xt + E * yt + F;
   StdEqu(RA0, DEC0, xx, yy, &RA_OBS, &DEC_OBS);
   *ra = RA_OBS * DEG2HOR;
   *dec = DEC_OBS ;
}

/**
 * @brief PlateConstants::imap Transformación inversa
 * @param ra Ascensión recta en horas
 * @param dec Declinación en grados
 * @param xt Coordenada de imagen en píxeles. El origen de coordenadas es el píxel superior izquierdo, comenzando en 0
 * @param yt Coordenada de imagen en píxeles. El origen de coordenadas es el píxel superior izquierdo, comenzando en 0
 */
void PlateConstants::imap(double ra, double dec,double* xt,double* yt)
{
   if (mDirty) Compute();
   double xx, yy;
   EquStd(RA0, DEC0, ra * HOR2DEG, dec, &xx, &yy);
   *xt = G * xx + H * yy + I;
   *yt = J * xx + K * yy + L;
}


/**
 * @brief PlateConstants::AddCtrlPoint
 * @param x Coordenada de imagen en píxeles. El origen de coordenadas es el píxel superior izquierdo, comenzando en 0
 * @param y Coordenada de imagen en píxeles. El origen de coordenadas es el píxel superior izquierdo, comenzando en 0
 * @param ra Ascensión recta en horas
 * @param dec Declinación en grados
 */
void PlateConstants::AddCtrlPoint(float x, float y, double ra, double dec)
{
    PlateCtrlPoint pc;
    pc.x = x; pc.y = y; pc.ra = ra * HOR2DEG; pc.dec = dec;
    pc.delta = NAN;
    mCtrlPoints.append(pc);
    mDirty = true;
}

void PlateConstants::rejectPoint(int i)
{
    PlateCtrlPoint p = mCtrlPoints[i];
    mCtrlPoints.removeAt(i);
    mRejectedPoints.append(p);
}

void PlateConstants::rejectPoint(PlateCtrlPoint* p)
{
    int idx = -1;
    for (int i=0; i<mCtrlPoints.count() && idx==-1; i++)
    {
        if (CART_DIST(mCtrlPoints.data()[i].x, mCtrlPoints.data()[i].y, p->x, p->y) < 1)
            idx = i;
    }
    if (idx < 0) return;
    rejectPoint(idx);
}

void PlateConstants::rejectPoint(double x, double y)
{
    PlateCtrlPoint p;
    p.x = (float)x, p.y = (float)y;
    rejectPoint(&p);
}

/**
 * @brief PlateConstants::ClearCtrlPoints Elimina todos los puntos de control
 */
void PlateConstants::ClearCtrlPoints()
{
    mCtrlPoints.clear();
    mRejectedPoints.clear();
    mDirty = true;
}

void PlateConstants::RemoveCtrlPoint(int i)
{
    mCtrlPoints.remove(i);
}

/**
 * @brief PlateConstants::Lsqfit Resuelve el sistema lineal de ecuaciones:  A[i,1]*s[1]+.... A[i,m] - A[i,m+1] = 0;     i=1,....n
 * @param _A Vector de entrada
 * @param S Vector donde se almacenarán las soluciones
 */

void PlateConstants::Lsqfit(FMatrix* _A,int N, int M, double* S)
{
    FMatrix A;
    A.copy(_A);

    double p = 0, q = 0, h = 0;
    for (int j = 0; j < M; j++)
    {
        for (int i = j + 1; i < N; i++)
        {
            if (A.get(i,j) != 0.0)
            {
                //Calcular p,q y el nuevo A.get(j,j)
                if (fabs(A.get(j, j)) < EPS * fabs(A.get(i, j)))
                {
                    p = 0.0; q = 1.0; A.set(j, j, -A.get(i, j)); A.set(i, j, 0.0);
                }
                else
                {
                    h = sqrt(A.get(j, j) * A.get(j, j) + A.get(i, j) * A.get(i, j));
                    if (A.get(j, j) < 0.0) h = -h;
                    p = A.get(j, j) / h; q = -A.get(i, j) / h; A.set(j, j,  h); A.set(i, j, 0.0);
                }
            }
            //Calcular el resto de la línea
            for (int k = j + 1; k <= M; k++)
            {
                h = p * A.get(j, k) - q * A.get(i, k);
                A.set(i, k, q * A.get(j, k) + p * A.get(i, k));
                A.set(j, k, h);
            }
        }
    }

    //Sustitución y cálculo de las incógnitas
    for (int i = M - 1; i >= 0; i--)
    {
        h = A.get(i, M);
        for (int k = i + 1; k < M; k++)
            h = h - A.get(i, k) * S[k];
        S[i] =  h / A.get(i, i);
    }
}


/**
 * @brief PlateConstants::SN Seno en grados
 * @param v ángulo expresado en grados
 * @return
 */
double PlateConstants::SN(double v)
{
    return sin(v * DEG2RAD);
}


/**
 * @brief PlateConstants::CS Coseno en grados
 * @param v ángulo expresado en grados
 * @return
 */
double PlateConstants::CS(double v)
{
    return cos(v * DEG2RAD);
}

/**
 * @brief PlateConstants::ASN Arcoseno en grados
 * @param X ángulo expresado en grados
 * @return arcoseno del ángulo indicado
 */
double PlateConstants::ASN(double X)
{
    if (fabs(X) == 1.0) return 90.0 * X;
    else if (fabs(X) > EPS) return atan(X / sqrt((1.0 - X) * (1.0 + X))) / DEG2RAD;
    else  return X / DEG2RAD;
}

/**
 * @brief PlateConstants::ATN Arcotangente en grados
 * @param X ángulo expresado en grados
 * @return arcotangente del ángulo indicado
 */
double PlateConstants::ATN(double X)
{
    return atan(X) / DEG2RAD;
}


/**
 * @brief PlateConstants::StdEqu Transforma coordenadas estándard a ecuatoriales suponiendo un centro óptico
 * @param RA0   Ascensión recta del centro óptico
 * @param DEC0  DEclinación del centro óptico
 * @param XX    Coordenada X estándard
 * @param YY    Coordenada Y estándard
 * @param RA    Resultado de ascensión recta
 * @param DEC   Resultado de declinación
 */
void PlateConstants::StdEqu(double RA0, double DEC0, double XX, double YY,double* RA,double* DEC)
{
    *RA = RA0 + ATN(-XX / (CS(DEC0) - YY * SN(DEC0)));
    *DEC = ASN((SN(DEC0) + YY * CS(DEC0)) / sqrt(1.0 + XX * XX + YY * YY));
}


/**
 * @brief PlateConstants::EquStd Transforma coordenadas ecuatoriales a estándar
 * @param RA0   Ascensión recta del centro óptico en grados
 * @param DEC0  Declinación del centro óptico
 * @param RA    Ascensión recta de entrada
 * @param DEC   Declinación de entrada
 * @param XX    Coordenada X resultante estándar
 * @param YY    Coordenada Y resultante estándar
 */
void PlateConstants::EquStd(double RA0, double DEC0, double RA, double DEC,double* XX,double* YY)
{
    double C = CS(DEC0) * CS(DEC) * CS(RA - RA0) + SN(DEC0) * SN(DEC);
    *XX = -(CS(DEC) * SN(RA - RA0)) / C;
    *YY = -(SN(DEC0) * CS(DEC) * CS(RA - RA0) - CS(DEC0) * SN(DEC)) / C;
}


void PlateConstants::Compute()
{
   ASSERT(mCtrlPoints.count()>=3,"Attempt to compute plate constants with less than 3 points");

   double* XX = new double [mCtrlPoints.count()];
   double* YY = new double [mCtrlPoints.count()];
   FMatrix AA(mCtrlPoints.count(), 5);
   double* S = new double [3];

   //Calculamos las coordenadas estándard desde las estrellas de referencia
   int j = 0;
   for (int i = 0; i < mCtrlPoints.count(); i++)
   {
       EquStd(RA0, DEC0, mCtrlPoints[i].ra, mCtrlPoints[i].dec, XX+i, YY+i);
       AA.set(j, 0, mCtrlPoints[i].x); AA.set(j, 1, mCtrlPoints[i].y); AA.set(j, 2, 1.0);
       AA.set(j, 3, XX[i]); AA.set(j, 4, YY[i]);
       j++;
   }
   int NREF = j;

   //Calculamos las constantes PLATE A,B,C
   Lsqfit(&AA, NREF, 3, S); A = S[0]; B = S[1]; C = S[2];

   //Calcular las constantes D,E,F
   for (int i = 0; i < NREF; i++)
       AA.set(i, 3 , AA.get(i, 4));    //Copia la columna AA[*,4] -> AA[*,3];
   Lsqfit(&AA, NREF, 3, S); D = S[0]; E = S[1]; F = S[2];


   //Calcular las coordenadas ecuatoriales y errores para las estrellas de referencia
   double RA_OBS = 0, DEC_OBS = 0;
   for (int i = 0; i < mCtrlPoints.count(); i++)
   {
       XX[i] =  A * mCtrlPoints[i].x + B * mCtrlPoints[i].y + C;
       YY[i] = D * mCtrlPoints[i].x + E * mCtrlPoints[i].y + F;
       StdEqu(RA0, DEC0, XX[i], YY[i], &RA_OBS, &DEC_OBS);
       //Calcular el residuo
       (mCtrlPoints.data()+i)->delta = Util::Adist(mCtrlPoints.at(i).ra * DEG2HOR, mCtrlPoints.at(i).dec, RA_OBS * DEG2HOR, DEC_OBS) * 3600.0;
       (mCtrlPoints.data()+i)->deltaRA = fabs(mCtrlPoints.at(i).ra - RA_OBS ) * 3600.0;
       (mCtrlPoints.data()+i)->deltaDEC = fabs(mCtrlPoints.at(i).dec - DEC_OBS ) * 3600.0;
   }

   //Post proceso una vez calculadas las seis constantes
   //------------------------------------------------------------------------------
   //Calcular la distancia focal
   ComputeScaleFLRot();

   //Por úlitmo calculamos las constantes para la transformación inversa G,H,I,J,K,L
   ComputeInverse();

   //Indicamos que ya está todo calculado y liberamos memoria
   delete XX;
   delete YY;
   delete S;
   mDirty = false;

   //Estadísticas finales de residuos y desviaciones típicass
   ComputeStats();
}


void PlateConstants::ComputeStats()
{
    mMinX = mMinY = INT_MAX;
    mMaxX = mMaxY = -1;
    mDeltaAvg = mDeltaAvgRA = mDeltaAvgDEC = 0;
    for (int i=0; i<mCtrlPoints.count(); i++)
    {
        mDeltaAvg += mCtrlPoints.at(i).delta;
        mDeltaAvgRA += mCtrlPoints.at(i).deltaRA;
        mDeltaAvgDEC += mCtrlPoints.at(i).deltaDEC;
        if (mCtrlPoints.at(i).x > mMaxX) mMaxX = mCtrlPoints.at(i).x;
        if (mCtrlPoints.at(i).x < mMinX) mMinX = mCtrlPoints.at(i).x;
        if (mCtrlPoints.at(i).y > mMaxY) mMaxY = mCtrlPoints.at(i).y;
        if (mCtrlPoints.at(i).y < mMinY) mMinY = mCtrlPoints.at(i).y;
    }
    mDeltaAvg /= mCtrlPoints.count();
    mDeltaAvgRA /= mCtrlPoints.count();
    mDeltaAvgDEC /= mCtrlPoints.count();

    mDeltaStdev = mDeltaStdevRA = mDeltaStdevDEC = 0;
    for (int i=0; i<mCtrlPoints.count(); i++)
    {
        mDeltaStdev += POW2( mDeltaAvg  - mCtrlPoints.at(i).delta );
        mDeltaStdevRA += POW2( mDeltaAvgRA  - mCtrlPoints.at(i).delta );
        mDeltaStdevDEC += POW2( mDeltaAvgDEC  - mCtrlPoints.at(i).delta );
    }
    mDeltaStdev = sqrt( mDeltaStdev / mCtrlPoints.count() );
    mDeltaStdevRA = sqrt( mDeltaStdevRA / mCtrlPoints.count() );
    mDeltaStdevDEC = sqrt( mDeltaStdevDEC / mCtrlPoints.count() );
}


int PlateConstants::WorstDelta()
{
    int index = -1;
    double delta = FLOAT_MIN;
    for (int i=0; i<mCtrlPoints.count(); i++)
    {
        if (mCtrlPoints.at(i).delta > delta)
        {
            index = i;
            delta = mCtrlPoints.at(i).delta;
        }
    }
    return index;
}

void PlateConstants::ComputeInverse()
{
    //Calculamos las constantes para la transformación inversa G,H,I,J,K,L
    //Ésta solución es analítica
    G = (-E)/(D*B-E*A);
    H = B / (D*B-E*A);
    I = (E*C - F*B)/(D*B-E*A);
    J = (-D)/(A*E - D*B);
    K = A / (A*E - D*B);
    L = (D*C - A*F)/(A*E - D*B);
}

bool PlateConstants::Refine(double stdev_s)
{
    double stdev_desv = 0.002;
    double thres = mDeltaAvg + stdev_s * mDeltaStdev;

    //Primero situamos el centro en la posición calculada y recalculamos
    double ra, dec;
    double cx = mImageWidth==-1||mImageHeight==-1? (mMaxX+mMinX)/2.0 : mImageWidth/2.0 - 1;
    double cy = mImageWidth==-1||mImageHeight==-1? (mMaxY+mMinY)/2.0 : mImageHeight/2.0 - 1;
    map(cx, cy, &ra, &dec);
    setCenter(ra, dec);
    Compute();

    double last_stdev = mDeltaStdev;
    //Borramos puntos hasta que ninguno esté por encima de la desviación típica
    if (mCtrlPoints.count() > 6)
    {
        do
        {
            int idx_worst = WorstDelta();
            if (mCtrlPoints.at(idx_worst).delta < thres) return false;
            mRejectedPoints.append(mCtrlPoints.at(idx_worst));
            mCtrlPoints.remove(idx_worst);
            //Recalculamos y reposicionamos el centro
            Compute();
            cx = mImageWidth==-1||mImageHeight==-1? (mMaxX+mMinX)/2.0 : mImageWidth/2.0 - 1;
            cy = mImageWidth==-1||mImageHeight==-1? (mMaxY+mMinY)/2.0 : mImageHeight/2.0 -1;
            map(cx, cy, &ra, &dec);
            setCenter(ra, dec);
            Compute();
            thres = mDeltaAvg + stdev_s * mDeltaStdev;
            //Comprobamos la condición de parada
            if (fabs(last_stdev - mDeltaStdev) < stdev_desv) return false;
            last_stdev = mDeltaStdev;

        }
        while (mCtrlPoints.count() > 4);
    }
    return true;
}

bool PlateConstants::rejected(float x, float y)
{
    for (int j=0;j<this->rejectcount(); j++)
    {
        double rx = this->rejectedPoints(j)->x;
        double ry = this->rejectedPoints(j)->y;
        if (CART_DIST(rx, ry, x,y) < 1.5) return true;
    }
    return false;
}


double PlateConstants::posAngXY(double x1, double y1, double x2, double y2)
{
    double ra1, dec1, ra2, dec2;
    map(x1, y1, &ra1, &dec1);
    map(x2, y2, &ra2, &dec2);
    return posAngRADec(ra1, dec1, ra2, dec2);
}


double PlateConstants::posAngRADec(double ra1, double dec1, double ra2, double dec2)
{
    return Util::PosAngle(ra1, dec1, ra2, dec2);
}



















