#include <QList>
#include <QSettings>
#include <QFile>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QStringBuilder>
#include <QDomDocument>
#include "vo.h"
#include "util.h"
#include "xglobal.h"

#define     MAX_LEN_WEB_READER      2048

QList<VizierCatalog*>       VizierCatalog::SOURCE_CATALOGS;
QList<VizierServer*>        VizierServer::SOURCE_SERVERS;
QString                     SesameReader::SESAME_URL;

const char*  VizierRecord::MAGS[] = { "Vmag", "<Gmag>", "f.mag",                  //Magnitud V y magnitud G de Gaia, f de UCAC4
                                      "Rmag", "R1mag", "R2mag", "r1mag", "r2mag", //Magnitudes en rojo
                                      "imag", "Imag", "Jmag", "Hmag", "Kmag",     //Magnitudes infrarrojas
                                      "b1mag", "b2mag", "B1mag", "B2mag",         //Azules
                                      NULL };

void VizierCatalog::loadCatalogs(QSettings& settings)
{
    for (int i=0; i<SOURCE_CATALOGS.count(); i++) delete SOURCE_CATALOGS[i];

    QVariant val = settings.value("VIZIER/catalogs",QVariant(QVariant::StringList));
    if (val.isNull() || val.type() != QVariant::StringList)
    {
        //Error leyendo catálogos
        return;
    }
    QStringList catalogs = val.toStringList();
    for (int i=0; i<catalogs.count(); i++)
    {
        QString catname  = catalogs[i];
        int obj_count, max_mag;
        QString desc;
        QString path;
        QStringList fields;
        QStringList filters;
        QString mag_filter;
        double epoch;
        QString id_field;
        QString ra_field;
        QString dec_field;

        //Número de elementos en el catálogo
        val = settings.value(QString("CATALOG_%1/obj_count").arg(catname),QVariant(QVariant::Int));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN obj_count");
            continue;
        }
        obj_count = val.toInt();
        //Magnitud máxima que alcanza
        val = settings.value(QString("CATALOG_%1/max_mag").arg(catname),QVariant(QVariant::Int));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN max_mag");
            continue;
        }
        max_mag = val.toInt();
        //Descripción
        val = settings.value(QString("CATALOG_%1/desc").arg(catname),QVariant(QVariant::String));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN desc");
            continue;
        }
        desc = val.toString();
        //Descripción
        val = settings.value(QString("CATALOG_%1/path").arg(catname),QVariant(QVariant::String));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN path");
            continue;
        }
        path = val.toString();
        //Campos disponibles
        val = settings.value(QString("CATALOG_%1/fields").arg(catname),QVariant(QVariant::StringList));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN catname");
            continue;
        }
        fields = val.toStringList();
        //Filtros disponibles
        val = settings.value(QString("CATALOG_%1/filters").arg(catname),QVariant(QVariant::StringList));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN filters");
            continue;
        }
        filters = val.toStringList();
        //Composición de la URL de búsqueda
        val = settings.value(QString("CATALOG_%1/mag_filter").arg(catname),QVariant(QVariant::String));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN mag_filter");
            continue;
        }
        mag_filter = val.toString();
        //---------------------------------------------------------------------------------------------
        //Nuevos campos tras GAIA DR1
        //---------------------------------------------------------------------------------------------
        //Campo Identificador de la fuente
        val = settings.value(QString("CATALOG_%1/id_field").arg(catname),QVariant(QVariant::String));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN id_field");
            continue;
        }
        id_field = val.toString();
        //Campo ascensión recta
        val = settings.value(QString("CATALOG_%1/ra_field").arg(catname),QVariant(QVariant::String));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN ra_field");
            continue;
        }
        ra_field = val.toString();
        //Campo ascensión declinación
        val = settings.value(QString("CATALOG_%1/dec_field").arg(catname),QVariant(QVariant::String));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN dec_field");
            continue;
        }
        dec_field = val.toString();
        //Época de las coordenadas
        val = settings.value(QString("CATALOG_%1/epoch").arg(catname),QVariant(QVariant::Double));
        if (val.isNull())
        {
            Q_ASSERT_X(SOURCE_CATALOGS.count()!=catalogs.count(),"SOURCE_CATALOGS","CATALOG DOS NOT CONTAIN epoch");
            continue;
        }
        epoch = val.toFloat();

        VizierCatalog* cat = new VizierCatalog(i);
        cat->mObjectCount = obj_count;
        cat->mMagMax = max_mag;
        cat->mName = catname;
        cat->mDesc = desc;
        cat->mFields = fields;
        cat->mFilters = filters;
        cat->mCatPath = path;
        cat->magFilter = mag_filter;
        cat->mIDFieldName = id_field;
        cat->mRAFieldName = ra_field;
        cat->mDECFieldName = dec_field;
        cat->mRADECEpoch = (float)epoch;

        SOURCE_CATALOGS.append(cat);
    }
    //Comprobación del estado de los catálogos
    Q_ASSERT_X(SOURCE_CATALOGS.count()>0,"SOURCE_CATALOGS","NO CATALOGS LOADED. CHECK CATALOG LOAD");
    Q_ASSERT_X(SOURCE_CATALOGS.count()==catalogs.count(),"SOURCE_CATALOGS","SOME CATALOGS FAILED TO LOAD");
}

QList<VizierCatalog*>& VizierCatalog::getCatalogs()
{
    return SOURCE_CATALOGS;
}

void VizierCatalog::DiscardCatalogs()
{
    while (SOURCE_CATALOGS.count())
    {
        delete SOURCE_CATALOGS.at(0);
        SOURCE_CATALOGS.removeAt(0);
    }
}

VizierCatalog* VizierCatalog::getCatalog(const QString& catname)
{
    for (int i=0; i<SOURCE_CATALOGS.count(); i++)
        if (SOURCE_CATALOGS[i]->name() == catname) return SOURCE_CATALOGS[i];
    return SOURCE_CATALOGS[0];
}

QString VizierCatalog::CreateMagnitudeFilter(float max_mag)
{
    QString c = QString("%1=<%2").arg(magFilter).arg(max_mag);
    return c;
}

float VizierRecord::anyMag() const
{
    VizierCatalog* cat = VizierCatalog::SOURCE_CATALOGS[mCatIndex];
    //Intenta devolver alguna magnitud, comenzando por Vmag
    for (int i=0; MAGS[i] ;i++)
    {
        if (cat->fields().contains(MAGS[i]))
        {
            int idx = cat->fields().indexOf(MAGS[i]);
            if (idx < mFields.length() && mFields.at(idx).length())
                return mFields.at(idx).toFloat();
        }
    }
   return INVALID_FLOAT;
}

float VizierRecord::mag(const QString& pref) const
{
    VizierCatalog* cat = VizierCatalog::SOURCE_CATALOGS[mCatIndex];
    //Intenta devolver la magnitud solicitada Vmag
    if (cat->fields().contains(pref))
    {
        int idx = cat->fields().indexOf(pref);
        if (idx < mFields.length() && mFields.at(idx).length())
            return mFields.at(idx).toFloat();
    }
    return INVALID_FLOAT;
}

float VizierRecord::averageMag(QStringList sl)
{
    int cont = 0;
    float sum  = 0;
    for (int i=0; i<sl.length();i++)
    {
        float mg = mag(sl.at(i));
        if (IS_INVALIDF(mg)) continue;
        cont++; sum += mg;
    }
    if (!cont) return INVALID_FLOAT;
    return sum/cont;
}

double VizierRecord::ra() const
{
    //La ascensión recta nos viene en grados, lo pasamos a horas
    return field(1).toDouble() * DEG2HOR;
}

double VizierRecord::dec() const
{
    //Directamente devolvemos grados
    return field(2).toDouble();
}

double VizierRecord::pmra() const
{
    VizierCatalog* cat = VizierCatalog::SOURCE_CATALOGS[mCatIndex];
    int idx = cat->fields().indexOf("pmRA");
    if (idx == -1) return INVALID_FLOAT;
    bool ok;
    double pmr = mFields.at(idx).toDouble(&ok);
    if (!ok || isnan(pmr)) return INVALID_FLOAT;
    return pmr;
}

double VizierRecord::pmdec() const
{
    VizierCatalog* cat = VizierCatalog::SOURCE_CATALOGS[mCatIndex];
    int idx = cat->fields().indexOf("pmDE");
    if (idx == -1) return INVALID_FLOAT;
    bool ok;
    double pmd = mFields.at(idx).toDouble(&ok);
    if (!ok || isnan(pmd)) return INVALID_FLOAT;
    return pmd;
}

void VizierRecord::setRa(double newra)
{
   newra *= HOR2DEG;
   char buffer[64];
   snprintf(buffer,64,"%0.34f",newra);
   mFields.replace(1, buffer);
}

void VizierRecord::setDec(double newDec)
{
    char buffer[64];
    snprintf(buffer,64,"%0.34f",newDec);
    mFields.replace(2, buffer);
}

void VizierServer::LoadSourceServers(QSettings& settings)
{
    for (int i=0; i<SOURCE_SERVERS.count(); i++) delete SOURCE_SERVERS[i];

    QVariant val = settings.value("VIZIER/servers",QVariant(QVariant::StringList));
    if (val.isNull() || val.type() != QVariant::StringList)
    {
        //Error leyendo servidores
        return;
    }
    QStringList servers = val.toStringList();
    for (int i=0; i<servers.count(); i++)
    {
        QString server_name  = servers[i];
        QString desc;
        QString url;
        QStringList catalogs;
        //Descripción del servidor
        val = settings.value(QString("SERVER_%1/desc").arg(server_name),QVariant(QVariant::String));
        if (val.isNull()) continue;
        desc = val.toString();
        //Url del servidor
        val = settings.value(QString("SERVER_%1/url").arg(server_name),QVariant(QVariant::String));
        if (val.isNull()) continue;
        url = val.toString();
        //Catálogos replicados en este servidor
        val = settings.value(QString("SERVER_%1/catalogs").arg(server_name),QVariant(QVariant::StringList));
        if (val.isNull()) continue;
        catalogs = val.toStringList();

        VizierServer* server = new VizierServer();
        server->mName = server_name;
        server->mDesc = desc;
        server->mUrl = url;
        server->mCatalogs = catalogs;

        SOURCE_SERVERS.append(server);
    }

}

void VizierServer::DiscardServers()
{
    while (SOURCE_SERVERS.count())
    {
        delete SOURCE_SERVERS.at(0);
        SOURCE_SERVERS.removeAt(0);
    }
}

VizierServer* VizierServer::getServer(const QString& servname)
{
    for (int i=0; i<SOURCE_SERVERS.count(); i++)
        if (SOURCE_SERVERS[i]->name() == servname) return SOURCE_SERVERS[i];
    return SOURCE_SERVERS[0];
}

NameRecord::NameRecord()
{
    mIsNull = true;
    mRA = INVALID_FLOAT;
    mDEC = INVALID_FLOAT;
    mEntries = INVALID_INT;
    mPMRA = INVALID_FLOAT;
    mPMDEC = INVALID_FLOAT;
}

NameRecord::~NameRecord()
{

}

VizierReader::VizierReader() :
    mCatalog(VizierCatalog::SOURCE_CATALOGS[0]),
    mServer(VizierServer::SOURCE_SERVERS[0]),
    useCache_(true)
{

}

VizierReader::~VizierReader()
{

}

void VizierReader::loadConf()
{
    QSettings settings(":/conf/res/vizier.ini",QSettings::IniFormat);
    //Inizializamos el módulo de VO
    VizierServer::LoadSourceServers(settings);
    VizierCatalog::loadCatalogs(settings);
}

void VizierReader::ClearConf()
{

}

bool VizierReader::webRead(double c_ra, double c_dec, double rad_deg, float max_mag)
{
    ASSERT(!isnan(c_ra)&&!isnan(c_dec)&&!isnan(rad_deg)&&!isnan(max_mag)
           && c_ra >= 0 && c_ra <= 360
           && rad_deg < 10 && rad_deg > 0
           && max_mag <= 21 && max_mag >= 8,
           "Invalid parameters in WebRead Vizier");

    mErrorString = "";
    mFieldNames.clear();
    mUnits.clear();
    WebReader wreader;
    QString url = CreateQueryUrl(c_ra, c_dec, rad_deg, max_mag);    
    INFO("Vizier download: %s", PTQ(url));
    wreader.setUrl(url);
    wreader.setMethod(WebReader::GET);

    int respr = wreader.doRequest(useCache_);
    if (respr != 0)
    {
        mErrorString = QString("Error %1 downloading sources: %2").arg(respr).arg(WebReader::formatError(respr));
        return false;
    }

    bool in_block_1 = true;        //Bloque 1
    bool in_block_2 = false;       //Bloque 2
    bool in_block_3 = false;       //Nombres de las cabeceras
    bool in_block_4 = false;       //Unidades de las cabeceras
    bool in_block_5 = false;       //Datos

    for (int ln = 0; ln < wreader.resp()->count(); ln++)
    {
        QString line = wreader.resp()->at(ln);
        if (line.length() <= 2) continue;
        if (in_block_5)
        {
            QStringList list = line.split("|");
            for (int i=0; i<list.count(); i++)
                list.replace(i , list.at(i).trimmed());
            if (list.count() < 4)
            {
                mErrorString = "Invalid catalog data. Please try another Vizier Server";
                return false;
            }
            mEntries.append(VizierRecord(mCatalog->index(), list));
            //Necesario comprobar si la última línea añadida es válida
        }
        else if (in_block_4)
        {
             in_block_5 = true;
        }
        else if (in_block_3)
        {
            in_block_4 = true;
            mUnits = line.trimmed().split('|');
            if (mUnits.count() != mFieldNames.count() || mUnits.count() < 4)
            {
                mErrorString = "Invalid catalog data. Please try another Vizier Server";
                return false;
            }
        }
        else if (in_block_2)
        {
           if (line[0] == '#') continue;
           else
           {
               in_block_3 = true;
               mFieldNames = line.trimmed().split('|');
           }
        }
        else if (in_block_1)
        {
           if (line[0] == '#') continue;
           else in_block_2 = true;
        }
    }
    INFO("\tDownloaded %d sources%s", mEntries.count(), wreader.readFromCache()?" (from cache)":"");
    return true;
}

bool VizierReader::WebRead2(double c_ra, double c_dec, double rad_deg, float max_mag)
{
    ASSERT(!isnan(c_ra)&&!isnan(c_dec)&&!isnan(rad_deg)&&!isnan(max_mag)
           && c_ra >= 0 && c_ra <= 360
           && rad_deg < 10 && rad_deg > 0
           && max_mag <= 21 && max_mag >= 8,
           "Invalid parameters in WebRead Vizier");

    mErrorString = "";
    mFieldNames.clear();
    mUnits.clear();

    QString url = CreateQueryUrl(c_ra, c_dec, rad_deg, max_mag);
    INFO("Vizier download: %s", PTQ(url));
    QEventLoop loop;
    QNetworkAccessManager *manager = new QNetworkAccessManager();

    QNetworkReply* reply = manager->get(QNetworkRequest(url));
    QObject::connect(reply, SIGNAL(finished()) , &loop, SLOT(quit()));

    //Esperar a que acabe
    loop.exec();

    if (reply->error() != QNetworkReply::NoError)
    {
        mErrorString = reply->errorString();
        return false;
    }

    //Comenzamos a leer los datos que nos vienen de la web
    char line[MAX_LEN_WEB_READER];
    bool in_block_1 = true;        //Bloque 1
    bool in_block_2 = false;       //Bloque 2
    bool in_block_3 = false;       //Nombres de las cabeceras
    bool in_block_4 = false;       //Unidades de las cabeceras
    bool in_block_5 = false;       //Datos

    while (reply->readLine(line, MAX_LEN_WEB_READER-1) > 0)
    {
        if (in_block_5)
        {
            if (strlen(line) <= 2) continue;
            QStringList list = QString(line).split("|");
            for (int i=0; i<list.count(); i++)
                list.replace(i , list.at(i).trimmed());
            if (list.count() < 4)
            {
                mErrorString = "Invalid catalog data. Please try another Vizier Server";
                return false;
            }
            mEntries.append(VizierRecord(mCatalog->index(), list));
            //Necesario comprobar si la última línea añadida es válida

        }
        else if (in_block_4)
        {
             in_block_5 = true;
        }
        else if (in_block_3)
        {
            in_block_4 = true;
            mUnits = QString(line).trimmed().split('|');
            if (mUnits.count() != mFieldNames.count() || mUnits.count() < 4)
            {
                mErrorString = "Invalid catalog data. Please try another Vizier Server";
                return false;
            }
        }
        else if (in_block_2)
        {
           if (line[0] == '#') continue;
           else
           {
               in_block_3 = true;
               mFieldNames = QString(line).trimmed().split('|');
           }
        }
        else if (in_block_1)
        {
           if (line[0] == '#') continue;
           else in_block_2 = true;
        }
    }
    reply->deleteLater();
    INFO("\tDownloaded %d sources", mEntries.count());
    return true;
}


QString  VizierReader::CreateQueryUrl(double c_ra, double c_dec, double  rad_deg, float max_mag)
{
   QStringList url;
   url << mServer->url() << "viz-bin/asu-tsv?";
   //Catálogo de origen
   url << "-source=" << mCatalog->path();
   //Centro de la imagen
   url << "&" << "-c=" << QString("%1").number(c_ra*HOR2DEG,'f', 3) << " " << QString("%1").number(c_dec,'f', 3);
   //Radio de la imagen
   url << "&"  << "-c.r=" << QString("%1").number(rad_deg,'f', 7) << "&" << "-c.u=deg";
   //Formato de salida
   url << "&" << "-out.form=|";
   //Número de fuentes ilimitado
   url << "&-out.max=unlimited";

   //Campos de salida
   //Todos los demás campos
   for (int i=0; i<mCatalog->fields().count(); i++)
       url << "&-out=" << mCatalog->fields().at(i);

   //Por último el filtro deseado
   url << "&" << mCatalog->CreateMagnitudeFilter(max_mag);


   QString result = "";
   for (int i=0; i<url.count(); i++)
       result = result % url.at(i);

   return result;
}

SesameReader::SesameReader()
{

}

SesameReader::~SesameReader()
{

}

NameRecord SesameReader::resolveName(const QString& name)
{
    QEventLoop loop;
    QMetaObject::Connection conn = connect(this, SIGNAL(EndRead_Signal(NameRecord&)) , &loop, SLOT(quit()));
    resolveNameAsync(name);
    loop.exec();
    disconnect(conn);
    return mNameRecord;
}

void SesameReader::resolveNameAsync(const QString& name)
{
    QString url = QString("%1?%2").arg(SESAME_URL).arg(name);
    INFO("Sesame Url: %s",PTQ(url));
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    emit InitRead_Signal();

    connect(manager, SIGNAL(finished(QNetworkReply*))  , this, SLOT(finished_Slot(QNetworkReply*)));
    manager->get(QNetworkRequest(url));
}

void SesameReader::loadConf()
{
     QSettings settings(":/conf/res/vizier.ini",QSettings::IniFormat);
     //Servicio de resolución de nombres
     SESAME_URL = settings.value("VIZIER/sesame_url").toString();
}

void SesameReader::ClearConf()
{
    //De momento nada que hacer
}


void SesameReader::finished_Slot(QNetworkReply* reply)
{
    NameRecord nr;
    char line[MAX_LEN_WEB_READER];
    int sz;

    QStringList available_names;
    QString response;
    if (reply->error() == QNetworkReply::NoError)
    {
        while ((sz = reply->readLine(line, MAX_LEN_WEB_READER-1)) > 0)
        {
            while (line[sz-1] == '\n' || line[sz-1] == '\r') line[--sz] = 0;
            DINFO(line);
            response += line;
        }
    }
    //http://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-oxp/NSV?m13
    QDomDocument doc("resp");
    doc.setContent(response);
    QDomElement res = doc.documentElement()                 //Elemento raiz
                         .firstChildElement("Target")       //Primer hijo que contiene el nombre
                         .firstChildElement("Resolver");    //Elemento resuelto
    QDomElement aux;
    if (!res.isNull())
    {
        aux = res.firstChildElement("jradeg");
        if (!aux.isNull()) nr.mRA = aux.text().toFloat() * DEG2HOR;
        aux = res.firstChildElement("jdedeg");
        if (!aux.isNull()) nr.mDEC = aux.text().toFloat();
        aux = res.firstChildElement("oname");
        if (!aux.isNull()) nr.mName = aux.text().trimmed();
        if (nr.mName.startsWith("NAME ")) nr.mName = nr.mName.mid(5);
        aux = res.firstChildElement("otype");
        if (!aux.isNull()) nr.mType = aux.text().trimmed();
        //Por último se comprueba que todo es muy correcto, si no se pone el nombre a vacío
        if (!IS_VALIDF(nr.mRA) || !IS_VALIDF(nr.mDEC)) nr.mName = "";
        else
        {
            nr.mEntries = 1;
            nr.mIsNull = false;
        }
        if (nr.mName.length()) INFO("Object name found: %s", PTQ(nr.mName));
    }
    //Si no hemos encontrado un nombre le ponemos alguno de la lista de nombres adicionales
    mNameRecord = nr;
    reply->deleteLater();
    emit EndRead_Signal(nr);
}





