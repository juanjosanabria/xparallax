#include "calibration.h"

#include <QString>
#include <QDir>
#include <QHash>
#include <QThread>
#include "xglobal.h"
#include "progress.h"
#include "internal.h"
#include "util.h"


/**************** PARÁMETROS DE CALIBRACIÓN POR DEFECTO **********************/
//Algoritmo de rechazo
#define DEF_CALI_reject_algorithm       Core::PIXREJECT_SIGMA
#define DEF_CALI_reject_sigma_high      6
#define DEF_CALI_reject_sigma_low       7
#define DEF_CALI_reject_min             0
#define DEF_CALI_reject_max             50000
//Parámetros del archivo de salida
#define DEF_CALI_norm_level              0
#define DEF_CALI_bit_pp                 16
//Guardado de masters
#define DEF_CALI_save_master_flat       true
#define DEF_CALI_save_master_dark       true
#define DEF_CALI_save_master_bias       true
//Datos no guardables
#define DEF_CALI_output_dir             ""
//Corrección de hot y cold pixels
//Corrección de hot pixels
#define   DEF_CALI_hot_cold_connectivity  Core::CONNECTIVITY_EIGHT
#define   DEF_CALI_hot_pixel_sigma        100   //Pre-proceso
#define   DEF_CALI_dead_pixel_sigma       50    //Pre-proceso
#define   DEF_CALI_cosmic_ray_sigma       20    //Post-proceso
#define   DEF_CALI_cold_pixel_sigma       15    //Post-proceso


CalibrationList::~CalibrationList()
{
}

CalibrationList::CalibrationList()
{

}

CalibrationList::CalibrationList(const QStringList &list)
{
    mError = false;

    Append(list);
}

void CalibrationList::Clear()
{
    mTotalList.clear();
    mBiasList.clear();
    mLightList.clear();
    mDarkList.clear();
    mFlatList.clear();
    mHeaders.clear();
}

int CalibrationList::idx(QString name)
{
    for (int i=0;i<mTotalList.length();i++)
        if (mTotalList.value(i) == name) return i;
    return -1;
}

FitHeaderList CalibrationList::header(int idx)
{
    int cont = 0;
    for (QLinkedList<FitHeaderList>::Iterator i = mHeaders.begin(); i != mHeaders.end() ; ++i,cont++)
        if (idx == cont) return *i;
    return FitHeaderList();
}

FitHeaderList CalibrationList::header(QString name)
{
    int i = idx(name);
    if (i<0) return FitHeaderList();
    return header(i);
}


void CalibrationList::Append(const QStringList &list)
{
    for (int i=0; i<list.length(); i++)
    {
       QString filename = list.value(i);
       //Asegurarnos de que son archivos fits
       if (!filename.endsWith(".fit",Qt::CaseInsensitive) &&
             !filename.endsWith(".fits",Qt::CaseInsensitive))
            continue;
       FitHeaderList fhe =  FitHeaderList::ReadFromFile(filename);
       if (!fhe.count())
       {
           mError = true;
           return;
       }
       mHeaders.append(fhe);
       mTotalList.append(filename);

       //Meter el archivo en la lista que corresponda
       if (GetType(filename) == Core::FRAME_FLAT || fhe.getImageType() == FitHeaderList::Flat)
          mFlatList.append(filename);
       else if (GetType(filename) == Core::FRAME_DARK || fhe.getImageType() == FitHeaderList::Dark)
           mDarkList.append(filename);
       else if (GetType(filename) == Core::FRAME_BIAS || fhe.getImageType() == FitHeaderList::Bias)
           mBiasList.append(filename);
       else
           mLightList.append(filename);
    }
   //Ordenamos los archivos en las listas secundarias
   mLightList.sort(Qt::CaseInsensitive);
   mFlatList.sort(Qt::CaseInsensitive);
   mDarkList.sort(Qt::CaseInsensitive);
   mBiasList.sort(Qt::CaseInsensitive);
}

Core::FrameType CalibrationList::GetType(const QString &filename)
{
    if (filename.contains("flat",Qt::CaseInsensitive) )
       return Core::FRAME_FLAT;
    else if (filename.contains("dark",Qt::CaseInsensitive))
        return Core::FRAME_DARK;
    else if (filename.contains("bias",Qt::CaseInsensitive))
       return Core::FRAME_BIAS;
    else
        return Core::FRAME_LIGHT;

}

QStringList CalibrationList::GetAll()
{
    QStringList result = QStringList();
    for (int i=0;i<mLightList.size();i++)
        result.append(mLightList.value(i));
    for (int i=0;i<mFlatList.size();i++)
        result.append(mFlatList.value(i));
    for (int i=0;i<mDarkList.size();i++)
        result.append(mDarkList.value(i));
    for (int i=0;i<mBiasList.size();i++)
        result.append(mBiasList.value(i));
    return result;
}

QStringList CalibrationList::GetAll(Core::FrameType type)
{
    switch(type)
    {
        case Core::FRAME_BIAS:
        return mBiasList;
        break;
        case Core::FRAME_LIGHT:
        return mLightList;
        break;
        case Core::FRAME_DARK:
        return mDarkList;
        break;
        case Core::FRAME_FLAT:
        return mFlatList;
        break;
        default:
        return QStringList();
    }
}

int CalibrationList::IndexOf(QString fname)
{
    for (int i=0;i<mTotalList.size();i++)
        if (mTotalList.value(i) == fname) return i;
    return -1;
}

QStringList CalibrationList::GetFramesByExpTime(Core::FrameType type, int exptime)
{
    QStringList baselist = GetAll(type);
    QStringList result = QStringList();
    for (int i=0; i<baselist.size(); i++)
    {
        QString filename = baselist.value(i);
        int index = IndexOf(filename);
        FitHeaderList fhe = header(index);
        if (fhe.exptime() == exptime)
            result.append(filename);
    }
    return result;
}

QVector<int> CalibrationList::ExpTimes()
{
    QVector<int> arr = QVector<int>();
    for (int i=0;i<mTotalList.count();i++)
    {
        QString filename = mTotalList.value(i);
        if ( GetType(filename) == Core::FRAME_BIAS ) continue;

        FitHeaderList hdr = header(i);
        if (hdr.exptime() == 0) continue;

        if (!arr.contains(hdr.exptime()))
            arr.append(hdr.exptime());
    }
    return arr;
}

ImStat::ImStat(int histosz)
{
    mHistoSize = histosz;
    mThresMin = 0;
    mThresMax = FLOAT_MAX;
}

ImStat::~ImStat()
{

}

ImStat ImStat::ComputeRect(float *data,int width, int min_x, int min_y, int max_x, int max_y)
{
    float min = data[min_x + min_y * width];
    float max = data[min_x + min_y * width];
    int rect_width = max_x - min_x + 1;
    int rect_height = max_y - min_y + 1;
    //Para evitar desbordamientos tendremos que calcular la media en filas y luego promediar
    double avg_sum = 0;
    for (int y=min_y; y <= max_y; y++)
    {
        double avg_line = 0;
        for (int x=min_x; x <= max_x; x++)
            avg_line += data[x + y * width];
        avg_sum += (avg_line/(double)rect_width);
    }
    double avg = avg_sum / (double)rect_height;
    //Y la desviación típica tampoco podemos calcularla con la manera corta
    double varian_sum = 0;
    for (int y=min_y; y <= max_y; y++)
    {
        double varian_line = 0;
        for (int x=min_x; x <= max_x; x++)
        {
            float fdata = data[x + y * width];
            varian_line += (fdata - avg) * (fdata - avg);
            if (min > fdata) min = fdata;
            if (max < fdata) max = fdata;
        }
        varian_sum += (varian_line/(double)rect_width);
    }
    double varian = varian_sum/(double)rect_height;
    double stdev = sqrt(varian);
    ImStat istat;
    istat.mAverage = avg;
    istat.mStdev = stdev;
    istat.mMin = min;
    istat.mMax = max;
    istat.mN = rect_width * rect_height;
    return istat;
}

ImStat ImStat::Compute(float* data,int width, int height)
{
    return ImStat::ComputeRect(data, width, 0, 0, width-1, height - 1);
}

float ImStat::Median(int* histo,int histo_sz, float min_histo, float max_histo)
{
    //Comenzamos contando cuantos datos hay
    int n = 0;
    for (int i=0; i<histo_sz; i++)
        n += histo[i];
    double midval = n/2.0;
    //Recorremos de izquierda a derecha hasta que alcancemos midval
    int izq = -1;
    int accu_izq = 0;
    for (; izq<histo_sz && accu_izq+histo[izq+1] < midval; izq++, accu_izq += histo[izq] );
    //Recorremos de derecha a izquierda
    int der = histo_sz;
    int accu_der = n;
    for (; der > izq && der > 1 && accu_der-histo[der-1] > midval; der--, accu_der -= histo[der]);
    //Ahora calculamos la mediana
    double median = (midval - accu_izq)
            / (accu_der - accu_izq)
            * (der - izq);
    return (float) RANGECHANGE(median, 0, histo_sz, min_histo, max_histo);
}

ImStat ImStat::ComputeRect(float *data, int width, int min_x, int min_y, int max_x, int max_y,
                           float ignore_min, float ignore_max,int histo_sz)
{
    if (histo_sz > IMSTAT_MAX_HISTO_SIZE) histo_sz = IMSTAT_MAX_HISTO_SIZE;
    ImStat istat(histo_sz);
    istat.mThresMin = ignore_min;
    istat.mThresMax = ignore_max;
    float min = FLOAT_MAX;
    float max = FLOAT_MIN;
    istat.mN = 0;

    //Para evitar desbordamientos tendremos que calcular primero la media y luego la desviación típica
    double avg_sum = 0;
    for (int y=min_y; y <= max_y; y++)
    {
        for (int x=min_x; x <= max_x; x++)
        {
            //Ignorar píxeles fuera dle máximo o el mínimo
            float fdata = data[x + y * width];
            if (fdata < ignore_min || fdata > ignore_max)
                continue;
            avg_sum += data[x + y * width];
            if (min > fdata)
                min = fdata;
            if (max < fdata)
                max = fdata;
            istat.mN++;
        }
    }
    if (!istat.mN) return istat;
    double avg = avg_sum / (double) istat.mN;

    //Y la desviación típica tampoco podemos calcularla con la manera corta

    //A la vez calculamos los valores para el histogramaa
    memset(istat.mHisto,0,sizeof(int)*histo_sz);
    istat.mHistoLow = min;
    istat.mHistoHigh = max;
    double varian_sum = 0;
    for (int y=min_y; y <= max_y; y++)
    {
        for (int x=min_x; x <= max_x; x++)
        {
            float fdata = data[x + y * width];
            if (fdata < ignore_min || fdata > ignore_max) continue;
            varian_sum += (fdata - avg) * (fdata - avg);
            //Quizá haya que calcular histograma
            if (histo_sz)
            {
                int histo_pos = (int)RANGECHANGE(fdata,min,max,0,histo_sz);
                if (histo_pos < 0) histo_pos = 0;
                else if (histo_pos >= histo_sz) histo_pos = histo_sz-1;
                istat.mHisto[histo_pos]++;
            }
        }
    }
    double varian = varian_sum/(double)istat.mN;
    if (varian <= 0) varian = 0;
    double stdev = sqrt(varian);
    istat.mVariance = varian;
    istat.mStdev = stdev;
    istat.mAverage = avg;
    istat.mMin = min;
    istat.mMax = max;
    //Si hay histograma calculamos la moda
    if (histo_sz)
    {
        int maxidx = 0;
        int maxval = istat.mHisto[0];
        for (int i=1; i<histo_sz; i++)
        {
            if (istat.mHisto[i] > maxval)
            {
                maxidx = i;
                maxval = istat.mHisto[i];
            }
        }
        //Transformamos el índice de histograma a valor
        istat.mMode = RANGECHANGE(maxidx, 0, histo_sz, istat.mHistoLow,istat.mHistoHigh);
        //Cálculo de la mediana
        istat.mMedian = Median(istat.mHisto, histo_sz, istat.mHistoLow, istat.mHistoHigh);
    }
    return istat;
}

CalibrationParameters::CalibrationParameters()
{
    SetDefaults();
}


CalibrationParameters::~CalibrationParameters()
{

}

void CalibrationParameters::SetDefaults()
{
   p_bkg.SetDefaults();
   //Algoritmo de rechazo
   reject_algorithm = DEF_CALI_reject_algorithm;
   reject_sigma_high = DEF_CALI_reject_sigma_high;
   reject_sigma_low = DEF_CALI_reject_sigma_low;
   reject_min = DEF_CALI_reject_min;
   reject_max = DEF_CALI_reject_max;
   //Parámetros del archivo de salida
   norm_level = DEF_CALI_norm_level;
   bit_pp = DEF_CALI_bit_pp;
   //Corrección de hot y cold pixels

   //Corrección de hot pixels
   hot_cold_connectivity = DEF_CALI_hot_cold_connectivity;
   hot_pixel_sigma = DEF_CALI_hot_pixel_sigma;
   dead_pixel_sigma = DEF_CALI_dead_pixel_sigma;
   cosmic_ray_sigma = DEF_CALI_cosmic_ray_sigma;
   cold_pixel_sigma = DEF_CALI_cold_pixel_sigma;


   //Guardado de masters
   save_master_flat = DEF_CALI_save_master_flat;
   save_master_dark = DEF_CALI_save_master_dark;
   save_master_bias = DEF_CALI_save_master_bias;
   //Datos no guardables
   output_dir = DEF_CALI_output_dir;
}

void CalibrationParameters::Save(QSettings* settings,const QString& section)
{
    p_bkg.Save(settings, section);

    //Parámetros de rechazo de píxeles
    SaveInt(settings, section,   "reject_algorithm", reject_algorithm);
    SaveFloat(settings, section, "reject_sigma_low", reject_sigma_low);
    SaveFloat(settings, section, "reject_sigma_high", reject_sigma_high);
    SaveFloat(settings, section, "reject_min", reject_min);
    SaveFloat(settings, section, "reject_max", reject_max);

    //Parámetro de normalización
    SaveFloat(settings, section, "norm_level", norm_level );
    SaveInt(settings, section,   "bit_pp" , bit_pp);

    //Parámetros de hot y cold pixels
    SaveInt(settings, section, "hot_cold_connectivity", hot_cold_connectivity);
    SaveFloat(settings, section, "hot_pixel_sigma", hot_pixel_sigma);
    SaveFloat(settings, section, "dead_pixel_sigma", dead_pixel_sigma);
    SaveFloat(settings, section, "cosmic_ray_sigma", cosmic_ray_sigma);
    SaveFloat(settings, section, "cold_pixel_sigma", cold_pixel_sigma);

    //Parámetros de guardado de masters
    SaveBool(settings,section, "save_master_flat", save_master_flat);
    SaveBool(settings,section, "save_master_dark", save_master_dark);
    SaveBool(settings,section, "save_master_bias", save_master_bias);
}

void CalibrationParameters::Load(QSettings* settings,const QString& section)
{
    p_bkg.Load(settings, section);
    reject_algorithm =(Core::PixRejectAlgorithm) ReadInt(settings, section, "reject_algorithm",(int) DEF_CALI_reject_algorithm);
    reject_sigma_low = ReadFloat(settings, section, "reject_sigma_low", DEF_CALI_reject_sigma_low);
    reject_sigma_high = ReadFloat(settings, section, "reject_sigma_high", DEF_CALI_reject_sigma_high);
    reject_min = ReadFloat(settings, section, "reject_min", DEF_CALI_reject_min);
    reject_max = ReadFloat(settings, section, "reject_max", DEF_CALI_reject_max);
    norm_level = ReadFloat(settings, section, "norm_level", DEF_CALI_norm_level);
    bit_pp = ReadInt(settings, section, "bit_pp", DEF_CALI_bit_pp);
    save_master_flat = ReadBool(settings, section, "save_master_flat", DEF_CALI_save_master_flat);
    save_master_dark = ReadBool(settings, section, "save_master_dark", DEF_CALI_save_master_dark);
    save_master_bias = ReadBool(settings, section, "save_master_bias", DEF_CALI_save_master_bias);
}

QString CalibrationManager::Calibrate( CalibrationList files, CalibrationParameters parameters, ProgressManager *progress_mg )
{
     //Tamaño de las imágenes
     int width = files.header(0).width();
     int height = files.header(0).height();

     //Inventario de archivos
     QStringList sub_light = files.GetAll(Core::FRAME_LIGHT);
     QStringList sub_bias = files.GetAll(Core::FRAME_BIAS);
     QStringList sub_flats = files.GetAll(Core::FRAME_FLAT);

     float* master_bias_data = new float[width * height];
     float* master_flat_data = new float[width * height];
     QVector<int> exptimes = files.ExpTimes();
     float** master_dark_data = new float*[exptimes.count()];
     for (int i=0;i<exptimes.count(); i++)
         master_dark_data[i] = new float[width * height];

     //Creación de los manejadores de progreso necesarios
     bool need_delete_pm = !progress_mg;
     if (!progress_mg) progress_mg = new VoidProgressManager();

     progress_mg->AddProgres("Initializing calibration",1);
     progress_mg->AddNullProgress();
     ProgressElement* prog_bias = progress_mg->AddProgres("Creating master BIAS",sub_bias.count());
     prog_bias->SetStatus("Creating frame");
     ProgressElement* prog_dark =  progress_mg->AddProgres("Creating master DARK",exptimes.count());
     prog_dark->SetStatus("Creating frame");
     ProgressElement* prog_flat = progress_mg->AddProgres("Creating master FLAT",sub_flats.count());
     prog_flat->SetStatus("Creating frame");
     ProgressElement* prog_light = progress_mg->AddProgres("Calibrating images",sub_light.count());
     prog_light->SetStatus("---");


     //Creamos el master bias
     /*
     QStringList files,
     float* result, float** substract,
     Core::PixRejectAlgorithm rejectalg,
     Core::BackgroundMethod bkgmethod, int bkg_mesh_size,int bkg_smooth,
     float sigma_upper, float sigma_lower,
     float reject_min, float reject_max,
     int normalize_level*/

      ImCombineAverage(sub_bias, master_bias_data, NULL, Core::PIXREJECT_NONE,
                      parameters.p_bkg.bkg_method, parameters.p_bkg.bkg_mesh_size, parameters.p_bkg.bkg_smooth,
                      parameters.reject_sigma_high, parameters.reject_sigma_low, 0, 65535, 0);
     /*ImCombineAverage(prog_bias,
                      sub_bias,master_bias_data, NULL, Core::PIXREJECT_NONE,
                      parameters.p_bkg.bkg_method, parameters.p_bkg.bkg_mesh_size, parameters.p_bkg.bkg_smooth, 0, 0, 0);*/
     if (parameters.save_master_bias)
     {
         QString dstpath = QDir::cleanPath(parameters.output_dir + QDir::separator() + "master_bias.fit");
         FitImage::SaveToFile(master_bias_data, width, height , dstpath, parameters.bit_pp);
     }


     //Creamos un master dark por cada tiempo de exposición. A la vez buscamos hot y cold pixels
     for (int i=0; i<exptimes.count() && !progress_mg->cancelled(); i++)
     {
         QStringList dark_exp = files.GetFramesByExpTime(Core::FRAME_DARK, exptimes[i]);
         prog_dark->SetPosition(i);
         prog_dark->SetStatus(QString("Exposition: '%1' seconds").arg(exptimes[i]));

         if (dark_exp.length() == 0)
         {
             //Copiamos la información del bias frame
             memcpy(master_dark_data[i], master_bias_data, width*height*sizeof(float));
         }
         else
         {
            //Creamos un master dark con los archivos obtenidos
            //ImCombineAverage(prog_bias, dark_exp, master_dark_data[i], NULL, Core::PIXREJECT_NONE, parameters.p_bkg.bkg_method, parameters.p_bkg.bkg_mesh_size, parameters.p_bkg.bkg_smooth, 0, 0, 0);
            if (parameters.save_master_dark)
            {
                QString dstpath = QDir::cleanPath(parameters.output_dir + QDir::separator() + QString().sprintf("master_dark_%d.fit",exptimes[i]));
                FitImage::SaveToFile(master_dark_data[i], width, height , dstpath, parameters.bit_pp);
            }
         }
     }

     //Creamos un master flat
     //----------------------------------------------------------------------------------------------------
     if (sub_flats.count() == 1 && sub_flats.at(0).toUpper().contains("MASTER"))
     {
         //Cuidado porque es un master flat!!

     }
     else
     {
         //Para comenzar creamos una lista con las correcciones de dark o bias a añadir a cada flat
         float** substract = NULL; new float*[sub_flats.length()];
         for (int i=0; i<sub_flats.length() && !progress_mg->cancelled();i++)
         {
              FitHeaderList fhe = FitHeaderList::ReadFromFile(sub_flats.value(i));
              int idx = exptimes.indexOf(fhe.exptime(),0);
              if (idx < 0) substract[i] = master_bias_data;
              else substract[i] = master_dark_data[idx];
         }


         //Ahora creamos realmente el master flat
         ImCombineAverage(sub_flats, master_flat_data, substract, parameters.reject_algorithm,
                          parameters.p_bkg.bkg_method, parameters.p_bkg.bkg_mesh_size, parameters.p_bkg.bkg_smooth,
                          parameters.reject_sigma_high, parameters.reject_sigma_low, parameters.reject_min, parameters.reject_max, 1);
         delete substract;
     }

     if (parameters.save_master_flat)
     {
         int norm_level = 0;
         //Cuidado, quizá antes de guardar hay que normalizar el master flat
         if (parameters.bit_pp == 8)
             norm_level = 100;
         else if (parameters.bit_pp == 16)
            norm_level = 10000;

         QString dstpath = QDir::cleanPath(parameters.output_dir + QDir::separator() + "master_flat.fit");
         FitImage::SaveToFile(master_flat_data, width, height , dstpath, parameters.bit_pp, norm_level);
     }

     //Es hora de calibrar, calibramos uno por uno los frames de la imagen
     float* outp = new float[width * height];
     for (int i=0; i<sub_light.length() && !progress_mg->cancelled(); i++)
     {
         prog_light->SetPosition(i);
         prog_dark->SetStatus(QString("File: '%1'").arg(sub_light.value(i)));

         QString srcfile = sub_light.value(i);
         QString dstpath = QDir::cleanPath(parameters.output_dir + QDir::separator() + QFileInfo(srcfile).baseName() + ".fit");
         FitImage* fi = FitImage::OpenFile(srcfile);

         //Escoger el pedestal a sustraer (dark o bias)
         float *substract = NULL;
         int exptime = fi->exptime();
         if (exptimes.contains(exptime)) substract = master_dark_data[exptimes.indexOf(exptime)];
         else substract = master_bias_data;

         //Realizar la correción de dark (o bias)
         if (substract)
            Imarith(fi->data(), substract, Core::IMARITH_SUBSTRACT, outp, width, height);
         else
             memcpy(outp, fi->data(), sizeof(float)*width*height);


         //Realizar la corrección de flat
         Imarith(outp, master_flat_data, Core::IMARITH_DIVIDE, outp, width,height );

         //Normalizamos
         if (parameters.norm_level > 0)
            NormalizeAverage(outp, parameters.norm_level, width, height);

         //Por último guardamos el archivoB
         FitImage::SaveToFile(outp, fi->headers(), dstpath, parameters.bit_pp);

         delete fi;
     }
     prog_light->SetPosition(prog_light->value());

     delete outp;

     //Borrar toda la memoria reservada antes de salir
     if (need_delete_pm) delete progress_mg;

     delete master_bias_data;
     delete master_flat_data;
     for (int i=0;i<exptimes.count(); i++)
         delete master_dark_data[i];
     delete master_dark_data;

     return "";
}


/**
 * @brief CalibrationManager::DetectHotAndColdPixels Detecta píxeles calientes y fríos
 * @param data Datos de la imagen a tratar
 * @param width Ancho de la imagen
 * @param height Alto de la imagen
 * @param conec Tipo de conectividad Core::PixelConectivityType
 * @param lpix Lista de QPoint donde se devolverán los píxeles afectados
 * @param sigma_hot Umbral sigma para detectar píxeles calientes. Si es menor o igual que cero no se buscarán píxeles calientes
 * @param sigma_cold Umbral sigma para detectar píxeles fríos. Si es menor o igual que cero no se buscarán píxeles fríos
 */
void CalibrationManager::DetectHotAndColdPixels(float* data,Core::PixelConectivityType /*conec*/,BitMatrix* lpix, float sigma_hot, float sigma_cold)
{
    BackgroundMap bkg;
    bkg.Setup(Core::BKG_KAPPA_SIGMA, lpix->width(), lpix->height(), 64, 1, 3, 4, false);
    bkg.Compute(data);
    bool detect_hot = sigma_hot > 0;
    bool detect_cold = sigma_cold > 0;
    //Suponemos que la matriz de bits está por completo a cero

    int hot_count = 0, cold_count = 0;

    //Variables de uso interno
    int width = lpix->width();
    int height = lpix->height();

    for (int y=1; y<height - 1; y++)
    {
        for (int x=+1; x<width - 1; x++)
        {
            float val = data[y * width + x];
            float bkgval = bkg.backentry(x, y)->Back;
            float bkgstdev = bkg.backentry(x, y)->StandardDev;

            //Detección del hot o el cold pixel
            if (detect_hot && val > bkgval + sigma_hot*bkgstdev)
            {
                lpix->set(x, y, true);
                hot_count++;
            }
            else if (detect_cold && val < bkgval - sigma_cold*bkgstdev)
            {
               lpix->set(x, y, true);
               cold_count++;
            }
        }
    }
    //Logamos información sobre la detección
    if (detect_hot && !detect_cold)
    INFO("Detected %d hot pixels", hot_count);
    else if (detect_cold && !detect_hot)
    INFO("Detected %d cold/dead pixels", cold_count);
    else
    INFO("Detected %d hot and %d cold pixels", hot_count, cold_count);
}


void CalibrationManager::RemoveCosmicRaysImpacts(float* data,
                                                Core::PixelConectivityType conec,
                                                float sigma_hot,
                                                float sigma_cold,int width, int height)
{
    //Revisar esto http://deepskystacker.free.fr/english/technical.htm
    float sqrt2 = sqrt(2);
    float eight_total = sqrt2 * 4 + 4;
    int cosmic_count = 0;
    int cold_count = 0;

    bool remove_hot = sigma_hot > 0;
    bool remove_cold = sigma_cold > 0;
    float avg = 0;
    float stdv = 0;
    for (int y=1; y<height-1; y++)
    {
        for (int x=+1; x<width-1; x++)
        {
            float val = data[y * width + x];
            float up = data[ (y-1) * width + x];
            float left = data[y * width + x-1];
            float right = data[y * width + x+1];
            float down = data[(y+1) * width + x];

            if (conec == Core::CONNECTIVITY_FOUR)
            {
                avg = (up + left + right + down)/4.0;
                stdv = sqrt( (POW2(up-avg) + POW2(left-avg) + POW2(right-avg) + POW2(down-avg)) / 4 );
            }
            else if (conec == Core::CONNECTIVITY_EIGHT)
            {
                float upleft = data[ (y-1) * width + x - 1];
                float upright = data[ (y-1) * width + x + 1];
                float downleft = data[ (y+1) * width + x - 1];
                float downright = data[ (y+1) * width + x + 1];
                avg = (up  + left   + right  + down  +  upleft  + upright  + downleft  + downright) / 8;
                stdv = sqrt( (POW2(up-avg) + POW2(left-avg) + POW2(right-avg) + POW2(down-avg)
                              + POW2(upleft-avg) + POW2(upright-avg) + POW2(downleft-avg) + POW2(downright-avg)
                              ) / eight_total );

            }
            else continue; //Caso no comtemplado, no hacer nada

            if (remove_hot && val > avg + sigma_hot*stdv)
            {
                data[y * width + x] = avg;
                cosmic_count++;
            }
            else if (remove_cold && val < avg - sigma_cold*stdv)
            {
                data[y * width + x] = avg;
                cold_count++;
            }
        }
    }
    INFO("Cosmic rays and cold pixels fixed found: %d/%d",cosmic_count, cold_count);
}

/**
 * @brief CalibrationManager::RemoveHotAndColdPixels Elimina los píxeles calientes y fríos procedentes de una lista (Bitmatrix).
 *        La altura y anchura de la imagen se sacará de la misma matriz de bits.
 * @param data Datos de la imagen
 * @param conec Conectividad de píxeles. Cuatro u ocho
 * @param to_replace. Matriz de bits
 */
void CalibrationManager::RemoveHotAndColdPixels(float* data,Core::PixelConectivityType conec, BitMatrix* to_replace)
{
    if (!data || !to_replace) return;
    //Revisar esto http://deepskystacker.free.fr/english/technical.htmx
    int width = to_replace->width();
    int height = to_replace->height();

    for (int y=0; y<height-1; y++)
    {
        for (int x=0; x<width-1; x++)
        {
            if (to_replace->get(x,y) || x == 155)
            {
                float sum_avg = 0;
                int sum_count = 0;

                //Píxel de arriba
                if (y-1 >= 0 && !to_replace->get(x, y-1)) { sum_avg += data[ (y-1) * width + x]; sum_count++; }
                //Pixel izquierda
                if (x-1 >= 0 && !to_replace->get(x-1, y)) { sum_avg += data[ y * width + (x-1)]; sum_count++; }
                //Pixel derecha
                if (x+1 < width && !to_replace->get(x+1, y)) { sum_avg += data[ y * width + (x+1)]; sum_count++;}
                //Píxel abajo
                if (y+1 < height && !to_replace->get(x, y+1)){ sum_avg +=  data[(y+1) * width + x]; sum_count++;}

                if (conec == Core::CONNECTIVITY_EIGHT)
                {
                    if (y-1 >= 0 && x-1 >= 0 && !to_replace->get(x-1, y-1)) {sum_avg += data[ (y-1) * width + x - 1]; sum_count++; }
                    if (y-1 >= 0 && x+1 < width && !to_replace->get(x+1, y-1)) {sum_avg += data[ (y-1) * width + x + 1]; sum_count++; }
                    if (y+1 < height && x-1 >= 0 && !to_replace->get(x-1, y+1)) {sum_avg += data[ (y+1) * width + x - 1]; sum_count++; }
                    if (y+1 < height && x+1 < width && !to_replace->get(x+1, y+1))  {sum_avg += data[ (y+1) * width + x + 1]; sum_count++; }
                }

                data[y * width + x] = sum_avg / sum_count;
            }
        }
    }
}

void CalibrationManager::RestoreSourceValues(float* src, float* dst, int width, int height,
                                             float min_val, float sat_val,float rep_min, float rep_max)
{
    int total = width*height;
    for (int i=0; i<total; i++)
    {
       if (src[i] < min_val || dst[i] < min_val) dst[i] = rep_min;
       else if (src[i] > sat_val || dst[i] > sat_val) dst[i] = rep_max;
    }
}

void CalibrationManager::AvoidNegativeValues(float* data,int width, int height)
{
    float minval = 65000;
    int total = width*height;
    double negval_sum = 0;
    double negval_square = 0;
    int negval_count = 0;
    for (int i=0; i<total; i++)
    {
       if (data[i] < 0)
       {
           negval_sum+= data[i];
           negval_square += POW2(data[i]);
           negval_count++;
       }
       if (data[i] < minval) minval = data[i];
    }
    double avgneg = negval_sum/negval_count;
    double stdneg = sqrt(negval_square/negval_count - POW2(avgneg));
    //Si hay valores negativos intentaremos esta corrección
    if (minval < 0 || avgneg < 0)
    {
        float pedestal = 0;
        //Si son más del 1% aplicamos un pedestal basado en desviación típica
        if ( negval_count/(double)total >= 0.01 )
        {
            pedestal  = ceil(-avgneg + 2*stdneg);
        }
        //Si son menos todavía < 1% > 0.5% aplica un pedestal basado en la media
        else if ( negval_count/(double)total >= 0.005 )
        {
            pedestal  = ceil(-avgneg + stdneg);
        }
        //Si son menos todavía < 5% > 0.25% aplica un pedestal basado en la media
        else if ( negval_count/(double)total >= 0.0025 )
        {
            pedestal  = ceil(-avgneg);
        }

        //Aplicamos la corrección a toda la imagen
        for (int i=0; i<total; i++)
        {
            data[i] += pedestal;
            if (data[i] < 0) data[i] = 1;
        }
    }
}


bool CalibrationManager::ImCombineAverage(
                                   QStringList files,
                                   float* result, float** substract,
                                   Core::PixRejectAlgorithm rejectalg,
                                   Core::BackgroundMethod bkgmethod, int bkg_mesh_size,int bkg_smooth,
                                   float sigma_upper, float sigma_lower,
                                   float reject_min, float reject_max,
                                   int normalize_level
                                    )
{
    //Valores de fondo de cada uno de los archivos
    QVector<BackgroundMap*> back = QVector<BackgroundMap*>(files.length(), NULL);
    QVector<FitScanner*> scaners =  QVector<FitScanner*>(files.length(), NULL);
    float* averages = NULL;
    float* exptimes = NULL; float expmax = 0;
    int width = 0, height = 0;
    int imsz = files.length();


    //Calcular los valores de fondo de cada archivo
    if (rejectalg != Core::PIXREJECT_NONE || normalize_level)
    {
        bool err = false;
        if (normalize_level) averages = new float[files.length()];  //Normalizar mediante una media
        else exptimes = new float[files.length()];                  //Homogeneizar usando tiempos de exposición
        for (int i=0; i<imsz; i++)
        {
            FitImage *fi = FitImage::OpenFile(files.value(i));
            if (exptimes && (exptimes[i] = fi->exptime()) > expmax) expmax = exptimes[i];
            if (!fi)
            {
                err = true;
                break;
            }
            if (!width || !height)
            {
                width = fi->width();
                height = fi->height();
            }
            if (rejectalg != Core::PIXREJECT_NONE)
            {
                back[i] = new BackgroundMap();
                back[i]->Setup(bkgmethod, width, height, bkg_mesh_size, bkg_smooth, BKG_SIGMA_LOWER, BKG_SIGMA_UPPER);
                back[i]->Compute(fi->data());
            }
            if (averages) averages[i] = fi->averagepix();
            delete fi;
        }
        if (err)
        {
            for (int i=0; i<back.count(); i++) if (back[i] != NULL) delete back[i];
            return false;
        }
    }
    //Calcular los tiempos de exposición de cada archivo para hacer una media ponderada
    if (!exptimes)
    {
        exptimes = new float[files.length()];
        for (int i=0; i<imsz; i++)
        {
            FitHeaderList fi = FitHeaderList::ReadFromFile(files.value(i));
            if ((exptimes[i] = fi.exptime()) > expmax) expmax = exptimes[i];
        }
    }
    //Si todos los tiempos de exposición son cero es un bias
    if (expmax < 0.1)
    {
        for (int i=0; i<imsz; i++) exptimes[i] = 1;
        expmax = 1;
    }

    //Abro un flujo por archivo y creo una zona de memoria para una línea de cada uno
    float ** scanlines = new float*[imsz];
    memset(scanlines,0,sizeof(float*)*imsz);

    for (int i=0;i<imsz; i++)
    {
        FitScanner* fscan = new FitScanner(files.value(i));
        scaners[i] = fscan;
        if (!width || !height)
        {
            width = fscan->width();
            height = fscan->height();
        }
        scanlines[i] = new float[width];
    }

    //Ahora calculamos la media aplicando corrección de dark o flat si estuviera disponible
    for (int y=0; y<height;y++)
    {
        for (int i=0;i<imsz;i++) scaners[i]->ReadScanLine(scanlines[i]);

        for (int x=0;x<width;x++)
        {
            float total_flux = 0;
            for (int i = 0; i < imsz; i++)
            {
                float flux = scanlines[i][x];
                float subst_level = substract?substract[i][x + y * width]:0;
                float norm_level = averages ? (float)normalize_level / averages[i] : exptimes[i] / expmax;
                switch (rejectalg)
                {
                    case Core::PIXREJECT_NONE:
                        total_flux += ((flux - subst_level) * norm_level);
                        break;
                    case Core::PIXREJECT_SIGMA:
                    {
                        BackgroundMapEntry* bke = back[i]->backentry(x, y);
                        float max_val = bke->Back + sigma_upper * bke->StandardDev;
                        float min_val = bke->Back - sigma_lower * bke->StandardDev;
                        if (flux < max_val && flux > min_val)
                            total_flux += ((flux - subst_level) * norm_level);
                        else
                            total_flux += ((bke->Back-subst_level) * norm_level);
                    }
                    break;
                    case Core::PIXREJECT_MINMAX:
                    {
                        BackgroundMapEntry* bke = back[i]->backentry(x, y);
                        if (flux < reject_max || flux > reject_min)
                            total_flux += ((flux - subst_level) * norm_level);
                        else
                            total_flux += ((bke->Back-subst_level) * norm_level);
                    }
                    break;
                }
            }
            result[x + y * width] = total_flux / imsz;
        }
    }

    //Liberar las scanlines y los fondos
    for (int i =0; i<imsz;i++)
    {
        if (back[i]) delete back[i];
        if (scaners[i]) delete scaners[i];
        if (scanlines[i]) delete scanlines[i];
    }
    delete scanlines;
    if (averages) delete averages;
    if (exptimes) delete exptimes;

    return true;
}


void CalibrationManager::Imarith(float* a,float* b, Core::ImarithOp oper, float *output, int width, int height)
{
    int sz = width*height;
    for (int i=0; i<sz; i++)
    {
        float right_op = b[i];
        switch (oper)
        {
            case Core::IMARITH_ADD:
            output[i] = a[i] + right_op;
            break;
            case Core::IMARITH_SUBSTRACT:
            output[i] = a[i] - right_op;
            break;
            case Core::IMARITH_DIVIDE:
            output[i] = (right_op > 0.001) ? (a[i] / right_op) : 0;     //Cuidado con las divisiones por cero
            break;
        }
    }
}


void CalibrationManager::NormalizeMinMax(float* data,float level,int width, int height)
{
   int n = width * height;

   float min = data[0];
   float max = data[0];
   for (int i=1; i<n; i++)
   {
       if (data[i] < min) min = data[i];
       if (data[i] > max) max = data[i];
   }
   double interval = max - min;
   for (int i=0; i<n; i++)
   {
       float vdata = data[i];
       if (vdata <= min) data[i] = 0;
       else if (vdata >= max) data[i] = (float)level;
       else
       {
           double vres = ((vdata - min) / interval) * level;
           data[i] = (float)vres;
       }
   }
}

void CalibrationManager::NormalizeAverage(float* data, int level ,int width, int height, float reject_under, float reject_to)
{
   ImStat istat = ImStat::Compute(data, width, height);
   double reason = level / istat.mean();
   int n = istat.n();
   if (IS_INVALIDF(reject_under))
   {
       for (int i=0; i<n; i++)
           data[i] *= reason;
   }
   else
   {
       //Normalización de flats, a 1 se hace un pequeño truco
       for (int i=0; i<n; i++)
        if ((data[i] *= reason) < reject_under)
            data[i] = reject_to;
   }
}


CalibrationComputer::CalibrationComputer()
    : BackgroundWorker("Calibrating images")
{
    m_master_bias_data = NULL;
    m_master_flat_data = NULL;
    m_master_dark_data = NULL;
    m_substract = NULL;
    m_outp = NULL;
    m_hot_pixels = NULL;
    m_cold_pixels = NULL;
    m_nan_pixels = NULL;
}

void CalibrationComputer::ClearMemory()
{
    if (m_master_bias_data) delete m_master_bias_data;
    if (m_master_flat_data) delete m_master_flat_data;
    if (m_master_dark_data)
    {
        for (int i=0; i<m_master_dark_count;i++) delete m_master_dark_data[i];
        delete m_master_dark_data;
    }
    //Aunque sea un float** solamente es usado comoo buffer de float*
    if (m_substract) delete m_substract;
    if (m_outp) delete m_outp;
    if (m_hot_pixels)
    {
        for (int i=0; i<m_master_dark_count;i++) if (m_hot_pixels[i]) delete m_hot_pixels[i];
        delete m_hot_pixels;
    }
    if (m_cold_pixels) delete m_cold_pixels;
    if (m_nan_pixels) delete m_nan_pixels;
    //Por si se reusa la clase las ponemos a NULL
    m_master_bias_data = NULL;
    m_master_flat_data = NULL;
    m_master_dark_data = NULL;
    m_substract = NULL;
    m_outp = NULL;
    m_hot_pixels = NULL;
    m_cold_pixels = NULL;
    m_nan_pixels = NULL;
}

CalibrationComputer::~CalibrationComputer()
{
    ClearMemory();
}

bool CalibrationComputer::doWork(CalibrationParameters* par, CalibrationList* files)
{
    ClearMemory();
    OnSetup(100);
    OnStart();

    QString ret = calibrate(par, files);

    ClearMemory();
    OnEnd();
    return ret == "";
}

QString CalibrationComputer::calibrate(CalibrationParameters* par, CalibrationList* files)
{
   // QSettings stt("C:\\Users\\Juanjo\\Desktop\\SALIDA\\parameters.ini", QSettings::Format::IniFormat);
//    par->Save(&stt,"CALIBRATION");


    //*********************************************************************
    //Los tiempos se distribuyen de la siguiente manera
    //  5% - Generación de marter bias (num_bias/total_count)
    // 10% - Generación de master flat (num_bias/total_count)
    // 10% - Generación de master dark (num_dark/total_count)
    // 70% - Calibración de light frames (num_light/total_count)
    //*********************************************************************
    const int pr_bias = 5;
    const int pr_dark = 10;  const int pr_dark_start = pr_bias;
    const int pr_flat = 15;  const int pr_flat_start = pr_dark_start+pr_dark;
    const int pr_light = 70; const int pr_light_start = pr_flat_start+pr_flat;

    //Tamaño de las imágenes
    mWidth = files->header(0).width();
    mHeight = files->header(0).height();
    mBitpp = (par->bit_pp == 0) || (par->bit_pp == -1) ? files->header(0).bitpix() : par->bit_pp;

    //Inventario de archivos
    QStringList sub_light = files->GetAll(Core::FRAME_LIGHT);
    QStringList sub_bias = files->GetAll(Core::FRAME_BIAS);
    QStringList sub_flats = files->GetAll(Core::FRAME_FLAT);
    QStringList sub_darks = files->GetAll(Core::FRAME_DARK);

    m_master_bias_data = new float[mWidth * mHeight];
    m_master_flat_data = new float[mWidth * mHeight];
    QVector<int> exptimes = files->ExpTimes();
    QVector<bool> darkpresent;
    m_master_dark_data = new float*[exptimes.count()];
    m_hot_pixels = new BitMatrix*[exptimes.count()];
    m_master_dark_count = exptimes.count();
    for (int i=0;i<exptimes.count(); i++)
    {
        m_master_dark_data[i] = new float[mWidth * mHeight];
        m_hot_pixels[i] = new BitMatrix(0,0, mWidth, mHeight);
    }

    //Creamos el master bias
    OnMsg("Creating master bias");
    INFO("Creating master bias");
    if (sub_bias.count() == 1 && sub_bias.at(0).toUpper().contains("MASTER"))
    {
       FitImage* fi = FitImage::OpenFile(sub_bias[0]);
       memcpy(m_master_bias_data, fi->data(), mWidth*mHeight*sizeof(float));
       delete fi;
    }
    else
    {
        CalibrationManager::ImCombineAverage(sub_bias, m_master_bias_data, NULL, Core::PIXREJECT_NONE,
                                             par->p_bkg.bkg_method, par->p_bkg.bkg_mesh_size, par->p_bkg.bkg_smooth, 0, 0, 0, 0, 0);

        if (par->save_master_bias)
        {
            QString dstpath = QDir::cleanPath(par->output_dir + QDir::separator() + "master_bias.fit");
            FitImage::SaveToFile(m_master_bias_data, mWidth, mHeight , dstpath,  -32);
        }
    }

    OnProgress( pr_bias );
    if (cancelled()) return "Cancelled";

    //Creamos un master dark por cada tiempo de exposición
    OnMsg("Creating master darks");
    INFO("Creating master darks");
    QVector<bool> fakedark;
    for (int i=0; i<exptimes.count() && !cancelled(); i++)
    {
        OnProgress(MINVAL(99, pr_dark_start + floor(i * pr_dark / sub_flats.length())));
        QStringList dark_exp = files->GetFramesByExpTime(Core::FRAME_DARK, exptimes[i]);
        INFO("Creating dark frame / exposure: %d s", exptimes[i]);

        if (dark_exp.length() == 0)
        {
            //Copiamos la información del bias frame
            memcpy(m_master_dark_data[i], m_master_bias_data, mWidth*mHeight*sizeof(float));
            fakedark.append(true);
        }
        else
        {
            //Creamos un master dark con los archivos obtenidos
            CalibrationManager::ImCombineAverage(dark_exp, m_master_dark_data[i], NULL, Core::PIXREJECT_NONE, par->p_bkg.bkg_method,
                                                 par->p_bkg.bkg_mesh_size, par->p_bkg.bkg_smooth, 0, 0, 0, 0, 0);
            if (par->save_master_dark)
            {
                QString dstpath = QDir::cleanPath(par->output_dir + QDir::separator() + QString().sprintf("master_dark_%d.fit",exptimes[i]));
                FitImage::SaveToFile(m_master_dark_data[i], mWidth, mHeight , dstpath, -32);
            }
            //Buscamos hot pixels en la información de dark
            if (par->hot_pixel_sigma > 0)
                CalibrationManager::DetectHotAndColdPixels(m_master_dark_data[i],
                                                           par->hot_cold_connectivity,
                                                           m_hot_pixels[i],
                                                           par->hot_pixel_sigma,
                                                           0
                                                           );
            fakedark.append(false);
        }
    }
    if (cancelled()) return "Cancelled";

    //****************************************************************************************************
    //OPERACIONES DE FLAT FRAME
    //****************************************************************************************************
    OnMsg("Creating master flat");
    INFO("Creating master flats");
    //Para comenzar creamos una lista con las correcciones de dark o bias a añadir a cada flat
    m_substract = new float*[sub_flats.length()];
    for (int i=0; i<sub_flats.length() && !cancelled();i++)
    {
        OnProgress(MINVAL(99, pr_flat_start + floor(i * pr_flat / sub_flats.length())));
        FitHeaderList fhe = FitHeaderList::ReadFromFile(sub_flats.value(i));
        int idx = exptimes.indexOf(fhe.exptime(),0);
        if (idx < 0) m_substract[i] = m_master_bias_data;
        else m_substract[i] = m_master_dark_data[idx];
    }
    if (cancelled()) return "Cancelled";

    //Ahora creamos realmente el master flat
    //Creamos un master flat
    //----------------------------------------------------------------------------------------------------

    if (sub_flats.count() == 1 && sub_flats.at(0).toUpper().contains("MASTER"))
    {
       FitImage* fi = FitImage::OpenFile(sub_flats[0]);
       memcpy(m_master_flat_data, fi->data(), mWidth*mHeight*sizeof(float));
       delete fi;
    }
    else
    {
        CalibrationManager::ImCombineAverage(
                    sub_flats, m_master_flat_data,
                    m_substract,
                    par->reject_algorithm, par->p_bkg.bkg_method,
                    par->p_bkg.bkg_mesh_size, par->p_bkg.bkg_smooth,
                    par->reject_sigma_high, par->reject_sigma_low, par->reject_min, par->reject_max, 1);
        CalibrationManager::AvoidNegativeValues(m_master_flat_data, mWidth, mHeight);

        if (par->save_master_flat)
        {
            int norm_level = 0;
            //Cuidado, quizá antes de guardar hay que normalizar el master flat
            if (par->bit_pp == 8)
                norm_level = 128;
            else if ((par->bit_pp == 16) || (par->bit_pp == -1 && mBitpp == 16))
                norm_level = 32768;        //Una buena resolución a mitad de camino
            else if (par->bit_pp == -32)
                norm_level = 10000;

            QString dstpath = QDir::cleanPath(par->output_dir + QDir::separator() + "master_flat.fit");
            FitImage::SaveToFile(m_master_flat_data, mWidth, mHeight , dstpath, -32);
        }
    }
    if (cancelled()) return "Cancelled";


    //Buscamos dead pixels
    if (par->dead_pixel_sigma > 0)
    {
        m_cold_pixels = new BitMatrix(0, 0, mWidth, mHeight);
        CalibrationManager::DetectHotAndColdPixels(m_master_flat_data,
                                                   par->hot_cold_connectivity,
                                                   m_cold_pixels,
                                                   0,
                                                   par->dead_pixel_sigma
                                                   );
    }
    if (cancelled()) return "Cancelled";

    //****************************************************************************************************
    //OPERACIONES FINALES DE CALIBRACIÓN, calibramos uno por uno los frames de la imagen
    //****************************************************************************************************
    m_outp = new float[mWidth * mHeight];
    for (int i=0; i<sub_light.length() && !cancelled(); i++)
    {
        XGlobal::APP_SETTINGS.ct_fcal++;
        OnMsg(QString("Calibrating light frame: <b>%1</b>").arg(QFileInfo(sub_light.value(i)).fileName()));
        OnProgress(MINVAL(99, pr_light_start + floor(i * pr_light / sub_light.length())));
        INFO("Calibrating light frame: %s", PTQ(sub_light.value(i)));
        //prog_light->SetPosition(i);
        //prog_dark->SetStatus(QString("File: '%1'").arg(sub_light.value(i)));

        QString srcfile = sub_light.value(i);
        QString dstpath = QDir::cleanPath(par->output_dir + QDir::separator() + QFileInfo(srcfile).baseName() + ".fit");
        FitImage* fi = FitImage::OpenFile(srcfile);
        if (dstpath != sub_light.value(i)) INFO("\tOutput to: %s", PTQ(dstpath));

        //Escoger el pedestal a sustraer (dark o bias)
        float *substract = NULL;
        int exptime = fi->exptime();
        if (exptimes.contains(exptime) && sub_darks.count())
            substract = m_master_dark_data[exptimes.indexOf(exptime)];
        else
            substract = m_master_bias_data;

        //Realizar la correción de dark (o bias)
        if (substract)
            CalibrationManager::Imarith(fi->data(), substract, Core::IMARITH_SUBSTRACT, m_outp, mWidth, mHeight);
        else
            memcpy(m_outp, fi->data(), sizeof(float)*mWidth*mHeight);

        CalibrationManager::AvoidNegativeValues(m_outp, mWidth, mHeight);

        //Realizar la corrección de flat
        CalibrationManager::Imarith(m_outp, m_master_flat_data, Core::IMARITH_DIVIDE, m_outp, mWidth, mHeight );

        //Normalizamos
        if (par->norm_level)
            CalibrationManager::NormalizeAverage(m_outp, par->norm_level, mWidth, mHeight);

        // CalibrationManager::AvoidNegativeValues(m_outp, mWidth, mHeight);
        //Por último guardamos el archivo
        if ((par->cosmic_ray_sigma > 0 || par->cold_pixel_sigma > 0))
        {
            //Si no hemos usado dark frames reales no realizamos corrección de hot y cold pixels
            int idx_dark = exptimes.indexOf(exptime);
            if (idx_dark != -1 && !fakedark.at(idx_dark))
                CalibrationManager::RemoveHotAndColdPixels(m_outp, par->hot_cold_connectivity, m_hot_pixels[exptimes.indexOf(exptime)]);
            CalibrationManager::RemoveCosmicRaysImpacts(m_outp, par->hot_cold_connectivity,
                                                        par->cosmic_ray_sigma, par->cold_pixel_sigma, mWidth, mHeight);
        }

        //Buscar valores negativos
        CalibrationManager::RestoreSourceValues(fi->data(), m_outp, mWidth, mHeight);

        //CalibrationManager::AvoidNegativeValues(m_outp, mWidth, mHeight);
        FitImage::SaveToFile(m_outp, fi->headers(), dstpath, mBitpp);

        delete fi;
    }
    if (cancelled()) return "Cancelled";

    OnEnd();
    return "";
}






