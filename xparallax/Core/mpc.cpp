#include "mpc.h"
#include "util.h"
#include "xstringbuilder.h"
#include <QRegularExpression>
#include <QCryptographicHash>
#include <QThread>
#include <QDir>

#define MPC_MAX_KNOWN_DESIGNATION "h9830"
#define MPC_REC_SZ                (202+1)
#define MPC_REC_COMET_SZ          (172+1)
#define MPC_MAX_CACHE_RECORDS      1024
//Expresiones regulares para parsear registros MPC
#define MPC_RECORD_QUERY    "([\s\(\)\d]){8}\s"
#define MPC_CHECKER_URL     "http://scully.cfa.harvard.edu/cgi-bin/mpcheck.cgi"
#define MPC_DESIG_PARS      "^\\s*\\((?<ID>\\d+)\\)(?<NAME>.*)$"
#define MPC_REPORT_HEADER   "^(?<HDR>COD|CON|OBS|MEA|TEL|NET|BND|COM|NUM|ACK|AC2)\\s(?<BODY>.*)$"
#define MPC_REPORT_END      "----- end -----"
//Consultar formato en: http://www.minorplanetcenter.net/iau/info/OpticalObs.html
#define MPC_REPORT_OBS_MP       "(?<MP_NUMBER>[A-Za-z\\d\\s][\\d\\s]{4})" \
                                "(?<MP_DESIG>.{7})" \
                                "(?<MP_DISCOVERY_ASTERISK>\\*|\\s)"
#define MPC_REPORT_OBS_COMET    "(?<CO_NUMBER>[\\d\\s]{4})" \
                                "(?<CO_ORBIT>[CPDXA ])" \
                                "(?<CO_DESIG>.{7})" \
                                "(?<CO_BLANK>\\s)"
#define MPC_REPORT_OBS_NATSAT   "(?<NS_PLANET>[JSUN\\s])" \
                                "(?<NS_NUMBER>[\\d\\s]{3})" \
                                "(?<NS_S>S)" \
                                "(?<NS_DESIG>.{7})" \
                                "(?<NS_BLANK>\\s)"
#define MPC_REPORT_OBS          "^" \
                                "((" MPC_REPORT_OBS_MP ")|(" MPC_REPORT_OBS_COMET  ")|(" MPC_REPORT_OBS_NATSAT "))" \
                                "(?<NOTE1>.)" \
                                "(?<NOTE2>.)" \
                                "(?<DATE_OBS>\\d{4}\\s\\d{2}\\s\\d{2}\\.((\\d{5}\\s)|(\\d{6})))" \
                                "(?<RA>.{12})" \
                                "(?<DEC>.{12})" \
                                "(?<BLANK1>\\s{9})" \
                                "(?<MAG>[\\d\\s\\.]{5})" \
                                "(?<BAND>.)" \
                                "(?<BLANK2>\\s{6})" \
                                "(?<OBSCODE>[A-Z0-9]{3})" \
                                ".*$"
#define MPC_REPORT_OBS_START    "^"
//Expresiones regulares de geolocalización del observatorio
#define REGEX_GEO_LONG          "Long\\.?\\s+(?<LONG>\\d{1,2}\\s+\\d{1,2}\\s+\\d{1,2}(\\.\\d{1,2})?\\s*(W|E))"
#define REGEX_GEO_LAT           "Lat\\.?\\s+(?<LAT>\\d{1,2}\\s+\\d{1,2}\\s+\\d{1,2}(\\.\\d{1,2})?\\s*(N|S))"
#define REGEX_GEO_ALT           "Alt\\.?\\s+(?<ALT>\\d+)m?"
#define REGEX_GEO_METHOD        "(?<METHOD>[^\\s][A-Z\\s]+[^\\s])\\s*"
#define REGEX_GEO_COORDINATES   "^" REGEX_GEO_LONG "\\s*,\\s*" REGEX_GEO_LAT "\\s*,\\s*" REGEX_GEO_ALT "\\s*,\\s*" REGEX_GEO_METHOD
//Expresiones regulares de telescopio
#define REGEX_TEL_APERTURE      "((?<APERTURE>\\d+(\\.\\d+)?)\\-m)"
#define REGEX_TEL_FOCALRATIO    "(f/(?<FOCALRATIO>\\d+(\\.\\d+)?))"
#define REGEX_TEL_TELTYPE       "((?<TELTYPE>[A-Z\\s\\-]+))"
#define REGEX_TEL_CCD           "\\+\\s+(?<CCD>[^\\+/]+)"
#define REGEX_TEL_REDUCER       "\\+\\s+f/(?<REDUCER>\\d+(\\.\\d+)?)\\s+focal reducer"
#define REGEX_TELESCOPEINFO     "^" \
                                REGEX_TEL_APERTURE \
                                "(\\s+" REGEX_TEL_FOCALRATIO ")?" \
                                "(\\s+" REGEX_TEL_TELTYPE ")" \
                                "(\\s+" REGEX_TEL_CCD ")?" \
                                "(\\s+" REGEX_TEL_REDUCER ")?" \
                                "\\s*$"

//Caracteres especiales de latex
const char*  MPCReportHdr::TEX_CHARS[] = {
  "á", "\\'a", "é", "\\'e", "í", "\\'i", "ó", "\\'o", "ú", "\\'u",
  "Á", "\\'A", "É", "\\'E", "Í", "\\'I", "Ó", "\\'O", "Ú", "\\'U",
  "ñ", "\\~n", "Ñ", "\\~N",
  "à", "\\`a", "è", "\\`e", "ì", "\\`i", "ò", "\\`o", "ù", "\\`u",
  "À", "\\`A", "È", "\\`E", "Ì", "\\`I", "Ò", "\\`O", "Ù", "\\`U",
  "ä", "\"a", "ë", "\"e", "ï", "\"i", "ö", "\"o", "ü", "\"u",
  "Ä", "\"A", "Ë", "\"E", "Ï", "\"I", "Ö", "\"O", "Ü", "\"U",
  "ç", "\\c{c}", "Ç", "\\c{C}",
  "ř", "\\v{r}",
  "æ", "{\\ae}", "Æ", "{\\AE}",
  "ø", "{\\ø}", "Ø", "{\\Ø}",
  "å", "{\\aa}", "Å", "{\\AA}",
  NULL
};

QString                  MpcOrb::mMpcMaxKnownDesignation = MPC_MAX_KNOWN_DESIGNATION;
QMutex                   MpcOrb::mCacheMutex;
QHash<QString,QString>   MpcOrb::mObjCache;
QMutex                   MpcObservatory::mCacheMutex;
QHash<QString,QString>   MpcObservatory::mObjCache;

#define     NUM2PACK5(n)   ((char)(n>=10&&n<=35?n-10+'A':n-36+'a'))
#define     PACK52NUM(p)   ((int)(p>='A'&&p<='Z'?p-'A'+10:p-'a'+36))
#define     MAXPACK5       619999 // = z9999


MpcObservatory MpcObservatory::Parse(const QString& cad)
{
    bool ok;
    MpcObservatory obs;
    obs.mCode = cad.left(3);
    obs.mLong = cad.mid(4,9).toDouble(&ok);
    if (!ok) return obs;
    if (obs.mLong > 90) obs.mLong = obs.mLong - 360.0;
    obs.mLat = acos(cad.mid(13,8).toDouble(&ok))*RAD2DEG;
    if (!ok) return obs;
    obs.mName = cad.mid(30).trimmed();
    obs.mValid = true;
    return obs;
}

MpcObservatory MpcObservatory::find(const QString& id)
{
    MpcObservatory obs;
    //Primero lo buscamos en la caché
    mCacheMutex.lock();
    if (mObjCache.contains(id))
        obs = Parse(mObjCache[id]);
    mCacheMutex.unlock();
    if (obs.isValid()) return obs;

    //Luego buscamos en el archivo
    char buffer[256];
    QString fname = XGlobal::GetSettingsPath("ObsCodes.html");
    QFile fil(fname);
    if (!fil.exists() || !fil.open(QFile::ReadOnly)) return obs;
    QByteArray buff_id = id.toLatin1();
    while (!fil.atEnd() && !obs.isValid())
    {
        int numread = fil.readLine(buffer, 256);
        if (numread <= 0) break;
        if (strstr(buffer, buff_id.data()) == buffer)
            obs = Parse(buffer);
    }
    if (obs.isValid())
    {
        mCacheMutex.lock();
        mObjCache[id] = buffer;
        mCacheMutex.unlock();
    }
    fil.close();
    return obs;
}

QString MpcObservatory::toString()
{
    return QString("").sprintf("%s LON:%s LAT:%s", PTQ(mName), PTQ(Util::LON2tring(mLong)), PTQ(Util::LAT2String(mLat)));
}


MpcRecord::MpcRecord()
{
    mInvalid = true;
}

MpcRecord:: ~MpcRecord()
{

}


MpcRecord MpcRecord::Parse(const QString &/*cad*/)
{
    MpcRecord rec;

    rec.mInvalid = false;
    return rec;
}

MpcUtils::MpcUtils()
{
    setName("MPC information reader");
}

MpcUtils::~MpcUtils()
{

}

QDateTime MpcUtils::ParseMPCDate(const QString& dtmpc)
{
    bool ok = false;
    QStringList sl = dtmpc.trimmed().split(' ');
    if (sl.at(0).length() != 4 || sl.at(1).length() != 2 || sl.at(2).length() < 8) return INVALID_DATETIME;
    int year = sl.at(0).toInt(&ok);
    if (!ok || year < 1000) return INVALID_DATETIME;
    int month = sl.at(1).toInt(&ok);
    if (!ok || month < 1 || month > 12) return INVALID_DATETIME;
    double dayf = sl.at(2).toDouble(&ok);
    if (!ok || dayf > 31 || dayf < 0) return INVALID_DATETIME;
    int day = (int) floor(dayf);
    double hourf = (dayf-day)*24;
    int hours = (int) floor(hourf);
    double minf = (hourf-hours)*60;
    int min = (int) floor(minf);
    double secf = (minf-min)*60;
    int sec = (int) floor(secf);
    double millisf = (secf-sec)*1000;
    int millis = (int) floor(millisf);

    return QDateTime(QDate(year, month, day), QTime(hours, min, sec, millis), Qt::UTC);
}

QString  MpcUtils::Date2MPCDate(const QDateTime& dt)
{
    int year = dt.date().year();
    int month = dt.date().month();
    int day = dt.date().day();
    double hours = (dt.time().hour()
                    + dt.time().minute() / 60.0
                    + dt.time().second() / 3600.0
                    + dt.time().msec() / 3600000.0) / 24.0;
    return QString("").sprintf("%04d %02d %08.5f", year, month, day + hours);
}


QDateTime MpcUtils::GetDateFrame(FitHeaderList* hl)
{
    //Obtener tiempo de exposición
    double et = hl->exptime();
    FitHeader sel_hdr;
    QDateTime dt  = INVALID_DATETIME;
    QRegularExpressionMatch mc;
    bool ok;
    //CABECERA DATE-OBS
    sel_hdr = hl->Contains("DATE-OBS");
    if (sel_hdr.isInvalid()) sel_hdr = hl->Contains("DATE_OBS");
    if (sel_hdr.isValid())
    {
        //Nos aseguramos de que la precisión es al segundo
        mc = QRegularExpression("(\\d{4}\\-\\d{2}\\-\\d{2}(\\s|T)?\\d{2}:\\d{2}:\\d{2}(\\.\\d{1,3})?)")
                .match(sel_hdr.value());
        if (mc.hasMatch()) dt = Util::ParseDate(sel_hdr.value());
        else sel_hdr = FitHeader();
        if (dt == INVALID_DATETIME) sel_hdr = FitHeader();
    }
    //Mezcla de DATE-OBS + UT
    if (sel_hdr.isInvalid() || dt == INVALID_DATETIME)
    {
        sel_hdr = hl->Contains("UT");
        FitHeader dt_obs = hl->Contains("DATE-OBS");
        if (sel_hdr.isValid() && dt_obs.isValid())
        {
            QString text = dt_obs.value() + "T" + sel_hdr.value();
            mc = QRegularExpression("(\\d{4}\\-\\d{2}\\-\\d{2}(\\s|T)?\\d{2}:\\d{2}:\\d{2}(\\.\\d{1,3})?)")
                    .match(text);
            if (mc.hasMatch()) dt = Util::ParseDate(text);
            else sel_hdr = FitHeader();
            if (dt == INVALID_DATETIME) sel_hdr = FitHeader();
        }
    }
    //Mezcla de DATE + UT
    if (sel_hdr.isInvalid() || dt == INVALID_DATETIME)
    {
        sel_hdr = hl->Contains("UT");
        FitHeader dt_obs = hl->Contains("DATE");
        if (sel_hdr.isValid() && dt_obs.isValid())
        {
            QString text = dt_obs.value() + "T" + sel_hdr.value();
            mc = QRegularExpression("(\\d{4}\\-\\d{2}\\-\\d{2}(\\s|T)?\\d{2}:\\d{2}:\\d{2}(\\.\\d{1,3})?)")
                    .match(text);
            if (mc.hasMatch()) dt = Util::ParseDate(text);
            else sel_hdr = FitHeader();
            if (dt == INVALID_DATETIME) sel_hdr = FitHeader();
        }
    }
    //CABECERA JD
    if (sel_hdr.isInvalid() || dt == INVALID_DATETIME)
    {
        sel_hdr = hl->Contains("JD");
        int idx = sel_hdr.value().indexOf(".");
        if (idx > 0 && sel_hdr.value().length() - idx > 4)
        {
           dt = Util::JD2DateTime(sel_hdr.value().toDouble(&ok));
           if (!ok)
           {
               dt = INVALID_DATETIME;
               sel_hdr = FitHeader();
           }
        }
    }
    //Comprobar inicio, fin ó mid exptime
    if (sel_hdr.isValid())
    {
        if (sel_hdr.comment().contains("strart", Qt::CaseInsensitive))
            dt = dt.addSecs(et/2);
        else if (sel_hdr.comment().contains("end", Qt::CaseInsensitive))
            dt = dt.addSecs(-et/2);
        /*
        else if (sel_hdr.name() == "DATE-OBS" || sel_hdr.name() == "DATE_OBS")
            dt = dt.addSecs(et/2);
            */
    } 
    dt.setTimeSpec(Qt::UTC);
    return dt;
}

MPCReportHdr::MPCReportHdr(bool valid)
{
    mValid = valid;
    mDirty = false;
}

MPCReportHdr::MPCReportHdr(const QString hdr, const QString val,bool valid) : MPCReportHdr(valid)
{
    mHdr = hdr;
    mText = val;
}

QString MPCReportHdr::toHtml()
{
    XStringBuilder xb;
    QString te = texEncode(mText);
    if (te.length() > 76) te = te.left(76);
    int sz1 = 4 + te.length();
    xb <<  "<span class='obs_hdr'><span class='hdr'>" +
            mHdr << "</span> <span class='text'>" << te << "</span></span>";
    while (sz1 < 80)
    {
        xb << " ";
        sz1++;
    }
    return xb.toString();
}

QString MPCReportHdr::toString()
{
    XStringBuilder xb;
    xb << mHdr << " " << texEncode(mText);
    xb.rtrim();
    return xb.toString();
}

MPCReportHdr MPCReportHdr::Parse(const QString& hdr)
{
    QRegularExpression rgex(MPC_REPORT_HEADER);
    QRegularExpressionMatch mc = rgex.match(hdr);
    if (!mc.hasMatch()) return MPCReportHdr(false);
    MPCReportHdr rhdr(true);
    rhdr.mHdr = mc.captured("HDR");
    rhdr.mText = texDecode(mc.captured("BODY"));
    return rhdr;
}

QString MPCReportHdr::texEncode(const QString& cad)
{
    QString resul = cad;
    for (int i=0; TEX_CHARS[i]; i+=2)
    {
        if (resul.contains(TEX_CHARS[i]))
            resul = resul.replace(TEX_CHARS[i], TEX_CHARS[i+1]);
    }
    return resul;
}

QString MPCReportHdr::texDecode(const QString& cad)
{
    QString resul = cad;
    for (int i=0; TEX_CHARS[i]; i+=2)
    {
        if (resul.contains(TEX_CHARS[i+1]))
            resul = resul.replace(TEX_CHARS[i+1], TEX_CHARS[i]);
    }
    return resul;
}

MPCReportMeasure::MPCReportMeasure()
    :MPCReportMeasure(MPCReportMeasure::INVALID)
{
}

MPCReportMeasure::MPCReportMeasure(MeasureType mt)
{
    mType = mt;
    mDirty = false;
    mNote2 = 'C';
    mNote1 = ' ';
    mDiscoveryAsterisk = false;
    mNumber = 0;
}

MPCReportMeasure MPCReportMeasure::Parse(const QString& obs)
{
    QRegularExpression regex(MPC_REPORT_OBS);
    QRegularExpressionMatch mc = regex.match(obs);
    bool ok = false;
    if (mc.hasMatch())
    {
        MPCReportMeasure mea(MPCReportMeasure::INVALID);
        if (mc.captured("MP_NUMBER").trimmed().length() || mc.captured("MP_DESIG").trimmed().length())
        {
            mea.mType = MPCReportMeasure::MINOR_PLANET;
            //Número
            QString sid = mc.captured("MP_NUMBER").trimmed();
            if (sid.length())
            {
                mea.mNumber = MpcUtils::unpack5Number(mc.captured("MP_NUMBER").trimmed());
                if (!mea.mNumber) return MPCReportMeasure(MPCReportMeasure::INVALID);
            }
            else mea.mNumber = 0;
            //Designación temporal
            mea.mDesignation = mc.captured("MP_DESIG").trimmed();
            //Discovery asterisk
            mea.mDiscoveryAsterisk = mc.captured("MP_DISCOVERY_ASTERISK").trimmed() == "*";
        }
        else if (mc.captured("CO_NUMBER").trimmed().length() || mc.captured("CO_DESIG").trimmed().length())
        {
            mea.mType = MPCReportMeasure::COMET;
            QString desig = mc.captured("CO_DESIG").trimmed();
            QString number = mc.captured("CO_NUMBER").trimmed();
            if (desig.length())
            {
                mea.mNumber = 0;
                mea.mDesignation = desig;
            }
            else
            {
                mea.mNumber = number.toInt();
                mea.mDesignation = "";
            }
            mea.mCMOrbitType = mc.captured("CO_ORBIT").at(0).toLatin1();
        }
        else if (mc.captured("NS_PLANET").length())
        {
            mea.mType = MPCReportMeasure::NATURAL_SATELLITE;
            QString number = mc.captured("NS_NUMBER").trimmed();
            QString desig = mc.captured("NS_DESIG").trimmed();
            if (desig.length())
            {
                mea.mNumber = 0;
                mea.mDesignation = desig;
            }
            else
            {
                mea.mNumber = number.toInt();
                mea.mDesignation = "";
                mea.mPlanetID = mc.captured("NS_PLANET").at(0).toLatin1();
            }
        }
        //Comunes para todos
        mea.mNote1 = mc.captured("NOTE1").at(0).toLatin1();
        mea.mNote2 = mc.captured("NOTE2").at(0).toLatin1();
        mea.mDateObs = MpcUtils::ParseMPCDate(mc.captured("DATE_OBS"));
        if (mea.mDateObs.isNull() || !mea.mDateObs.isValid()) return MPCReportMeasure(MPCReportMeasure::INVALID);
        mea.mRa = Util::ParseRA(mc.captured("RA"));
        if (IS_INVALIDF(mea.mRa)) return MPCReportMeasure(MPCReportMeasure::INVALID);
        mea.mDec = Util::ParseDEC(mc.captured("DEC"));
        if (IS_INVALIDF(mea.mDec)) return MPCReportMeasure(MPCReportMeasure::INVALID);

        QString mag = mc.captured("MAG").trimmed();
        QString band = mc.captured("BAND").trimmed();
        if ((!mag.length() && band.length()) || (mag.length() && !band.length())) return MPCReportMeasure(MPCReportMeasure::INVALID);
        if (mag.length())
        {
            mea.mMag = mag.toFloat(&ok);
            if (!ok || mea.mMag < 0) return MPCReportMeasure(MPCReportMeasure::INVALID);
            mea.mBand = band.at(0).toLatin1();
        }
        else
        {
            mea.mMag = 0;
            mea.mBand = ' ';
        }
        mea.mObsCode = mc.captured("OBSCODE");
        return mea;
    }
    return MPCReportMeasure(MPCReportMeasure::INVALID);
}

MPCReportMeasure::MeasureType MPCReportMeasure::IdMeasure(const QString& hdr,int* number)
{
    if (number) *number = 0;
    bool ok;
    //Satélites naturales J013S, SJ99U030
    if (hdr.length() == 5 && hdr.endsWith("S") &&
        hdr.mid(1,3).toInt(&ok) > 0 && ok &&
            (hdr.at(0) == 'J' ||  hdr.at(0) == 'S' ||hdr.at(0) == 'U' ||hdr.at(0) == 'N'))
    {
        if (number) *number =  hdr.mid(1,3).toInt();
        return MPCReportMeasure::MINOR_PLANET;
    }
    if (hdr.length() == 8 && hdr.startsWith("S"))
        return MPCReportMeasure::MINOR_PLANET;
    //Cometas 0040P, CK15G020
    if (hdr.length() <= 5 && hdr.endsWith("P"))
    {
        if (number) *number =  hdr.left(hdr.length()-1).toInt();
        return MPCReportMeasure::COMET;
    }
    if (hdr.length() == 8 && hdr.endsWith("0") &&
            (hdr.at(0) == 'C' || hdr.at(0) == 'P' || hdr.at(0) == 'D' || hdr.at(0) == 'X' || hdr.at(0) == 'A'))
       return MPCReportMeasure::COMET;
    //Minor planets 122, h2211, SDESIG1
    if (hdr.length() <= 5 && MpcUtils::unpack5Number(hdr) > 0)
    {
        if (number) *number = MpcUtils::unpack5Number(hdr);
        return MPCReportMeasure::MINOR_PLANET;
    }
    if (hdr.length() == 7) return MPCReportMeasure::MINOR_PLANET;
    return MPCReportMeasure::INVALID;
}

QString MPCReportMeasure::toString()
{
   QString rest, hdr;
   switch(mType)
   {
       case MINOR_PLANET:
       if (mNumber)
       hdr = QString("").sprintf(
             "%s       %c",
             MpcUtils::pack5Number(mNumber).toLatin1().data(), mDiscoveryAsterisk?'*':' ');
       else
           hdr = QString("").sprintf(
                 "     %-7s%c",
                 mDesignation.toLatin1().data(), mDiscoveryAsterisk?'*':' ');
       break;
       case COMET:
       if (mNumber) hdr = QString("").sprintf("%04d%c        ",mNumber, mCMOrbitType);
       else hdr = QString("").sprintf("    %c%-07s ", mCMOrbitType, mDesignation.toLatin1().data());
       break;
       case NATURAL_SATELLITE:
       if (mNumber) hdr = QString("").sprintf("%c%03dS        ",mPlanetID, mNumber);
       else hdr = QString("").sprintf("     S%-07s",mDesignation.toLatin1().data());
       break;
       default: return QString("INVALID"); break;
   }
   rest = QString("").sprintf(
   "%c"         //Note 1
   "%c"         //Note 2
   "%-17s"      //Date
   "%-12s"      //RA
   "%-12s"      //DEC
   "         "
   "%s"         //Mag
   "%c"         //Band
   "      "
   "%s"         //ObsCode
   ,
   mNote1, mNote2, MpcUtils::Date2MPCDate(mDateObs).toLatin1().data(),
   Util::RA2String(mRa).toLatin1().data(), Util::DEC2String(mDec).toLatin1().data(),
   (mMag > 0.1 ? QString("").sprintf("%04.1f ", mMag) : QString("     ")).toLatin1().data(), mBand,
   mObsCode.toLatin1().data());
   QString ret = hdr + rest;
   if (ret.length() > 80)
       return ret.left(80);
   return ret;
}

QString MPCReportMeasure::toHtml()
{
    QString rest, hdr;
    switch(mType)
    {
        case MINOR_PLANET:
        if (!isMPTempDesig())
        hdr = QString("").sprintf(
              "<span class='number'>%s</span><span class='designation'>       </span>%c",
              MpcUtils::pack5Number(mNumber).toLatin1().data(),
              mDiscoveryAsterisk?'*':' ');
        else
            hdr = QString("").sprintf(
                  "<span class='number'>     </span><span class='designation'>%-7s</span>%c",
                  mDesignation.left(7).toLatin1().data(),
                  mDiscoveryAsterisk?'*':' ');
        break;
        case COMET:
        if (mNumber)
        hdr = QString("").sprintf(
        "<span class='number'>%04d</span><span class='orbit'>%c</span><span class='designation'>       </span> ",
            mNumber, mCMOrbitType);
        else
            hdr = QString("").sprintf(
            "<span class='number'>    </span><span class='orbit'>%c</span><span class='designation'>%-07s</span> ",
            mCMOrbitType, mDesignation.toLatin1().data());
        break;
        case NATURAL_SATELLITE:
        if (mNumber)
            hdr = QString("").sprintf(
            "<span class='planet'>%c</span><span class='number'>%03d</span><span class='orbit'>S</span><span class='designation'>       </span> ",
             mPlanetID,mNumber);
        else
            hdr = QString("").sprintf(
            " <span class='number'>   </span><span class='orbit'>S</span><span class='designation'>%-7s</span> ",
             mDesignation.toLatin1().data());
        break;
        default: return QString("INVALID"); break;
    }
    rest = QString("").sprintf(
    "<span class='note'>%c</span>"         //Note 1
    "<span class='note'>%c</span>"         //Note 2
    "<span class='date'>%-17s</span>"      //Date
    "<span class='ra'>%-12s</span>"        //RA
    "<span class='dec'>%-12s</span>"       //DEC
    "         "                            //----------------------
    "<span class='mag'>%s</span>"          //Mag
    "<span class='band'>%c</span>"         //Band
    "      "                               //----------------------
    "<span class='ocode'>%s</span>"        //ObsCode
    ,
    mNote1, mNote2, MpcUtils::Date2MPCDate(mDateObs).toLatin1().data(),
    Util::RA2String(mRa).toLatin1().data(), Util::DEC2String(mDec).toLatin1().data(),
    (mMag > 0.1 ? QString("").sprintf("%04.1f ", mMag) : QString("     ")).toLatin1().data(), mBand,
    mObsCode.toLatin1().data());
    return hdr + rest;
}

QString MPCReportMeasure::id()
{
    if (mType == MINOR_PLANET)
        return mNumber?MpcUtils::pack5Number(mNumber):mDesignation;
    else if (mType == COMET)
        return mNumber?QString("").sprintf("%04d%c", mNumber, mCMOrbitType):
                       QString("").sprintf("%c%07s", mCMOrbitType, mDesignation.toLatin1().data());
    else
    {
        return mNumber?QString("").sprintf("%c%03dS",mPlanetID, mNumber):
                       QString("").sprintf("S%07s", mDesignation.toLatin1().data());

    }
}

MPCReport::MPCReport()
{

}

MPCReport::~MPCReport()
{

}

QString MPCReport::toString(const char* newline)
{
    XStringBuilder xb;
    for (int i=0; i<mHdrs.count(); i++)
        xb << mHdrs.data()[i].toString() << newline;
    for (int i=0; i<mMeasures.count(); i++)
        xb << mMeasures.data()[i].toString() << newline;
    xb << MPC_REPORT_END << "\n";;
    return xb.toString();
}

QString MPCReport::toHtml()
{
    XStringBuilder xb;
    xb <<  "<html>"
           "<head><link rel='stylesheet' type='text/css' href='style.css'>"
           "<style type='text/css'> .over{text-decoration:underline;}</style>"
           "<style type='text/css'> .hdr{color:#000080; font-weight:bold;}</style>"
           "<style type='text/css'> .number{color:#800000; }</style>"
           "<style type='text/css'> .note{color:#BF8F00;}</style>"
           "<style type='text/css'> .date{color:black; }</style>"
           "<style type='text/css'> .ra{color:#000080;}</style>"
           "<style type='text/css'> .dec{color:#008000; }</style>"
           "<style type='text/css'> .end{color:#aaa; }</style>"
           "<style type='text/css'> .orbit{color:#FF6600; }</style>"
           "<style type='text/css'> .planet{color:#990066; }</style>"
           "</head>"
           "<body><pre>";
    for (int i=0; i<mHdrs.count(); i++)
        xb << mHdrs.data()[i].toHtml() << "\r\n";
    for (int i=0; i<mMeasures.count(); i++)
        xb << mMeasures.data()[i].toHtml() << "\r\n";
    xb << "<span class='end'>" MPC_REPORT_END "</span>"
       << "\n\n"
       << "</pre></body></html>";
    return xb.toString();
}

MPCReport* MPCReport::LoadFromFile(const QString &filename)
{
    MPCReport*  report = new MPCReport();
    report->mFilename = filename;
    QFile fil(filename);
    if (!fil.open(QFile::ReadOnly))
    {
        report->mError = QString("Unable to open file '%1'").arg(filename);
        return report;
    }
    int line = 1;
    QTextStream ts(&fil);
    while (!ts.atEnd())
    {
        QString cad = ts.readLine();
        if (cad == MPC_REPORT_END) break;
        if (cad.length() == 0) {line++; continue;}
        MPCReportHdr hdr = MPCReportHdr::Parse(cad);
        if (hdr.isValid())
        {
            report->mHdrs.append(hdr);
        }
        else
        {
            MPCReportMeasure mea = MPCReportMeasure::Parse(cad);
            if (mea.isValid()) report->mMeasures.append(mea);
            else WARN("Invalid line [%d] '%s' in MPC report file", line, PTQ(cad));
        }
        line++;
    }
    fil.close();
    report->sortHdrs();
    return report;
}

MPCReport* MPCReport::LoadFromString(const QString& str)
{
    int line = 1;
    QStringList list = str.split("\n", QString::SkipEmptyParts);
    MPCReport*  report = new MPCReport();
    for (int i=0; i<list.count();i++)
    {
        QString cad = list[i];
        if (cad == MPC_REPORT_END) break;
        MPCReportHdr hdr = MPCReportHdr::Parse(cad);
        if (hdr.isValid())
        {
            report->mHdrs.append(hdr);
        }
        else
        {
            MPCReportMeasure mea = MPCReportMeasure::Parse(cad);
            if (mea.isValid()) report->mMeasures.append(mea);
            else WARN("Invalid line [%d] '%s' in MPC report string", line, PTQ(cad));
        }
        line++;
    }
    report->sortHdrs();
    return report;
}

bool MPCReport::dirty()
{
    for (int i=0; i<mHdrs.count(); i++)
        if (mHdrs.data()[i].dirty()) return true;
    for (int i=0; i<mMeasures.count(); i++)
        if (mMeasures.data()[i].dirty()) return true;
    return false;
}

void MPCReport::setDirty(bool dirty)
{
    for (int i=0; i<mHdrs.count(); i++)
        mHdrs.data()[i].setDirty(dirty);
    for (int i=0; i<mMeasures.count(); i++)
        mMeasures.data()[i].setDirty(dirty);
}

QString MPCReport::md5()
{
    QStringList lst;
    for (int i=0; i<mMeasures.count(); i++)
       lst.append(mMeasures.data()[i].toString());
    lst.sort(Qt::CaseInsensitive);
    XStringBuilder xb;
    for (int i=0; i<lst.count(); i++)
        xb << lst.at(i);
    return QString(QCryptographicHash::hash(xb.toString().toLatin1(),QCryptographicHash::Md5).toHex());
}

void MPCReport::sortHdrs()
{
    //Son muy pocas, con usar una ordenación de la burbuja es suficiente
    bool change = true;
    for (int i=1; i<mHdrs.count() && change; i++)
    {
        change = false;
        for (int j=0; j<mHdrs.count()-1; j++)
        {
            if (compare(mHdrs.data()+j, mHdrs.data()+j+1) > 0)
            {
                MPCReportHdr hdr = mHdrs.at(j);
                mHdrs.replace(j, mHdrs[j+1]);
                mHdrs.replace(j+1, hdr);
                change = true;
            }
        }
    }
}

void MPCReport::addMeasure(const MPCReportMeasure& mea)
{
    mMeasures.append(mea);
    MPCReportHdr* hdr_num = hdrByHdr("NUM");
    if (hdr_num) hdr_num->setText(QString("%1").arg(mMeasures.count()));
    setDirty(true);
}

void MPCReport::removeMeasure(int mid)
{
    ASSERT(mMeasures.length() >= mid, "Out of index removing report measure");
    mMeasures.removeAt(mid);
    MPCReportHdr* hdr_num = hdrByHdr("NUM");
    if (hdr_num) hdr_num->setText(QString("%1").arg(mMeasures.count()));
    setDirty(true);
}

int MPCReport::compare(MPCReportHdr*a, MPCReportHdr*b)
{
    if (a->hdr() == b->hdr() && a->hdr() != "CON") return 0;
    if (a->hdr() == "COD") return -1;
    if (b->hdr() == "COD") return 1;
    if (a->hdr() == "CON" && b->hdr() == "CON" && !a->text().startsWith("[")) return -1;
    if (a->hdr() == "CON" && b->hdr() == "CON" &&  a->text().startsWith("[")) return 1;
    if (a->hdr() == "CON") return -1;
    if (b->hdr() == "CON") return 1;
    if (a->hdr() == "OBS") return -1;
    if (b->hdr() == "OBS") return 1;
    if (a->hdr() == "MEA") return -1;
    if (b->hdr() == "MEA") return 1;
    if (a->hdr() == "COM") return -1;
    if (b->hdr() == "COM") return 1;
    if (a->hdr() == "TEL") return -1;
    if (b->hdr() == "TEL") return 1;
    if (a->hdr() == "ACK") return -1;
    if (b->hdr() == "ACK") return 1;
    if (a->hdr() == "AC2") return -1;
    if (b->hdr() == "AC2") return 1;
    if (a->hdr() == "NET") return -1;
    if (b->hdr() == "NET") return 1;
    if (a->hdr() == "BND") return -1;
    if (b->hdr() == "BND") return 1;
    if (a->hdr() == "NUM") return -1;
    if (b->hdr() == "NUM") return 1;
    return a->hdr() < b->hdr() ? -1 : 1;
}

int MPCReport::containsHdr(const QString& hdr)
{
    for (int i=0; i<mHdrs.count(); i++)
        if (mHdrs.at(i).hdr() == hdr) return i+1;
    return 0;
}

int MPCReport::containsHdr(const QString &hdr, const QRegularExpression& regex)
{
    for (int i=0; i<mHdrs.count(); i++)
        if (mHdrs.at(i).hdr() == hdr && regex.match(mHdrs.at(i).text()).hasMatch()) return i+1;
    return 0;
}

bool MPCReport::discovery(const QString & id_des)
{
    if (MpcUtils::isMinorPlanet(id_des))
    {
        int number = MpcUtils::unpack5Number(id_des);
        for (int i=0; i<mMeasures.count(); i++)
            if (mMeasures.data()[i].number() == number && mMeasures.data()[i].discovery()) return true;
    }
    else
    {
        for (int i=0; i<mMeasures.count(); i++)
            if (mMeasures.data()[i].designation() == id_des && mMeasures.data()[i].discovery()) return true;
    }
    return false;
}

MpcGeoInfo MPCReport::geoInfo()
{
    MpcGeoInfo gi;
    bool ok = false;
    for (int i=0; i<mHdrs.count(); i++)
    {
        if (mHdrs.at(i).hdr() == "COM")
        {
            QRegularExpression regex(REGEX_GEO_COORDINATES, QRegularExpression::CaseInsensitiveOption);
            QRegularExpressionMatch mc = regex.match(mHdrs.at(i).text());
            if (mc.hasMatch())
            {
                 gi.lng_str =  mc.captured("LONG");
                 gi.lng = Util::ParseLon(gi.lng_str);
                 if (IS_INVALIDF(gi.lng)) return MpcGeoInfo();
                 gi.lat_str = mc.captured("LAT");
                 gi.lat = Util::ParseLat(gi.lat_str);
                 if (IS_INVALIDF(gi.lat)) return MpcGeoInfo();
                 gi.alt_str = mc.captured("ALT");
                 gi.alt = gi.alt_str.toInt(&ok);
                 if (!ok) return MpcGeoInfo();
                 gi.method = mc.captured("METHOD");
            }
            break;
        }
     }
    return gi;
}

MpcObservatory MPCReport::obsInfo()
{
    MpcObservatory obs;
    MPCReportHdr* hdr = hdrByHdr("COD");
    if (hdr && hdr->isValid())
        obs = MpcObservatory::find(hdr->text().trimmed());
    return obs;
}

MpcTelInfo MPCReport::telInfo()
{
    MpcTelInfo ti;
    for (int i=0; i<mHdrs.count(); i++)
    {
        if (mHdrs.at(i).hdr() == "TEL")
        {
            QRegularExpression regex(REGEX_TELESCOPEINFO, QRegularExpression::CaseInsensitiveOption);
            QRegularExpressionMatch mc = regex.match(mHdrs.at(i).text());
            if (!mc.hasMatch())
            {
                ti.errors << "Invalid format in TEL header";
                return ti;
            }
            ti.valid = true;
            if (mc.captured("APERTURE").length()) ti.aperture = mc.captured("APERTURE").toFloat();
            if (mc.captured("FOCALRATIO").length())  ti.focalratio = mc.captured("FOCALRATIO").toFloat();
            if (mc.captured("TELTYPE").length()) ti.type = mc.captured("TELTYPE");
            if (mc.captured("CCD").length())
            {
                if (mc.captured("CCD").endsWith("CCD"))
                {
                    ti.withCCD = true;
                    ti.ccdInfo = mc.captured("CCD").mid(0, mc.captured("CCD").length()-3).trimmed();
                }
                else
                {
                    ti.withCCD = false;
                    ti.ccdInfo = mc.captured("CCD");
                }
            }
            if (mc.captured("REDUCER").length()) ti.reducer = mc.captured("REDUCER").toFloat();
        }
    }
    return ti;
}

MPCReportHdr* MPCReport::hdrByHdr(const QString& hdr)
{
    for (int i=0; i<mHdrs.count(); i++)
        if (mHdrs.data()[i].hdr() == hdr) return mHdrs.data()+i;
    return NULL;
}

MPCReportHdr* MPCReport::hdrByText(const QString& text)
{
    for (int i=0; i<mHdrs.count(); i++)
        if (mHdrs.data()[i].toString() == text) return mHdrs.data()+i;
    return NULL;
}

MPCReportHdr* MPCReport::hdrByLine(int line)
{
    if (line < mHdrs.count()) return mHdrs.data()+line;
    return NULL;
}

MPCReportMeasure* MPCReport::measureByLine(int line)
{
    if (line < mHdrs.count()+mMeasures.count()) return mMeasures.data()+(line-mHdrs.count());
    return NULL;
}


bool MpcUtils::isMinorPlanet(QString number)
{
    number = number.trimmed();
    while (number.startsWith('0')) number = number.right(number.count()-1);
    if (number.length() > 5 || !number.length()) return false;
    bool ok; number.toInt(&ok); if (ok) return true;
    for (int i=0; i<number.length(); i++)
        if (!((number.at(i) >= 'A' && number.at(i) <= 'Z' ||
            number.at(i) >= 'a' && number.at(i) <= 'z' ||
            number.at(i) >= '0' && number.at(i) <= '9'))) return false;
    return true;
}

bool MpcUtils::isProvDesig(QString td)
{
    td = td.trimmed();
    if (td.length() != 7) return false;
    QRegularExpressionMatch mc =
        QRegularExpression("[LJK]\\d[A-Za-z0-9]{4}").match(td);
    if (mc.hasMatch()) return true;
    mc = QRegularExpression("[A-Za-z0-9]{7}").match(td);
    if (mc.hasMatch()) return true;
    return false;
}

QString MpcUtils::pack5Number(int number)
{
    if (number > MAXPACK5) return "";
    QString pk = QString("").sprintf("%05d",number);
    if (pk.length() == 5) return pk;
    QString left = pk.left(2);
    QString right = pk.right(4);
    int inum = left.toInt();
    char chr = NUM2PACK5(inum);
    QString cad = QString("%1%2").arg(chr).arg(right);
    return cad;
}

int MpcUtils::unpack5Number(const QString number)
{
    bool ok = false;
    int nm = 0;
    if ((number.data()[0] >= 'A' && number.data()[0] <= 'Z') || (number.data()[0] >= 'a' && number.data()[0] <= 'z'))
    {
        QString left = number.left(1);
        QString right = number.right(4);
        int chr_numb = left.data()[0].toLatin1(); //A-Z -> 10-35  [a-z] -> 36-61
        int ileft = 10000*PACK52NUM(chr_numb);
        nm = ileft + right.toInt(&ok);
    }
    else nm = number.toInt(&ok);
    //Devolver so la conversión fue bien
    if (!ok) return 0;
    return nm;
}


QDate MpcUtils::unpackDate(const QString dt)
{
    bool ok = false;
    int year, month, day;
    if (dt.startsWith("I")) year = 1800;
    else if (dt.startsWith("J")) year = 1900;
    else if (dt.startsWith("K")) year = 2000;
    else return INVALID_DATE;
    QString ayear = dt.mid(1, 2);
    int iyear = ayear.toInt(&ok); if(!ok) return INVALID_DATE;
    year += iyear;
    char charmonth = dt.at(3).toLatin1();
    if (charmonth >= '1' && charmonth <= '9') month = charmonth - '1' + 1;
    else if (charmonth >= 'A' && charmonth <= 'C') month = charmonth  - 'A' + 10;
    else return INVALID_DATE;
    char charday = dt.at(4).toLatin1();
    if (charday >= '1' && charday <= '9') day = charday - '1' + 1;
    else if (charday >= 'A' && charday <= 'V') day = charday - 'A' + 10;
    else return INVALID_DATE;
    return QDate(year, month, day);
}

MpcOrbRecord::MpcOrbRecord()
{
    mValid = false;
}

MpcOrbRecord MpcOrb::findObject(const QString& ref,char* buffer)
{
    //Cuidadin, que buscamos primero en la cache
    if (ref.length() == 8) return findComet(ref,buffer);
    if (ref.length() == 5 && (ref.endsWith('C') ||
                              ref.endsWith('P') ||
                              ref.endsWith('D') ||
                              ref.endsWith('X') ||
                              ref.endsWith('A')
                              )) return findComet(ref,buffer);

    //Como es dificil de averiguar
    MPCReportMeasure::MeasureType mt = MPCReportMeasure::IdMeasure(ref);
    if (mt == MPCReportMeasure::MINOR_PLANET) return findMP(ref,buffer);
    else if (mt == MPCReportMeasure::COMET) return findComet(ref,buffer);
    else return MpcOrbRecord();
}

MpcOrbRecord MpcOrb::findMP(const QString& _ref,char* buff)
{
    QString ref;
    bool ok;
    if (_ref.trimmed().toInt(&ok) > 0 && ok) ref = MpcUtils::pack5Number(_ref.trimmed().toInt());
    else ref = _ref;
    //Comprobar si se encuentra en la caché
    QString cacheitem;
    mCacheMutex.lock();
    if (mObjCache.contains(ref)) cacheitem = mObjCache[ref];
    mCacheMutex.unlock();
    if (buff && cacheitem.length()) strcpy(buff,cacheitem.toLatin1().data());
    if (cacheitem.length()) return MpcOrbRecord::ParseMPCORB(cacheitem);
    //Caso contrario buscar en archivos
    MpcOrbRecord rec;
    QString fname = XGlobal::GetSettingsPath("MPCORB.DAT");
    QVector<int> sindex = getSearchIndexes(fname);
    if (sindex.count() != 4) return rec;

    //Abrir el archivo y encontrar la posición del primer registro mpc
    QFile fil(fname);
    if (!fil.open(QFile::ReadOnly)) return rec;
    QTextStream ts(&fil);
    //¿Es un número de planeta menor
    if (MpcUtils::isMinorPlanet(ref))
    {
        int id = MpcUtils::unpack5Number(ref);
        ts.seek(sindex[0] + MPC_REC_SZ * (id-1));
        QString cad = ts.readLine();
        if (cad.left(5) != ref) return rec;
        rec = MpcOrbRecord::ParseMPCORB(cad);
        fil.close();
        if (rec.valid())
        {
            mCacheMutex.lock();
            if (!mObjCache.contains(ref)) mObjCache.insert(ref, cad);
            while (mObjCache.size() > MPC_MAX_CACHE_RECORDS) mObjCache.remove(mObjCache.begin().key());
            mCacheMutex.unlock();
            if (buff) strcpy(buff, cad.toLatin1().data());
        }
        return rec;
    }
    //¿Es una designación temporal?
    //Realizamos una búsqueda binaria hasta el final del archivo porque estos elementos vienen ordenados
    rec = binarySearch(ref, &ts, sindex.at(1), sindex.at(2),7, MPC_REC_SZ);
    if (!rec.valid()) rec = binarySearch(ref, &ts, sindex.at(2), sindex.at(3),7, MPC_REC_SZ);
    fil.close();
    return rec;
}

MpcOrbRecord MpcOrb::findComet(const QString& _ref,char* buff)
{
    //Comprobar si se encuentra en la caché
    QString cacheitem;
    mCacheMutex.lock();
    if (mObjCache.contains(_ref)) cacheitem = mObjCache[_ref];
    mCacheMutex.unlock();
    if (buff && cacheitem.length()) strcpy(buff,cacheitem.toLatin1().data());
    if (cacheitem.length()) return MpcOrbRecord::ParseComet(cacheitem);
    //Si no, pues lo buscamos en el archivo de cometas
    char buffer[MPC_REC_COMET_SZ];
    MpcOrbRecord rec;
    QByteArray ba = _ref.toLatin1();
    QString fname = XGlobal::GetSettingsPath("CometEls.txt");
    QFile fil(fname);
    if (!fil.open(QFile::ReadOnly)) return rec;
    while (!fil.atEnd())
    {
        int numread = fil.readLine(buffer, MPC_REC_COMET_SZ);
        if (!numread) break;
        char* ipos;
        if ((ipos = strstr(buffer, ba.data())) == buffer || (ipos == buffer+4))
        {
            rec = MpcOrbRecord::ParseComet(buffer);
            //Agregar el elemento a la caché
            if (rec.valid())
            {
                mCacheMutex.lock();
                if (!mObjCache.contains(_ref)) mObjCache.insert(_ref, buffer);
                while (mObjCache.size() > MPC_MAX_CACHE_RECORDS) mObjCache.remove(mObjCache.begin().key());
                mCacheMutex.unlock();
                if (buff) strcpy(buff, buffer);
            }
            break;
        }
    }
    fil.close();
    return rec;
}

MpcOrbRecord MpcOrb::binarySearch(const QString& ref, QTextStream* ts,int start, int end,int id_sz, int recsz)
{
    int streccount = (end - start) / recsz;
    MpcOrbRecord rec;
    //Realizamos una búsqueda binaria hasta el final del archivo porque estos elementos vienen ordenados
    int left = 0; int right = streccount-1; int mid = (left+right)/2;
    int idx_found = 0; int comp;
    ts->seek(start + recsz * left); QString sleft = ts->readLine(id_sz);
    ts->seek(start + recsz * right); QString sright = ts->readLine(id_sz);
    ts->seek(start + recsz * mid); QString smid = ts->readLine(id_sz);
    while(!idx_found)
    {
        if (sleft == ref) idx_found = left;
        else if (sright == ref) idx_found = right;
        else if (smid == ref) idx_found = mid;
        else if ((comp = ref.localeAwareCompare(smid))<0)
        {
            right = mid;
            ts->seek(start + recsz * right); sright = ts->readLine(id_sz);
        }
        else if (comp > 0)
        {
            left = mid;
            ts->seek(start + recsz * left); sleft = ts->readLine(id_sz);
        }
        else break;
        mid = (right+left)/2;
        ts->seek(start + recsz * mid); smid = ts->readLine(id_sz);
        if (left+1 >= right) break;
    }
    if (idx_found)
    {
        ts->seek(start + recsz * idx_found);
        smid = ts->readLine();
        rec = MpcOrbRecord::ParseMPCORB(smid);
        //Agregar el elemento a la caché
        if (rec.valid())
        {
            mCacheMutex.lock();
            if (!mObjCache.contains(ref)) mObjCache.insert(ref, smid);
            while (mObjCache.size() > MPC_MAX_CACHE_RECORDS) mObjCache.remove(mObjCache.begin().key());
            mCacheMutex.unlock();
        }
    }
    return rec;
}

QVector<int> MpcOrb::getSearchIndexes(const QString& fname, int recursivity)
{
    QFile fil(fname);
    if (!fil.open(QFile::ReadOnly)) return QVector<int>();
    QTextStream ts(&fil);
    QString cad = ts.readLine();
    fil.close();
    if (!cad.startsWith("#INDEX,") && !recursivity)
    {
        if (!addSearchIndexes(fname)) return QVector<int>();
        return getSearchIndexes(fname, 1);
    }
    QStringList sl = cad.split(",", QString::SkipEmptyParts);
    QVector<int> vcidx;
    for (int i=1; i<sl.count(); i++)
    {
        bool ok;
        vcidx.append(sl[i].toInt(&ok));
        if (!ok) return QVector<int>();
    }
    vcidx.append(fil.size());
    fil.close();
    return vcidx;
}

bool MpcOrb::addSearchIndexes(const QString& fname)
{
    QString add = "#INDEX";
    QFileInfo finfo(fname);
    QFile fil(fname);
    if (!fil.open(QFile::ReadWrite)) return false;
    QString cad;
    QTextStream ts(&fil);
    char buffer[MPC_REC_SZ+2];

    if (finfo.fileName() == "MPCORB.DAT")
    {
        //Primero buscar el indicador "-----------------------------------"
        while (!fil.atEnd() && !cad.startsWith("-------------------------------------------"))
            cad = ts.readLine();
        add += QString().sprintf(",%d",(int)ts.pos());
        //Ahora buscar todos los saltos de línea
        int maxde = MpcUtils::unpack5Number(mMpcMaxKnownDesignation);
        int stbasepos = ts.pos() + MPC_REC_SZ * (maxde-1);
        int fpos = stbasepos; fil.seek(stbasepos);
        while (!fil.atEnd())
        {
            quint64 numread = fil.readLine(buffer, MPC_REC_SZ+1);
            fpos += numread;
            if (numread == 1)
                add += QString().sprintf(",%d",fpos);
        }
        ts.seek(0);
        ts << add << "\n";
        fil.close();
        return true;
    }
    fil.close();
    return false;
}

QVector<double> MpcOrbRecord::getPos(QDateTime dt)
{

    double ra = INVALID_FLOAT, dec = INVALID_FLOAT;
/* Sky::calcPlanetCoord(
                QDateTime(QDate(2015,06,19),QTime(13,22,49),Qt::UTC),
                Sky::dEpoch,
                Sky::fPeriod[0],Sky::fEcc[0],Sky::fEpochLong[0],Sky::fPerihelionLong[0],Sky::fSMA[0],Sky::fAscNode[0],Sky::fIncl[0],
                Sky::fPeriod[Sky::EARTH],Sky::fEcc[Sky::EARTH],Sky::fEpochLong[Sky::EARTH],Sky::fPerihelionLong[Sky::EARTH],Sky::fSMA[Sky::EARTH],true,&ra,&dec

    );*/
    //Obtener el periodo
    //double period =  1691.491928922146 / TROPICAL_YEAR;

    //2014-Jun-30 23:25     15 25 36.34 -22 06 09.1  15.22   6.12 2.02474350287453  16.3212234 135.6480 /T  14.4775
   /*
    Sky::calcPlanetCoord(
                    QDateTime(QDate(2014, 06, 30), QTime(23, 25, 0)),
                    QDateTime(QDate(2014, 12, 9), QTime(0, 0, 0), Qt::UTC),
                    period,
                    0.02723022221399827	,
                    180.6194420212456,
                    63.72275868624874,
                    2.778311918608153,
                    37.4194380607105,
                    4.43923496885829	,
                    false, &ra, &dec);
    */
    return QVector<double>(2) << ra << dec;
}


MpcOrbRecord MpcOrbRecord::ParseMPCORB(const QString& cad)
{
    MpcOrbRecord ob;
    ob.mValid = true;
    ob.mId = cad.left(7).trimmed();
    ob.mName = cad.mid(175, 19).trimmed();
    return ob;
}

MpcOrbRecord MpcOrbRecord::ParseComet(const QString& cad)
{
    MpcOrbRecord ob;
    ob.mValid = true;
    ob.mId = cad.left(12).trimmed();
    ob.mName = cad.mid(103, 56).trimmed();
    return ob;
}











