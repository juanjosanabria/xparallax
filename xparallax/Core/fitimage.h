#ifndef FITIMAGE_H
#define FITIMAGE_H


#include "core_global.h"
#include "core.h"
#include <QString>
#include <QLinkedList>
#include <QFile>
#include <QRegularExpression>


#define FIT_DOUBLE_FORMAT       "%0.16g"
#define SIMPLE_COMMENT          "File does conform to FITS standard"
#define BITPIX_COMMENT          "Number of bits per data pixel"
#define NAXIS_COMMENT           "Number of bits per data pixel"
#define NAXIS1_COMMENT          "Image width"
#define NAXIS2_COMMENT          "Image height"
#define BZERO_COMMENT           "Offset data range"
#define BSCALE_COMMENT          "Default scaling factor"
#define DATE_OBS_COMMENT        "Date and time when the image was taken"
#define EXTEND_COMMENT          "File may contain extensions"
#define OBSERVER_COMMENT        "Name of the observer"
#define PROGRAM_COMMENT         "Software used to process this image"
#define SECTION_HEADERS_ADD     "HEADERS_ADD"
#define SECTION_HEADERS_DELETE  "HEADERS_DELETE"
#define SECTION_HEADERS_SORT    "HEADERS_SORT"

class CORESHARED_EXPORT FitHeader
{

public:
    FitHeader();
    FitHeader(const FitHeader &hdr);
    FitHeader(const QString &name);
    FitHeader(const QString &name, const QString &value);
    FitHeader(const QString &name, const QString &value, const QString &comment);
    FitHeader(const QString &name, int value);
    FitHeader(const QString &name, int value, const QString &comment);
    FitHeader(const QString &name, float value);
    FitHeader(const QString &name, float value, const QString &comment);
    FitHeader(const QString &name, double value);
    FitHeader(const QString &name, double value, const QString &comment);
    ~FitHeader();

    bool operator==(const FitHeader & b);
    bool operator!=(const FitHeader & b);
    bool operator>(const FitHeader & b);
    bool operator<(const FitHeader & b);
    bool operator>=(const FitHeader & b);
    bool operator<=(const FitHeader & b);

    inline QString name(){return mName;}
    void setName(const QString &name);

    inline QString& value(){return mValue;}
    void setValue(const QString &value);

    inline QString& comment(){return mComment;}
    void setComment(const QString &comment);

    inline void resetID(){mUniqueID=S_UNIQID++;}
    inline bool isInvalid() const{return !mName.length();}
    inline bool isValid() const{return mName.length();}
    inline bool isEmpty() const{return mName ==  "--------";}            //Indica si la cabecera está vacía
    inline bool isHistory() const{return mName == "HISTORY";}           //Indica que la cabecera es de tipo history
    inline bool isComment() const{return mName == "COMMENT";}           //Indica que la cabecera es de tipo comment
    inline bool isHierarch() const{return mIsHierarch;}
    inline int uniqID(){return mUniqueID;}

    inline bool isData(){return !isInvalid() && !isEmpty() && !isHistory() && !isComment();}
    void toString(char *buffer);

    static const FitHeader Parse(QString txt);

private:    
    int     mUniqueID;
    QString mName;
    QString mValue;
    QString mComment;
    bool    mIsDotted;        //Indica si tiene comillas
    bool    mIsHierarch;      //Extensión de cabeceras Hierarch https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/f_user/node28.html

    static int S_UNIQID;
};

Q_DECLARE_METATYPE(FitHeader*)

class PlateConstants;
class CORESHARED_EXPORT FitHeaderList
{
public:
    enum ImageType {Unknown, Object, Bias, Dark, Flat};

public:
    FitHeaderList();
    ~FitHeaderList();

    int AddHeader(FitHeader hdr);
    int RemoveMatches(QRegularExpression name, QRegularExpression content);
    void RemoveHeader(const QString &name);
    void RemoveHeader(const FitHeader &hdr);
    void RemoveHeaders(const char* list[]);
    double ra();
    double dec();
    double pixSize();
    ImageType getImageType();

    int AddHeader(const QString &name);
    int AddHeader(const QString &name,const QString &value);
    int AddHeader(const QString &name, const QString &value, const QString &comment);
    int AddHeader(const QString &name, int value);
    int AddHeader(const QString &name, int value, const QString &comment);
    int AddHeader(const QString &name, float value);
    int AddHeader(const QString &name, float value, const QString &comment);
    int AddHeader(const QString &name, double value);
    int AddHeader(const QString &name, double value, const QString &comment);

    void SetHeader(const QString &name,const QString &value){ AddHeader(name,value); }
    void SetHeader(const QString &name, const QString &value, const QString &comment){ AddHeader(name, value, comment); }
    void SetHeader(const QString &name, int value) { AddHeader(name, QString::number(value)); }
    void SetHeader(const QString &name, int value, const QString &comment) { AddHeader(name, QString::number(value), comment); }
    void SetHeader(const QString &name, float value) { AddHeader(name, value); }
    void SetHeader(const QString &name, float value, const QString &comment){ AddHeader(name, QString().sprintf("%f",value), comment); }
    void SetHeader(const QString &name, double value){ AddHeader(name, value); }
    void SetHeader(const QString &name, double value, const QString &comment){ AddHeader(name, QString().sprintf("%lf",value), comment); }
    void ReplaceHeader(int idx,const FitHeader &hdr);
    void RemoveAt(int idx);
    void Insert(int idx, const FitHeader &hdr);
    void UpdateHeader(const FitHeader &hdr);
    void Switch(int idx1, int idx2);
    void Sort(int (*lessthan)(FitHeader a, FitHeader b));

    //Lectura y escritura de INI
    bool ReadFromIni(const QString &filename,const QString &section);
    bool SaveToIni(const QString &filename,const QString &section);

    /**
     * @brief LookForPlateRATAN Devuelve una solución astrométrica.
     * Es responsabilidad del que llama liberar la memoria ocupada
     * @return
     */
    PlateConstants* LookForPlateRATAN();

    /**
     * @brief LookForPlateROTA Devuelve una solución astrométrica basada
     * en centro y rotación. La típica aplicada por el programa astrometrica
     * @return
     */
    PlateConstants* LookForPlateROTA();

    /**
     * @brief LookForPlateConstants Devuelve una solución astrométrica.
     * Es responsabilidad del que llama liberar la memoria ocupada
     * @return
     */
    PlateConstants* LookForPlateConstants();

    PlateConstants* LookForPlate();

    void Clear();

    inline int count(){ return mList.count(); }
    inline int width(){ return mWidth; }
    inline void setWidth(int wi){mWidth=wi;}
    inline int height(){ return mHeight;}
    inline void setHeight(int hei){mHeight=hei;}
    inline int bitpix(){return mBitPix;}
    inline float bzero(){return mBzero;}
    inline float bscale(){return mBscale;}
    inline float satLevel(){return mSatLevel;}
    inline void setSatLevel(float satlvl){ mSatLevel = satlvl;}
    inline int exptime(){return mExpTime;}
    inline bool dirty(){return mDirty;}
    inline void SetDirty(bool dirty){mDirty = dirty;}

    const FitHeader& get(int i);
    int indexOf(const QString &name);
    const FitHeader& getById(int id);
    const FitHeader& Contains(const QString &name);
    bool BadHeaderOrder();
    QString CheckFormat();
    void UpdateValues();

    /**
     * @brief GetHeaderBuffer Obtiene un buffer de datos con todas las cabeceras formateadas, tal cual se grabarían en el
     * archivo. Es responsabilidad del llamante liberar el buffer con un delete
     * @return Buffer de datos de 80 caracteres por cabecera, terminado en cero
     */
    char* GetHeaderBuffer();

    int invalidCount();
    int dataCount();
    int commentCount();
    int historyCount();
    int emptyCount();    
    int validCount();
    QDateTime  GetDate();                                 //Busca la fecha dentro de las cabeceras de la imagen

    ///
    /// \brief Lee la lista de cabeceras de un archivo FIT/FITS, devolviendo una lista vacía si hay error
    /// \param Archivo abierto
    /// \return la lista de cabeceras. Una lista vacía si hay error
    ///
    static FitHeaderList   ReadFromFile(QFile *fil);
    static FitHeaderList   ReadFromFile(const QString &filename);
    static bool            IsMainHeader(const QString& hdrname);
    static bool            IsStandardHeader(const QString& hdrname);
    static QString         HeaderComments(const QString& hdrname);

private:
    QList<FitHeader>    mList;
    int                 mWidth;
    int                 mHeight;
    int                 mBitPix;
    float               mBzero;
    float               mBscale;
    float               mSatLevel;
    int                 mExpTime;
    bool                mDirty;

    void    ReorderHeaders();

public:
    //Indica el orden lógico de las cabeceras y cabeceras a eliminar
    static const char *     S_HEADER_ORDER[];
    static const char *     S_HEADERS_DELETE[];
    static const char *     S_HEADERS_MAIN[];
    static const char *     S_HEADERS_XASTROMETRY[];        //Astrometríá XParallax PLATE A,B,C,D
    static const char *     S_HEADERS_ASTROMETRY[];         //Usadas para la astrometría
    static const char *     S_HEADERS_ASTROMETRY_EXTRA[];   //No necesarias para la astrometría pero a veces incluidas
    static const char *     S_HEADERS_DATEFRAME[];          //Fechas de la cabecera
    static const char *     S_HEADERS_STANDARD[];           //Cabeceras que son standard
    static const char *     S_HEADERS_COMMENTS[];           //Comentarios de las cabeceras
    static const FitHeader  S_EMPTY_HEADER;

};


class CORESHARED_EXPORT FitImage
{
public:
    FitImage();
    ~FitImage();

    static FitImage* OpenFile(const QString &filename, QString* err = NULL);

    inline bool     error(){return mErrorMsg.length();}
    inline QString& errorMsg(){return mErrorMsg;}

    inline QString&       filename(){return mFilename;}
    inline void           setFileName(const QString& fname){mFilename = fname;}
    inline int            width(){return mHeaders.width();}
    inline int            height(){return mHeaders.height();}
    inline int            bitpix(){return mBitPix;}
    inline int            numpix(){return mHeaders.width()*mHeaders.height();}
    inline float*         data(){return mData;}
    inline float          pixel(int x,int y){ return mData?mData[ x + y * mHeaders.width() ]:NAN; }
    inline int            exptime(){return mHeaders.exptime();}
    inline FitHeaderList* headers(){return &mHeaders;}
    inline float          minpix(){return mMinpix;}
    inline float          maxpix(){return mMaxPix;}
    inline float          averagepix(){return mAveragePix;}
    inline float          stdevpix(){return mStandardDev;}
    inline int            invalidPixels(){return mInvalidPixels;}
    //Acceso al histograma (2 resoluciones disponibles)
    inline int*           histogram(){return mHistogram;}
    inline int            histoSZ(){return mHistoSize;}
    inline int            histoMax(){return this->mMaxHistoIdx;}
    inline float          mode(){return this->mMode;}
    inline int            flux2HistoIdx(float flux);
    double                BilinearConvolution(double xPrime, double yPrime);

    inline void SetDirty(bool dirty){mDirty = dirty; mHeaders.SetDirty(dirty);}
    inline bool dirty(){return mDirty||mHeaders.dirty();}

    inline PlateConstants*  plate(){return mPlate;}
    void                    setPlate(PlateConstants* pt);
    PlateConstants*         LookForPlate();
    PlateConstants*         LookForPlateConstants();
    PlateConstants*         LookForPlateRATAN();
    PlateConstants*         LookForPlateROTA();
    PlateConstants*         LookForPlateWCS();
    QDateTime               GetDate();                                 //Busca la fecha dentro de las cabeceras de la imagen

    void        ComputeStats();
    bool        SaveToFile(const QString &filename, int bitpix);    //Guarda a un archivo realizando la transformación indicada
    bool        SaveToFile(const QString &filename);                //Guarda a un archivo
    FitImage*   Clone();                                            //Crea una copia de memoria de la imagen, devuelve el control
    FitImage*   CloneBinned(int bin);
    FitImage* ExtractSlice(QPointF a, QPointF b, int height);

    static bool SaveToFile(float* data,FitHeaderList* headers,const QString &filename,int bitpix, int norm_level = 0);
    static bool SaveToFile(float* data,int width,int height,const QString &filename,int bitpix, int norm_level = 0);

private:
    FitHeaderList   mHeaders;
    QString         mFilename;
    QString         mErrorMsg;                 //Descripción del error
    float*          mData;
    float           mMinpix;                    //Valor mínimo que toma un píxel dentro de la imagen
    float           mMaxPix;                    //Valor máximo que toma un píxel dentro de la imagen
    int             mBitPix;                    //Número de bits por píxel (8, 16, 32, -32)
    float           mAveragePix;
    float           mStandardDev;
    bool            mDirty;                     //Indica si ha sido modificada (cabeceras)
    PlateConstants* mPlate;
    int             mInvalidPixels;             //Número íxeles no válidos a cero ó INFinitos
    int*            mHistogram;                 //Histograma grande
    int             mHistoSize;                 //Tamaño del histograma
    int             mMaxHistoIdx;
    float           mMode;                      //Moda ó máximo del histograma

    /**
     * @brief OpenFitFile Abre una imagen de formato fit
     * @param filename
     * @return
     */
    static FitImage* OpenFitFile(const QString &filename, QString* err = NULL);
    static FitImage* OpenFitFile2(const QString &filename, QString* err = NULL); //Mas rápida (x5)

    /**
     * @brief OpenNonFitFile Abre un archivo que no es un archivo fit tipo jpeg,jpg,png,tif,tiff,bmp
     * @param filename
     * @return
     */
    static FitImage* OpenNonFitFile(const QString &filename);

    /**
     * @brief OpenSbigFile Abre un archivo de formato sbig
     * @param filename
     * @return
     */
    static FitImage* OpenSbigFile(const QString &filename);

};


//Ofrece funcionalidad de lectura de fit línea a línea
class CORESHARED_EXPORT FitScanner
{
public:
    FitScanner(QString filename);
    ~FitScanner();

    inline int width(){return mHeaders.width();}
    inline int height(){return mHeaders.height();}
    inline bool error(){ return !mFile || !mFile->isOpen();}
    inline int scanlinesize(){return width() * abs(mHeaders.bitpix()) / 8; }
    inline FitHeaderList   headers(){return mHeaders;}
    void ReadScanLine(float* scanline);

private:
    QFile*          mFile;
    FitHeaderList   mHeaders;
};

#define CP_COLOR_TABLE_SZ      1024
#define CP_HISTOGRAM_SZ        1024

class CORESHARED_EXPORT ConvergeProfile {

public:
    enum ConvergeType{
        HISTOGRAM_ECU,
        LINEAR,
        LOGARITMIC,
        SQUARE_ROOT,
        POWER,
        EXPONENTIAL
    };

public:
    ConvergeProfile();
    ~ConvergeProfile();
    void SetOptimumFor(FitImage* im);
    void SetOptimumFor(float* data, int width, int height);
    void SetImage(FitImage* im);
    void SetData(float* data, int width, int height);
    void SetDataAndRanges(float* data, int width, int height, FitImage* im);
    void CreateColorTable();
    void Converge(QImage *pic);
    float min(){return mMin;}
    float max(){return mMax;}
    void setMin(float min){mMin=min;}
    void setMax(float max){mMax=max;}


private:
    float*              mData;
    int                 mWidth;
    int                 mHeight;
    ConvergeType        mType;
    quint8*             mColorTable;
    float*              mContrastTable;
    int                 mColorTableSize;
    bool                mInvalidColorTable;
    float               mBackLevel;             //Nivel propio de brillo del fondo
    float               mBackStdev;             //Desviación típica del fondo

    float               mDataMin;               //Mínimo valor dentro de los datos
    float               mDataMax;               //Máximo valor dentro de los datos
    float               mMin;
    float               mMax;
    float               mBright;

    void Converge_Indexed8(QImage* pic);
    void Converge_RGB32(QImage* pic);
};


#endif // FITIMAGE_H
