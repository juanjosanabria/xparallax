#ifndef MPC_H
#define MPC_H

#include "core_global.h"
#include "core.h"
#include "util.h"
#include "xglobal.h"
#include <QNetworkSession>
#include <QNetworkAccessManager>

struct CORESHARED_EXPORT MpcGeoInfo
{
    double  lng;
    QString lng_str;
    double  lat;
    QString lat_str;
    int     alt;
    QString alt_str;
    QString method;

    inline bool isValid(){return lng_str.length()&&lat_str.length();}
    inline QString toString()
           {return  "Long. " + Util::LON2tring(lng) +
                ", Lat. " + Util::LAT2String(lat) +
                ", Alt. " + QString("").sprintf("%d", alt) + "m, " +  method;
           }
};


struct CORESHARED_EXPORT MpcTelInfo
{
    bool        valid;
    float       aperture;
    QString     type;
    float       focalratio;
    bool        withCCD;
    QString     ccdInfo;
    float       reducer;
    QStringList errors;

    MpcTelInfo(){aperture=focalratio=reducer=0;withCCD=false;valid=false;}
    bool isValid() {return valid&&!errors.length();}
    QString toString(){
        return
        QString("%1").arg(aperture) + "-m"
        + (focalratio > 0.001 ? QString(" f/%1").arg(focalratio) : "")
        + (" " + type.trimmed())
        + (ccdInfo.trimmed().length() ? (" + " + ccdInfo) : "" )
        + (withCCD ? ((ccdInfo.trimmed().length() ? QString(" ") : QString(" + ") ) + "CCD") : "")
        + (reducer > 0.001 ? QString(" + f/%1 focal reducer").arg(reducer) : "" );
    }
};


class CORESHARED_EXPORT MpcUtils
        : public WebReader
{
    Q_OBJECT

public:
    MpcUtils();
    virtual ~MpcUtils();

    static QDateTime   ParseMPCDate(const QString& dtmpc);
    static QString     Date2MPCDate(const QDateTime& dt);
    //Empaqueta un número de 6 dígitos de la forma (A0000 = 100000)
    static bool        isMinorPlanet(QString number);
    static bool        isProvDesig(QString td);
    static QString     pack5Number(int number);
    static int         unpack5Number(const QString number);

    //Fechas
    static QDate       unpackDate(const QString dt);
    //Acceso a fichero MP
    static QString     getMPOrbit(const QString& id);


    /**
     * @brief GetDateFrame Obtiene una fecha válida para un informe MPC por orden de preferencias exigiendo
     * que tenga una precisión de segundos. Evalúa los comentarios start/mid/end para corregir el tiempo de
     * exposición.
     * @param hl Conjunto de cabeceras de la imagen
     * @return
     */
    static QDateTime   GetDateFrame(FitHeaderList* hl);

private:

};

class CORESHARED_EXPORT MPCReportHdr
{
public:
    MPCReportHdr(bool valid=true);
    MPCReportHdr(const QString hdr, const QString val,bool valid=true);
    QString toHtml();
    QString toString();
    inline bool isValid(){return mValid;}
    inline QString hdr() const {return mHdr;}
    inline QString text() const {return mText;}
    inline void setHdr(const QString& hdr){mHdr=hdr; mDirty=true;}
    inline void setText(const QString& txt){mText=txt; mDirty=true;}
    inline bool dirty(){return mDirty;}
    inline void setDirty(bool dirty){mDirty = dirty;}

    static MPCReportHdr Parse(const QString& hdr);
    static QString texEncode(const QString& cad);
    static QString texDecode(const QString& cad);


private:
    bool        mValid;
    QString     mHdr;
    QString     mText;
    bool        mDirty;

    static const char* TEX_CHARS[];
};

class CORESHARED_EXPORT MPCReportMeasure
{
public:
    enum MeasureType{INVALID = 0, MINOR_PLANET = 1, COMET = 2, NATURAL_SATELLITE = 3};

    MPCReportMeasure();
    MPCReportMeasure(MeasureType mt);
    QString toString();
    QString toHtml();
    QString id();
    inline bool isValid(){return mType;}
    inline MeasureType type(){return mType;}
    inline void setType(MeasureType t){mType = t;}
    inline int number(){return mNumber;}
    inline void setNumber(int oid){if((mNumber=oid)) mDesignation="";mDirty=true;}
    inline QString& designation(){return mDesignation;}
    inline void setDesignation(const QString &des){mDesignation = des; if (des.length()) mNumber = 0;mDirty=true;}
    inline bool isMPTempDesig(){return !mNumber;}
    inline QDateTime dateObs(){return mDateObs;}
    inline void setDateObs(const QDateTime& dt){mDateObs = dt;}
    inline float mag(){return mMag;}
    inline void setMag(float m){mMag = m;}
    inline QString& obsCode(){return mObsCode;}
    inline void setObsCode(const QString& code){mObsCode=code.length()<=3?code:code.left(3); mDirty=true;}
    inline char note1(){return mNote1;}
    inline char note2(){return mNote2;}
    inline char planet(){return mPlanetID;}
    inline char cometOrbit(){return mCMOrbitType;}
    inline char band(){return mBand;}
    inline bool discovery(){return mDiscoveryAsterisk;}
    inline double ra(){return mRa;}
    inline void setRa(double ra){mRa = ra;}
    inline double dec(){return mDec;}
    inline void setDec(double dec){mDec = dec;}
    inline bool dirty(){return mDirty;}
    inline void setDirty(bool dirty){mDirty = dirty;}
    inline void setNote1(char nt1){mNote1=nt1; mDirty=true;}
    inline void setNote2(char nt2){mNote2=nt2; mDirty=true;}
    inline void setPlanet(char p){mPlanetID=p; mDirty=true;}
    inline void setComtOrbit(char o){mCMOrbitType=o; mDirty=true;}
    inline void setDiscovery(bool d){mDiscoveryAsterisk = d; mDirty=true;}
    inline void setBand(char b){mBand=b; mDirty=true;}
    static MPCReportMeasure Parse(const QString& hdr);
    //Identifica el tipo de medida según el identificador
    static MeasureType IdMeasure(const QString& id,int* number=NULL);

private:
    MeasureType mType;
    bool        mDirty;
    //Minor planets (y compartidos por  comets y satellites)
    int         mNumber;                //Planeta menor con número confirmado
    QString     mDesignation;           //Temporary or provisional designation
    bool        mDiscoveryAsterisk;     //Asterisco de descubrimiento que solo podrá tener una medida del informe
    //Comets
    char        mCMOrbitType;
    //Natural Satellites
    char        mPlanetID;
    //Compartidos
    char        mNote1;
    char        mNote2;
    QDateTime   mDateObs;
    double      mRa;
    double      mDec;
    float       mMag;
    char        mBand;
    QString     mObsCode;

};

class CORESHARED_EXPORT MpcObservatory
{
public:
    MpcObservatory(){mValid=false;}
    ~MpcObservatory(){;}

    inline QString& name(){ return mName;}
    inline QString& code(){ return mCode;}
    inline double lat(){return mLat;}
    inline double lon(){return mLong;}
    inline bool   isValid(){return mValid;}
           QString toString();

    static MpcObservatory Parse(const QString& cad);
    static MpcObservatory find(const QString& id);      //Busca los datos de un observatorio en la bbdd local

private:
    QString mCode;
    double  mLong; //Longitud (este) en grados
    double  mLat;  //Latitud en grados
    QString mName;
    bool    mValid;
    static  QMutex                   mCacheMutex;    //Caché de elementos recientes
    static  QHash<QString,QString>   mObjCache;      //Muy útil en consultas sucesivas

};

class CORESHARED_EXPORT MPCReport
{
public:
    MPCReport();
    ~MPCReport();
    QString toString(const char* newline="\r\n");
    QString toHtml();

private:
    QString                     mFilename;
    QString                     mError;
    QVector<MPCReportHdr>       mHdrs;
    QVector<MPCReportMeasure>   mMeasures;

public:
    inline const QString& filename(){return mFilename;}
    inline void setFilename(const QString& fn){mFilename = fn;}
    inline const QString& error(){return mFilename;}
    bool dirty();
    void setDirty(bool dirty);
    bool discovery(const QString & id_des);
    inline QVector<MPCReportHdr>* headers(){return &mHdrs;}
    inline QVector<MPCReportMeasure>* measures(){return &mMeasures;}
    void addMeasure(const MPCReportMeasure& mea);
    void removeMeasure(int mid); //Índice basado en cero
    void sortHdrs();
    int compare(MPCReportHdr*a, MPCReportHdr*b);
    QString md5();
    int containsHdr(const QString& hdr);
    int containsHdr(const QString &hdr, const QRegularExpression& regex_text);
    MpcGeoInfo geoInfo();
    MpcTelInfo telInfo();
    MpcObservatory obsInfo();

    MPCReportHdr* hdrByText(const QString& text);
    MPCReportHdr* hdrByHdr(const QString& hdr);
    MPCReportHdr* hdrByLine(int line);
    MPCReportMeasure* measureByLine(int line);

    static MPCReport* LoadFromFile(const QString& filename);
    static MPCReport* LoadFromString(const QString& str);

private:

};


class CORESHARED_EXPORT MpcRecord
{
public:
    MpcRecord();
     ~MpcRecord();

private:
    bool        mInvalid;
    int         mId;
    QString     mDesignation;
    double      mRa;
    double      mDec;
    float       mMag;
    QString     mComment;


private:
    static MpcRecord Parse(const QString& cad);

};

class CORESHARED_EXPORT MpcOrbRecord {

public:
    MpcOrbRecord();
     ~MpcOrbRecord(){;}
    inline bool valid(){return mValid;}
    inline void setValid(bool v){mValid = v;}
    inline QString& id(){return mId;}
    inline QString& name(){return mName;}
    inline void setName(const QString& n){mName = n;}
    inline QDate& epoch(){return mEpoch;}
    inline void setEpoch(const QDate& ep){mEpoch = ep;}
    inline float mag(){return mMag;}
    inline void setMag(float m){mMag = m;}
    inline double node(){return mNode;}
    inline void setNode(double l){mNode = l;}
    inline double inc(){return mInc;}
    inline void setInc(double in){mInc = in;}
    inline double ecc(){return mEcc;}
    inline void setEcc(double e){mEcc = e;}
    inline double jDPeri(){return mJDPeri;}
    inline void setjDPeri(double jd){mJDPeri = jd;}
    inline double peri(){return mPeri;}
    inline void setPeri(double per){mPeri = per;}
    inline double periDist(){return mPeriDist;}
    inline void setPeriDist(double per){mPeriDist = per;}

    QVector<double> getPos(QDateTime dt);
    static MpcOrbRecord ParseMPCORB(const QString& cad);
    static MpcOrbRecord ParseComet(const QString& cad);

private:
    bool        mValid;
    QString     mId;
    QString     mName;
    QDate       mEpoch;
    float       mMag;

    double      mJDPeri;        //Fecha de paso por el perihelio
    double      mPeriDist;      //Distancia en el perihelio
    double      mEcc;           //Excentricidad
    double      mInc;           //Inclinación de la órbita
    double      mNode;          //Longitud del nodo ascendente
    double      mPeri;          //Distancia al perihelio

};

class CORESHARED_EXPORT MpcOrb {

public:
    static MpcOrbRecord findObject(const QString& ref,char* buffer=NULL);
    static MpcOrbRecord findMP(const QString& ref,char* buffer=NULL);
    static MpcOrbRecord findComet(const QString& ref,char* buffer=NULL);
    static QVector<int> getSearchIndexes(const QString&filename, int recursivity=0);
    static bool addSearchIndexes(const QString& fname);
    static void clearCache(){mCacheMutex.lock();mObjCache.clear();mCacheMutex.unlock();}

private:
    static QString mMpcMaxKnownDesignation;
    static MpcOrbRecord binarySearch(const QString& ref,QTextStream* ts,int start, int end,int id_sz, int recsz);

    static QMutex                   mCacheMutex;    //Caché de elementos recientes
    static QHash<QString,QString>   mObjCache;      //Muy útil en consultas sucesivas

};


#endif // MPC_H
