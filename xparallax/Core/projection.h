#ifndef PROJECTION_H
#define PROJECTION_H

#include "core_global.h"
#include "core.h"
#include "fitimage.h"
#include <QPointF>



class CORESHARED_EXPORT Projection
{
public:
    Projection();
    virtual ~Projection();

public:
    virtual void XYToRADec(double x, double y,double* ra, double* lat) = 0;
    virtual void RADecToXY(double ra, double lat,double* x,double* y) = 0;
    virtual void XYToRADec(double x, double y,float* ra, float* lat) = 0;
    virtual void RADecToXY(double ra, double lat,float* x,float* y) = 0;
};

class CORESHARED_EXPORT GnomonicProjection
        : public Projection

{
public:
    GnomonicProjection();
    GnomonicProjection(double center_x, double center_y, double center_ra, double center_dec, double rot1, double rot2, double scale_x, double scale_y);
    ~GnomonicProjection();
     void XYToRADec(double x, double y,double* ra, double* lat);
     void RADecToXY(double ra, double lat,double* x,double* y);
     void XYToRADec(double x, double y,float* ra, float* lat);
     void RADecToXY(double ra, double lat,float* x,float* y);

public:
    //Parámetros del centro
      double CenterPixX;		//En píxeles
      double CenterPixY;		//En píxeles

      double Alphai;		    //En radianes
      double Deltai;		    //En radianes

      //Parámetros de transformación
      double Rotation1;
      double Rotation2;

      double ScaleX;			//En grados de arco por píxel
      double ScaleY;			//En grados de arco por píxel

      //Variables de transformación directa (plano->esfera) e inversa (esfera->plano)
      double	cdelz;
      double 	sdelz;
      double	CD[2][2];	//Matriz de transformación directa
      double	ID[2][2];	//Matriz de transformación inversa

      void ComputeIntermediate();

};


class CORESHARED_EXPORT AstrometricReduction
{

public:
    AstrometricReduction();
    virtual~AstrometricReduction();

public:
    virtual void map(double x, double y,double &ra, double &lat) = 0;
    virtual void imap(double ra, double lat, double &x, double &y) = 0;
    virtual bool read(FitHeaderList* headers) = 0;

protected:
    double      mRA0;       //Ascensión recta del centro óptico en horas
    double      mDEC0;      //Declinación del centro óptico en grados
    double      mX0;        //Píxel central
    double      mY0;        //Píxel central

    void Std2Equ(double x, double y, double *ra, double *lat);
    void Equ2Std(double ra, double lat, double *x, double *y);

};




#endif // PROJECTION_H











