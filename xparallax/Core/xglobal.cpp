#include "xglobal.h"
#include "vo.h"
#include "calibration.h"
#include <QCoreApplication>
#include <QFileInfo>
#include <QDir>
#include <QStandardPaths>
#include <QLoggingCategory>

QString         XGlobal::BIN_PATH;          //Ruta al directorio donde se encuentra el ejecutable
QString         XGlobal::CONF_PATH;         //Ruta donde están los archivos de configuración
QString         XGlobal::FONT_PATH;         //Ruta donde están los archivos de ruentes
QString         XGlobal::LIC_PATH;          //Ruta donde están los archivos de licencia
QString         XGlobal::PLUGIN_PATH;       //Ruta donde están los archivos de plugins
QString         XGlobal::DATA_PATH;         //Ruta donde están los archivos de datos
QString         XGlobal::CACHE_PATH;        //Ruta con archivos de caché (peticiones web)
double          XGlobal::SCREEN_DPI=1.0;    //Relación de aspecto entre píxeles y puntos de pantalla
QSettings*      XGlobal::INI_SETTINGS=NULL; //Archivo INI de configuración genereal
Logger*         XGlobal::LOGGER = NULL;     //Objeto de logging
EnviParameters  XGlobal::APP_SETTINGS;      //Configuración de la aplicación
QString         XGlobal::OS_TYPE = "xx";    //Windows, Linux, Mac, Unknown
int             XGlobal::OS_BITS = -1;      //32 ó 64 bits
#ifdef QT_DEBUG
bool            XGlobal::DEBUG = true;      //Indica si se motrarán las trazas de tipo DINFO
#else
bool            XGlobal::DEBUG = false;
#endif
int             XGlobal::LIC_REST = 0;
int             XGlobal::LIC_QUOTA = 0;

XGlobal::XGlobal()
{
}


void XGlobal::initialize()
{
    QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
    setlocale(LC_ALL,"en_US.UTF-8");        //Importantísimo para compilaciones en Linux
    LOGGER = new Logger();
#ifdef QT_DEBUG
    BIN_PATH = QDir::toNativeSeparators(QDir::currentPath());
#else
    BIN_PATH = QDir::toNativeSeparators(QFileInfo(QCoreApplication::applicationFilePath()).absolutePath());
#endif
    //Directorio de configuración. una ruta /home
    CONF_PATH = QDir::cleanPath(QString("%1/../%2").arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)).arg(XPROGRAMS));
    if (!QDir(CONF_PATH).exists()) QDir().mkpath(CONF_PATH);
    //Otras rutas de plugins y fuentes
    FONT_PATH = QDir::cleanPath(QString("%1/../fonts").arg(BIN_PATH));
    LIC_PATH = QDir::cleanPath(QString("%1/../lic").arg(BIN_PATH));
    PLUGIN_PATH = QDir::cleanPath(QString("%1/../plugins").arg(BIN_PATH));
    DATA_PATH = QDir::cleanPath(QString("%1/../dat").arg(BIN_PATH));
    CACHE_PATH = QDir::cleanPath(QString("%1/cache").arg(CONF_PATH));
    if (!QDir(CACHE_PATH).exists()) QDir().mkpath(CACHE_PATH);

    INI_SETTINGS = new QSettings(QDir::cleanPath(QString("%1/%2").arg(CONF_PATH).arg("settings.ini")),QSettings::IniFormat);
    APP_SETTINGS.Load(INI_SETTINGS,"");
    APP_SETTINGS.run_c++;

    #if defined(Q_OS_WIN)
        OS_TYPE = "Windows";
    #elif defined(Q_OS_MAC)
        OS_TYPE = "Mac";
    #elif defined(Q_OS_LINUX)
        OS_TYPE = "Linux";
    #else
        OS_TYPE = "Unknown";
    #endif
    OS_BITS = sizeof(void*)*8;

    VizierReader::loadConf();
    SesameReader::loadConf();

    //Inicializar generador de números aleatorios
    QTime time = QTime::currentTime();
    qsrand((uint)time.msecsSinceStartOfDay());

    //Guardar la configuración (porque se ha cambiado el número de veces ejecutado)
    APP_SETTINGS.Save(INI_SETTINGS,"");
    //Evitar que se muestren errores de SSL
    QLoggingCategory::setFilterRules("qt.network.ssl.warning=false");
}

void XGlobal::Save()
{
   APP_SETTINGS.Save(INI_SETTINGS,"");
}

void XGlobal::End()
{
    VizierReader::ClearConf();
    SesameReader::ClearConf();
    Save();
    if (INI_SETTINGS) delete INI_SETTINGS;
    if (LOGGER) delete LOGGER;
}

QString XGlobal::GetSettingsPath(const QString& filename)
{
    return QDir::cleanPath(QString("%1/%2").arg(CONF_PATH).arg(filename));
}

QString XGlobal::GetDataPath(const QString& filename)
{
  return QDir::cleanPath(QString("%1/%2").arg(DATA_PATH).arg(filename));
}

QSettings* XGlobal::GetSettings(const QString& filename)
{
    return new QSettings(GetSettingsPath(filename),QSettings::IniFormat);
}


