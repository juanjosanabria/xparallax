#ifndef CORE_DEF_H
#define CORE_DEF_H

//Información de la versión
#define XVERSION_MAJOR   1
#define XVERSION_MIDDLE  2
#define XVERSION_MINOR   5
#define XVERSION         "1.2.5"
#define XPROGRAM         "XParallax"
#define XPROGRAMS        "XParallax"
#define XMAIL            "info@xparallax.com"
#ifdef QT_DEBUG
#define XUSER_AGENT      "Test user agent"
#else
#define XUSER_AGENT      XPROGRAM " - " XVERSION " user agent"
#endif
#define XPROGRAM_HTML    "<span style='font-name: Tahoma, Geneva'>XParallax</span>"
#define DEBUG_UID        "{c4909475-9314-4196-9a9e-886dbf69c7f2}"

/********** INTERCAMBIO DE RANGOS *******************
   A1 --------v1-------B1
   A2-----v2----B2
   v2 = (v1 - A1) / ( B1 - A1 ) * (B2 - A2) + A2
 ****************************************************/
#define RANGECHANGE(V,A1,B1,A2,B2) ( ((V)-(A1)) * ((B2)-(A2)) / ((B1)-(A1)) + (A2) )
//Macros máximo y mínimo
#define MINVAL(A,B)      ((A)<(B)?(A):(B))
#define MAXVAL(A,B)      ((A)>(B)?(A):(B))
#define MINVAL3(A,B,C)   ((A)<(B)?MINVAL(A,C):MINVAL(B,C))
#define MAXVAL3(A,B,C)   ((A)>(B)?MAXVAL(A,C):MAXVAL(B,C))
#define MINVAL4(A,B,C,D) ((A)<(B)?MINVAL3(A,C,D):MINVAL3(B,C,D))
#define MAXVAL4(A,B,C,D) ((A)>(B)?MAXVAL3(A,C,D):MAXVAL3(B,C,D))
//Signo de un número
#define SIGN(A) ((A)>0?1:((A)<0?-1:0))
//Distancia cartesiana entre dos puntos
#define CART_DIST(X1,Y1,X2,Y2) (sqrt(((X2)-(X1))*((X2)-(X1))+((Y2)-(Y1))*((Y2)-(Y1))))
//Elevar al cuadrado
#define POW2(A) ((A)*(A))
//Comparación de excentricidades entre dos valores
#define ECOMP(A,B) (1-MINVAL(A,B)/MAXVAL(A,B))
//Comparación por porcentajes. B, valor base, P, porcentaje, V, valor a comparar B-B*P < V < B+B*P
#define PCOMP(B,P,V) (((V)>=((B)*(1-P))&&(V)<=((B)*(P+1))))
//Cuadrante de un vector en el plano, numerados en sentido antihorario desde el 1 (superior derecho)
#define CUADRANT(VX,VY) ((VX)>0&&(VY)>0?1:((VX)<0&&(VY)>0?2:((VX)<0&&(VY)<0?3:4)))
//Redondeo de uso recomendado
#define FROUND(N) ((N)<0.0?ceil((N)-0.5):floor((N)+0.5))
//Comprueba si un objeto derivado de QObject hererda la clase indicada
#define IS(OBJ,CLS)    (qobject_cast<CLS*>(OBJ))

//Para logar
#define LOG     XGlobal::LOGGER->info
#define INFO    XGlobal::LOGGER->info
#define ERR     XGlobal::LOGGER->error
#define WARN    XGlobal::LOGGER->warn
#define DINFO   XGlobal::LOGGER->debug
#define NETINFO XGlobal::LOGGER->net

//Necesario para enviar como parámetros los QString en las funciones de logging
#define PTQ(QS) ((QS).toLatin1().data())

//Factores de conversión entre unidades de ángulos
#define DEG2RAD     0.0174532925199432957692369076848900        //2*PI/360
#define DEG2HOR     0.0666666666666666666666666666666700        //24/360
#define RAD2DEG     57.295779513082320876798154814105000        //360/(2*PI)
#define HOR2DEG     15.000000000000000000000000000000000        //360/24
#define HOR2RAD     0.2617993877991494365385536152732900        //2*PI/24
#define RAD2HOR     3.8197186342054880584532103209403000        //24/(2*PI)
#define RAD2ASEC    206264.80624709635515647335733078000        //3600*2*PI
#define XAMIN2RAD   0.1047197551196597746154214461093200        //Arcmin a rad
#define RADIN360    6.2831853071795864769252867665590000        //2*PI
#define ROOT_2      1.4142135623730950488016887242097           //Raiz cuadrada de 2

//Valores muy pequeño y muy grande para cálculos de mínimos y máximos
#define FLOAT_MAX           3.402823466e+38F      // Máximo valor positivo
#define FLOAT_MIN           1.175494351e-38F      // Mínimo valor positivo sin ser cero
#define EPS                 0.000000000000001     // Precisión Lsqfit 1e-15

//Distintas épocas y algunos datos referentes a fechas
#define JD_HIPPARCOS        2448349.0625        //Para Hipparcos
#define JD_J2000            2451545.0000        //Mediodía del 1 de enero de 2000 12:00:00
#define JD_J1950            2433282.5000        //1 de enero de 1950 00:00:00
#define JD_J1900            2415020.0000        //Por definición, 100 años julianos antes de J2000
#define JULIAN_YEAR         365.25              //Duración del año juliano en días
#define SIDEREAL_YEAR       365.2563631         //Año sidéreo en días solares medios
#define TROPICAL_YEAR       365.242191
#define JD2MJD(d)           (d-2400000.5)       //Conversiones entre MJD<->JD
#define MJD2JD(d)           (d+2400000.5)

//Otros valores
#define FWHM_CTE            2.3548200450309493820231386529194

//Otros macros
#define INVALID_DATE        QDate(0, 0, 0)
#define INVALID_TIME        QTime(0,0,0,0)
#define INVALID_DATETIME    QDateTime(INVALID_DATE,INVALID_TIME)
#define INVALID_FLOAT       NAN                //Coma floatante no válido
#define INVALID_INT         (0x80000000)       //Entero inválido = -2147483648
#define IS_VALIDF(a)        (!isnan(a))        //Comprueba si un núm IS_VALIDF un float válido
#define IS_INVALIDF(a)      (isnan(a))         //Comprueba si un número no es un float válido
#define IS_INFINITE(a)      ((a)==PLUS_INFINITE||(a)==MINUS_INFINITE)
#define IS_NANINF(a)        (IS_INVALIDF(a)||IS_INFINITE(a))
#define PLUS_INFINITE       (INFINITO)         //Valor infinio
#define MINUS_INFINITE      (_INFINITO)        //Menos infinito
#define IS_LEAP(year)       (((year%4)==0)||(!(year%100==0)&&(year%400==0))) //Calcula si un año es bisiesto

//Comparación de floats y doubles
#define COMPF3(A,B,C)((fabs((A)-(B))<(C))?0:(((A)>(B))?1:-1))
#define COMPF2(A,B)(COMPF3(A,B,0.0000001))


#endif // CORE_DEF_H
