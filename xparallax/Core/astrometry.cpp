#include "astrometry.h"
#include "xglobal.h"
#include "core.h"
#include "error.h"
#include "novas.h"
#include <QThread>
#include <QtCore/QFileInfo>
#include <QRegularExpression>

AstrometryComputer::AstrometryComputer() :
    BackgroundWorker("Computing astrometry")
{
    vizierReader_ = NULL;
    detector_ = NULL;
    matcher_ = NULL;
    plateConstants_ = NULL;
}

AstrometryComputer::~AstrometryComputer()
{
    if (vizierReader_) delete vizierReader_;
    vizierReader_ = NULL;
    if (detector_) delete detector_;
    detector_ = NULL;
    if (matcher_) delete matcher_;
    matcher_ = NULL;
    if (plateConstants_) delete plateConstants_;
    plateConstants_ = NULL;
}

bool AstrometryComputer::doWork(WorkFileList* wf, DetectionParameters* dp, MatchParameters* mp)
{
    detectParams_ = dp;
    matchParams_ = mp;
    workList_ = wf;
    singleProcess_ = wf->fitimages()->count() == 1;
    wf->clearResults();

    if (singleProcess_) OnSetup(40);
    else if (wf->fitimages()->count()) OnSetup(wf->fitimages()->count());
    else OnSetup(wf->filenames()->count());

    OnStart();

    if (singleProcess_)
    {
        QString ok = ComputeAstrometry(wf->fitimages()->at(0));
        if (ok.length()) WARN(ok);
    }
    else if (wf->fitimages()->count())
    {
        for (int i=0; i<wf->fitimages()->count() && !cancelled(); i++)
        {
            OnProgress(i);
            QString ok = ComputeAstrometry(wf->fitimages()->at(i));
            if (ok.length()) WARN(ok);
        }
    }
    else
    {
        for (int i=0; i<wf->filenames()->count() && !cancelled(); i++)
        {
            OnProgress(i);
            INFO("Processing file: %s", PTQ(QFileInfo(wf->filenames()->at(i)).baseName()));
            QString ok = ComputeAstrometry(wf->filenames()->at(i));
            if (ok.length()) WARN(ok);
        }
    }

    OnEnd();

    if (!CountKO())
    {
        INFO(LOG_SEPARATOR);
        INFO("Astrometry finished with %d file%s reduced OK", CountOK(), CountOK() == 1 ? "" : "s");
        INFO(LOG_SEPARATOR);
    }
    else
    {
        WARN("Astrometry finished. %d errors", CountKO());
    }
    return true;
}

QString AstrometryComputer::ComputeAstrometry(FitImage* fitimage)
{
    //Comprobación de si es necesaria la fecha para la corrección de movimiento propio
    //--------------------------------------------------------------------------
    if (matchParams_->pm_correct == MatchParameters::PMDATE_FROM_FIT)
    {
        matchParams_->date_frame = fitimage->GetDate().date();
        if (!matchParams_->date_frame.year())
            INFO("No date information in image header!!");
    }
    //También comprobamos el tipo de centro de las estrellas que vamos a usar
    detectParams_->flags &= ~StarDetector::DEFAULT_CENTER_PEAK;
    detectParams_->flags &= ~StarDetector::DEFAULT_CENTER_WINDOWED;
    detectParams_->flags &= ~StarDetector::DEFAULT_CENTER_PSF;
    detectParams_->flags &= ~StarDetector::COMPUTE_WINDOWED_CENTER;
    detectParams_->flags &= ~StarDetector::COMPUTE_PSF_CENTER;
    if (!matchParams_->preferred_center || matchParams_->preferred_center==Core::STARCENTER_WINDOWED)
        detectParams_->flags |= StarDetector::COMPUTE_WINDOWED_CENTER | StarDetector::DEFAULT_CENTER_WINDOWED;
    else if (matchParams_->preferred_center == Core::STARCENTER_PSF)
        detectParams_->flags |= StarDetector::COMPUTE_PSF_CENTER | StarDetector::DEFAULT_CENTER_PSF;
    else if (matchParams_->preferred_center == Core::STARCENTER_PEAK)
        detectParams_->flags |= StarDetector::DEFAULT_CENTER_PEAK;
    //Si no, el centor por defecto es el iso

    bool ok = false;

    //Lectura de catálogo de las estrellas (Solamente se hará una vez, para el primer fit)
    //--------------------------------------------------------------------------
    if (!vizierReader_)
    {
        vizierReader_ = new VizierReader();
        OnMsg(QString("Reading catalog information: %1").arg(matchParams_->vizier_catalog));
        vizierReader_->SetCatalog(VizierCatalog::getCatalog(matchParams_->vizier_catalog));
        vizierReader_->SetServer(VizierServer::getServer(matchParams_->vizier_server));
        matchParams_->image_width = fitimage->width(); matchParams_->image_height = fitimage->height();
        matchParams_->ComputeScale();
        ok = vizierReader_->webRead(matchParams_->center_ra, matchParams_->center_dec, matchParams_->fov_radius_deg * matchParams_->fov_extension, matchParams_->mag_limit);
        if (!ok)
        {
            workList_->addResult(ERR_NETWORK,"Error downloading catalog information, please, check your Internet connection or move to another vizier server");
            return "Error downloading catalog information, please, check your internet connection or move to another vizier server";
        }
    }
    if (singleProcess_) OnProgress(10);
    if (!vizierReader_->stars()->count())
    {
        workList_->addResult(ERR_AST_INVALID_CATLOG_INFORMATION,"No stars downloaded from catalog. Please, check vizier server");
        WARN("No stars downloaded from catalog. Please, check vizier server");
        cancel();
        return "No stars downloaded from catalog. Please, check vizier server";
    }

    //Detectar las estrellas
    //--------------------------------------------------------------------------
    if (cancelled()) return "Cancelled";
    OnMsg("Detecting sources");
    if (detector_) delete detector_; detector_ = NULL;
    if (!detector_) detector_ = new StarDetectorParallel(fitimage->data(), fitimage->width(), fitimage->height(), *detectParams_);
    detector_->setData(fitimage->data(), fitimage->width(), fitimage->height());
    detector_->DetectStars(0, 0, fitimage->width() - 1, fitimage->height() - 1);
    if (singleProcess_) OnProgress(20);

    //Machear las estrellas
    //--------------------------------------------------------------------------
    if (cancelled()) return "Cancelled";
    OnMsg("Looking for match");
    if (!matcher_) matcher_ = new Matcher();
    matchParams_->image_width = fitimage->width();
    matchParams_->image_height = fitimage->height();
    matchParams_->image_invalid_pix = fitimage->invalidPixels();
    matchParams_->ComputeScale();
    matcher_->setParameters(*matchParams_);
    ok = matcher_->Match(detector_->stars(), detector_->count(), vizierReader_->stars());
    if (singleProcess_) OnProgress(30);
    if (!ok)
    {
        workList_->addResult(ERR_AST_MATCH_NOT_FOUND, "Match failed");
        WARN("Match failed...!!");
        return "Match failed...!!";
    }

    //Cálculo de la solución astrométrica
    //--------------------------------------------------------------------------
    if (cancelled()) return "Cancelled";
    OnMsg("Computing plate constants");
    INFO("Computing plate constants with %d stars", matcher_->matchcoin()->count());
    if (matcher_->matchCount() <= 3)
    {
        workList_->addResult(ERR_AST_NOT_ENOUGH_STARS);
        WARN("Match error. Not enough stars (%d) to compute astrometric solution", matcher_->matchCount());
        return "Match error. Not enough stars to compute astrometric solution";
    }
    if (plateConstants_) delete plateConstants_; plateConstants_ = NULL;
    //if (!mPlateConstants) mPlateConstants = ComputePlateConstants_ANUAL_BB(mMatcher, fitimage->headers());
    if (!plateConstants_) plateConstants_ = ComputePlateConstants(matcher_);


    //Cálculo del MAGZEROP fotométrico previo
    //--------------------------------------------------------------------------
    QString use_mag = "";
    double zerop = ComputeMagZeroP(use_mag="V", matcher_, detector_, vizierReader_);
    if (!IS_VALIDF(zerop))
        zerop = ComputeMagZeroP(use_mag=vizierReader_->catalog()->magfilter(), matcher_, detector_, vizierReader_);
    if (IS_VALIDF(zerop)) fitimage->headers()->AddHeader("MAGZEROP",zerop,"Photometric zero point at BAND " + use_mag);

    //Actualización de cabeceras
    //--------------------------------------------------------------------------
    plateConstants_->setEquinox(vizierReader_->catalog()->raDecEpoch());
    XGlobal::APP_SETTINGS.ct_fast++;                //Aumentar el número de ficheros calibrados astrométricamente
    AddRaDecHeader(fitimage, plateConstants_);
    //AddLspcHeader(fitimage, mPlateConstants);     //¿Realmente queremos añadir esta cabecera?
    AddCommentsHeader(fitimage, plateConstants_, matcher_);
    AddStarndardHeader(fitimage, plateConstants_);


    //Informe de resultados
    //--------------------------------------------------------------------------
    INFO("    PLATE CONSTANTS: a: %18.15f; b: %18.15f; c: %18.15f",plateConstants_->a(), plateConstants_->b(), plateConstants_->c());
    INFO("    PLATE CONSTANTS: d: %18.15f; e: %18.15f; f: %18.15f",plateConstants_->d(), plateConstants_->e(), plateConstants_->f());
    INFO("    PLATE CENTER   : RA: %18.15f; DEC: %18.15f",plateConstants_->ra(), plateConstants_->dec());
    INFO("    PLATE TRANSFORM STATS: Davg: %7.4f\"; Dstdev: %7.4f\"",plateConstants_->deltaavg(), plateConstants_->deltastdev());
    INFO("                 RA STATS: Davg: %7.4f\"; Dstdev: %7.4f\"",plateConstants_->deltaavgRA(), plateConstants_->deltastdevRA());
    INFO("                DEC STATS: Davg: %7.4f\"; Dstdev: %7.4f\"",plateConstants_->deltaavgDEC(), plateConstants_->deltastdevDEC());
    INFO("    REFERENCE STARS: %d", plateConstants_->ctrlcount());
    if (plateConstants_->ctrlcount() < 6)
    {
        WARN("Warning!! astrometic soluction precission is poor. Only a few stars matches with catalog entries");
        WARN("Please, increase magnitude limit or choose another deeper catalog");
    }
    //Añadir información del catálogo usado y el límite de magnitud ¿Realmente es necesario?

    //Fin de la cita!!
    //--------------------------------------------------------------------------
    fitimage->setPlate(plateConstants_);    //Dejamos el control de la reducción a la imagen
    plateConstants_ = NULL;

    if (singleProcess_) OnProgress(40);
    workList_->addResult(0);
    return "";
}

QString AstrometryComputer::ComputeAstrometry(const QString& filename)
{
    OnTitleChange(QString("Processing: <b>%1</b>").arg( QFileInfo(filename).baseName() ));
    OnMsg("Reading file");
    FitImage* fi = FitImage::OpenFile(filename);
    if (!fi) return "Unable to open fit image";
    if (fi->error())
    {
        delete fi;
        workList_->addResult(ERR_UNABLE_OPEN_INPUT_FILE);
        return QString("Unable to open file %1").arg(filename);
    }
    QString ok = ComputeAstrometry(fi);
    //Por último, si hay que salvar la imagen la salvamos
    QString dest_filename = workList_->overwrite() ? filename :
                                                     QString("%1/%2").arg(workList_->outputDir(),
                                                                          QFileInfo(filename).fileName());
    if (!ok.length())
    {
        INFO("Writing file: %s",PTQ(filename));
        if (!FitImage::SaveToFile(fi->data(),fi->headers(), dest_filename, fi->bitpix()))
        {
            workList_->addResult(ERR_UNABLE_WRITE_OUTPUT_FILE);
            ERR("ERROR SAVING FILE - %s",PTQ(filename));
            return QString("Error saving file %1").arg(filename);
        }
    }

    delete fi;
    return ok;
}

void AstrometryComputer::cancel()
{
    if (mEnded) return;
    mCancelled = true;
    if (matcher_) matcher_->cancel();
    if (detector_) detector_->cancel();
    WARN("Astrometry cancelled");
}

int  AstrometryComputer::CountOK()
{
   int ok = 0;
   for (int i=0; i<workList_->results()->count(); i++)
       if (!workList_->results()->at(i).retVal)
           ok++;
   return ok;
}

int  AstrometryComputer::CountKO()
{
    return  workList_->results()->count() - CountOK();
}

PlateConstants* AstrometryComputer::ComputePlateConstants(Matcher* mcher)
{
    PlateConstants* pc = new PlateConstants(mcher->parameters()->image_width, mcher->parameters()->image_height);
    pc->setCenter(mcher->parameters()->center_ra, mcher->parameters()->center_dec);
    for (int i=0; i<mcher->matchcoin()->count(); i++)
    {
        MatchCoincidence mc = mcher->matchcoin()->at(i);
        pc->AddCtrlPoint(mc.a_x, mc.a_y, mc.ra, mc.dec);
    }
    pc->Compute();
    pc->Refine(STDEV_PLATE_REJECT);
    return pc;
}

PlateConstants* AstrometryComputer::ComputePlateConstants_ANUAL_BB( Matcher* mcher, FitHeaderList* fh)
{
    FitHeader fhdate = fh->Contains("DATE-OBS");
    FitHeader obopen =fh->Contains("OPENTIME");
    FitHeader obclose =fh->Contains("CLOSTIME");
    QDateTime dtd = Util::ParseDate(fhdate.value());
    QTime t_open = Util::ParseTime(obopen.value());
    QTime t_close = Util::ParseTime(obclose.value());
    QTime tmid = QTime::fromMSecsSinceStartOfDay((t_close.msecsSinceStartOfDay()+t_open.msecsSinceStartOfDay())/2);

    double jd = Util::JulianDate(dtd.date().year(), dtd.date().month(), dtd.date().day(), tmid.hour(), tmid.minute(), tmid.second(), tmid.msec());


    PlateConstants* pc = new PlateConstants(mcher->parameters()->image_width, mcher->parameters()->image_height);
    pc->setCenter(mcher->parameters()->center_ra, mcher->parameters()->center_dec);
    for (int i=0; i<mcher->matchcoin()->count(); i++)
    {
        MatchCoincidence mc = mcher->matchcoin()->at(i);
        Novas::cat_entry ce; double ab_ra, ab_dec;
        Novas::make_cat_entry("dummy","CAT", 0, mc.ra, mc.dec, 0, 0, 0, 0, &ce);
        int ret = Novas::astro_star(jd, &ce, 0, &ab_ra, &ab_dec);
        ASSERT(!ret, "ERROR RET NOVAS");
        if (!ret) pc->AddCtrlPoint(mc.a_x, mc.a_y, ab_ra, ab_dec);

        double ira, idec;
        Novas::mean_star(jd, ab_ra, ab_dec, 0, &ira, &idec);
        ira += 1;
    }
    pc->Compute();
    pc->Refine(STDEV_PLATE_REJECT);
    return pc;
}

void AstrometryComputer::AddRaDecHeader(FitImage* fitimage, PlateConstants* pc)
{
    double ra, dec;
    pc->map(fitimage->width() / 2.0 - 1, fitimage->height() / 2.0 - 1, &ra, &dec);

    //Cabecera méramente informativa, indica RA y DEC legibles del centro de la imagen
    FitHeader fhra = fitimage->headers()->Contains("RA");
    FitHeader fhdec = fitimage->headers()->Contains("DEC");
    if (fhra.isInvalid() && fhdec.isInvalid())
    {
        fitimage->headers()->AddHeader("RA",Util::RA2String(ra),"Right ascension at the center of the image");
        fitimage->headers()->AddHeader("DEC",Util::DEC2String(dec),"Declination at the center of the image");
    }
}

void AstrometryComputer::AddCommentsHeader(FitImage* fitimage, PlateConstants* pc, Matcher* mc)
{
    //Añadir cabeceras no astrométricas, meramente informativas
    fitimage->headers()->RemoveMatches(QRegularExpression("^COMMENT$"), QRegularExpression("^Astrometry by .*"));
    fitimage->headers()->AddHeader("COMMENT",
                                   QString("Astrometry by %1 v%2").arg(XPROGRAM).arg(XVERSION) +
                                   QString("").sprintf(" -- Ref.stars: %d; Prob: <%0.0e", pc->ctrlcount(), mc ? mc->matchProb() : 0)
                                   );
}

void AstrometryComputer::AddLspcHeader(FitImage* fitimage, PlateConstants* pc)
{
    //Cabeceras LSPC
    fitimage->headers()->AddHeader("LSPC-A",pc->a(),"Plate constant [a]");
    fitimage->headers()->AddHeader("LSPC-B",pc->b(),"Plate constant [b]");
    fitimage->headers()->AddHeader("LSPC-C",pc->c(),"Plate constant [c]");
    fitimage->headers()->AddHeader("LSPC-D",pc->d(),"Plate constant [d]");
    fitimage->headers()->AddHeader("LSPC-E",pc->e(),"Plate constant [e]");
    fitimage->headers()->AddHeader("LSPC-F",pc->f(),"Plate constant [f]");
    fitimage->headers()->AddHeader("LSPC-RA",pc->ra(),"Plate center");
    fitimage->headers()->AddHeader("LSPC-DEC",pc->dec(),"Plate center");
}

void AstrometryComputer::AddStarndardHeader(FitImage* fitimage, PlateConstants* pc)
{
    //Coordenadas ecuatoriales en el píxel de referencia
    //Según WCS el fit no comienza en 0 sino en 1, tendremos un error de un píxel si no restamos 1
    //Esto está probado con Aladin superponiendo a la imagen reducida el catálogo de origen (normalmente PPMXL)
    //y comprobando las posiciones. El caso bueno se obtiene de restar 1 a la coordenada de imagen antes de calcular
    //la ascensión recta y la declinación
    double ra, dec;
    double ref_x = fitimage->width() / 2.0;
    double ref_y = fitimage->height() / 2.0;
    pc->map(ref_x - 1.0, ref_y - 1.0, &ra, &dec);

    //Limpiamos las cabeceras de otras astrometrías y añadimos el comentario de quién la ha hecho
    fitimage->headers()->RemoveHeaders(FitHeaderList::S_HEADERS_ASTROMETRY);
    fitimage->headers()->RemoveHeaders(FitHeaderList::S_HEADERS_ASTROMETRY_EXTRA);

    //http://www.not.iac.es/instruments/FITS-header/mef/new_keywords.html
    //------------------------------------------------------------------------------------------------------------
    //Cálculo de la proyección Gnomónica RA---TAN DEC--TAN,
    // el entrecomillado es neceario para programas que usan WCSlib
    //------------------------------------------------------------------------------------------------------------
    //Tipo de proyección
    fitimage->headers()->AddHeader("CTYPE1","'RA---TAN'","Name of coordinate (RA, gnomonic projection)");
    fitimage->headers()->AddHeader("CTYPE2","'DEC--TAN'","Name of coordinate (Dec, gnomonic projection)");

    //Píxel de referencia en la mitad de la imagen
    fitimage->headers()->AddHeader("CRPIX1",ref_x,"the x location of the reference pixel");
    fitimage->headers()->AddHeader("CRPIX2",ref_y,"the y location of the reference pixel");

    //Unidades. El entrecomillado es neceario para programas que usan WCSlib
    fitimage->headers()->AddHeader("CUNIT1","'deg'","Unit of 1st axis");
    fitimage->headers()->AddHeader("CUNIT2","'deg'","Unit of 2nd axis");

    //Píxel de referencia en grados
    fitimage->headers()->AddHeader("CRVAL1",ra * HOR2DEG ,QString("RA at reference point: %1").arg(Util::RA2String(ra)));
    fitimage->headers()->AddHeader("CRVAL2",dec ,QString("DEC at reference point: %1").arg(Util::DEC2String(dec)));

    fitimage->headers()->AddHeader("CD1_1",-pc->a() * RAD2DEG,"partial of first axis coord w.r.t. x");
    fitimage->headers()->AddHeader("CD1_2",-pc->b() * RAD2DEG,"partial of first axis coord w.r.t. y");
    fitimage->headers()->AddHeader("CD2_1",pc->d() * RAD2DEG,"partial of second axis coord w.r.t. x");
    fitimage->headers()->AddHeader("CD2_2",pc->e() * RAD2DEG,"partial of second axis coord w.r.t. y");

    //Adicionalmente añadiremos EQUINOX. Hay algunos catálogos que no son J2000
    fitimage->headers()->AddHeader("EQUINOX",QString().sprintf("%0.1f",pc->equinox()),"equinox of plate constants coordinates");

    //------------------------------------------------------------------------------------------------------------
}

bool AstrometryComputer::SaveReferenceStars(PlateConstants* plate, QString output_filename, QString input_filename)
{
    INFO("Saving reference stars to: %s", PTQ(output_filename));
    QFile fil(output_filename);
    if (!fil.open(QFile::WriteOnly))
    {
        ERR("Unable to open: %s", PTQ(output_filename));
        return false;
    }

    QTextStream str(&fil);
    if (input_filename.length())
    str << "#Astrometric reduction results for: " << QFileInfo(input_filename).fileName() << "\n";
    str << "#Reference stars: " <<  QString("").sprintf("%3d ", plate->ctrlcount()) << "\n";
    str << "#Residual avg:    " <<  QString("").sprintf("%7.4f\" ", plate->deltaavg()) << "\n";
    str << "#Residual stdev:  " <<  QString("").sprintf("%7.4f\" ", plate->deltastdev()) << "\n";
    str << "#-------------------------------------------------------------------------------------------------------------\n";
    str << "#ID                 Source ID" << "\n";
    str << "#IMAGE_X, IMAGE_Y   Image coordinates" << "\n";
    str << "#CAT_RA, CAT_DEC    Equatorial coordinates from catalog (RA/hours, DEC/degrees)" << "\n";
    str << "#RED_RA, RED_DEC    Equatorial coordinates from reduction (RA/hours, DEC/degrees)" << "\n";
    str << "#RES                Residual. Distance between catalog and astrometric reduction for this source. (arcseconds)" << "\n";
    str << "#-------------------------------------------------------------------------------------------------------------\n";
    str << "#ID    IMAGE_X   IMAGE_Y         CAT_RA        CAT_DEC         RED_RA        RED_DEC     RES\n";
    int index = 1;
    for (int i=0; i<plate->ctrlpoints()->count(); i++)
    {
        PlateCtrlPoint* ctrlpoint = plate->ctrlpoints()->data()+i;
        if (plate->rejected(ctrlpoint->x, ctrlpoint->y)) continue;
        double img_ra, img_dec;
        plate->map(ctrlpoint->x, ctrlpoint->y, &img_ra, &img_dec);

        //POS
        str << QString("").sprintf("%4d ", index);
        //IMG_X
        str << QString("").sprintf("%9.2f ", ctrlpoint->x);
        //IMG_Y
        str << QString("").sprintf("%9.2f ", ctrlpoint->y);
        //CATALOG_RA
        str << QString("").sprintf("%14.10f ", ctrlpoint->ra * DEG2HOR);
        //CATALOG_DEC
        str << QString("").sprintf("%14.10f ", ctrlpoint->dec);
        //REDUCTION_RA
        str << QString("").sprintf("%14.10f ", img_ra);
        //REDUCTION_DEC
        str << QString("").sprintf("%14.10f ", img_dec);
        //Residuo en segundos de arco
        str << QString("").sprintf("%7.4f ", Util::Adist(ctrlpoint->ra * DEG2HOR, ctrlpoint->dec, img_ra, img_dec)*3600);
        //Salto de línea final
        str << "\n";
        index++;
    }

    fil.close();
    return true;
}


double AstrometryComputer::ComputeMagZeroP(QString band, Matcher* mcher, StarDetector* det_stars, VizierReader* cat_stars)
{
    if (band.length() == 1)
        band = band + "mag";
    QVector<double> zps;
    for (int i=0; i<mcher->matchcoin()->count(); i++)
    {
        MatchCoincidence mc = mcher->matchcoin()->at(i);
        double mag_cat = cat_stars->stars()->at(mc.src_cat).mag(band);
        if (IS_INVALIDF(mag_cat)) continue;
        double flux_img = det_stars->stars()[mc.src_det].fluxISO();
        double zp = mag_cat + 2.5 * log10(flux_img);
        zps.append(zp);
    }
    if (!zps.count()) return INVALID_FLOAT;
    double avg;
    int idx_bigger;
    //Calcular la media y desviación típica
    avg = 0;
    for (int i=0; i<zps.count(); i++)
        avg += zps[i];
    avg /= zps.count();
    double stdev = 0;
    for (int i=0; i<zps.count(); i++)
        stdev += POW2(zps[i] - avg);
    stdev = sqrt(stdev / zps.count());
    //Buscar aquellos que se desvíen 3sigma
    double level = 2*stdev;
    idx_bigger = -1;
    for (int i=0; i<zps.count(); i++)
    {
        if (fabs(zps[i] - avg) > level)
        {
            zps.removeAt(i);
            i--;
        }
    }
    if (!zps.count()) return INVALID_FLOAT;
    //Volver a calcular la media
    avg = 0;
    for (int i=0; i<zps.count(); i++)
        avg += zps[i];
    avg /= zps.count();
    return avg;
}




















