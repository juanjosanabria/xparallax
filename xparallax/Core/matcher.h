#ifndef MATCHER_H
#define MATCHER_H

#include "core_global.h"
#include "core.h"
#include "progress.h"
#include "vo.h"
#include "detection.h"
#include "projection.h"
#include "matcher.h"

/**
 * @brief The Transform2D class Representa una transformación 2D de rotación, traslación y escalado.
 * De esta clase heredarán la transformación ER(Escala-rotación) basada en 2 puntos y la
 * transformación LS(Least-squares) basada en 4 puntos y calculada por mínimos cuadrados.
 */
class CORESHARED_EXPORT Transform2D {
public:
    Transform2D();
    virtual ~Transform2D();
    virtual void reset() = 0;                                           //Resetea la transformación
    virtual void map(double x, double y, double *rx, double* ry) = 0;   //Transformación directa A-->B
    virtual void imap(double x, double y, double *rx, double* ry) = 0;  //Transformación inversa B-->A
    virtual double theta() = 0;                                         //Devuelve el ángulo de rotación

protected:

};

/**
 * @brief The Transform2D_ER class Transformación 2D basada solamente en escala-rotación.
 * se inicializa con 2 puntos de datos nada más.
 */
class CORESHARED_EXPORT Transform2D_ER : public Transform2D{

public:
    void reset();
    void rotate(double angle_deg);
    void translate(double tx, double ty);

    /**
     * @brief map   Transformación directa. Rotación + traslación + escalado (en este orden)
     * @param x     Coordenada x original
     * @param y     Coordenada y original
     * @param rx    Coordenada resultante x
     * @param ry    Coordenada resultante y
     */
    void map(double x, double y, double *rx, double* ry);

    /**
     * @brief imap  Transformación inversa. desescalado + destraslación + desrotación
     * @param x     Coordenada x original
     * @param y     Coordenada y original
     * @param rx    Coordenada resultante x
     * @param ry    Coordenada resultante y
     */
    void imap(double x, double y, double *rx, double* ry);

    void scale(double sx, double sy);
    void scale(double s);
    QPointF map(QPointF p);
    QPointF imap(QPointF p);
    inline double getScale(){return m_scale_x;}
    inline double theta(){return m_theta;}
    inline double transX(){return m_trans_x;}
    inline double transY(){return m_trans_y;}

    static double       VectorAngle(double v1x, double v1y,double v2x,double v2y);
    static double       VectorAngle(double x1, double y1,double x2,double y2,double x3, double y3,double x4,double y4);
    /**
     * @brief VectorAngle Cálculo del ángulo en sentido antihorario entre dos vectores. de 0 a 360 en dirección de a -> b
     * @param v1x
     * @param v1y
     * @param v2x
     * @param v2y
     * @return
     */
    static double       VectorAngle_AH(double v1x, double v1y,double v2x,double v2y);
    /**
     * @brief VectorAngle Cálculo del ángulo en sentido antihorario entre 2 vectores definidos por 2 puntos. de 0 a 360 en dirección de a -> b
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     * @param x4
     * @param y4
     * @return
     */
    static double       VectorAngle_AH(double x1, double y1,double x2,double y2,double x3, double y3,double x4,double y4);

    static void            ComputeTransform(double a1x, double a1y, double a2x, double a2y,double b1x, double b1y, double b2x, double b2y, Transform2D_ER* tf);
    static Transform2D_ER  ComputeTransform(double a1x, double a1y, double a2x, double a2y,double b1x, double b1y, double b2x, double b2y);

private:
    double  m_scale_x;
    double  m_scale_y;
    double  m_theta;
    double  m_cos_theta;
    double  m_sin_theta;
    double  m_trans_x;
    double  m_trans_y;

};

/**
 * @brief The Transform2DLS class Transformación 2D por mínimos cuadrados.
 * Análoga a PlateConstants. Se inicializa con al menos 4 puntos de datos
 * x' = A*x + B*y + C
 * y' = D*x + D*y + F
 */
class CORESHARED_EXPORT Transform2D_LS : public Transform2D{

public:
    inline double a(){return A;}
    inline double b(){return B;}
    inline double c(){return C;}
    inline double d(){return D;}
    inline double e(){return E;}
    inline double f(){return F;}
    inline double rot(){return ROT;}                    //Rotación con respecto al norte

    void reset();
    void addCtrlPoint(QPointF& a, QPointF& b);
    void addCtrlPoint(double ax, double ay, double bx, double by);
    void map(double x, double y, double *rx, double* ry);
    void imap(double x, double y, double *rx, double* ry);
    double theta();
    void Compute();                                       //Calcula la transformación
    void initialize(double a, double b, double c, double d, double e, double f);
    int ctrlPointCount(){return mListA.count();}

private:
    QList<QPointF>  mListA;                         //Lista de puntos del conjunto de partida
    QList<QPointF>  mListB;                         //Lista de puntos del conjunto de destino

    //Constantes de transformación directa
    double A;
    double B;
    double C;
    double D;
    double E;
    double F;

    double ROT;             //Rotación

    //Constantes para la transformación inversa (post-calculadas)
    double G;
    double H;
    double I;
    double J;
    double K;
    double L;

    void computeInverse();
    static void Lsqfit(FMatrix* A,int N, int M, double* S);
};

class CORESHARED_EXPORT MatchParameters
        : public Parameters
{
public:
    enum PMTDateType{ PMDATE_NO_CORRECTION, PMDATE_FROM_FIT, PMDATE_FIXED };

public:
    MatchParameters();
    ~MatchParameters();
    void SetDefaults();
    void Save(QSettings* settings,const QString& section);
    void Load(QSettings* settings,const QString& section);
    void ComputeScale();

    //Información de la imagen
    QDate           date_frame;           //Fecha en la que se tomó la imagen para corrección de movimientos propios
    PMTDateType     pm_correct;           //Tipo de corrección de fecha y hora
    int             image_width;
    int             image_height;
    int             image_invalid_pix;    //Número de píxeles inválidos dentro de la imagen (para el cálculo de áreas)
    float           fov_radius_deg;       //Campo de visión en grados

    //Información de la imagen solicitada o calculada
    float     center_ra;
    float     center_dec;
    float     pixel_size_um;        // Si alguna de estas dos es NAN se usa la de abajo y viceversa
    int       binning;              // Binning de píxel
    float     focal_length_mm;      // Si alguna de estas dos es NAN se usa la de abajo y viceversa
    float     focal_reducer;        // Indica si hay algún tipo de reductor de focal
    float     pixel_scale_arcsec;   // Si esta es NAN se usa la de arriba
    bool      use_scale_by_fl_ps;   // Indica si usar la escala basada en longitud focal y pixel size
    bool      flip_field;           // Indica si habrá que hacer un flip del campo antes de machear

    //Intoducido nueva versión
    float     rotation;             //INTRODUCIDO NUEVO: Rotación de la imagen en grados
    float     rotation_tolerance;   //INTRODUCIDO NUEVO: Tolerancia de la rotación en grados

    //Catálogo y matching
    QString   vizier_catalog;       // Catálogo que se usará
    QString   vizier_server;
    float     mag_limit;            // Límite de magnitud para dese
    int       preferred_center;     // Centro preferido

    //Internas del matcher
    float     dist_tolerance;       // Tolerancia en distancia entre dos estrellas macheadas
    float     fov_extension;        // Indica en tanto por uno cuánto se extenderá el campo para descargar del catálogo
    bool      scale_filter;
    float     scale_tolerance;      // Tolerancia de escala
    bool      mag_filter;
    float     mag_tolerance;        // Tolerancia de magnitud
    int       num_align_stars;      // Número de estrellas a seleccionar de la imagen

};

struct MatchStar
{
    int            src_id;
    float          x;
    float          y;
    float          flux;
};

struct MatchCoincidence{
     float a_x;
     float a_y;
     double ra;
     double dec;
     float b_x;             //Posición de la estrella de catálogo
     float b_y;
     int src_cat;           //Identificador o índice en el catálogo descargado
     int src_det;
     bool rejected;
};

class CORESHARED_EXPORT Matcher
        : public BackgroundWorker
{
public:
    Matcher();
    ~Matcher();
    inline void setParameters(const MatchParameters& par){ mParameters = par; ComputeProjection();}
    inline MatchParameters* parameters(){return &mParameters;}
    inline int matchCount(){return mMatchCount;}
    inline double rms(){return mRMS;}
    inline bool success(){return mMatchSuccess;}
    inline int  progress(){return mProgress;}
    inline double matchProb(){return mMatchProb;}
    //virtual MatchResult Match(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris);
    void Clear();

    //Métodos provisionales que habrá que cambiar para no ser tan brutos
    QVector<MatchStar>*          matchdet(){return &mMatchDet;}
    QVector<MatchStar>*          matchcat(){return &mMatchCat;}
    QVector<MatchCoincidence>*   matchcoin(){return &mCoincidences;}  //Estrellas que coinciden
    Transform2D_ER*              transform(){return &mTransform_ER;}
    MatchStar*                   candidatea1(){return mCandidateA1;}
    MatchStar*                   candidatea2(){return mCandidateA2;}
    MatchStar*                   candidateb1(){return mCandidateB1;}
    MatchStar*                   candidateb2(){return mCandidateB2;}

    //Acceso a los catálogos fuentes de estrellas
    VizierRecord                getSourceStarCat(int id) const {return mSourceStarsCat.at(id);}
    DetectedStar                getSourceStarDet(int id) const {return mSourceStarsDet.at(id);}
    //Candidatas para usar en plate constants


protected:
    MatchParameters         mParameters;
    GnomonicProjection      mProjection;
    Transform2D_ER          mTransform_ER;
    Transform2D_LS          mTransform_LS;
    double                  mEpochDiff;
    int                     mMatchCount;
    //Match final
    MatchStar*              mCandidateA1;
    MatchStar*              mCandidateA2;
    MatchStar*              mCandidateB1;
    MatchStar*              mCandidateB2;
    //Mejor match hasta el momento
    QVector<MatchStar>      mBestMatch;
    int                     mProgress;
    double                  mMatchProb;         //Probabilidad de que el match se haya producido al azar.

    //Para calcular las condiciones de parada
    double                          mRMS;
    float                           mCoinMinX;
    float                           mCoinMinY;
    float                           mCoinMaxX;
    float                           mCoinMaxY;
    bool                            mMatchSuccess;

    //Estrellas de origen
    QList<DetectedStar>             mSourceStarsDet;
    QList<VizierRecord>             mSourceStarsCat;

    //Conjunto de estrellas para machear (proyectadas con centro y escala en el caso de las de catálogo)
    QVector<MatchStar>              mMatchDet;
    QVector<MatchStar>              mMatchCat;
    QVector<MatchCoincidence>       mCoincidences;

    //Vectores solamente usados para el emparejado, contienen un subconjunto de las estrellas más brillantes
    QVector<MatchStar>              mPairDet;
    QVector<MatchStar>              mPairCat;

    QVector< QVector<MatchStar>* >  mMatchDetOrd;   //Lista ordenada por coordenada X


    int LoadCoincidences(int dist_tolerance, Transform2D* tr);
    void ConstructPlatePointList();
    void ProperMotinCorrection(VizierRecord* star);

    //Construye las listas de estrellas
    void ConstructLists(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris);
    void ConstructListsHeuristic(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris);
    void ConstructListFull(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris);
    void ConstructLists2(DetectedStar* stars, int stcount, QList<VizierRecord>* catentris);

    void ComputeProjection();
    //Buscar estrellas fuente por coordenadas x e y
    MatchStar*  getCatMatchStar(double ra, double lat);
    MatchStar*  getDetMatchStar(double x, double y);
    MatchStar*  getMatchStarInList(double x, double y, QVector<MatchStar>* list);
    //Buscar estrellas de origen por coordenada x e y
    VizierRecord getCatSrcStar(double x, double y);
    DetectedStar getDetSrcStar(double x, double y);
    double  probAlign(int cat_on_image, int detecteds, int align_stars_det,int align_stars_cat, int coincidences);
    bool    stopCondition(int test_case=0);
    bool    stopConditionLessStars(int needed_stars);
    bool    stopConditionManyStars(int needed_stars);
    bool    stopConditionMuchMoreStars(int needed_stars);
    bool    stopEndMagnitudeCorr();
    //Cuenta cuántas estrellas de catálogo caen dentro de la imagen
    int     catStarsInImage(Transform2D* t2d);
    //Calcula el área de intersección (en píxeles entre el catálogo y la imagen)
    double  intersectPercent(Transform2D* t2d);

    void SetSuccess(MatchStar* a1,MatchStar* a2, MatchStar* b1, MatchStar* b2, QVector<MatchStar>* _det_s, QVector<MatchStar>* _cat_s);

public:
    //Distancia angular en GRADOS
    static double AngularDistance(double al1,double del1,double al2,double del2);

private:
    static bool DetectedStar_greaterThan(DetectedStar a, DetectedStar b);
    static bool VizierRecord_greaterThan(VizierRecord a, VizierRecord b);
    static bool DetectedStar_OrderX(MatchStar a, MatchStar b);


public:
    QVector<MatchStar> MatchBruteForce(QVector<MatchStar>* det_s, QVector<MatchStar>* cat_s);
    int MatchCount(QVector<MatchStar>* _det_s, QVector<MatchStar>* _cat_s, Transform2D* tr);
    int MatchCount2(QVector<MatchStar>* _det_s, QVector<MatchStar>* _cat_s, Transform2D* tr, Transform2D_LS* ls = NULL);
    bool Match(DetectedStar* stars,int stcount, QList<VizierRecord>* catentri);
    bool MatchPair(DetectedStar* stars, int stcount, QList<VizierRecord>* catentri,
                            float a_x, float a_y, QString a_ra, QString a_dec,
                            float b_x, float b_y, QString b_ra, QString b_dec);
    int TestMatch(MatchStar* a1, MatchStar* a2, MatchStar* b1, MatchStar* b2,QVector<MatchStar>* _det_s, QVector<MatchStar>* _cat_s);
    void TestMyStars(double x1, double y1, QString _ra1,QString _dec1,double x2, double y2, QString _ra2,QString _dec2);

};












#endif // MATCHER_H
