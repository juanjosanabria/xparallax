#ifndef ERROR_H
#define ERROR_H

#define ERR_NO_ERROR                        0
//----------------------------------------------------------------------------------------------------
//Global errors: 0-20 based
//----------------------------------------------------------------------------------------------------
#define ERR_UNABLE_OPEN_INPUT_FILE          1
#define ERR_UNABLE_WRITE_OUTPUT_FILE        2
#define ERR_NETWORK                         3
#define ERR_INVALID_INPUT_PARAMETERS        4
#define ERR_INI_FILE                        5
#define ERR_INVALID_LICENSE                 6
#define ERR_OS_SYSTEM_SEMAPHORE             7
#define ERR_NUMBER_OF_PROCESSED             8
//----------------------------------------------------------------------------------------------------
//Astrometry errors: 10-based
//----------------------------------------------------------------------------------------------------
#define ERR_AST_MATCH_NOT_FOUND             10
#define ERR_AST_NOT_ENOUGH_STARS            11
#define ERR_AST_INVALID_CATLOG_INFORMATION  12
#define ERR_AST_INVALID_IMAGE_HEADER        13  //Falta alguna cabecera (RA-DEC)
//----------------------------------------------------------------------------------------------------
//Trail detection errors: 20-based
//----------------------------------------------------------------------------------------------------
#define ERR_TRAIL_NO_DETECTED               20  //No se ha detectado ningún trail
#define ERR_TRAIL_NO_ENDS_DETECTED          21  //No se ha detectado correctamente algún extremo del trail
#define ERR_TRAIL_BAD_FORMAT_FIELD          22  //Los campos no están correctamente formateados
#define ERR_NO_PLATE_CONSTANTS              23  //No hay astroetría para la imagen
//----------------------------------------------------------------------------------------------------
//Image calibration errors: 30-based
//----------------------------------------------------------------------------------------------------
#define ERR_CAL_NO_BIAS_FRAME               30  //No se ha detectado ningún trail
#define ERR_CAL_NO_FLAT_FRAME               31  //No se ha detectado correctamente algún extremo del trail
//----------------------------------------------------------------------------------------------------
//License problems:         40-based
//----------------------------------------------------------------------------------------------------
#define ERR_NO_ID_FILE                      40  //No existe el archivo de identidad
#define ERR_NOT_VALID_LICENSE               41  //No existe una licencia válida
#define ERR_NETWORK_ERROR_DOWNLOADING_LIC   42  //No se pueden descargar las licencias
#define ERR_LIC_EXCEEDED                    43  //Se ha excedido el límite de esta licencia

#endif // ERROR_H
