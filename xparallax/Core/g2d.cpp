#include "g2d.h"


G2D_Line::G2D_Line(double m, double b)
{
    init(m, b);
}

G2D_Line::G2D_Line(QPointF pa, QPointF pb)
{
    init(pa, pb);
}

void G2D_Line::init(QPointF pa, QPointF pb)
{
    mRCoeff=0;
    //Caso degenerado. Recta vertical
    if (!COMPF2(pa.x(), pb.x()))
    {
       this->mM = this->mB = INVALID_FLOAT;
       mp = 0; bp = pa.x();
       initc(mp, bp);
    }
    else
    {
        this->mM = (pb.y()-pa.y())/(pb.x()-pa.x());
        this->mB = pa.y() - mM*pa.x();
        init(this->mM, this->mB);
    }
}

void G2D_Line::init(double m, double b)
{
    this->mM = m; this->mB = b;
    this->mp = -1/m; this->bp=1/m*b;
}

void G2D_Line::initc(double mp, double bp)
{
    this->mp = mp; this->bp = bp;
    this->mM=-1/mp; this->mB=1/mp*bp;
}

void G2D_Line::map(double x, double* y)
{
    if (isVertical())
        *y = INVALID_FLOAT;
    else
        *y = this->mM*x + this->mB;
}

void G2D_Line::mapc(double y, double *x)
{
    if (isHorizontal())
        *x = INVALID_FLOAT;
    else
        *x = this->mp*y + this->bp;
}

G2D_Line G2D_Line::perpendicular(QPointF p)
{
    G2D_Line per;
    double pm = -1/mM;
    double pb = p.y() - pm*p.x();
    per.init(pm, pb);
    return per;
}

QVector<QPointF> G2D_Line::pointFarFrom(QPointF p, double d)
{
    QVector<QPointF> vec;

    if (isVertical())
    {
        vec.append(QPointF(p.x() + d, p.y()));
        vec.append(QPointF(p.x() - d, p.y()));
    }
    else if (isHorizontal())
    {
        vec.append(QPointF(p.x(), p.y()+d));
        vec.append(QPointF(p.x(), p.y()-d));
    }
    else
    {
        //Pendiente  y ordenada en el origen de la recta perpendicular
        G2D_Line per = perpendicular(p);
        double m = per.m();
        double b = per.b();
        double m2 = m*m; double d2 = d*d;
        double px1 = (sqrt(d2*(m2+1))+m2*p.x()+p.x())/(m2+1);
        double px2 = (-sqrt(d2*(m2+1))+m2*p.x()+p.x())/(m2+1);
        vec.append(QPointF(px1, m*px1+b));
        vec.append(QPointF(px2, m*px2+b));
       // double dist = CART_DIST(p.x(),p.y(),vec[0].x(),vec[0].y());
       // double dist2 = CART_DIST(p.x(),p.y(),vec[0].x(),vec[0].y());
    }

    return vec;
}


double G2D_Line::distance(QPointF p)
{
   double a = mM;
   double b = -1;
   double c = mB;
   if (isHorizontal()) return fabs(p.y() - mB);
   else if (isVertical()) return fabs(p.x() - bp);
   else
       return fabs(a*p.x() + b*p.y() + c)/sqrt(a*a+b*b);
}



bool G2D_Line::IntersectsSegment(QPointF p1, QPointF p2)
{
    G2D_Line line2(p1, p2);
    if (isHorizontal()) return this->b() == line2.b();
    if (isVertical()) return this->bp = line2.bp;
    //Calculo el punto de intersección
    double sx = (line2.b() - b())/(m() - line2.m());
    double x1 = MINVAL(p1.x(),p2.x());
    double x2 = MAXVAL(p1.x(), p2.x());
    return sx >= x1 && sx <= x2;
}












