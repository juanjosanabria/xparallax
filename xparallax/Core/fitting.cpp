#include "fitting.h"
#include "core.h"
#include "core_def.h"
#include "ap.h"
#include "interpolation.h"

using namespace alglib;

//f= d - ae(-(x-b)^2/c^2)
void Fit::gauss_1d(const real_1d_array &c, const real_1d_array &x, double &func, void * /* ptr */)
{
    // Esta función de callback calcula f = a*exp(-(x-b)^2/c^2) + d
    // donde x es una posición del eje X y c es el conjunto de constantes a,b,c,d
    func = GSS(c[0], c[1], c[2], c[3], x[0]);
}

void Fit::fit_gaussian_1d(double* x, double* y, int cont, double* a, double* b, double* c, double *d, double lambda, int it_count)
{
    //1- Calculamos valores aproximados de la gausiana (a,b,c,d)
    //--------------------------------------------------------------------------------
    //Calculamos el pico (a)
    double peak_x = x[0];
    double peak_y = y[0];
    int peak_index = 0;
    double min_val_y = FLOAT_MIN;
    for (int i=1; i<cont; i++)
    {
        if (y[i] > peak_y)
        {
            peak_x = x[i];
            peak_y = y[i];
            peak_index = i;
        }
        if (y[i] < min_val_y)  min_val_y = y[i];
    }

    //Calculamos el flujo total
    double total_flux = 0;
    for (int i=1; i<cont; i++) total_flux += y[i];



    //Calculamos la desviación típica como el ancho que contiene el 68.2% del flujo (aproximado)
    int spindex = 0; // (anchura alrededor de la media)
    double pflux;
    do
    {
        spindex++;
        pflux = 0;
        for (int i=peak_index-spindex; i<= peak_index+spindex; i++)
        {
            if (i >= 0 && i < cont)
                pflux += y[i];
        }
    } while (pflux < 0.682*total_flux && peak_index-spindex >= 0 && peak_index+spindex<cont);
    if (peak_index-spindex < 0) spindex = peak_index;
    if (peak_index+spindex >= cont) spindex = peak_index-cont-1;
    double stdev = ((x[peak_index]-x[peak_index-spindex])+(x[peak_index+spindex]-x[peak_index]))/2;

    //Primera inicialización de valores (que no vale para mucho)
    *a = peak_y; *b = peak_x; *c = stdev; *d = min_val_y;


    real_2d_array _x;               //Array con coordenadas X
    real_1d_array _y;               //Array con valores observacionales
    real_1d_array _c;               //Array con las constantes a calcular

    _x.setcontent(cont, 1, x);
    _y.setcontent(cont, y);


    double epsf = 0;
    double epsx = 0.000001;
    ae_int_t maxits = 0;
    ae_int_t info;
    lsfitstate state;
    lsfitreport rep;
    double diffstep = lambda;

    //Inicializar el resultado aproximado inicial
    double vc[4] = {*a, *b, *c, *d};
    _c.setcontent(4, vc);


    lsfitcreatef(_x, _y, _c, diffstep, state);
    lsfitsetcond(state, epsf, epsx, maxits);
    alglib::lsfitfit(state, gauss_1d);
    lsfitresults(state, info, _c, rep);

    *a = _c.getcontent()[0];
    *b = _c.getcontent()[1];
    *c = _c.getcontent()[2];
    *d = _c.getcontent()[3];
}



double Fit::mean(double* v, int count)
{
    double mn=0;
    for (int i=0;i<count;i++)
        mn += v[i];
    return mn/(double)count;
}

double Fit::stdv(double* v, int count)
{
    double mn = mean(v,count);
    double st=0;
    for (int i=0;i<count;i++)
        st += POW2(mn - v[i]);
    return sqrt(st/count);
}
















