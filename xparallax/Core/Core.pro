#-------------------------------------------------
#
# Project created by QtCreator 2013-07-19T10:05:38
#
# Para un listado de variables disponibles:
# http://doc.qt.io/qt-5/qmake-variable-reference.html
#-------------------------------------------------

QT       += core network xml concurrent

TARGET = Core
TEMPLATE = lib
DEFINES += CORE_LIBRARY
DEFINES +=_CRT_SECURE_NO_WARNINGS
CONFIG += openssl-linked

DEPENDPATH += . ../Algebra
INCLUDEPATH +=  ../Algebra

DEPENDPATH += . ../Novas
INCLUDEPATH +=  ../Novas

mingw:{
QMAKE_CXXFLAGS += -std=gnu++0x
}

#Optimizaciones de paralelización para MSVC
release:{
   # QMAKE_CXXFLAGS += -Qpar
   # QMAKE_CXXFLAGS += -Qvec-report:1
}


win32:release {LIBS +=  "-L$$OUT_PWD/../Algebra/Release" -lAlgebra}
win32:debug   {LIBS +=  "-L$$OUT_PWD/../Algebra/Debug" -lAlgebra}
win32:release {LIBS +=  "-L$$OUT_PWD/../Novas/Release" -lNovas}
win32:debug   {LIBS +=  "-L$$OUT_PWD/../Novas/Debug" -lNovas}

linux:{
    CONFIG += static
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
    QMAKE_CXXFLAGS += -std=c++11
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN/lib
}

macx:{
    CONFIG += static
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -stdlib=libc++ -mmacosx-version-min=10.7
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
}


SOURCES += \
    fitimage.cpp \
    core.cpp \
    calibration.cpp \
    background.cpp \
    progress.cpp \
    xglobal.cpp \
    detection.cpp \
    vo.cpp \
    util.cpp \
    matcher.cpp \
    projection.cpp \
    plateconstants.cpp \
    astrometry.cpp \
    log.cpp \
    mpc.cpp \
    geometry.cpp \
    xstringbuilder.cpp \
    catalog.cpp \
    g2d.cpp \
    profiling.cpp \
    license.cpp


HEADERS +=\
        core_global.h \
    fitimage.h \
    core.h \
    calibration.h \
    background.h \
    progress.h \
    internal.h \
    xglobal.h \
    detection.h \
    vo.h \
    util.h \
    matcher.h \
    projection.h \
    plateconstants.h \
    astrometry.h \
    log.h \
    mpc.h \
    msvc.h \
    core_def.h \
    geometry.h \
    xstringbuilder.h \
    catalog.h \
    error.h \
    g2d.h \
    profiling.h \
    license.h


unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

FORMS +=

RESOURCES += \
    res_core.qrc

OTHER_FILES += \
    sex/Makefile.in \
    sex/Makefile.am
