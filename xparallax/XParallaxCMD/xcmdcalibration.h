#ifndef XCMDCALIBRATION_H
#define XCMDCALIBRATION_H

#include <QObject>
#include <QCoreApplication>
#include "xcmd.h"
#include "fitimage.h"
#include "util.h"
#include "calibration.h"


class XCmdCalibration : public XCmd
{

    Q_OBJECT
public:
    explicit XCmdCalibration(QCoreApplication *parent);
    ~XCmdCalibration();

protected:
    virtual QString helpRes();
    virtual QString iniEx();

private:
    int                     readCmdLine();
    QString                 mIniFile;
    QString                 mInput;              //Imagenes fit de entrada
    QStringList             mInputFiles;
    QString                 mOutputDir;          //Directorio de salida
    CalibrationParameters*  mCalPar;
    CalibrationList*        mCalList;

signals:

public slots:
    void run();

};

#endif // XCMDCALIBRATION_H
