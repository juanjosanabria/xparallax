#include "xexception.h"

XException::XException(const QString msg)
{
    mMsg = msg;
    mErr = 0;
}

XException::XException(const QString msg, int /* err */)
{
    mMsg = msg;
    mErr = errno;
}
