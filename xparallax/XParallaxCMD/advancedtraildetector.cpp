#include "advancedtraildetector.h"
#include "g2d.h"
#include "util.h"
#include "detection.h"
#include "core.h"
#include "core_def.h"
#include "vo.h"
#include "xglobal.h"
#include "plateconstants.h"
#include "robusfit.h"
#include "geometry.h"
#include "ap.h"
#include "interpolation.h"
#include "statistics.h"
#include "mpc.h"
#include "superfit.h"
#include "superfit2.h"
#include <QDir>
#include <QImage>
#include <QPainter>
#include <QThread>

//Comparación de semejanza de dos floats. Diferencia partida por la suma
//Dará valor entre cero y 1
#define CO_COMP(a,b)        (fabs((a)-(b))/((a)+(b)))

#define ATD_HISTO_SIZE  32768

#define MIN_PEARSON_1  0.7
#define MIN_SPEARMAN_1 0.8

#define C_PEARSON_2  0.89
#define C_SPEARMAN_2 0.98

//Selecciona el coeficiente de spearman o pearson
#define RCOEFF(regr) (mPar.regr_coeff_spearman ? regr.s() : regr.r())

#define COEFF_OK(r,s) ((r)>C_PEARSON_2||(s)>C_SPEARMAN_2||((s)>MIN_PEARSON_1&&(s)>MIN_SPEARMAN_1))

//Obtiene un valor de la imagen mFitImage, comprobando que no se sale de límites
#define PVAL(x,y) (x >= 0 && x < mWidth && y>=0 && (y) < mHeight) ? mFitWithoutStars->data()[(y)*mWidth+(x)] : 0

#define DEF_snr_min                     1.03f
#define DEF_trail_est_length            0
#define DEF_trail_est_flank_size        30
#define DEF_trail_est_sig_flanks        10
#define DEF_bin                         4
#define DEF_detect_box_width            400
#define DEF_detect_box_height           400
#define DEF_rho_min                     0.1f
#define DEF_superfit                    true

#define DEF_crop_left                   50
#define DEF_crop_right                  0
#define DEF_crop_top                    0
#define DEF_crop_bottom                 0

#define DEF_trail_mid_height            11
#define DEF_sigma_units                 1.5f
#define DEF_slice_final_height          28
#define DEF_slice_iter_decrement        64
#define DEF_min_neighbours              3
#define DEF_min_area                    350
#define DEF_min_area_regr               10
#define DEF_regr_coeff_spearman         true
#define DEF_max_iters_extend_flanks     10


#define DEF_save_image_without_stars_fit    false
#define DEF_save_image_without_stars_img    true
#define DEF_save_overlays_iter_img          true
#define DEF_save_final_overlay_img          true
#define DEF_save_profile_img                true
#define DEF_save_fitting_data               false
#define DEF_save_regr_line_data             false

#define DEF_output_profile_img_format       "png"
#define DEF_output_overlay_img_format       "jpg"

void ATDParams::SetDefaults()
{
    //Parámetros de cálculo
    //---------------------------------------------
    snr_min = 1.03f;
    trail_est_length = 0;           //Longitud estimada de un trail
    trail_est_flank_size = 30;       //Número de píxeles máximos para ajustar flancos con longitudes estimadas
    trail_est_sig_flanks=10;        //Número de píxeles hacia adentro del flanco para calcular un nivel de señal
    bin = 4;                        //Bin para hacer en la imagen
    detect_box_width = 400;         //Tamaño de la caja de detección
    detect_box_height = 400;
    rho_min = 0.1f;                  //Mínimo coeficiente de spearman en el perfil de la traza
    superfit = true;

    //Recorte de la imagen
    crop_left   = 50;
    crop_right  = 0;
    crop_top    = 0;
    crop_bottom = 0;

    trail_mid_height = 11;          //Supongamos que un trail puede tener como máximo 20 píxeles de grosor
    sigma_units = 1.5f;             //Unidades de desviación típica
    slice_final_height = 28;        //Tamaño de la rodaja final en píxeles debería ser el doble que trail_mid_height
    slice_iter_decrement = 64;      //Decremento de cada iteración
    min_neighbours = 3;             //Mínimo número de vecinos para considerar un punto en conjunto
    min_area = 350;                 //Mínima área en píxeles para considerar un candidato
    min_area_regr = 10;             //Mímina área en píxeles para calcular una recta de regresión
    regr_coeff_spearman = true;     //Usar el coeficiente de regresión de spearman (1) o pearson (0)
    max_iters_extend_flanks = 10;   //Máximo número de iteraciones

    //Definir un área de trabajo de la imagen


    //Parámetros de debugueo
    //---------------------------------------------
    save_image_without_stars_fit = false;
    save_image_without_stars_img = true;
    save_overlays_iter_img = true;
    save_final_overlay_img = true;
    save_profile_img = true;
    save_fitting_data = false;
    save_regr_line_data = false;

    //Parámetros de formato y salida
    //---------------------------------------------
    output_profile_img_format = "png";
    output_overlay_img_format = "jpg";
}

void ATDParams::Save(QSettings* settings,const QString& section)
{

}

void ATDParams::Load(QSettings* settings,const QString& section)
{
    snr_min = ReadFloat(settings, section, "snr_min", 1.03f);
    bin = ReadInt(settings, section, "bin", 4);
    detect_box_width = ReadInt(settings, section, "detect_box_width", 400);
    detect_box_height = ReadInt(settings, section, "detect_box_height", 400);

    min_area = ReadInt(settings, section, "min_area", 350);
    min_neighbours = ReadInt(settings, section, "min_neighbours", 3);


    //Parámetros estimados
    trail_est_length  = ReadInt(settings, section, "trail_est_length", 0);
    trail_est_flank_size = ReadInt(settings, section, "trail_est_flank_size", 10);
    trail_est_sig_flanks = ReadInt(settings, section, "trail_est_sig_flanks", 15);
    rho_min = ReadFloat(settings, section, "rho_min", 0.1f);

    superfit = ReadBool(settings, section, "superfit", true);
}

void ATDBoundingBox::ComputeBoundingBox(QPointF p1, QPointF p2,int height)
{
    mP1=p1; mP2=p2; mHeight=height;
    mPoly = QPolygonF();
    G2D_Line ln(p1,p2);
    //Caja interna
    QVector<QPointF> pf1 = ln.pointFarFrom(p1, height/2.0);
    QVector<QPointF> pf2 = ln.pointFarFrom(p2, height/2.0);
    mPoly.append(pf1[0]);mPoly.append(pf2[0]);mPoly.append(pf2[1]);mPoly.append(pf1[1]);
    //Caja envolvente para calcular el fondo
    mExtPoly = QPolygon();
    pf1 = ln.pointFarFrom(p1,height);
    pf2 = ln.pointFarFrom(p2,height);
    mExtPoly.append(pf1[0]);mExtPoly.append(pf2[0]);mExtPoly.append(pf2[1]);mExtPoly.append(pf1[1]);
}

void ATDBoundingBox::showConsole()
{
    double a = foo()->length() >= 5 ? foo()->at(0) : INVALID_FLOAT;
    double b = foo()->length() >= 5 ? foo()->at(1) : INVALID_FLOAT;
    double c = foo()->length() >= 5 ? foo()->at(2) : INVALID_FLOAT;
    double d = foo()->length() >= 5 ? foo()->at(3) : INVALID_FLOAT;
    double e = foo()->length() >= 5 ? foo()->at(4) : INVALID_FLOAT;

    QString id = mId < 0 ? "" : QString().sprintf("%02d", mId);
    if (mReg.isNull())
        INFO("\t FITTING[%s]: Invalid fit. No data to fit", PTQ(id));
    else if (mPointCount > 0)
    INFO("\t FITTING[%s]: y = %0.3fx %c %0.3f ; r=%0.4f; s=%0.4f; rhoF=%0.4f; point-count=%d; P1=(%0.2f,%0.2f); P2=(%0.2f,%0.2f)",
         PTQ(id),
         mReg.m(), (mReg.b()>0?'+':'-'), fabs(mReg.b()), mReg.r(), mReg.s(),
         rhoF(),
         mPointCount,
         mP1.x(),mP1.y(),mP2.x(),mP2.y()
         );
    else
    INFO("\t FITTING[%s]: y = %0.3fx %c %0.3f ; r=%0.4f; s=%0.4f; P1=(%0.2f,%0.2f); P2=(%0.2f,%0.2f)",
             PTQ(id),
             regr().m(), (mReg.b()>0?'+':'-'), fabs(mReg.b()), mReg.r(), mReg.s(),
             mP1.x(),mP1.y(),mP2.x(),mP2.y()
         );

    if (!IS_INVALIDF(a))
    {
        INFO("\t\t FITT=> f(x) = %0.6f * sech(( (2(x-%0.6f))/%0.6f - 1)^%0.6f)^2 + %0.6f",
                 a, c, b, d, e);
        INFO("\t\t S/Nr = %0.6f", sNr());
    }
}

void ATDBoundingBox::showFoo()
{
    INFO("\tFitting: A=%0.4f; B=%0.4f; C=%0.4f; D=%0.4f; E=%0.4f; RHO=%0.4f; SNR=%0.4f;",
              foo()->at(0), foo()->at(1),
              foo()->at(2), foo()->at(3),
              foo()->at(4),
              rhoF(), sNr());
}

void ATDBoundingBox::foldBoxStats(int start_idx, int end_idx,double* _avg, double* _stdev,int* _point_count)
{
    int point_count = 0;
    double sum = 0;
    double sum2 = 0;
    if (start_idx < 0) start_idx = 0;
    if (end_idx >= foldBox()->length()) end_idx = foldBox()->length()-1;
    //Calcular estadísticas
    for (int i=start_idx; i<=end_idx; i++)
    {
        point_count++;
        sum += foldBox()->data()[i];
        sum2 += POW2(foldBox()->data()[i]);
    }
    double avg = sum / point_count;
    double stdv = sqrt(sum2/point_count - POW2(avg));
    //Retorno de valores
    if (_avg) *_avg = avg;
    if (_stdev) *_stdev = stdv;
    if (_point_count) *_point_count = point_count;
}

void ATDBoundingBox::computeSNr()
{
    int left_flank = (int)flanks()->at(0);
    int right_flank = (int)flanks()->at(1);
    int point_count1, point_count2, point_count3;

    double avg1 = 0, stdv1;
    foldBoxStats(0, left_flank, &avg1, &stdv1, &point_count1);
    double avg2, stdv2;
    foldBoxStats(left_flank, right_flank, &avg2, &stdv2, &point_count2);
    if (point_count2 < 5)
    {
        //No se puede calcular señal a ruido con estos valores
        mSNr = 1;
        return;
    }
    double avg3, stdv3;
    foldBoxStats(right_flank, foldBox()->length()-1, &avg3, &stdv3, &point_count3);

    double avg_bkg = (avg1+avg3)/2;
    if (avg_bkg < 0.1  || point_count1 + point_count3 < 10)
    {
        //No se puede calcular señal a ruido con estos valores
        mSNr = 1;
        return;
    }
    mSNr = avg2 / avg_bkg;
}

ATDDetector::ATDDetector(FitImage* fi)
{
    mIter = 0;
    mPar.SetDefaults();
    mSourceImage = mWorkImage = fi;
    mWidth = fi->width();
    mHeight = fi->height();
    mWorkingPath = QFileInfo(fi->filename()).dir().absolutePath() + "/trail_detection/" + QFileInfo(fi->filename()).baseName();
    //Limpiar el path de trabajo
    if (!QDir(mWorkingPath).exists()) QDir().mkpath(mWorkingPath);
    QStringList files = QDir(mWorkingPath).entryList();
    for (int i=0; i<files.length(); i++)
    {
        QString fname = files.at(i);
        if (fname.startsWith(".")) continue;
        fname = mWorkingPath + "/" + fname;
        QFile(fname).remove();
    }
    XGlobal::LOGGER->setOutputFile(GetOutputFilename("trail_detection","log"));
}


QString ATDDetector::GetOutputFilename(QString prefix, QString ext, bool for_new)
{
    QString fn = QString().sprintf("%s/%02d.00.%s.%s",
                             mWorkingPath.toLatin1().data(),
                             mIter,
                             prefix.toLatin1().data(),
                             ext.toLatin1().data());
    if (!for_new) return fn;
    for (int i=1; QFileInfo(fn).exists(); i++)
    {
        fn = QString().sprintf("%s/%02d.%02d.%s.%s",
                                     mWorkingPath.toLatin1().data(),
                                     mIter, i,
                                     prefix.toLatin1().data(),
                                     ext.toLatin1().data());
    }
    return fn;
}

bool ATDDetector::IsPointOfTrail(QPoint p1)
{
    for (int i=0; i<mPointAccepted.count(); i++)
    {
        QPoint p2 = mPointAccepted.at(i);
        if (p2 == p1) return true;
    }
    return false;
}

void ATDDetector::SaveOverlay(FitImage* fi, ATDBoundingBox& bb, bool color_points)
{
    if (!mPar.save_overlays_iter_img) return;
    if (bb.id() != -1) mIter = bb.id();
    QString filename = GetOutputFilename("overlay",mPar.output_overlay_img_format);
    QImage img = QImage(fi->width(), fi->height(), QImage::Format_RGB32);
    img.fill(Qt::darkGray);
    //Dibujar la imagen
    ConvergeProfile cp;
    cp.SetOptimumFor(fi);
    cp.CreateColorTable();
    cp.Converge(&img);
    //Crear el painter
    QPainter painter;
    painter.begin(&img);

    //Dibujar la caja
    QPen pen = QPen(QColor::fromRgb(255,0,0),1, Qt::DashLine);
    painter.setPen(pen);
    QPolygonF poly = bb.polygon(); poly.append(poly[0]);
    painter.drawPolyline(poly);
    //Linea media de la caja
    painter.drawLine(bb.p1(),bb.p2());

    //Dibujar la caja 2
    poly = bb.polygonEX(); poly.append(poly[0]);
    painter.drawPolyline(poly);

    //Dibujar la recta de regresión. Obtengo el punto mas a la izquierda y el mas a la derecha
    if (!bb.regr().isNull())
    {
        QPointF p_left = poly[0], p_right = poly[1];
        for (int i=0; i<poly.length(); i++)
        {
            if (poly[i].x() < p_left.x()) p_left = poly[i];
            if (poly[i].x() > p_right.x()) p_right = poly[i];
        }
        G2D_Line line = bb.regr();
        double y1, y2;
        line.map(p_left.x(), &y1);
        line.map(p_right.x(), &y2);
        painter.setPen(QPen(QColor::fromRgb(0,255,0),1, Qt::DashLine));
        painter.drawLine(QPointF(p_left.x(), y1),QPointF(p_right.x(), y2));
    }
    //Colorear los puntos
    painter.setPen(QPen(QColor::fromRgb(0,255,255),1));
    for (int i=0; i<mPointAccepted.count()&&color_points;i++)
        painter.drawLine(mPointAccepted.at(i), mPointAccepted.at(i));
    //Guardar la imagen
    painter.end();
    img.save(filename);
}

void ATDDetector::SaveFinalOverlay(const QVector<ATDBoundingBox> &boxes)
{
    if (!mPar.save_final_overlay_img) return;
    QString filename = GetOutputFilename("final_overlay",mPar.output_overlay_img_format);
    QImage img = QImage(mWidth, mHeight, QImage::Format_RGB32);
    img.fill(Qt::darkGray);
    //Dibujar la imagen
    ConvergeProfile cp;
    cp.SetOptimumFor(mWorkImage);
    cp.CreateColorTable();
    cp.Converge(&img);
    //Crear el painter
    QPainter painter;
    painter.begin(&img);

    //Dibujar la caja
    int cross_sz = 20;
    QPen pen_dash = QPen(Qt::green,1.5, Qt::DashLine);
    QPen pen_solid = QPen(Qt::red,3, Qt::SolidLine);
    for (int i=0; i<boxes.length(); i++)
    {
        painter.setPen(pen_dash);
        ATDBoundingBox bb = boxes[i];
        QPolygonF poly = bb.polygon(); poly.append(poly[0]);
        painter.drawPolyline(poly);
        //Dibujar la cruz central
        painter.setPen(pen_solid);
        double cx, cy;
        bb.transform()->imap((bb.flanks()->data()[1] + bb.flanks()->data()[0])/2.0, bb.height()/2.0, &cx, &cy);
        bb.regr().map(cx, &cy);
        painter.drawLine(QPointF(cx-cross_sz/2.0, cy),QPointF(cx+cross_sz/2.0, cy));
        painter.drawLine(QPointF(cx, cy-cross_sz/2.0),QPointF(cx, cy+cross_sz/2.0));
    }

    //Guardar la imagen
    painter.end();
    img.save(filename);
}

void  ATDDetector::SaveRegrLine(ATDBoundingBox& bb)
{
    if (!mPar.save_regr_line_data) return;
    QString filename = GetOutputFilename("regr",".txt");
    FILE* fil = fopen(filename.toLatin1().data(),"wt");
    if (!fil) return;
    fprintf(fil,"#REGRESSION: y = %0.3fx %s %0.3fb ; r=%0.4f\n",bb.regr().m(),bb.regr().b()>0?"+":"-",fabs(bb.regr().b()), bb.regr().r());
    fprintf(fil,"#Number of points: %d\n",mPointAccepted.count());
    fprintf(fil,"#--------------------------------------------------------------\n");
    fprintf(fil,"#X\tY\n");
    for (int i=0; i<mPointAccepted.count(); i++)
        fprintf(fil, "%5d\t%5d\n",mPointAccepted[i].x(), mPointAccepted[i].y());
    fclose(fil);
}

void  ATDDetector::SaveFittingData(ATDBoundingBox& bbox)
{
    if (!mPar.save_fitting_data) return;
    QString filename = GetOutputFilename("fitting",".txt");
    FILE* fil = fopen(filename.toLatin1().data(),"wt");
    if (!fil) return;

    double a = bbox.foo()->at(0);
    double b = bbox.foo()->at(1);
    double c = bbox.foo()->at(2);
    double d = bbox.foo()->at(3);
    double e = bbox.foo()->at(4);

    int start = (int)floor(bbox.flanks()->at(0)) - mPar.trail_mid_height;
    if (start < 0) start = 0;
    int end =  (int)ceil(bbox.flanks()->at(1)) + mPar.trail_mid_height;
    if (end >= bbox.foldBox()->length()) end = bbox.foldBox()->length();

    fprintf(fil,"#FITTING: f(x) = a * sech(( (2(x-c))/b - 1)^d)^2 + e\n");
    fprintf(fil,"#FITTING: f(x) = %0.3f * sech(( (2(x-%0.3f))/%0.3f - 1)^%0.3f)^2 + %0.3f\n",a,b,c,d,e);
    if (IS_VALIDF(bbox.rhoF()))
        fprintf(fil,"#SPEARMAN CORRELATION: %0.4f\n",bbox.rhoF());
    fprintf(fil,"#Number of points: %d\n",mPointAccepted.count());
    fprintf(fil,"#--------------------------------------------------------------\n");
    fprintf(fil,"#X\tY_REAL\tY_FITTING\n");
    for (int x=start; x<=end; x++)
    {
        double real_val_y = bbox.foldBox()->at(x);
        double theorical_y = a * POW2(SQSECH(pow((2*x-2*c)/b-1,d))) + e;
        fprintf(fil,"%4d\t%9.4f\t%9.4f\n", x, real_val_y, theorical_y);
    }
    fclose(fil);
}

inline bool ppol(QPointF point, QPointF* points, int nvert)
{
  bool c = false;
  for(int i = 0, j = nvert - 1; i < nvert; j = i++)
  {
    if( ( (points[i].y() >= point.y() ) != (points[j].y() >= point.y()) ) &&
        (point.x() <= (points[j].x() - points[i].x()) * (point.y() - points[i].y()) / (points[j].y() - points[i].y()) + points[i].x())
      )
      c = !c;
  }
  return c;
}

G2D_Line ATDDetector::GetRegressionLine(QPolygonF bbox, double csigma)
{
    double thres = mBackground + csigma*mBackgroundSigma;
   // bbox.append(bbox.first());

    //Mater en la lista de puntos aceptados aquellos que cumplen el criterio de valor de fondo
    mPointAccepted.clear();
    QVector<double>  weights;
    double minx,maxx,miny,maxy; this->GetMinMaxIter(bbox, &minx, &maxx, &miny, &maxy);
    for (int y=miny;y<=maxy;y++)
    {
        for (int x=minx;x<maxx;x++)
        {
            if (!Geometry::PointInPolygon(QPointF(x,y), bbox.data(), bbox.count())) continue;
            //bool pp1 = bbox.containsPoint(QPointF(x,y),Qt::FillRule::OddEvenFill);
            float val = mFitWithoutStars->data()[x+y*mWidth];
            if (val < thres) continue;
            mPointAccepted.append(QPoint(x,y));
            weights.append(val);
        }
    }


    //Ahora de la lista de puntos aceptados eliminamos aquellos que no tienen vecino
    //----------------------------------------------------------------------------------
    QVector<QPoint> with_neigh;             //Puntos aceptados por tener vecinos
    BitMatrix rejected(0,0,mWidth,mHeight); //Puntos que han sido rechazados por tener pocos vecinos
    do
    {
        with_neigh.clear();
        //Repetir mientras haya puntos con vecinos
        for (int i=0; i<mPointAccepted.count(); i++)
        {
            QPoint p1 = mPointAccepted.at(i);
            int neigh = 0;
            //8 puntos circundantes
            double val[8] = {
                 PVAL(p1.x()-1, p1.y()-1),
                 PVAL(p1.x(), p1.y()-1),
                 PVAL(p1.x()+1, p1.y()-1),
                 PVAL(p1.x()-1, p1.y()),
                 PVAL(p1.x()+1, p1.y()),
                 PVAL(p1.x()-1, p1.y()+1),
                 PVAL(p1.x(), p1.y()+1),
                 PVAL(p1.x()+1, p1.y()+1)
            };
            //No es necesario comprobar si el punto circundante está en el polígono, asumimos que solo
            //puede haber problemas con esto en los bordes y es bastante difícil
            for (int n=0; n<8; n++) neigh += val[n] > thres ? 1 : 0;
            //Cuidado, no contar con vecinos anteriores que fueron rechazados
            if (rejected.get(p1.x()-1, p1.y()-1)) neigh--;
            if (rejected.get(p1.x(), p1.y()-1)) neigh--;
            if (rejected.get(p1.x()+1, p1.y()-1)) neigh--;
            if (rejected.get(p1.x()-1, p1.y())) neigh--;
            if (rejected.get(p1.x()+1, p1.y())) neigh--;
            if (rejected.get(p1.x()-1, p1.y()+1)) neigh--;
            if (rejected.get(p1.x(), p1.y()+1)) neigh--;
            if (rejected.get(p1.x()+1, p1.y()+1)) neigh--;
            //Lo añadimos a la lista de aceptados o de rechazados?
            if (neigh >= mPar.min_neighbours)
                with_neigh.append(p1);
            else
                rejected.set(p1.x(),p1.y(),true);
        }
        if (mPointAccepted.count() == with_neigh.count()) break;
        mPointAccepted = with_neigh;
    } while (true);

    double m,b,r_coeff, s_coeff;
    //Esto habrá que cambiarlo a un valor razonable y parametrizado
    if (mPointAccepted.length() < 10)
    {
       G2D_Line line = G2D_Line(INVALID_FLOAT, INVALID_FLOAT);
       line.setR(0);
       return line;
    }
    RobustFitting(mPointAccepted.data(), weights.data(), mPointAccepted.length(), &m, &b);
    G2D_Line line = G2D_Line(m, b);
    //Ahora eliminamos aquellos muy alejados de la linea
    with_neigh.clear();
    for (int i=0; i<mPointAccepted.count(); i++)
    {
        double dist = line.distance(QPointF(mPointAccepted[i]));
        if (dist < mPar.trail_mid_height+0.5) with_neigh.append(mPointAccepted[i]);
    }
    mPointAccepted = with_neigh;

    if (mPointAccepted.count() >= mPar.min_area_regr)
    {
        r_coeff = fabs(LinearFittingCoefficientW(mPointAccepted, weights));
        s_coeff = fabs(LinearSpearmanCoefficientW(mPointAccepted, m, b, weights));
    }
    else
        r_coeff = 0;
    line.setR(r_coeff);
    line.setS(s_coeff);

    return line;
}


QVector<TRAIL_DATA> ATDDetector::DetectBox()
{    
    //TODO: quitar esto
    return QVector<TRAIL_DATA>();

    mIter = 0;

    int width = mPar.detect_box_width / mPar.bin;
    int height = mPar.detect_box_height / mPar.bin;

    if (mPar.trail_est_length)
    {
        INFO("* Use trail estimated length: %d", mPar.trail_est_length);
    }

    ComputeGlobalBackground(mWorkImage);
    CreateImageWithoutStars(mWorkImage, 1);
    mSourceImageWithoutStars = mFitWithoutStarsBK;
    if (mPar.bin == 1)
    {
        mWorkImage = mSourceImage;
    }
    else
    {
        mImageBinned = CreateImageBinned(mWorkImage,"BINNED");
        mFitWithoutStars = CreateImageBinned(mFitWithoutStars);
        mFitWithoutStarsBK = CreateImageBinned(mFitWithoutStarsBK,"BINNED_WITHOUT_STARS");
        mWorkImage = FitImage::OpenFile(GetOutputFilename("BINNED","fit", false));
        mWorkImage->LookForPlate();
        ComputeGlobalBackground(mWorkImage);
        mSourcePar = mPar;
        mPar.min_area /= POW2(mPar.bin);
        mPar.trail_est_length /= mPar.bin;
        mPar.trail_est_flank_size /= mPar.bin;
        mPar.trail_est_sig_flanks /= mPar.bin;
        mPar.trail_mid_height /= mPar.bin;
        mPar.detect_box_width /= mPar.bin;
        mPar.detect_box_height /= mPar.bin;
        mPar.slice_final_height /= mPar.bin;

        mPar.crop_left /= mPar.bin;
        mPar.crop_right /= mPar.bin;
        mPar.crop_top /= mPar.bin;
        mPar.crop_bottom /= mPar.bin;
    }
    mWidth = mWorkImage->width();
    mHeight = mWorkImage->height();


    QVector<ATDBoundingBox> candidates;
    int step_h = height-mPar.slice_final_height;//*3/4;
    int step_w = width-mPar.slice_final_height;//*3/4;

    int min_x = mPar.crop_left; int max_x = mWorkImage->width()-mPar.crop_right-step_w;
    int min_y = mPar.crop_top; int max_y = mWorkImage->height()-mPar.crop_bottom-step_h;

    for (int y=min_y; y<max_y; y+=step_h)
    {
        for (int x=min_x; x<max_x; x+=step_w)
        {
           QPointF p1 = QPointF(x,y+height/2);
           QPointF p2 = QPointF(x+width,y+height/2);
           INFO("****DETECT [%02d] H=%02d P1=(%0.2f,%0.2f),P2=(%0.2f,%0.2f)", mIter,
                height, p1.x(),p1.y(),p2.x(),p2.y());
           ATDBoundingBox atb(p1, p2, height);
           ComputeBackground(mFitWithoutStars, atb.polygonEX());
           G2D_Line regr = GetRegressionLine(atb.polygon(), mPar.sigma_units);
           atb.setRegr(regr); atb.setPointCount(mPointAccepted.count());           
           atb.showConsole();
           //TODO
           SaveRegrLine(atb);
           //Usamos el coeficiente de pearson o el de spearman
           if (!regr.isNull() && mPointAccepted.count() >= mPar.min_area &&
                  //Comprobación laxa de coeficientes
                   COEFF_OK(regr.r(),regr.s()) )

           {
                INFO("\tCANDIDATE FOUND!!!");
                atb.setId(mIter);
                candidates.append(atb);
           }
           SaveOverlay(mWorkImage, atb);
           SaveRegrLine(atb);
           mIter++;
        }
    }
    INFO("ITERATING CANDIDATES - REFINE TRAILS");
    //Iteraciones sobre los candidatos. Refinamos cada caja
    for (int i=0; i<candidates.count(); i++)
    {
        ATDBoundingBox bb = candidates.at(i);
        INFO("**** REFINE CANDIDATE %d", bb.id());
        mIter =  candidates[i].id();
        bb = DetectInBox(bb.p1(), bb.p2(), bb.height(), mPar.sigma_units);
        bb.setId(candidates[i].id());
        candidates.replace(i, bb);
    }

    INFO("FINALLY FOUND: %d candidates", candidates.count());
    for (int i=0; i<candidates.count(); i++)
    {
        INFO("\tCandidate: %d ID=%d", i+1, candidates[i].id());
        candidates[i].showConsole();
    }

    //Ahora se itera sobre cada candidato obteniendo los datos abatidos
    for (int i=0; i<candidates.count(); i++)
    {
        mIter = candidates[i].id();
        QVector<double> d; Transform2D_ER t;
        INFO("Computing profile for candidate: [%02d]", candidates[i].id());
        FoldBoundigBox(candidates[i],mFitWithoutStarsBK, &d, &t);
        candidates.data()[i].setFoldBox(d);
        candidates.data()[i].setTransform(t);
        QVector<double> signal = ComputeSignalLevel(candidates[i]);
        candidates.data()[i].setBackground(signal[0],signal[2]);
        candidates.data()[i].setSignalLevel(signal[1]);
        QVector<double> flanks = ComputeImpreciseFlanks(candidates[i]);
        candidates.data()[i].setFlanks(flanks);
        //Guardar la primera versión del profile
        SaveSliceProfile(candidates[i], d.data(),d.length(), "A_first");
    }

    //Ahora se redimensionan las cajas para ajustarse mejor a los nuevos extremos
    for (int i=0; i<candidates.count(); i++)
    {
        mIter = candidates[i].id();
        //Primero ajustamos a los nuevos flancos
        INFO("Adjust candidate flanks: [%02d]", candidates[i].id());
        ATDBoundingBox bb_new = AdjustToFlanks(candidates[i]);
        bb_new.setId(candidates[i].id());
        ComputeBackground(mFitWithoutStarsBK, bb_new.polygonEX());
        bb_new.setBackground(mBackground, mBackgroundSigma);
        bb_new.setBaseBackground(mBackground, mBackgroundSigma);
        G2D_Line regr = GetRegressionLine(bb_new.polygon(), mPar.sigma_units);
        bb_new.setRegr(regr);
        bb_new.setPointCount(mPointAccepted.count());
        //Y ahora todos los cálculos de abatido con cálculo de flancos imprecisos
        QVector<double> d; Transform2D_ER t;
        FoldBoundigBox(bb_new, mFitWithoutStarsBK, &d, &t);
        bb_new.setFoldBox(d);
        bb_new.setTransform(t);
        QVector<double> signal = ComputeSignalLevel(bb_new);
        bb_new.setBackground(signal[0],signal[2]);
        bb_new.setSignalLevel(signal[1]);
        bb_new.setIgnoreOver(signal[3]);
        QVector<double> flanks = ComputeImpreciseFlanks(bb_new);
        bb_new.setFlanks(flanks);
        SaveSliceProfile(bb_new, d.data(),d.length(), "B_second");
        candidates[i] = bb_new;
    }

    //Ahora se realiza el fitting real de los flancos
    for (int i=0; i<candidates.count(); i++)
    {
        ATDBoundingBox* can = candidates.data()+i;
        mIter = candidates.data()[i].id();
        QVector<double> flanks, foo;
        INFO("Compute precise flanks. [%02d]", candidates[i].id());
        bool ok = ComputePreciseFlanks(can, &flanks, &foo);

        if (!ok || (flanks.length() != 2))
        {
            INFO("\tCompute flanks error. No data to fit. Candidate removed");
            candidates.removeAt(i);
            i--;
            continue;
        }

        can->setFlanks(flanks);
        can->setFoo(foo);
        SaveSliceProfile(*can, can->foldBox()->data(), can->foldBox()->length(), "C_last_00");
        INFO("\tFitting done: A=%0.4f; B=%0.4f; C=%0.4f; D=%0.4f; E=%0.4f / RHO=%0.4f",
                  can->foo()->at(0), can->foo()->at(1),
                  can->foo()->at(2), can->foo()->at(3),
                  can->foo()->at(4),
                  can->rhoF()
                  );
    }

    //Nos aseguramos de que todos los flancos estén dentro del perfil
    AssertFlanksInsideProfile(candidates);

    //Comprobar que todos los candidatos tienen un coeficiente de regresión aceptable
    for (int i=0; i<candidates.count(); i++)
    {         
         ATDBoundingBox can = candidates[i];

         if (!CheckTrailParams(can))
         {
             INFO("\t CANDIDATE %d rejected", can.id());
             candidates.removeAt(i);
             i--;
             continue;
         }
    }

    RemoveDuplicates(&candidates, 0.5);

    //Si estamos usando bin, calculamos los parámetros con la imagen
    QVector<TRAIL_DATA> tdata;
    QVector<ATDBoundingBox> trails_found;
    if (mPar.bin == 1)
    {
        trails_found = candidates;
    }
    else
    {
        INFO("********************************************");
        INFO("******** [%2d BINNED TRAILS FOUND] *********", candidates.count());
        INFO("********************************************");
        for (int i=0; i<trails_found.count(); i++)
        {
            ATDBoundingBox can = trails_found.at(i);
            mIter = can.id();
            double ds = ProfileSpearmanCoefficientOnFlanks(can);
            can.setRhoF(ds);
            can.showConsole();
            if (ds < mPar.rho_min)
            {
                INFO("\tWARNING : rho = %0.3f < %0.3f ==> NOT A TRAIL?", ds, mPar.rho_min);
            }
        }
       INFO("************ MOVE BIN %dx%d -> 1x1 ************", mPar.bin, mPar.bin);
       trails_found = BacktoBin1x1(candidates);
       RemoveDuplicates(&trails_found, 0.5);
    }

    INFO("********************************************");
    INFO("************* [%2d TRAILS FOUND] *************", trails_found.count());
    INFO("********************************************");
    for (int i=0; i<trails_found.count(); i++)
    {
        ATDBoundingBox can = trails_found.at(i);
        mIter = can.id();
        double ds = ProfileSpearmanCoefficientOnFlanks(can);
        can.setRhoF(ds);
        can.showConsole();
        if (ds < mPar.rho_min)
        {
            INFO("\t\tWARNING : rho = %0.3f < %0.3f ==> NOT A TRAIL? => REJECTED", ds, mPar.rho_min);
            trails_found.removeAt(i);
            i--;
            continue;
        }
        SaveFittingData(can);
        tdata.append(ComputeTrailData(can));
    }
    mIter = 99;
    SaveFinalOverlay(trails_found);

   return tdata;
}


void ATDDetector::AssertFlanksInsideProfile(QVector<ATDBoundingBox> &candidates)
{
    //Aquellos candidatos con el borde fuera de la caja (o muy cerca del extremo)
    //Serán redimensionados y recalculados
    for (int i=0; i<candidates.count(); i++)
    {
        bool flank_moved_left, flank_moved_right;
        int iters = 1;
        do
        {
            int ideal_dist = mPar.trail_mid_height * 5;
            double min_dist = ideal_dist;
            flank_moved_left = false; flank_moved_right = false;
            ATDBoundingBox can = candidates[i];
            mIter = can.id();

            //Borde fuera por la izquierda. Atención, los dos bordes no pueden estar fuera

            if (can.flanks()->data()[0] < min_dist)
            {
                INFO("Candidate %d resized, Left flank is out of foldBox", can.id());
                can.flanks()->data()[0] = can.flanks()->data()[0] - can.foldBox()->length()/2;
                flank_moved_left = true;
            }
            else if (can.flanks()->data()[1] >= can.foldBox()->length() - min_dist)
            {
                INFO("Candidate %d resized, Right flank is out of foldBox", can.id());
                can.flanks()->data()[1] = can.flanks()->data()[1] + can.foldBox()->length()/2;
                flank_moved_right = true;
            }

            if (flank_moved_left || flank_moved_right)
            {
                //Primero ajustamos a los nuevos flancos
                INFO("\t\t-- Flank moved, Resize box");
                ATDBoundingBox bb_new = ResizeBox(can, flank_moved_left ? ideal_dist : 0, flank_moved_right ? ideal_dist : 0);
                bb_new.setId(can.id());
                INFO("\t\t-- Compute Background");
                ComputeBackground(mFitWithoutStarsBK, bb_new.polygonEX());
                G2D_Line regr = GetRegressionLine(bb_new.polygon(), mPar.sigma_units);
                if (regr.isNull())
                {
                    INFO("Irregular background detected. Try with original background");
                    mBackground = can.baseBkg(); mBackgroundSigma = can.baseBkgSigma();
                    regr = GetRegressionLine(bb_new.polygon(), mPar.sigma_units);
                }
                bb_new.setRegr(regr);
                bb_new.setPointCount(mPointAccepted.count());
                //Y ahora todos los cálculos de abatido
                QVector<double> d; Transform2D_ER t;
                FoldBoundigBox(bb_new,mFitWithoutStarsBK, &d, &t);
                bb_new.setFoldBox(d);
                bb_new.setTransform(t);
                QVector<double> signal = ComputeSignalLevel(bb_new);
                bb_new.setBackground(signal[0],signal[2]);
                bb_new.setSignalLevel(signal[1]);
                bb_new.setIgnoreOver(signal[3]);

                INFO("\t\t-- Compute imprecise flanks");
                QVector<double> flanksx = ComputeImpreciseFlanks(bb_new);

                if (flanksx.length() != 2)
                {
                    INFO("Candidate %d rejected. Precise flank compute error", can.id());
                    iters =  mPar.max_iters_extend_flanks;
                    break;
                }

                bb_new.setFlanks(flanksx);
                QVector<double> flanks, foo;

                INFO("\t\t-- Compute precise flanks");
                bool ok = ComputePreciseFlanks(&bb_new, &flanks, &foo);

                if (!ok || flanks.length() != 2)
                {
                    INFO("Candidate %d rejected. Precise flank compute error", can.id());
                    iters =  mPar.max_iters_extend_flanks;
                    break;
                }

                bb_new.setFlanks(flanks);
                bb_new.setFoo(foo);

                INFO("\t\t-- Save slice profile");
                SaveSliceProfile(bb_new, bb_new.foldBox()->data(), bb_new.foldBox()->length(), "C_last_" + QString().sprintf("%02d", iters));
                candidates[i] = bb_new;
            }
            iters++;
        } while ((iters < mPar.max_iters_extend_flanks) && (flank_moved_left || flank_moved_right));
        ATDBoundingBox can = candidates[i]; can.foo()->clear();
        SaveSliceProfile(can, can.foldBox()->data(), can.foldBox()->length(), "C_last_98");
        SaveSliceProfile(candidates[i], candidates[i].foldBox()->data(), candidates[i].foldBox()->length(), "C_last_99");
        if (iters == mPar.max_iters_extend_flanks)
        {
            INFO("Candidate %d rejected. Flank is out of foldbox", can.id());
            candidates.removeAt(i);
            i--;
        }
    }
}

void ATDDetector::RemoveDuplicates(QVector<ATDBoundingBox>* candidates,double percent_tolerance)
{
    //Ahora se eliminan duplicados y si alguna se corta entre ella, la que tenga
    //menor coeficiente de correlación de spearman
    for (int i=0; i<candidates->count(); i++)
    {
        ATDBoundingBox* can1 = candidates->data()+i;
        G2D_Line regr1 = can1->regr();
        for (int j=i+1; j<candidates->count(); j++)
        {
            ATDBoundingBox* can2 = candidates->data()+j;
            G2D_Line regr2 = can2->regr();
            double pte = CO_COMP(regr1.m(),regr2.m())*100;
            double poo = CO_COMP(regr1.b(),regr2.b())*100;
            double len = CO_COMP(can1->flanks()->at(1) - can1->flanks()->at(0),can2->flanks()->at(1) - can2->flanks()->at(0))*100;
            if (pte < percent_tolerance && poo < percent_tolerance && len < percent_tolerance)   //Diferencias máximas de un 0.5%
            {
                //Quitar el que tenga algún flanco pequeño
                double fa1 = can1->flanks()->data()[0];
                double fa2 = can1->foldBox()->count() -  can1->flanks()->data()[1];
                //Digamos que el primero tiene preferencia
                double fb1 = can2->flanks()->data()[0] - 1;
                double fb2 = can2->foldBox()->count() - can2->flanks()->data()[1] - 1;
                if ((fa1 < mPar.trail_mid_height * 5) || (fa2 < mPar.trail_mid_height * 5)
                    || (fa1 < fb1 && fa2 < fb2))
                {
                    INFO("Candidate %d rejected, DUPLICATED with %d", can1->id(), can2->id());
                    candidates->removeAt(i);
                    i=-1; break;
                }
                else
                {
                    INFO("Candidate %d rejected, DUPLICATED with %d", can2->id(), can1->id());
                    candidates->removeAt(j);
                    i=-1; break;
                }
            }
        }
    }
}

QVector<ATDBoundingBox> ATDDetector::BacktoBin1x1(QVector<ATDBoundingBox>& can)
{
    QVector<ATDBoundingBox> detected;
    int bin = mPar.bin;
    mPar = mSourcePar;
    mPar.bin = 1;
    mWorkImage = mSourceImage;
    mFitWithoutStars = mFitWithoutStarsBK = mSourceImageWithoutStars;
    mWidth = mWorkImage->width(); mHeight = mWorkImage->height();


    for (int i=0; i<can.length(); i++)
    {
        ATDBoundingBox atb = can.at(i);
        mIter = atb.id();

        //Quitamos el bin para los puntos del polígono
        QPointF p1(atb.p1().x() * bin, atb.p1().y()*bin);
        QPointF p2(atb.p2().x() * bin, atb.p2().y()*bin);
        //Viejos flancos
        QVector<double> old_flanks;
        old_flanks.append(atb.flanks()->at(0)*bin);
        old_flanks.append(atb.flanks()->at(1)*bin);
        //Viejo ajuste
        QVector<double> old_foo = *atb.foo();
        old_foo[0] /= bin;
        old_foo[4] /= bin;
        old_foo[1] *= bin;
        old_foo[2] *= bin;

        //Calculamos la nueva caja
        atb.showConsole();

        double sigmag = mPar.sigma_units;
        mPointAccepted.clear();
        ATDBoundingBox newbb;
        while ((mPointAccepted.length() < mPar.min_area) && (sigmag > 0.05))
        {            
            mIter = atb.id();
            INFO(">> ITERATE CANDIDATE [%02d] Sigma*: %0.4f; Background(%0.3f)+Sigma(%0.3f*%0.3f=%0.3f)",
                 atb.id(), sigmag,
                 mBackground, mBackgroundSigma,sigmag,mBackgroundSigma*sigmag);
            newbb  = DetectInBox(p1, p2, mPar.slice_final_height+2, sigmag);
            newbb.setId(atb.id());
            if (mPointAccepted.length()< mPar.min_area) sigmag /= 1.5;
            if (!CheckTrailParams(newbb))
            {
                INFO("\tCANDIDATE %d rejected", newbb.id());
                continue;
            }
        }
        newbb.setRegr(GetRegressionLine(newbb.polygon(), sigmag));
        newbb.setPointCount(mPointAccepted.length());
        newbb.setId(atb.id());

        if (mPointAccepted.length() < mPar.min_area)
        {
            INFO("*** NO SIGNAL POINTS IN AREA. REJECTING TRAIL");
            continue;
        }

        QVector<double> d; Transform2D_ER t;
        FoldBoundigBox(newbb,mFitWithoutStarsBK, &d, &t);
        newbb.setFoldBox(d);
        newbb.setTransform(t);       

        QVector<double> signal = ComputeSignalLevel(newbb);
        newbb.setBackground(signal[0],signal[2]);
        newbb.setSignalLevel(signal[1]);
        newbb.setFlanks(old_flanks);
        newbb.setFoo(old_foo);
        newbb.showFoo();
        //Guardar la primera versión del profile
        SaveSliceProfile(newbb, newbb.foldBox()->data(), newbb.foldBox()->length(), "source_binned_flanks_98");

        QVector<double> flanks; QVector<double> foo;
        INFO("Compute precise flanks. fitting: [%02d]", newbb.id());
        bool ok = ComputePreciseFlanks(&newbb, &flanks, &foo);
        if (!ok || (flanks.length() != 2))
        {
            INFO("\tCompute flanks error. No data to fit.");
            if (mPar.superfit)
            {

            }
            else
            {
                continue;
            }
        }
        SaveSliceProfile(newbb, newbb.foldBox()->data(), newbb.foldBox()->length(), "C_last_99");
        if (mPar.superfit)
        {
            SuperFit sf,sf2;
            sf.setCoeff(*newbb.foo());
            sf.setDataExp(*newbb.foldBox());
            sf.setBkg(newbb.bkg(), newbb.signalLevel());
            sf.saveImage(GetOutputFilename("superfit_start","png"));
            if (!sf.superfitAtLeft(15))
            {
                WARN("Superfit at left failed. Rejecting trail[%d]", mIter);
                continue;
            }
            sf.saveImage(GetOutputFilename("superfit_left","png"));
            sf2 = sf;
            if (!sf2.superfitAtRight(15))
            {
                 WARN("Superfit at right failed. Rejecting trail[%d]", mIter);
                 continue;
            }
            sf2.saveImage(GetOutputFilename("superfit_right","png"));
            SuperFit2 ssf2(sf, sf2);
            ssf2.saveImage(GetOutputFilename("superfit","png"));
            newbb.setFlanks(ssf2.flanks());
            newbb.setRhoF(ssf2.rho());
        }
        //Agregar al vector
        detected.append(newbb);
    }

    //Nos aseguramos de que todos los flancos estén dentro del perfil
    AssertFlanksInsideProfile(detected);

    return detected;
}


bool ATDDetector::CheckTrailParams(ATDBoundingBox &can)
{
    double a = can.foo()->length() >= 5 ? can.foo()->at(0) : INVALID_FLOAT;

    //Rechazo harcodeado
    if (!can.regr().isNull() && !COEFF_OK(can.regr().r(),can.regr().s()))
    {
        INFO("Candidate %d should be rejected, non linear", can.id());
        return false;
    }

    //No son aceptables amplitudes negativas
    if (IS_VALIDF(a) && a < 0)
    {
        INFO("Candidate %d should be rejected, A < 0 [%0.4f]", can.id(), a);
        return false;
    }

    //No son aceptables relaciones señal a ruido muy pequeñas
    if ((can.foo()->length() >= 5) && IS_VALIDF(can.sNr()) && can.sNr() < mPar.snr_min)
    {
        INFO("Candidate %d should be rejected, S/Nr [%0.4f] < [%0.4f]", can.id(), can.sNr(), mPar.snr_min);
        return false;
    }

    return true;
}


ATDBoundingBox ATDDetector::DetectInBox(QPointF p1, QPointF p2, int sliceInitialHeight, double sigma_units)
{
    int last_iter_count = 0;
    ATDBoundingBox atb;
    for (int sliceHeight=sliceInitialHeight;
         sliceHeight >= mPar.slice_final_height;)
    {
        INFO("****REFINE [%02d] H=%02d P1=(%0.2f,%0.2f),P2=(%0.2f,%0.2f)", mIter,
             sliceHeight, p1.x(),p1.y(),p2.x(),p2.y());
        double ra1,dec1,ra2,dec2;
        mWorkImage->plate()->map(p1.x(),p1.y(),&ra1,&dec1);
        mWorkImage->plate()->map(p2.x(),p2.y(),&ra2,&dec2);
        INFO("\tP1_RADEC=(%s %s),P2_RADEC=(%s %s)",
             PTQ(Util::RA2String(ra1)),PTQ(Util::DEC2String(dec1)),
             PTQ(Util::RA2String(ra2)),PTQ(Util::DEC2String(dec2)));

        //Inicio. Calculamos el bounding box a partir de los puntos p1,p2 y el alto de la rodaja
        //------------------------------------------------------------------------------
        atb = ATDBoundingBox(p1, p2, sliceHeight);
        ComputeBackground(mFitWithoutStarsBK, atb.polygonEX());
        atb.setRegr(GetRegressionLine(atb.polygon(), sigma_units));
        atb.setBackground(mBackground, mBackgroundSigma);
        atb.setPointCount(mPointAccepted.length());
        SaveOverlay(mWorkImage, atb);
        SaveRegrLine(atb);
        if (atb.regr().isNull())
        {
            INFO("\tNo points to compute data...");
            return atb;
        }

        //Calculados los datros, se crea el bounding box de la siguiente iteración
        //------------------------------------------------------------------------------
        //Calcular el ángulo de nuestra recta de regresión con nuestra bounding box
        G2D_Line lbb(p1, p2);
        double m2 = lbb.m();
        double m1 = atb.regr().m();
        double ang = atan(fabs( (m2-m1) / (1+m2*m1)  ))*RAD2DEG;

        QLineF ln(p1, p2);
        double angle = ln.angle();
        ln.setAngle(angle + ang);
        p1 = ln.p1(); p2 = ln.p2();

        //Pero también hay que desplazar la línea para que esté sobre la recta de regresión
        //------------------------------------------------------------------------------
        QPolygonF poly = atb.polygon();
        QPointF p_left = poly[0], p_right = poly[1];
        for (int i=0; i<poly.length(); i++)
        {
            if (poly[i].x() < p_left.x()) p_left = poly[i];
            if (poly[i].x() > p_right.x()) p_right = poly[i];
        }
        double y_left,y_right;
        atb.regr().map(p_left.x(), &y_left);
        atb.regr().map(p_right.x(), &y_right);

        QLineF regr(QPointF(p_left.x(), y_left), QPointF(p_right.x(), y_right));
        QLineF leftl(poly[0], poly[3]); QLineF rightl(poly[1],poly[2]);
        QPointF pin1; QPointF pin2;
        regr.intersect(leftl, &pin1);
        regr.intersect(rightl, &pin2);
        p1 = pin1; p2 = pin2;

        //Fin de la iteración. Tenemos una línea de regresión y un coeficiente de ajuste
        //------------------------------------------------------------------------------
        INFO("\tEnd of iteration: Regression line: y = %0.3fx + %0.3f; r=%0.4f; s=%0.4f; Points: %d; Boxlen: %0.1f",
             atb.regr().m(), atb.regr().b(),
             atb.regr().r(), atb.regr().s(),
             atb.pointCount(),
             CART_DIST(p1.x(),p1.y(),p2.x(),p2.y()));

        if (sliceHeight == mPar.slice_final_height)
        {
            last_iter_count++;
            //Cuidado, si el ángulo entre la recta de regresión y la caja es muy grande,
            //No damos por teminada la iteración
            QLineF line_old(atb.p1(), atb.p2());
            QLineF line_new(p1, p2);
            ang = fabs(line_new.angleTo(line_old));
            if (ang < 1 || last_iter_count > 5) break;
        }
        else sliceHeight-=mPar.slice_iter_decrement;
        if (sliceHeight < mPar.slice_final_height) sliceHeight = mPar.slice_final_height;
    }
    return atb;
}

void ATDDetector::Detect1(QPointF p1, QPointF p2)
{
    mPar.sigma_units = 2.5;
    mPar.slice_iter_decrement = 16;
    mIter = 0;
    CreateImageWithoutStars(mWorkImage, 1);
    DetectInBox(p1,p2, 1224, mPar.sigma_units);
}

void ATDDetector::ReadStarsInField()
{
    INFO("Reading stars from VIZIER");
    double ra0, dec0;
    mWorkImage->plate()->map(mWorkImage->width()/2, mWorkImage->height()/2, &ra0, &dec0);
    double radius = mWorkImage->plate()->scale() * MAXVAL(mWorkImage->width(), mWorkImage->height())*1.42;
    VizierReader vr;
    VizierCatalog* vc = VizierCatalog::getCatalog("PPMXL");
    vr.SetCatalog(vc);
    bool ok = vr.webRead(ra0, dec0, radius/3600.0, 19.5);
    mStars.clear();
    for (int i=0; i<vr.stars()->length(); i++)
    {
       VizierRecord star = vr.stars()->at(i);
       mStars.append(star);
    }
    INFO("Read: %d stars", mStars.count());
}

bool ATDDetector::RemoveStarPoint(StarDetector*, int x, int y, float /*flux*/, void *user_data)
{
    ATDDetector* atd = (ATDDetector*)user_data;
    atd->mFitWithoutStars->data()[x + y * atd->mWidth] = 0;
    atd->mFitWithoutStarsBK->data()[x + y * atd->mWidth] = atd->mGlobalBackground;
    return true;
}

FitImage* ATDDetector::CreateImageBinned(FitImage* fi, QString savefilename)
{
   FitImage* binned = fi->CloneBinned(mPar.bin);
   if (savefilename.length())
       binned->SaveToFile(GetOutputFilename(savefilename,"fit"));
   return binned;
}

void ATDDetector::CreateImageWithoutStars(FitImage* fi, int bin_at_0304)
{
    ReadStarsInField();
    mFitWithoutStars = fi->Clone();
    mFitWithoutStarsBK = mFitWithoutStars->Clone();
    StarDetector sd(mFitWithoutStars->data(), fi->width(), fi->height());
    QStringList mg;
    mg.append("b1mag");mg.append("b2mag");mg.append("r1mag");mg.append("r2mag");
    for (int i=0;i<mStars.count();i++)
    {
        VizierRecord star = mStars.at(i);
        float mag = star.averageMag(mg);
        float aperture = 7966.4*pow(mag,-2.329) / bin_at_0304;
        if (IS_INVALIDF(mag) || mag > 19.9) continue;
        double x,y;
        fi->plate()->imap(star.ra(), star.dec(), &x, &y);
        if (x<0 || x > fi->width() || y<0 || y>fi->height()) continue;
        sd.IterateAllPointsOfCircle(x, y, aperture , this, RemoveStarPoint); 
    }
    if (mPar.save_image_without_stars_fit)
    {
        mFitWithoutStars->SaveToFile(GetOutputFilename("IMAGE_WITHOUT_STARS","fit"));
        mFitWithoutStarsBK->SaveToFile(GetOutputFilename("IMAGE_WITHOUT_STARSBK","fit"));
    }
    if (mPar.save_image_without_stars_img)
    {
        QImage img = QImage(mWidth, mHeight, QImage::Format_RGB32);
        ConvergeProfile cp;
        cp.SetOptimumFor(fi);
        cp.SetImage(mFitWithoutStarsBK);
        cp.CreateColorTable();
        cp.Converge(&img);
        img.save(GetOutputFilename("IMAGE_WITHOUT_STARS","jpg"));
    }
}

void ATDDetector::ComputeBackground(FitImage* fi, QPolygonF external,bool use_mode)
{
    int histo[ATD_HISTO_SIZE];
    memset(histo, 0, sizeof(int)*ATD_HISTO_SIZE);
    double mode = INVALID_FLOAT;

    double minx,maxx,miny,maxy;
    GetMinMaxIter(external, &minx, &maxx, &miny, &maxy);
    //Ahora iteramos para calcular la media y desviación típica
    double avg = 0, avg2 = 0;
    int cont = 0;
    for (int y=miny;y<=maxy;y++)
    {
        for (int x=minx;x<=maxx;x++)
        {
            if (!Geometry::PointInPolygon(QPointF(x,y), external.data(), external.count())) continue;
            float val = fi->data()[y*fi->width()+x];
            //TODO: Este valor de 4 está metido a pedalín para evitar el slice izquierdo sin cuentas
            //de las imagenes de iac80ccd
            if (val < 1) continue;
            avg += val;
            avg2 += val*val;
            cont++;
            if (use_mode)
            {
                int histo_pos = (int) (val*ATD_HISTO_SIZE/65535.0);
                if (histo_pos >= ATD_HISTO_SIZE) histo_pos = ATD_HISTO_SIZE-1;
                if (histo_pos < 0) histo_pos = 0;
                histo[histo_pos]++;
            }
        }
    }
    if (use_mode)
    {
        int imax=0, vmax=histo[0];
        for (int i=0; i<ATD_HISTO_SIZE;i++)
        {if (histo[i] > vmax) vmax = histo[imax=i];}
        mode = imax * 65535.0 / ATD_HISTO_SIZE;
    }

    avg /= cont;
    mBackground = MINVAL(mode,avg);
    mBackgroundSigma = sqrt(avg2/cont - avg*avg);
     /*
    if (mBackgroundSigma > mBackground/2)
    {
        mBackground = mGlobalBackground;
        mBackgroundSigma = mGlobalBackgroundSigma;
    }*/
    //TODO: Iterar sobre el histograma para calcular la desviación típica del fondo
    INFO("\tBackground: Avg:%0.2f; Mode:%0.2f; Stdv: %0.2f", avg, mode, mBackgroundSigma);
}

double ATDDetector::LinearFittingCoefficientW(const QVector<QPoint> &points, const QVector<double>& weights)
{
    if (points.count() < 2) return -1;
    double med_x = 0, med_y = 0, sum = 0;
    for (int i=0; i<points.length(); i++)
    {
        med_x +=  points.at(i).x()*weights[i];
        med_y +=  points.at(i).y()*weights[i];
        sum += weights[i];
    }
    med_x /= sum;
    med_y /= sum;

    //Calcular la varianza
    double sx = 0, sy = 0;
    for (int i=0; i<points.length(); i++)
    {
        sx += POW2(points.at(i).x()*weights[i]-med_x);
        sy += POW2(points.at(i).y()*weights[i]-med_y);
    }
    sx = sqrt(sx / sum);
    sy = sqrt(sy / sum);

    //Finalmente calculamos el coeficiente
    double r = 0;
    for (int i=0; i<points.length(); i++)
    {
        r += ((points.at(i).x()*weights[i] - med_x)/sx) * ((points.at(i).y()*weights[i] - med_y)/sy);
    }
    r /= sum;
    return r;
}

double ATDDetector::LinearFittingCoefficient(const QVector<QPoint> &points)
{

    if (points.count() < 2) return -1;
    double n = points.length();
    double med_x = 0, med_y = 0, sum = 0;
    double sum_xy = 0;
    for (int i=0; i<points.length(); i++)
    {
        med_x +=  points.at(i).x();
        med_y +=  points.at(i).y();
        sum_xy += points.at(i).x()*points.at(i).y();
    }
    med_x /= n;
    med_y /= n;

    //Calcular la varianza
    double sx = 0, sy = 0;
    for (int i=0; i<points.length(); i++)
    {
        sx += POW2(points.at(i).x()-med_x);
        sy += POW2(points.at(i).y()-med_y);
    }
    sx = sqrt(sx / n);
    sy = sqrt(sy / n);

    //Finalmente calculamos el coeficiente
    double r = (sum_xy - n*med_x*med_y)/((n-1) * sx *sy);
}


double ATDDetector::LinearSpearmanCoefficient(const QVector<QPoint>& points, double m, double b)
{
    //Calculo los puntos del perfil
    int n = 0;
    double ds = 0;  //Coeficiente de correlación de spearman
    for (int i= 0;i<points.count(); i++)
    {
        QPoint p = points[i];
        double theorical_y = (m * p.x() + b);
        ds += POW2(p.y()-theorical_y);
        n++;
    }
    ds = 1-6*ds/(n*(POW2(n)-1));
    if (ds > 1) ds = 1;
    if (ds < -1) ds = -1;
    return ds;
}

double ATDDetector::LinearSpearmanCoefficientW(const QVector<QPoint>& points, double m, double b, const QVector<double>& weights)
{
    //Calculo los puntos del perfil

    int n = 0;
    double ds = 0;  //Coeficiente de correlación de spearman
    for (int i= 0;i<points.count(); i++)
    {
        QPoint p = points[i];
        double theorical_y = (m * p.x() + b);
        ds += POW2((p.y()-theorical_y)*weights[i]);
        n += weights[i];
    }
    ds = 1-6*ds/(n*(POW2(n)-1));
    if (ds > 1) ds = 1;
    if (ds < -1) ds = -1;


    //Otra manera de calcularlo
/*
    //1 - Media en x
    double real_med = 0, theorical_med = 0;
    double sumall = 0;
    for (int i= 0;i<points.count(); i++)
    {
        QPoint p = points[i];
        double theorical_y = (m * p.x() + b);
        real_med += p.y()*weights[i];
        theorical_med +=  theorical_y*weights[i];
        sumall += weights[i];
    }
    real_med /= sumall;
    theorical_med /= sumall;

    //Diferncias al cuadrado media
    double real_med2 = 0, theorical_med2 = 0;
    double diff_por = 0;
    for (int i= 0;i<points.count(); i++)
    {
        QPoint p = points[i];
        double theorical_y = (m * p.x() + b);
        real_med2 += POW2(p.y()-real_med)*weights[i];
        theorical_med2 += POW2(theorical_y-theorical_med)*weights[i];
        diff_por += (p.y()-real_med)*(theorical_y-theorical_med)*weights[i];
    }
    real_med2 /= sumall;
    theorical_med2 /= sumall;
    diff_por /= sumall;

    double ds2 =  diff_por / sqrt(real_med2*theorical_med2);

    if (ds2<-1||ds2>1)
    {
        WARN("Profile spearman coefficient error");
        ds2 = 0;
    }
*/
    return fabs(ds);
}


double ATDDetector::ProfileSpearmanCoefficient(ATDBoundingBox& bbox)
{
    //Calculo los puntos del perfil
    double a = bbox.foo()->at(0);
    double b = bbox.foo()->at(1);
    double c = bbox.foo()->at(2);
    double d = bbox.foo()->at(3);
    double e = bbox.foo()->at(4);

    int start = (int)floor(bbox.flanks()->at(0)) - mPar.trail_est_flank_size;
    if (start < 0) start = 0;
    int end =  (int)ceil(bbox.flanks()->at(1)) + mPar.trail_est_flank_size;
    if (end >= bbox.foldBox()->length()) end = bbox.foldBox()->length()-1;

    alglib::real_1d_array sx; sx.setlength(end-start+1);
    alglib::real_1d_array sy; sy.setlength(end-start+1);

    for (int x=start,i=0; x<=end; x++, i++)
    {
        double real_val_y = bbox.foldBox()->at(x);
        double theorical_y = a * POW2(SQSECH(pow(2*(x-c)/b-1,d))) + e;
        sx[i] = real_val_y;
        sy[i] = theorical_y;
    }
    double s = alglib::spearmancorr2(sx, sy);
    return fabs(s);
}

double ATDDetector::ProfileSpearmanCoefficientOnFlanks(ATDBoundingBox& bbox)
{
    //Calculo los puntos del perfil
    double a = bbox.foo()->at(0);
    double b = bbox.foo()->at(1);
    double c = bbox.foo()->at(2);
    double d = bbox.foo()->at(3);
    double e = bbox.foo()->at(4);

    int start = (int)floor(bbox.flanks()->at(0)) - mPar.trail_est_flank_size;
    if (start < 0) start = 0;
    int end =  (int)ceil(bbox.flanks()->at(0))  + mPar.trail_est_flank_size*2;
    if (end >= bbox.foldBox()->length()) end = bbox.foldBox()->length()-1;

    if (end <= start) return 0;

    alglib::real_1d_array sx; sx.setlength(end-start+1);
    alglib::real_1d_array sy; sy.setlength(end-start+1);


    for (int x=start,i=0; x<=end; x++, i++)
    {
        double real_val_y = bbox.foldBox()->at(x);
        double theorical_y = a * POW2(SQSECH(pow(2*(x-c)/b-1,d))) + e;
        sx[i] = real_val_y;
        sy[i] = theorical_y;
    }
    double s1 = alglib::spearmancorr2(sx, sy);

    start = (int)floor(bbox.flanks()->at(1)) - mPar.trail_est_flank_size*2;
    if (start < 0) start = 0;
    end =  (int)ceil(bbox.flanks()->at(1))  + mPar.trail_est_flank_size;
    if (end >= bbox.foldBox()->length()) end = bbox.foldBox()->length()-1;
    if (end <= start) return 0;

    sx.setlength(end-start+1);
    sy.setlength(end-start+1);
    for (int x=start,i=0; x<=end; x++, i++)
    {
        double real_val_y = bbox.foldBox()->at(x);
        double theorical_y = a * POW2(SQSECH(pow(2*(x-c)/b-1,d))) + e;
        sx[i] = real_val_y;
        sy[i] = theorical_y;
    }
    double s2 = alglib::spearmancorr2(sx, sy);

    return fabs((s1+s2)/2);
}

double ATDDetector::SquareDifferenceOnFlanks(ATDBoundingBox& bbox, bool left, bool right)
{
    //Calculo los puntos del perfil
    double a = bbox.foo()->at(0);
    double b = bbox.foo()->at(1);
    double c = bbox.foo()->at(2);
    double d = bbox.foo()->at(3);
    double e = bbox.foo()->at(4);

    int start = (int)floor(bbox.flanks()->at(0)) - mPar.trail_est_flank_size;
    if (start < 0) start = 0;
    int end =  (int)ceil(bbox.flanks()->at(0))  + mPar.trail_est_flank_size*2;
    if (end >= bbox.foldBox()->length()) end = bbox.foldBox()->length()-1;

    if (end <= start) return 0;

    double s1 = 0;
    if (left)
    {
        for (int x=start,i=0; x<=end; x++, i++)
        {
            double real_val_y = bbox.foldBox()->at(x);
            double theorical_y = a * POW2(SQSECH(pow(2*(x-c)/b-1,d))) + e;
            s1 = POW2(real_val_y-theorical_y);
        }
        s1 += (end-start+1)>0 ? sqrt(s1/(end-start+1)) : 0;
    }

    start = (int)floor(bbox.flanks()->at(1)) - mPar.trail_est_flank_size*2;
    if (start < 0) start = 0;
    end =  (int)ceil(bbox.flanks()->at(1))  + mPar.trail_est_flank_size;
    if (end >= bbox.foldBox()->length()) end = bbox.foldBox()->length()-1;
    if (end <= start) return 0;

    double s2 = 0;
    if (right)
    {
        for (int x=start,i=0; x<=end; x++, i++)
        {
            double real_val_y = bbox.foldBox()->at(x);
            double theorical_y = a * POW2(SQSECH(pow(2*(x-c)/b-1,d))) + e;
            s2 += POW2(real_val_y-theorical_y);
        }
    }
    s2 += (end-start+1)>0 ? sqrt(s1/(end-start+1)) : 0;

    if (!left) return s2;
    if (!right) return s1;

    return fabs((s1+s2)/2);
}

void ATDDetector::GetMinMaxIter(QPolygonF poly,double* _minx, double *_maxx,double *_miny, double* _maxy)
{
    int Rmin_x = mPar.crop_left; int Rmax_x = mWorkImage->width()-mPar.crop_right;
    int Rmin_y = mPar.crop_top; int Rmax_y = mWorkImage->height()-mPar.crop_bottom;

    //Calcular el mínimo de x e y para recorrer menos puntos
    double minx = 0, maxx = mWorkImage->width()-1, miny=0, maxy = mWorkImage->height()-1;
    for (int i=0; i<poly.count(); i++)
    {
        QPointF p = poly.at(i);
        if (p.x() < minx) minx = p.x();
        if (p.x() > maxx) maxx = p.x();
        if (p.y() < miny) miny = p.y();
        if (p.y() > maxy) maxy = p.y();
    }
    if (minx < Rmin_x) minx = Rmin_x; if (miny < Rmin_y) miny = Rmin_y;
    if (maxx >= Rmax_x) maxx = Rmax_x-1;
    if (maxy >= Rmax_y) maxy = Rmax_y-1;
    *_minx = minx; *_maxx = maxx; *_miny = miny; *_maxy = maxy;

}

void ATDDetector::ComputeGlobalBackground(FitImage* fi)
{
    //Calculamos el fondo
    BackgroundMap bkg; BackgroundParameters par;
    par.bkg_mesh_size = MAXVAL(fi->width(), fi->height());
    bkg.Setup(par.bkg_method, fi->width(), fi->height(),
              par.bkg_mesh_size, par.bkg_smooth,
              par.bkg_lower_conv, par.bkg_upper_conv );
    bkg.Compute(fi->data());
    mGlobalBackground = bkg.back(0,0);
    mGlobalBackgroundSigma = bkg.backentry(0,0)->StandardDev;
    INFO("Computing image global background: %0.3f %0.3f", mGlobalBackground , mGlobalBackgroundSigma);
}

void ATDDetector::FoldBoundigBox(ATDBoundingBox& bb, FitImage* fi, QVector<double>* dt, Transform2D_ER* trans)
{
    bb.showConsole();
    int width = (int)ceil(CART_DIST(bb.p1().x(), bb.p1().y(),bb.p2().x(), bb.p2().y()));
    *trans =
    Transform2D_ER::ComputeTransform
                            (bb.p1().x(), bb.p1().y(),   //Punto inicial del slice de origen
                            bb.p2().x(), bb.p2().y(),   //Puntos final del slice de origen
                            0, bb.height()/2.0,         //Punto izquierdo de la caja abatida de destino
                            width,bb.height()/2.0       //Punto derecho de la caja abatida de destino
                            );
    dt->resize(width);

    //Recorrido del slice de destino
    for (int x=0;x<width;x++)
    {
        dt->data()[x] = 0.0;
        for (int y=0; y<bb.height(); y++)
        {
            double xPrime, yPrime;
            trans->imap(x, y, &xPrime, &yPrime);
            double value = fi->BilinearConvolution(xPrime, yPrime);
            dt->data()[x] += value;
        }
    }
}

QVector<double> ATDDetector::ComputeSignalLevel(ATDBoundingBox& bb)
{
    //Si el objeto tiene flancos calculado, solo calculamos en un entorno alrededor de ellos
    int istart = bb.flanks()->count() ? bb.flanks()->at(0)-mPar.trail_mid_height*5 : 0;
    if (istart < 0) istart = 0;
    int iend = bb.flanks()->count() ? bb.flanks()->at(1)+mPar.trail_mid_height*5 : bb.foldBox()->count()-1;
    if (iend >= bb.foldBox()->count()) iend = bb.foldBox()->count()-1;

    int iters = 3;
    double med  = 0; //Punto medio entre señal y  fondo
    int cur_retry = 0;
    double ignore_over = 10e40; //Ignorar píxeles con valores por encima de este
    //Lo que se hace es dividir en 2 el slice, de manera que habrá m,n puntos por arriba y por abajo
    //Se calcula la media de los puntos que hay por arriba y por debajo
    do
    {
        double min = bb.foldBox()->at(0), max = -1;
        for (int i=istart; i<=iend; i++)
        {
            double val = bb.foldBox()->at(i);
            if (val > ignore_over) continue;
            if (val > max) max = val;
            if (val < min) min = val;
        }
        //Se empieza dividiendo por el punto medio
        med = (max+min)/2;

        //Cuidado, cuento el número de puntos que son señal. Podría haber
        //Una estrela
        int cont_signal = 0;
        for (int i=istart; i<iend; i++)
        {
            if (bb.foldBox()->at(i) > med)
                cont_signal++;
        }
        if (cont_signal < 20)
            ignore_over = med;
        else
            break;
        if(cur_retry++ > 4) break;
    } while (true);

    //Iteramos varias veces para que vaya convergiendo
    double avg_upper, avg_lower; int count_upper, count_lower;
    for (int j=0; j<iters; j++)
    {
        avg_upper = 0, avg_lower = 0; count_upper = 0; count_lower = 0;
        for (int i=istart; i<iend; i++)
        {
            double val = bb.foldBox()->at(i);
            if (val > med)
            {
                avg_upper += val;
                count_upper++;
            }
            else
            {
                avg_lower += val;
                count_lower++;
            }

        }
        avg_lower /= count_lower;
        avg_upper /= count_upper;
        med = (avg_upper+avg_lower)/2;
    }

    //Ahora calculamos la desviación típica de los valores de fondo
    double stdev = 0; int count = 0;
    for (int i=istart; i<=iend; i++)
    {
        double val = bb.foldBox()->at(i);
        if (val > med) continue;
        stdev += POW2(val - avg_lower);
        count++;
    }
    stdev = sqrt(stdev/(count-1));

    QVector<double> vec;
    vec.append(avg_lower);
    vec.append(avg_upper);
    vec.append(stdev);
    vec.append(ignore_over);
    return vec;
}

QVector<double> ATDDetector::ComputeImpreciseFlanks2(ATDBoundingBox& bb, int suggested_width)
{
    if (!suggested_width) return ComputeImpreciseFlanks1(bb);

    QVector<double> flanks;
    flanks.append(0); flanks.append(bb.foldBox()->count()-1);

    double best_signal_sum = 0;
    double best_signal_level = 0;

    //Nos quedamos con el valor de fondo más grande
    for (int i=0;i<bb.foldBox()->count()-suggested_width-1;i++)
    {
        double signal_level = 0;
        for (int j=i; j<i+suggested_width;j++)
            signal_level += bb.foldBox()->data()[j];
        if (signal_level > best_signal_sum)
        {
            flanks[0] = i;
            flanks[1] = i+suggested_width;
            best_signal_sum = signal_level;
            best_signal_level = best_signal_sum/suggested_width;
        }
    }

    //Calcular el nivel de fondo
    double bkg_level = 0;
    int bkg_count = 0;
    for (int i=0;i<flanks[0];i++,bkg_count++)
        bkg_level += bb.foldBox()->data()[i];
    for (int i=(int)flanks[1];i<bb.foldBox()->count();i++,bkg_count++)
        bkg_level += bb.foldBox()->data()[i];
    bkg_level /= bkg_count;

    if (bkg_count > 5)
    {
        bb.setBackground(bkg_level, 0);
        bb.setSignalLevel(best_signal_level);
        //Calcular el signal level around flanks
        double bsum = 0;
        int bcount = 0;
        int it1_start = (int)flanks[0] + mPar.trail_est_flank_size;
        int it2_start = (int)flanks[1] - mPar.trail_est_flank_size;
        for (int i=it1_start;i<it1_start + mPar.trail_est_sig_flanks;i++,bcount++)
        {
            bsum += bb.foldBox()->data()[i];
        }
        for (int i=it2_start;i>it2_start - mPar.trail_est_sig_flanks;i--,bcount++)
        {
            bsum += bb.foldBox()->data()[i];
        }
        bsum /= bcount;
        bb.setSignalLevelAroundFlanks(bsum);
    }

    return flanks;
}

QVector<double> ATDDetector::ComputeImpreciseFlanks(ATDBoundingBox& bb)
{
   QVector<double> res = mPar.trail_est_length ? ComputeImpreciseFlanks2(bb, mPar.trail_est_length) :
                                                 ComputeImpreciseFlanks1(bb);
   return res;
}

bool ATDDetector::ComputeSuperPreciseFlanks(ATDBoundingBox* bb, QVector<double>* flanks, QVector<double>* foo)
{
    QVector<double> flanks_old = *bb->flanks(); QVector<double> foo_old = *bb->foo();
    QVector<double> flanks_left; QVector<double> foo_left;
    QVector<double> flanks_right; QVector<double> foo_right;
    bool ok1 = ComputeSuperPreciseFlanks(bb, &flanks_left, &foo_left, true, false);
    if (!ok1)
    {
        bb->setFlanks(flanks_old);
        bb->setFoo(foo_old);
        return false;
    }
    bool ok2 = ComputeSuperPreciseFlanks(bb, &flanks_right, &foo_right, false, true);
    if (!ok2)
    {
        bb->setFlanks(flanks_old);
        bb->setFoo(foo_old);
        return false;
    }
    //Esctablecemos los nuevos flancos
    flanks->clear();
    flanks->append(flanks_left[0]);
    flanks->append(flanks_right[1]);
    flanks->append((flanks_right[1]+flanks_left[0])/2);
    bb->setFlanks(*flanks);
    foo->clear();
    for (int i=0; i<foo_right.length(); i++)
        foo->append(foo_left[i]);
    for (int i=0; i<foo_right.length(); i++)
       foo->append(foo_right[i]);
    bb->setFoo(*foo);
    return true;
}

bool ATDDetector::ComputeSuperPreciseFlanks(ATDBoundingBox* _bbox,QVector<double>* flanks, QVector<double>* foo, bool left, bool right)
{
    QVector<double> res1; QVector<double> foo1;
    ATDBoundingBox aux_bb = *_bbox;

    //Calculo los puntos del perfil
    double a = _bbox->foo()->at(0);
    double b = _bbox->foo()->at(1);
    double c = _bbox->foo()->at(2);
    double d = _bbox->foo()->at(3);
    double e = _bbox->foo()->at(4);
    QVector<double> fooInitial = *_bbox->foo();


    int bflank1 = (int)aux_bb.flanks()->at(0);
    int bflank2 = (int)aux_bb.flanks()->at(1);

    int slice_1_start = bflank1 + mPar.trail_est_flank_size;
    int slice_1_end = bflank1 + mPar.trail_est_flank_size*3;
    int slice_2_start = bflank2 - mPar.trail_est_flank_size*3;
    int slice_2_end = bflank2 - mPar.trail_est_flank_size;

    double media_left = 0;
    aux_bb.foldBoxStats(slice_1_start, slice_1_end, &media_left, NULL, NULL);
    double media_right = 0;
    aux_bb.foldBoxStats(slice_2_start, slice_2_end, &media_right, NULL, NULL);

    int iwidth = (slice_2_start-slice_1_end)/2;

    if (left)
    {
        for (int i=slice_1_end; i<aux_bb.foldBox()->length(); i++)
        {
            if (i < slice_2_start)
                aux_bb.foldBox()->data()[i] = media_left;
            else
                aux_bb.foldBox()->data()[i] =
                        aux_bb.foldBox()->data()[aux_bb.foldBox()->length()-(i)];
        }
    }
    else if (right)
    {
        for (int i=0; i<slice_2_start; i++)
        {
            aux_bb.foldBox()->data()[i] = media_right;
        }
    }


    //Iteramos en D
    int minD = 2; int maxD = 28;
    bool ok = ComputePreciseFlanks1(&aux_bb, &res1, &foo1, minD);
    aux_bb.setFoo(foo1); aux_bb.setFlanks(res1);

    double hLeft = SquareDifferenceOnFlanks(aux_bb, left, right);
    ok &= ComputePreciseFlanks(&aux_bb, &res1, &foo1, maxD);
    aux_bb.setFoo(foo1); aux_bb.setFlanks(res1);

    double hRight = SquareDifferenceOnFlanks(aux_bb, left, right);

    if (IS_INVALIDF(hLeft) || !IS_VALIDF(hRight) || !ok)
    {
        ERR("Invalid float computing coefficients");
        return false;
    }

    double medD = (maxD+minD)/2;
    while (minD < maxD && (maxD-minD > 1) && fabs(hLeft-hRight) > 0.1)
    {
        medD = (maxD+minD)/2;

        aux_bb.setFoo(fooInitial);
        bool ok = ComputePreciseFlanks(&aux_bb, &res1, &foo1, medD);
        double hMed = SquareDifferenceOnFlanks(aux_bb, left, right);
        if (IS_INVALIDF(hMed) || !ok)
        {
            ERR("Invalid float computing coefficients");
            *foo = fooInitial; aux_bb.setFoo(fooInitial);
            return false;
        }
        if (hMed > hRight)
        {
            minD = medD;
            hLeft = hMed;
        }
        else if (hMed < hRight)
        {
            maxD = medD;
            hRight = hMed;
        }
        else
        {
            ERR("Invalid float computing coefficients");
            *foo = fooInitial; aux_bb.setFoo(fooInitial);
            return false;
        }
    }
    d = medD;


    //Nuevos valores de foo
    foo->clear();
    foo->append(a);foo->append(b);foo->append(c);foo->append(d);foo->append(e);
    *flanks = res1;
    _bbox->setFoo(*foo);


    aux_bb.setFoo(*foo);
    aux_bb.setFlanks(res1);
    SaveSliceProfile(aux_bb, aux_bb.foldBox()->data(), aux_bb.foldBox()->length(), "fixme_please");
    return true;
}

bool ATDDetector::ComputePreciseFlanks(ATDBoundingBox* bb,QVector<double>* flanks, QVector<double>* foo,double suggested_d)
{
    double s1=0, s2=0;
    double h1=0, h2=0;
    QVector<double> res1; QVector<double> foo1;
    bool ok1 = ComputePreciseFlanks1(bb, &res1, &foo1, suggested_d);
    QVector<double> res2; QVector<double> foo2;
    bool ok2 = mPar.trail_est_length ?
                ComputePreciseFlanks2(bb, &res2, &foo2, suggested_d): false;
    if (mPar.trail_est_length && res2.length() == 2 && ok2)
    {
        bb->setFlanks(res2);
        bb->setFoo(foo2);
        *flanks = res2;
        *foo = foo2;
        s2 = ProfileSpearmanCoefficientOnFlanks(*bb);
        bb->setRhoF(s2);
        bb->setRhoL(h1);
        return true;
    }

    bb->setFlanks(res1);
    bb->setFoo(foo1);
    *flanks = res1;
    *foo = foo1;
    s1 = ProfileSpearmanCoefficientOnFlanks(*bb);
    bb->setRhoF(s1);
    bb->setRhoL(h1);
}


QVector<double> ATDDetector::ComputeImpreciseFlanks1(ATDBoundingBox& bb)
{
    QVector<double> flanks;
    flanks.append(0); flanks.append(bb.foldBox()->count()-1);
    int box = 10/mPar.bin;
    double medval = bb.signalLevel() - (bb.signalLevel()-bb.bkg())/3;
    //Comenzamos por el flanco izquierdo
    int iter = 0;
    for (int i=0;i<bb.foldBox()->count()-box-1;i++)
    {
        double sum = 0;
        for (int j=i;j<i+box;j++)
        {
            double val = bb.foldBox()->data()[j];
            if (val > bb.ignoreOver()) continue;
            sum += val;
        }
        sum /= box;
        if (sum > medval)
        {
            if (iter) flanks[0] = i+box/2;
            break;
        }
        iter++;
    }
    //Y ahora el derecho
    iter = 0;
    for (int i=bb.foldBox()->count()-1;i>box+1 && i > flanks[0]+box+1;i--)
    {
        double sum = 0;
        for (int j=i;j>i-box;j--)
        {
            double val = bb.foldBox()->data()[j];
            if (val > bb.ignoreOver()) continue;
            sum += val;
        }
        sum /= box;
        if (sum > medval)
        {
            if (iter) flanks[1] = i+box/2;
            break;
        }
        iter++;
    }
    return flanks;
}


void SquareFlankFit_fx_func2(const alglib::real_1d_array &coef, const alglib::real_1d_array &_x, double &func, void* /*ptr*/)
{
    // f(x,y) = a * sech(((2x-2c)/b-1)^d)^2 + e
    double x = _x[0];
    double a = coef[0];
    double b = coef[1];
    double c = coef[2];
    double d = (int)FROUND(coef[3]);
    double e = coef[4];
    func = a * POW2(SQSECH(pow(2*(x-c)/b-1,d))) + e;
}


void SquareFlankFit_fx_func2X(const alglib::real_1d_array &coef, const alglib::real_1d_array &_x, double &func, void* ptr)
{
    double* ae = (double*)ptr;
    double a = ae[0];
    double e = ae[1];

    double x = _x[0];

    double b = coef[0];
    double c = coef[1];
    double d = coef[2];

    double trail_length = ae[3]-ae[2];

    double trail_est_flank_size = ae[4];

    if (fabs(b-trail_length) > trail_est_flank_size)
        func = 0;
    else
        func = a * POW2(SQSECH(pow(2*(x-c)/b-1,d))) + e;
}

bool ATDDetector::ComputePreciseFlanks1(ATDBoundingBox* bb ,QVector<double>* flanks, QVector<double>* foo,double suggested_d)
{
    alglib::real_2d_array src_points;   //Coordenadas x
    alglib::real_1d_array src_obsdata;  //Valores observacionales
    alglib::real_1d_array c;            //Soluciones del ajuste c0, c1, c2, c3 => bcd
    src_points.setlength(bb->foldBox()->count(), 1);
    src_obsdata.setlength(bb->foldBox()->count());
    for (int x=0;x<bb->foldBox()->count();x++)
    {
        src_points[x][0] = x;
        src_obsdata[x] = bb->foldBox()->data()[x];
    }
    c.setlength(5);
    /*   a => Amplitud del pulso
     b => Ancho del pulso de flanco a flanco (punto de inflexión a punto de inflexión)
    c => Posición del flanco de subida en el eje x (coordenada x del primer punto de inflexión
    d => Pendiente de los flanos (de 6 a 20 son buenos valores)
    e => Pedestal
    */
    double mA = bb->signalLevel() - bb->bkg(); //Amplitud del pulso
    double mB = bb->flanks()->data()[1] - bb->flanks()->data()[0];
    double mC = bb->flanks()->data()[0];
    double mD = suggested_d ? suggested_d : (int)FROUND((mB*mPar.bin)/6); //Esto se ha determinado experimentalmente
    if (mD > 18) mD = 18; if (mD < 5) mD = 5;
    double mE = bb->bkg();

    if (!IS_VALIDF(mA) || !IS_VALIDF(mB) ||!IS_VALIDF(mC) || !IS_VALIDF(mD) || !IS_VALIDF(mE))
    {
        //No válido para fitting
        return false;
    }

    c[0] = mA;
    c[1] = mB;
    c[2] = mC;
    c[3] = (int)FROUND(mD);
    c[4] = mE;

    double epsf = 0;
    double epsx = 0.00001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitstate state;
    alglib::lsfitreport rep;
    double diffstep = 0.001;

    alglib::lsfitcreatef(src_points, src_obsdata, c, diffstep, state);   //Inicialización del ajuste
    alglib::lsfitsetcond(state, epsf, epsx, maxits);                     //Condiciones de parada del ajuste
    alglib::lsfitfit(state,
                     SquareFlankFit_fx_func2,
                     NULL,(void*)this);                                  //Cálculo de los coeficientes
    alglib::lsfitresults(state, info, c, rep);                           //Cuidado!! hay que comprobar si hubo convergencia

    double val_a = c[0];
    double val_b = c[1];
    double val_c = c[2];
    double val_d = (int)FROUND(c[3]);
    double val_e = c[4];

    mA = val_a;
    mB = val_b;
    mC = val_c;
    mD = val_d;
    mE = val_e;

    flanks->clear(); flanks->append(mC); flanks->append(mC+mB);
    foo->clear(); foo->append(mA); foo->append(mB); foo->append(mC); foo->append(mD); foo->append(mE);

    if (flanks->at(0) < 0 && flanks->at(1) < 0) return false;
    if (flanks->at(0) > bb->foldBox()->count() && flanks->at(1) > bb->foldBox()->count()) return false;
    return true;
}

bool ATDDetector::ComputePreciseFlanks2(ATDBoundingBox* bb ,QVector<double>* flanks, QVector<double>* foo,double suggested_d)
{
    alglib::real_2d_array src_points;   //Coordenadas x
    alglib::real_1d_array src_obsdata;  //Valores observacionales
    alglib::real_1d_array c;            //Soluciones del ajuste c0, c1, c2, c3 => bcd
    src_points.setlength(bb->foldBox()->count(), 1);
    src_obsdata.setlength(bb->foldBox()->count());
    for (int x=0;x<bb->foldBox()->count();x++)
    {
        src_points[x][0] = x;
        src_obsdata[x] = bb->foldBox()->data()[x];
    }
    c.setlength(3);
    /*
    a => Amplitud del pulso
    b => Ancho del pulso de flanco a flanco (punto de inflexión a punto de inflexión)
    c => Posición del flanco de subida en el eje x (coordenada x del primer punto de inflexión
    d => Pendiente de los flanos (de 6 a 20 son buenos valores)
    e => Pedestal
    */
    double mA = bb->signalLevelAroundFlanks() - bb->bkg(); //Amplitud del pulso       //Parámetro fijo
    double mB = bb->flanks()->data()[1] - bb->flanks()->data()[0];
    double mC = bb->flanks()->data()[0];
    double mD = suggested_d ? suggested_d : (mB*mPar.bin)/6; //Esto se ha determinado experimentalmente
    if (mD > 18) mD = 18; if (mD < 5) mD = 5;
    double mE = bb->bkg();                                               //Parámetro fijo

    if (!IS_VALIDF(mA) || !IS_VALIDF(mB) ||!IS_VALIDF(mC) || !IS_VALIDF(mD) || !IS_VALIDF(mE) ||
            mA < 0 || mB < 0 || mC < 0 || mD < 0 || mE < 0)
    {
        //No válido para fitting
        return false;
    }

    //Guardo A, E que son fijas y los flancos que son ligéramente móviles
    double ae[6] = {mA, mE, bb->flanks()->at(0), bb->flanks()->at(1),
                   (double)mPar.trail_est_flank_size,
                   (double)mPar.trail_est_sig_flanks};

    c[0] = mB;
    c[1] = mC;
    c[2] = (int)FROUND(mD);

    double epsf = 0;
    double epsx = 0.00001;
    alglib::ae_int_t maxits = 0;
    alglib::ae_int_t info;
    alglib::lsfitstate state;
    alglib::lsfitreport rep;
    double diffstep = 0.001;

    alglib::lsfitcreatef(src_points, src_obsdata, c, diffstep, state);   //Inicialización del ajuste
    alglib::lsfitsetcond(state, epsf, epsx, maxits);                     //Condiciones de parada del ajuste
    alglib::lsfitfit(state,
                     SquareFlankFit_fx_func2X,
                     NULL,(void*)ae);                                  //Cálculo de los coeficientes
    alglib::lsfitresults(state, info, c, rep);                           //Cuidado!! hay que comprobar si hubo convergencia


    double val_b = c[0];
    double val_c = c[1];
    double val_d = c[2];


    mA = mA;
    mB = val_b;
    mC = val_c;
    mD = (int)FROUND(val_d);
    mE = mE;

    flanks->clear(); flanks->append(mC); flanks->append(mC+mB);
    foo->clear(); foo->append(mA); foo->append(mB); foo->append(mC); foo->append(mD); foo->append(mE);

    if (flanks->at(0) < 0 && flanks->at(1) < 0) return false;
    if (flanks->at(0) > bb->foldBox()->count() && flanks->at(1) > bb->foldBox()->count()) return false;
    return true;
}

ATDBoundingBox ATDDetector::ResizeBox(ATDBoundingBox& bb,int  sz_left, int sz_right)
{
    double xp1, yp1, xp2, yp2;

    bb.transform()->imap(0 - sz_left, bb.height()/2, &xp1, &yp1);
    bb.transform()->imap(bb.foldBox()->count()+sz_right, bb.height()/2, &xp2, &yp2);
    ATDBoundingBox atb = ATDBoundingBox(QPointF(xp1,yp1), QPointF(xp2, yp2), bb.height());
    atb.setBackground(bb.bkg(), bb.bkgSigma());
    atb.setSignalLevel(bb.signalLevel());

    //Recalculamos la transformación de rotación
    int width = (int)ceil(CART_DIST(atb.p1().x(), atb.p1().y(),atb.p2().x(), atb.p2().y()));
    Transform2D_ER trans =
    Transform2D_ER::ComputeTransform
                            (atb.p1().x(), atb.p1().y(),   //Punto inicial del slice de origen
                            atb.p2().x(), atb.p2().y(),   //Puntos final del slice de origen
                            0, atb.height()/2.0,         //Punto izquierdo de la caja abatida de destino
                            width,atb.height()/2.0       //Punto derecho de la caja abatida de destino
                            );
    atb.setTransform(trans);
    return atb;
}

ATDBoundingBox ATDDetector::AdjustToFlanks(ATDBoundingBox& bb)
{
    int margin = mPar.trail_mid_height * 5;
    double xp1, yp1, xp2, yp2;
    //Calcular los nuevos puntos izquierdo y derecho
    double x_left = bb.flanks()->data()[0] - margin;
    if (x_left < 0)
        x_left = 0;
    double x_right = bb.flanks()->data()[1] + margin;
    if (x_right >= bb.foldBox()->count())
        x_right = bb.foldBox()->count()-1;

    bb.transform()->imap(x_left, bb.height()/2, &xp1, &yp1);
    bb.transform()->imap(x_right, bb.height()/2, &xp2, &yp2);
    ATDBoundingBox atb = ATDBoundingBox(QPointF(xp1,yp1), QPointF(xp2, yp2), bb.height());
    atb.setBackground(bb.bkg(), bb.bkgSigma());
    atb.setSignalLevel(bb.signalLevel());

    //Recalculamos la transformación de rotación
    int width = (int)ceil(CART_DIST(atb.p1().x(), atb.p1().y(),atb.p2().x(), atb.p2().y()));
    Transform2D_ER trans =
    Transform2D_ER::ComputeTransform
                            (atb.p1().x(), atb.p1().y(),   //Punto inicial del slice de origen
                            atb.p2().x(), atb.p2().y(),   //Puntos final del slice de origen
                            0, atb.height()/2.0,         //Punto izquierdo de la caja abatida de destino
                            width,atb.height()/2.0       //Punto derecho de la caja abatida de destino
                            );
    atb.setTransform(trans);
    return atb;
}

void ATDDetector::SaveSliceProfile(ATDBoundingBox& bb,double* data, int datalen,QString suffix)
{
    int width = IMG_PROFILE_WIDTH;
    int height = IMG_PROFILE_HEIGHT;
    int margin = IMG_PROFILE_MARGIN;
    if (!margin) margin = height/10;
    QImage img(width, height, QImage::Format_RGB32);
    img.fill(Qt::white);
    QPainter painter(&img);
    painter.setPen(QPen(Qt::black));
    painter.setBrush(QBrush(Qt::black));

    //Calculo el mínimo y el máximo
    double min = data[0], max = -1;
    for (int i=0; i<datalen; i++)
    {
        if (data[i] < min) min = data[i];
        if (data[i] > max) max = data[i];
    }

    double scale_x = width/(double)datalen;
    double scale_y = (height-margin*2)/(max-min);

    QPointF last(0,0); QVector<QPointF> points;
    for (int i=0; i<datalen; i++)
    {
        double x = i * scale_x;
        double y = (height - (data[i]-min) * scale_y) - margin;
        if (last.x() > 0) points.append(QPointF(x,y)); //painter.drawLine(last, QPointF(x,y));
        last = QPointF(x,y);
    }
    points.append(QPointF(width+1, points.last().y()));
    points.append(QPointF(width+1, height));
    points.append(QPointF(-1, height));
    points.append(QPointF(-1, points.at(0).y()));
    points.append(points.at(0));

    //painter.setPen(QPen(QColor::fromRgb(16,124,244),1));
    //painter.setBrush(QBrush(QColor::fromRgb(16, 124, 244,32)));
    painter.setPen(QPen(Qt::black));
    painter.setBrush(QBrush(Qt::white));
    painter.drawPolygon(points.data(), points.count(), Qt::OddEvenFill);

    //Dibujo la línea de background
    painter.setPen(QPen(Qt::red, 0.5, Qt::DashLine));
    QPointF p1(0, (height - (bb.bkg()-min) * scale_y) - margin);
    QPointF p2(width, p1.y());
    painter.drawLine(p1,p2);
    p1=QPointF(0, (height - (bb.signalLevel()-min) * scale_y) - margin);
    p2=QPointF(width, p1.y());
    painter.drawLine(p1,p2);

    //Dibujar los flancos y la línea central
    p1 = QPointF(bb.flanks()->data()[0]*scale_x, 0);
    p2 = QPointF(bb.flanks()->data()[0]*scale_x, height);
    painter.drawLine(p1,p2);
    p1 = QPointF(bb.flanks()->data()[1]*scale_x, 0);
    p2 = QPointF(bb.flanks()->data()[1]*scale_x, height);
    painter.drawLine(p1,p2);
    p1 = QPointF((bb.flanks()->data()[1]+bb.flanks()->data()[0])/2.0*scale_x, 0);
    p2 = QPointF((bb.flanks()->data()[1]+bb.flanks()->data()[0])/2.0*scale_x, height);
    painter.drawLine(p1,p2);

    //Dibujar la función de ajuste        
    if (bb.foo() && bb.foo()->count())
    {
        double a1,b1,c1,d1,e1,a2,b2,c2,d2, e2;
        QPen pen1 = QPen(Qt::blue, 2, Qt::SolidLine), pen2 = QPen(Qt::blue, 2, Qt::SolidLine);
        a1 = a2 = bb.foo()->at(0);
        b1 = b2 = bb.foo()->at(1);
        c1 = c2 = bb.foo()->at(2);
        d1 = d2 = bb.foo()->at(3);
        e1 = e2 = bb.foo()->at(4);
        if (bb.foo()->length() > 5)
        {
            a2 = bb.foo()->at(5);
            b2 = bb.foo()->at(6);
            c2 = bb.foo()->at(7);
            d2 = bb.foo()->at(8);
            e2 = bb.foo()->at(9);
            pen2 = QPen(Qt::red, 2, Qt::SolidLine);
        }

        painter.setPen(pen1);
        QPointF last = QPointF(0,0);

        double a=a1,b=b1,c=c1,d=d1,e=e1;
        for (int i=0; i<width; i++)
        {
            if (i == (int)p1.x())
            {
                a=a2; b=b2; c=c2; d=d2; e=e2;
                painter.setPen(pen2);
            }
            double x = i/scale_x; //f(x) = a * sech(( (2(x-c))/b - 1)^d)^2 + e
            double y = a * POW2(SQSECH(pow((2*x-2*c)/b-1,d))) + e;
            QPointF p = QPointF(i, (height - (y-min) * scale_y) - margin);
            if (last.x() != 0 && last.y() != 0)
                painter.drawLine(last, p);
            last = p;
        }
    }

    painter.end();
    img.save(GetOutputFilename("profile_"+suffix,mPar.output_profile_img_format));
}


TRAIL_DATA ATDDetector::ComputeTrailData(ATDBoundingBox& bbox)
{
    TRAIL_DATA td; bool ok;
    td.id = bbox.id();
    //Fecha
    td.dateFrame = MpcUtils::GetDateFrame(mWorkImage->headers());
    //Centro de la traza
    bbox.transform()->imap((bbox.flanks()->at(0)+bbox.flanks()->at(1))/2, bbox.height()/2.0, &td.cX, &td.cY);
    mWorkImage->plate()->map(td.cX, td.cY, &td.cRA, &td.cDEC);
    //Longitud de la traza en píxeles
    td.length = bbox.flanks()->at(1) - bbox.flanks()->at(0);
    //Velocidad de la traza
    FitHeader fh = mWorkImage->headers()->Contains("EXPTIME");
    double exp = fh.value().toFloat(&ok);
    if (!ok) td.speed = INVALID_FLOAT;
    else td.speed = td.length * mWorkImage->plate()->scale() / exp;
    //Ángulo de fase y posición J2000 de los extremos
    double x1, y1, x2, y2;
    bbox.transform()->imap(bbox.flanks()->at(0), bbox.height()/2.0, &x1, &y1);
    bbox.transform()->imap(bbox.flanks()->at(1), bbox.height()/2.0, &x2, &y2);
    mWorkImage->plate()->map(x1,  y1, &td.f1RA, &td.f1DEC);
    mWorkImage->plate()->map(x2,  y2, &td.f2RA, &td.f2DEC);
    td.pa = Util::PosAngle(td.f1RA, td.f1DEC, td.f2RA, td.f2DEC);
    //Polígono envolvente
    td.bbox = bbox.polygon();
    //Flujo de la traza. Asumimos como fondo el pedestal de la función foo ajustada
    double bkg = bbox.foo()->at(4);
    td.flux = 0;
    for (int i=0; i<bbox.foldBox()->count(); i++) td.flux += bbox.foldBox()->at(i) - bkg;
    //Elementos lineales
    td.r = bbox.regr().r();
    td.s = bbox.regr().s();
    td.rho = bbox.rhoF();
    td.pixel_count = bbox.pointCount();
    //Otros
    td.snr = bbox.sNr();    
    return td;
}








