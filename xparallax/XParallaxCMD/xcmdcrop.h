#ifndef XCMDCROP_H
#define XCMDCROP_H

#include <QObject>
#include <QCoreApplication>
#include "xcmd.h"
#include "fitimage.h"
#include "util.h"


class XCmdCrop : public XCmd
{
    public:
        XCmdCrop(QCoreApplication *parent);
        ~XCmdCrop();

protected:
    virtual QString helpRes();
    virtual QString iniEx();

public slots:
    void run();
};

#endif // XCMDCROP_H
