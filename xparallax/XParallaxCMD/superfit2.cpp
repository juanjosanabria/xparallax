#include <QImage>
#include <QPainter>
#include  <QPen>
#include "superfit2.h"
#include "util.h"
#include "statistics.h"
#include "interpolation.h"

SuperFit2::SuperFit2(SuperFit left, SuperFit right)
{
    //Estos dos valores no se deberían usar en este ajuste
    mBkg = (left.getBkg()+right.getBkg())/2;
    mSignal = (left.getSignalLevel()+right.getSignalLevel())/2;

    mDataExp = left.getData();
    mFitLeft.append(left.getA());mFitLeft.append(left.getB());
    mFitLeft.append(left.getC());mFitLeft.append(left.getD());mFitLeft.append(left.getE());
    mFitRight.append(right.getA());mFitRight.append(right.getB());
    mFitRight.append(right.getC());mFitRight.append(right.getD());mFitRight.append(right.getE());
    mStart1 = left.getFitStart(); mEnd1 = left.getfitEnd();
    mStart2 = right.getFitStart(); mEnd2 = right.getfitEnd();
    mRho1 = left.rho();
    mRho2 = right.rho();
}

void SuperFit2::saveImage(QString filename,int start, int end)
{
    int datalen = mDataExp.length();
    if (start == -1 || end == -1)
    {
        start = 0;
        end = datalen-1;
    }
    if (end >= datalen) end = datalen-1;

    int width = IMG_PROFILE_WIDTH;
    int height = IMG_PROFILE_HEIGHT;
    int margin = IMG_PROFILE_MARGIN;
    double* data = mDataExp.data();
    if (!margin) margin = height/10;
    QImage img(width, height, QImage::Format_RGB32);
    img.fill(Qt::white);
    QPainter painter(&img);
    painter.setPen(QPen(Qt::black));
    painter.setBrush(QBrush(Qt::black));


    //Calculo el mínimo y el máximo
    double min = data[0], max = -1;
    for (int i=0; i<datalen; i++)
    {
        if (data[i] < min) min = data[i];
        if (data[i] > max) max = data[i];
    }

    double scale_x = width/(double)datalen;
    double scale_y = (height-margin*2)/(max-min);

    QPointF last(0,0); QVector<QPointF> points;
    for (int i=0; i<datalen; i++)
    {
        double x = i * scale_x;
        double y = (height - (data[i]-min) * scale_y) - margin;
        if (last.x() > 0) points.append(QPointF(x,y)); //painter.drawLine(last, QPointF(x,y));
        last = QPointF(x,y);
    }
    points.append(QPointF(width+1, points.last().y()));
    points.append(QPointF(width+1, height));
    points.append(QPointF(-1, height));
    points.append(QPointF(-1, points.at(0).y()));
    points.append(points.at(0));

    //painter.setPen(QPen(QColor::fromRgb(16,124,244),1));
    //painter.setBrush(QBrush(QColor::fromRgb(16, 124, 244,32)));
    painter.setPen(QPen(Qt::black));
    painter.setBrush(QBrush(Qt::white));
    painter.drawPolygon(points.data(), points.count(), Qt::OddEvenFill);

    //Dibujo la línea de background
    painter.setPen(QPen(Qt::red, 0.5, Qt::DashLine));
    QPointF p1(0, (height - (mBkg-min) * scale_y) - margin);
    QPointF p2(width, p1.y());
    painter.drawLine(p1,p2);
    p1=QPointF(0, (height - (mSignal-min) * scale_y) - margin);
    p2=QPointF(width, p1.y());
    painter.drawLine(p1,p2);

    //Dibujar los flancos y la línea central
    p1 = QPointF(flank1()*scale_x, 0);
    p2 = QPointF(flank1()*scale_x, height);
    painter.drawLine(p1,p2);
    p1 = QPointF(flank2()*scale_x, 0);
    p2 = QPointF(flank2()*scale_x, height);
    painter.drawLine(p1,p2);
    p1 = QPointF((flank2()+flank1())/2.0*scale_x, 0);
    p2 = QPointF((flank2()+flank1())/2.0*scale_x, height);
    painter.drawLine(p1,p2);

    //Dibujar la función de ajuste por la parte izquierda
    last.setX(0); last.setY(0);
    painter.setPen(QPen(Qt::blue, 2, Qt::SolidLine));
    for (int i=(int)floor(mStart1*scale_x); i<mEnd1*scale_x; i++)
    {
        double x = i/scale_x;
        double y = theo(x);
        QPointF p = QPointF(i, (height - (y-min) * scale_y) - margin);
        if (last.x() != 0 && last.y() != 0)
            painter.drawLine(last, p);
        last = p;
    }

    last.setX(0); last.setY(0);
    for (int i=(int)floor(mStart2*scale_x); i<mEnd2*scale_x; i++)
    {
        double x = i/scale_x;
        double y = theo(x);
        QPointF p = QPointF(i, (height - (y-min) * scale_y) - margin);
        if (last.x() != 0 && last.y() != 0)
            painter.drawLine(last, p);
        last = p;
    }

    painter.end();
    img.save(filename, "png");
}
