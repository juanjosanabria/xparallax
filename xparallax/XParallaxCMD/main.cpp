#include <QTimer>
#include <QPixmap>
#include <QProcessEnvironment>
#include "xcmdastrometry.h"
#include "xcmdtraildetector.h"
#include "xcmdcalibration.h"
#include "xcmdcrop.h"
#include "xcmdstardetector.h"
#include "xglobal.h"

#ifdef Q_OS_WIN32
#include <windows.h>
#include <iostream>
#else
#include <locale.h>
#endif

int main(int argc, char *argv[])
{
    QProcess process;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
 #ifdef Q_OS_WIN
    QString cur_path = env.value("PATH","");
    env.insert("PATH",QString(".")+(cur_path.length()?":":""));
 #endif
    process.setProcessEnvironment(env);

    //Inicializaciones generales
    QCoreApplication app(argc, argv);
    XGlobal::initialize();
    XGlobal::LOGGER->setOutputDebug(false);
    XCmd* cmd = NULL;

    if (app.arguments().contains("-dbg"))
        XGlobal::DEBUG = true;

    //El padre es la aplicación, por lo que será borrado por ella
    if (app.arguments().contains("-ast"))               //*ASTROMETRÍA
        cmd = new XCmdAstrometry(&app);
    else if (app.arguments().contains("-tde"))          //*TRAIL DETECTION
        cmd = new XCmdTrailDetector(&app);
    else if (app.arguments().contains("-cal"))          //*IMAGE CALIBRATION
        cmd = new XCmdCalibration(&app);
    else if (app.arguments().contains("-crop"))         //*IMAGE CROP (NO IMPLEMENTADO)
        cmd = new XCmdCrop(&app);
    else if (app.arguments().contains("-sde"))         //*STAR DETECTOR
        cmd = new XCmdStarDetector(&app);

    if (app.arguments().contains("-err"))
    {
        XCmd::showError(-1);
        exit(0);
    }

    if (!cmd)
    {
        XCmd::showtFile(":/help/res/xparallaxcmd.txt");
        exit(0);
    }

    //Si se ha solicitado mostrar la ayuda mostramos la ayuda de este comando
    if (app.arguments().contains("-h") || app.arguments().contains("-usage") || app.arguments().contains("-help"))
    {
        if (cmd) cmd->showHelp();
        else XCmd::showtFile(":/help/res/xparallaxcmd.txt");
        exit(0);
    }
    //Si se ha solicitado el archivo de ini de ejemplo
    if (app.arguments().contains("-iex"))
    {
        cmd->showIniExample();
        exit(0);
    }

    // This will cause the application to exit when
    // the task signals finished.
    QObject::connect(cmd, SIGNAL(finished()), &app, SLOT(quit()));

    // This will run the task from the application event loop.
    QTimer::singleShot(0, cmd, SLOT(run()));

    return app.exec();
}



