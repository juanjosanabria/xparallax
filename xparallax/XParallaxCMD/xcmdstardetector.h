#ifndef XCMDSTARDETECTOR_H
#define XCMDSTARDETECTOR_H

#include "xcmd.h"
#include "xglobal.h"
#include "fitimage.h"
#include "detection.h"

class XCmdStarDetector : public XCmd
{
private:
    QString         mInputFilename;
    QString         mOutputFilename;
    FitImage*       mFitImage;
    FILE*           mOutputFile;
    StarDetector*   mDetector;
    DetectionParameters mParameters;

protected:
    QString helpRes(){return ":/help/res/xcmdtraildetector.txt"; }
    QString iniEx() { return ":/help/res/xcmdtraildetector.ini"; }

public:
    XCmdStarDetector(QCoreApplication* app);
    ~XCmdStarDetector();

public slots:
    void run();

};

#endif // XCMDSTARDETECTOR_H
