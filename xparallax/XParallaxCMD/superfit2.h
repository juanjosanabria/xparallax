#ifndef SUPERFIT2_H
#define SUPERFIT2_H

#include "superfit.h"

class SuperFit2 : SuperFit {

protected:
    QVector<double> mFitLeft;
    QVector<double> mFitRight;
    double          mRho1;
    double          mRho2;
    //Rangos de los flancos derecho e izquierdo
    double          mStart1;
    double          mEnd1;
    double          mStart2;
    double          mEnd2;

public:
    SuperFit2(SuperFit left, SuperFit right);

    double getA1(){return mFitLeft[0];}
    double getB1(){return mFitLeft[1];}
    double getC1(){return mFitLeft[2];}
    double getD1(){return mFitLeft[3];}
    double getE1(){return mFitLeft[4];}

    double getA2(){return mFitRight[0];}
    double getB2(){return mFitRight[1];}
    double getC2(){return mFitRight[2];}
    double getD2(){return mFitRight[3];}
    double getE2(){return mFitRight[4];}

    virtual double flank1(){return getC1();}
    virtual double flank2(){return getC2()+getB2();}
    QVector<double> flanks(){return QVector<double>({flank1(), flank2()});}

    virtual double theo(double x){return x < mEnd1 ?
                    getA1() * POW2(SQSECH(pow(2*(x-getC1())/getB1()-1,getD1()))) + getE1() :
                    getA2() * POW2(SQSECH(pow(2*(x-getC2())/getB2()-1,getD2()))) + getE2() ;}    
    virtual void saveImage(QString filename,int start=-1, int end=-1);
    virtual double rho(){return (mRho1+mRho2)/2;}

};


#endif // SUPERFIT2_H
