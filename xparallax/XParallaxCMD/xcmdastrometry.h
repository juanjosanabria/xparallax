#ifndef XCMDASTROMETRY_H
#define XCMDASTROMETRY_H

#include <QObject>
#include <QCoreApplication>
#include "xcmd.h"
#include "fitimage.h"
#include "util.h"
#include "astrometry.h"

class XCmdAstrometry : public XCmd
{
    Q_OBJECT
public:
    explicit XCmdAstrometry(QCoreApplication *parent);
    ~XCmdAstrometry();

private:
    int                     readCmdLine();
    QString                 mInputFile;          //Imagen fit de entrada
    QString                 mOutputFile;         //Imagen fit de salida
    QString                 mRefFile;            //Fichero de resultados con la tabla de estrellas de referencia
    FitImage*               mFitImage;
    QString                 mIniFile;
    QString                 mRa;
    QString                 mDec;
    QString                 mPixScale;
    WorkFileList            mWorkFileList;
    DetectionParameters     mDetectionPar;
    MatchParameters         mMatchPar;
    AstrometryComputer*     mAstrometryComputer;
    QString                 mEquinox;           //"J2000" ó "EQUINOX" de las coordenadas del centro

    bool saveReferenceStars(AstrometryComputer* ac);

protected:
    virtual QString helpRes();
    virtual QString iniEx();

signals:

public slots:
    void run();

};

#endif // XCMDASTROMETRY_H
