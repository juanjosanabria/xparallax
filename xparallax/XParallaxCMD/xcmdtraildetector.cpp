#include "xcmdtraildetector.h"
#include "xglobal.h"
#include "detection.h"
#include "mpc.h"
#include "g2d.h"
#include "plateconstants.h"
#include "vo.h"
#include "profiling.h"
#include "license.h"
#include <QFileInfo>
#include <QPainter>
#include <QPainterPath>
#include <QPixmap>
#include <QImage>
#include <QDir>
#include <QThread>
#include <advancedtraildetector.h>
#include <QHostInfo>
#include <QDirIterator>

#ifdef QT_DEBUG
//#define REQUESTS_URL    "localhost:8080"
#define REQUESTS_URL    "www.xparallax.com"
#else
#define REQUESTS_URL    "www.xparallax.com"
#endif

XCmdTrailDetector::XCmdTrailDetector(QCoreApplication *parent) :
    XCmd(parent)
{
    mFitImage = NULL;
    //Inicializar fechas
    QDateTime dt_today = QDateTime::currentDateTime();
    if (dt_today.time().hour() < 17)
      dt_today = dt_today.addDays(-1);
    QDateTime dt_yesterday = dt_today.addDays(-1);
    mDateToday = dt_today.toString("yyyyMMdd");
    mDateYesterday = dt_yesterday.toString("yyyyMMdd");
}

XCmdTrailDetector::~XCmdTrailDetector()
{
    if (mFitImage) delete mFitImage;
    mFitImage = NULL;
}

QString XCmdTrailDetector::helpRes()
{
  return ":/help/res/xcmdtraildetector.txt";
}

QString XCmdTrailDetector::iniEx()
{
    return ":/help/res/xcmdtraildetector.ini";
}


void XCmdTrailDetector::run()
{
    INFO("Trail detection v%s", PTQ(QString(ATD_VERSION)));

    // Lectura de línea de comandos y de archivo INI
    //----------------------------------------------------------------------------
    INFO("Trail detection: %s v%s", XPROGRAM, XVERSION);
    int ret = readCmdLine();
    if (ret)  return parent()->exit(ret);

    INFO("Loading INI file: %s", PTQ(mIniFile));
    mDetParams.Load(settings(),"TRAILS");

    // Apertura de archivo de entrada
    //----------------------------------------------------------------------------
    INFO("Opening input file: %s", PTQ(mInputFile));
    mFitImage = FitImage::OpenFile(mInputFile);
    if (!mFitImage || mFitImage->error())
    {
        ERR("Error reading input file");
        return parent()->exit(ERR_UNABLE_OPEN_INPUT_FILE);
    }
    mFitImage->LookForPlate();
    if (!mFitImage->plate())
    {
        ERR("Error, no plate constants for image");
        return parent()->exit(ERR_NO_PLATE_CONSTANTS);
    }

    ATDDetector atd(mFitImage);
    atd.setParams(mDetParams);

    //Comprobar número de archivos tratados
    QString wpath = atd.workingPath();
    QString fname = QFileInfo(wpath).fileName();
    int total_proc = GetProcessedFileCount(QFileInfo(wpath).absolutePath());
    DINFO("Quota %d || Proc: %d",XGlobal::LIC_QUOTA, total_proc);
    if (total_proc > XGlobal::LIC_QUOTA)
    {
        ERR("ERROR: Image quota exceeded");
        return parent()->exit(ERR_LIC_EXCEEDED);
    }

    INFO("*********** DETECTING IN BOX ***********");
    mTrailData = atd.DetectBox();
    setRegString("PFILE_" + mDateToday + "_" + fname,
              QDateTime().currentDateTime().toString("yyyy-MM-dd HH:mm:ss"));
    PrintTrailInfo();

    INFO("Trail detection finished");
    emit finished();
}


int XCmdTrailDetector::readCmdLine()
{
    //Comprobar primero la licencia
    DownloadLicenses(false);
    if (!CheckLicense())
    {
        DownloadLicenses(true);
        if (!CheckLicense()) return ERR_INVALID_LICENSE;
    }
    ReportData();

    mIniFile = QDir::cleanPath(argValueStr("-ini"));
    if (mIniFile.length() && !QFileInfo(mIniFile).exists())
    {
        ERR("Ini file '%s' does not exits", PTQ(mIniFile));
        return ERR_INI_FILE;
    }
    mInputFile = QDir::cleanPath(argValueStr("-i"));
    if (mInputFile.length() && !QFileInfo(mInputFile).exists())
    {
        ERR("Input file '%s' does not exits", PTQ(mInputFile));
        return ERR_UNABLE_OPEN_INPUT_FILE;
    }
    if (!mInputFile.length())
    {
        ERR("Please, specify input file");
        return ERR_INVALID_INPUT_PARAMETERS;
    }
    mOutputPath = argValueStr("-o");
    if (!mOutputPath.length()) mOutputPath = QFileInfo(mInputFile).path();
    else mOutputPath = QDir::cleanPath(mOutputPath);
    /*
    if (QDir(mOutputPath).exists())
    {
        ERR("Output path does not exist: %s", PTQ(mOutputPath));
        return ERR_INVALID_INPUT_PARAMETERS;
    }*/
    mOutputFile = argValueStr("-of");
    if (mOutputFile.length() && mOutputPath.length() && QFileInfo(mOutputFile).fileName().length() == mOutputFile.length())
    {
        mOutputFile = QDir::cleanPath(mOutputPath + QDir::separator() + QFileInfo(mOutputFile).fileName());
        INFO("Output file: %s", PTQ(mOutputFile));
    }
    INFO("Output fields: %s",  PTQ(argValueStr("-f")));
    return 0;
}


void XCmdTrailDetector::PrintTrailInfo()
{
    //Imprimir parámetros solicitados de cada trail
    if (argExists("-f"))
    {
        char separator = ';';
        QStringList op = argValueStr("-f").trimmed().split(",");
        for (int i=0; i<op.count(); i++)
        {
            op[i] = op[i].trimmed().toUpper();
            if (!op[i].length()) {op.removeAt(i);i--;}
        }
        INFO("Output fields: %d : %s", op.count(), PTQ(argValueStr("-f")));
        for (int i=0; i<mTrailData.count(); i++)
        {
            TRAIL_DATA* dt = mTrailData.data()+i;
            XStringBuilder sb; sb.setCapacity(1024);
            for (int j=0;j<op.count(); j++)
            {
                if (j) sb.append(separator);
                QStringList prm = op.at(j).split('%');
                OutputField(&sb, dt, prm[0], prm.count()>1?prm[1].toInt():-1);
            }
            INFO("TDATA_%03d:%s", dt->id, sb.buffer());
            //Si existe archivo de salida imprimimos
            if (mOutputFile.length())
            {
                QFile fil(mOutputFile) ;
                if (!fil.open(QFile::Append|QFile::Text))
                {
                    ERR("Unable to open output file: %s", PTQ(mOutputFile));
                    return;
                }
                else
                {
                    QTextStream ts(&fil);
                    ts << sb.toString() << "\n";
                    fil.close();
                }
            }
        }
    }
}


void XCmdTrailDetector::OutputField(XStringBuilder* xsb, TRAIL_DATA* dt, const QString& field, int precission)
{
    //Mostrar los campos solicitados
    char bfmt[32];
    if (field == "ID")
    {
        xsb->appendFormat("%02d", dt->id);
    }
    else if (field == "JD")
    {
        sprintf(bfmt,"%%.%df",precission==-1?8:precission);
        if (dt->dateFrame == INVALID_DATETIME)
        {
            ERR("No date found for image. Unable to compute [JD] field");
            xsb->append("#");
        }
        else xsb->appendFormat(bfmt, Util::JulianDate(dt->dateFrame));
    }
    else if (field == "MPCDATE")
    {
        if (precission==-1) precission = 5;
        if (dt->dateFrame == INVALID_DATETIME)
        {
            ERR("No date found for image. Unable to compute [JD] field");
            xsb->append("#");
        }
        else xsb->appendFormat("%s", PTQ(Util::Date2MPCString(dt->dateFrame,precission)));
    }
    else if (field == "DATE")
    {
        if (precission==-1) precission = 5;
        if (dt->dateFrame == INVALID_DATETIME)
        {
            ERR("No date found for image. Unable to compute [DATE] field");
            xsb->append("#");
        }
        else xsb->appendFormat("%s", PTQ(dt->dateFrame.toString("yyyy-MM-ddTHH:mm:ss.zzz")));
    }
    else if (field == "LENGTH")
    {
        //Longitud de la traza en píxeles
        xsb->appendFormat("%0.2f", dt->length);
    }
    else if (field == "C_RA")
    {
        if (precission==-1) precission = 1;
        double ra,dec;
        mFitImage->plate()->map(mFitImage->width()/2.0, mFitImage->height()/2.0, &ra, &dec);
        xsb->appendFormat("%s", PTQ(Util::RA2String(ra, precission)));
    }
    else if (field == "C_DEC")
    {
        if (precission==-1) precission = 1;
        double ra,dec;
        mFitImage->plate()->map(mFitImage->width()/2.0, mFitImage->height()/2.0, &ra, &dec);
        xsb->appendFormat("%s", PTQ(Util::DEC2String(dec, precission)));
    }
    else if (field == "FLUX")
    {
        if (precission==-1) precission=2;
        sprintf(bfmt,"%%.%df",precission);
        xsb->appendFormat(bfmt, dt->flux);
    }
    else if (field == "AREA")
    {
        xsb->appendFormat("%d", dt->area);
    }
    else if (field == "RA")
    {
        if (precission==-1) precission = 1;
        xsb->appendFormat("%s", PTQ(Util::RA2String(dt->cRA, precission)));
    }
    else if (field == "DEC")
    {
        if (precission==-1) precission = 2;
        xsb->appendFormat("%s", PTQ(Util::DEC2String(dt->cDEC, precission)));
    }
    else if (field == "SPEED") //Velocidad en arcosegundos por minuto
    {
        double exptime; bool ok;
        FitHeader fhexp = mFitImage->headers()->Contains("EXPTIME");
        if (!fhexp.isValid() || ((exptime = fhexp.value().toDouble(&ok)) == 0) || !ok)
        {
            ERR("Invalid exposure time found for image. Unable to compute [SPEED] field");
            xsb->append("#");
        }
        else
        {
            if (precission==-1) precission=1;
            sprintf(bfmt,"%%.%df",precission);
            xsb->appendFormat(bfmt, dt->speed);
        }
    }
    else if (field == "PA") //Ángulo de posición respecto al norte en grados
    {
        if (precission==-1) precission=2;
        sprintf(bfmt,"%%.%df",precission);
        xsb->appendFormat(bfmt, dt->pa);

    }
    else if (field == "MAG") //Magnitud aproximada
    {
        FitHeader zp = mFitImage->headers()->Contains("MAGZEROP");
        double zerop; bool ok;
        if (!zp.isValid() || ((zerop = zp.value().toDouble(&ok))<5) || !ok)
        {
            ERR("Invalid MAGZEROP header. Unable to compute [MAG] field");
            xsb->append("#");
        }
        else
        {
           if (precission==-1) precission=1;
           double mag = -2.5 * log10(dt->flux) + zerop;
           sprintf(bfmt,"%%.%df",precission);
           xsb->appendFormat(bfmt, mag);
        }
    }
    else if (field == "POLY") //Polígono envolvente
    {
        for (int i=0; i<dt->bbox.length(); i++)
        {
            if (i) xsb->append("/");
            xsb->appendFormat("%8.3f,%8.3f",dt->bbox[i].x(),dt->bbox[i].y());
        }
    }
    else if (field == "S" || field == "R") //Coeficiente de correlación de spearman
    {
        xsb->appendFormat("%6.4f",field == "S" ? dt->s : dt->r);
    }
    else if (field == "PC")
    {
        xsb->appendFormat("%d",dt->pixel_count);
    }
    else if (field == "SNR")
    {
        xsb->appendFormat("%7.4f", dt->snr);
    }
    else if (field == "RHO")
    {
        xsb->appendFormat("%7.4f", dt->rho);
    }
    else if (field.startsWith("$"))
    {
        FitHeader fh = mFitImage->headers()->Contains(field.mid(1));
        xsb->appendFormat("%s", PTQ(fh.value().trimmed()));
    }
    else
    {
        ERR("Unknown field format: %s", PTQ(field));
        return parent()->exit(ERR_TRAIL_BAD_FORMAT_FIELD);
    }
}

void XCmdTrailDetector::DownloadLicenses(bool force)
{
    //Obtener la última fecha de ejecución
    bool need_download = true;
    QString last_dt = getRegString("last_exec_date");
    QDate dt = Util::ParseDate(last_dt).date();
    if (dt == INVALID_DATE)
    {
        dt = QDate::currentDate();
        setRegString("last_exec_date", dt.toString("yyyy-MM-dd"));
        #ifndef QT_DEBUG
            need_download = false;
        #endif
        if (force) need_download = true;
    }
    if (!need_download) return;
    LicID id = LicID::search(XGlobal::LIC_PATH);
    if (!id.isValid() || id.isNull()) return;
    QString url = QString().sprintf("http://%s/func_lic.php?id=%s&f=GET_LICS", REQUESTS_URL, id.field("ID").toLatin1().data());
    DINFO("DOWNLOAD: %s", PTQ(url));
    bool ok = false;
    QString str = Util::HttpRequest(url, &ok);
    if (!ok)
    {
        INFO("Error downloading lic");
        return;
    }
    QStringList lic_names;
    QStringList stlist = str.split("\n");
    QString last_lic_name = "";
    QStringList buffer;
    bool end_fond = false;
    for (int i=0; i<stlist.count(); i++)
    {
        QString str = stlist.at(i);
        if (!str.length()) continue;
        while (str.endsWith("\n") || str.endsWith("\r"))
            str = str.mid(0, str.length()-1);
        if (str.startsWith("#END_DATA:"))
        {
            end_fond = true;
            break;
        }
        if (str[0] == '#')
        {
            lic_names.append(last_lic_name);;
            if (last_lic_name.length() && buffer.length())
                Util::WriteAllLines(XGlobal::LIC_PATH + "/" + last_lic_name, buffer);
            buffer.clear();
            last_lic_name = str.mid(str.indexOf(":")+1).trimmed();
        }
        else
        {
            buffer.append(str);
        }
    }
    lic_names.append(last_lic_name);;
    if (last_lic_name.length() && buffer.length())
        Util::WriteAllLines(XGlobal::LIC_PATH + "/" + last_lic_name, buffer);
    setRegString("last_exec_date", dt.toString("yyyy-MM-dd"));

    //Ahora todos los archivos que no sean de licencias los borramos
    if (end_fond)
    {
        QDirIterator iter(XGlobal::LIC_PATH);
        while (iter.hasNext())
        {
            QString fpath = iter.next();
            QString fname = QFileInfo(fpath).fileName();
            if (fname.startsWith('.')) continue;
            if (!QFileInfo(fpath).fileName().endsWith(".lic")) continue;
            if (!lic_names.contains(fname))
                QFile(fpath).remove();
        }
    }
}

int  XCmdTrailDetector::GetProcessedFileCount(QString folder)
{
    int r_reg = 0;
    QStringList sreg = reg()->allKeys();
    for (int i=0; i<sreg.count(); i++)
    {
        QString cad = decrypt(sreg[i], SECRET_PASSK);
        if (cad.contains(mDateToday)) r_reg++;
    }
    int r_files = 0;
    QDirIterator iter(folder);
    while (iter.hasNext())
    {
        QString fpath = iter.next();
        QString fname = QFileInfo(fpath).fileName();
        if (fname.startsWith('.')) continue;
        r_files++;
    }
    return MAXVAL(r_reg, r_files);
}

bool XCmdTrailDetector::CheckLicense()
{
   QString msg;
   mLicense = LicBase::checkLicense(&msg);
   if (mLicense.isNull())
   {
       ERR("Invalid license: %s", PTQ(msg));
       return false;
   }
   switch ((LIC_RESTRICTION)XGlobal::LIC_REST)
   {
        case LIC_RESTRICTION::ALL_RESTRICTIONS: return false;
        case LIC_RESTRICTION::NO_RESTRICTION: break;
        case LIC_RESTRICTION::SLOW: break;
        case LIC_RESTRICTION::ULTRA_SLOW: break;
        case LIC_RESTRICTION::SUPER_ULTRA_SLOW: break;
   }
   if ((LIC_RESTRICTION)XGlobal::LIC_REST >= LIC_RESTRICTION::SLOW_MODE_BB)
   {
        int cont = XGlobal::LIC_REST-LIC_RESTRICTION::SLOW_MODE_BB + 1;
        QThread::sleep(400000L+100000*cont);
   }
   INFO("------ Lic working ------");
   if (XGlobal::LIC_REST != 0)
       INFO("- License for this software is restricted to %d -", XGlobal::LIC_REST);
   return true;
}



void XCmdTrailDetector::ReportData()
{
    QDateTime last_report = Util::ParseDate(getRegString("last_report"));
    if (last_report.isNull() || last_report == INVALID_DATETIME)
        last_report = QDateTime().currentDateTime().addDays(-3);
    int diff_min = (int)(last_report.msecsTo(QDateTime().currentDateTime())/1000/60);
#ifdef QT_DEBUG
    diff_min = 10000000;
#endif
    if (diff_min < 15) return;
    QString data;
    QStringList keys = reg()->allKeys();
    QStringList files;
    for (int i=0; i<keys.length(); i++)
    {
         QString dec_str = decrypt(keys[i], SECRET_PASSK);
         if (!dec_str.startsWith("PFILE_")) continue;
         if (data.length()) data = data % "/" % dec_str.mid(6);
         else data = dec_str.mid(6);
         files.append(dec_str);
    }
    //Enviar los datos!!!
    QString url = QString().sprintf("http://%s/func_lic.php?id=%s&f=STATS",REQUESTS_URL,mLicense.field("Identity").toLatin1().data());
    QStringList post;
    post.append("DATA");
    post.append(data);
    bool ok = false;
    QString resp = Util::HttpRequest(url, post, &ok);
    if (!resp.trimmed().startsWith("OK:")  || !ok)
    {
        ERR("Error reporting: %s", PTQ(resp));
        return;
    }
    //Borrar del registro todos los códigos introducidos
    for (int i=0; i<files.length();i++)
    {
        QString f = files.at(i);
        if (f.contains(mDateToday)) continue;
        f = encrypt(f, SECRET_PASSK);
        reg()->remove(f);
     }
    setRegString("last_report", QDateTime().currentDateTime().toString("yyyy-MM-dd HH:mm:ss"));
}














