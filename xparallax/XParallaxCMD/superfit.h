#ifndef SUPERFIT_H
#define SUPERFIT_H

#include <QVector>
#include "core_def.h"
#include "interpolation.h"
#include "advancedtraildetector.h"

#define SQSECH(x)       (1.0/cosh(x))
#define IMG_PROFILE_WIDTH 1024
#define IMG_PROFILE_HEIGHT 256
#define IMG_PROFILE_MARGIN  15

/*   a => Amplitud del pulso
 b => Ancho del pulso de flanco a flanco (punto de inflexión a punto de inflexión)
 c => Posición del flanco de subida en el eje x (coordenada x del primer punto de inflexión
 d => Pendiente de los flanos (de 6 a 20 son buenos valores)
 e => Pedestal
*/

class SuperFit
{
    protected:
        double mA;
        double mB;
        double mC;
        double mD;
        double mE;
        double mBkg;
        double mSignal;
        double mFitStart=0;
        double mFitEnd;
        QVector<double> mDataExp;

    public:
        SuperFit();
        SuperFit(double a, double b, double c, double d, double e, double bkg, double signal)
            {mA=a;mB=b;mC=c;mD=d;mE=e;mBkg=bkg;mSignal=signal;}
        double getA(){return mA;}
        double getB(){return mB;}
        double getC(){return mC;}
        double getD(){return mD;}
        double getE(){return mE;}
        QVector<double> getData(){return mDataExp;}
        double getFitStart(){return mFitStart;}
        double getfitEnd(){return mFitEnd;}
        double getBkg(){return mBkg;}
        double getSignalLevel(){return mSignal;}

        void set(double a, double b, double c, double d, double e, double bkg, double signal)
            {mA=a;mB=b;mC=c;mD=d;mE=e;mBkg=bkg;mSignal=signal;}
        void setCoeff(QVector<double> coef){
            set(coef[0],coef[1],coef[2],coef[3],
                    coef[4],mBkg,mSignal);}
        void setBkg(double bkg, double signal){mBkg=bkg;mSignal=signal;}
        void setA(double A){mA=A;}
        void setB(double B){mB=B;}
        void setC(double C){mC=C;}
        void setD(double D){mD=D;}
        void setE(double E){mE=E;}
        void setDataExp(QVector<double>& vec){mDataExp=vec;mFitEnd=mDataExp.length()-1;}
        int flank1I(){return (int)flank1() < 0 ? 0 : (int)flank1();}
        int flank2I(){return (int)flank2() >= mDataExp.length() ? mDataExp.length()-1 : (int)flank2();}

        virtual double flank1(){return mC;}
        virtual double flank2(){return flank1()+mB;}
        virtual double rho();

        virtual double theo(double x){return mA * POW2(SQSECH(pow(2*(x-mC)/mB-1,mD))) + mE;}

        double squareDiff(double* v, double len,int start=-1, int end=-1);
        double squareDiff(QVector<double>& vec,int start=-1, int end=-1);
        double squareDiff(int start=-1, int end=-1){return squareDiff(mDataExp, start, end);}
        double avgData(int start=-1,int end=-1);

        virtual void saveImage(QString filename,int start=-1, int end=-1);

        bool superfitMe(int start=-1, int end=-1);
        bool superfitAtLeft(int flank_size);
        bool superfitAtRight(int flank_size);
        void superfitC(int start, int end);
        void superfitD(int start, int end);

};






#endif // SUPERFIT_H
