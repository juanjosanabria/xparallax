#include "xcmdastrometry.h"
#include <QFileInfo>
#include "xglobal.h"
#include "core.h"
#include "log.h"
#include "fitimage.h"
#include "mpc.h"
#include "astrometry.h"
#include "novas.h"

#define  SZ_BUFFER 64

XCmdAstrometry::XCmdAstrometry(QCoreApplication *parent) :
    XCmd(parent)
{
    mFitImage = NULL;
    mAstrometryComputer = NULL;
    mRa = "AUTO";
    mDec = "AUTO";
    mPixScale = "AUTO";
}

QString XCmdAstrometry::helpRes()
{
  return ":/help/res/xcmdastrometry.txt";
}

QString XCmdAstrometry::iniEx()
{
    return ":/help/res/xcmdastrometry.ini";
}

XCmdAstrometry::~XCmdAstrometry()
{
    if (mFitImage) delete mFitImage;
    mFitImage = NULL;
    if (mAstrometryComputer) delete mAstrometryComputer;
    mAstrometryComputer = NULL;
}

void XCmdAstrometry::run()
{
    // Lectura de línea de comandos y de archivo INI
    //----------------------------------------------------------------------------
    INFO("Astrometric reduction: %s v%s", XPROGRAM, XVERSION);
    int ret = readCmdLine();
    if (ret)  return parent()->exit(ret);

    INFO("Loading INI file: %s", PTQ(mIniFile));
    mMatchPar.Load(settings(),"MATCHER");
    mDetectionPar.Load(settings(),"DETECTION");

    // Apertura de archivo de entrada
    //----------------------------------------------------------------------------
    INFO("Opening input file: %s", PTQ(mInputFile));
    mFitImage = FitImage::OpenFile(mInputFile);
    if (!mFitImage || mFitImage->error())
    {
        ERR("Error reading input file");
        return parent()->exit(ERR_UNABLE_OPEN_INPUT_FILE);
    }
    mWorkFileList.fitimages()->append(mFitImage);

    // Ascensión recta y declinación
    //----------------------------------------------------------------------------
    if (mRa == "AUTO")
    {
        mMatchPar.center_ra = mFitImage->headers()->ra();
        if (IS_INVALIDF(mMatchPar.center_ra))
        {
            INFO("Invalid center RA");
            return parent()->exit(ERR_INVALID_INPUT_PARAMETERS);
        }
    }
    else mMatchPar.center_ra = Util::ParseRA(mRa);

    if (mDec == "AUTO")
    {
        mMatchPar.center_dec = mFitImage->headers()->dec();
        if (IS_INVALIDF(mMatchPar.center_dec))
        {
            INFO("Invalid center DEC");
            return parent()->exit(ERR_INVALID_INPUT_PARAMETERS);
        }
    }
    else mMatchPar.center_dec = Util::ParseDEC(mDec);

    if (mPixScale == "AUTO")
    {
        mMatchPar.pixel_scale_arcsec = mFitImage->headers()->pixSize();
        if (IS_INVALIDF(mMatchPar.pixel_scale_arcsec))
        {
            INFO("Invalid value for pixel size");
            return parent()->exit(ERR_INVALID_INPUT_PARAMETERS);
        }
    } else mMatchPar.pixel_scale_arcsec = mPixScale.toDouble();

    //Tansformación de coordenadas si fuera necesario
    //https://ned.ipac.caltech.edu/forms/calculator.html
    if (mEquinox == "EQUINOX")
    {
        QDateTime dtt = MpcUtils::GetDateFrame(mFitImage->headers());
        if (dtt == INVALID_DATETIME)
        {
            INFO("Invalid image date. Can't change epoch in coordinates");
            return parent()->exit(ERR_INVALID_INPUT_PARAMETERS);
        }

        double ra = mMatchPar.center_ra;
        double dec = mMatchPar.center_dec;
        double new_ra, new_dec;

        Novas::cat_entry cat_1, cat_2;
        if ( Novas::make_cat_entry("dummy","CAT",0, ra, dec, 0, 0, 0, 0, &cat_1))
        {
            INFO("Invalid image date. Can't change epoch in coordinates");
            return parent()->exit(ERR_INVALID_INPUT_PARAMETERS);
        }

        double jd_src = Util::JulianDate(dtt);;
        double jd_dst = JD_J2000;

        // Novas::transform_hip(&cat_1, &cat_2);
        //= 1 ... change epoch; same equator and equinox
        //= 2 ... change equator and equinox; same epoch
        //= 3 ... change equator and equinox and epoch
        //= 4 ... change equator and equinox J2000.0 to ICRS
        //= 5 ... change ICRS to equator and equinox of J2000.0
        Novas::transform_cat(2, jd_src, &cat_1, jd_dst, (char*)"HP2", &cat_2);
        new_ra = cat_2.ra;
        new_dec = cat_2.dec;

        INFO("Equator and equinox change: %s[%s/%s] -> J2000[%s/%s]", PTQ(Util::Epoch2String(jd_src)),
                PTQ(Util::RA2String(ra)), PTQ(Util::DEC2String(dec)),
                PTQ(Util::RA2String(new_ra)), PTQ(Util::DEC2String(new_dec)));

        mMatchPar.center_ra = new_ra;
        mMatchPar.center_dec = new_dec;
    }

    mMatchPar.ComputeScale();

    // Astrometría
    //----------------------------------------------------------------------------
    AstrometryComputer* ac = new AstrometryComputer();

    INFO("Looking for a match");
    if (!ac->doWork(&mWorkFileList, &mDetectionPar, &mMatchPar) || ac->CountKO())
    {
        ERR("No match found");
        return parent()->exit(mWorkFileList.results()->at(0).retVal);
    }

    if (mOutputFile != "null")
    {
        INFO("Saving output file");
        if (!mFitImage->SaveToFile(mOutputFile))
        {
            ERR("Unable to save output file");
            return parent()->exit(ERR_UNABLE_WRITE_OUTPUT_FILE);
        }
    }
    else INFO("No output file selected. Result is shown on screen");

    if (mRefFile.length() && !saveReferenceStars(ac))
    {
        ERR("Error saving reference stars");
        return parent()->exit(ERR_UNABLE_WRITE_OUTPUT_FILE);
    }

    INFO("Astrometric reduction finished OK");
    emit finished();
}


int XCmdAstrometry::readCmdLine()
{
    mIniFile = argValueStr("-ini");
    if (mIniFile.length() && !QFileInfo(mIniFile).exists())
    {
        ERR("Ini file '%s' does not exits", PTQ(mIniFile));
        return ERR_INI_FILE;
    }

    mInputFile = argValueStr("-i");
    if (mInputFile.length() && !QFileInfo(mInputFile).exists())
    {
        ERR("Input file '%s' does not exits", PTQ(mInputFile));
        return ERR_UNABLE_OPEN_INPUT_FILE;
    }

    mOutputFile = argValueStr("-o");
    if (!mOutputFile.length())
    {
        INFO("No output file, using input file: '%s'", PTQ(mInputFile));
        mOutputFile = mInputFile;
    }
    if (mOutputFile == "null") INFO("NO OUTPUT SELECTED");

    mRa = argExists("-ra") ? argValueStr("-ra") : "AUTO";
    if (mRa != "AUTO" && IS_INVALIDF(Util::ParseRA(mRa)))
    {
        INFO("Invalid center RA");
        return ERR_INVALID_INPUT_PARAMETERS;
    }

    mDec = argExists("-dec") ? argValueStr("-dec") : "AUTO";
    if (mDec != "AUTO" && IS_INVALIDF(Util::ParseDEC(mDec)))
    {
        INFO("Invalid center DEC");
        return ERR_INVALID_INPUT_PARAMETERS;
    }

    mPixScale = argExists("-ps") ? argValueStr("-ps") : "AUTO";
    if (mRa != "AUTO" && IS_INVALIDF(Util::ParseRA(mRa)))
    {
        INFO("Invalid pixel scale PS");
        return ERR_INVALID_INPUT_PARAMETERS;
    }

    //Tipo de coordenadas
    mEquinox = argExists("-raeq") ? argValueStr("-raeq") : "J2000";
    mEquinox = mEquinox.toUpper();
    if (mEquinox != "J2000" && mEquinox != "EQUINOX")
    {
        INFO("Invalid value for -raeq Equinox. Please, specify J2000 or EQUINOX");
        return ERR_INVALID_INPUT_PARAMETERS;
    }

    //Archivo de referencia
    mRefFile = argExists("-ref") ? argValueStr("-ref") : "";
    if (mRefFile.length() && !QFileInfo(mRefFile).exists())
    {
        QFile fil(mRefFile);
        if (!fil.open(QFile::Append))
        {
            INFO("Unable to open file: %s", PTQ(mRefFile));
            return ERR_UNABLE_WRITE_OUTPUT_FILE;
        }
        fil.close();
        fil.remove();
    }
    return ERR_NO_ERROR;
}

bool XCmdAstrometry::saveReferenceStars(AstrometryComputer* ac)
{
    return AstrometryComputer::SaveReferenceStars(ac->platec(), mRefFile, mInputFile);
}
























