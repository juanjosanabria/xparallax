#ifndef ROBUSFIT_H
#define ROBUSFIT_H

#include <QPointF>

void RobustFitting(QPoint* data, double* weight, int datalen, double* a, double* b);


#endif // ROBUSFIT_H
