#include "robusfit.h"

#include <QVector>
#include "util.h"

#define NR_END 1
#define FREE_ARG char*
#define D 1.4901e-08
#define TUNE 4.6850

void nrerror(const char*){;}
double** dmatrix(long nrl, long nrh, long ncl, long nch);
void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch);
void LSfit(double x[], double y[], int ndata, double *a, double *b,
        double *siga, double *sigb, double *chi2);
double stdx(double *x, int ndata);
void WLSfit(double x[], double y[], double w[], int ndata, double *a, double *b,
        double *siga, double *sigb, double *chi2);
double* dvector(long nl, long nh);
void free_dvector(double *v, long nl, long nh);

static double dmaxarg1,dmaxarg2;
#define DMAX(a,b) (dmaxarg1=(a),dmaxarg2=(b),(dmaxarg1)>(dmaxarg2)?(dmaxarg1):(dmaxarg2))
static float sqrarg;
#define SQR(a) ((sqrarg=(a))==0.0?0.0:sqrarg*sqrarg)

/* Sort compare function for qsort function */
int compare_double(const void *v1, const void *v2)
{
    double *x1,*x2;
    x1 = (double *)v1;
    x2 = (double *)v2;
        if(*x1<*x2)return (-1);
        else if(*x1==*x2) return (0);
        else return(1);
}

void inverse2(double **A, double **inv)
/* compute inverse of a 2-by-2 matrix */
{
    double determinant = A[1][1]*A[2][2] - A[1][2]*A[2][1];
    inv[1][1] = A[2][2]/determinant;
    inv[1][2] = -A[1][2]/determinant;
    inv[2][1] = -A[2][1]/determinant;
    inv[2][2] = A[1][1]/determinant;
}

double stdx(double *x, int ndata)
/* (Unbiased) standard deviation of x */
{
    int i;
    double mean=0.0, variance=0.0;

    for (i=1; i<=ndata; i++)
        mean += x[i];
    mean /= ndata;

    for (i=1; i<=ndata; i++)
        variance += (x[i]-mean)*(x[i]-mean);
    variance /= (ndata-1);

    return sqrt(variance);
}


double dot(double **u, int colu, double **v, int colv, int rows)
/* u(:,colu)'*v(:,colv), i.e. sum(u(:,colu).*v(:,colv)) */
{
    int i;
    double sum = 0.0;

    for(i=1;i<=rows;i++)
        sum += u[i][colu]*v[i][colv];

    return sum;
}

double Norm_l2(double **v, int col, int rows)
{
    int i;
    double sum = 0.0;

    for (i=1; i<=rows; i++)
        sum += v[i][col]*v[i][col];

    return (sqrt(sum));
}

double madsigma(double *r, int ndata)
/* Compute sigma estimate using MAD (Median Absolute Deviation) of residuals from 0 */
{
    int i;
    double *rs, s=0.0;

    //rs = sort(abs(r));
    rs = dvector(1,ndata);
    rs[0]=-1;
    for (i=1; i<=ndata; i++)
        rs[i] = fabs(r[i]);

    /* sort rs using the qsort function */
    qsort((void *)rs, ndata+1, sizeof(double), compare_double);

    //s = median(rs(2:end)) / 0.6745;
    /* These are done exactly using formulas appropriate for small samples*/
    if((ndata-1)%2)  /* this is case for odd number */
        s = rs[2+(ndata-1)/2];
    else
        s = (rs[1+(ndata-1)/2]+rs[2+(ndata-1)/2])/2.0;

    free_dvector(rs, 1,ndata);
    return s/0.6745;
}


int ModifiedGramSchmidt(double **x, int rows, int cols, double **q, double **r)
{
    int i, j, k;
    double **y = dmatrix(1,rows, 1,cols);

    /* y=x */
    for (i=1; i<=rows; i++)
        for (j=1; j<=cols; j++)
            y[i][j]=x[i][j];


    for (j=1; j<=cols; j++)
    {
        r[j][j] = Norm_l2(y, j, rows);

        if (r[j][j] == 0.0)
        {
            free_dmatrix(y, 1,rows, 1,cols);
            return -1;
        }
        else
            /* q(:,j)=x(:,j)./r(j,j) */
            for (i=1; i<=rows; i++)
                q[i][j] = y[i][j]/r[j][j];

        for (k=j+1; k<=cols; k++)
        {
            r[j][k] = dot(y, k, q, j, rows);
            /* y = y - r(j,k).*q(:,j) */
            for (i=1; i<=rows; i++)
                y[i][k] -= r[j][k]*q[i][j];

        }
    }

    free_dmatrix(y, 1,rows, 1,cols);
    return 0;
}

void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch)
/*
 free a double matrix allocated by dmatrix()
 */
{
    free((FREE_ARG)	(m[nrl]+ncl-NR_END));
    free((FREE_ARG)	(m+nrl-NR_END));
}

double** dmatrix(long nrl, long nrh, long ncl, long nch)
/*
 allocate a double matrix with subscript range m[nrl..nrh][ncl..nch]
 */
{
    long i, nrow=nrh-nrl+1, ncol=nch-ncl+1;
    double**m;

    /*
     allocate pointers to rows
     */
    m=(double**)malloc((size_t)((nrow+NR_END)*sizeof(double*)));
    if (!m)
        nrerror("allocation failure 1 in matrix()");
    m+=NR_END;
    m-=nrl;

    /*
     allocate rows and set pointers to them
     */
    m[nrl]=(double*)malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
    if (!m[nrl])
        nrerror("allocation failure 2 in matrix()");
    m[nrl]+=NR_END;
    m[nrl]-=ncl;

    for (i=nrl+1; i<=nrh; i++)
        m[i]=m[i-1]+ncol;

    /*
     return pointer to array of pointers to rows
     */
    return m;
}

int QRDecomposition(double **A, int rows, int cols, double **Q, double **R)
/* [Q,R,perm]=qr(X,0) */
{
    int i, j;
    int status;

    double **q = dmatrix(1,rows, 1,cols),
            **v = dmatrix(1,rows, 1,cols);

    for (i=1; i<=rows; i++)
        for (j=1; j<=cols; j++)
            v[i][j] = A[i][cols-(j-1)];

    status = ModifiedGramSchmidt(v, rows, cols, q, R);

    for (i=1; i<=rows; i++)
        for (j=1; j<=cols; j++)
            Q[i][j] = -q[i][j];

    for (i=1; i<=cols; i++)
        for (j=1; j<=cols; j++)
            R[i][j] = -R[i][j];

    free_dmatrix(q, 1,rows, 1,cols);
    free_dmatrix(v, 1,rows, 1,cols);

    return status;
}



double* dvector(long nl, long nh)
/*
 allocate a double vector with subscript range v[nl..nh]
 */
{
    double *v;
    v=(double*)malloc((size_t)
            ((nh-nl+1+NR_END)*sizeof(double)));
    if (!v)
        nrerror("allocation failure in dvector()");
    return v-nl+NR_END;
}

void free_dvector(double *v, long nl, long nh)
/*
 free a double vector allocated with dvector()
 */
{
    free((FREE_ARG)	(v+nl-NR_END));
}

void computeE(double **X, double **invR, double **E, int ndata)
/* compute E=X(:,[2 1])*inv(R); */
{
    int i, j, k;
    for (i=1; i<=ndata; i++)
        for (j=1; j<=2; j++)
            for (k=1; k<=2; k++)
                E[i][j] += X[i][2-(k-1)]*invR[k][j];
}

void computeADJ(double **E, double *adjfactor, int ndata)
/* h = min(.9999, sum((E.*E)')'); adjfactor = 1 ./ sqrt(1-h); */
{
    int i;
    for (i=1; i<=ndata; i++)
        adjfactor[i] = (E[i][1]*E[i][1] + E[i][2]*E[i][2]) > 0.9999? 1.0/sqrt(1-0.9999)
                :1.0/sqrt(1 - (E[i][1]*E[i][1] + E[i][2]*E[i][2]));
}



int robustfit(double* x, double* y, int ndata, double *a, double *b, double *w)
{
    int 	i, p=2, iter=0, iterlim=50;
    double 	a0=0, b0=0, s, tiny_s, r, siga, sigb, chi2;
    double 	**X, **Q, **R, **E, **invR;
    double 	*adjfactor, *radj;

    X = dmatrix(1,ndata, 1,p);
    Q = dmatrix(1,ndata, 1,p);
    R = dmatrix(1,p, 1,p);
    invR = dmatrix(1,p, 1,p);
    E = dmatrix(1,ndata, 1,p);

    adjfactor = dvector(1,ndata);
    radj = dvector(1,ndata);

    /* X=[ones(n,1) X]; */
    for (i=1; i<=ndata; i++)
    {
        X[i][1] = 1;
        X[i][2] = x[i];
        E[i][1] = 0.0;
        E[i][2] = 0.0;
    }

    QRDecomposition(X, ndata, p, Q, R);

    LSfit(x, y, ndata, a, b, &siga, &sigb, &chi2);

    //fprintf(stderr, "%.3f ", *b);

    inverse2(R, invR);
    computeE(X, invR, E, ndata); //E=X(:,[2 1])*inv(R);
    computeADJ(E, adjfactor, ndata); // h = min(.9999, sum((E.*E)')'); adjfactor = 1 ./ sqrt(1-h);

    // If we get a perfect or near perfect fit, the whole idea of finding
    // outliers by comparing them to the residual standard deviation becomes
    // difficult.  We'll deal with that by never allowing our estimate of the
    // standard deviation of the error term to get below a value that is a small
    // fraction of the standard deviation of the raw response values.
    tiny_s = 10e-6 * stdx(y, ndata);
    if (tiny_s==0.0)
        tiny_s = 1.0;

    /* Perform iteratively reweighted least squares to get coefficient estimates */
    while((iter==0) || fabs(*a-a0)>D*DMAX(fabs(*a),fabs(a0))
                    || fabs(*b-b0)>D*DMAX(fabs(*b),fabs(b0)) ) // |a-a0|>D*max(|a|,|a0|) ?? same for b
    {
        if(++iter > iterlim)
        {
            //fprintf(stderr,"Warning: Iteration limit reached.\n");
            break;
        }

        /* Compute residuals from previous fit, then compute scale estimate */
        // r=y - X*b; radj=r.*adjfactor;
        for (i=1; i<=ndata; i++)
            radj[i] = adjfactor[i] * (y[i]-((*a)+X[i][2]*(*b)));

        s = madsigma(radj, ndata);

        /* Compute new weights from these residuals, then re-fit */
        // w=(abs(radj/(s*tune))<1) .* (1 - (radj/(s*tune)).^2).^2;
        for (i=1; i<=ndata; i++)
        {
            r = radj[i]/(DMAX(s,tiny_s)*TUNE);
            if (fabs(r)<1)
                w[i] = (1 - r*r) * (1 - r*r);
            else
                w[i] = 0;
        }

        a0=*a;
        b0=*b;

        WLSfit(x, y, w, ndata, a, b, &siga, &sigb, &chi2);
//		printf("a=%.4f b=%.4f\n", a0, b0);

    }

    free_dmatrix(X, 1,ndata, 1,p);
    free_dmatrix(Q, 1,ndata, 1,p);
    free_dmatrix(R, 1,p, 1,p);
    free_dmatrix(invR, 1,p, 1,p);
    free_dmatrix(E, 1,ndata, 1,p);

    free_dvector(adjfactor, 1,ndata);
    free_dvector(radj, 1,ndata);

    if (iter>iterlim)
        return 1;
    else
        return 0;
}


void LSfit(double x[], double y[], int ndata, double *a, double *b,
        double *siga, double *sigb, double *chi2)
/*
Given a set of data points x[1..ndata],y[1..ndata] fit them to a straight line y = a + bx
by minimizing chi2. Returned are a,b and their respective probable uncertainties siga and sigb,
the chi-square chi2. The standard deviations are assumed to be unavailable: the normalization
of chi2 is to unit standard deviation on all points.
*/
{
    int i;
    double t,sxoss,sx=0.0,sy=0.0,st2=0.0,ss,sigdat;
    *b=0.0;
    for (i=1;i<=ndata;i++) { // Accumulate sums without weights.
        sx += x[i];
        sy += y[i];
    }
    ss=ndata;
    sxoss=sx/ss;

    for (i=1;i<=ndata;i++) {
        t=x[i]-sxoss;
        st2 += t*t;
        *b += t*y[i];
    }

    *b /= st2; // Solve for a, b, siga, and sigb.
    *a=(sy-sx*(*b))/ss;
    *siga=sqrt((1.0+sx*sx/(ss*st2))/ss);
    *sigb=sqrt(1.0/st2);

    *chi2=0.0; // Calculate chi2.
    for (i=1;i<=ndata;i++)
        *chi2 += SQR(y[i]-(*a)-(*b)*x[i]);
    sigdat=sqrt((*chi2)/(ndata-2)); // For unweighted data evaluate typical sig using chi2, and adjust the standard deviations.
    *siga *= sigdat;
    *sigb *= sigdat;
}


void WLSfit(double x[], double y[], double w[], int ndata, double *a, double *b,
        double *siga, double *sigb, double *chi2)
/*
 * Weighted least square fit
 */
{
    int j;
    double del,sxy=0.0,sy=0.0,sx=0.0,sxx=0.0,s=0.0;

    for (j=1;j<=ndata;j++)
    {
        sx += x[j]*w[j];
        sy += y[j]*w[j];
        sxy += x[j]*y[j]*w[j];
        sxx += x[j]*x[j]*w[j];
        s += w[j];
    }
    del=s*sxx-sx*sx;
    *a=(sxx*sy-sx*sxy)/del; //Least-squares solutions.
    *b=(s*sxy-sx*sy)/del;
}

void RobustFitting(QPoint* data, double* weight, int datalen, double* a, double* b)
{
    //La función robusfit usa índice basado en [1..datalen]
    //Chiquito cerebro al que se le ocurrió esa idea....
    double* x3 = new double[datalen+1];
    double* y3 = new double[datalen+1];
    double* w = new double[datalen+1];
    for (int i=0; i<datalen; i++)
    {
        x3[i+1] = data[i].x();
        y3[i+1] = data[i].y();
        w[i+1] = weight[i];
    }
    robustfit(x3, y3, datalen, b, a, w);
    delete x3; delete y3; delete w;
}










