
#include <QImage>
#include <QPainter>

#include "superfit.h"
#include "util.h"
#include "statistics.h"
#include "interpolation.h"



SuperFit::SuperFit()
{

}

double SuperFit::squareDiff(double* x, double len,int start, int end)
{
    if (start == -1 || end == -1)
    {
        start = 0;
        end = len-1;
    }
    if (end >= len) end = len-1;
    ASSERT(start>0 && end>start, "Índices fueras de la matriz en SuperFit::squareDiff");

    double sdiff = 0;
    for (int i=start; i<=end; i++)
    {
        sdiff += POW2(x[i] - theo(i));
    }
    return sqrt(sdiff/(end-start+1));
}

double SuperFit::avgData(int start, int end)
{
    int datalen = mDataExp.length();
    if (start == -1 || end == -1)
    {
        start = 0;
        end = datalen-1;
    }
    if (end >= datalen) end = datalen-1;

    if (start < 0 || end <= start)
        return INVALID_FLOAT;

    double avg = 0;
    for (int i=start; i<=end; i++)
    {
        avg+= mDataExp[i];
    }
    return avg / (end-start+1);
}

double SuperFit::squareDiff(QVector<double>& vec,int start, int end)
{
    return squareDiff(vec.data(), vec.length()-1, start, end);
}


void SuperFit::saveImage(QString filename, int start, int end)
{
    int datalen = mDataExp.length();
    if (start == -1 || end == -1)
    {
        start = 0;
        end = datalen-1;
    }
    if (end >= datalen) end = datalen-1;

    int width = IMG_PROFILE_WIDTH;
    int height = IMG_PROFILE_HEIGHT;
    int margin = IMG_PROFILE_MARGIN;
    double* data = mDataExp.data();
    if (!margin) margin = height/10;
    QImage img(width, height, QImage::Format_RGB32);
    img.fill(Qt::white);
    QPainter painter(&img);
    painter.setPen(QPen(Qt::black));
    painter.setBrush(QBrush(Qt::black));


    //Calculo el mínimo y el máximo
    double min = data[0], max = -1;
    for (int i=0; i<datalen; i++)
    {
        if (data[i] < min) min = data[i];
        if (data[i] > max) max = data[i];
    }

    double scale_x = width/(double)datalen;
    double scale_y = (height-margin*2)/(max-min);

    QPointF last(0,0); QVector<QPointF> points;
    for (int i=0; i<datalen; i++)
    {
        double x = i * scale_x;
        double y = (height - (data[i]-min) * scale_y) - margin;
        if (last.x() > 0) points.append(QPointF(x,y)); //painter.drawLine(last, QPointF(x,y));
        last = QPointF(x,y);
    }
    points.append(QPointF(width+1, points.last().y()));
    points.append(QPointF(width+1, height));
    points.append(QPointF(-1, height));
    points.append(QPointF(-1, points.at(0).y()));
    points.append(points.at(0));

    //painter.setPen(QPen(QColor::fromRgb(16,124,244),1));
    //painter.setBrush(QBrush(QColor::fromRgb(16, 124, 244,32)));
    painter.setPen(QPen(Qt::black));
    painter.setBrush(QBrush(Qt::white));
    painter.drawPolygon(points.data(), points.count(), Qt::OddEvenFill);

    //Dibujo la línea de background
    painter.setPen(QPen(Qt::red, 0.5, Qt::DashLine));
    QPointF p1(0, (height - (mBkg-min) * scale_y) - margin);
    QPointF p2(width, p1.y());
    painter.drawLine(p1,p2);
    p1=QPointF(0, (height - (mSignal-min) * scale_y) - margin);
    p2=QPointF(width, p1.y());
    painter.drawLine(p1,p2);

    //Dibujar los flancos y la línea central
    p1 = QPointF(flank1()*scale_x, 0);
    p2 = QPointF(flank1()*scale_x, height);
    painter.drawLine(p1,p2);
    p1 = QPointF(flank2()*scale_x, 0);
    p2 = QPointF(flank2()*scale_x, height);
    painter.drawLine(p1,p2);
    p1 = QPointF((flank2()+flank1())/2.0*scale_x, 0);
    p2 = QPointF((flank2()+flank1())/2.0*scale_x, height);
    painter.drawLine(p1,p2);

    //Dibujar la función de ajuste
    last.setX(0); last.setY(0);
    painter.setPen(QPen(Qt::blue, 2, Qt::SolidLine));
    for (int i=(int)floor(mFitStart*scale_x); i<mFitEnd*scale_x; i++)
    {
        double x = i/scale_x;
        double y = theo(x);
        QPointF p = QPointF(i, (height - (y-min) * scale_y) - margin);
        if (last.x() != 0 && last.y() != 0)
            painter.drawLine(last, p);
        last = p;
    }

    painter.end();
    img.save(filename, "png");
}

bool SuperFit::superfitAtLeft(int flank_size)
{
    int start = flank1I()-flank_size*3/2;
    int end = flank1I()+flank_size*3/2;

    if (start <0 || end < 0 || start >= end) return false;

    double bkg_left = avgData(start, start + flank_size);
    if (IS_INVALIDF(bkg_left)) return false;
    double signal_right = avgData(end-flank_size, end);
    if (IS_INVALIDF(signal_right)) return false;

    mBkg = bkg_left;
    mSignal = signal_right;

    mA = mSignal - mBkg;
    mE = mBkg;

    return superfitMe(start, end);
}

bool SuperFit::superfitAtRight(int flank_size)
{
    int start = flank2I()-flank_size*3/2;
    int end = flank2I()+flank_size*3/2;

    if (start <0 || end < 0 || start >= end)
        return false;

    double signal_left = avgData(start, start + flank_size);
    if (IS_INVALIDF(signal_left)) return false;
    double bkg_right = avgData(end - flank_size, end);
    if (IS_INVALIDF(bkg_right)) return false;

    mBkg = bkg_right;
    mSignal = signal_left;

    mA = mSignal - mBkg;
    mE = mBkg;

    return superfitMe(start, end);
}

void SuperFit::superfitD(int start, int end)
{
    if (start == -1 || end == -1)
    {
        start = 0;
        end = mDataExp.length()-1;
    }
    if (end >= mDataExp.length()) end = mDataExp.length()-1;
    int datalen = end-start+1;

    double old_d = mD;
    double best_sig = 1e32;
    double best_d = mD;
    for (int d=5; d<32; d++)
    {
        mD = d;
        double res = squareDiff(start,end);
        if (res < best_sig)
        {
            best_d = d;
            best_sig = res;
        }
    }
    mD = best_d;
}

void SuperFit::superfitC(int start, int end)
{
    if (start == -1 || end == -1)
    {
        start = 0;
        end = mDataExp.length()-1;
    }
    if (end >= mDataExp.length()) end = mDataExp.length()-1;
    int datalen = end-start+1;
    double idxmed = (end+start)/2;

    double idx_left =  mC-datalen/2;
    double idx_right = mC+datalen/2;


    double hDiff = squareDiff(start, end);

    double step_inc = 0.1;
    double lastC = mC;

    //Mover a la derecha en incrementos de 0.1 a ver si mejoro
    for (mC+=step_inc; mC<idx_right; mC+=step_inc)
    {
        double diff = squareDiff(start, end);
        if (diff > hDiff)
        {
            mC = lastC;
            break;
        }
        else
        {
            lastC = mC;
            hDiff = diff;
        }
    }
    //Mover a la izquierda en incrementos de 0.1 a ver si mejoro
    for (mC-=step_inc; mC>idx_left; mC-=step_inc)
    {
        double diff = squareDiff(start, end);
        if (diff > hDiff)
        {
            mC = lastC;
            break;
        }
        else
        {
            lastC = mC;
        }
    }

}

bool SuperFit::superfitMe(int start, int end)
{
    if (start == -1 || end == -1)
    {
        start = 0;
        end = mDataExp.length()-1;
    }
    if (end >= mDataExp.length()) end = mDataExp.length()-1;


    SuperFit sf1 = *this;
    for (int i=0; i<3; i++)
    {
        sf1.superfitD(start, end);
        sf1.superfitC(start, end);
    }
    mD = sf1.mD;
    mC = sf1.mC;

    mFitStart = start;
    mFitEnd = end;

    return mA > 0 && mC > 0;
}

double SuperFit::rho()
{
    int datalen = mFitEnd-mFitStart+1;

    alglib::real_1d_array sx; sx.setlength(datalen);
    alglib::real_1d_array sy; sy.setlength(datalen);

    for (int x=mFitStart,i=0; x<=mFitEnd; x++, i++)
    {
        double real_val_y = mDataExp[x];
        double theorical_y = theo(x);
        sx[i] = real_val_y;
        sy[i] = theorical_y;
    }
    double s = alglib::spearmancorr2(sx, sy);
    return fabs(s);
}


















