#ifndef XCMD_H
#define XCMD_H

#include <QObject>
#include <QCoreApplication>
#include <QTemporaryFile>
#include <QTextStream>
#include "core.h"
#include "error.h"

//Número máximo de procesos q ue pueden ejecutarse en paralelo
#define     MAX_PROCESSES               4
#define     MAX_PROCESSES_SEM_NAME      "XParallaxCMD_Semaphore"
#define     SECRET_KEY "Winrecstack"
#define     SECRET_PASS  "9crTuxTGWWzHBpVQEq3LofXCiArAasiFWmLpuXDXtd7wTiKvbm52qljalf7m"
#define     SECRET_PASSK "asñdlfe"

/**
* @brief The XCmd class
* Clase de la que heredarán todos los procesos de trabajo.
*
* Deben implementar el método virtual "run()", el cual ejecutará el proceso.
* Para salir devolviendo un valor llamar a "parent()->exit(val)"
* Para salir devolviendo cero es más interesante usar "emit finished()"
*/
class XCmd : public QObject
{
    Q_OBJECT

public:
    explicit XCmd(QCoreApplication *parent);
    virtual ~XCmd();
    void showHelp();                         //Muestra la ayuda para este comando
    void showIniExample();                   //Muestra un archivo ini de ejemplo
    static void showtFile(QString file);     //Vuelca un archivo de recursos a la salida estándar
    static void showError(int wich=-1);      //Vuelca una descripción del error seleccionado. -1 vuelca todos los errores

protected:
    QCoreApplication* parent(){return (QCoreApplication*)QObject::parent();}
    bool argExists(QString arg);
    //Devuelve una cadena o la cadena vacía si no existiera el argumento
    QString argValueStr(QString arg, QString def = "");
    //Devuelve un valor de fecha o fecha y hora parseado
    QDateTime argValueDate(QString arg, QDateTime def = INVALID_DATETIME);
    int argValueInt(QString arg, int def=0);
    //Devuelve un valor double parseado
    double argValueDouble(QString arg, double def=INVALID_FLOAT);
    //Devuelve un valor de ascensión recta parseado. Devuelve INVALID_FLOAT si la ascensión recta no es válida
    double argValueRA(QString arg, double def=INVALID_FLOAT);
    //Devuelve un valor de declinación parseado
    double argValueDEC(QString arg, double def=INVALID_FLOAT);
    //Devuelve una lista de parámetros. Como por ejemplo una lista de archivos
    QStringList argValueStringList(QString arg);
    //Devuelve un valor bool, 1,true,yes,si = true : 0,false,no = false, es case insensitive
    bool argValueBool(QString arg, double def=false);
    QSettings* settings();
    //Nombre del recurso que contiene la ayuda para esta opción
    virtual QString helpRes() = 0;
    //Nombre del recurso que contiene un archivo ini de ejemplo para esta opción
    virtual QString iniEx() = 0;
    void exit(int code){Quit_Slot(); ::exit(code);}
    //Devuelve una entrada que permite editar valores de registro
    QSettings* reg();
    void flushReg(){if (mReg) delete mReg; mReg = NULL;}

    //Encriptación y decriptación sencilla con XOR y en base 64
    QString encrypt(QString secret, QString pass);
    QString decrypt(QString data, QString pass);

    QString getRegString(QString key);
    void setRegString(QString key, QString val);
    int getRegInt(QString key);
    void setRegInt(QString key, int val);


private:
    QCoreApplication*   mParent;
    QTemporaryFile*     mTempIni;
    QSettings*          mSettings;
    QTextStream*        mConsole;
    QSettings*          mReg;
    //QSystemSemaphore*   mSemaphore;

signals:
    void finished();

public slots:
    virtual void run() = 0;
    void Log_Slot(qint64 thread_id, int log_type, QString msg);
    void Quit_Slot();

};

#endif // XCMD_H
