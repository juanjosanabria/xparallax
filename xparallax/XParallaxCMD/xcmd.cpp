#include <QDateTime>
#include <QFile>
#include <QFileInfo>
#include "log.h"
#include "xcmd.h"
#include "xglobal.h"
#include "util.h"

#ifdef Q_OS_WIN
#include <windows.h>
#include <iostream>
#else
#include <locale.h>
#endif

XCmd::XCmd(QCoreApplication *parent) :
    QObject(parent)
{
    mSettings = NULL;
    mReg = NULL;
    mTempIni = NULL;
    mConsole = new QTextStream(stdout, QIODevice::WriteOnly);
    mParent = parent;
    connect(XGlobal::LOGGER, SIGNAL(msg(qint64,int,QString)), this, SLOT(Log_Slot(qint64,int,QString)));
    connect(parent, SIGNAL(aboutToQuit()), this, SLOT(Quit_Slot()));

    //Controlar el número máximo de procesos que se pueden ejecutar
    /*
    mSemaphore = new QSystemSemaphore(MAX_PROCESSES_SEM_NAME, MAX_PROCESSES, QSystemSemaphore::Open);
    if (mSemaphore->error())
    {
        ERR("System semaphore crete error %d", mSemaphore->error());
        exit(ERR_OS_SYSTEM_SEMAPHORE);
    }
    if (!mSemaphore->acquire())
    {
        ERR("Max number of processes (%d) reached",MAX_PROCESSES);
        exit(ERR_NUMBER_OF_PROCESSED);
    }*/
}

XCmd::~XCmd()
{
    if (mSettings) delete mSettings;
    mSettings = NULL;
    if (mTempIni) delete mTempIni;
    mTempIni = NULL;
    if (mConsole) delete mConsole;
    mConsole = NULL;
    if (mReg) delete mReg;
    mReg = NULL;
}

void XCmd::showHelp()
{
   showtFile(helpRes());
}

void XCmd::showIniExample()
{
    showtFile(iniEx());
}

QSettings* XCmd::settings()
{
    if (mSettings) return mSettings;
    mTempIni = new QTemporaryFile();
    mTempIni->setAutoRemove(true);
    //Copiamos el archivo INI a un archivo temporal
    QString ini_path = argValueStr("-ini");
    if (ini_path.length() && QFileInfo(ini_path).exists())
    {
        QFile ini_src(ini_path);
        if (!ini_src.open(QFile::ReadOnly|QFile::Text) ||
            !mTempIni->open())
        {
            ERR("Error loading ini FILE: %s", PTQ(ini_path));
            parent()->exit(ERR_INI_FILE);
            return NULL;
        }
        QTextStream in_str(&ini_src);
        QTextStream out_str(mTempIni);
        while (!in_str.atEnd())
        {
           QString cad = in_str.readLine();
           if (!cad.startsWith(";"))
           out_str << cad << "\n";
        }
    }
    mTempIni->close();
    mSettings = new QSettings(mTempIni->fileName(), QSettings::IniFormat);

    //Ahora localizamos todos los valores INI de la línea de comandos
    QStringList qa = parent()->arguments();
    for (int i=0; i<qa.count()-1; i++)
    {
        if (qa.at(i).startsWith("-oi"))
        {
            QString rval = qa.at(i+1);
            int idx_cor = rval.indexOf("]");
            int idx_equ = rval.indexOf("=");
            if (idx_cor < 0 || idx_equ < 0)
            {

                ERR("Invalid argument -oi %s", PTQ(rval));
                parent()->exit(ERR_INVALID_INPUT_PARAMETERS);
                return NULL;
            }
            QString section = rval.mid(0,idx_cor).trimmed()
                    .replace("[","").replace("]","");
            QString name = rval.mid(idx_cor+1,idx_equ-idx_cor-1).trimmed();
            QString value = rval.mid(idx_equ+1, rval.length()-idx_equ-1).trimmed();
            if (value[0] == '\'' && value[value.length()-1]== '\'')
                value = value.mid(1, value.length()-2);
            mSettings->setValue(QString("%1/%2").arg(section, name), value);
            i++;
        }
    }

    return mSettings;
}

void XCmd::showtFile(QString fname)
{
    if (!fname.length())
    {
        printf("No file defined");
    }
    else
    {
        QFile fil(fname);
        if (!fil.open(QFile::ReadOnly))
        {
            printf("Unable to open ini %s", fname.toLatin1().data());
        }
        else
        {
            QTextStream ts(&fil);
            QString cad = "";
            do
            {
                cad = ts.readLine();
                if (cad.length())
                {
                    cad = cad.replace("[XPROGRAM]", XPROGRAM);
                    cad = cad.replace("[XVERSION]", XVERSION);
                    cad = cad.replace("[XMAIL]",XMAIL);
                    cad = cad.replace("[OSTYPE]",XGlobal::OS_TYPE);
                    cad = cad.replace("[OSBITS]",QString("%1").arg(XGlobal::OS_BITS));
                    printf("%s\n",cad.toLatin1().data());
                    fflush(stdout);
                }
                else printf("\n");
            } while (!ts.atEnd());
            fil.close();
        }
    }
}


void XCmd::showError(int wich)
{
    bool ok;
    QSettings sett(":/help/res/errcodes.ini", QSettings::IniFormat);
    QStringList grp = sett.childGroups();
    for (int i=0; i<grp.count(); i++)
    {
        QString section = grp.at(i);
        if (!section.startsWith("ERR_")) continue;
        int code = section.mid(4).toInt(&ok);
        ASSERT(ok,"Error in error list ini file");
        if (!ok) continue;
        QVariant short_desc =sett.value(QString("%1/%2").arg(section).arg("short_desc"));
        if (short_desc.isNull()) continue;
        QVariant long_desc = sett.value(QString("%1/%2").arg(section).arg("long_desc"));
        if (long_desc.isNull()) continue;
        if (wich == -1 || wich==code )
        printf("%03d %s\n", code, short_desc.toString().toLatin1().data());
        if (wich==code) break;
    }
}

void XCmd::Log_Slot(qint64 /*thread_id*/, int log_type, QString msg)
{
    //Hay que montar todo esto para asegurarse de que en Windows/Linux se muestran correctamente los mensajes a consola
    QDateTime dt = QDateTime::currentDateTime();
    printf("%04d-%02d-%02d %02d:%02d:%02d.%03d [%s] ",
           dt.date().year(), dt.date().month(), dt.date().day(),
           dt.time().hour(), dt.time().minute(), dt.time().second(), dt.time().msec(),
           log_type == (int)Logger::LOGTYPE_INFO?"INFO ":(log_type == (int)Logger::LOGTYPE_WARN?"WARN ": log_type == (int)Logger::LOGTYPE_DEBUG ? "DEBUG" : "ERROR")
           );
    fflush(stdout);
    /*
    #ifdef Q_OS_WIN
      WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE),
          msg.utf16(), msg.size(), NULL, NULL);
      fflush(stdout);
    #else
      *mConsole << msg;
       mConsole->flush();
    #endif
    */
    printf("%s\n",msg.toLatin1().data());
    fflush(stdout);
}


void XCmd::Quit_Slot()
{
    /*
    if (mSemaphore)
    {
        mSemaphore->release();
        delete mSemaphore;
        mSemaphore = NULL;
        DINFO("System semaphore release");
    }*/
}

bool XCmd::argExists(QString arg)
{
    return parent()->arguments().contains(arg);
}

QString XCmd::argValueStr(QString arg, QString def)
{
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return def;
    return parent()->arguments().at(idx+1);
}

QDateTime XCmd::argValueDate(QString arg, QDateTime def)
{
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return def;
    QDateTime dt = Util::ParseDate(parent()->arguments().at(idx+1));
    if (!dt.isValid()) return def;
    return dt;
}

int XCmd::argValueInt(QString arg, int def)
{
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return def;
    bool ok;
    int val = parent()->arguments().at(idx+1).toInt(&ok);
    if (!ok) return def;
    return val;
}

double XCmd::argValueDouble(QString arg, double def)
{
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return def;
    bool ok;
    double val = parent()->arguments().at(idx+1).toDouble(&ok);
    if (!ok) return def;
    return val;
}

bool XCmd::argValueBool(QString arg, double def)
{
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return def;
    QString val = parent()->arguments().at(idx+1).trimmed().toLower();
    if (val == "yes" || val == "y" || val == "1" || val == "s" || val == "true") return true;
    else if (val == "no" || val == "n" || val == "0" || val == "false") return false;
    return def;
}

double XCmd::argValueRA(QString arg, double def)
{
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return def;
    QString val = parent()->arguments().at(idx+1).trimmed().toLower();
    double ra = Util::ParseRA(val);
    if (IS_INVALIDF(ra)) return def;
    return ra;
}

double XCmd::argValueDEC(QString arg, double def)
{
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return def;
    QString val = parent()->arguments().at(idx+1).trimmed().toLower();
    double dec = Util::ParseDEC(val);
    if (IS_INVALIDF(dec)) return def;
    return dec;
}

QSettings* XCmd::reg()
{
    if (mReg) return mReg;
    mReg = new QSettings("HKEY_CURRENT_USER\\Software", QSettings::NativeFormat);
    mReg->beginGroup(SECRET_KEY);
    mReg->setValue("dum",1);
    mReg->setValue("ran", qrand());
    return mReg;
}

QStringList XCmd::argValueStringList(QString arg)
{
    QStringList slist;
    int idx = parent()->arguments().indexOf(arg);
    if (idx < 0 || parent()->arguments().count() <= idx) return slist;
    for (int i=idx+1; i<parent()->arguments().count(); i++)
    {
        if (parent()->arguments().at(i).startsWith("-")) break;
        slist.append(parent()->arguments().at(i));
    }
    return slist;
}


QString XCmd::encrypt(QString secret, QString pass)
{
    QByteArray b_secret = secret.toLatin1();
    QByteArray b_pass = pass.toLatin1();
    for (int i=0; i<b_secret.count(); i++)
    {
        b_secret[i] = b_secret[i] ^ b_pass[i % b_pass.count()];
    }  
    return b_secret.toBase64();
}

QString XCmd::decrypt(QString data, QString pass)
{
    QByteArray b_data = QByteArray::fromBase64(data.toLatin1());
    QByteArray b_pass = pass.toLatin1();
    for (int i=0; i<b_data.count(); i++)
    {
        b_data[i] = b_data[i] ^ b_pass[i % b_pass.count()];
    }
    return QString::fromLatin1(b_data);
}

QString XCmd::getRegString(QString key)
{
   key = encrypt(key, SECRET_PASSK);
   QString val = reg()->value(key,"").toString();
   return decrypt(val, SECRET_PASS);
}

void XCmd::setRegString(QString key, QString val)
{
    key = encrypt(key, SECRET_PASSK);
    val = encrypt(val, SECRET_PASS);
    reg()->setValue(key, val);
}

int  XCmd::getRegInt(QString key)
{
    key = encrypt(key, SECRET_PASSK);
    QString val = reg()->value(key,"").toString();
    bool ok = false;
    int x = val.toInt(&ok, 10);
    if (!ok) return 0;
    return x;
}

void XCmd::setRegInt(QString key, int val)
{
    key = encrypt(key, SECRET_PASSK);
    QString val_str = encrypt(QString("").sprintf("%d",val), SECRET_PASS);
    reg()->setValue(key, val_str);
}
















