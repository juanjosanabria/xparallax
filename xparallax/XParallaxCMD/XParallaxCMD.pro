#-------------------------------------------------
#
# Project created by QtCreator 2014-06-01T19:39:42
#
#-------------------------------------------------

QT       += core network xml concurrent

TARGET = XParallaxCMD
CONFIG   += console
CONFIG   -= app_bundle
TEMPLATE = app
DEFINES  += _CRT_SECURE_NO_WARNINGS

RC_FILE = myapp.rc

#--------------------------------------------------------------------
#Flags de construcción
#--------------------------------------------------------------------
mingw:{
QMAKE_CXXFLAGS += -std=gnu++0x
}

#--------------------------------------------------------------------
#Dependencias
#--------------------------------------------------------------------
DEPENDPATH += . ../Core
INCLUDEPATH +=  ../Core

DEPENDPATH += . ../Algebra
INCLUDEPATH +=  ../Algebra

DEPENDPATH += . ../Novas
INCLUDEPATH +=  ../Novas

win32{ ICON = img/favicon.ico }
win32:release {
    LIBS +=  "-L$$OUT_PWD/../Algebra/Release" -lAlgebra
    LIBS +=  "-L$$OUT_PWD/../Core/Release" -lCore
    LIBS +=  "-L$$OUT_PWD/../Novas/Release" -lNovas
}
win32:debug {
    LIBS +=  "-L$$OUT_PWD/../Algebra/Debug" -lAlgebra
    LIBS +=  "-L$$OUT_PWD/../Core/Debug" -lCore
    LIBS +=  "-L$$OUT_PWD/../Novas/Debug" -lNovas
}

linux:{
    LIBS += -L"$$OUT_PWD/../Core" -lCore
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
    RESOURCES += ../Core/res_core.qrc
    QMAKE_CXXFLAGS += -std=c++11
    #Indica dos posibles rutas para cargar las librerías dinámicas
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN/lib
}

macx:{
    ICON = img/favicon.icns
    LIBS += -L"$$OUT_PWD/../Core" -lCore
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
    RESOURCES += ../Core/res_core.qrc
}

SOURCES += main.cpp \
    xcmdastrometry.cpp \
    xcmd.cpp \
    xcmdtraildetector.cpp \
    xcmdcalibration.cpp \
    advancedtraildetector.cpp \
    robusfit.cpp \
    xexception.cpp \
    xcmdcrop.cpp \
    superfit.cpp \
    superfit2.cpp \
    xcmdstardetector.cpp

HEADERS += \
    xcmd.h \
    xcmdastrometry.h \
    xcmdtraildetector.h \
    xcmdcalibration.h \
    advancedtraildetector.h \
    robusfit.h \
    xexception.h \
    xcmdcrop.h \
    superfit.h \
    superfit2.h \
    xcmdstardetector.h

RESOURCES += \
    res_cmd.qrc

OTHER_FILES += \
    myapp.rc

DISTFILES +=

