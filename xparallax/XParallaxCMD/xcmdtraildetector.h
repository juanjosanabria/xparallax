#ifndef XCMDTRAILDETECTOR_H
#define XCMDTRAILDETECTOR_H

#include "xcmd.h"
#include "fitimage.h"
#include "detection.h"
#include "xstringbuilder.h"
#include "advancedtraildetector.h"
#include "license.h"
#include <QPen>

//Términos de lentitud de imágenes. Lo que tiene que tardar
#define     DELAY_SLOW          (540*MAX_PROCESSES)     //Segundos por imagen
#define     DELAY_ULTRA_SLOW    (612*MAX_PROCESSES)     //Segundos por imagen
#define     DELAY_SULTRA_SLOW   (792*MAX_PROCESSES)     //Segundos por imagen


class XCmdTrailDetector : public XCmd
{
public:
    explicit XCmdTrailDetector(QCoreApplication *parent);
    ~XCmdTrailDetector();

protected:
    virtual QString helpRes();
    virtual QString iniEx();

private:
    int                         readCmdLine();
    //License                     mCurrentLicense;
    FitImage*                   mFitImage;
    QVector<TRAIL_DATA>         mTrailData;
    QString                     mIniFile;
    QString                     mInputFile;
    QString                     mOutputFile;
    QString                     mOutputPath;
    QString                     mDateToday;
    QString                     mDateYesterday;
    ATDParams                   mDetParams;
    License                     mLicense;

    //Acciones internas de detección
    void PrintTrailInfo();
    void OutputField(XStringBuilder* xsb, TRAIL_DATA* dt, const QString& field, int precission=-1);
    LicID GetId();
    void DownloadLicenses(bool force);
    bool CheckLicense();
    void ReportData();
    int GetProcessedFileCount(QString output_path);

signals:

public slots:
    void run();
};

#endif // XCMDTRAILDETECTOR_H
