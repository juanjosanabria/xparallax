#include "xcmdcalibration.h"
#include <QFileInfo>
#include <QDir>
#include "xglobal.h"
#include "core.h"
#include "calibration.h"

XCmdCalibration::XCmdCalibration(QCoreApplication *parent) :
    XCmd(parent)
{
    mCalPar = new CalibrationParameters();
    mCalList = NULL;
}

XCmdCalibration::~XCmdCalibration()
{
    if (mCalPar) delete mCalPar;
    mCalPar = NULL;
    if (mCalList) delete mCalList;
    mCalList = NULL;
}

QString XCmdCalibration::helpRes()
{
  return ":/help/res/xcmdcalibration.txt";
}

QString XCmdCalibration::iniEx()
{
    return ":/help/res/xcmdcalibration.ini";
}


void XCmdCalibration::run()
{
    INFO("Image calibration process");
    int ret = readCmdLine();
    if (ret)  return parent()->exit(ret);



    INFO("Reading input files");
    mCalList = new CalibrationList(mInputFiles);
    INFO("Read OK. Bias: %d, Flats: %d, Darks: %d, Object: %d",
         mCalList->biasCount(),
         mCalList->flatCount(),
         mCalList->darkCount(),
         mCalList->lightCount());

    mCalPar->output_dir = mOutputDir;

    CalibrationComputer* cc = new CalibrationComputer();
    INFO("Calibration starts");
    if (!cc->doWork(mCalPar, mCalList))
    {
        ERR("Error calibrating");
        return parent()->exit(-1);
    }
    delete cc;

    INFO("Calibration finished OK");
    emit finished();
}


int XCmdCalibration::readCmdLine()
{
    mIniFile = argValueStr("-ini");
    if (mIniFile.length() && !QFileInfo(mIniFile).exists())
    {
        ERR("Ini file '%s' does not exits", PTQ(mIniFile));
        return ERR_INI_FILE;
    }
    mCalPar->Load(settings(), "CALIBRATION");

    mInput = argValueStr("-i");
    if (!mInput.length())
    {
        ERR("Input files '%s' does not exits", PTQ(mInput));
        return ERR_UNABLE_OPEN_INPUT_FILE;
    }
    //Ahora se procesan todos los archivos de entrada
    mInputFiles.clear();
    QStringList list = mInput.split(',',QString::SkipEmptyParts);
    for (int i=0; i<list.length(); i++)
    {
        INFO("ADD: %s", PTQ(list[i]));
        //Listas de archivos o caracteres comodines
        if (list[i].contains("*") || list[i].contains("?"))
        {
            QDir dir = QFileInfo(list[i]).absoluteDir();
            QString dirname = dir.absolutePath();
            QString filter = QFileInfo(list[i]).fileName();
            if (!dir.exists(dirname))
            {
                ERR("Input dirctory: %s does not exist", PTQ(dirname));
                return ERR_UNABLE_OPEN_INPUT_FILE;
            }
            QStringList filters; filters.append(filter);
            dir.setNameFilters(filters);
            QStringList files = dir.entryList();
            for (int i=0; i<files.length(); i++)
            {
                QString filename = QDir::cleanPath(dirname + QDir::separator() + files[i]);
                mInputFiles.append(filename);
                INFO("\t+ %s", PTQ(files[i]));
            }
        }
        //Archivos tal cual, sin comodines ni nada
        else if (QFile::exists(list[i]))
        {
            mInputFiles.append(list[i]);
            INFO("\t+ %s", PTQ(list[i]));
        }
        else
        {
           ERR("Input file: %s does not exist", PTQ(list[i]));
           return ERR_UNABLE_OPEN_INPUT_FILE;
        }
    }


    mOutputDir = argValueStr("-o");
    INFO("Output path: %s", PTQ(mOutputDir));
    if (!mOutputDir.length())
    {
        mOutputDir = QDir::currentPath();
        INFO("No output directory, using current directory: '%s'", PTQ(mOutputDir));
    }
    if (mOutputDir == "null") INFO("NO OUTPUT DIRECTORY SELECTED");

    QDir odir = QDir(mOutputDir);
    if (!odir.exists())
    {
        INFO("Output dir does not exist. Creating");
        if (!QDir().mkdir(mOutputDir))
        {
            ERR("Unable to create output path: %s", PTQ(mOutputDir));
            return ERR_UNABLE_WRITE_OUTPUT_FILE;
        }
    }

    //Comprobar que todos los archivos de entrada existen
    for (int i=0; i<mInputFiles.length(); i++)
    {
        if (!QFile::exists(mInputFiles[i]))
        {
            ERR("Input file: %s does not exist", PTQ(list[i]));
            return ERR_UNABLE_OPEN_INPUT_FILE;
        }
    }

    return ERR_NO_ERROR;
}
