#include "xcmdstardetector.h"
#include "detection.h"

XCmdStarDetector::XCmdStarDetector(QCoreApplication *parent) :
    XCmd(parent)
{
    mFitImage = NULL;
    mDetector = NULL;
    mInputFilename = "";
    mOutputFilename = "STDOUT";
    mOutputFile = stdout;
}

XCmdStarDetector::~XCmdStarDetector()
{
    if (mFitImage) delete mFitImage;
    mFitImage = NULL;
    if (mDetector) delete mDetector;
    mDetector = NULL;
}

void XCmdStarDetector::run()
{
    INFO("-- XParallax Star Detector ---");

    mInputFilename = argValueStr("-i","");
    if (!(QFile(mInputFilename).exists()))
    {
        ERR("Error. Invalid input file name or input file name does not exist");
        return parent()->exit(-1);
    }

    mFitImage = FitImage::OpenFile(mInputFilename);

    if (!mFitImage)
    {
        ERR("Error. Invalid input file name or input file name does not exist");
        return parent()->exit(-1);
    }

    mDetector = new StarDetectorParallel(mFitImage->data(), mFitImage->width(), mFitImage->height(), mParameters);
    mDetector->DetectStars(0, 0, mFitImage->width()-1, mFitImage->height()-1);

    int count = mDetector->count();
    INFO("Detected %d stars", count);
    INFO("X Y RADIUS  FLUX");
    INFO("---------------------------------------------");
    for (int i=0; i<count; i++)
    {
       DetectedStar* ds = mDetector->stars()+i;
       fprintf(mOutputFile, "S: %0.3f %0.3f %0.3f %0.1f\n", ds->x(), ds->y(), ds->radius(), ds->fluxISO());
    }

    //Terminador
    INFO("---------------------------------------------");
    parent()->exit(0);
}




