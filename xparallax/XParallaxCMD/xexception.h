#ifndef XEXCEPTION_H
#define XEXCEPTION_H

#include <QException>

class XException :
        public QException
{
private:
    QString mMsg;
    int     mErr;

public:
    XException(const QString msg);
    XException(const QString msg, int err);

    const QString& msg(){return mMsg;}
    int err(){return mErr;}
};

#endif // XEXCEPTION_H
