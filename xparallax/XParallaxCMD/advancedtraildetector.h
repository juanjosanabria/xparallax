#ifndef ADVANCEDTRAILDETECTOR_H
#define ADVANCEDTRAILDETECTOR_H

#include "fitimage.h"
#include "g2d.h"
#include "vo.h"
#include "detection.h"
#include "matcher.h"
#include "superfit.h"
#include <QPolygon>
#include <QLineF>

#define ATD_VERSION     "2.0.9"
#define LIC_FILE        "trails.lic"

struct TRAIL_DATA
{
    int              id;             //Identificador de la traza
    QDateTime        dateFrame;      //Punto medio de la imagen
    double           length;         //Longitud de la traza en píxeles
    double           cRA;            //Coordenada J2000 del centro de la traza
    double           cDEC;           //Coordenada J2000 del centro de la traza
    double           f1RA;           //Ascensión recta del flanco 1
    double           f1DEC;          //Declinación del flanco 1
    double           f2RA;           //Ascensión recta del flanco 2
    double           f2DEC;          //Declinación del flanco 2
    double           cX;             //Coordenadas de píxel del centro de la traza
    double           cY;             //Coordenadas de píxel del centro de la traza
    double           speed;          //Velocidad de la traza en arcsecs/sec
    double           flux;           //Flujo total de la traza restado de fondo
    int              area;           //Área de la traza en píxeles
    double           pa;             //Ángulo de posición con respecto al norte en grados
    double           snr;            //Signal to-noise ratio

    //Datos del ajuste lineal
    double           r;              //Coeficiente de correlación de Pearson
    double           s;              //Coeficiente de correlación de spearman
    double           rho;            //Coeficiente de correlación en la traza
    int              pixel_count;    //Número de píxeles pertenecientes al trail
    QVector<QPointF> bbox;           //Polígono que envuelve a la caja
};

class ATDParams
        : public Parameters
{
    public:
        ATDParams(){SetDefaults();}
        ~ATDParams(){;}
        void SetDefaults();
        void Save(QSettings* settings,const QString& section);
        void Load(QSettings* settings,const QString& section);

    public:
        //Parámetros de debugueo
        //---------------------------------------------
        bool    save_image_without_stars_fit;
        bool    save_image_without_stars_img;
        bool    save_overlays_iter_img;
        bool    save_final_overlay_img;
        bool    save_profile_img;
        bool    save_fitting_data;
        bool    save_regr_line_data;
        //Parámetros de dibujado para guardar imágenes
        //---------------------------------------------
        QString output_profile_img_format;
        QString output_overlay_img_format;
        //Parámetros de cálculo
        //---------------------------------------------
        float   snr_min;                //Mínima relación señal a ruido
        int     trail_est_length;       //Tamaño estimado en píxeles para un trail. 0 para no indicars
        int     trail_est_flank_size;   //Número de píxeles máximos para ajustar flancos con longitudes estimadas
        int     trail_est_sig_flanks;   //Número de píxeles hacia adentro del flanco para calcular un nivel de señal
        int     trail_mid_height;       //Mitad del alto de un trail
        int     bin;                    //Bin para hacer. 1 no se haci bin
        int     detect_box_width;       //Tamaño de la caja de detección
        int     detect_box_height;
        float   rho_min;                //Al final de la detección. Mínimo coeficiente de ajuste de spearman (en el perfil)
        int     min_area;               //Mínima area en píxeles para considerar un candidato
        int     min_area_regr;          //Mínima area el píxeles para calcular una recta de regresión
        bool    regr_coeff_spearman;    //Usar el coeficiente de regresión de spearman (1) o pearson (0)
        float   sigma_units;            //Unidades de desviación típica   
        int     slice_final_height;     //Tamaño de la rodaja final en píxeles 20 por ejemplo
        int     slice_iter_decrement;   //Decremento
        int     min_neighbours;         //Mínimo número de vecinos para considerar un punto en conjunto
        int     max_iters_extend_flanks;//Máximo de iteraciones de extensión de flancos
        bool    superfit;

        //Parámetros de recorte de las imágenes
        int     crop_left;
        int     crop_right;
        int     crop_top;
        int     crop_bottom;

};

class ATDBoundingBox
{
    private:
        QPolygonF       mPoly;       //Bounding box que contiene la traza
        QPolygonF       mExtPoly;    //Bounding box de cáculo fondo, que rodea a la primera
        G2D_Line        mReg;        //Recta de regresión para la traza
        QPointF         mP1;         //Punto semilla 1. La BBOX va de p1 a p2 y tiene un alto de mHeight
        QPointF         mP2;         //Punto semilla 2
        int             mId;
        int             mHeight;     //Alto del bounding box
        int             mPointCount; //Número de puntos
        double          mBackground;
        double          mBackgroundSigma;
        double          mBaseBackground;            //Background original
        double          mBaseBackgroundSigma;
        double          mSignalLevel;
        double          mSignalLevelAroundFlanks;
        double          mIgnoreOver;
        double          mSNr;
        QVector<double> mFoldBox;
        QVector<double> mFlanks;
        Transform2D_ER  mTransform;
        QVector<double> mFoo;        //Coeficientes de la función de ajuste   f(x) = a * sech(( (2(x-c))/b - 1)^d)^2 + e
        double          mRhoL;       //Coeficiente de correlación de sperman de la rectra de regresión
        double          mRhoF;       //Coeficiente de correlación de sperman del ajuste


        void       ComputeBoundingBox(QPointF p1, QPointF p2,int midHeight);

    public:
        ATDBoundingBox(){mP1 = mP2 = QPointF(0,0);mId=-1;mPointCount=-1;mIgnoreOver=1e32;mSNr=mRhoL=INVALID_FLOAT;mRhoF=0;}
        ATDBoundingBox(QPointF p1, QPointF p2,int height){mId=-1;mPointCount=-1;mSNr=INVALID_FLOAT;ComputeBoundingBox(p1,p2,height);mIgnoreOver=1e32;}
        QPolygonF polygon(){return mPoly;}
        void setPolygon(QPolygonF pol){mPoly=pol;}
        QPolygonF polygonEX(){return mExtPoly;}
        void setPolygonEX(QPolygonF pol){mExtPoly=pol;}
        G2D_Line   regr(){return mReg;}
        void setRegr(G2D_Line f){mReg=f;}
        QPointF p1(){return mP1;}
        void setP1(QPointF p1){ComputeBoundingBox(p1,mP2,mHeight);}
        QPointF p2(){return mP2;}
        void setP2(QPointF p2){ComputeBoundingBox(mP1,p2,mHeight);}
        int height(){return mHeight;}
        void setHeigt(int h){ComputeBoundingBox(mP1,mP2,h);}
        int id(){return mId;}
        void setId(int sid){mId=sid;}
        int pointCount(){return mPointCount;}
        void setPointCount(int pc){mPointCount=pc;}
        void showConsole();
        void setBackground(double bkg,double sigma){mBackground = bkg; mBackgroundSigma = sigma;}
        void setBaseBackground(double bkg,double sigma){mBaseBackground = bkg; mBaseBackgroundSigma = sigma;}
        void foldBoxStats(int start_idx, int end_idx,double* avg, double* stdev,int* point_count);
        void computeSNr();
        double bkg(){return mBackground;}
        double bkgSigma(){return mBackgroundSigma;}
        double baseBkg(){return mBaseBackground;}
        double baseBkgSigma(){return mBaseBackgroundSigma;}
        double signalLevel(){return mSignalLevel;}
        double signalLevelAroundFlanks(){return mSignalLevelAroundFlanks;}
        void setSignalLevel(double level){mSignalLevel = level;}
        void setSignalLevelAroundFlanks(double level){mSignalLevelAroundFlanks = level;}
        double ignoreOver(){return mIgnoreOver;}
        void setIgnoreOver(double level){mIgnoreOver = level;}
        void setFoldBox(const QVector<double>& fb){mFoldBox=fb;}
        QVector<double>* foldBox(){return &mFoldBox;}
        void setFlanks(const QVector<double>& fb){mFlanks=fb;}
        QVector<double>* flanks(){return &mFlanks;}
        void setTransform(const Transform2D_ER& t){mTransform=t;}
        Transform2D_ER* transform(){return &mTransform;}
        QVector<double>* foo(){return &mFoo;}
        void setFoo(const QVector<double>& f){mFoo=f;}
        void showFoo();
       //double fooFunc(double x){return mFoo[0] * POW2(SQSECH(pow((2*x-2*mFoo[2])/mFoo[1]-1,mFoo[3]))) + mFoo[4];}

        void setRhoL(double rhol){mRhoL = rhol;}
        double rhoL(){return mRhoL;}
        void setRhoF(double rhof){mRhoF = rhof;}
        double rhoF(){return mRhoF;}
        double sNr(){if (!mFoldBox.length()) return INVALID_FLOAT;
            if (IS_INVALIDF(mSNr)||(mSNr < 1.00)) computeSNr(); return mSNr;}
};


class ATDDetector
{
    private:
        ATDParams               mSourcePar;
        ATDParams               mPar;
        FitImage*               mSourceImage;               //Sin binning
        FitImage*               mSourceImageWithoutStars;   //Sin binnning

        FitImage*               mWorkImage;
        FitImage*               mFitWithoutStars;
        FitImage*               mFitWithoutStarsBK;

        FitImage*               mImageBinned;



        int                     mWidth;
        int                     mHeight;
        QString                 mWorkingPath;
        QVector<VizierRecord>   mStars;
        QVector<QPoint>         mPointAccepted;
        int                     mIter;
        double           mBackground;
        double           mBackgroundSigma;
        double           mGlobalBackground;
        double           mGlobalBackgroundSigma;


        void CheckLicense();

        /**
         * @brief GetRegressionLine Calcula la recta de regresión que se ajusta
         * @param bbox Bounding box. O caja en torno a la cual
         * @param csigma
         * @return
         */
        G2D_Line GetRegressionLine(QPolygonF bbox,double csigma);

        void SaveOverlay(FitImage* fi, ATDBoundingBox& bb,bool color_points=true);
        void SaveFinalOverlay(const QVector<ATDBoundingBox> &boxes);
        void SaveRegrLine(ATDBoundingBox& bb);
        void SaveSliceProfile(ATDBoundingBox& bb,double* data, int datalen,QString suffix);
        void SaveFittingData(ATDBoundingBox& bb);
        void CreateImageWithoutStars(FitImage* fi, int bin_at_0304);
        void ReadStarsInField();
        bool CheckTrailParams(ATDBoundingBox &can);

        /**
         * @brief ComputeGlobalBackground Calcula el fondo de la imagen completa
         */
        void ComputeGlobalBackground(FitImage* fi);
        void ComputeBackground(FitImage* fi,QPolygonF external,bool mode=true);
        QString GetOutputFilename(QString prefix, QString ext,bool for_new=true);
        bool IsPointOfTrail(QPoint p);
        static bool RemoveStarPoint(StarDetector*, int x, int y, float /*flux*/, void *user_data);

        void                    RemoveDuplicates(QVector<ATDBoundingBox>* candidates, double percent_tolerance);
        void                    AssertFlanksInsideProfile(QVector<ATDBoundingBox>& candidates);
        ATDBoundingBox          DetectInBox(QPointF p1, QPointF p2, int sliceInitialHeight, double sigma_units);
        QVector<ATDBoundingBox> BacktoBin1x1(QVector<ATDBoundingBox>& can);

        double LinearFittingCoefficientW(const QVector<QPoint> &points, const QVector<double>& weights);
        double LinearFittingCoefficient(const QVector<QPoint> &points);        
        double LinearSpearmanCoefficient(const QVector<QPoint>& points, double m, double b);
        double LinearSpearmanCoefficientW(const QVector<QPoint>& points, double m, double b, const QVector<double>& weights);
        double ProfileSpearmanCoefficient(ATDBoundingBox& bbbox);
        double ProfileSpearmanCoefficientOnFlanks(ATDBoundingBox& bbox);
        double SquareDifferenceOnFlanks(ATDBoundingBox& bbox, bool left, bool right);

        void GetMinMaxIter(QPolygonF poly,double* minx, double *max,double *miny, double* maxy);

        /**
         * @brief SaveImageBinned Salva la imagen con el bin especificado
         * @param fi
         * @param bin
         */
        FitImage* CreateImageBinned(FitImage* fi,QString savefilename = "");

        /**
         * @brief FoldBoundigBox Abate los datos de un bounding box a un vector de una sola dimensión
         * con los datos acumulados.
         * @param data  Vector donde se almacenarán los datos
         * @param trans Transformación aplicada que permite el cambio de unas coordenadas a otras
         */
        void FoldBoundigBox(ATDBoundingBox& bb, FitImage* fi, QVector<double>* data, Transform2D_ER* trans);

        /**
         * @brief ComputeSignalLevel Calcula el valor de fondo,señal del slice acumulado
         * @param bb Bounding box
         * @return Vector de dos elementos, fondo y señal y la desviación típica del fondo
         */
        QVector<double> ComputeSignalLevel(ATDBoundingBox& bb);


        QVector<double> ComputeImpreciseFlanks(ATDBoundingBox& bb);

        /**
         * @brief ComputeImpreciseFlanks Calcula el flanco impreciso, aproximado de los datos del
         * slice abatidos.
         * @param bb
         * @return
         */
        QVector<double> ComputeImpreciseFlanks1(ATDBoundingBox& bb);

        /**
         * @brief ATDDetector::ComputeImpreciseFlanks
         * @param bb
         * @param suggested_width Ancho sugerido de la traza
         * @return
         */
        QVector<double> ComputeImpreciseFlanks2(ATDBoundingBox& bb, int suggested_width);



        /**
         * @brief ComputePreciseFlanks Calcula los flancos precisos a partir de los flancos imprecisos
         * proporcionados
         * @param bb
         * @return left_flank_x, right_flank_y, (valores AJUSTE a,b,c,d,e)  f(x) = a * sech(( (2(x-c))/b - 1)^d)^2 + e
         */
        bool ComputePreciseFlanks(ATDBoundingBox* bb,QVector<double>* flanks, QVector<double>* foo,double suggested_d=0);
        bool ComputePreciseFlanks1(ATDBoundingBox* bb,QVector<double>* flanks, QVector<double>* foo,double suggested_d=0);
        bool ComputePreciseFlanks2(ATDBoundingBox* bb,QVector<double>* flanks, QVector<double>* foo,double suggested_d=0);
        bool ComputeSuperPreciseFlanks(ATDBoundingBox* bb,QVector<double>* flanks, QVector<double>* foo);
        bool ComputeSuperPreciseFlanks(ATDBoundingBox* bb,QVector<double>* flanks, QVector<double>* foo, bool left, bool right);


        /**
         * @brief AdjustToImpreciseFlanks Recalcula el bounding box para ajustarse mejor a los flancos izquierdo y
         * derecho. Solamente recalcula el bounding box. Todos los demás cálculos, como buscar flancos etc... deberán ser
         * hechos de nuevo
         * @param bb
         * @return
         */
        ATDBoundingBox AdjustToFlanks(ATDBoundingBox& bb);

        ATDBoundingBox ResizeBox(ATDBoundingBox& bb,int  sz_left, int sz_right);


        TRAIL_DATA ComputeTrailData(ATDBoundingBox& bbox);
public:
        ATDDetector(FitImage*fi);
        void setParams(ATDParams par){mPar = par;}
        void Detect1(QPointF p1, QPointF p2);
        QVector<TRAIL_DATA>  DetectBox();
        QString workingPath(){return mWorkingPath;}

};

#endif // ADVANCEDTRAILDETECTOR_H
