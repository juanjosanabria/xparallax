-------------------------------------------------------------------------------
- [XPROGRAM] v[XVERSION] by Juan Jose Sanabria
- Please, report suggestions and issues to: [XMAIL]
-------------------------------------------------------------------------------
- Help about ** Astrometric reduction **
-------------------------------------------------------------------------------
Usage:
XParallaxCMD -ast -i input_file -o output_fie [OPTIONS]
-------------------------------------------------------------------------------
- Use it with the following arguments [M]=Mandatory, [O]=Optional
-------------------------------------------------------------------------------
-i    [M] Input file path.
-ini  [O] Ini file with Detection/Matcher parameters
-o    [O] Output file path. If not set, the astrometric reduction headers will
      be added to the input file. Set to 'null' if no output required.
-ra   [O] Right ascention at the center of the image. If not set or set to AUTO
      it will be extracted information from fits header. Format HH:MM:SS
-dec  [O] Declination at the center of the image. If not set or set to AUTO
      it will be extracted information from fits header. Format DD:MM:SS
-raeq [O] J2000 (default) or EQUINOX. Specify the format of image header
      or input coordinates.
-ref  [O] Optional file to output astrometric reduction information
-oi   [O] Override ini parameter. Override ini parameter -oi [SECTION]param=value
