#include "testXParallax.h"
#include "xglobal.h"
#include "util.h"
#include "vo.h"


void TestXParallax::vizier_Catalogs()
{
    QString ra      = "19 03 50.7";
    QString dec     = "+36 37 58.4";
    double radius   = 0.3861333;
    float mag       = 14.0f;

    // This declares a lambda, which can be called just like a function
    auto testCatalog = [](const QString& name, const QString& server, const QString& raStr, const QString& decStr,
            double radius, float mag)
    {
        DINFO("Download: %s, %s, (%s : %s)", PTQ(name), PTQ(server), PTQ(raStr), PTQ(decStr));
        VizierReader* vizierReader_ = new VizierReader();

        vizierReader_->setUseCache(false);
        vizierReader_->SetCatalog(VizierCatalog::getCatalog(name));
        vizierReader_->SetServer(VizierServer::getServer(server));
        double ra = Util::ParseRA(raStr);
        double dec = Util::ParseDEC(decStr);

        bool ok = vizierReader_->webRead(ra, dec, radius, mag);
        QTEST_ASSERT(ok);
        QTEST_ASSERT(vizierReader_->stars()->count() > 100);
        delete vizierReader_;
    };   

    testCatalog("UCAC4",     "CDS", ra, dec, radius, mag);
    testCatalog("URAT1",     "CDS", ra, dec, radius, mag);
    testCatalog("GAIADR1",   "CDS", ra, dec, radius, mag);
    testCatalog("GAIADR2",   "CDS", ra, dec, radius, mag);
    testCatalog("PPMXL",     "CDS", ra, dec, radius, mag);
    testCatalog("USNO-B1.0", "CDS", ra, dec, radius, mag);
    testCatalog("GSC2.3",    "CDS", ra, dec, radius, mag);
    testCatalog("2MASS",     "CDS", ra, dec, radius, mag);
}


void TestXParallax::sessame_Read()
{
    // Arrange
    SesameReader* sr = new SesameReader();

    // Act
    NameRecord nrAltair = sr->resolveName("Altair");
    QStringList posAltair { Util::RA2String(nrAltair.ra()), Util::DEC2String(nrAltair.dec()) };

    NameRecord nrVega = sr->resolveName("Vega");
    QStringList posVega{ Util::RA2String(nrVega.ra()), Util::DEC2String(nrVega.dec()) };

    NameRecord nrM13 = sr->resolveName("M13");
    QStringList posM13{Util::RA2String(nrM13.ra()), Util::DEC2String(nrM13.dec())} ;

    // Assert
    QTEST_ASSERT(!nrAltair.isnull());
    QCOMPARE(posAltair[0], "19 50 47.00");
    QCOMPARE(posAltair[1], "+08 52 06.0");

    QTEST_ASSERT(!nrVega.isnull());
    QCOMPARE(posVega[0], "18 36 56.34");
    QCOMPARE(posVega[1], "+38 47 01.3");

    QTEST_ASSERT(!nrM13.isnull());
    QCOMPARE(posM13[0], "16 41 41.63");
    QCOMPARE(posM13[1], "+36 27 40.7");

    // Acabar
    delete sr;
}
