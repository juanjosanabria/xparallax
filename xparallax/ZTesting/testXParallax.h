#ifndef TESTVIZIER_H
#define TESTVIZIER_H

#include <QObject>
#include <QtTest/QtTest>

class TestXParallax : public QObject
{
    Q_OBJECT
public:
    explicit TestXParallax(QObject *parent = nullptr);

private slots:

    virtual void initTestCase();

    //***********************************************************************************
    // VO tests
    //***********************************************************************************
    void vizier_Catalogs();
    void sessame_Read();

};

#endif // TESTVIZIER_H
