#-------------------------------------------------
#
# Project created by QtCreator 2015-02-05T10:55:18
#
#-------------------------------------------------

QT       += core network xml testlib

TARGET = ZTesting
CONFIG   += console
CONFIG   -= app_bundle
TEMPLATE = app
DEFINES  += _CRT_SECURE_NO_WARNINGS

#--------------------------------------------------------------------
#Dependencias
#--------------------------------------------------------------------
DEPENDPATH += . ../Core
INCLUDEPATH +=  ../Core

DEPENDPATH += . ../Algebra
INCLUDEPATH +=  ../Algebra

DEPENDPATH += . ../Novas
INCLUDEPATH +=  ../Novas

DEPENDPATH += . ../Sky
INCLUDEPATH +=  ../Sky

win32:release {
    LIBS +=  "-L$$OUT_PWD/../Algebra/Release" -lAlgebra
    LIBS+=  "-L$$OUT_PWD/../Core/Release" -lCore
    LIBS+=  "-L$$OUT_PWD/../Novas/Release" -lNovas
    LIBS+=  "-L$$OUT_PWD/../Sky/Release" -lSky
}
win32:debug {
    LIBS +=  "-L$$OUT_PWD/../Algebra/Debug" -lAlgebra
    LIBS+=  "-L$$OUT_PWD/../Core/Debug" -lCore
    LIBS+=  "-L$$OUT_PWD/../Novas/Debug" -lNovas
    LIBS+=  "-L$$OUT_PWD/../Sky/Debug" -lSky
}

linux:{
    LIBS += -L"$$OUT_PWD/../Core" -lCore
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
    LIBS += -L"$$OUT_PWD/../Sky" -lSky
    RESOURCES += ../Core/res_core.qrc
    QMAKE_CXXFLAGS += -std=c++11
    #Indica dos posibles rutas para cargar las librerías dinámicas
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN
    QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN/lib
}

macx:{
    ICON = img/favicon.icns
    LIBS += -L"$$OUT_PWD/../Core" -lCore
    LIBS += -L"$$OUT_PWD/../Algebra" -lAlgebra
    LIBS += -L"$$OUT_PWD/../Novas" -lNovas
    LIBS += -L"$$OUT_PWD/../Sky" -lSky
    RESOURCES += ../Core/res_core.qrc
}

SOURCES += main.cpp \
    vo.cpp

HEADERS += \
    testXParallax.h

