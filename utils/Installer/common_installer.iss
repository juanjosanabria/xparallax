; -- 64Bit.iss --
; -- 64Bit.iss --
; Demonstrates installation of a program built for the x64 (a.k.a. AMD64)
; architecture.
; To successfully run this installation and the program it installs,
; you must have a "x64" edition of Windows.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!
;Ayuda: http:\\www.jrsoftware.org\ishelp\index.php?topic=setup_wizardsmallimagefile
#define ApplicationName 'XParallax'
#define ApplicationSName 'XParallax'
#define ApplicationVersion '1.2.5'
#define App

[Setup]
AppName={#ApplicationName}
AppVersion={#ApplicationVersion}
AppContact=info@xparallax.com
AppPublisher=Juan Jos� Sanabria
AppPublisherURL=http://www.xparallax.com/
DefaultDirName={commonpf}\XParallax
DefaultGroupName=XParallax
UninstallDisplayIcon={app}\bin\XParallax.exe
Compression=lzma2
SolidCompression=yes
OutputDir=.\
AppMutex=XParallax_semaphore
;Im�genes
SetupIconFile=Additional\icinst.ico
WizardSmallImageFile=Additional\logo_64.bmp
WizardImageFile=Additional\Simple_left.bmp

; "ArchitecturesAllowed=x64" specifies that Setup cannot run on
; anything but x64.
ArchitecturesAllowed={#AppArquitecture}
; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
ArchitecturesInstallIn64BitMode=x64
OutputBaseFilename=XParallax_Setup_v{#ApplicationVersion}_win{#AppBits}

[Files]
Source:..\run_environment\bin{#AppBits}\{#ApplicationSName}.exe; DestDir: "{app}\bin"; DestName: "{#ApplicationSName}.exe"; Flags: ignoreversion
;Source: "MyProg.chm"; DestDir: "{app}"
;Source: "Readme.txt"; DestDir: "{app}"; Flags: isreadme
Source:..\run_environment\bin{#AppBits}\*; DestDir: "{app}\bin"; Flags: ignoreversion
Source:..\run_environment\plugins{#AppBits}\*; DestDir: "{app}\plugins"; Flags: recursesubdirs ignoreversion
Source:..\run_environment\fonts\*; DestDir: "{app}\fonts"; Flags: recursesubdirs ignoreversion 


[Icons]
;Name: "{group}\gtrr"; Filename: "{app}\bin\XParallax.exe"; IconFilename: "{app}\bin\XParallax.exe";
Name: "{group}\{#ApplicationName}"; Filename: "{app}\bin\{#ApplicationSName}.exe"; 
;Icono de acceso directo en la carpeta del instalador
Name: "{app}\{#ApplicationName}"; Filename: "{app}\bin\XParallax.exe"; WorkingDir: "{app}\bin"


[Run]
Filename: "ie4uinit.exe"; Parameters: "-ClearIconCache"; Flags: runhidden
Filename: "{app}\bin\XParallax.exe"; Description: {cm:LaunchProgram,{#ApplicationName}}; Flags: nowait postinstall skipifsilent

[Tasks]
Name: assoc_images; Description:"&Associate with Image types (.fit, .fts, .fits, .sbig, .st6)"; GroupDescription: "File association:"
Name: associate_mpc; Description:"&Associate with .mpc files"; GroupDescription: "File association:"

[Registry]
;Asociar archivos fit/fits
Tasks: assoc_images; Root: HKCR; Subkey: ".fit"; ValueType: string; ValueName: ""; ValueData: "{#ApplicationSName}.Fitfile"; Flags: uninsdeletevalue; 
Tasks: assoc_images; Root: HKCR; Subkey: ".fits"; ValueType: string; ValueName: ""; ValueData: "{#ApplicationSName}.Fitfile"; Flags: uninsdeletevalue; 
Tasks: assoc_images; Root: HKCR; Subkey: ".fts"; ValueType: string; ValueName: ""; ValueData: "{#ApplicationSName}.Fitfile"; Flags: uninsdeletevalue; 
Tasks: assoc_images; Root: HKCR; Subkey: "{#ApplicationSName}.Fitfile"; ValueType: string; ValueName: ""; ValueData: "Astronomy image file"; Flags: uninsdeletekey
Tasks: assoc_images; Root: HKCR; Subkey: "{#ApplicationSName}.Fitfile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\bin\icons.dll,0"; Flags: uninsdeletekey
Tasks: assoc_images; Root: HKCR; Subkey: "{#ApplicationSName}.Fitfile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\bin\{#ApplicationSName}.exe"" ""%1"""; Flags: uninsdeletekey

Tasks: assoc_images; Root: HKCR; Subkey: ".sbig"; ValueType: string; ValueName: ""; ValueData: "{#ApplicationSName}.SbigFile"; Flags: uninsdeletevalue; 
Tasks: assoc_images; Root: HKCR; Subkey: ".st6"; ValueType: string; ValueName: ""; ValueData: "{#ApplicationSName}.SbigFile"; Flags: uninsdeletevalue; 
Tasks: assoc_images; Root: HKCR; Subkey: "{#ApplicationSName}.SbigFile"; ValueType: string; ValueName: ""; ValueData: "Astronomy image file"; Flags: uninsdeletekey
Tasks: assoc_images; Root: HKCR; Subkey: "{#ApplicationSName}.SbigFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\bin\icons.dll,0"; Flags: uninsdeletekey
Tasks: assoc_images; Root: HKCR; Subkey: "{#ApplicationSName}.SbigFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\bin\{#ApplicationSName}.exe"" ""%1"""; Flags: uninsdeletekey

Tasks: associate_mpc; Root: HKCR; Subkey: ".mpc"; ValueType: string; ValueName: ""; ValueData: "{#ApplicationSName}.Mpcfile"; Flags: uninsdeletevalue; 
Tasks: associate_mpc; Root: HKCR; Subkey: "{#ApplicationSName}.Mpcfile"; ValueType: string; ValueName: ""; ValueData: "Minor Planet Center report"; Flags: uninsdeletekey
Tasks: associate_mpc; Root: HKCR; Subkey: "{#ApplicationSName}.Mpcfile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\bin\icons.dll,1"; Flags: uninsdeletekey
Tasks: associate_mpc; Root: HKCR; Subkey: "{#ApplicationSName}.Mpcfile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\bin\{#ApplicationSName}.exe"" ""%1"""; Flags: uninsdeletekey



