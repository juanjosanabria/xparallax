<?php include('common.php'); ?>
<!DOCTYPE html> 
<html>
<head>
<?php include('sub_header.php'); ?>
<title>Calibrating images with XParallax viu</title>
<meta name='keywords' lang='en' content='<?PHP echo GLOBAL_KEYWORDS; ?>' />
<meta name='description' lang='en' content='<?PHP echo GLOBAL_DESC ?>' />
</head>


<body>
<div class="main_wrapper">
    <?php 
		include('sub_menu.php'); 
	?>

<div class="totalbox">
<?PHP include('sub_submenu.php');  ?>
    <div class="submenu_content">		
	    <h3>Image calibration</h3><br/>
		<p>
		You can use <?PHP echo PROGRAM_NAME ?> to calibrate your source CCD images. The calibration process is automatic, so calibration frames (flat, bias or dark) and calibrated frames (light) must be acquired at the same binning (mandatory) and temperature (if possible).
		</p><br/>
		<p>
		There is no problem by using sky-flats, taken pointing the telescope to the sunset or sunrise sky. Stars present in flat frames will be removed and their pixels averaged. In this case, move the telescope between exposures. Flats with different exposure time can be used (avoid saturated pixels) but it is better to use the same as the light frames. Don't worry about small diferences in light intensity between frames, it will be fixed automaticaly too.
		</p><br/>
		<p>
		If you are using dark frames, bias information will be extracted from them, so don't waste your time by taking bias frames (altough they are zero exposure).
		</p>
		<br/><br/>
		
		<h3>The calibration dialog</h3><br/>
		<p>
		Select the menu option "Image processing > Image calibration" to open the calibration dialog. Two tabs are present in this dialog where you can select the input files and output directory and where configure the calibration options.
		</p>
		<br/></br>
		
		<h3>Input files selection</h3><br/>
		<p>
		The first step is to select input files in the tab "Source images".
		</p>
		<br/>
		
		<p style='text-align:middle;'>
			<img src='sampimg/calib_01.png' width='358' height='423' style='display:inline' title='XParallax viu calibration dialog' />
			<br/><br/>
		<p>
		<p>
		File naming is so important to let the program to determine the frame type. Please, make sure that input files contains the words <b>bias, dark or flat</b>, upper or lowercased. All other files will be considered as light frames. Take it in account before start capturing frames if you want to perform the calibration with <?PHP echo PROGRAM_NAME ?>. Once the source files are loaded in the system an icon to the left will show the file type. 
		</p><br/>
		<p> 
		<ul style='margin-left: 50px;'>
			<li><img src='img/light-16.png' style="vertical-align: middle" /> Light frame</li>
			<li><img src='img/bias-16.png' style="vertical-align: middle" /> Bias frame</li>
			<li><img src='img/dark-16.png' style="vertical-align: middle" /> Dark frame</li>
			<li><img src='img/flat-16.png' style="vertical-align: middle" /> Flat frame</li>
		</ul>
		</p></br></br>
		
		<h3>Calibration parameters</h3><br/>
		<p style='text-align:middle;'>
			<img src='sampimg/calib_02.png' width='358' height='423' style='display:inline' title='XParallax viu calibration dialog' />
			<br/><br/>
		<p>
		
		<ul style='margin-left: 50px;'>
			<li>
				<h4>Calibration / Pixel rejecting:</h4>
				As we said, sometimes stars and cosmic rays are present in sky flats. If you want to reject these pixels select one of the available rejecting methods, "sigma level" based in the standard deviation above background or "MinMax", based in the absolute ADU value of the pixel.
				<br/><br/>
			</li>
			<li>
				<h4>Calibration / Output format:</h4> You can select the output fit file format. 32 bit float obtains the better results but output files are bigger. If you prefer, the option "same as input" will preserve the input file format.
				<br/><br/>
			</li>
			<li>
				<h4>Calibration / Save master flat, dark and bias:</h4> While calibration is in progress, intermediate frames are stored in memory. It is not necessary to save intermediate master frames. By checking this options these frames will be saved allowing you to inspect them or even use then in future process.
				<br/><br/>
			</li>
			<li>
				<h4>Calibration / Normalize image:</h4> If you check this option, image counts will be fixed to set the mean of all pixels in the image to this value. By default, pedestal of the source images is preserved. Be careful by checking this option because it will result in a loss of the source pedestal.
				<br/><br/>
			</li>
			<li>
				<h4>Background / Mesh size:</h4> Background is used to reject star pixels in flat field frames and to detect hot or dead pixels. A local background is computed in small portions of the image (a grid). Set the mesh size four or five times bigger than the diameter of bigger star in the image. 64 or 128 pixels is usually a good value in the most of the cases.
				<br/><br/>
			</li>
			<li>
				<h4>Background / Local background estimation:</h4> This option let you to select the method to estimate the local background.
				<br/><br/>
			</li>
			<li>
				<h4>Background / σ upper:</h4> The image histogram is clipped iteratively. This option expresses the level, in standard deviation units above the estimated background level to clip the histogram
				<br/><br/>
			</li>
			<li>
				<h4>Background / σ lower:</h4> Lower level in standard deviation units under the background to clip the histogram when background is being calculated.
				<br/><br/>
			</li>
			<li>
				<h4>Background / σ precission:</h4> This option sets the difference in standard deviation between two background iterations to stop iterating. When σ is changed by less than this value, histogram clipping will stop and the mean of the clipped histogram will be taken as the background level.
				<br/><br/>
			</li>
			<li>
				<h4>Background / Smooth:</h4> After the local background grid is set up, a median filter can be applied to suppress local overestimations due to bright stars or underestimations due to other causes. Set this option to a small odd value (1, 3, 5) to get better results.
				<br/><br/>
			</li>
			<li>
				<h4>Background / Interpolation:</h4> After the local background (mesh) is set up, a unique value per pixel is calculated (background map). This option sets the interpolation method used to compute the background map.
				<br/><br/>
			</li>
		</ul>
		
		
		<?PHP
		/*
		http://www.cyanogen.com/help/maximdl/HID_PROC_SETCALIB.htm
		Calibration requires special "calibration images" taken with the same CCD camera as the image being calibrated (the "light"). These must be full-frame images, acquired at the same binning and temperature (if possible) as the "light". They come in three types: the Bias Frame is used to remove offsets, the Dark Frame to remove variations in dark current, and the Flat-Field Frame to remove pixel-specific differences in sensitivity, as well as variations in illumination across the camera sensor. All three types are usually constructed by combining multiple calibration images into a "master". This reduces the effect of readout and thermal noise compared to using single calibration images. For more information on calibration, please refer to the appropriate sections of the manual.*/
		?>
		
    </div>
</div><!-- Totalbox -->

<?php include('sub_footer.php'); ?>
</div><!-- main_wrapper -->

</body>
</html>