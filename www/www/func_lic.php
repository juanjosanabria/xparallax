<?PHP
include('common.php');
include('class/smtp.php');

db_lock('lic.s3db');
define('END_DATA', "#END_DATA:");

$IDS = get_ids();
if (!isset($_GET['id']) || !array_key_exists($_GET['id'],$IDS))
{
    header('HTTP/1.1 503 Service Temporarily Unavailable');
    header('Status: 503 Service Temporarily Unavailable');
    echo "Invalid USER ID in get request";
    exit(0);
}

foreach ($_GET as $key => $value) $_POST[$key] = $value;
$ok = false;
if (isset($_POST['f']))
{
    if ($_POST['f']=='GET_LICS') $ok = fn_get_lics(); //Devuelve una lista de licencias
    else if ($_POST['f']=='STATS') $ok = fn_stats();
}
if (!$ok)
{
    header('HTTP/1.1 503 Service Temporarily Unavailable');
    header('Status: 503 Service Temporarily Unavailable');
    echo "You ar trying to do something wrong";
}
else
{
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/plain; charset=utf-8');
    echo $ok;
}

function fn_get_lics()
{
    $fil = scandir("lic");
    $cad = "";
    foreach ($fil as $strfil)
    {
        if (strtolower(pathinfo($strfil, PATHINFO_EXTENSION )) != 'lic') continue;
        $path = "lic/$strfil";
        $fdata = new SimpleXMLElement(file_get_contents($path), LIBXML_NOWARNING);
        if (!$fdata || !isset($fdata->Identity)) continue;
        if ((string)$fdata->Identity != $_GET['id']) continue;
        $cad .= "#LICENSE: $strfil\n";     
        $fcontent = file_get_contents($path);
        $cad .=  $fcontent;
    }
    return $cad . "\n" . END_DATA . md5($cad); 
}



/**
* Obtiene todos los IDS que hay en los archivos de disco
*/
function get_ids()
{
    $IDS = array();
    $fil = scandir("lic");
    foreach ($fil as $strfil)
    {
        if (strtolower(pathinfo($strfil, PATHINFO_EXTENSION )) != 'id') continue;
        $path = "lic/$strfil";
        $fdata = new SimpleXMLElement(file_get_contents($path), LIBXML_NOWARNING);
        $IDS[(string)$fdata->ID] = new stdClass();
        foreach ($fdata as $key => $value)
        {
            $IDS[(string)$fdata->ID]->$key = (string)$value;
        }
    } 
    return $IDS;
}

function fn_stats()
{
    //FORMATO: 20180310_T_O20171114_0858
    $vec = explode('/',$_POST['DATA']);
    $cont = 0;
    foreach ($vec as $str)
    {
        $v = explode('_',$str,2);
        if (count($v) != 2) continue;
        if (!is_numeric($v[0])) continue;
        $v[0] = (int)$v[0];
        @db_query('INSERT OR REPLACE INTO IMAGES(FILENAME,DATE) VALUES(%s, %d)', $v[1], $v[0]);
        $cont++;
    }
    return "OK:$cont";
}