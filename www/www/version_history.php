<?php 
    include('common.php'); 
    $files = scandir('version_history');
    $VER = array();
    foreach ($files as $file)
    {
        if ($file{0} == '.' || pathinfo($file,PATHINFO_EXTENSION) != 'htm') continue;
        $obj = new stdClass();
        $obj->VER = pathinfo($file, PATHINFO_FILENAME);
        $data = file_get_contents("version_history/$file");
        $pos1 = strpos($data,"<!--"); $pos2 = strpos($data,"--", $pos1+4);
        $datestr = trim(substr($data, $pos1+4, $pos2-$pos1-4));
        $obj->DATE = strtotime($datestr);
        if (!$obj->DATE) continue;
        array_push($VER, $obj);
    }
    //Ordenamos el array de versiones por fecha
    for ($i=0; $i<count($VER)+1; $i++)
        for ($j=$i+1; $j<count($VER); $j++)
        {
            if ($VER[$i]->DATE < $VER[$j]->DATE)
            {
                $aux = $VER[$i]; 
                $VER[$i] = $VER[$j];
                $VER[$j] = $aux;
            }
        }
    $CURVER = null;
    if (!isset($_GET['v'])) $_GET['v'] = $VER[0]->VER;
    foreach ($VER as $v)
    {
        if ($v->VER == $_GET['v'])
        { 
            $CURVER = $v;
            break;
        }
    }
    if (!$CURVER) 
    {
        header('HTTP/1.1 302 Moved Permanently');
        header('Location: version_history.php');
        exit(0);
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head>
<?php include('sub_header.php'); ?>
<title>XParallax viu - version history</title>
<meta name='keywords' lang='en' content='astrometrica free, astrometry, astronomy, minor bodies, minor planet center' />
<meta name='description' lang='en' content='XParallax VIU is a software tool for high precission astrometric data reduction of CCD images' />
<style>
    .submenu_content ul{ margin-left:50px; }
</style>
</head>


<body>
<div class="main_wrapper">
    <?php 
        include('sub_menu.php'); 
    ?>
    
<div class="totalbox">
    <!-- OPCIONES DE SUBMENU -->
    <div class="submenu_wrapper">
        <ul>
        <?php 
            //Contenido del submenú
            foreach ($VER as $v)
            {
                $dt = date('Y-m-d',$v->DATE);
                if ($v->VER == $_GET['v']) echo "<li class='selected'>{$v->VER}&nbsp;($dt)</li>";
                else echo "<li class='noselected'><a href='version_history.php?v={$v->VER}'>{$v->VER}&nbsp;($dt)</a></li>";
            }
        ?>  
        </ul>
    </div>
    
    <div class="submenu_content">
        <h2>Version history</h3><br/>
	    <!-- Contenido de la página -->        
        <?php
            //Mostramos la versión solicitada
            echo sprintf("<h3>Version: %s, Released: %s</h3><br/>",$_GET['v'],date('Y-m-d',$CURVER->DATE));
            echo file_get_contents("version_history/{$CURVER->VER}.htm");
            
            //Mostramos hasta 4 versiones más
            $idx = 0;
            foreach ($VER as $v)
            {
                if ($v->VER == $_GET['v']) break;
                else $idx++;
            }
            for ($i=$idx+1;$i<min($idx+4, count($VER));  $i++)
            {
                 $v = $VER[$i];
                 echo sprintf("<br/><br/><h3>Version: %s, Released: %s</h3><br/>",$v->VER,date('Y-m-d',$v->DATE));
                 echo file_get_contents("version_history/{$v->VER}.htm");
            }
        ?>
    </div>
</div><!-- Totalbox -->

<?php include('sub_footer.php'); ?>
</div><!-- main_wrapper -->

</body>
</html>