<!-- Pie de página -->
<div id="divider" style="height:3px; background-color:#0055af;"></div>
<!-- Fin del divisor -->

<div id="footer" style="background:white; min-height:80px; padding:20px; font-size:10px; overflow:auto;">

	<div id="license" style="width: 150px; text-align:center; overflow:auto; float:left;">
	<br/>
	<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
    <img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" />
    </a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Dataset" property="dct:title" rel="dct:type">XParallax viu</span> by <br/><a xmlns:cc="http://creativecommons.org/ns#" href="http://www.xparallax.com" property="cc:attributionName" rel="cc:attributionURL">Juan José Sanabria</a>
	</div>


	<div style="max-width: 150px; text-align:center; float: left; margin-left:30px; float:left;">
	<a href="http://www.viu.es/">
	<img src='/img/logo_viu.png' style="border-style: none" alt="Valencian International University" />
	</a>
	</div>
	
	<div id="icontact" style="max-width: 150px; text-align:right; margin-left:30px; float:right;">
			Please, report suggestions and issues to:<br/>
			<span class='at'>info</span>at<span class='at'>xparallax</span>dot<span class='at'>com</span></a>
	        Or contact by filling up this <a href='/contact.php'>contact form</a>
	</div>


</div>