<?php 
//Página de descargas
require_once('common.php'); 

//define('DOWNLOAD_WIN32_EXE','http://downloads.sourceforge.net/project/xparallaxviu/release_[VERSION]/XParallaxVIU_Setup_v[VERSION]_win32.exe?r=&ts=[TS]&use_mirror=optimus');
//define('DOWNLOAD_WIN32_ZIP','http://downloads.sourceforge.net/project/xparallaxviu/release_[VERSION]/XParallaxVIU_v[VERSION]_win32.zip?r=&ts=[TS]&use_mirror=optimus');
//define('DOWNLOAD_WIN64_EXE','http://downloads.sourceforge.net/project/xparallaxviu/release_[VERSION]/XParallaxVIU_Setup_v[VERSION]_win64.exe?r=&ts=[TS]&use_mirror=optimus');
//define('DOWNLOAD_WIN64_ZIP','http://downloads.sourceforge.net/project/xparallaxviu/release_[VERSION]/XParallaxVIU_v[VERSION]_win64.zip?r=&ts=[TS]&use_mirror=optimus');

define('DOWNLOAD_WIN32_EXE','http://www.xparallax.com/exe/[VERSION]/XParallaxVIU_Setup_v[VERSION]_win32.exe');
define('DOWNLOAD_WIN32_ZIP','http://www.xparallax.com/exe/[VERSION]/XParallaxVIU_v[VERSION]_win32.zip');
define('DOWNLOAD_WIN64_EXE','http://www.xparallax.com/exe/[VERSION]/XParallaxVIU_Setup_v[VERSION]_win64.exe');
define('DOWNLOAD_WIN64_ZIP','http://www.xparallax.com/exe/[VERSION]/XParallaxVIU_v[VERSION]_win64.zip');


function flink($link,$vs=CUR_VERSION)
{
	$link = str_replace('[VERSION]',$vs,$link);
	$link = str_replace('[TS]',time(),$link);
	return $link;
}

?><!DOCTYPE html> 
<html>
<head>
<?php include('sub_header.php'); ?>
<title>Download XParallax viu</title>
<meta name='keywords' lang='en' content='<?PHP echo GLOBAL_KEYWORDS; ?>' />
<meta name='description' lang='en' content='<?PHP echo GLOBAL_DESC ?>' />
<script type="text/javascript">

$(document).ready(function(){
	
	if (detect_os() == 'Windows') 
	{	
		$("#choice ul>li#select").hide();
		if (detect_windows_bits() == 64)$("#choice ul>li#win64").show();
		else $("#choice ul>li#win32").show();
	}
	
});

</script>
<style>
	.infobox_body img{  margin-right:15px;}
</style>

</head>


<body>
<div class="main_wrapper">

<?php include('sub_menu.php'); ?>

<div class="totalbox">
    <div class="infobox_container" style="position: relative;"> 
        <div class="infobox_header">
            <span class="infobox_icon"><img src="img/download.png" /></span>
             <?php echo PROGRAM_NAME; ?> <?php echo CUR_VERSION; ?> downloads
        </div>
        <div class="infobox_body" style="padding-left:25px;">
			<div id="choice">
			<br/>
			<h3>Best choice for you</h3>
             <br/>
			 <ul style="margin-left: 20px;">
			 <li id="select">We can't determine the best choice for you, please choose one of the downloads below:</li>
			 
			 <li id="win32" style='display:none' ><a href="<?php echo flink(DOWNLOAD_WIN32_EXE); ?>"> <img src='img/installer_ico.png' style='vertical-align:middle' />Windows 32 bits</a>: Setup file for Windows 32 bits is the version that fits better to your system capabilities. 
			 </li>
			 
			 <li id="win64" style='display:none'><a href="<?php echo flink(DOWNLOAD_WIN64_EXE); ?>"><img src='img/installer_ico.png' style='vertical-align:middle' />Windows 64 bits</a>: Setup file for Windows 64 bits is the version that fits better to your system capabilities. </li>
			 </ul>
			</div>
			  <br/> <br/>
            <h3>All Available downloads</h3>
             <br/>
            <ul style="margin-left: 20px;">
                <li>
					<a href="<?php echo flink(DOWNLOAD_WIN32_EXE); ?>"><img src='img/installer_ico.png' style='vertical-align:middle' />Windows 32 bits setup</a>: Setup file for Windows 32 bits. 
					<br/><br/>
				</li>
				<li>
					<a href="<?php echo flink(DOWNLOAD_WIN32_EXE); ?>"><img src='img/zip_ico.png' style='vertical-align:middle' />Windows 32 bits zip file</a>: Zip file for Windows 32 bits. Just uncompress this file anywhere and start working. 
					<br/><br/>
				</li>
                <li>
					<a href="<?php echo flink(DOWNLOAD_WIN64_EXE); ?>"><img src='img/installer_ico.png' style='vertical-align:middle' />Windows 64 bits setup</a>: Setup file for Windows 64 bits. 
					<br/><br/>
				</li>
               <li>
					<a href="<?php echo flink(DOWNLOAD_WIN64_EXE); ?>"><img src='img/zip_ico.png' style='vertical-align:middle' />Windows 64 bits zip file</a>: Zip file for Windows 64 bits. Just uncompress this file anywhere and start working. 
					<br/><br/>
			   </li>
                <li>
                    <p style="text-align:left;">
					<b>Mac OSx</b>: If  you are interested in a Mac version, please, let me know by using the <a href="contact.php">contact form<a>
                    <img src="img/wine-icon.png" alt="" style="display:inline; float: left;"><br/>You can use <a href="http://wiki.winehq.org/MacOSX">WineHQ for Mac</a> and run <?php echo PROGRAM_NAME; ?> <?php echo CUR_VERSION; ?> in your Mac.
                    </p>
				</li>
                    <br/><br/>
					<li><b>Linux</b>: If  you are interested in a Linux version, please, let me know by using the <a href="contact.php">contact form<a><br/>You can use <a href="https://www.winehq.org/download">WineHQ for Linux</a> and run <?php echo PROGRAM_NAME; ?> <?php echo CUR_VERSION; ?> in your Linux system.<br/><br/>
				</li>
            </ul>         
            <br/>
            <h3>Last release <b>1.2.3</b></h3>
            <br/>
            <ul style="margin-left: 20px;">
               <li>
                    <a href="<?php echo flink(DOWNLOAD_WIN64_EXE,"1.2.3"); ?>"><img src='img/installer_ico.png' style='vertical-align:middle' />1.2.3 Windows 64 bits installer</a>: Installer  for Windows 64 bits. 
                    <br/><br/>
               </li> 
               <li>
                    <a href="<?php echo flink(DOWNLOAD_WIN32_EXE,"1.2.3"); ?>"><img src='img/installer_ico.png' style='vertical-align:middle' />1.2.3 Windows 32 bits installer</a>: Installer for Windows 32 bits. 
                    <br/><br/>
               </li> 
            </ul>                
            <br/><br/><br/>            
        </div>
    </div>

<br/><br/><br/>
<h3>System requirements</h3>
<br/>
<?php echo PROGRAM_NAME; ?> takes advantage of the modern hardware capabilities like multi-threading, and multi-core CPUs. It is particularly useful
in batch processing where dozens or even hundreds of images are reduced or calibrated. There are no special hardware requirements and the program
will work in any of the modern computers. At the moment, only the Windows version is available, but we expect to have the Mac and Linux releases
as soon as possible.
<br/><br/>
		
<h3>Current version release notes ( <?PHP echo CUR_VERSION; ?> )</h3>
<br/>
<div style="margin-left: 50px;">
<?php
    $fname = CUR_VERSION . ".htm";
    echo file_get_contents("version_history/$fname");
?>
</div>
<br/>
<a href='version_history.php'>Version history</a>
           
<br/><br/><br/><br/>
</div>
<?php include('sub_footer.php'); ?>
</div>

</body>
</html>