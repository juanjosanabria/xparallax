<?PHP
include('common.php');
include('class/smtp.php');

foreach ($_GET as $key => $value) $_POST[$key] = $value;
$ok = false;
if (isset($_POST['f']))
{
    if ($_POST['f']=='CHECK_UPDATE') $ok = fn_check_update();
	else if ($_POST['f']=='CONTACT') $ok = fn_contact();
	else if ($_POST['f']=='IP') $ok = fn_ip();
    else if ($_POST['f']=='GET_WEBSITE') $ok = fn_get_website();
}
if (!$ok)
{
    header('HTTP/1.1 503 Service Temporarily Unavailable');
    header('Status: 503 Service Temporarily Unavailable');
    echo "You ar trying to don something wrong";
}
else
{
    header('HTTP/1.1 200 OK');
    echo $ok;
}


/*
Información de ejecución de instancia. Datos recibidos:
uid=User id
os=Descripción del sistema operativo
bits=Bits del sistema operativo
version=Número de version del programa
*/
function fn_check_update()
{
    if (!isset($_POST['uid']) || !isset($_POST['os']) || !isset($_POST['bits']) || !isset($_POST['version']) || !isset($_POST['rc'])) return false;
    $ret = db_select_value('SELECT UID FROM USERS WHERE UID=%s',$_POST['uid']);
    $ostype = stristr($_POST['os'], "win") ? "W" : ( stristr($_POST['os'], "mac") ? "M" : "L" );
	
	//Si hemos recibido un DEBUG_GUID, significa que es una máquina de desarrollo (también si la versión es mayor que la de release)
	//Hacemis un borrado de todos los insertados desde la misma ip (limpieza)
	if ($_POST['uid'] == DEBUG_UID || compare_version($_POST['version'], CUR_VERSION) > 0 || isset($_POST['des_machine']))
		db_query('DELETE FROM USERS WHERE LAST_IP=%s AND UID !=%s', $_SERVER['REMOTE_ADDR'], $_POST['uid']);
	
    if (!$ret)
    {
        $ret = db_query('INSERT INTO USERS(UID,OS_TYPE,OS_NAME,OS_BITS, XP_VERSION,NUM_CONNECTIONS,'
                        .'FIRST_CONNECTION,LAST_CONNECTION,LAST_IP,RUN_COUNT,DPI_RES) '
                        .'VALUES(%s,%s,%s,%d,%s,0,%s,%s,%s,%d,%f)',
        $_POST['uid'],$ostype,
        $_POST['os'],$_POST['bits'],
        $_POST['version'],
        date('Y-m-d H:i:s'),
        date('Y-m-d H:i:s'),
        $_SERVER['REMOTE_ADDR'],
        $_POST['rc'],
        isset($_POST['dpi'])?$_POST['dpi']:null
        );
        if (!$ret) return false;
    }
	
	//Cadena de actualización de contadores
	$ct = '';
	if (isset($_POST['ct_ast'])) $ct .= sprintf(', CT_AST=%d',$_POST['ct_ast']);
	if (isset($_POST['ct_fast'])) $ct .= sprintf(', CT_FAST=%d',$_POST['ct_fast']);
	if (isset($_POST['ct_cal'])) $ct .= sprintf(', CT_CAL=%d',$_POST['ct_cal']);
	if (isset($_POST['ct_fcal'])) $ct .= sprintf(', CT_FCAL=%d',$_POST['ct_fcal']);
	if (isset($_POST['ct_bhe'])) $ct .= sprintf(', CT_BHE=%d',$_POST['ct_bhe']);
	if (isset($_POST['ct_fbhe'])) $ct .= sprintf(', CT_FBHE=%d',$_POST['ct_fbhe']);
	if (isset($_POST['ct_ica'])) $ct .= sprintf(', CT_ICA=%d',$_POST['ct_ica']);
	if (isset($_POST['ct_fica'])) $ct .= sprintf(', CT_FICA=%d',$_POST['ct_fica']);
    if (isset($_POST['ct_mpc'])) $ct .= sprintf(', CT_MPC=%d',$_POST['ct_mpc']);
	
    //Actualizar el número de conexiones y la hora de la última conexión y los contadores
    $ret = db_query('UPDATE USERS SET NUM_CONNECTIONS=NUM_CONNECTIONS+1, XP_VERSION=%s, '
                     .'OS_TYPE=%s, OS_NAME=%s, OS_BITS=%d, LAST_CONNECTION = %s, RUN_COUNT = %d, '
                     .'LAST_IP=%s, DPI_RES=%f '
                     .'%l WHERE UID=%s', 
                     $_POST['version'],
                     $ostype,
                     $_POST['os'], 
                     $_POST['bits'],                                                
                     date('Y-m-d H:i:s'),                                           //LAST_CONNECTION
                     $_POST['rc'],                                                  //RUN_COUNT
                     $_SERVER['REMOTE_ADDR'],                                       //LAST_IP
                     isset($_POST['dpi'])?$_POST['dpi']:null,                       //DPI_RES
					 
					 //--------------------------------- ACTUALIZACIÓN DE CONTADORES AL FINAL ------------
					 $ct,
					 //--------------------------------- SELECTOR WHERE ----------------------------------
                     $_POST['uid']);
					 //-----------------------------------------------------------------------------------
    return file_get_contents("versions.json");
}


function fn_contact()
{
    sleep(2);
	$obj = new stdClass();
	$obj->error = false;

	//Configurar el servidor local smtp
	smtp_configure('localhost',ADMIN_MAIL);
	
	//Enviar el email al administrador
	$msg = smtp_start_message();
	smtp_add_subject($msg,'Contacto usuarios');
	smtp_add_from($msg, ADMIN_NAME, ADMIN_MAIL);
	smtp_add_to($msg,'Juan José Sanabria','juanjosanabria@yahoo.es');
	$body = file_get_contents('mail/contact_admin.htm');
    $body = str_replace('%NAME%',$_POST['name'],$body);
	$body = str_replace('%EMAIL%',$_POST['email'],$body);
    $body = str_replace('%TEXT%',$_POST['text'],$body);
    $body = str_replace('%DATE%',date('Y-m-d H:i:s'),$body);
	$body = str_replace('%USER_AGENT%',$_SERVER['HTTP_USER_AGENT'],$body);
	$body = str_replace('%IP%',$_SERVER['REMOTE_ADDR'],$body);
	smtp_add_body($msg,$body,'text/html');
	send_eml('juanjosanabria@yahoo.es',$msg);
    
    //Enviar mensaje
    $msg = smtp_start_message();
    smtp_add_subject($msg,'Contact confirmation');
    smtp_add_from($msg, ADMIN_NAME, ADMIN_MAIL);
    smtp_add_to($msg,$_POST['name'],$_POST['email']);
    $body = file_get_contents('mail/template.htm');
    $body = str_replace('%TEXT%','We confirm the receipt of your mail and will revert with our respond as soon as possible',$body);
    smtp_add_body($msg,$body,'text/html');
    send_eml($_POST['email'],$msg);
    
	
	return json_encode($obj);;
}

function compare_version($v1, $v2)
{
	$v1 = explode('.',$v1);
	$v2 = explode('.',$v2);
	if (count($v1) != 3 || count($v2) != 3) return 0;
	for ($i=0;$i<3;$i++)
		if ($v1[$i] > $v2[$i]) return 1;
		else if ($v1[$i] < $v2[$i]) return -1;
	return 0;
}

function fn_ip()
{
	return  "Hi there, your public IP is: \n" . $_SERVER['REMOTE_ADDR'];
}


function fn_get_website()
{
   header('Access-Control-Allow-Origin: *');
   return file_get_contents($_GET['url']);
}

