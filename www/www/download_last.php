<?PHP
//Página de descargas.
//Se ocupa de gestionar la descarga para antiguas versiones, redirigiendo siempre a la última
//Para ello es necesario meter en .htaccess una línea que gestione la versión.
//Algo así: RewriteRule ^.*exe/1\.0\.0/.*$       download_last.php
//También la expresión regular se puede cambiar para que abarque todo un rango
//Algo así: RewriteRule ^.*exe/1\.0\.\d/.*$       download_last.php
//Lo que abarcaría de la 1.0.0 a la 1.0.9
require_once('common.php'); 

define('REGEX_FILE','/.*_v(?<VERSION>\d\.\d\.\d)_.*/i');
$RU = $_SERVER['REQUEST_URI'];
if (preg_match(REGEX_FILE,$RU,$matches))
{
	$filename = str_replace($matches['VERSION'],CUR_VERSION, basename($RU));
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: ../".CUR_VERSION."/$filename");
	exit(0);
}

