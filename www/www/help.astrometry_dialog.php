<?php include('common.php'); ?>
<!DOCTYPE html> 
<html>
<head>
<?php include('sub_header.php'); ?>
<title>Astrometric reduction with XParallax viu</title>
<meta name='keywords' lang='en' content='<?PHP echo GLOBAL_KEYWORDS; ?>' />
<meta name='description' lang='en' content='<?PHP echo GLOBAL_DESC ?>' />
</head>


<body>
<div class="main_wrapper">
    <?php 
		include('sub_menu.php'); 
	?>

<div class="totalbox">
<?PHP include('sub_submenu.php');  ?>
    <div class="submenu_content">		
	    <h3>Astrometric reduction</h3><br/>
		<p>
			Astrometric reduction may be computed for one or several files. When you are computing this reduction for a big amount of files, we recommend you to try out the reduction for one of them, in order to set reduction parameters to the best values. Check out the star matching visually and, if you need repeat the process every time as needed to fix the better values.
			Once the parameters are set up, probably you'll never need to move them (except image center) if you are using the same telescope and CCD.
		</p><br/><br/>

		<h3>The astrometry dialog</h3><br/>
		<p>
		Select the menu option "Image processing > Astrometric reduction" to open the astrometric reduction dialog. Three tabs are present in this dialog where you can select the input files and output directory or configure the astrometry options.
		</p>
		<br/></br>
		
		<h3>Input files selection</h3><br/>
		<p>
		The first step is to select input files in the tab "Source images".
		</p>
		<br/>
		
		<p style='text-align:middle;'>
			<img src='sampimg/red_01.png' width='358' height='423' style='display:inline' title='XParallax viu astrometry dialog' />
			<br/><br/>
		<p>
		<p>
		There is no restriction in the number of files you can select. When a previous astrometric reduction is detected an small icon to the left of the image name <img src='img/rule.png' width='16' height='16' style='vertical-align:middle;' /> will appear. If you continue to process this file, previous reduction information will be overwritten. Astrometric reduction parameters are globally applied to the entire input set and all files may share the same center and scale. In the future, different center and scale files will be allowed in the input set by detecting some entries in the fit header.
		</p><br/>
		<p> 

		
		<h3>Astrometry options</h3><br/>
		<p style='text-align:middle;'>
			<img src='sampimg/red_02.png' width='358' height='423' style='display:inline' title='XParallax viu calibration dialog' />
			<br/><br/>
		<p>
		
		<ul style='margin-left: 50px;'>
			<li>
				<h4>Telescope and CCD information / Focal lenght (mm):</h4>
				The telescope focal lenght in milimeters. 	
				<br/><br/>
			</li>
			<li>
				<h4>Telescope and CCD information / Pixel size (μm):</h4>
				The CCD pixel size in microns. 
				<br/><br/>
			</li>
			<li>
				<h4>Telescope and CCD information / Reducer:</h4>
				If you are using a focal reducer or barlow lens, check this option and fill in the spin box with the reduction factor.
				<br/><br/>			
			</li>
			<li>
				<h4>Telescope and CCD information / Date for proper motion correction:</h4>
				You can get an extra precission using the proper motion information provided by the catalogs. This is made by correcting catalog star positions to the actual one when the image was taken.
				<br/><br/>			
			</li>
			<li>
				<h4>Telescope and CCD information / Image center RA and DEC:</h4>
				Selects the aproximate image center.
				<br/><br/>			
			</li>
			<li>
				<h4>Astrometric reduction options / Catalog and magnitude:</h4>
				Two catalogs are available to compute the reduction. Please, use UCAC-4 when magnitude is below 15 and PPMXL otherwise to get better results.
				Move the magnitude limit according to the image scale, aperture of your system and exposure time to cover every star in the image. In very crowed fields (Milky Way images) with huge field of view, set this option to cover not more than 50000 stars.
				<br/><br/>			
			</li>
			<li>
				<h4>Astrometric reduction options / Server:</h4>
				This comobo box allows you to select the Vizier server to download catalog stars. Sometimes one server is down or is working poorly so you can select another to work with.
				<br/><br/>			
			</li>
		
			<li>
				<h4>Astrometric reduction options / Align stars:</h4>
				Number of stars to try alignement. If a succesful alginement is found this number doesn't affect to astrometric results but computing time may be increased if you set this number above 20.
				<br/><br/>			
			</li>		
			
			<li>
				<h4>Astrometric reduction options / Match dist:</h4>
				Distance in pixels between a catalog star and an image star to be considered the same. Do not set this value above than the radius of the smallest star in the image.
				<br/><br/>			
			</li>	
			
			<li>
				<h4>Astrometric reduction options / Fov ext:</h4>
				Field of view extension. When catalog stars are downloaded, the program try to download a little more big surface in order to cover the entire image. Set up this value between 200 and 300 to ensure that catalog information and image overlaps totally.
				<br/><br/>			
			</li>	
			
			<li>
				<h4>Astrometric reduction options / Scale filter, magnitude filter:</h4>
				Do not deactivate this options. They allow the matching algorithm to works faster by comparing catalog and image fluxes and distances and rejecting wrong align pairs.
				<br/><br/>			
			</li>	
			
		</ul>
		
		<h3>Sources detection</h3><br/>
		<p style='text-align:middle;'>
			This tab will help you to configure how stars are detected and centers computed.
			<br/><br/>
			<img src='sampimg/red_03.png' width='358' height='423' style='display:inline' title='XParallax viu calibration dialog' />
			<br/><br/>
		<p>
		<ul style='margin-left: 50px;'>
			<li>
				<h4>Background / Mesh size:</h4> Background is used to reject star pixels in flat field frames and to detect hot or dead pixels. A local background is computed in small portions of the image (a grid). Set the mesh size four or five times bigger than the diameter of bigger star in the image. 64 or 128 pixels is usually a good value in the most of the cases.
				<br/><br/>
			</li>
			<li>
				<h4>Background / Local background estimation:</h4> This option let you to select the method to estimate the local background.
				<br/><br/>
			</li>
			<li>
				<h4>Background / σ upper:</h4> The image histogram is clipped iteratively. This option expresses the level, in standard deviation units above the estimated background level to clip the histogram
				<br/><br/>
			</li>
			<li>
				<h4>Background / σ lower:</h4> Lower level in standard deviation units under the background to clip the histogram when background is being calculated.
				<br/><br/>
			</li>
			<li>
				<h4>Background / σ precission:</h4> This option sets the difference in standard deviation between two background iterations to stop iterating. When σ is changed by less than this value, histogram clipping will stop and the mean of the clipped histogram will be taken as the background level.
				<br/><br/>
			</li>
			<li>
				<h4>Background / Smooth:</h4> After the local background grid is set up, a median filter can be applied to suppress local overestimations due to bright stars or underestimations due to other causes. Set this option to a small odd value (1, 3, 5) to get better results.
				<br/><br/>
			</li>
			<li>
				<h4>Background / Interpolation:</h4> After the local background (mesh) is set up, a unique value per pixel is calculated (background map). This option sets the interpolation method used to compute the background map.
				<br/><br/>
			</li>
		
			<li>
				<h4>Star detection / Thres σ:</h4> 
				Number ni standard deviation units avobe the background level to consider a pixel belonging to a star.
				<br/><br/>
			</li>
			<li>
				<h4>Star detection / Min star radius:</h4> 
				Minimus radius to consider a set of pixel detected as a star.
				<br/><br/>
			</li>	
			<li>
				<h4>Star detection / Max star eccentricity:</h4> 
				Every time a star is detected an ellipse around star pixels is computed. Sometimes so eccentric ellipse indicate a bad deblended pair or another near source contaminating star flux. You can reject some stars by setting up this value. 
				<br/><br/>
			</li>
			
			<li>
				<h4>Star detection / Aperture center:</h4> 
				Select this option to perform a PSF fitting around the star area instead of baricenter of connected pixels. This center works better in non-crowed fields.
				<br/><br/>
			</li>
			
			<li>
				<h4>Star detection / Min star area:</h4> 
				Minimum star area in pixels to consider a detection as a good star.
				<br/><br/>
			</li>
			
			<li>
				<h4>Star detection / Max star area:</h4> 
				Maximum star area in pixels to consider a detection as a good star.
				<br/><br/>
			</li>
			
			<li>
				<h4>Star detection / Deblending:</h4> 
				If this option is set up, every time a star is detection is completed, the program will try to split the source into eventual overlapping components. Detection threshold will be increased iteratively in small steps to isolate the overlapping stars. Number of steps and starting and ending levels are user-selectable.
				<br/><br/>
			</li>	
			
			<li>
				<h4>Star detection / Deblending levels:</h4> 
				Number of steps to divide the detection interval.
				<br/><br/>
			</li>	
			
			<li>
				<h4>Star detection / Deblending contrast:</h4> 
				Max difference in bright between two deblended sources to consider the dimmer as a valid star.
				<br/><br/>
			</li>
			
			<li>
				<h4>Star detection / Deblending σ lower:</h4> 
				Value, in standard deviation units above the estimated background level to start the deblending process. It must be bigger than the detection σ level.
				<br/><br/>
			</li>
			
			<li>
				<h4>Star detection / Deblending σ upper:</h4> 
				Value, in standard deviation units above the estimated background level to stop the deblending process. It must be bigger than the detection σ level and bigger than the σ lower level.
				<br/><br/>
			</li>
			
		</ul>
		
		
    </div>
</div><!-- Totalbox -->

<?php include('sub_footer.php'); ?>
</div><!-- main_wrapper -->

</body>
</html>