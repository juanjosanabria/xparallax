<?php include('common.php'); ?>
<!DOCTYPE html> 
<html>
<head>
<?php include('sub_header.php'); ?>
<title>XParallax VIU, a free software for automated astrometry</title>
<meta name='keywords' lang='en' content='astrometrica free, astrometry, astronomy, minor bodies, minor planet center' />
<meta name='description' lang='en' content='XParallax VIU is a software tool for high precission astrometric data reduction of CCD images' />
</head>


<body>
<div class="main_wrapper">

<?php include('sub_menu.php'); ?>
 
<div class="totalbox">

<!--
This software been tested hard, but there is no software completely free of bugs so we can't provide a warranty of any kind. If you detect any of these bugs we will be glad to fix it as soon as possible. Please, report suggestions and issues to <a href="mailto://info@xparallax.com">info@xparallax.com</a>.
     -->
<h3>Contact form</h3><br/>
<div style='margin:auto; text-align:center;'>
    <p>
    Contact the author by filling in this form or if you prefer, you can send an email to <a href='mailto://info@xparallax.com'>info@xparallax.com</a> <br/><br/>
    </p>
    <p>
    <p id='success' style="color: #008000; font-weight: bold; display: none;">Your message was successfully sent. Thank you for the feedback.</p>
	<p id='error' style="color: red; font-weight: bold; display: none;">Error submiting form.</p>
    <form style='display:inline-flex;'>
		<table cellspacing="15">
			<tr id='tr_name'>
				<td valign="top"><label for='name'>Name:</label></td>
				<td><input type='text' id='name' name='name' style='width:320px;' placeholder='Type your name here' /></td>
                <td style="color:red;" valign="top"></td>
			</tr>
			<tr id='tr_email'>
				<td valign="top"><label for='email'>Email:</label></td>
				<td><input type='text' id='email' name='email' style='width:320px;' placeholder='Type your email here' /></td>
                <td style="color:red;" valign="top"></td>
			</tr>
			<tr id='tr_text'>
				<td valign="top"><label for='text'>Message:&nbsp;&nbsp;</label></td>
				<td><textarea name='text' id='text' style='width:320px; height:200px' ></textarea></td>
                <td style="color:red;" valign="top"></td>
			</tr>
            <tr>
                <td colspan='2'>        
                <div style='margin: auto; text-align:right'>
                    <br/>
                    <span class="button" onclick='do_contact();'>Submit form</span>
                </div>
                </td>
            </tr>
		</table> 
	</form>                                
    </p>
</div>

<!--
<br/><br/>
<h3>Abouth the author</h3><br/>
Juan José Sanabria Cumbreño, natural from Spain, computer engineer an masters degree in astronomy an astrophysics. Currently working as software developer in an IT consulting companny.
    -->

<br/> <br/> <br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/>
 <br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/>
 <br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/><br/> <br/>
 
</div>
	
<?php include('sub_footer.php'); ?>
</div><!-- mainn wrapper -->
<script type="text/javascript">

function do_contact()
{
    //Quitamos posibles mesnajes que hubiera acumulados
    $("tr#tr_name td:last-child").text('');
    $("tr#tr_email td:last-child").text('');
    $("tr#tr_text td:last-child").text('');
    $("#error").hide();
    
    var dt = new Object();
    dt.f = 'CONTACT';
    dt.name = $.trim($("#name").val());
    dt.email = $.trim($("#email").val());
    dt.text = $.trim($("#text").val());

    var ok = true;
    if (!dt.email.match(/(?:[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/))
    {
        $("tr#tr_email  td:last-child").text('Invalid email');
        ok = false;
    }
    if (dt.name.length < 3)
    {
        $("tr#tr_name  td:last-child").text('Invalid name');
        ok = false;
    }
    if (dt.text.length < 3)
    {
        $("tr#tr_text  td:last-child").text('Invalid text');
        ok = false;
    }
    if (!ok) return;
    $("form textarea, form input").attr('disabled','disabled');
    
    $.ajax({
         url : 'func_ws.php',
         dataType : 'JSON',
         type : 'POST',
         data: dt,
         success: function(response){
              if (!response.error)$("#success").slideDown();
              else {
                  $("#error").text(response.msg).show();
                  $("*[disabled='disabled']").removeAttr('disabled');
              }
         },
         error: function(response){
             $("#error").show(); 
         }
    });
}
</script>

</body>
</html>