<?php 
//P�gina del �ndice de la ayuda
include('common.php'); 
?>
<!DOCTYPE html> 
<html>
<head>
<?php include('sub_header.php'); ?>
<title>XParallax viu help index</title>
<meta name='keywords' lang='en' content='<?PHP echo GLOBAL_KEYWORDS; ?>' />
<meta name='description' lang='en' content='<?PHP echo GLOBAL_DESC ?>' />
</head>


<body>
<div class="main_wrapper">
    <?php 
		include('sub_menu.php'); 
	?>

<div class="totalbox">
<?PHP include('sub_submenu.php');  ?>
    <div class='submenu_content'>		
	    <h3>What's astrometry?</h3><br/>
		<p>Astrometry is the branch of astronomy that involves precise measurements of the positions and movements of stars and other celestial bodies.</p><br/>
		<p><b>Small field CCD astrometry</b> analyzes images of a tiny field of view, computing the precise position for each pixel in the image and indeed, the position for every object appearing in it. <?PHP echo PROGRAM_NAME; ?> computes the mathematical transformation needed to get the right ascention and declination for each pixel in the image (and vice-versa). This is made by using catalog reference stars downloaded from CDS Vizier service. You can use this transformation in further image analysis.</p>
		<br/><br/>
		
		<h3>What applications are suitable for this software?</h3><br/>
		<p>It is useful in observatories where you want to provide full astrometricaly calibrated images. So the astronomers, just can start working with this images without worring about computing the reduction.</p>
		<br/>
		<p>For amateur astronomers who wants to perform astrometic reduction of their images and/or measure the position of any kind of object detected. To perform, for example, <a href='http://www.minorplanetcenter.net/iau/info/Astrometry.html' target='_blank'>minor body astrometry</a> and generate Minor Planet Center reports.</p>
		<br/>
		<p>Sometimes, astrometric reduction of CCD images is a first steep to start doing science with them. Many non-related astrometry works can get advantage of this software.</p>
		
		<br/><br/>
		
		<h3>How do I start?</h3><br/>
		<p>There are no restrictions on using it. Just <a href='download.php'>download</a> it and start working. If you are used to other astronomy software and apps you won't have any problems. Otherwise, check out these <a href='help.astrometry_dialog.php'>tutorials </a> and other help resources to become familiar with the interface options.<p/>
		
		<h3></h3><br/>
		<p></p>
		<br/><br/>
		
    </div>
</div><!-- Totalbox -->

<?php include('sub_footer.php'); ?>
</div><!-- main_wrapper -->

</body>
</html>