<?php include('common.php'); ?>
<!DOCTYPE html> 
<html>
<head>
<?php include('sub_header.php'); ?>
<title>XParallax VIU, a free software for precise astrometry</title>
<meta name='keywords' lang='en' content='astrometrica free, astrometry, astronomy, minor bodies, minor planet center' />
<meta name='description' lang='en' content='XParallax VIU is a software tool for high precission astrometric data reduction of CCD images' />

<style type="text/css">

</style>

</head>

<style type="text/css">



</style>

<body>
<div class="main_wrapper">

<?php include('sub_menu.php'); ?>

    <div class="infobox_container">
        
        <div class="infobox_header">
            <span class="infobox_icon"></span>
            About it
        </div>
        <div class="infobox_body">
                 <?php echo PROGRAM_NAME; ?> is a free software tool for high precission astrometric data reduction of CCD images.
                 If you have astronomical imaging of the sky with celestial coordinates you do not know—or do not trust—then Astrometry.net is for you. Input an image and we'll give you back astrometric calibration meta-data, plus lists of known objects falling inside the field of view.

We have built this astrometric calibration service to create correct, standards-compliant astrometric meta-data for every useful astronomical image ever taken, past and future, in any state of archival disarray. We hope this will help organize, annotate and make searchable all the world's astronomical information.
        </div>
    </div>

<?php include('sub_footer.php'); ?>
</div>
</body>
</html>