<?php
function pretty_date($dt)
{
    if (!ctype_digit($dt)) $dt = strtotime($dt);
    return date('D', $dt) .
        ", " . date('M' ,$dt) . " " . date('d',$dt) . ", "
        . date('Y',$dt);
}

function is_boot()
{
	if (!isset($_SERVER['HTTP_USER_AGENT'])) return false;
	return strpos($_SERVER['HTTP_USER_AGENT'],'Googlebot')
	|| strpos($_SERVER['HTTP_USER_AGENT'],'crawl')
	|| strpos($_SERVER['HTTP_USER_AGENT'],'spider');
}
