<script type='text/javascript'>
var AN_SHOWN = false;
function analytics(){
<?php  include_once("analyticstracking.php");  ?>
}

$(document).ready(function(){
	$(document).click(function(){
		if (AN_SHOWN) return;
		$("#cookies_msg").slideUp();
		analytics();
		AN_SHOWN = true;
	});
});

</script>
<div id="cookies_msg" style="padding:10px; background:#F2F5A9; font-size:10px; text-align:center">
We use cookies to track your use of this website. Cookies are anonymous, we can't identify you with them. By interacting with this website we assume you allow us to use cookies.
</div>