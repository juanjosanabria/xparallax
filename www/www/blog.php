<?php 
//Redirección primordial
if (strpos($_SERVER['REQUEST_URI'],'blog.php') && $_SERVER['SERVER_ADDR'] != '127.0.0.1' && $_SERVER['SERVER_ADDR'] != '0:0:0:0:0:0:0:1')
{
    header("HTTP/1.0 302 Moved temporary");
    header('Location: blog/last');
    exit(0);
}

//Establecer el archivo para pruebas
if ($_SERVER['SERVER_ADDR'] == '127.0.0.1')
    $_GET['art'] = 'star_occultations_by_asteroids_02';

//Página del índice de blogs
include('common.php');
include('class/blog_entry.php');
include('utils.php');
define('MAX_IMG_SIZE', 500);
define('DEF_MAX_IMG_SIZE', 640);

$cur_date = date('Y-m-d');
$entries = BlogEntry::scanForEntries('blog');

//Si no está seteada la entrada se coge la última de todas
if (!isset($_GET['art']) || !strlen($_GET['art']) || $_GET['art'] == 'last')
{
    while (count($entries) && $entries[0]->DATE > $cur_date) array_shift($entries);
    $art =  $entries[0];
    if (isset($_GET['art']) && $_GET['art'] == 'last')
    {
        header("HTTP/1.0 302 Moved temporary");
        header('Location: '.$art->FRIENDLY);
        exit(0);
    }
}
else 
{
    $_GET['art'] = str_replace('_','-',$_GET['art']);
    foreach ($entries as $art) if ($art->FRIENDLY == $_GET['art']) break;
    if ($art->FRIENDLY != $_GET['art'])
    {
        header("HTTP/1.0 401 Not found");
        echo "<h1>Error 404</h1><h2>Page not found</h2>";
        exit(0);
    }
}

$filename = "blog/" . $art->FILENAME;
$be = new BlogEntry($filename, $_SERVER['SERVER_ADDR'] == '127.0.0.1'? 'blog.php?art=%s&img=%s' : '%s/%s',
                    MAX_IMG_SIZE, DEF_MAX_IMG_SIZE); 

//Solicitud de imágenes
if (isset($_GET['img']))
{
    $se = $be->getImage($_GET['img']);
    header_remove('Pragma');
    headeR('Cache-Control:max-age=604800');
    
    header('Date: '.gmdate('D, d M Y H:i:s', time()).' GMT');
    header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($filename)).' GMT');
    header('Expires: '.gmdate('D, d M Y H:i:s', time() + 3600*24*365*5).' GMT');
    foreach ($se->HEADERS as $key => $value) 
        if ($key != 'content-location' 
        && $key != 'content-transfer-encoding'
        && $key != 'transfer-encoding') header("$key: $value"); 
    
    echo base64_decode($se->DATA);
    exit(0);
}

//Función dictionary replace
function dr($otext)
{
    static $DICTIONARY = null;
    if (!$DICTIONARY)
    {
        $DICTIONARY = array();
        if (file_exists('blog/_dictionary.txt'))
        {
            $entries = file('blog/_dictionary.txt',  FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
            foreach ($entries as $entry)
            {
                $entry  = trim($entry);
                if (!count($entry)  || $entry{0} == ';') continue;
                $idx = strpos($entry,'=');
                if ($idx <= 0) continue;
                $key = substr($entry,0,$idx);
                $value = substr($entry, $idx+1);
                $DICTIONARY[$key] = $value;
            }
        }
    }
    return strtr($otext, $DICTIONARY);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head>
<?php include('sub_header.php'); ?>
<link rel="stylesheet" type="text/css" href="/jscss/blog.css" />
<!-- Add fancyBox -->
<link rel="stylesheet" href="/jscss/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/jscss/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<title><?PHP echo $be->getTitle(); ?></title>
<meta name='keywords' lang='en' content='<?PHP echo $be->getKeyWords(); ?>' />
<meta name='description' lang='en' content='<?PHP echo $be->getDescription(); ?>' />
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox({
			helpers : {
				title: {
					type: 'inside'
				}
			}
		});
    });
</script>
</head>


<body>
<div class="main_wrapper">
    <?php 
        include('sub_menu.php'); 
    ?>

<div class="totalbox">
<?PHP //include('sub_submenu.php');  ?>
    <div class="submenu_wrapper">
    <ul id="entries">
    <?PHP 
        for ($i=0,$cont=count($entries);$i<$cont&&$i<10;$i++)
        {
            if ($entries[$i]->DATE > $cur_date) continue;
            if ($entries[$i]->FRIENDLY != $be->getFriendly())
            echo sprintf('<li><a href="%s">%s</a><br/></li>',$entries[$i]->FRIENDLY, dr($entries[$i]->TITLE));
            else
            echo sprintf('<li class="selected">%s</li>',$entries[$i]->TITLE);
        }
    ?>
    </ul>
    <!-- Espacio final -->
    &nbsp;
    </div>
    <!-- Contenido del blog -->
    <div id='be' class='submenu_content'>
	  <div class='datepub'><?PHP echo pretty_date($be->getPublishDate()); ?></div>
	  <br/><br/>
      <?PHP echo dr($be->getText()); ?>
    </div>
</div><!-- Totalbox -->

<?php include('sub_footer.php'); ?>
</div><!-- main_wrapper -->

</body>
</html>