<?php
//Establecer las cabeceras de no caché
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

if ($_SERVER['HTTP_HOST'] != 'www.xparallax.com' && strpos($_SERVER['HTTP_HOST'],'localhost') !== 0) 
    exit(header('Location: http://www.xparallax.com'));
//Opciones de configuración y variables globales
require_once('config.php');
require_once('db_sqlite.php');


define('CUR_VERSION','1.2.4');
define('PROGRAM_NAME','<b>XParallax <span style="color:#D26900">v</span>iu</b>');
define('GLOBAL_KEYWORDS','astrometrica free, astrometry, astronomy, minor bodies, minor planet center');
define('GLOBAL_DESC','XParallax VIU is a software tool for automated astrometric data reduction of CCD images');
define('ADMIN_MAIL','info@xparallax.com');
define('ADMIN_NAME','Juan José Sanabria (XParallax)');
define('MAIL_REDIRECTION','juanjosanabria@yahoo.es');
define('ADMIN_PASS','aquilae');
define('DEBUG_UID','{c4909475-9314-4196-9a9e-886dbf69c7f2}');


if (strpos($_SERVER['REMOTE_ADDR'],'161.72') === 0 || strpos($_SERVER['REMOTE_ADDR'],'188.76.xxxxxxx') === 0)
    include('index_pirata.php');
     
