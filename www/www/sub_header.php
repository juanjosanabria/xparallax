<!-- Cabecera común -->
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta name='author' content='Juan José Sanabria' />
<link rel='shortcut icon' href='/img/favicon.png' />
<script type='text/javascript' src='/jscss/jquery-2.0.3.min.js'></script>
<script type='text/javascript' src='/jscss/function.js'></script>
<script type='text/javascript' src='/jscss/main.js'></script>
<script type='text/javascript' src='/jscss/jquery-ui/jquery-ui-1.9.2.custom.min.js'></script>
<link rel="stylesheet" href="/jscss/jquery-ui/jquery-ui-1.9.2.custom.min.css" />
<link rel="stylesheet" href="/jscss/style.css" />
<!-- Analytics -->
<script type="text/javascript">
var _gaq = window._gaq || [];
<?php 
if (isset($_SESSION) && isset($_SESSION['cookies']) || isset($_COOKIE) && isset($_COOKIE['cookies']))
include_once("analyticstracking.php"); 
?>
</script>

<!-- 
	This is a comment 
	
<?php echo($_SERVER['REMOTE_ADDR']); ?>

-->