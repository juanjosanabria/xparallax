<?PHP
define("SQLITE_BUSY_TIMEOUT",120000);

$DB_LOCK = null;

/**
* Conecta a la base de datos
* 
* @param string $host   
* @param int    $port
* @param string $user
* @param string $pass
* @param string $dbname
*/
function db_connect($mode,$db = DATABASE_SQLITE)
{
    global $DB_LOCK;
    if ($DB_LOCK) return $DB_LOCK;    
    $DB_CONNECTOR = new SQLite3($db,$mode);
    if ($DB_CONNECTOR) 
    {
        $resul = $DB_CONNECTOR;    
        $ret = $DB_CONNECTOR->busyTimeout(SQLITE_BUSY_TIMEOUT);  
        $ret = $DB_CONNECTOR->exec("PRAGMA journal_mode = OFF;");
        $ret = $DB_CONNECTOR->exec("PRAGMA synchronous = OFF;");
        $ret = $DB_CONNECTOR->exec('PRAGMA encoding = "UTF-8"');
    }
    else $resul = FALSE;     
    if (! $resul ) 
        trigger_error("Error conectando a la base de datos" . $DB_CONNECTOR->lastErrorMsg()); 
    return $resul;     
}

function db_disconnect()
{
    global $DB_LOCK;
    global $DB_CONNECTOR;
    if ($DB_LOCK) return;      
    $DB_CONNECTOR->close();
}

/**
 * Ejecuta una consulta de manera segura desde un archivo de disco con un número variable de argumentos
 * y formateando las cadenas de manera necesaria. También se le puede pasar una consulta SQL en lugar
 * de un archivo. Conecta a la base de datos si fuera necesario.
 * 
 * @param mixed $filename archivo sql (se le pasa sin extensión) o consulta que se va a a ejecutar
 * @args Argumentos que se insertarán donde indiquen los especificadores de formato
 *       Los especificadores de formato aceptados son:
 *       %d %u          : Entero con signo y entero con signo
 *       %s %c %S %C    : Cadena y sus correspondientes en mayúsculas
 *       %b             : Valor booleano que será traducido a 0 ó 1
 *       %f             : Coma flotante
 *       %k             : Cadena que se convertirá a friendly
 *       %t             : Nombre de tabla/esquema, cadena a la que se le pondrán comillas ``
 *       %l             : Literal sin comillas se volcará tal cual venga  
 * @return  - Devolverá FALSE en caso de error
 *          - En el caso de INSERT se devolverá "mysql_insert_id" ó 0 si no hubiera valor.
 *          - En el caso de UPDATE, DELETE se devolverá el número de filas afectadas
 *          - En caso de SELECT ó SHOW se devolverá un identificador de recurso asociado.     
 */
function db_query($filename /*, ... params*/)
{
    $params = array($filename);        
    $args = func_get_args(); 
    array_shift($args);
    array_push($params, $args);
    $sql = call_user_func_array('db_format',  $params);
    return db_exec($sql);
}

function db_exec($sql)
{
    //Ejecución de la consulta
    if (stripos($sql,"SELECT")===0)
    {
        if (!($db = db_connect(SQLITE3_OPEN_READWRITE))) throw new Exception("Error conectando a la base de datos");
        if (($result = $db->query($sql)) === FALSE) throw new Exception("Error ejecutando: '".substr($sql,0,min(strlen($sql),32)).(strlen($sql)>32?'...':'')."' con mensaje: " . $db->lastErrorMsg());
        $res = new stdClass();
        $res->idx = -1;
        $res->data_assoc = array();
        $res->data_array = array();
        $res->count = 0;
        while ($ret = $result->fetchArray())
        {                 
            $arr1=array();
            $arr2=array();
            foreach ($ret as $key => $value)
            {
                $stcad = str_replace('\'',"",$value);
                $isnull = $stcad === 'NULL';
                if ($stcad === 'FALSE') $value = FALSE;
                if (is_integer($key))
                    array_push($arr1,$isnull?null:$value);
                else                                   
                    $arr2[$key] = $isnull?null:$value;
            }
            array_push($res->data_array,$arr1);      
            array_push($res->data_assoc,$arr2);
            $res->count++;
        }
        $result->finalize();
        return $res;
    }
    else
    {
        if (!($db = db_connect(SQLITE3_OPEN_READWRITE))) throw new Exception("Error conectando a la base de datos");
        if (($result = $db->exec($sql)) === FALSE) throw new Exception("Error ejecutando: '".substr($sql,0,min(strlen($sql),32)).(strlen($sql)>32?'...':'')."' con mensaje: " . $db->lastErrorMsg());       
         //Para los inserts devolvemos INSERT_ID
        if (stripos($sql,"INSERT")===0)
        {
            $id = $db->lastInsertRowID();
            $ret = ($id === 0?true:$id);
        }
        //Para updates devolvemos el número de filas afectadas
        else if (stripos($sql,"UPDATE")===0 ||  stripos($sql,"DELETE")===0)  $ret = $db->changes(); //Número de filas afectadas
        else $ret = true;
        return $ret;
    }
}

/**
* Formatea una sentencia para insertar en una BBDD.
* 
* @param mixed $query
*/      
function db_format(/*varg args*/)
{
    //Obtenemos los argumentos pasados a la función
    $args = func_get_args();    //Eliminar el primer elemento (el archivo ó consulta)
    $filename = $args[0];
    //Esto hay que reacerlo de la manera correcta
    if (@file_exists("$filename")) $fdata = @file("$filename");     
    else if (@file_exists("sql/$filename.sql")) $fdata = @file("sql/$filename.sql"); 
    else $fdata = explode("\n",$filename);
    if (!$fdata || count($fdata) == 0) throw new Exception("Error accediendo a archivo sql ó consulta: $filename");
    //Se eliminan los posibles comentarios al principio
    while (count($fdata)>0 && strpos(trim($fdata[0]),'--') === 0) array_shift($fdata);
    $fdata[0] = ltrim($fdata[0]);
    $fdata = implode("\n",$fdata);
    $tam=strlen($fdata);
    //No es necesario que temine en punto y coma y se elimina para evitar varias consultas
    if ($fdata{$tam-1} == ';') $fdata{$tam-1} = "\0";
    array_shift($args);
    //Si se le ha pasado como argumento un array ahí van los parámetros
    if (count($args)==1 && is_array($args[0])) $args=$args[0];
    $argcount = count($args);
    $icount = 0;
    for ($i=0;$i<$tam-1 && $icount <= $argcount ;$i++)
    {
        if (($fdata{$i} == '%') && ($i<$tam-1) && ($fdata{$i+1} != '%') && ($fdata[$i+1] != ' ') )
        {  
            $cpos = $i;
            $i++;   //Pasamos del porcentaje
            //Hasta que se encuentre el tipo "%[flags][width][.precision][type]" se sigue buscando
            $fmtesc = '';
            while ($i < $tam && ($fdata{$i} == '+' || $fdata{$i} == '-' || $fdata{$i} == '#' || $fdata{$i} == '.'  || ctype_digit($fdata{$i})) )
            {
                $fmtesc.=$fdata{$i};
                $i++;
                $fdata{$i}=' ';
            }
            $type=$fdata{$i};
            if ($args[$icount] === NULL || is_string($args[$icount]) && !strlen($args[$icount])) $args[$icount] = 'NULL';
            else
            switch ($type)
            {
                case 'd':
                    if (is_float($args[$icount])) $args[$icount] = (int)round($args[$icount]);
                    if ($args[$icount] === FALSE) $args[$icount] = sprintf("%$fmtesc$type",0);
                    else if (!is_numeric($args[$icount]) && (double)$args[$icount] != (int)$args[$icount]) throw new Exception("Error, en parámetro $icount, se esperaba un entero:" . $args[$icount]);
                    else $args[$icount] = $args[$icount] = sprintf("%$fmtesc$type",$args[$icount]) ;
                    break;
                case 'u':
                    if (is_float($args[$icount])) $args[$icount] = (int)round($args[$icount]); 
                    if ($args[$icount] === FALSE) $args[$icount] = sprintf("%$fmtesc$type",0);
                    else if (is_numeric($args[$icount]) && !ctype_digit((string)$args[$icount])) throw new Exception("Error, en parámetro $icount, se esperaba un entero sin signo:" . $args[$icount]);
                    else $args[$icount] = sprintf("%$fmtesc$type",$args[$icount]);
                    break;
                case 'f':
                     if ($args[$icount] === FALSE) $args[$icount] = sprintf("%$fmtesc$type",0);
                     else if (!is_float($args[$icount]) && !is_numeric($args[$icount])) throw new Exception("Error, en parámetro $icount, se esperaba un float:" . $args[$icount]);
                     else $args[$icount] = "%$fmtesc$type" == "%f" ? sprintf("%0.20f",$args[$icount]) : sprintf("%$fmtesc$type", $args[$icount]);
                    break;
                case 'C':
                case 'S':
                     if ($args[$icount] != NULL) $args[$icount] = mb_convert_case($args[$icount],MB_CASE_UPPER,mb_detect_encoding($args[$icount]));
                case 'c':
                case 's':
                    if ($args[$icount] === FALSE) $args[$icount] = 'false';
                    else $args[$icount] = sprintf("'%$fmtesc$type'" , SQLite3::escapeString($args[$icount]));
                    break;
                case 'k':
                    if ($args[$icount] === FALSE) $args[$icount] = 'false';
                    else $args[$icount] =  sf_friendly_string($args[$icount]);
                    break;
                case 't':
                    $args[$icount] =  '`' . $args[$icount] . '`';
                case 'l':
                    break;
                case 'b':
                    if ($args[$icount] === FALSE || $args[$icount] === 0 || (is_string($args[$icount]) && strtolower(trim($args[$icount])) == 'false') ) $args[$icount] = '0';
                    else $args[$icount] = '1';
                    break;
                default:
                    throw new Exception("Especificador de formato '$type' desconocido ejecutando consulta: $fdata");
                    break;
            }
            $fdata{$i}=' ';
            $fdata{$cpos+1} = 's';
            $i++;
            $icount++;           
        }
        else if ($fdata{$i} == '%' && ($fdata{$i+1} == '%' || $fdata[$i+1]==' ') ) $i++;
    }
    //Tenemos en $icount los argumentos encontrados y en $argcount los que nos venían en la función
    if ($icount != $argcount) throw new Exception("Error, el formato de la cadena no contiene igual número de argumentos que los pasados a la función  $icount : Args: $argcount");
    //Formateamos
    if (!($sql = @vsprintf($fdata,$args))) throw new Exception("Error en vsprintf formateando la cadena FDATA: ".substr($fdata,0,min(strlen($fdata),32)).(strlen($fdata)>32?'...':'')." : \r\n : ARGS:" . var_export($args,true));
    return $sql;
}


function db_fetch_array($ed)
{
    $ed->idx++;
    if ($ed->idx >= $ed->count) return null;
    return $ed->data_assoc[$ed->idx];
}

function db_fetch_row($ed)
{
    $ed->idx++;
    if ($ed->idx >= $ed->count) return null;
    return $ed->data_array[$ed->idx];
}

function db_fetch_assoc($ed)
{
    $ed->idx++;
    if ($ed->idx >= $ed->count) return null;
    return $ed->data_assoc[$ed->idx];
}

function db_fetch_object($ed)
{
    $arr = db_fetch_array($ed);
    if (!$arr) return $arr;
    $obj = new StdClass();
    foreach ($arr as $key=>$value)
        $obj->$key = $value;
    return $obj;
}

function db_select_value($query /*, ... params*/)
{
    $params = func_get_args();
    $res = call_user_func_array('db_query',$params);
    $ret = db_fetch_row($res);
    return $ret[0]==="NULL"?null:$ret[0];
}


function db_dump_table($table,$destfile)
{
    $pi = pathinfo($destfile);
    $pi['extension'] = strtolower($pi['extension']);
    $sqlfile = ($pi['extension']=='zip'||$pi['extension']=='rar')?sf_get_tmp_name("dump_table-$table",'sql',true):$destfile;
    $cmd = sprintf("mysqldump %s %s --skip-dump-date --complete-insert --user=%s --password=%s --host=%s --result-file=%s",DATABASE_NAME,$table,DATABASE_USER,DATABASE_PASS,DATABASE_HOST,$sqlfile);
    $res = sf_system($cmd);
    if ($res['return'])
    {
      trigger_error("Error dumpeando tabla: $table : " . $res['stdout'] . " / " . $res['stderr']);
      return FALSE;  
    }
    $md5 = md5_file($sqlfile);
    if ($pi['extension']=='zip'||$pi['extension']=='rar')
    {
       if (! sf_compress($sqlfile,$destfile,$pi['extension'],BACKUP_ZIP_PASSWORD) ) return FALSE;   
    }
    return $md5;
}


function db_select_all($query /*, ... params*/)
{
    $params = func_get_args();
    $res = call_user_func_array('db_query',$params);
    if (!$res) return;
    if (!count($res->data_array)) return $res->data_assoc;
    if (count($res->data_array[0]) == 1)
    {
        $marray = array();
        foreach ($res->data_array as $value)
            array_push($marray,$value[0]);
        return $marray;
    }
    return $res->data_assoc;
}

function db_lock($db = DATABASE_SQLITE)
{
    global $DB_LOCK;
    global $DB_LOCK_NAME;
    if ($DB_LOCK && ($DB_LOCK_NAME == $db)) return $DB_LOCK;
    if ($DB_LOCK) db_unlock();
    $DB_LOCK = db_connect(SQLITE3_OPEN_READWRITE, $DB_LOCK_NAME = $db);
    register_shutdown_function('db_unlock');
    return $DB_LOCK;
}

function db_unlock()
{   
    global $DB_LOCK;
    if ($DB_LOCK)
    {
        $DB_LOCK->close();
        $DB_LOCK = null;    
    }
}






















