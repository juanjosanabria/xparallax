<?PHP
require_once('config.php');
require_once('utils.php');
	
//Calculamos el menú y submenú que estamos mostrando
$FNAME = isset($_SERVER['QUERY_STRING']) ? basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) : basename($_SERVER['REQUEST_URI']);
if (!strlen($FNAME)) $FNAME = 'index.php';
$FNAME = explode('.',basename($FNAME, '.php'));
$MENU = $FNAME[0];
$SUBMENU = count($FNAME) > 1 ? $FNAME[1] : null;
if (strpos($_SERVER['REQUEST_URI'],'/blog/') === 0) $MENU = 'blog';
?>
<!-- Política de cookies -->
<?PHP
	if (!isset($_SESSION)) @session_start();
	if (!is_boot())
	if (!isset($_SESSION) || !isset($_COOKIE) && !isset($_SESSION['cookies']) || !isset($_COOKIE['cookies']))
	{
		include('cookies.php');
		$_SESSION['cookies'] = 1;
		@setcookie("cookies", 1, time()+3600*24*365*10);
	}
?>
<!-- Mis opciones de menú -->
<div class="menu_wrapper">
<div>
  <a href='/' style='color:#000;'>
  <span class="logo"></span>
  <h1>XParallax <b><b style="color:#D26900">v</b>iu</b><br/><span> a free software for automated astrometry</span></h1>
  </a>
</div>                         
<?PHP
	foreach ($MENUS as $key => $value)
	{
		if ($key == $MENU) echo "<span>$value</span>";
		else
        {   
			if ($key{0} == '#') continue;
            if (array_key_exists($key, $SUBMENUS))
            {
                foreach ($SUBMENUS[$key] as $skey => $svalue) break;
                echo "<a href='/$key.$skey.php'>$value</a>";
            }
            else if ($key == 'index') 
            echo "<a href='/'>$value</a>";
            else
            echo "<a href='/$key.php'>$value</a>";
        }
	}
?>
</div>









