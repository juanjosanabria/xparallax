<?PHP
/** Configuración de base de datos y otras */
define('DATABASE_SQLITE'		,'xp.s3db');
error_reporting(E_ALL);
ini_set('display_errors', '1');

$MENUS = array('index'    => 'Home', 
			   'download' => 'Downloads', 
			   'help'     => 'Help', 
               'blog'     => 'Blog',
               '#mlearning'  => 'Object Classifier',        //No se muestra en menús principales
			   '#contact'  => 'Contact',        //No se muestra en menús principales
			   '#admin'   => 'Admin'			//Menú oculto de administración
			   );

$SUBMENUS = array(
	//Submenú de ayuda
	'help' => array(
		'index' => 'Help index',
		'calibration_dialog' => 'Image calibration',
		'astrometry_dialog' => 'Astrometric reduction'/*,
		'calib_tutorial' => 'Calibration tutorial',
		'astred_tutorial' => 'Astrometry tutorial',
		'screenshoots' => 'Screenshoots of the interface'*/
	)
);

