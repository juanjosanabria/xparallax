<?PHP 
//Redirección primordial
if (strpos($_SERVER['REQUEST_URI'],'sitemap.php'))
{
    header("HTTP/1.0 301 Moved permanently");
    header('Location: sitemap.xml');
    exit(0);
}

//Página de sitemap
echo '<?xml version="1.0" encoding="UTF-8"?>' 
?>

<urlset
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
>
<?php
include('config.php');
include('class/blog_entry.php');

foreach ($MENUS as $menuk => $menuv)
{
	if ($menuk{0} == '#') continue;
    if (!array_key_exists($menuk, $SUBMENUS))
    {
       if ($menuk == 'index') $menuk = '';
       else $menuk = "$menuk.php";
       $priority = $menuk ? "0.9" : "1.0"; 
       echo "<url>\n";
       echo "\t<loc>http://www.xparallax.com/$menuk</loc>\n";
       echo "\t<changefreq>monthly</changefreq>\n";
       echo "\t<priority>$priority</priority>\n";
       echo "</url>\n\n"; 
    }
    else
    {
        foreach ($SUBMENUS[$menuk] as $subk => $subv)
        {
           if ($subk == 'index') $priority = "0.8";
           else $priority = "0.7";
           echo "<url>\n";
           echo "\t<loc>http://www.xparallax.com/$menuk.$subk.php</loc>\n";
           echo "\t<changefreq>monthly</changefreq>\n";
           echo "\t<priority>$priority</priority>\n";
           echo "</url>\n\n";         
        }   
    }
}    
?>

<!-- Blog entries -->
<?PHP
//Entradas de blog
$entries = BlogEntry::scanForEntries('blog');
$priority = 0.6;
foreach ($entries as $et)
{
   echo "<url>\n";
   echo "\t<loc>http://www.xparallax.com/blog/{$et->FRIENDLY}</loc>\n";
   echo "\t<changefreq>monthly</changefreq>\n";
   echo "\t<priority>$priority</priority>\n";
   echo "</url>\n\n";         
}
?>
<!-- Version history page -->
<url>
	<loc>http://www.xparallax.com/version_history.php</loc>
	<changefreq>monthly</changefreq>
	<priority>0.5</priority>
</url>
<!-- End of sitemap -->
</urlset>