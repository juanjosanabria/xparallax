<?PHP
define('MHTML_LINE_SIZE',76);

class BlogEntry
{
    var $mBlogID;
    var $mSections;             //Secciones del documento mhtml
    var $mFilename;
    var $mText;
    var $mPublishDate;
    var $mTitle;
    var $mRedirectImages;
    var $mAuthor;
    var $mKeywords;
    var $mPublishTimespan;
    var $mDescription;
    var $mFriendly;
    var $mAddTitlesAsDiv;    //Meterá el títlo de la imagen como un div
    var $mImages;
    var $mVShapes;
    var $mMaxImgSize;
    var $mMaxDefImgSize;
    var $mComments;
   
    
    function BlogEntry($filename,$redirect_images=null, $def_img_size = 520, $def_max_img_size = 640)
    {
        $this->mBlogID = $this->friendly(substr(pathinfo($filename,PATHINFO_FILENAME),11));
        $this->mMaxImgSize = $def_img_size;
        $this->mMaxDefImgSize = $def_max_img_size;
        $this->mRedirectImages = $redirect_images;
        $this->mFilename = $filename;
        $this->mPublishDate = substr(basename($filename),0, 10);
        $this->mTitle = BlogEntry::title($filename);
        $this->mImages = array();
        $this->mVShapes = array();
        $this->mText = $this->decode($this->mFilename);  
    }    
    
    
    private static function friendly($text)
    {
        $text = str_replace('_','-', $text);
        $text = str_replace(' ','-', $text, $count);
        do { $text = str_replace('--','-',$text, $count); } while ($count);
        return strtolower($text);
    }
    
    public static function title($filename)
    {
        $fil = fopen($filename,'rt'); if (!$fil) return null;
        while ($cad = fgets($fil))
        {
            $ipos = strpos($cad, '<title');
            if ($ipos !== FALSE)
            {
                 $cad = trim($cad);
                 if (strlen($cad) == MHTML_LINE_SIZE && $cad{MHTML_LINE_SIZE-1} == '=') $cad = substr($cad,0,MHTML_LINE_SIZE-1);
                 else $cad .= ' ';
                 $buff = $cad;
                 while ($cad = fgets($fil))
                 {
                     $buff .= str_replace("\n"," ", str_replace("=\n","", str_replace("\r", "", $cad)));
                     $xpos = strpos($buff, '</title>');
                     if ($xpos !== FALSE)
                     {
                        $title =  strip_tags($buff);
                        $title = str_replace("\n", " ", $title);
                        return mb_convert_encoding($title, 'UTF-8');
                     }
                 }  
            }
        }
        fclose($fil); 
        return null;
    }
    
    //Getters y setters
    public function getText(){return $this->mText;}
    public function getTitle(){return $this->mTitle;}
    public function getPublishDate(){return $this->mPublishDate;}
    public function getKeyWords() {return $this->mKeywords;}
    public function getAuthor(){return $this->mAuthor;}
    public function getDescription(){ return $this->mDescription; }
    public function getFriendly(){return $this->mFriendly;}
    
    
    private function mHtmlParse($filename)
    {
        $this->mSections = array();
        preg_match('/(\d\d\d\d\-\d\d\-\d\d)?_?(?<NAME>[^\.]+)/i',pathinfo($filename,PATHINFO_FILENAME), $matches);
        $this->mFriendly = str_replace("_","-",$matches['NAME']);
        $fil = fopen($filename,'rt');
        $cad = fgets($fil);
        if (!stripos($cad,'MIME-Version') === 0) throw new Exception("Unable to parse document");
    
        $nextpart = "np"; $cad = "--np";
        $cur_section = null;
        $in_headers = false;
        do
        {
            if (trim($cad) == "--".$nextpart)
            {
               if ($cur_section) array_push($this->mSections,$cur_section);
               $cur_section = new stdClass();
               $cur_section->HEADERS = array();
               $cur_section->DATA = "";
               $in_headers = true;
            }
            //En la cabecera
            else if ($in_headers)
            {
                if (!strlen($cad) || $cad == "\r\n" ||  $cad == "\n") $in_headers = false;
                else
                {
                     $cad = trim($cad);
                     $idx = strpos($cad,':');
                     if (!$idx) continue;
                     $hname = strtolower(substr($cad, 0, $idx));
                     $hvalue = substr($cad, $idx+1);
                     $cur_section->HEADERS[$hname] = $hvalue;
                     //Capturar nextpart
                     if ($hname == 'content-type' && preg_match('/boundary="?(?<BD>[^\"]+)"?/i',$cad, $matches))
                         $nextpart = $matches['BD'];
                }
            }
            //En los datos
            else 
            {
                  if (($end = strlen(rtrim($cad))) == MHTML_LINE_SIZE)
                  {
                      if (($cad{$end-1} == '=')) $cad = substr($cad,0,$end-1);
                      else $cad .= ' ';
                  }
                  $cur_section->DATA .= $cad;  
            }
        }
        while ($cad = fgets($fil));
        fclose($fil);
    }
    
    public function getSectionByContentType($ct)
    {
        foreach ($this->mSections as $key => $value)
        {
            //Buscar en las cabeceras la que sea content-type
            foreach ($value->HEADERS as $hname => $hvalue)
            {
                if ($hname != 'content-type') continue;
                if (stripos($hvalue,$ct)!== false) return $value;
            }
        }
        return false;
    }
    
    public function getSectionByLocation($loc)
    {
        $loc = basename($loc);
        foreach ($this->mSections as $key => $value)
        {
            //Buscar en las cabeceras la que sea content-type
            foreach ($value->HEADERS as $hname => $hvalue)
            {
                if ($hname != 'content-location') continue;
                if (basename($hvalue) == $loc) return $value;
            }
        }
        return false; 
    }
    
    
    public function getImage($name, $smaller_one = true)
    {
        if (pathinfo($name,PATHINFO_EXTENSION) != 'bmp')
        {
            $sid = null;
            foreach ($this->mImages as $img)
            {
                if ($img->ONE_FILENAME == $name
                    || $img->TWO_FILENAME == $name
                    || $img->FRIENDLY_NAME == $name
                ) 
                {
                    $sid = $img->ONE_FILENAME;
                    break;
                }
            }
            if (!$sid) return null;
            $simage1 =  $this->getSectionByLocation($img->ONE_FILENAME);
            if (!$smaller_one) return $simage1;
            $simage2 =  isset($img->VSHAPE) ? $this->getImage("{$img->VSHAPE}-{$img->IDX}.bmp") : $simage1;
            return (strlen($simage2->DATA) > strlen($simage1->DATA)
                 && $simage2->DATA && strlen($simage2->DATA) 
                ) 
               ? $simage2 : $simage1;
        }
        else
        {
            if (preg_match('/^.*\-(?<IDX>\d+)\.bmp$/i', $name,$matches))
                $idx = (int)$matches['IDX'];
            else 
                $idx = FALSE;
            
            $name = pathinfo($name,PATHINFO_FILENAME);      //Vienen con extensión bmp para que funcione el fancybox
            $cont = 0;
            foreach ($this->mVShapes as $vs)
            {
                if (($idx === FALSE && $vs->ID == $name) || $idx === $cont)
                {
                    $tmp = tempnam(sys_get_temp_dir(),'zz') . '.zip';
                    file_put_contents($tmp, $vs->DATA);                                      
                    $imgdata = $this->searchImageInZip($tmp);
                    $img = new stdClass();
                    $imgdata->HEADERS = array();
                    $imgdata->HEADERS['content-type'] = $imgdata->CONTENT_TYPE;
                    $imgdata->HEADERS['content-transfer-encoding'] = 'base64';
                    $imgdata->DATA = base64_encode($imgdata->DATA);
                    @unlink($tmp);
                    return $imgdata;
                }
                $cont++;
            }
        }
        return null;
    }
    
    private function searchImageInZip($filename)
    {
        $imgdata = new stdClass();
        $imgdata->NAME = null;
        $imgdata->DATA = null;
        $imgdata->CONTENT_TYPE = null;
        $zip = zip_open($filename);
        do
        {
            if ($ze = zip_read($zip))
            {
                $ename = zip_entry_name($ze);
                $ext = strtolower(pathinfo($ename, PATHINFO_EXTENSION));
                switch ($ext)
                {             
                    case 'png':
                    case 'jpeg':
                    case 'jpg':
                    case 'gif':
                    $imgdata->CONTENT_TYPE = "image/$ext";
                    $imgdata->DATA = zip_entry_read($ze, zip_entry_filesize($ze));
                    $imgdata->NAME = $ename;
                    break;
                }
            }
        } while ($ze && !$imgdata->CONTENT_TYPE);
        @zip_close($zip); 
        return $imgdata;  
    }
    
    private function decode($filename)
    {      
        $this->mHtmlParse($filename);
        $section = $this->getSectionByContentType('text/html');
        //Obtenemos el charset
        $text = $section->DATA;
        if (preg_match('/charset="?(?<CHARSET>[^"]+)"?/i',$section->HEADERS['content-type'],$matches))
        {
            $matches['CHARSET'] = strtoupper($matches['CHARSET']);
            if ($matches['CHARSET'] != 'UTF-8' && $matches['CHARSET'] != 'UTF8')
            $text = mb_convert_encoding($text, 'UTF-8', $matches['CHARSET']);
        }        
        
        //Primero limpiamos de saltos de línea
        $text = str_replace("\r","\n",$text);
        do{ $text = str_replace("\n\n"," ",$text, $count);} while ($count);
        do{ $text = str_replace("\n"," ",$text, $count);} while ($count);
        
        //Obtenemos la cabecera
        $head = $this->getBetween('<head>','</head>',$text);
        $this->extractDocumentInfo($head);
        
        //Obtener los datos que hay dentro del body
        preg_match('/\<BODY[^>]*\>(?<BODY>.*)\<\\/BODY\>/i',$text,$matches);  
        $text = $matches['BODY'];
      
        //Párrafos de lista
        $text = preg_replace('/\<p[^\>]*mso\-list[^\>]*\>/i','<p cname="mso-list"><span cname="mso-list">·</span><span>&nbsp;&nbsp;</span>',$text);       //Párrafo de comienzo de lista
        $text = preg_replace('/\<p[^\>]*margin\-left[^\>]*\>/i','<p cname="mso-parr">',$text);    //Párrafo continuación de lista
       
        //Hacer ciertas operaciones
        $this->manageVShapes($text);
        $text =  $this->manageLists($text);
        $text =  $this->manageImages($text);
        $text =  $this->manageTables($text);
        $text =  $this->manageEquations($text);
        $text =  $this->manageLinks($text);
        $text =  $this->removeBetween("<!--","-->",$text);
        $text = str_replace("=3D","=",$text);
       // $text =  $this->removeBetween("<![if","<![endif]>",$text);
        
        
        //Ahora quitamos más morralla
        $text = preg_replace('/CLASS="[^"]*"/i','',$text);             //Todas las clases
        $text = preg_replace('/CLASS=\'[^\']*\'+/i','',$text);             
        $text = preg_replace('/CLASS=[^\s\>]+/i','',$text);  
        $text = preg_replace('/STYLE="[^"]*"/i','',$text);             //Todos los estilos
        $text = preg_replace('/STYLE=\'[^\']*\'+/i','',$text);
        $text = preg_replace('/STYLE=[^\>]+/i','',$text);  
        $text = preg_replace('/LANG="[^"]*"/i','',$text);              //Todos los tags lang
        $text = preg_replace('/LANG=\'[^\']*\'+/i','',$text);             
        $text = preg_replace('/LANG=[^\s\>]+/i','',$text);             
        
        
        
        //Eliminar ciertos tags        
        $text = preg_replace('/\<o:p\>[^\<]*\<\/o:p\>/i','',$text);
        $text = str_replace('<a name="_GoBack"></a>','',$text);
        
        $text = preg_replace('/\<([A-Z0-9]+)\s+\>/i','<$1>',$text);     //Armonizar espacios antes de cierre de etiquetas de apertura 
        
        //Por útimo dejamos nuestros estilos en paz
        $text = str_replace('_style_=', 'style=', $text);    
        $text = str_replace('_class_=', 'class=', $text);    
        return $text;      
    }
    
    /**
    * Se ocupa de manejar el contenido entre <![if !supportLists]> <![endif]>
    * 
    * @param mixed $text
    */
    private function manageLists($text)
    {
        $star_tag = "<![if !supportLists]>";
        $end_tag = "<![endif]>";
        while (true)
        {
            $idx1 = strpos($text,$star_tag);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $left = substr($text, 0, $idx1);
            $right = substr($text, $idx2 + strlen($end_tag), strlen($text)- $idx2 - strlen($end_tag)); 
            $middle = substr($text,$idx1 + strlen($star_tag), $idx2 - $idx1 - strlen($star_tag));
            
          //  $middle = preg_replace('/\<span.*\<o:p\>\<\/o:p\>\<\/span\>/i','kakafuti',$middle);
         
            
            $text =  $left /*. $middle*/ . $right;
        }
        return $text;
    }
    
    private function manageVShapes($text)
    {
        $star_tag = "<v:shape ";
        $end_tag = "</v:shape>";
        $startin = 0;
        while (true)
        {
            $idx1 = strpos($text,$star_tag, $startin);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $left = substr($text, 0, $idx1);
            $right = substr($text, $idx2 + strlen($end_tag), strlen($text)- $idx2 - strlen($end_tag)); 
            $middle = substr($text,$idx1 + strlen($star_tag), $idx2 - $idx1 - strlen($star_tag));
      
            $obj = new stdClass();
            $obj->ID = $this->getBetween('id=3D"','"',$middle);
            $obj->DATA = base64_decode($this->getBetween('o:gfxdata=3D"','"',$middle));
            array_push($this->mVShapes, $obj);
          
            $startin = strlen($left) + strlen($middle);
        }
    }
    
     private function manageTables($text)
    {
        $star_tag = "<table";
        $end_tag = "</table>";
        $startin = 0;
        while (true)
        {
            $idx1 = strpos($text,$star_tag, $startin);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $left = substr($text, 0, $idx1);
            $right = substr($text, $idx2 + strlen($end_tag), strlen($text)- $idx2 - strlen($end_tag)); 
            $middle = substr($text,$idx1 + strlen($star_tag), $idx2 - $idx1 - strlen($star_tag));
            if (strpos($text,'background:black;') !== FALSE)
            {
                $middle = str_replace("</p>","\r\n",$middle);
                $txt = trim(strip_tags("<table $middle </table>"));
                $txt = str_replace("=3D","=",$txt);
                $middle = "<div _class_='pre'>" .
                          "<pre>"  
                          . $txt . 
                          "</pre></div>";   
            }
            else
            {
                $middle = str_replace("=3D","=",$middle);    
                $idatt = strpos($middle, '>');
                $x_left = substr($middle, 0, $idatt-1);
                $x_right = substr($middle, $idatt+1); 
                $x_right = $this->manageTableTrs($x_right, $mode); 
                if ($mode == 'COMMENT') $middle = '';         
                else $middle = "<table $x_left> $x_right</table>";
            }
            $text = $left . $middle . $right; 
            $startin = strlen($left) + strlen($middle);
        }
    }
    
    private function manageTableTrs($text, &$mode)
    {
        //TODO: Necesario revisar para conservar los estilos
        $mode = '';
        $buffer = "";
        $star_tag = "<tr";
        $end_tag = "</tr>";
        $first_row = '';
        $second_row = '';
        $row_count = 0;
        $pos = 0;
        while (true)
        {
            $idx1 = strpos($text, $star_tag, $pos);
            if ($idx1 === false)  break;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false)  break;
            $idx3 = strpos($text, '>', $idx1+strlen($star_tag));
            if ($idx3 === false)  break;
            
            $style = substr($text, $idx1+strlen($star_tag), $idx3 - $idx1 - strlen($star_tag));
            $content = substr($text, $idx3 + 1,  $idx2 - $idx3 - 1); 
            
            $tddata = $this->manageTableTds($content);
            $buffer .= "<tr ctr='ctr'>". $tddata . "</tr>";
            $row_count++;
            if ($row_count == 1) $first_row = trim(strip_tags($tddata));
            else if ($row_count == 2) $second_row = strip_tags($tddata);
            $pos = $idx2 + 1;
        }
        if ($row_count == 2 && $first_row{0} == '#')
        {
            $mode = substr($first_row, 1);
            return $second_row;
        }
        return $buffer;
    }
  
    /**
    * put your comment there...
    *   
    * @param mixed $text
    * @param mixed Se devolverá TABLE (para tablas), COMMENT, HTML, JAVASCRIPT
    */
    private function manageTableTds($text)
    {
        //TODO: Necesario revisar para conservar los estilos
        $buffer = "";
        $star_tag = "<td";
        $end_tag = "</td>";
        $pos = 0; 
        while (true)
        {
            $idx1 = strpos($text, $star_tag, $pos);
            if ($idx1 === false) break;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false)  break;
            $idx3 = strpos($text, '>', $idx1+strlen($star_tag));
            if ($idx3 === false)  break;
            
            $style = substr($text, $idx1+strlen($star_tag), $idx3 - $idx1 - strlen($star_tag));
            $content = substr($text, $idx3 + 1,  $idx2 - $idx3 - 1); 
            
            $nstyle = '';
            if ($substyle = $this->getBetween("style='", "'", $style))
            {
                $vec = explode(';', $substyle);
                foreach ($vec as $st)
                {
                    $st = explode(':', $st, 2);
                    $st[0] = trim($st[0]);
                    if ($st[0] == 'background') 
                    {
                        $nstyle .= $st[0] . ":"  . $st[1] . ";";
                    }
                }    
            }
            
            $buffer .= "<td $style _style_='$nstyle'> $content </td>";
            $pos = $idx2 + 1;
        }        
        return $buffer;
    }
    
    
    /**
    * Se ocupa de manejar el contenido entre <![if !vml]> <![endif]>"
    * 
    * @param mixed $text
    */
    private function manageImages($text)
    {
        $star_tag = "<![if !vml]>";
        $end_tag = "<![endif]>";
        $startin = 0;
        while (true)
        {
            $idx1 = strpos($text,$star_tag, $startin);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $left = substr($text, 0, $idx1);
            $right = substr($text, $idx2 + strlen($end_tag), strlen($text)- $idx2 - strlen($end_tag)); 
            $middle = substr($text,$idx1 + strlen($star_tag), $idx2 - $idx1 - strlen($star_tag));
            
            //Cuidado, si empieza por <a, es un enlaceee
            $alt = $title = '';
            $img_obj = new stdClass();
            $img_obj->IDX = count($this->mImages);
            if (preg_match('/alt=3D"((T[^:]+:\s(?<ALT>((?!\s-\sDesc).)+)))?(( - Desc[^:]+: )?(?<TITLE>[^"\>]*))?"/i',$middle,$matches))
            {
               $img_obj->ALT = $alt = $matches['ALT'];
               $img_obj->TITLE = $title = $matches['TITLE']; 
            }
            
            $v = explode(' ',$middle);
            if (count($v) && strtolower($v[0]) != '<img')
            {
                  $text =  $left . $right;
            }
            else
            {
                $calign = '';
                $tx = "<img";
                $cname = 'mso-img';
                //Origen de la imagen
                for ($i=0; $i<count($v); $i++)
                {
                    //Control de ancho y máximos de la imagen
                    //Tamaño máximo de la imagen para ponerla en linea
                    if (isset($img_obj->WIDTH) && isset($img_obj->HEIGHT) && $this->mMaxImgSize && $img_obj->WIDTH >= $this->mMaxImgSize)
                    {
                        $width = $this->mMaxDefImgSize;
                        $height = (int)($img_obj->HEIGHT * $this->mMaxDefImgSize / $img_obj->WIDTH);
                        $tx = str_replace("width='{$img_obj->WIDTH}'", "width='{$width}'", $tx);
                        $tx = str_replace("height='{$img_obj->HEIGHT}'", "height='{$height}'", $tx);
                        $cname = 'mso-inlined';
                    }
                    if (preg_match('/width\s*=(3D)?(?<WIDTH>\d+)/i',$v[$i],$mc)) 
                        $tx .= sprintf(" width='%s'", $img_obj->WIDTH = $width = $mc['WIDTH']);
                    else if (preg_match('/height\s*=(3D)?(?<HEIGHT>\d+)/i',$v[$i],$mc)) 
                        $tx .= sprintf(" height='%s'", $img_obj->HEIGHT = $mc['HEIGHT']);
                    else if (preg_match('/v:shapes\s*=(3D)"(?<HSPACE>[\w_]+)"/i',$v[$i],$mc)) 
                    {
                        $calign = sprintf(" calign='%s'", $img_obj->HSPACE = $mc['HSPACE']{0} == '_' ? 'right' : 'right');   
                        $img_obj->VSHAPE =  $mc['HSPACE'];
                    }
                    else if (preg_match('/src\s*=(3D)?"?(?<FNAME>[\w\.\/\-]+)"?/i',$v[$i],$mc))
                    {
              
                        $filename = pathinfo($mc['FNAME'],PATHINFO_FILENAME);
                        $extension = strtolower(pathinfo($mc['FNAME'],PATHINFO_EXTENSION));
                        $img_obj->TWO_FILENAME = basename($mc['FNAME']);
                        //Si existe png preferimos png
                        if (preg_match('/image(?<NUM>\d\d\d)/i', $filename, $matches))
                        {
                            if ($fsec = $this->getSectionByLocation($newfname = sprintf("image%03d.png",$matches['NUM']-1))) 
                                $filename = $newfname;
                            //Probamos jpg
                            if (!$fsec && ($fsec = $this->getSectionByLocation($newfname = sprintf("image%03d.jpg",$matches['NUM']-1)))) 
                                $filename = $newfname;
                            //Probamos jpg
                            if (!$fsec && ($fsec = $this->getSectionByLocation($newfname = sprintf("image%03d.gif",$matches['NUM']-1)))) 
                                $filename = $newfname;
                        }
                        if (!$fsec) $fsec = $this->getSectionByLocation($mc['FNAME']);
                        if ($fsec) $fname = basename($fsec->HEADERS['content-location']);
                        $img_obj->ONE_FILENAME = $fname;
                        //No obstante, si está seteado el alt... la imagen no es la misma
                        if ($alt) $fname = $this->friendly($alt) .  '.'. pathinfo($fname,PATHINFO_EXTENSION);    
                        else $fname = basename($mc['FNAME']);
                        $img_obj->FRIENDLY_NAME = $fname;
                        $tx .= " src='" . sprintf($this->mRedirectImages, $this->mBlogID , $fname) . "'";
                        $fancy = sprintf($this->mRedirectImages, $this->mBlogID , $fname);
                    }
                }                
                if ($alt && strlen($alt)) $tx .= " alt='$alt'";
                if ($title && strlen($title)) $tx .= " title='$title'";
                if ($title && strlen($title))
                {                    
                    $vshapelink = sprintf($this->mRedirectImages, $this->mBlogID , $img_obj->VSHAPE);
                    $middle = "<span $calign cname='$cname'>"   //Container
                          . sprintf("<a _class_='fancybox' href='$vshapelink-{$img_obj->IDX}.bmp' title='%s' >",str_replace('(click to enlarge)','',$title))
                          . $tx . " cname='mclick-img' />"
                          . "</a>"  
                          . ($title ?  ( "<span _style_='width:{$width}px;' cname='mso-imgtitle'><br/>$title</span>")   : '')
                          . "</span>";
                }
                else
                {
                    $middle = $tx . " $calign/>";
                     
                }
                $startin = strlen($left) + strlen($middle);
                $text =  $left .  $middle . $right;
               
                array_push($this->mImages, $img_obj);
            }
        }
        return $text;
    }
    
    /**
    * Evita que se eliminen las ecuaciones preservando el texto entre condicionales de ecuaciones
    * 
    * @param mixed $text
    */
    private function manageEquations($text)
    {
        
        $star_tag = "<![if !msEquation]>";
        $end_tag = "<![endif]>";
        while (true)
        {
            $idx1 = strpos($text,$star_tag);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $left = substr($text, 0, $idx1);
            $middle = substr($text,$idx1 + strlen($star_tag), $idx2 - $idx1 - strlen($star_tag));
            $right = substr($text, $idx2 + strlen($end_tag), strlen($text)- $idx2 - strlen($end_tag)); 
            //En todas las imágenes del texto del medio incluimos el estilo de ecuación
            $middle = preg_replace('/\<img([^\>]*)\/\>/i','<img $1 cname="mso-equation" />',$middle);
            $text =  $left . $middle . $right;
        }
    }
    
    /**
    * Formatea correctamente los links y los anclajes.
    * 
    * @param mixed $text
    */
    private function manageLinks($text)
    {
          //<a name=3D"_Calibrating_with_XParallax"></a>  <<-- ejemplo
         //Eliminamos el 3D de los anclajes
         $text = preg_replace('/\<a\s+name=3D"([^"]*)"([^\>]*)>/i','<a name="$1"$2/>',$text,100, $count);
         //Enlaces con HREF
         $text = preg_replace('/\<a\s+href=3D"([^"]*)"([^\>]*)>/i','<a href="$1"$2/>',$text,100, $count);
         return $text;
    }
    
    /**
    * Elimina todo el texto contenido entre dos cadenas
    * 
    * @param mixed $star_tag
    * @param mixed $end_tag
    * @param mixed $text
    */
    private function removeBetween($star_tag, $end_tag, $text)
    {
        while (true)
        {
            $idx1 = strpos($text,$star_tag);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $left = substr($text, 0, $idx1);
            $right = substr($text, $idx2 + strlen($end_tag), strlen($text)- $idx2 - strlen($end_tag)); 
            $text =  $left . $right;
        }
    }
    
    /**
    * Preserva todo el texto contenido entre dos cadenas pero elimina las cadenas.
    * 
    * @param mixed $star_tag
    * @param mixed $end_tag
    * @param mixed $text
    */
    private function preserveBetween($star_tag, $end_tag, $text)
    {
        while (true)
        {
            $idx1 = strpos($text,$star_tag);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $left = substr($text, 0, $idx1);
            $middle = substr($text,$idx1 + strlen($star_tag), $idx2 - $idx1 - strlen($star_tag));
            $right = substr($text, $idx2 + strlen($end_tag), strlen($text)- $idx2 - strlen($end_tag)); 
            $text =  $left . $middle . $right;
        }
    }
    
    private function getTagContent($text, $tag)
    {
        $regex = sprintf('/\<%s[^\>]*\>(?<VALUE>[^\<]*)\<\/%s\>/i',$tag,$tag);
        if (!preg_match($regex,$text,$matches)) return null;
        return $matches['VALUE'];
    }
    
    private function getBetween($star_tag, $end_tag, $text)
    {
        while (true)
        {
            $idx1 = strpos($text,$star_tag);
            if ($idx1 === false) return $text;
            $idx2 = strpos($text, $end_tag, $idx1+strlen($star_tag));
            if ($idx2 === false) return $text;
            $middle = substr($text,$idx1 + strlen($star_tag), $idx2 - $idx1 - strlen($star_tag));
            return $middle;
        }
    }
    
    private function getTagAttributes($text, $tag)
    {
        $regex = sprintf('/\<%s(?<ATTR>[^\>]*)\>/i',$tag,$tag);
        if (!preg_match($regex,$text,$matches)) return null;
        return $matches['ATTR'];
    }

    private function extractDocumentInfo($header)
    {
        $this->mTitle = $this->getTagContent($header, 'title');
        $this->mAuthor = $this->getTagContent($header,'o:Author');
        $this->mKeywords = $this->getTagContent($header,'o:Keywords');
        $this->mDescription = $this->getTagContent($header,'o:Description');
        $this->mPublishDate = ($pd = $this->getTagContent($header,'o:PUBLISH_DATE'))?$pd:$this->mPublishDate;
        $this->mPublishTimespan = @strtotime($this->mPublishDate);
    }
    
    public static function scanForEntries($directory)
    {
        //Leer todas las entradas y decodificarlas
        $files = scandir($directory);
        $entries = array();
        foreach ($files as $file)
        {
            if ($file{0} == '.') continue;
            $date = strtotime(substr($file,0,10));
            if (!$date) continue;
            if ( ($ext = pathinfo($file,PATHINFO_EXTENSION)) != 'mhtml' && $ext != 'mhtm') continue;
            if (($fname = substr(pathinfo($file,PATHINFO_FILENAME),11)) == 'template') continue;
            $obj = new stdClass();
            $obj->DATE = date('Y-m-d', $date);
            $obj->FRIENDLY = BlogEntry::friendly($fname);
            $obj->FILENAME = $file;
            $obj->TITLE = BlogEntry::title("$directory/$file");
            array_push($entries,$obj);
        }

        //Ordenar el vector de mayor a menor
        for ($i=0,$cont=count($entries);$i<$cont;$i++)
        {
            $cambio = false;
            for ($j=$i+1;$j<$cont;$j++)
            {
                $obj1 = $entries[$i];
                $obj2 = $entries[$j];
                if ($obj1->DATE < $obj2->DATE)
                {
                    $aux = $obj1; 
                    $entries[$i] = $obj2;
                    $entries[$j] = $aux;
                    $cambio = true;
                }                
            }
            if (!$cambio) break;
        }
        return $entries;
    }
    
};