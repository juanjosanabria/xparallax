<?PHP

function send_eml($to,$data)
{
    global $SMTP_SERVER;  global $SMTP_USER; global $SMTP_PASS; global $SMTP_PORT;
    if (file_exists($data)) $data = file_get_contents($data);
    $to = explode(';',$to); foreach ($to as $key => $value) $to[$key] = trim($value);
    $sock = fsockopen($SMTP_SERVER,$SMTP_PORT);
    $cad = fgets($sock); if (strpos($cad,'220') !== 0) return false;
    fprintf($sock,"HELO dominiodecorreo.es\r\n");
    $cad = fgets($sock); if ($cad{0} != '2') return false;
    
    //AUTENTICACIÓN
    if ($SMTP_USER&&$SMTP_PASS)
    {
        if (!smtp_auth($sock,$SMTP_USER,$SMTP_PASS)) return false;
    }
    
    //MAIL FROM:
    fprintf($sock,"MAIL FROM: %s\r\n",$SMTP_USER);
    $cad = fgets($sock); if ($cad{0} != '2') return false;
    
    //RCPT TO:
    foreach ($to as $toname)
    {
        fprintf($sock,"RCPT TO: %s\r\n",$toname);
        $cad = fgets($sock); if ($cad{0} != '2') return false;    
    }
    
    //DATA
    fprintf($sock,"DATA\r\n");
    $cad = fgets($sock); if ($cad{0} != '3') return false;    
    fprintf($sock,rtrim($data));
    fprintf($sock,"\r\n.\r\n");
    $cad = fgets($sock); 
    
    //QUIT
    fprintf($sock,"QUIT\r\n");
    $cad = fgets($sock); 
    
    fclose($sock);
    return true;
}


function smtp_configure($server,$user,$pass=NULL,$port=25)
{
    global $SMTP_SERVER;  global $SMTP_USER; global $SMTP_PASS; global $SMTP_PORT;
    $SMTP_SERVER = $server;
    $SMTP_USER = $user;
    $SMTP_PASS = $pass;
    $SMTP_PORT = $port;
}

function smtp_next_boundary()
{
    global $current_boundary;
    return ($current_boundary = uniqid('--=_NextPart_',true));
}

function smtp_current_boundary()
{
    global $current_boundary;
    return $current_boundary;
}

function smtp_start_message()
{
    $data =  "X-Priority: 10\r\n";
    $data .= "X-Mailer: Mailer for XParallax 1.0\r\n";
    $data .= "Date: " .   gmdate('D, d M Y H:i:s') . " GMT\r\n";
    $data .= "Message-ID: <" . uniqid('',true) . "\$@avv.cmm>\r\n";
    $data .= "MIME-Version: 1.0\r\n";
    return $data;
}

function smtp_add_subject(&$data,$subject)
{
    $data .= "Subject: ".smtp_utf8($subject)."\r\n";
}

function smtp_utf8($cad)
{return  "=?utf-8?B?".base64_encode($cad)."?=";}

function smtp_add_from(&$data,$name,$email=null)
{
    global $SMTP_USER;
    if (!$email) $email = $SMTP_USER;
    $data .= "From: ".smtp_utf8($name)." <$email>\r\n";
}

function smtp_add_to(&$data,$name,$email)
{
    global $SMTP_USER;
    if (!$email) $email = $SMTP_USER;
    $data .= "To: ".smtp_utf8($name)." <$email>\r\n";
}


function smtp_add_body(&$data,$body,$content_type='text/plain')
{
    //Añadir la última cabecera
    $data .= "Content-Type: multipart/related;\r\n boundary=\"" . smtp_next_boundary() . "\"\r\n";
    //Añadir el cuerpo
    $data .= "\r\n--" . smtp_current_boundary() . "\r\n" 
                ."Content-Type: $content_type\n"
                ."Content-Transfer-Encoding: base64\r\n\r\n";
    $data .=  chunk_split(base64_encode($body)) . "\r\n";
}

function smtp_add_header(&$data,$hname,$hdata)
{
   if (strtolower($hname) == 'to')
        $data = "$hname: $hdata\r\n$data" ; 
   else
        $data .= "$hname: $hdata\r\n";
}

function smtp_attatch(&$data,$fdata,$fname=null,$content_type='text/plain')
{
    $data .= "\r\n--" . smtp_current_boundary() . "\r\n" 
    ."Content-Type: $content_type\r\n";
    if ($fname)
        $data.= "Content-Disposition: attachment; filename=$fname;\r\n"; 
    $data .= "Content-Transfer-Encoding: base64\r\n\r\n";
    $data .=  chunk_split(base64_encode($fdata)) . "\r\n";
}

function smtp_auth($sock,$user,$pass,$auth_type='ANY')
{
    if ($auth_type == 'ANY')
    {
        if (smtp_auth($sock,$user,$pass,'CRAM-MD5')) return true;
        if (smtp_auth($sock,$user,$pass,'LOGIN')) return true;
        if (smtp_auth($sock,$user,$pass,'PLAIN')) return true;
        return false;
    }
    else if ($auth_type == 'CRAM-MD5')
    {
        fprintf($sock,"AUTH CRAM-MD5\r\n");
        $cads = explode(' ', fgets($sock), 2);
        if (!$cads || count($cads) != 2 || $cads[0]{0} != '3') return false;
        $challenge = base64_decode(trim($cads[1]));
        $enci = sprintf("%s %s",$user,hash_hmac('md5',$challenge, $pass));
        fprintf($sock,"%s\r\n", base64_encode($enci));
        $cads = explode(' ', fgets($sock), 2);
        if (!$cads || count($cads) != 2 || $cads[0] != '235') return false;   
    }
    else if ($auth_type == 'LOGIN')
    {
        fprintf($sock,"AUTH LOGIN\r\n");  
        $cads = explode(' ', fgets($sock), 2);
        if (!$cads || count($cads) != 2 || $cads[0]{0} != '3') return false;
        fprintf($sock, "%s\r\n", base64_encode($user));
        $cads = explode(' ', fgets($sock), 2);
        if (!$cads || count($cads) != 2 || $cads[0]{0} != '3') return false;
        fprintf($sock, "%s\r\n", base64_encode($pass)); 
        $cads = explode(' ', fgets($sock), 2);
        if (!$cads || count($cads) != 2 || $cads[0] != '235') return false;    
    }
    else if ($auth_type == 'PLAIN')
    {
        fprintf($sock,"AUTH PLAIN %s\r\n",base64_encode("$user $pass"));
        $cads = explode(' ', fgets($sock), 2);
        if (!$cads || count($cads) != 2 || $cads[0] != '235') return false;    
    }
    return true;
}




