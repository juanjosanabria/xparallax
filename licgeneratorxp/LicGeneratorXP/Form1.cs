﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace LicGeneratorXP
{
    public partial class Form1 : Form
    {
        private static Random RANDOM = new Random();
        private static Dictionary<String, String> FIELDS;

        public Form1()
        {
            InitializeComponent();
            LoadSettings();
        }

        private Dictionary<String, String> GetFieldsLic()
        {
            Dictionary<String, String> fields = new Dictionary<string, string>();
            //Valid from
            fields.Add("ValidFrom", dateTimeValidFrom.Value.ToString("yyyy-MM-dd"));
            //Valid to
            fields.Add("ValidTo", dateTimeValidTo.Value.ToString("yyyy-MM-dd"));
            //Free comment
            textBoxFreeComment.Text = textBoxFreeComment.Text.Trim();
            if (textBoxFreeComment.Text.Length > 0) fields.Add("Comments", textBoxFreeComment.Text);
            if (dateTimeValidFrom.Value >= dateTimeValidTo.Value)
            {
                MessageBox.Show("Las fechas ValidFrom y ValidTo deben comprender un intervalo mayor que un día");
                return null;
            }
            //Identidad
            textBoxIdentity.Text = textBoxIdentity.Text.Trim();
            if (textBoxIdentity.Text.Length == 0)
            {
                MessageBox.Show("Se debe especificar una identidad");
                return null;
            }
            if (textBoxIdentity.Text.Length > 0) fields.Add("Identity", textBoxIdentity.Text);
            //Restriction
            if (comboBoxRestriction.SelectedIndex > 0)
            {
                fields.Add("Restriction", comboBoxRestriction.SelectedIndex.ToString());
            }
            //Quota
            textBoxQuota.Text = textBoxQuota.Text.Trim();
            if (textBoxQuota.Text.Length == 0)
            {
                MessageBox.Show("Se debe especificar la quota");
                return null;
            }
            fields.Add("Quota", textBoxQuota.Text);
            //Random field
            textBoxRandom.Text = textBoxRandom.Text.Trim();
            string rnd = textBoxRandom.Text;
            if (textBoxRandom.Text.Length == 0)
            {
                for (int i = 0; i < 5; i++) rnd += Math.Abs(RANDOM.Next()).ToString();
                textBoxRandom.Text = rnd;
            }
            fields.Add("Random", rnd);
            SaveSettings();
            return fields; 
        }

        private Dictionary<String, String> GetFieldsID()
        {
            Dictionary<String, String> fields = new Dictionary<string, string>();
            //ID
            textBoxLicID.Text = textBoxLicID.Text.Trim();
            if (textBoxLicID.Text.Length > 0)
            {
                fields.Add("ID", textBoxLicID.Text);
            }
            //Free comment
            textBoxFreeCommentID.Text = textBoxFreeCommentID.Text.Trim();
            if (textBoxFreeCommentID.Text.Length > 0) fields.Add("Comments", textBoxFreeCommentID.Text);
            //Machine MAC
            textBoxMAC.Text = textBoxMAC.Text.Trim().Replace("-", ":");
            if (textBoxMAC.Text.Length > 0)
            {
                if (!Regex.IsMatch(textBoxMAC.Text, "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$"))
                {
                    MessageBox.Show("MAC no válida. Inserte una MAC en formato 3D:F2:C9:A6:B3:4F ó 3D-F2-C9-A6-B3-4F");
                    return null;
                }
                fields.Add("MacAddr", textBoxMAC.Text);
            }
            //Host name
            textBoxMachineName.Text = textBoxMachineName.Text.Trim();
            if (textBoxMachineName.Text.Length > 0)
            {
                fields.Add("HostName", textBoxMachineName.Text);
            }
            //Host name
            textBoxUserName.Text = textBoxUserName.Text.Trim();
            if (textBoxUserName.Text.Length > 0)
            {
                fields.Add("User", textBoxUserName.Text);
            }
            //Random field
            textBoxRandomID.Text = textBoxRandomID.Text.Trim();
            string rnd = textBoxRandomID.Text;
            if (textBoxRandomID.Text.Length == 0)
            {
                for (int i = 0; i < 5; i++) rnd += Math.Abs(RANDOM.Next()).ToString();
                textBoxRandomID.Text = rnd;
            }
            fields.Add("Random", rnd);
            SaveSettings();
            return fields;
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            textBoxSecret.Text = textBoxSecret.Text.Trim();
            if (textBoxSecret.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, rellena el campo 'SECRETO'");
                return;
            }
            StringBuilder concat = new StringBuilder();
            FIELDS = GetFieldsLic();
            if (FIELDS == null) return;
            foreach (KeyValuePair<string,string> kv in FIELDS)
            {
                concat.Append(kv.Key);
                concat.Append("|");
                concat.Append(kv.Value);
                concat.Append(",");
            }
            concat.Append(textBoxSecret.Text);
            string md5 = MD5(concat.ToString());
            textBoxSignatureID.Text = md5;
        }

        public string MD5(string input)
        {
            // step 1, calculate MD5 hash from input
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() != DialogResult.OK) return;
            FIELDS = null;
            buttonCalculate_Click(null, null);
            if (FIELDS == null) return;
            if (File.Exists(sfd.FileName)) File.Delete(sfd.FileName);
            StreamWriter sw = new StreamWriter(File.OpenWrite(sfd.FileName), Encoding.UTF8);
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sw.WriteLine("<License xmlns=\"XPTRACES\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"XP NewXMLSchema.xsd\" >");
            foreach (KeyValuePair<string, string> kv in FIELDS)
            {
                sw.WriteLine("\t<" + kv.Key + ">" + kv.Value + "</" + kv.Key + ">");
            }
            sw.WriteLine("\t<Sig>" + textBoxSignature.Text + "</Sig>");
            sw.WriteLine("</License>");
            sw.Close();
        }

        private void LoadSettings()
        {
            textBoxSecret.Text = "999865946131645798331615427984546";
            /*
            try
            {
                if (LCXP.Default.ValidFrom > DateTime.MinValue)
                    dateTimeValidFrom.Value = LCXP.Default.ValidFrom;
                if (LCXP.Default.ValidTo > DateTime.MinValue)
                    dateTimeValidTo.Value = LCXP.Default.ValidTo;
                textBoxMAC.Text = LCXP.Default.MacAddr;
                textBoxMachineName.Text = LCXP.Default.HostName;
                textBoxFreeComment.Text = LCXP.Default.FreeComment;
                comboBoxRestriction.SelectedIndex = LCXP.Default.Restriction;
            }
            catch(Exception)
            { }*/
        }

        private void SaveSettings()
        {/*
            LCXP.Default.ValidFrom = dateTimeValidFrom.Value;
            LCXP.Default.ValidTo = dateTimeValidTo.Value;
            LCXP.Default.MacAddr = textBoxMAC.Text;
            LCXP.Default.HostName = textBoxMachineName.Text;
            LCXP.Default.FreeComment = textBoxFreeComment.Text;
            LCXP.Default.Restriction = comboBoxRestriction.SelectedIndex;
            LCXP.Default.Save();^*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() != DialogResult.OK) return;
            FIELDS = null;
            buttonCalcID_Click(null, null);
            if (FIELDS == null) return;
            if (File.Exists(sfd.FileName)) File.Delete(sfd.FileName);
            StreamWriter sw = new StreamWriter(File.OpenWrite(sfd.FileName), Encoding.UTF8);
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sw.WriteLine("<MachineId xmlns=\"XPTRACES\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"XP NewXMLSchema.xsd\" >");
            foreach (KeyValuePair<string, string> kv in FIELDS)
            {
                sw.WriteLine("\t<" + kv.Key + ">" + kv.Value + "</" + kv.Key + ">");
            }
            sw.WriteLine("\t<Sig>" + textBoxSignatureID.Text + "</Sig>");
            sw.WriteLine("</MachineId>");
            sw.Close();
        }

        private void buttonCalcID_Click(object sender, EventArgs e)
        {
            textBoxSecret.Text = textBoxSecret.Text.Trim();
            if (textBoxSecret.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, rellena el campo 'SECRETO'");
                return;
            }
            StringBuilder concat = new StringBuilder();
            FIELDS = GetFieldsID();
            if (FIELDS == null) return;
            foreach (KeyValuePair<string, string> kv in FIELDS)
            {
                concat.Append(kv.Key);
                concat.Append("|");
                concat.Append(kv.Value);
                concat.Append(",");
            }
            concat.Append(textBoxSecret.Text);
            string md5 = MD5(concat.ToString());
            textBoxSignatureID.Text = md5;
        }

        private void buttonSave_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() != DialogResult.OK) return;
            FIELDS = null;
            buttonCalculate_Click(null, null);
            if (FIELDS == null) return;
            if (File.Exists(sfd.FileName)) File.Delete(sfd.FileName);
            StreamWriter sw = new StreamWriter(File.OpenWrite(sfd.FileName), Encoding.UTF8);
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sw.WriteLine("<License xmlns=\"XPTRACES\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"XP NewXMLSchema.xsd\" >");
            foreach (KeyValuePair<string, string> kv in FIELDS)
            {
                sw.WriteLine("\t<" + kv.Key + ">" + kv.Value + "</" + kv.Key + ">");
            }
            sw.WriteLine("\t<Sig>" + textBoxSignatureID.Text + "</Sig>");
            sw.WriteLine("</License>");
            sw.Close();
        }
    }
}
