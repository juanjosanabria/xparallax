﻿namespace LicGeneratorXP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxSecret = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Identidad = new System.Windows.Forms.TabPage();
            this.textBoxRandomID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonCalcID = new System.Windows.Forms.Button();
            this.textBoxSignatureID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxMachineName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxMAC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFreeCommentID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxLicID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonSaveID = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBoxRandom = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.textBoxSignature = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxRestriction = new System.Windows.Forms.ComboBox();
            this.textBoxIdentity = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxFreeComment = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimeValidTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimeValidFrom = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxQuota = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Identidad.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxSecret);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(561, 47);
            this.panel1.TabIndex = 0;
            // 
            // textBoxSecret
            // 
            this.textBoxSecret.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSecret.Location = new System.Drawing.Point(161, 12);
            this.textBoxSecret.Name = "textBoxSecret";
            this.textBoxSecret.Size = new System.Drawing.Size(388, 23);
            this.textBoxSecret.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(21, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 23);
            this.label1.TabIndex = 19;
            this.label1.Text = "Shared secret:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Identidad);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 47);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(561, 301);
            this.tabControl1.TabIndex = 1;
            // 
            // Identidad
            // 
            this.Identidad.Controls.Add(this.textBoxRandomID);
            this.Identidad.Controls.Add(this.label12);
            this.Identidad.Controls.Add(this.buttonCalcID);
            this.Identidad.Controls.Add(this.textBoxSignatureID);
            this.Identidad.Controls.Add(this.label13);
            this.Identidad.Controls.Add(this.textBoxUserName);
            this.Identidad.Controls.Add(this.label15);
            this.Identidad.Controls.Add(this.textBoxMachineName);
            this.Identidad.Controls.Add(this.label5);
            this.Identidad.Controls.Add(this.textBoxMAC);
            this.Identidad.Controls.Add(this.label4);
            this.Identidad.Controls.Add(this.textBoxFreeCommentID);
            this.Identidad.Controls.Add(this.label11);
            this.Identidad.Controls.Add(this.textBoxLicID);
            this.Identidad.Controls.Add(this.label10);
            this.Identidad.Controls.Add(this.buttonSaveID);
            this.Identidad.Location = new System.Drawing.Point(4, 25);
            this.Identidad.Name = "Identidad";
            this.Identidad.Padding = new System.Windows.Forms.Padding(3);
            this.Identidad.Size = new System.Drawing.Size(553, 272);
            this.Identidad.TabIndex = 0;
            this.Identidad.Text = "Identidad";
            this.Identidad.UseVisualStyleBackColor = true;
            // 
            // textBoxRandomID
            // 
            this.textBoxRandomID.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRandomID.Location = new System.Drawing.Point(157, 159);
            this.textBoxRandomID.Name = "textBoxRandomID";
            this.textBoxRandomID.Size = new System.Drawing.Size(388, 23);
            this.textBoxRandomID.TabIndex = 50;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(17, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 23);
            this.label12.TabIndex = 49;
            this.label12.Text = "Random:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonCalcID
            // 
            this.buttonCalcID.Location = new System.Drawing.Point(470, 185);
            this.buttonCalcID.Name = "buttonCalcID";
            this.buttonCalcID.Size = new System.Drawing.Size(75, 26);
            this.buttonCalcID.TabIndex = 48;
            this.buttonCalcID.Text = "Calcualte";
            this.buttonCalcID.UseVisualStyleBackColor = true;
            this.buttonCalcID.Click += new System.EventHandler(this.buttonCalcID_Click);
            // 
            // textBoxSignatureID
            // 
            this.textBoxSignatureID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBoxSignatureID.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSignatureID.Location = new System.Drawing.Point(157, 188);
            this.textBoxSignatureID.Name = "textBoxSignatureID";
            this.textBoxSignatureID.ReadOnly = true;
            this.textBoxSignatureID.Size = new System.Drawing.Size(298, 23);
            this.textBoxSignatureID.TabIndex = 47;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(17, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 23);
            this.label13.TabIndex = 46;
            this.label13.Text = "Signature:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMachineName
            // 
            this.textBoxMachineName.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMachineName.Location = new System.Drawing.Point(157, 101);
            this.textBoxMachineName.Name = "textBoxMachineName";
            this.textBoxMachineName.Size = new System.Drawing.Size(388, 23);
            this.textBoxMachineName.TabIndex = 45;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(17, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 23);
            this.label5.TabIndex = 44;
            this.label5.Text = "Machine name:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMAC
            // 
            this.textBoxMAC.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMAC.Location = new System.Drawing.Point(157, 72);
            this.textBoxMAC.Name = "textBoxMAC";
            this.textBoxMAC.Size = new System.Drawing.Size(388, 23);
            this.textBoxMAC.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(17, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 23);
            this.label4.TabIndex = 42;
            this.label4.Text = "Machine MAC:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxFreeCommentID
            // 
            this.textBoxFreeCommentID.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFreeCommentID.Location = new System.Drawing.Point(157, 43);
            this.textBoxFreeCommentID.Name = "textBoxFreeCommentID";
            this.textBoxFreeCommentID.Size = new System.Drawing.Size(388, 23);
            this.textBoxFreeCommentID.TabIndex = 41;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(17, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 23);
            this.label11.TabIndex = 40;
            this.label11.Text = "Free comment:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxLicID
            // 
            this.textBoxLicID.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLicID.Location = new System.Drawing.Point(157, 14);
            this.textBoxLicID.Name = "textBoxLicID";
            this.textBoxLicID.Size = new System.Drawing.Size(388, 23);
            this.textBoxLicID.TabIndex = 39;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(17, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 23);
            this.label10.TabIndex = 38;
            this.label10.Text = "ID:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonSaveID
            // 
            this.buttonSaveID.Location = new System.Drawing.Point(438, 226);
            this.buttonSaveID.Name = "buttonSaveID";
            this.buttonSaveID.Size = new System.Drawing.Size(109, 40);
            this.buttonSaveID.TabIndex = 37;
            this.buttonSaveID.Text = "Save to file";
            this.buttonSaveID.UseVisualStyleBackColor = true;
            this.buttonSaveID.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBoxQuota);
            this.tabPage2.Controls.Add(this.textBoxRandom);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.buttonSave);
            this.tabPage2.Controls.Add(this.buttonCalculate);
            this.tabPage2.Controls.Add(this.textBoxSignature);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.comboBoxRestriction);
            this.tabPage2.Controls.Add(this.textBoxIdentity);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.textBoxFreeComment);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.dateTimeValidTo);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.dateTimeValidFrom);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(553, 272);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Licencia";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBoxRandom
            // 
            this.textBoxRandom.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRandom.Location = new System.Drawing.Point(157, 151);
            this.textBoxRandom.Name = "textBoxRandom";
            this.textBoxRandom.Size = new System.Drawing.Size(388, 23);
            this.textBoxRandom.TabIndex = 38;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(17, 150);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 23);
            this.label9.TabIndex = 37;
            this.label9.Text = "Random:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(436, 224);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(109, 40);
            this.buttonSave.TabIndex = 36;
            this.buttonSave.Text = "Save to file";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click_1);
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Location = new System.Drawing.Point(470, 177);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(75, 26);
            this.buttonCalculate.TabIndex = 35;
            this.buttonCalculate.Text = "Calcualte";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // textBoxSignature
            // 
            this.textBoxSignature.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBoxSignature.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSignature.Location = new System.Drawing.Point(157, 180);
            this.textBoxSignature.Name = "textBoxSignature";
            this.textBoxSignature.ReadOnly = true;
            this.textBoxSignature.Size = new System.Drawing.Size(298, 23);
            this.textBoxSignature.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(17, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 23);
            this.label8.TabIndex = 33;
            this.label8.Text = "Signature:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxRestriction
            // 
            this.comboBoxRestriction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRestriction.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxRestriction.FormattingEnabled = true;
            this.comboBoxRestriction.Items.AddRange(new object[] {
            "No restriction",
            "Slow",
            "Ultra Slow",
            "Super Ultra Slow",
            "Add small error",
            "Add medium error",
            "Add big error",
            "SS slow 7",
            "SS slow 8",
            "SS slow 9",
            "SS slow 10",
            "SS slow 11",
            "SS slow 12",
            "SS slow 13",
            "SS slow 14",
            "SS slow 15",
            "SS slow 16",
            "SS slow 17",
            "SS slow 18",
            "SS slow 19",
            "SS slow 20"});
            this.comboBoxRestriction.Location = new System.Drawing.Point(157, 122);
            this.comboBoxRestriction.Name = "comboBoxRestriction";
            this.comboBoxRestriction.Size = new System.Drawing.Size(224, 23);
            this.comboBoxRestriction.TabIndex = 32;
            // 
            // textBoxIdentity
            // 
            this.textBoxIdentity.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIdentity.Location = new System.Drawing.Point(157, 93);
            this.textBoxIdentity.Name = "textBoxIdentity";
            this.textBoxIdentity.Size = new System.Drawing.Size(388, 23);
            this.textBoxIdentity.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(17, 92);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(134, 23);
            this.label14.TabIndex = 30;
            this.label14.Text = "Identity:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxFreeComment
            // 
            this.textBoxFreeComment.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFreeComment.Location = new System.Drawing.Point(157, 64);
            this.textBoxFreeComment.Name = "textBoxFreeComment";
            this.textBoxFreeComment.Size = new System.Drawing.Size(388, 23);
            this.textBoxFreeComment.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(17, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 23);
            this.label6.TabIndex = 30;
            this.label6.Text = "Free comment:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTimeValidTo
            // 
            this.dateTimeValidTo.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeValidTo.Location = new System.Drawing.Point(157, 35);
            this.dateTimeValidTo.Name = "dateTimeValidTo";
            this.dateTimeValidTo.Size = new System.Drawing.Size(388, 23);
            this.dateTimeValidTo.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(17, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 23);
            this.label3.TabIndex = 24;
            this.label3.Text = "Valid to:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTimeValidFrom
            // 
            this.dateTimeValidFrom.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeValidFrom.Location = new System.Drawing.Point(157, 6);
            this.dateTimeValidFrom.Name = "dateTimeValidFrom";
            this.dateTimeValidFrom.Size = new System.Drawing.Size(388, 23);
            this.dateTimeValidFrom.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(17, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 23);
            this.label2.TabIndex = 22;
            this.label2.Text = "Valid from:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(17, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 23);
            this.label7.TabIndex = 20;
            this.label7.Text = "Restriction:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(17, 130);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(134, 23);
            this.label15.TabIndex = 44;
            this.label15.Text = "User name:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUserName.Location = new System.Drawing.Point(157, 130);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(388, 23);
            this.textBoxUserName.TabIndex = 45;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(387, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 23);
            this.label16.TabIndex = 20;
            this.label16.Text = "Quota:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxQuota
            // 
            this.textBoxQuota.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQuota.Location = new System.Drawing.Point(463, 123);
            this.textBoxQuota.Name = "textBoxQuota";
            this.textBoxQuota.Size = new System.Drawing.Size(82, 23);
            this.textBoxQuota.TabIndex = 38;
            this.textBoxQuota.Text = "2000";
            this.textBoxQuota.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 348);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LicGeneratorXP";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.Identidad.ResumeLayout(false);
            this.Identidad.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxSecret;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Identidad;
        private System.Windows.Forms.Button buttonSaveID;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBoxRandom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.TextBox textBoxSignature;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxRestriction;
        private System.Windows.Forms.TextBox textBoxFreeComment;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimeValidTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimeValidFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxLicID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxFreeCommentID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxMachineName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxMAC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRandomID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonCalcID;
        private System.Windows.Forms.TextBox textBoxSignatureID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxIdentity;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxQuota;
        private System.Windows.Forms.Label label16;
    }
}

